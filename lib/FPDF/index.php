<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

use setasign\Fpdi;

require_once 'vendor/autoload.php';

$parser = 'fpdi-pdf-parser';
$filename = 'FormW-9.pdf';

class CustomPdf extends Fpdi\Fpdi
{
    /**
     * @var string
     */
    protected $pdfParserClass = null;

    /**
     * Set the pdf reader class.
     *
     * @param string $pdfParserClass
     */
    public function setPdfParserClass($pdfParserClass)
    {
        $this->pdfParserClass = $pdfParserClass;
    }

    /**
     * @param Fpdi\PdfParser\StreamReader $streamReader
     * @return Fpdi\PdfParser\PdfParser|\setasign\FpdiPdfParser\PdfParser\PdfParser
     */
    protected function getPdfParserInstance(Fpdi\PdfParser\StreamReader $streamReader)
    {
        if ($this->pdfParserClass !== null) {
            return new $this->pdfParserClass($streamReader);
        }

        return parent::getPdfParserInstance($streamReader);
    }

    /**
     * Checks whether a compressed cross-reference reader instance was used or not.
     *
     * @return bool
     */
    public function isCompressedXref()
    {
        foreach (array_keys($this->readers) as $readerId) {
            $crossReference = $this->getPdfReader($readerId)->getParser()->getCrossReference();
            $readers = $crossReference->getReaders();
            foreach ($readers as $reader) {
                if ($reader instanceof \setasign\FpdiPdfParser\PdfParser\CrossReference\CompressedReader) {
                    return true;
                }
            }
        }

        return false;
    }
}

$pdf = new CustomPdf();
if ($parser === 'default') {
    $pdf->setPdfParserClass(Fpdi\PdfParser\PdfParser::class);
}

$pdf->AddPage();
$pageCount = $pdf->setSourceFile($filename);
$tplIdx = $pdf->ImportPage(1);
$size = $pdf->useTemplate($tplIdx, 0, 0, null, null, true);

$leftMargin = $pdf->getX() + 12.8;
$pdf->SetLeftMargin($leftMargin);
$pdf->SetFont('helvetica', 'B', 8);

if ($pdf->isCompressedXref()) {
    $pdf->SetTextColor(72, 179, 84);
    $pdf->Write(5, 'This document uses new PDF compression technics introduced in PDF version 1.5 ;-)');
} else {
//    $pdf->SetTextColor(0, 0, 0);
//    $pdf->Write(50, 'Anthony Vu');
//    $pdf->Ln(0);
//
//    $pdf->Write(95, 'x');
//    $pdf->Ln(0);
//
//    $pdf->Write(160, '1234 Anywhere St');
//    $pdf->Ln(0);
//
//    $pdf->Write(177, 'Phoenix, Arizona 85234, United States');
//    $pdf->Ln(0);
//
//    $pdf->SetLeftMargin(148);
//    $pdf->Write(225, '1');
//    $pdf->SetLeftMargin(153);
//    $pdf->Write(225, '2');
//    $pdf->SetLeftMargin(158);
//    $pdf->Write(225, '3');
//    $pdf->SetLeftMargin(168.5);
//    $pdf->Write(225, '4');
//    $pdf->SetLeftMargin(173.5);
//    $pdf->Write(225, '5');
//    $pdf->SetLeftMargin(183.5);
//    $pdf->Write(225, '6');
//    $pdf->SetLeftMargin(188.5);
//    $pdf->Write(225, '7');
//    $pdf->SetLeftMargin(193.5);
//    $pdf->Write(225, '8');
//    $pdf->SetLeftMargin(198.5);
//    $pdf->Write(225, '9');
//
//    $pdf->Ln(0);
//    $pdf->SetLeftMargin(147);
//    $pdf->Write(124, '01/17/2018');
//
//    $pdf->Ln(0);
//    $pdf->Image('sign.png',48,191,30, 8);
}


$pdf->Output('F', 'result.pdf');
$pdf->Output('I', 'test.pdf');


