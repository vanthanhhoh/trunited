$(document).ready(function(e) {
    
$('.single-item').slick({
      dots: false,
      infinite: true,
      arrows:true,
      speed: 300,
      slidesToShow: 6,
      slidesToScroll: 6,
  responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 4,
        slidesToScroll: 4, 
      }
    },
    {
      breakpoint: 767,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 3
      }
    },
    {
      breakpoint: 599,
      
      settings: {
        slidesToShow: 2,
        slidesToScroll:2
      }
    }
      ,
    {
      breakpoint: 479,
      settings: {
        slidesToShow: 1,
        slidesToScroll:1
      }
    }
    
  ]
}); 
    
    
$('.single-item1').slick({
      dots: true,
      infinite: false,
      autoPlay:false,
      arrows:false,
      speed: 300,
      slidesToShow: 3,
      slidesToScroll: 3,
       draggable:false,
       
  responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 3, 
      }
    },
    {
      breakpoint: 767,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2,
         infinite: true,
         autoPlay:true,  
         draggable:true,
         dots:true,
          
      }
    },
    {
      breakpoint: 599,
        
      settings: {
            dots: true,
        slidesToShow: 1,
        slidesToScroll:1
      }
    }
      ,
    {
      breakpoint: 479,
      settings: {
           dots: true,
        slidesToShow: 1,
        slidesToScroll:1
      }
    }
    
  ]
}); 
 
    
    
   
$('.single-item2').slick({
      dots: true,
      infinite: true,
      arrows:true,
      speed: 300,
      slidesToShow: 5,
      slidesToScroll: 5,
  responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 4,
        slidesToScroll: 4, 
      }
    },
     {
      breakpoint: 767,
      settings: {
          
        slidesToShow: 3,
        slidesToScroll: 3, 
      }
    },
    
    {
      breakpoint: 599,
      settings: {
           dots: false,
        slidesToShow: 2,
        slidesToScroll:2
      }
    }
      ,
    {
      breakpoint: 479,
      settings: {
           dots: false,
        slidesToShow: 1,
        slidesToScroll:1
      }
    }
      
    
  ]
}); 
    
    
    
// thmb nail gallery     
        
   
$('.single-item3').slick({
      dots: false, 
      arrows:true,
      speed: 300,
      slidesToShow: 4,
      slidesToScroll: 4,
  responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 4,
        slidesToScroll: 4, 
      }
    },
     {
      breakpoint: 767,
      settings: {
          
        slidesToShow: 3,
        slidesToScroll:3, 
      }
    },
    
    {
      breakpoint: 599,
      settings: {
           dots: false,
        slidesToShow: 2,
        slidesToScroll:2
      }
    }
      ,
    {
      breakpoint: 479,
      settings: {
           dots: false,
        slidesToShow:1,
        slidesToScroll:1
      }
    }
      
    
  ]
}); 
    
 
// countries show hide    
 $(".countries > a").click(function(){
    $(".footer_countries").toggle();
});
				
 // back to top 
     $(window).scroll(function () {
			if ($(this).scrollTop() > 100) {
				$('.BaclToTop').fadeIn();
			} else {
				$('.BaclToTop').fadeOut();
			} 
		}); 
   
 // countries show hide 
    
//    $(".experience_mess a").click(function(){
//       $(".experience").hide();
//    });
//
//    $(".experience_mess a").click(function(){
//        $(".experience").show();
//    });
    
    $(".experience_mess a").click(function(){
        $(".experience_mess a").toggleClass("intro");  
    });
     $(".experience_mess a").click(function(){
        $(".experience").toggleClass("intro_exp");  
    });
   // 
  $(function(){
  $(window).scroll(function(){
    var winTop = $(window).scrollTop();
    if(winTop >= 100){
      $("body").addClass("sticky-header");
    }else{
      $("body").removeClass("sticky-header");
    } 
  }); 
}); 
  //navigation--------------------
    $('.shopby > a').click(function(e){
        $('body').toggleClass('main_nav');
        $('body').removeClass('nav_rgt');
        $('.sub_nav').slideToggle();
        $('.rgt_drop').slideUp();
         $('.shop_block').slideUp();
    });
    
    $('.over_lay').click(function(e){
        $('body').removeClass('nav_rgt');
        $('body').removeClass('main_nav');
         $('body').removeClass('search_box');
         $('.sub_nav, .rgt_drop, .shop_block').slideUp();
        
    });
   
    $('.account a').click(function(e){
        $('body').toggleClass('nav_rgt');
        $('body').removeClass('main_nav');
        $('.rgt_drop').slideToggle();
       $('.sub_nav').slideUp(); 
        $('.shop_block').slideUp();
    });
    
    $('.sub_nav ul li ul').parent().find('>a').prepend('<span></span>');
    $('.sub_nav ul li a').click(function(e){
        $(this).closest('li').addClass('drop_add');
    });
    
    $('.sub_nav a.back_btn').click(function(e){
       $(this).parent().parent().closest('li').removeClass('drop_add');
    });
    
     //search box--------------------
    
    $('.shop_search_but a').click(function(e){
        $('body').addClass('search_box');
    });
    
    $('.btn_remove').click(function(e){
        $('body').removeClass('search_box');
    });
    
    $('.shop_search_text a').click(function(e){
        $(this).parent().find('.shop_block').slideToggle();
        $(this).parent().toggleClass('search_on');
        $('.rgt_drop').slideUp();
        $('.sub_nav').slideUp();
    });
    
});

 
