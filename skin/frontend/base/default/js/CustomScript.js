jQuery(document).ready(function() {
	// For Menu Button -----
	jQuery('.navbar-toggle').click(function(e) {
		jQuery('body').toggleClass('mobo_menu');
	});
});

// For Sticky footer -----
function sticky_ftr(){
	var _footerH = jQuery('footer').height();
	jQuery('.wrapper').css({'margin-bottom': -_footerH });
	jQuery('.push').height(_footerH);
}
sticky_ftr();