<?php

class Trunited_ConnectionMap_Block_Tree extends Mage_Core_Block_Template
{
    /* @var $model Trunited_ConnectionMap_Model_Tree */
    protected $model;
    protected $customerData;
    protected $badgeMap;

    public function getTreeModel()
    {
        if(!$this->model){
            $this->model = Mage::getModel('truinted_cm/tree');
        }
        return $this->model;
    }

    public function getCustomerData()
    {
        if(!$this->customerData){
            $model = $this->getTreeModel();
            $this->customerData = $model->getCustomerData();
        }
        return $this->customerData;
    }

    public function getCustomerCount($data)
    {
        $count = count($data);
        if($count)
        $count = $count - 1; // remove root
        return $count;
    }

    public function renderTreeItem($treeMap, $level)
    {
        $itemBlock = new Trunited_ConnectionMap_Block_Tree_Item();
        $itemBlock->setTreeMap($treeMap)->setLevel($level);
        return $itemBlock->toHtml();
    }

    public function renderTreePopup($customerData, $treeMap)
    {
        $popupBlock = new Trunited_ConnectionMap_Block_Tree_Popup();
        $popupBlock->setCustomerData($customerData)->setTreeMap($treeMap);
        return $popupBlock->toHtml();
    }

    public function getBadgeMap()
    {
        if(!$this->badgeMap){
            $data = array();
            $media_path = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA);
            $root_badget_dir = $media_path . 'badges/icon/';
            /* @var $model Magestore_Badges_Model_Badges */
            $model = Mage::getModel('badges/badges');
            $collection = $model->getCollection();
            foreach($collection as $item){
                $row = $item->getData();
                $row_badge = strtolower($row['name']);
                $row_image = $row['image_name'];
                $image_path = $root_badget_dir . $row_image;
                $data[$row_badge] = $image_path;
            }
            $this->badgeMap = $data;
        }
        return $this->badgeMap;
    }

    public function getBadgeImage($badge)
    {
        $badge = strtolower($badge);
        $badgeMap = $this->getBadgeMap();
        $image_path = isset($badgeMap[$badge]) ? $badgeMap[$badge] : false;
        return $image_path;
    }

    public function getCustomerName($customer_name)
    {
        if(strpos(strtolower($customer_name), 'socomm') !== false){
            $customer_name = "Trunited";
        }
        return $customer_name;
    }
}
