<?php

class Trunited_ConnectionMap_Model_Tree
{
    /* @var $resource Mage_Core_Model_Resource */
    protected $resource;
    protected $connection;

    public function getResource()
    {
        if(!$this->resource){
            $this->resource = Mage::getSingleton('core/resource');
        }
        return $this->resource;
    }

    public function getConnection()
    {
        $resource = $this->getResource();
        if(!$resource){
            return false;
        }
        if(!$this->connection){
            $this->connection = $resource->getConnection(Mage_Core_Model_Resource::DEFAULT_SETUP_RESOURCE);
        }
        return $this->connection;
    }

    public function getPayoutPeriodId()
    {
        $connection = $this->getConnection();
        if(!$connection)
            return false;

        $payout_period_id = false;
        $row = $connection->fetchRow("SELECT * FROM tr_PayoutPeriod WHERE start_date <= CURDATE() AND end_date >= CURDATE()");
        if($row){
            $payout_period_id = $row['payout_period_id'];
        }
        return $payout_period_id;
    }

    public function getCustomerId()
    {
        //return 2401;
        /* @var $customer Mage_Customer_Model_Session */
        $customer = Mage::getSingleton('customer/session');
        if(!$customer->isLoggedIn())
            return null;
        return $customer->getId();
    }

    public function getCustomerData()
    {
        $customer_id = $this->getCustomerId();

        if(!$customer_id)
            return array();

        $connection = $this->getConnection();
        if(!$connection)
            return array();

        $payout_period_id = $this->getPayoutPeriodId();
        if(!$payout_period_id)
            return array();

        $query = "SELECT g.TreeRootID, g.Level, pps.*
                    FROM tr_Genealogy g
                      JOIN tr_payoutperiod_stats pps ON g.personid = pps.customer_id
                    WHERE g.TreeRootID = ? AND pps.payout_period_id = ? ORDER BY g.Level, pps.frontline DESC";
        $data = $connection->fetchAll($query, array($customer_id, $payout_period_id));
        return $data;
    }

    public function getCustomerByParent($data, $customer_id)
    {
        $result = array();
        foreach($data as $item){
            $parent_id = $item['parent_id'];
            if($parent_id == $customer_id)
                $result[] = $item;
        }
        return $result;
    }

    public function getCustomerByLevel($data, $level = 0)
    {
        $result = array();
        foreach($data as $item){
            $item_lever = $item['Level'];
            if($item_lever == $level)
                $result[] = $item;
        }
        return $result;
    }

    public function buildTreeMap($data)
    {
        $result = $this->_buildDefaultTreeLevel();
        if(!$data)
            return $result;
        foreach($data as $item){
            $item_level = $item['Level'];
            if($item_level == 0){
                $key = $item['customer_id'];
            } else {
                $key = $item['parent_id'];
            }
            if(!isset($result[$item_level][$key]))
                $result[$item_level][$key] = array();
            $result[$item_level][$key][] = $item;
        }
        return $result;
    }

    protected function _buildDefaultTreeLevel()
    {
        $data = array();
        for($i = 0; $i <= 15; $i++){
            $data[$i] = array();
        }
        return $data;
    }

    public function buildSearchData($data){
        $result = array();
        foreach($data as $row){
            $item = array();
            $item['id'] = $row['customer_id'];
            $item['level'] = $row['Level'];
            $item['label'] = $row['name'];
            $item['value'] = $row['name'];
            $item['parent_id'] = $row['parent_id'];
            $result[$row['customer_id']] = $item;
        }
        return $result;
    }

    public function buildSearchAutoComplete($data){
        $result = array();
        foreach($data as $row){
            $result[] = array(
                'id' => $row['customer_id'],
                'label' => $row['name'],
                'value' => $row['name']
            );
        }
        return $result;
    }
}