<?php

class Trunited_ConnectionMap2_Block_Tree_Item extends Mage_Core_Block_Template
{
    protected $_template = 'truinted/connectionmap2/item.phtml';
    protected $blockTree;

    public function getBlockTree()
    {
        if(!$this->blockTree){
            $this->blockTree = new Trunited_ConnectionMap2_Block_Tree();
        }
        return $this->blockTree;
    }

    public function splitFirstname($name)
    {
        $name = $this->getBlockTree()->getCustomerName($name);
        $name = trim($name);
        $split = explode(" ", $name);
        $result = $name;
        if(isset($split[0]) && $split[0]){
            $result = $split[0];
        }
        return $result;
    }

    public function getBackgroundClass($count_child, $customer_data)
    {
        if(!$count_child){
            return "circle_no_child";
        }
        $badge = $customer_data['badge'];
        $badge = strtolower($badge);
        $class = "circle_" . $badge;
        return $class;
    }

    public function getCustomerBadgeFilter($customer_data){
        $badge = $customer_data['badge'];
        $badge = strtolower($badge);
        $result = 0;
        switch($badge){
            case "customer":
                $result = 1;
                break;
            case "affiliate":
                $result = 2;
                break;
            case "promoter":
                $result = 3;
                break;
            case "pacesetter":
                $result = 4;
                break;
            case "influencer":
                $result = 5;
                break;
            case "partner":
                $result = 6;
                break;
            case "icon":
                $result = 7;
                break;
            case "legend":
                $result = 8;
                break;
            default:
                break;
        }
        return $result;
    }
}
