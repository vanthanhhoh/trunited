<?php

class Trunited_ConnectionMap2_Block_Tree_Popup extends Mage_Core_Block_Template
{
    protected $_template = 'truinted/connectionmap2/popup.phtml';
    protected $blockItem;
    protected $blockTree;

    public function getBlockItem()
    {
        if(!$this->blockItem){
            $this->blockItem = new Trunited_ConnectionMap2_Block_Tree_Item();
        }
        return $this->blockItem;
    }

    public function getBackgroundClass($customer_data, $treeMap)
    {
        $level = $customer_data['Level'];
        $next_level = $level + 1;
        $customer_id = $customer_data['customer_id'];
        $child = isset($treeMap[$next_level][$customer_id]) ? $treeMap[$next_level][$customer_id] : array();
        $count = count($child);
        $blockItem = $this->getBlockItem();
        return $blockItem->getBackgroundClass($count, $customer_data);
    }

    public function formatPoint($point)
    {
        return number_format($point, 1);
    }

    public function formatPercent($percent){
        return number_format($percent, 0);
    }

    public function formatPopupDate($date = null){
        if(!$date){
            return "N/A";
        }
        $format = date('m/d/Y', strtotime($date));
        return $format;
    }

    public function getRootName($treeMap)
    {
        /* @var $model Trunited_ConnectionMap2_Model_Tree */
        $model = Mage::getModel('truinted_cm2/tree');
        $customer_id = $model->getCustomerId();
        $rootCustomer = isset($treeMap[0][$customer_id][0]) ? $treeMap[0][$customer_id][0] : false;
        $customer_name = "";
        if($rootCustomer){
            $customer_name = $rootCustomer['name'];
        }
        return $customer_name;
    }

    public function getBlockTree()
    {
        if(!$this->blockTree){
            $this->blockTree = new Trunited_ConnectionMap2_Block_Tree();
        }
        return $this->blockTree;
    }

    public function getBadgeImage($badge)
    {
        $blockTree = $this->getBlockTree();
        return $blockTree->getBadgeImage($badge);
    }

    public function getParentName($customer_data, $customerData)
    {
        $parent_name = "";
        $parent_id = $customer_data['parent_id'];
        foreach($customerData as $item){
            if($item['customer_id'] == $parent_id){
                $parent_name = $item['name'];
                break;
            }
        }
        return $this->getBlockTree()->getCustomerName($parent_name);
    }

    public function getCustomerName($customer_name)
    {
        return $this->getBlockTree()->getCustomerName($customer_name);
    }
}
