<?php

class Trunited_ConnectionMap2_ConnectionmapController extends Mage_Core_Controller_Front_Action
{
    public function indexAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }
    public function countfilterajaxAction()
    {
        $badge_filter  =   $this->getRequest()->getParam('badge_filter');
        $badge_filter = explode("|", $badge_filter);
         $badge_filter = implode("','", $badge_filter);

        $personal_point_min =   $this->getRequest()->getParam('personal_point_min');
        $personal_point_max =   $this->getRequest()->getParam('personal_point_max');
        $connection_point_min =   $this->getRequest()->getParam('connection_point_min');
        $connection_point_max =   $this->getRequest()->getParam('connection_point_max');
        $trubox_point =   $this->getRequest()->getParam('trubox_point');

        $treeModel = Mage::getModel('truinted_cm2/tree');
    //    $customerData = $treeModel->filterCustomer($badge_filter, $personal_point_min, $personal_point_max, $connection_point_min, $connection_point_max, $trubox_point);

        $amount = $treeModel->countFilterCustomer($badge_filter, $personal_point_min, $personal_point_max, $connection_point_min, $connection_point_max, $trubox_point);
        echo $amount;
    }
    /*
    {"TreeRootID":"2094","Level":"2","payout_period_id":"12","customer_id":"27411","name":"Test8 Test8","badge":"Customer","member_since":"2017-10-02","depth_credit":"0","frontline":"0","affiliate_renewal":"1900-01-01","parent_id":"2549","district":"10","personal_points":"0.0000","match_points":"0.0000","affiliate_points":"0.0000","next_level_points":"0.0000","unearned_points":"0.0000","efficient_match_points":"0.0000","on_hold_points":"0.0000","trubox_points":"0.0000","social_stock":"0","ytd_rewards":null,"trubox_engagement_pct":"0.0000","sweet15_trubox_engagement_pct":"0.0000","all_trubox_engagement_pct":"0.0000","customer_engagement_pct":"0.0000","sweet15_customer_engagement_pct":"0.0000","all_customer_engagement_pct":"0.0000"}
    */

    /*
      parent_name      
*/
   
    public function getFilterChildMemberAction()
    {

        
        $badge_filter  =   $this->getRequest()->getParam('badge_filter');
        $badge_filter = explode("|", $badge_filter);
        $badge_filter = implode("','", $badge_filter);
        $personal_point_min =   $this->getRequest()->getParam('personal_point_min');
        $personal_point_max =   $this->getRequest()->getParam('personal_point_max');
        $connection_point_min =   $this->getRequest()->getParam('connection_point_min');
        $connection_point_max =   $this->getRequest()->getParam('connection_point_max');
        $trubox_point =   $this->getRequest()->getParam('trubox_point');
        $parentId  =   $this->getRequest()->getParam('parent_id');

        $treeModel = Mage::getModel('truinted_cm2/tree');
        $customerData = $treeModel->filterCustomer($badge_filter, $personal_point_min, $personal_point_max, $connection_point_min, $connection_point_max, $trubox_point,$parentId);

        $memberCount = $this->getMemBerCountArr($customerData);
        $level = 1;
        $memberNameArr = $treeModel->getMemberNameArr();

         foreach($customerData  as $key=> $member)
        {
            $customerData[$key]['parent_name'] = $memberNameArr[$member['customer_id']];
            $customerData[$key]['customer_badge_img'] = $this->getBadgeImage($member['badge']);
            $customerData[$key]['depth_credit'] = $this->formatPoint($member['depth_credit']);
            $customerData[$key]['personal_points'] = $this->formatPoint($member['personal_points']);
            $customerData[$key]['match_points'] = $this->formatPoint($member['match_points']);
            $customerData[$key]['affiliate_points'] = $this->formatPoint($member['affiliate_points']);
            $customerData[$key]['next_level_points'] = $this->formatPoint($member['next_level_points']);
            $customerData[$key]['unearned_points'] = $this->formatPoint($member['unearned_points']);
             $customerData[$key]['on_hold_points'] = $this->formatPoint($member['on_hold_points']);

            $customerData[$key]['trubox_engagement_pct'] = $this->formatPercent($member['trubox_engagement_pct']);
            $customerData[$key]['customer_engagement_pct'] = $this->formatPercent($member['customer_engagement_pct']);
            $customerData[$key]['member_since'] = $this->formatPopupDate($member['member_since']);
            $customerData[$key]['affiliate_renewal'] = $this->formatPopupDate($member['affiliate_renewal']);

           // $customerData[$key]['child_count'] =0;
            if(isset($memberCount[$member['customer_id']]))
            {
                // $customerData[$key]['child_count'] = $memberCount[$member['customer_id']];
            }
           // $customerData[$key]['child_count'] =2;
            $customerData[$key]['backgroundClass']= $this->getBackgroundClass($customerData[$key]['child_count'], $customerData[$key]);
            $customerData[$key]['customerBadgeFilter'] = $this->getCustomerBadgeFilter( $customerData[$key]);
            $customerData[$key]['next_level'] =  $customerData[$key]['Level']+1;
            $customerData[$key]['display_name'] = $this->getCustomerName($member['name']);
            $customerData[$key]['name'] = $this->splitFirstname($customerData[$key]['name']);
            $level = $customerData[$key]['Level'] ;
        }
        //print_r($memberCount); die();
        $output= array();
        

        $output['parent_id'] = $parentId ;
        $output['level'] = $level; 
        $output['items']=$customerData;
    // echo "<pre>"; print_r($output); die();

        ob_start('ob_gzhandler');
        echo json_encode($output);

    }
    public function getMemBerCountArr($customerData)
    {
        $memberCountArr = [];
        foreach($customerData  as $member)
        {
            $memberCountArr[] = $member['parent_id']; 
        }
        return array_count_values($memberCountArr);
    }

    public function getBackgroundClass($count_child, $customer)
    {

        if(!$count_child){
            return "circle_no_child";
        }
        $badge = $customer['badge'];
        $badge = strtolower($badge);
        $class = "circle_" . $badge;
        return $class;
    }
    public function getCustomerBadgeFilter($customer_data){
        $badge = $customer_data['badge'];
        $badge = strtolower($badge);
        $result = 0;
        switch($badge){
            case "customer":
                $result = 1;
                break;
            case "affiliate":
                $result = 2;
                break;
            case "promoter":
                $result = 3;
                break;
            case "pacesetter":
                $result = 4;
                break;
            case "influencer":
                $result = 5;
                break;
            case "partner":
                $result = 6;
                break;
            case "icon":
                $result = 7;
                break;
            case "legend":
                $result = 8;
                break;
            default:
                break;
        }
        return $result;
    }
     public function splitFirstname($name)
    {
        
        $name = trim($name);
        $split = explode(" ", $name);
        $result = $name;
        if(isset($split[0]) && $split[0]){
            $result = $split[0];
        }
        return $result;
    }
    public function getBadgeImage($badge)
    {
        $badge = strtolower($badge);
        $badgeMap = $this->getBadgeMap();
        $image_path = isset($badgeMap[$badge]) ? $badgeMap[$badge] : false;
        return $image_path;
    }
    public function getBadgeMap()
    {
        if(!$this->badgeMap){
            $data = array();
            $media_path = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA);
            $root_badget_dir = $media_path . 'badges/icon/';
            /* @var $model Magestore_Badges_Model_Badges */
            $model = Mage::getModel('badges/badges');
            $collection = $model->getCollection();
            foreach($collection as $item){
                $row = $item->getData();
                $row_badge = strtolower($row['name']);
                $row_image = $row['image_name'];
                $image_path = $root_badget_dir . $row_image;
                $data[$row_badge] = $image_path;
            }
            $this->badgeMap = $data;
        }
        return $this->badgeMap;
    }
    public function formatPopupDate($date = null){
        if(!$date){
            return "N/A";
        }
        $format = date('m/d/Y', strtotime($date));
        return $format;
    }
     public function formatPoint($point)
    {
        return number_format($point, 1);
    }

    public function formatPercent($percent){
        return number_format($percent, 0);
    }
    public function getCustomerName($customer_name)
    {
        if(strpos(strtolower($customer_name), 'socomm') !== false){
            $customer_name = "Trunited";
        }
        return $customer_name;
    }
}