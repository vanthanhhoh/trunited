<?php

class Trunited_ConnectionMap2_Model_Tree
{
    /* @var $resource Mage_Core_Model_Resource */
    protected $resource;
    protected $connection;

    public function getResource()
    {
        if(!$this->resource){
            $this->resource = Mage::getSingleton('core/resource');
        }
        return $this->resource;
    }

    public function getConnection()
    {
        $resource = $this->getResource();
        if(!$resource){
            return false;
        }
        if(!$this->connection){
            $this->connection = $resource->getConnection(Mage_Core_Model_Resource::DEFAULT_SETUP_RESOURCE);
        }
        return $this->connection;
    }

    public function getPayoutPeriodId()
    {
        $connection = $this->getConnection();
        if(!$connection)
            return false;

        $payout_period_id = false;
        $row = $connection->fetchRow("SELECT * FROM tr_PayoutPeriod WHERE start_date <= CURDATE() AND end_date >= CURDATE()");
        if($row){
            $payout_period_id = $row['payout_period_id'];
        }
        return $payout_period_id;
    }

    public function getCustomerId()
    {
        /* @var $customer Mage_Customer_Model_Session */
        $customer = Mage::getSingleton('customer/session');
        if(!$customer->isLoggedIn())
            return null;
        return $customer->getId();
    }
    public function getCurrentCustomerData()
    {
         $customer_id = $this->getCustomerId();
        if(!$customer_id)
            return array();

        $connection = $this->getConnection();
        if(!$connection)
            return array();

        $payout_period_id = $this->getPayoutPeriodId();
        if(!$payout_period_id)
            return array();

        $query = "SELECT g.TreeRootID, g.Level, pps.*
                    FROM tr_Genealogy g
                      JOIN tr_payoutperiod_stats pps ON g.personid = pps.customer_id
                    WHERE g.TreeRootID = ? AND pps.payout_period_id = ? and g.personid =?";
        $data = $connection->fetchRow($query, array($customer_id, $payout_period_id,$customer_id));
        return $data;
    }
    public function getCustomerData()
    {
        $customer_id = $this->getCustomerId();
        if(!$customer_id)
            return array();

        $connection = $this->getConnection();
        if(!$connection)
            return array();

        $payout_period_id = $this->getPayoutPeriodId();
        if(!$payout_period_id)
            return array();

        $query = "SELECT g.TreeRootID, g.Level, pps.*
                    FROM tr_Genealogy g
                      JOIN tr_payoutperiod_stats pps ON g.personid = pps.customer_id
                    WHERE g.TreeRootID = ? AND pps.payout_period_id = ? ORDER BY g.Level, pps.frontline DESC LIMIT 12000";
        $data = $connection->fetchAll($query, array($customer_id, $payout_period_id));
        return $data;
    }

    public function getCustomerByParent($data, $customer_id)
    {
        $result = array();
        foreach($data as $item){
            $parent_id = $item['parent_id'];
            if($parent_id == $customer_id)
                $result[] = $item;
        }
        return $result;
    }

    public function getCustomerByLevel($data, $level = 0)
    {
        $result = array();
        foreach($data as $item){
            $item_lever = $item['Level'];
            if($item_lever == $level)
                $result[] = $item;
        }
        return $result;
    }

    public function buildTreeMap($data)
    {
        $result = $this->_buildDefaultTreeLevel();

        if(!$data)
            return $result;
        foreach($data as $item){
            $item_level = $item['Level'];
            if($item_level == 0){
                $key = $item['customer_id'];
            } else {
                $key = $item['parent_id'];
            }
            if(!isset($result[$item_level][$key]))
                $result[$item_level][$key] = array();
            $result[$item_level][$key][] = $item;
        }
        return $result;
    }

    protected function _buildDefaultTreeLevel()
    {
        $data = array();
        for($i = 0; $i <= 15; $i++){
            $data[$i] = array();
        }
        return $data;
    }
    public function filterCustomer($badge_filter=null, $personal_point_min=null, $personal_point_max=null, $connection_point_min=null, $connection_point_max=null, $trubox_point=null, $parentId=null)
    {
         $customer_id = $this->getCustomerId();

        if(!$customer_id)
            return array();

        $connection = $this->getConnection();
        if(!$connection)
            return array();

        $payout_period_id = $this->getPayoutPeriodId();
        if(!$payout_period_id)
            return array();
 

       /* $query = "SELECT g.TreeRootID, g.Level, pps.*
                    FROM tr_Genealogy g
                      JOIN tr_payoutperiod_stats pps ON g.personid = pps.customer_id
                    WHERE g.TreeRootID = :customer_id AND pps.payout_period_id = :payout_period_id
                    and customer_id <>:customer_id
                    ";
                    */
        $query = "SELECT distinct g.TreeRootID, g.Level , pps.*
                    FROM tr_Genealogy g
                      JOIN tr_payoutperiod_stats pps ON g.personid = pps.customer_id
                                          
                    WHERE g.TreeRootID = :customer_id AND pps.payout_period_id = :payout_period_id 
                    ";
        $queryCount = "SELECT ParentId , (count(ParentId)) as child_count
                    FROM tr_Genealogy g
                      JOIN tr_payoutperiod_stats pps ON g.personid = pps.customer_id
                    WHERE g.TreeRootID = :customer_id AND pps.payout_period_id = :payout_period_id
                    and customer_id <>:customer_id
                    ";
        $bindParam = array('customer_id'=>$customer_id,'payout_period_id'=>$payout_period_id);

        if($parentId)
        {

            $query .= " and pps.parent_id = :parent_id ";
         //   $queryCount .= " and parent_id = :parent_id ";
            $bindParam['parent_id']=$parentId;

        }
         if($badge_filter)
        {
            $query .= " and badge in('$badge_filter')  ";
             $queryCount .= " and badge in('$badge_filter')  ";

        }
        if($personal_point_min)
        {
            $query = "  and personal_points >= :personal_point_min ";
            $queryCount = "  and personal_points >= :personal_point_min ";
            $bindParam['personal_point_min']=$personal_point_min;
        }
        if($personal_point_max)
        {
             $query = " and personal_points <= :personal_point_max and   ";
             $queryCount = " and personal_points <= :personal_point_max and   ";
              $bindParam['personal_point_max']=$personal_point_max;
        }
        if($connection_point_min)
        {
             $query = " and match_points >= :connection_point_min  ";
              $queryCount = " and match_points >= :connection_point_min  ";
              $bindParam['connection_point_min']=$connection_point_min;
        }
        if($connection_point_max)
        {
              $query = " and match_points <= :connection_point_max  ";
               $queryCount = " and match_points <= :connection_point_max  ";
               $bindParam['connection_point_max']=$connection_point_max;
        }

        if($trubox_point)
        {
             $query .= " and trubox_points > 0 ";
              $queryCount .= " and trubox_points > 0 ";
                    
        }
        $queryCount .=' GROUP  BY  ParentId';
        $query .= " ORDER BY g.Level, pps.frontline, pps.customer_id ";  
        $finalQuery = "select a.*, b.child_count 
                    FROM ($query) a LEFT JOIN ($queryCount) b
                    ON a.customer_id = b.ParentId
                    ";



        $cacheId = serialize($bindParam);
        if (false !== ($data = Mage::app()->getCache()->load($cacheId))) {
         
            $data = unserialize($data);
        } else {
            $data = $connection->fetchAll($finalQuery, $bindParam);
            Mage::app()->getCache()->save(serialize($data), $cacheId,array('customer'),3600);
        }


        


        return $data;
    }
    public function countFilterCustomer($badge_filter=null, $personal_point_min=null, $personal_point_max=null, $connection_point_min=null, $connection_point_max=null, $trubox_point=null)
    {
         $customer_id = $this->getCustomerId();

        if(!$customer_id)
            return 0;

        $connection = $this->getConnection();
        if(!$connection)
            return 0;

        $payout_period_id = $this->getPayoutPeriodId();
        if(!$payout_period_id)
            return 0;
 

        $query = "SELECT count(*)
                    FROM tr_Genealogy g
                      JOIN tr_payoutperiod_stats pps ON g.personid = pps.customer_id
                    WHERE g.TreeRootID = :customer_id AND pps.payout_period_id = :payout_period_id
                    and customer_id <>:customer_id
                    ";

        $bindParam = array('customer_id'=>$customer_id,'payout_period_id'=>$payout_period_id);
        if($badge_filter)
        {
            $query .= " and badge in('$badge_filter')  ";

        }
        if($personal_point_min)
        {
            $query = "  and personal_points >= :personal_point_min ";
            $bindParam['personal_point_min']=$personal_point_min;
        }
        if($personal_point_max)
        {
             $query = " and personal_points <= :personal_point_max and   ";
              $bindParam['personal_point_max']=$personal_point_max;
        }
        if($connection_point_min)
        {
             $query = " and match_points >= :connection_point_min  ";
              $bindParam['connection_point_min']=$connection_point_min;
        }
        if($connection_point_max)
        {
              $query = " and match_points <= :connection_point_max  ";
               $bindParam['connection_point_max']=$connection_point_max;
        }

        if($trubox_point)
        {
             $query .= " and trubox_points > 0 ";
                    
        }

        $query .= " ORDER BY g.Level, pps.frontline ";  

        $data = $connection->fetchOne($query, $bindParam);
        return $data;
    }

    public function getMemberNameArr()
    {
        $cacheId = 'member_name_cache';
        if (false !== ($data = Mage::app()->getCache()->load($cacheId))) {
            $data = unserialize($data);
        } else {
            $query = 'select customer_id, name from tr_payoutperiod_stats';
         $connection = $this->getConnection();
         $result = $connection->fetchAll($query);
         $data = array();
         foreach($result as $row)
         {
            $data[$row['customer_id']] = $row['name'];
         }
            Mage::app()->getCache()->save(serialize($data), $cacheId,array('customer'),3600);
        }


            
        return $data;
    }


}