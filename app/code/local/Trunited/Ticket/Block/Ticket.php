<?php

use TicketEvolution\Client as TEvoClient;

class Trunited_Ticket_Block_Ticket extends Mage_Core_Block_Template
{
    protected $_client;

    protected $_location;

    public function _prepareLayout()
    {
        return parent::_prepareLayout();
    }

    public function getGoogleMapApiKey()
    {
        $storeId = Mage::app()->getStore()->getStoreId();
//        return Mage::getStoreConfig('onestepcheckout/general/api_key',$storeId);
        return 'AIzaSyA0I6SosyN-4GeFDvBtTFYERHABD_daOgE';
    }


    public function getHelper()
    {
        return Mage::helper('ticket');
    }

    public function getClient()
    {
        if ($this->_client == null) {
            if ($this->getHelper()->getConfigData('ticket_setting', 'is_sandbox')) {
                $this->_client = new TEvoClient([
                    'baseUrl' => $this->getHelper()->getConfigData('ticket_setting', 'url_sandbox'),
                    'apiVersion' => $this->getHelper()->getConfigData('ticket_setting', 'version'),
                    'apiToken' => $this->getHelper()->getConfigData('ticket_setting', 'token'),
                    'apiSecret' => $this->getHelper()->getConfigData('ticket_setting', 'secret'),
                ]);
            } else {
                $this->_client = new TEvoClient([
                    'baseUrl' => $this->getHelper()->getConfigData('ticket_setting', 'url_production'),
                    'apiVersion' => $this->getHelper()->getConfigData('ticket_setting', 'version'),
                    'apiToken' => $this->getHelper()->getConfigData('ticket_setting', 'token'),
                    'apiSecret' => $this->getHelper()->getConfigData('ticket_setting', 'secret'),
                ]);
            }
        }

        return $this->_client;
    }

    public function getLocation(){
        if ($this->_location == null){
            $this->_location = $this->getHelper()->getVisitorLocation();
        }

        return $this->_location;
    }

    public function getObjClient()
    {
        return $this->getClient();
    }

    public function getListEvents()
    {
        $lat = $this->getRequest()->getParam('lat');
        $lng = $this->getRequest()->getParam('lng');
        $keyword = $this->getRequest()->getParam('keyword');

        if($lat != null && $lng != null){
            return $this->getClient()->listEvents([
                'page' => 1,
                'per_page' => intval($this->getHelper()->getConfigData('events_setting', 'per_page')),
                'lat' => $lat,
                'lon' => $lng,
                'within' => $this->getHelper()->getConfigData('events_setting', 'within'),
                'order_by' => 'events.occurs_at',
            ]);
        }

        if($keyword != null)
        {
            return $this->getClient()->search([
                'order_by_popularity' => true,
                'page' => 1,
                'per_page' => 50,
                'q' => $keyword,
                'types' => 'events,performers,venues',
            ]);
        }

        if ($this->isEnableGeoIP()) {
            $location = $this->getLocation();
            if($location != false && $location['status'] == 'success'){

                return $this->getObjClient()->listEvents([
                    'page' => 1,
                    'per_page' => intval($this->getHelper()->getConfigData('events_setting', 'per_page')),
                    'lat' => $location['lat'],
                    'lon' => $location['lon'],
                    'within' => $this->getHelper()->getConfigData('events_setting', 'within'),
                    'order_by' => 'events.occurs_at',
                ]);
            }
        }

        return $this->getClient()->listEvents([
            'page' => 1,
            'per_page' => intval($this->getHelper()->getConfigData('events_setting', 'per_page')),
            'within' => $this->getHelper()->getConfigData('events_setting', 'within'),
            'order_by' => 'events.occurs_at',
        ]);
    }

    public function getCityVisitor()
    {
        $location = $this->getLocation();

        $name = '';
        if($location != false && $location['status'] == 'success'){
            $name .= $location['city'].', '.$location['countryCode'];
        }

        return $name;
    }

    public function getTitle()
    {
        return $this->getHelper()->getConfigData('ticket_setting', 'title');
    }

    public function isEnableGeoIP()
    {
        return $this->getHelper()->getConfigData('geoip_setting', 'enable');
    }

    public function getVisitorLocation()
    {
        return $this->getLocation();
    }

    public function getSearchingLocation()
    {
        if($this->getRequest()->getParam('format_address') != null)
            return $this->getRequest()->getParam('format_address');
        else
            return '';
    }

    public function getSearchAutocompleteUrl()
    {
        return Mage::getUrl('*/*/searchAuto');
    }

    public function getSuggestion()
    {
        $response = $this->getObjClient()->listSearchSuggestions([
            'entities' => 'events,performers,venues',
            'fuzzy' => true,
            'limit' => 20,
            'q' => '',
        ]);

        return $response['suggestions'];
    }

    public function getKeyword()
    {
        return $this->getRequest()->getParam('keyword');
    }

    public function getDetailEvent($id, $name)
    {
        return Mage::getUrl('*/event/', array('id'  => $id, 'slug'  => $name));
    }
}
