<?php

use TicketEvolution\Client as TEvoClient;

class Trunited_Ticket_Block_Event extends Mage_Core_Block_Template
{
    protected $_client;

    protected $_location;

    public function _prepareLayout()
    {
        return parent::_prepareLayout();
    }

    public function getHelper()
    {
        return Mage::helper('ticket');
    }

    public function getClient()
    {
        if ($this->_client == null) {
            if ($this->getHelper()->getConfigData('ticket_setting', 'is_sandbox')) {
                $this->_client = new TEvoClient([
                    'baseUrl' => $this->getHelper()->getConfigData('ticket_setting', 'url_sandbox'),
                    'apiVersion' => $this->getHelper()->getConfigData('ticket_setting', 'version'),
                    'apiToken' => $this->getHelper()->getConfigData('ticket_setting', 'token'),
                    'apiSecret' => $this->getHelper()->getConfigData('ticket_setting', 'secret'),
                ]);
            } else {
                $this->_client = new TEvoClient([
                    'baseUrl' => $this->getHelper()->getConfigData('ticket_setting', 'url_production'),
                    'apiVersion' => $this->getHelper()->getConfigData('ticket_setting', 'version'),
                    'apiToken' => $this->getHelper()->getConfigData('ticket_setting', 'token'),
                    'apiSecret' => $this->getHelper()->getConfigData('ticket_setting', 'secret'),
                ]);
            }
        }

        return $this->_client;
    }

    public function getListTicketGroups()
    {
        $id = $this->getRequest()->getParam('id');
        return $this->getClient()->listTicketGroups([
            'event_id' => (int)$id,
            'lightweight'   => true,
        ]);
    }

    public function getDetailed()
    {
        $id = $this->getRequest()->getParam('id');
        return $this->getClient()->showEvent([
            'event_id' => (int)$id,
        ]);
    }

    public function getBackUrl()
    {
        return Mage::getUrl('ticket');
    }

    public function getSearchUrl()
    {
        return Mage::getUrl('ticket');
    }

    public function getSearchAutocompleteUrl()
    {
        return Mage::getUrl('ticket/index/searchAuto');
    }

    public function getSuggestion()
    {
        $response = $this->getClient()->listSearchSuggestions([
            'entities' => 'events,performers,venues',
            'fuzzy' => true,
            'limit' => 1,
            'q' => '',
        ]);

        return $response['suggestions'];
    }
}
