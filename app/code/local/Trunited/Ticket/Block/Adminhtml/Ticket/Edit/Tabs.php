<?php

class Trunited_Ticket_Block_Adminhtml_Ticket_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
	public function __construct(){
		parent::__construct();
		$this->setId('ticket_tabs');
		$this->setDestElementId('edit_form');
		$this->setTitle(Mage::helper('ticket')->__('Item Information'));
	}

	protected function _beforeToHtml(){
		$this->addTab('form_section', array(
			'label'	 => Mage::helper('ticket')->__('Item Information'),
			'title'	 => Mage::helper('ticket')->__('Item Information'),
			'content'	 => $this->getLayout()->createBlock('ticket/adminhtml_ticket_edit_tab_form')->toHtml(),
		));
		return parent::_beforeToHtml();
	}
}