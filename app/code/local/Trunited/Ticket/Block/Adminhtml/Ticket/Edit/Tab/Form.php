<?php

class Trunited_Ticket_Block_Adminhtml_Ticket_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
	protected function _prepareForm(){
		$form = new Varien_Data_Form();
		$this->setForm($form);
		
		if (Mage::getSingleton('adminhtml/session')->getTicketData()){
			$data = Mage::getSingleton('adminhtml/session')->getTicketData();
			Mage::getSingleton('adminhtml/session')->setTicketData(null);
		}elseif(Mage::registry('ticket_data'))
			$data = Mage::registry('ticket_data')->getData();
		
		$fieldset = $form->addFieldset('ticket_form', array('legend'=>Mage::helper('ticket')->__('Item information')));

		$fieldset->addField('title', 'text', array(
			'label'		=> Mage::helper('ticket')->__('Title'),
			'class'		=> 'required-entry',
			'required'	=> true,
			'name'		=> 'title',
		));

		$fieldset->addField('filename', 'file', array(
			'label'		=> Mage::helper('ticket')->__('File'),
			'required'	=> false,
			'name'		=> 'filename',
		));

		$fieldset->addField('status', 'select', array(
			'label'		=> Mage::helper('ticket')->__('Status'),
			'name'		=> 'status',
			'values'	=> Mage::getSingleton('ticket/status')->getOptionHash(),
		));

		$fieldset->addField('content', 'editor', array(
			'name'		=> 'content',
			'label'		=> Mage::helper('ticket')->__('Content'),
			'title'		=> Mage::helper('ticket')->__('Content'),
			'style'		=> 'width:700px; height:500px;',
			'wysiwyg'	=> false,
			'required'	=> true,
		));

		$form->setValues($data);
		return parent::_prepareForm();
	}
}