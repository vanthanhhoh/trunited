<?php

class Trunited_Ticket_Block_Adminhtml_Ticket_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
	public function __construct(){
		parent::__construct();
		
		$this->_objectId = 'id';
		$this->_blockGroup = 'ticket';
		$this->_controller = 'adminhtml_ticket';
		
		$this->_updateButton('save', 'label', Mage::helper('ticket')->__('Save Item'));
		$this->_updateButton('delete', 'label', Mage::helper('ticket')->__('Delete Item'));
		
		$this->_addButton('saveandcontinue', array(
			'label'		=> Mage::helper('adminhtml')->__('Save And Continue Edit'),
			'onclick'	=> 'saveAndContinueEdit()',
			'class'		=> 'save',
		), -100);

		$this->_formScripts[] = "
			function toggleEditor() {
				if (tinyMCE.getInstanceById('ticket_content') == null)
					tinyMCE.execCommand('mceAddControl', false, 'ticket_content');
				else
					tinyMCE.execCommand('mceRemoveControl', false, 'ticket_content');
			}

			function saveAndContinueEdit(){
				editForm.submit($('edit_form').action+'back/edit/');
			}
		";
	}

	public function getHeaderText(){
		if(Mage::registry('ticket_data') && Mage::registry('ticket_data')->getId())
			return Mage::helper('ticket')->__("Edit Item '%s'", $this->htmlEscape(Mage::registry('ticket_data')->getTitle()));
		return Mage::helper('ticket')->__('Add Item');
	}
}