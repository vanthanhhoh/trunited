<?php

class Trunited_Ticket_Block_Adminhtml_Ticket extends Mage_Adminhtml_Block_Widget_Grid_Container
{
	public function __construct(){
		$this->_controller = 'adminhtml_ticket';
		$this->_blockGroup = 'ticket';
		$this->_headerText = Mage::helper('ticket')->__('Item Manager');
		$this->_addButtonLabel = Mage::helper('ticket')->__('Add Item');
		parent::__construct();
	}
}