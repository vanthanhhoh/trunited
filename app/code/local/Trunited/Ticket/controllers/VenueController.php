<?php

class Trunited_Ticket_VenueController extends Mage_Core_Controller_Front_Action
{
	public function indexAction(){

	    if(!Mage::helper('ticket')->isEnable())
        {
            Mage::getSingleton('core/session')->addNotice(
                Mage::helper('ticket')->__('The Event Tickets has been disabled!')
            );
            return $this->_redirectUrl(Mage::getBaseUrl());
        }

        $url_redirect = Mage::helper('ticket')->checkUrlRewrite(
            Trunited_Ticket_Model_Status::TYPE_VENUE,
            $this->getRequest()->getParam('id'),
            $this->getRequest()->getParam('slug')
        );

        if($url_redirect != '' && strpos(Mage::helper('core/url')->getCurrentUrl(),'.html') === false){
            $this->_redirectUrl($url_redirect);
            return;
        }

        $this->loadLayout();

        $this->_title(Mage::helper('ticket')->__('Event Tickets'));

        $breadcrumbs = $this->getLayout()->getBlock("breadcrumbs");
        $breadcrumbs->addCrumb("home", array(
            "label" => $this->__("Home"),
            "title" => $this->__("Home"),
            "link"  => Mage::getBaseUrl()
        ));

        $breadcrumbs->addCrumb("ticket", array(
            "label" => $this->__("Event Tickets"),
            "title" => $this->__("Event Tickets"),
            "link"  => Mage::getUrl('tickets.html')
        ));

        $breadcrumbs->addCrumb("venue", array(
            "label" => $this->__("Venue"),
            "title" => $this->__("Venue"),
        ));

		$this->renderLayout();
	}

    public function getHelper()
    {
        return Mage::helper('ticket');
    }
}
