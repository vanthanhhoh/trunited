<?php

class Trunited_Ticket_IndexController extends Mage_Core_Controller_Front_Action
{
	public function indexAction(){

	    if(!Mage::helper('ticket')->isEnable())
        {
            Mage::getSingleton('core/session')->addNotice(
                Mage::helper('ticket')->__('The Event Tickets has been disabled!')
            );
            return $this->_redirectUrl(Mage::getBaseUrl());
        }

        if(strpos(Mage::helper('core/url')->getCurrentUrl(),'tickets.html') === false){
            $this->_redirectUrl(Mage::getUrl('tickets.html'));
            return;
        }

        $this->loadLayout();

        $this->_title(Mage::helper('ticket')->__('Event Tickets'));

        $breadcrumbs = $this->getLayout()->getBlock("breadcrumbs");
        $breadcrumbs->addCrumb("home", array(
            "label" => $this->__("Home"),
            "title" => $this->__("Home"),
            "link"  => Mage::getBaseUrl()
        ));

        $breadcrumbs->addCrumb("ticket", array(
            "label" => $this->__("Event Tickets"),
            "title" => $this->__("Event Tickets"),
        ));

		$this->renderLayout();
	}

    public function getHelper()
    {
        return Mage::helper('ticket');
    }

	public function searchAutoAction()
    {
        $start = time();
        $suggestions = Mage::helper('ticket')->suggestion($this->getRequest()->getParam('term'));
//        $suggestions = Mage::helper('ticket')->suggestion($this->getRequest()->getParam('phrase'));

        $_data = array();

        if(isset($suggestions['events']) && sizeof($suggestions['events']) > 0){
            foreach ($suggestions['events'] as $evt) {

                $_data[] = array(
                    'id'    => Mage::getUrl('*/event/', array('id'  =>  $evt['id'], 'slug'  => $evt['slug'])),
                    'label' => $evt['name'],
                    'value' => $evt['name']
                );

                $data[] = array(
                    'name'  => $evt['name']
                );
            }
        }

        if(isset($suggestions['performers']) && sizeof($suggestions['performers']) > 0){
            foreach ($suggestions['performers'] as $pef) {

                $_data[] = array(
                    'id'    => Mage::getUrl('*/event/', array('id'  =>  $pef['id'], 'slug'  => $pef['slug'])),
                    'label' => $pef['name'],
                    'value' => $pef['name']
                );

                $data[] = array(
                    'name'  => $pef['name']
                );
            }
        }

        if(isset($suggestions['venues']) && sizeof($suggestions['venues']) > 0){
            foreach ($suggestions['venues'] as $vn) {

                $_data[] = array(
                    'id'    => Mage::getUrl('*/event/', array('id'  =>  $vn['id'], 'slug'  => $vn['slug'])),
                    'label' => $vn['name'],
                    'value' => $vn['name']
                );

                $data[] = array(
                    'name'  => $vn['name']
                );
            }
        }

        echo json_encode($_data);
//        echo json_encode($data);
        $end = time();
        Mage::log('FORMAT '.$this->getRequest()->getParam('term').' SUGGESTION: ' .($end - $start), null, 'tickets.log');

    }
}
