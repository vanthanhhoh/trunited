<?php
use TicketEvolution\Client as TEvoClient;

class Trunited_Ticket_DetailController extends Mage_Core_Controller_Front_Action
{
	public function indexAction(){

        $data = array(
            'aladdin_the_musical_tickets',
            'an_american_in_paris_tickets',
            'beautiful_the_carole_king_musical_tickets',
            'cats_tickets',
            'chicago_the_musical_tickets',

            'aladdin_the_musical_tickets',
            'lion_king_tickets',
            'jersey_boys_tickets',
            'finding_neverland_tickets',
            'frozen_tickets',

            'hamilton',
            'hello_dolly_tickets',
            'meteor_shower_tickets',
            'miss_saigon_tickets',
            'the_book_of_mormon_tickets',

            'dear_evan_hansen_tickets',
            'the_king_and_i_tickets',
            'the_phantom_of_the_opera_tickets',
            'wicked_tickets',
        );

        zend_debug::dump(sizeof($data));
        foreach ($data as $name) {
            $_q = str_replace('_tickets', '', $name);
            $q = str_replace('_', ' ', $_q);

            $results = $this->getClient()->searchPerformers([
                'fuzzy' => false,
                'q' => $q
            ]);

            foreach ($results['performers'] as $pef) {
                if(strcasecmp(trim(strtolower($pef['name'])), trim(strtolower($q))) == 0){
                    zend_debug::dump('<?php echo $this->getUrl("*/performer/", array("id"=> "'.$pef['id'].'", "slug"   =>"'.$pef['slug'].'"));?>');
                    break;
                }
            }
        }

        exit;
	}

    public function getHelper()
    {
        return Mage::helper('ticket');
    }

    public function getClient()
    {
        if ($this->getHelper()->getConfigData('ticket_setting', 'is_sandbox')) {
            $client = new TEvoClient([
                'baseUrl' => $this->getHelper()->getConfigData('ticket_setting', 'url_sandbox'),
                'apiVersion' => $this->getHelper()->getConfigData('ticket_setting', 'version'),
                'apiToken' => $this->getHelper()->getConfigData('ticket_setting', 'token'),
                'apiSecret' => $this->getHelper()->getConfigData('ticket_setting', 'secret'),
            ]);
        } else {
            $client = new TEvoClient([
                'baseUrl' => $this->getHelper()->getConfigData('ticket_setting', 'url_production'),
                'apiVersion' => $this->getHelper()->getConfigData('ticket_setting', 'version'),
                'apiToken' => $this->getHelper()->getConfigData('ticket_setting', 'token'),
                'apiSecret' => $this->getHelper()->getConfigData('ticket_setting', 'secret'),
            ]);
        }

        return $client;
    }

    public function testAction()
    {
        $suggestions = Mage::helper('ticket')->suggestion($this->getRequest()->getParam('term'));
        zend_debug::dump($suggestions);
    }
}
