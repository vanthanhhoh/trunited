<?php

use TicketEvolution\Client as TEvoClient;

class Trunited_Ticket_Helper_Data extends Mage_Core_Helper_Abstract
{

    protected $_client;

    public function getClient()
    {
        if ($this->_client == null) {
            if ($this->getConfigData('ticket_setting', 'is_sandbox')) {
                $this->_client = new TEvoClient([
                    'baseUrl' => $this->getConfigData('ticket_setting', 'url_sandbox'),
                    'apiVersion' => $this->getConfigData('ticket_setting', 'version'),
                    'apiToken' => $this->getConfigData('ticket_setting', 'token'),
                    'apiSecret' => $this->getConfigData('ticket_setting', 'secret'),
                ]);
            } else {
                $this->_client = new TEvoClient([
                    'baseUrl' => $this->getConfigData('ticket_setting', 'url_production'),
                    'apiVersion' => $this->getConfigData('ticket_setting', 'version'),
                    'apiToken' => $this->getConfigData('ticket_setting', 'token'),
                    'apiSecret' => $this->getConfigData('ticket_setting', 'secret'),
                ]);
            }
        }

        return $this->_client;
    }

    public function suggestion($keyword)
    {
        $client = $this->getClient();

        $start = time();
        $response = $client->listSearchSuggestions([
            'entities' => 'events,performers,venues',
            'fuzzy' => false,
            'limit' => 1,
            'q' => $keyword,
        ]);
        $end = time();
        Mage::log('KEYWORD '.$keyword.' SUGGESTION: ' .($end - $start), null, 'tickets.log');

        return $response['suggestions'];
    }

    public function getConfigData($group, $field, $store = null)
    {
        return Mage::getStoreConfig('ticket/' . $group . '/' . $field, $store);
    }

    public function isEnable()
    {
        return $this->getConfigData('general', 'enable');
    }

    function get_client_ip()
    {
        if(isset($_SERVER['HTTP_CF_CONNECTING_IP'])) {
            $ipaddress =  $_SERVER['HTTP_CF_CONNECTING_IP'];
        } else if (isset($_SERVER['HTTP_X_REAL_IP'])) {
            $ipaddress = $_SERVER['HTTP_X_REAL_IP'];
        }
        else if (isset($_SERVER['HTTP_CLIENT_IP']))
            $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
        else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
            $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
        else if(isset($_SERVER['HTTP_X_FORWARDED']))
            $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
        else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
            $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
        else if(isset($_SERVER['HTTP_FORWARDED']))
            $ipaddress = $_SERVER['HTTP_FORWARDED'];
        else if(isset($_SERVER['REMOTE_ADDR']))
            $ipaddress = $_SERVER['REMOTE_ADDR'];
        else
            $ipaddress = 'UNKNOWN';

        return $ipaddress;
    }

    public function getVisitorLocation()
    {
        if (!$this->getConfigData('geoip_setting', 'enable')) {
            return false;
        }

        $client_ip = getenv('REMOTE_ADDR') != '::1' ? getenv('REMOTE_ADDR') : '';
        $url = $this->getConfigData('geoip_setting', 'geopip_url') . $client_ip;
        $geo = @unserialize(file_get_contents($url));

        return $geo;
    }

    public function checkUrlRewrite($type, $id, $slug = '')
    {
        $_slug = str_replace(' ', '-', strtolower($slug));
        $id_path = 'tickets_' . $id . '_' . $_slug;

        $target_path = '';

        if($type == Trunited_Ticket_Model_Status::TYPE_PERFORMER)
        {
            $target_path .= 'ticket/performer/index/id/'.$id.'/slug/'.$_slug;
        } else if ($type == Trunited_Ticket_Model_Status::TYPE_VENUE){
            $target_path .= 'ticket/venue/index/id/'.$id.'/slug/'.$_slug;
        } else if($type == Trunited_Ticket_Model_Status::TYPE_EVENT){
            $target_path .= 'ticket/event/index/id/'.$id;
        }

        $request_path = 'tickets/'.$_slug.'.html';
        $rewrite = Mage::getResourceModel('core/url_rewrite_collection')
            ->addFieldToFilter('request_path', $request_path)
            ->getFirstItem();

        $redirect_url = '';
        if ($rewrite != null && $rewrite->getId()) {
            $redirect_url .= Mage::getUrl($rewrite->getRequestPath());
        } else {
           
            $_rewrite = Mage::getModel('core/url_rewrite')
                ->setStoreId(1)
                ->setIdPath($id_path)
                ->setRequestPath($request_path)
                ->setTargetPath($target_path)
                ->setIsSystem(0)
                ->save();

            $redirect_url .= Mage::getUrl($_rewrite->getRequestPath());

        }
        return $redirect_url;
    }
}
