<?php

class Trunited_Ticket_Model_Status extends Varien_Object
{
	const STATUS_ENABLED	= 1;
	const STATUS_DISABLED	= 2;

	const TYPE_PERFORMER = 1;
	const TYPE_VENUE = 2;
	const TYPE_EVENT = 3;

	static public function getOptionArray(){
		return array(
			self::STATUS_ENABLED	=> Mage::helper('ticket')->__('Enabled'),
			self::STATUS_DISABLED   => Mage::helper('ticket')->__('Disabled')
		);
	}
	
	static public function getOptionHash(){
		$options = array();
		foreach (self::getOptionArray() as $value => $label)
			$options[] = array(
				'value'	=> $value,
				'label'	=> $label
			);
		return $options;
	}
}