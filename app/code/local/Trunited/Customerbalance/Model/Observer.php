<?php

class Trunited_Customerbalance_Model_Observer
{
    public function guestPointsCreated($observer)
    {
        $customer = $observer->getCustomer();

        if($customer != null && $customer->getId()){
            Mage::helper('customerbalance')->createNewObj($customer->getId());
        }

        return $this;
    }

    public function orderPlaceAfter($observer)
    {
        if(!Mage::helper('customerbalance')->getConfigData('guest_points', 'enable')){
            return $this;
        }

        $order = $observer['order'];

        if($order->getData('customer_is_guest')){
            $affiliate_account = Mage::helper('affiliateplus/account')->getAffiliateInfoFromCookie();
            if($affiliate_account != null && $affiliate_account->getId())
            {
                Mage::helper('customerbalance/transaction')->saveTemporary($order, $affiliate_account->getCustomerId());
            }
        }
    }

    public function orderSaveAfter($observer)
    {
    	if(!Mage::helper('customerbalance')->getConfigData('guest_points', 'enable')){
    		return $this;
    	}

        $order = $observer['order'];

        if (in_array($order->getStatus(), array(
            Mage_Sales_Model_Order::STATE_COMPLETE,
            Mage_Sales_Model_Order::STATE_PROCESSING,
        )))
        {
            Mage::helper('customerbalance/transaction')->createTransactionFromGuest($order);
        }
    }

    public function orderCancelAfter($observer)
    {
        $order = $observer->getOrder();
        $order_id = $order->getIncrementId();
        
        $transactions = Mage::getModel('customerbalance/transactionguest')->getCollection()
            ->addFieldToFilter('order_id', $order_id)
            ->addFieldToFilter('status', Trunited_Customerbalance_Model_Status::STATUS_TRANSACTION_COMPLETED)
            ->addFieldToFilter('action_type', array('in' => array(
                Trunited_Customerbalance_Model_Type::TYPE_TRANSACTION_RECEIVE_FROM_GUEST_CHECKOUT,
            )))
            ->setOrder('transaction_id', 'desc')
        ;

        if(sizeof($transactions) > 0)
        {
            $connection = Mage::getSingleton('core/resource')->getConnection('core_write');
            try{
                $connection->beginTransaction();

                foreach ($transactions as $transaction) {
                    $amount_credit = $transaction->getData('changed_credit');
                    $transaction->setData('status', Trunited_Customerbalance_Model_Status::STATUS_TRANSACTION_CANCELED);
                    $transaction->setData('updated_at', now());
                    $transaction->save();

                    $customer_id = $transaction->getCustomerId();

                    $customerbalance_account = Mage::helper('customerbalance/account')->updatePoints(
                        $customer_id, 
                        -$amount_credit
                    );

                    if($customerbalance_account != null && $customerbalance_account->getId())
                    {
                        $params = array(
                            'title' => Mage::helper('customerbalance')->__('Canceled order #<a href="%s">%s</a>',
                                Mage::getUrl('sales/order/view', array('order_id' => $order->getId())),
                                $order->getIncrementId()
                            ),
                            'award_from' => Trunited_Customerbalance_Model_Status::AWARD_FROM_OTHER,
                            'current_credit' => $customerbalance_account->getGuestPoints(),
                            'is_on_hold' => 0,
                            'hold_credit' => 0,
                            'changed_credit' =>  -$amount_credit,
                            'expiration_at' => '',
                            'created_at' => now(),
                            'order_id' => $order->getIncrementId()
                        );

                        Mage::helper('customerbalance/transaction')->createTransaction(
                            $customerbalance_account,
                            $params,
                            Trunited_Customerbalance_Model_Type::TYPE_TRANSACTION_CANCEL_ORDER,
                            Trunited_Customerbalance_Model_Status::STATUS_TRANSACTION_COMPLETED
                        );
                    }
                }

                $connection->commit();
            } catch (Exception $e) {
                $connection->rollback();
            }

        }
    }

    public function creditmemoSaveAfter(Varien_Event_Observer $observer)
    {
        $creditmemo = $observer->getEvent()->getCreditmemo();
        $order_id = $creditmemo->getOrderId();

        $order = Mage::getSingleton('sales/order')->load($order_id);
        
        $transactions = Mage::getModel('customerbalance/transactionguest')->getCollection()
            ->addFieldToFilter('order_id', $order->getIncrementId())
            ->addFieldToFilter('status', Trunited_Customerbalance_Model_Status::STATUS_TRANSACTION_COMPLETED)
            ->addFieldToFilter('action_type', array('in' => array(
                Trunited_Customerbalance_Model_Type::TYPE_TRANSACTION_RECEIVE_FROM_GUEST_CHECKOUT,
            )))
            ->setOrder('transaction_id', 'desc')
        ;

        if(sizeof($transactions) > 0)
        {
            $connection = Mage::getSingleton('core/resource')->getConnection('core_write');
            try{
                $connection->beginTransaction();

                foreach ($transactions as $transaction) {
                    $amount_credit = $transaction->getData('changed_credit');
                    $transaction->setData('status', Trunited_Customerbalance_Model_Status::STATUS_TRANSACTION_CANCELED);
                    $transaction->setData('updated_at', now());
                    $transaction->save();

                    $customer_id = $transaction->getCustomerId();

                    $customerbalance_account = Mage::helper('customerbalance/account')->updatePoints(
                        $customer_id, 
                        -$amount_credit
                    );

                    if($customerbalance_account != null && $customerbalance_account->getId())
                    {
                        $params = array(
                            'title' => Mage::helper('manageapi')->__('Refunded order #<a href="%s">%s</a>',
                                Mage::getUrl('sales/order/view', array('order_id' => $order->getId())),
                                $order->getIncrementId()
                            ),
                            'award_from' => Trunited_Customerbalance_Model_Status::AWARD_FROM_OTHER,
                            'current_credit' => $customerbalance_account->getGuestPoints(),
                            'is_on_hold' => 0,
                            'hold_credit' => 0,
                            'changed_credit' =>  -$amount_credit,
                            'expiration_at' => '',
                            'created_at' => now(),
                            'order_id' => $order->getIncrementId()
                        );

                        Mage::helper('customerbalance/transaction')->createTransaction(
                            $customerbalance_account,
                            $params,
                            Trunited_Customerbalance_Model_Type::TYPE_TRANSACTION_REFUNDED_ORDER,
                            Trunited_Customerbalance_Model_Status::STATUS_TRANSACTION_COMPLETED
                        );
                    }
                }

                $connection->commit();
            } catch (Exception $e) {
                $connection->rollback();
            }

        }
    }
}
