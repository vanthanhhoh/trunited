<?php

class Trunited_Customerbalance_Model_Customerguestpending extends Mage_Core_Model_Abstract
{
	public function _construct(){
		parent::_construct();
		$this->_init('customerbalance/customerguestpending');
	}

    /**
     * Before object save manipulations
     *
     * @return Trunited_Customerbalance_Model_Customerguestpending
     */
    protected function _beforeSave()
    {
        parent::_beforeSave();

        if($this->isObjectNew())
            $this->setData('created_at', now());

        $this->setData('updated_at', now());

        return $this;
    }
}
