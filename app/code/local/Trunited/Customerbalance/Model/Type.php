<?php

class Trunited_Customerbalance_Model_Type extends Varien_Object
{
	const TYPE_TRANSACTION_BY_ADMIN	= 1;
	const TYPE_TRANSACTION_RECEIVE_FROM_GLOBAL_BRAND = 2;
	const TYPE_TRANSACTION_RECEIVE_FROM_GUEST_CHECKOUT = 3;
	const TYPE_TRANSACTION_CANCEL_ORDER = 4;
	const TYPE_TRANSACTION_REFUNDED_ORDER = 5;

	static public function getOptionArray(){
		return array(
			self::TYPE_TRANSACTION_BY_ADMIN	=> Mage::helper('customerbalance')->__('Changed by Admin'),
			self::TYPE_TRANSACTION_RECEIVE_FROM_GLOBAL_BRAND   => Mage::helper('customerbalance')->__('Received Guest Points from Global Brand'),
			self::TYPE_TRANSACTION_RECEIVE_FROM_GUEST_CHECKOUT   => Mage::helper('customerbalance')->__('Received Guest Points from Guest checkout'),
			self::TYPE_TRANSACTION_CANCEL_ORDER   => Mage::helper('customerbalance')->__('Reduce the Guest Points when order has been canceled'),
			self::TYPE_TRANSACTION_REFUNDED_ORDER   => Mage::helper('customerbalance')->__('Reduce the Guest Points when order has been refunded'),
		);
	}
	
	static public function getOptionHash(){
		$options = array();
		foreach (self::getOptionArray() as $value => $label)
			$options[] = array(
				'value'	=> $value,
				'label'	=> $label
			);
		return $options;
	}

	static public function getDataType()
	{
		return array(
			self::TYPE_TRANSACTION_BY_ADMIN,
			self::TYPE_TRANSACTION_RECEIVE_FROM_GLOBAL_BRAND,
			self::TYPE_TRANSACTION_RECEIVE_FROM_GUEST_CHECKOUT,
		);
	}

}
