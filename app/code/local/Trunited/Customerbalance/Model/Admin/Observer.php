<?php

class Trunited_Customerbalance_Model_Admin_Observer
{

    /**
     * @param $observer
     * @return $this
     * @throws Exception
     */
    public function customerSaveAfter($observer)
    {
        $customer = $observer['customer'];
        $params = Mage::app()->getRequest()->getParam('Guestpointspending');
        $params_points = Mage::app()->getRequest()->getParam('Guestpoints');
        $helper = Mage::helper('customerbalance');

        if($params['guest_points_pending'] > 0){

            if(filter_var($params['guest_points_pending'], FILTER_VALIDATE_FLOAT) === false)
                throw new Exception(
                    Mage::helper('customerbalance')->__('Data invalid')
                );

            $params['is_on_hold'] = empty($params['is_on_hold']) ? 0 : 1;

            $connection = Mage::getSingleton('core/resource')->getConnection('core_write');

            try {
                $connection->beginTransaction();

                if($params['is_on_hold'] == 1){
                    $customerbalanceAccount = Mage::helper('customerbalance/account')->updatePointsPending(
                        $customer->getId(),
                        0
                    );

                    if($params['expiration_at'] != null)
                        $params['expiration_at'] = $helper->addDaysToDate(
                            now(),
                            $params['expiration_at']
                        );

                    $params['hold_credit'] = $params['guest_points_pending'];
                } else {
                    $customerbalanceAccount = Mage::helper('customerbalance/account')->updatePointsPending(
                        $customer->getId(),
                        $params['guest_points_pending']
                    );
                }

                if($customerbalanceAccount != null)
                {
                    if($params['is_on_hold'] == 1){
                        $status = Trunited_Customerbalance_Model_Status::STATUS_TRANSACTION_ON_HOLD;
                    } else {
                        $params['changed_credit'] = $params['guest_points_pending'];
                        $status = Trunited_Customerbalance_Model_Status::STATUS_TRANSACTION_COMPLETED;
                    }
                    Mage::helper('customerbalance/transaction')->createTransaction(
                        $customerbalanceAccount,
                        $params,
                        Trunited_Customerbalance_Model_Type::TYPE_TRANSACTION_BY_ADMIN,
                        $status,
                        true
                    );
                }

                $connection->commit();

            } catch (Exception $e) {
                $connection->rollback();
                Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('customerbalance')->__($e->getMessage())
                );
            }
        }

        if($params_points['guest_points'] > 0){

            if(filter_var($params_points['guest_points'], FILTER_VALIDATE_FLOAT) === false)
                throw new Exception(
                    Mage::helper('customerbalance')->__('Data invalid')
                );

            $connection = Mage::getSingleton('core/resource')->getConnection('core_write');

            try {
                $connection->beginTransaction();

                $customerbalanceAccount = Mage::helper('customerbalance/account')->updatePoints(
                    $customer->getId(),
                    $params_points['guest_points']
                );

                if($customerbalanceAccount != null)
                {
                    $params_points['changed_credit'] = $params_points['guest_points'];
                    $status = Trunited_Customerbalance_Model_Status::STATUS_TRANSACTION_COMPLETED;

                    Mage::helper('customerbalance/transaction')->createTransaction(
                        $customerbalanceAccount,
                        $params_points,
                        Trunited_Customerbalance_Model_Type::TYPE_TRANSACTION_BY_ADMIN,
                        $status
                    );
                }

                $connection->commit();

            } catch (Exception $e) {
                $connection->rollback();
                Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('customerbalance')->__($e->getMessage())
                );
            }
        }
    }
}
