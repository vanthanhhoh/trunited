<?php

class Trunited_Customerbalance_Model_Status extends Varien_Object
{
	const STATUS_ENABLED	= 1;
	const STATUS_DISABLED	= 2;

    const STATUS_TRANSACTION_PENDING = 1;
    const STATUS_TRANSACTION_COMPLETED = 2;
    const STATUS_TRANSACTION_CANCELED = 3;
    const STATUS_TRANSACTION_EXPIRED = 4;
    const STATUS_TRANSACTION_ON_HOLD = 5;

    const AWARD_FROM_OTHER	= 0;
    const AWARD_FROM_FUNDRAISER	= 1;
    const AWARD_FROM_DAILY_DEALS = 2;
    const AWARD_FROM_GUEST_CHECKOUT = 3;

    const TEMPORARY_PENDING = 1;
    const TEMPORARY_COMPLETE = 2;
    const TEMPORARY_CANCELED = 3;

	static public function getOptionArray(){
		return array(
			self::STATUS_ENABLED	=> Mage::helper('customerbalance')->__('Enabled'),
			self::STATUS_DISABLED   => Mage::helper('customerbalance')->__('Disabled')
		);
	}
	
	static public function getOptionHash(){
		$options = array();
		foreach (self::getOptionArray() as $value => $label)
			$options[] = array(
				'value'	=> $value,
				'label'	=> $label
			);
		return $options;
	}

    static public function getTransactionOptionArray(){
        return array(
            self::STATUS_TRANSACTION_PENDING	=> Mage::helper('customerbalance')->__('Pending'),
            self::STATUS_TRANSACTION_COMPLETED   => Mage::helper('customerbalance')->__('Completed'),
            self::STATUS_TRANSACTION_CANCELED   => Mage::helper('customerbalance')->__('Canceled'),
            self::STATUS_TRANSACTION_EXPIRED   => Mage::helper('customerbalance')->__('Expired'),
            self::STATUS_TRANSACTION_ON_HOLD   => Mage::helper('customerbalance')->__('On Hold'),
        );
    }

    static public function getTransactionOptionHash(){
        $options = array();
        foreach (self::getTransactionOptionArray() as $value => $label)
            $options[] = array(
                'value'	=> $value,
                'label'	=> $label
            );
        return $options;
    }

    static public function getOptionAwardFromArray(){
        return array(
            self::AWARD_FROM_OTHER	=> Mage::helper('customerbalance')->__('Other'),
            self::AWARD_FROM_FUNDRAISER	=> Mage::helper('customerbalance')->__('Fundraiser'),
            self::AWARD_FROM_DAILY_DEALS   => Mage::helper('customerbalance')->__('Daily Deals'),
            self::AWARD_FROM_GUEST_CHECKOUT   => Mage::helper('customerbalance')->__('Guest Checkout')
        );
    }

    static public function getOptionAwardFromHas(){
        $options = array();
        foreach (self::getOptionArray() as $value => $label)
            $options[] = array(
                'value'	=> $value,
                'label'	=> $label
            );
        return $options;
    }

    static public function getOptionTemporaryArray(){
        return array(
            self::TEMPORARY_PENDING    => Mage::helper('customerbalance')->__('Pending'),
            self::TEMPORARY_COMPLETE   => Mage::helper('customerbalance')->__('Completed'),
            self::TEMPORARY_CANCELED   => Mage::helper('customerbalance')->__('Canceled')
        );
    }
    
    static public function getOptionTemporaryHash(){
        $options = array();
        foreach (self::getOptionTemporaryArray() as $value => $label)
            $options[] = array(
                'value' => $value,
                'label' => $label
            );
        return $options;
    }
}
