<?php

$installer = $this;
$installer->startSetup();

$installer->run("

DROP TABLE IF EXISTS {$this->getTable('customerbalance/customerguest')};
DROP TABLE IF EXISTS {$this->getTable('customerbalance/transactionguest')};	
DROP TABLE IF EXISTS {$this->getTable('customerbalance/temporaryguest')};

CREATE TABLE {$this->getTable('customerbalance/customerguest')} (
	`customerguest_id` int(11) unsigned NOT NULL auto_increment,
	`customer_id` int(10) unsigned NOT NULL,
	`guest_points` DECIMAL(10,2) unsigned NOT NULL default 0,
	`created_at` datetime NULL,
	`updated_at` datetime NULL,
	PRIMARY KEY (`customerguest_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE {$this->getTable('customerbalance/transactionguest')} (
	`transaction_id` int(10) unsigned NOT NULL auto_increment,
	`customerguest_id` int(10) unsigned NULL,
	`customer_id` int(10) unsigned NULL,
	`customer_email` varchar(255) NOT NULL,
	`title` varchar(255) NOT NULL,
	`description` varchar(255) NOT NULL,
	`action_type` smallint(5) NOT NULL,
	`status` smallint(5) NOT NULL,
	`current_credit` DECIMAL(10,2) unsigned NOT NULL default 0,
	`changed_credit` DECIMAL(10,2) NOT NULL default 0,
	`is_on_hold` TINYINT(4) NULL default 0,
	`hold_credit` DECIMAL(10,2) NULL default 0,
	`expiration_at` datetime NULL,	
	`created_at` datetime NULL,
	`updated_at` datetime NULL,
	`order_id` varchar(255) NULL,
	`receiver_email` varchar(255) NULL,
	`receiver_customer_id` INT unsigned NULL,
	`recipient_transaction_id` int(10) unsigned,
	`credit_back` FLOAT,
	`order_filter_ids` text,
	PRIMARY KEY (`transaction_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;	

CREATE TABLE {$this->getTable('customerbalance/temporaryguest')} (
	`temporary_id` int(10) unsigned NOT NULL auto_increment,
	`customer_id` int(10) unsigned NULL,
	`created_at` datetime NULL,
	`updated_at` datetime NULL,
	`order_id` int(10) unsigned NULL,
	`status` smallint(5) NOT NULL,
	PRIMARY KEY (`temporary_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE {$this->getTable('customerbalance/customerguest')} ADD UNIQUE `unique_customerbalance_index`(`customer_id`);


DROP TABLE IF EXISTS {$this->getTable('customerbalance/customerguestpending')};
DROP TABLE IF EXISTS {$this->getTable('customerbalance/transactionguestpending')};	
DROP TABLE IF EXISTS {$this->getTable('customerbalance/temporaryguestpending')};

CREATE TABLE {$this->getTable('customerbalance/customerguestpending')} (
	`customerguestpending_id` int(11) unsigned NOT NULL auto_increment,
	`customer_id` int(10) unsigned NOT NULL,
	`guest_points_pending` DECIMAL(10,2) unsigned NOT NULL default 0,
	`created_at` datetime NULL,
	`updated_at` datetime NULL,
	PRIMARY KEY (`customerguestpending_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE {$this->getTable('customerbalance/transactionguestpending')} (
	`transaction_id` int(10) unsigned NOT NULL auto_increment,
	`customerguestpending_id` int(10) unsigned NULL,
	`customer_id` int(10) unsigned NULL,
	`customer_email` varchar(255) NOT NULL,
	`title` varchar(255) NOT NULL,
	`description` varchar(255) NOT NULL,
	`action_type` smallint(5) NOT NULL,
	`status` smallint(5) NOT NULL,
	`current_credit` DECIMAL(10,2) unsigned NOT NULL default 0,
	`changed_credit` DECIMAL(10,2) NOT NULL default 0,
	`is_on_hold` TINYINT(4) NULL default 0,
	`hold_credit` DECIMAL(10,2) NULL default 0,
	`expiration_at` datetime NULL,	
	`created_at` datetime NULL,
	`updated_at` datetime NULL,
	`order_id` varchar(255) NULL,
	`receiver_email` varchar(255) NULL,
	`receiver_customer_id` INT unsigned NULL,
	`recipient_transaction_id` int(10) unsigned,
	`credit_back` FLOAT,
	`order_filter_ids` text,
	PRIMARY KEY (`transaction_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;	

CREATE TABLE {$this->getTable('customerbalance/temporaryguestpending')} (
	`temporary_id` int(10) unsigned NOT NULL auto_increment,
	`customer_id` int(10) unsigned NULL,
	`created_at` datetime NULL,
	`updated_at` datetime NULL,
	`order_id` int(10) unsigned NULL,
	`status` smallint(5) NOT NULL,
	PRIMARY KEY (`temporary_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE {$this->getTable('customerbalance/customerguestpending')} ADD UNIQUE `unique_customerguestpending_index`(`customer_id`);

    ");

$installer->endSetup(); 
