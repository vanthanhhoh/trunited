<?php
/**
 * Magestore
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magestore.com license that is
 * available through the world-wide-web at this URL:
 * http://www.magestore.com/license-agreement.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Magestore
 * @package     Magestore_customerbalance
 * @copyright   Copyright (c) 2012 Magestore (http://www.magestore.com/)
 * @license     http://www.magestore.com/license-agreement.html
 */

/**
 * customerbalance Tab on Customer Edit Form Block
 *
 * @category    Magestore
 * @package     Magestore_customerbalance
 * @author      Magestore Developer
 */
class Trunited_Customerbalance_Block_Adminhtml_Customer_Edit_Tab_Guestpointspending_History
    extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('guestpointspendingTransactionGrid');
        $this->setDefaultSort('transaction_id');
        $this->setDefaultDir('DESC');
        $this->setUseAjax(true);
    }

    /**
     * prepare collection for block to display
     *
     * @return Trunited_Customerbalance_Block_Adminhtml_Customer_Edit_Tab_Guestpointspending_History
     */
    protected function _prepareCollection()
    {
        $collection = Mage::getModel('customerbalance/transactionguestpending')->getCollection()
            ->addFieldToFilter('customer_id', $this->getRequest()->getParam('id'))
            ->setOrder('transaction_id', 'desc');
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    /**
     * prepare columns for this grid
     *
     * @return Trunited_Customerbalance_Block_Adminhtml_Customer_Edit_Tab_Guestpointspending_History
     */
    protected function _prepareColumns()
    {
        $this->addColumn('transaction_id', array(
            'header' => Mage::helper('customerbalance')->__('ID'),
            'align' => 'right',
            'width' => '50px',
            'index' => 'transaction_id',
        ));

        $this->addColumn('title', array(
            'header' => Mage::helper('customerbalance')->__('Title'),
            'align' => 'left',
            'index' => 'title',
            'renderer'  => 'Trunited_Customerbalance_Block_Adminhtml_Renderer_Transaction_Title',
        ));

        $this->addColumn('action_type', array(
            'header' => Mage::helper('customerbalance')->__('Action'),
            'align' => 'left',
            'index' => 'action_type',
            'type' => 'options',
            'options' => Mage::getSingleton('customerbalance/type')->getOptionArray(),
        ));

        $this->addColumn('current_credit', array(
            'header' => Mage::helper('customerbalance')->__('Current Credit'),
            'align' => 'right',
            'index' => 'current_credit',
            'type' => 'number',
        ));

        $this->addColumn('changed_credit', array(
            'header' => Mage::helper('customerbalance')->__('Updated Credit'),
            'align' => 'right',
            'index' => 'changed_credit',
            'type' => 'number',
        ));

        $this->addColumn('created_at', array(
            'header' => Mage::helper('customerbalance')->__('Created On'),
            'index' => 'created_at',
            'type' => 'datetime',
        ));

        $this->addColumn('updated_at', array(
            'header' => Mage::helper('customerbalance')->__('Updated On'),
            'index' => 'updated_at',
            'type' => 'datetime',
        ));

        $this->addColumn('expiration_at', array(
            'header' => Mage::helper('customerbalance')->__('Expires On'),
            'index' => 'expiration_at',
            'type' => 'datetime',
        ));

        $this->addColumn('status', array(
            'header' => Mage::helper('customerbalance')->__('Status'),
            'align' => 'left',
            'index' => 'status',
            'type' => 'options',
            'options' => Mage::getSingleton('customerbalance/status')->getTransactionOptionArray(),
        ));

        $this->addColumn('is_on_hold', array(
            'header' => Mage::helper('customerbalance')->__('Is On Hold'),
            'align' => 'left',
            'index' => 'is_on_hold',
            'type' => 'options',
            'options' => array(
                '1' => 'Yes',
                '0' =>  'No',
            ),
        ));

        $this->addColumn('hold_credit', array(
            'header' => Mage::helper('customerbalance')->__('Hold Credit'),
            'align' => 'left',
            'index' => 'hold_credit',
        ));

        return parent::_prepareColumns();
    }

    /**
     * get url for each row in grid
     *
     * @return string
     */
    public function getRowUrl($row)
    {
//        return $this;
    }

    /**
     * get grid url (use for ajax load)
     *
     * @return string
     */
    public function getGridUrl()
    {
        return Mage::helper('adminhtml')->getUrl('customerbalanceadmin/adminhtml_transactionguestpending/grid', array('_current' => true));
    }
}
