<?php
/**
 * Magestore
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magestore.com license that is
 * available through the world-wide-web at this URL:
 * http://www.magestore.com/license-agreement.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Magestore
 * @package     Magestore_customerbalance
 * @copyright   Copyright (c) 2012 Magestore (http://www.magestore.com/)
 * @license     http://www.magestore.com/license-agreement.html
 */

/**
 * customerbalance Tab on Customer Edit Form Block
 *
 * @category    Magestore
 * @package     Magestore_customerbalance
 * @author      Magestore Developer
 */
class Trunited_Customerbalance_Block_Adminhtml_Customer_Edit_Tab_Guestpointspending
    extends Mage_Adminhtml_Block_Widget_Form
    implements Mage_Adminhtml_Block_Widget_Tab_Interface
{
    protected $_guestpointsAccount = null;

    /**
     * @return Mage_Core_Model_Abstract|null
     * @throws Exception
     */
    public function getGuestPointsAccount()
    {
        if (is_null($this->_guestpointsAccount)) {
            $customerId = $this->getRequest()->getParam('id');
            $this->_guestpointsAccount = Mage::getModel('customerbalance/customerguestpending')
                ->load($customerId, 'customer_id');
        }
        return $this->_guestpointsAccount;
    }

    public function getGuestPointsPending()
    {
        return $this->getGuestPointsAccount()->getGuestPointsPending();
    }

    public function getOnHoldGuestPointsPending()
    {
        $customerguestpending_id = $this->getGuestPointsAccount()->getId();

        $collection = Mage::getModel('customerbalance/transactionguestpending')->getCollection()
            ->addFieldToSelect('transaction_id')
            ->addFieldToSelect('hold_credit')
            ->addFieldToFilter('customerguestpending_id', $customerguestpending_id)
            ->addFieldToFilter('customer_id', $this->getRequest()->getParam('id'))
            ->addFieldToFilter('status', Trunited_Customerbalance_Model_Status::STATUS_TRANSACTION_ON_HOLD)
            ->addFieldToFilter('action_type', array(
                'in'    => Trunited_Customerbalance_Model_Type::getDataType()
            ))
        ;

        $total = 0;

        if(sizeof($collection) > 0)
        {
            foreach ($collection as $item) {
                $total += $item->getData('hold_credit');
            }
        }

        return number_format($total, 2);
    }

    /**
     * @return Mage_Adminhtml_Block_Widget_Form
     */
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form();
        $form->setHtmlIdPrefix('customerguestpointspending_');
        $this->setForm($form);

        $fieldset = $form->addFieldset('customerbalance_form', array(
            'legend' => Mage::helper('customerbalance')->__('Guest Points Pending Information')
        ));

        $fieldset->addField('customerbalance_balance', 'note', array(
            'label' => Mage::helper('customerbalance')->__('Guest Points Pending'),
            'title' => Mage::helper('customerbalance')->__('Guest Points Pending'),
            'text' => '<strong>' . Mage::helper('core')->currency($this->getGuestPointsPending(),true,false) . '</strong>',
        ));

        $fieldset->addField('holding_point', 'note', array(
            'label' => Mage::helper('rewardpoints')->__('On Hold Guest Points Pending Balance'),
            'title' => Mage::helper('rewardpoints')->__('On Hold Guest Points Pending Balance'),
            'text' => '<strong>' . Mage::helper('core')->currency($this->getOnHoldGuestPointsPending(),true,false) . '</strong>',
        ));

        $fieldset->addField('customerbalance_point', 'text', array(
            'label' => Mage::helper('customerbalance')->__('Change Guest Points Pending'),
            'title' => Mage::helper('customerbalance')->__('Change Guest Points Pending'),
            'name' => 'Guestpointspending[guest_points_pending]',
            'class' => 'validate-number',
            'note' => Mage::helper('customerbalance')->__('Add or subtract customer\'s guest points pending. For ex: 99 or -99 affiliate points.'),
        ));

        $fieldset->addField('title_point', 'textarea', array(
            'label' => Mage::helper('customerbalance')->__('Change Transaction Title'),
            'title' => Mage::helper('customerbalance')->__('Change Transaction Title'),
            'name' => 'Guestpointspending[title]',
            'style' => 'height: 5em;'
        ));

        $fieldset->addField('description_point', 'textarea', array(
            'label' => Mage::helper('customerbalance')->__('Change Transaction Description'),
            'title' => Mage::helper('customerbalance')->__('Change Transaction Description'),
            'name' => 'Guestpointspending[description]',
            'style' => 'height: 5em;'
        ));

        $fieldset->addField('expiration_at', 'text', array(
            'label' => Mage::helper('customerbalance')->__('Guest Points Pending Expire On'),
            'title' => Mage::helper('customerbalance')->__('Guest Points Pending Expire On'),
            'name'  => 'Guestpointspending[expiration_at]',
            'note'  => Mage::helper('customerbalance')->__('day(s) since the transaction date. If empty or zero, there is no limitation.')
        ));

        $fieldset->addField('is_on_hold', 'checkbox', array(
            'label' => Mage::helper('customerbalance')->__('Guest Points Pending On Hold'),
            'title' => Mage::helper('customerbalance')->__('Guest Points Pending On Hold'),
            'name'  => 'Guestpointspending[is_on_hold]',
            'value' => 1,
        ));

        $fieldset = $form->addFieldset('customerbalance_history_fieldset', array(
            'legend' => Mage::helper('customerbalance')->__('Transaction History')
        ))->setRenderer($this->getLayout()->createBlock('adminhtml/widget_form_renderer_fieldset')->setTemplate(
            'customerbalance/guestpointspending/history.phtml'
        ));

        return parent::_prepareForm();
    }

    public function getTabLabel()
    {
        return Mage::helper('customerbalance')->__('Guest Points Pending');
    }

    public function getTabTitle()
    {
        return Mage::helper('customerbalance')->__('Guest Points Pending');
    }

    public function canShowTab()
    {
        return true;
    }

    public function isHidden()
    {
        return false;
    }
}
