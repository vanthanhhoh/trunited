<?php
/**
 * Magestore
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magestore.com license that is
 * available through the world-wide-web at this URL:
 * http://www.magestore.com/license-agreement.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Magestore
 * @package     Magestore_customerbalance
 * @copyright   Copyright (c) 2012 Magestore (http://www.magestore.com/)
 * @license     http://www.magestore.com/license-agreement.html
 */

/**
 * customerbalance Tab on Customer Edit Form Block
 *
 * @category    Magestore
 * @package     Magestore_customerbalance
 * @author      Magestore Developer
 */
class Trunited_Customerbalance_Block_Adminhtml_Customer_Edit_Tab_Guestpoints
    extends Mage_Adminhtml_Block_Widget_Form
    implements Mage_Adminhtml_Block_Widget_Tab_Interface
{
    protected $_guestpointsAccount = null;

    /**
     * @return Mage_Core_Model_Abstract|null
     * @throws Exception
     */
    public function getGuestPointsAccount()
    {
        if (is_null($this->_guestpointsAccount)) {
            $customerId = $this->getRequest()->getParam('id');
            $this->_guestpointsAccount = Mage::getModel('customerbalance/customerguest')
                ->load($customerId, 'customer_id');
        }
        return $this->_guestpointsAccount;
    }

    public function getGuestPoints()
    {
        return $this->getGuestPointsAccount()->getGuestPoints();
    }

    /**
     * @return Mage_Adminhtml_Block_Widget_Form
     */
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form();
        $form->setHtmlIdPrefix('customerbalance_');
        $this->setForm($form);

        $fieldset = $form->addFieldset('customerbalance_form', array(
            'legend' => Mage::helper('customerbalance')->__('Guest Points Information')
        ));

        $fieldset->addField('customerbalance_balance', 'note', array(
            'label' => Mage::helper('customerbalance')->__('Guest Points'),
            'title' => Mage::helper('customerbalance')->__('Guest Points'),
            'text' => '<strong>' . Mage::helper('core')->currency($this->getGuestPoints(),true,false) . '</strong>',
        ));

        $fieldset->addField('customerbalance_point', 'text', array(
            'label' => Mage::helper('customerbalance')->__('Change Guest Points'),
            'title' => Mage::helper('customerbalance')->__('Change Guest Points'),
            'name' => 'Guestpoints[guest_points]',
            'class' => 'validate-number',
            'note' => Mage::helper('customerbalance')->__('Add or subtract customer\'s guest points. For ex: 99 or -99 affiliate points.'),
        ));

        $fieldset->addField('title_point', 'textarea', array(
            'label' => Mage::helper('customerbalance')->__('Change Transaction Title'),
            'title' => Mage::helper('customerbalance')->__('Change Transaction Title'),
            'name' => 'Guestpoints[title]',
            'style' => 'height: 5em;'
        ));

        $fieldset->addField('description_point', 'textarea', array(
            'label' => Mage::helper('customerbalance')->__('Change Transaction Description'),
            'title' => Mage::helper('customerbalance')->__('Change Transaction Description'),
            'name' => 'Guestpoints[description]',
            'style' => 'height: 5em;'
        ));

        $fieldset = $form->addFieldset('customerbalance_history_fieldset', array(
            'legend' => Mage::helper('customerbalance')->__('Transaction History')
        ))->setRenderer($this->getLayout()->createBlock('adminhtml/widget_form_renderer_fieldset')->setTemplate(
            'customerbalance/guestpoints/history.phtml'
        ));

        return parent::_prepareForm();
    }

    public function getTabLabel()
    {
        return Mage::helper('customerbalance')->__('Guest Points');
    }

    public function getTabTitle()
    {
        return Mage::helper('customerbalance')->__('Guest Points');
    }

    public function canShowTab()
    {
        return true;
    }

    public function isHidden()
    {
        return false;
    }
}
