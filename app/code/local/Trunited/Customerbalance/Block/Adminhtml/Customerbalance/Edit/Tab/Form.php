<?php

class Trunited_Customerbalance_Block_Adminhtml_Customerbalance_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
	protected function _prepareForm(){
		$form = new Varien_Data_Form();
		$this->setForm($form);
		
		if (Mage::getSingleton('adminhtml/session')->getCustomerbalanceData()){
			$data = Mage::getSingleton('adminhtml/session')->getCustomerbalanceData();
			Mage::getSingleton('adminhtml/session')->setCustomerbalanceData(null);
		}elseif(Mage::registry('customerbalance_data'))
			$data = Mage::registry('customerbalance_data')->getData();
		
		$fieldset = $form->addFieldset('customerbalance_form', array('legend'=>Mage::helper('customerbalance')->__('Item information')));

		$fieldset->addField('title', 'text', array(
			'label'		=> Mage::helper('customerbalance')->__('Title'),
			'class'		=> 'required-entry',
			'required'	=> true,
			'name'		=> 'title',
		));

		$fieldset->addField('filename', 'file', array(
			'label'		=> Mage::helper('customerbalance')->__('File'),
			'required'	=> false,
			'name'		=> 'filename',
		));

		$fieldset->addField('status', 'select', array(
			'label'		=> Mage::helper('customerbalance')->__('Status'),
			'name'		=> 'status',
			'values'	=> Mage::getSingleton('customerbalance/status')->getOptionHash(),
		));

		$fieldset->addField('content', 'editor', array(
			'name'		=> 'content',
			'label'		=> Mage::helper('customerbalance')->__('Content'),
			'title'		=> Mage::helper('customerbalance')->__('Content'),
			'style'		=> 'width:700px; height:500px;',
			'wysiwyg'	=> false,
			'required'	=> true,
		));

		$form->setValues($data);
		return parent::_prepareForm();
	}
}