<?php

class Trunited_Customerbalance_Block_Adminhtml_Customerbalance_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
	public function __construct(){
		parent::__construct();
		
		$this->_objectId = 'id';
		$this->_blockGroup = 'customerbalance';
		$this->_controller = 'adminhtml_customerbalance';
		
		$this->_updateButton('save', 'label', Mage::helper('customerbalance')->__('Save Item'));
		$this->_updateButton('delete', 'label', Mage::helper('customerbalance')->__('Delete Item'));
		
		$this->_addButton('saveandcontinue', array(
			'label'		=> Mage::helper('adminhtml')->__('Save And Continue Edit'),
			'onclick'	=> 'saveAndContinueEdit()',
			'class'		=> 'save',
		), -100);

		$this->_formScripts[] = "
			function toggleEditor() {
				if (tinyMCE.getInstanceById('customerbalance_content') == null)
					tinyMCE.execCommand('mceAddControl', false, 'customerbalance_content');
				else
					tinyMCE.execCommand('mceRemoveControl', false, 'customerbalance_content');
			}

			function saveAndContinueEdit(){
				editForm.submit($('edit_form').action+'back/edit/');
			}
		";
	}

	public function getHeaderText(){
		if(Mage::registry('customerbalance_data') && Mage::registry('customerbalance_data')->getId())
			return Mage::helper('customerbalance')->__("Edit Item '%s'", $this->htmlEscape(Mage::registry('customerbalance_data')->getTitle()));
		return Mage::helper('customerbalance')->__('Add Item');
	}
}