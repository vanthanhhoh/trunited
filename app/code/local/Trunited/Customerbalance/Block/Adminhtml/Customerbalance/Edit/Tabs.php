<?php

class Trunited_Customerbalance_Block_Adminhtml_Customerbalance_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
	public function __construct(){
		parent::__construct();
		$this->setId('customerbalance_tabs');
		$this->setDestElementId('edit_form');
		$this->setTitle(Mage::helper('customerbalance')->__('Item Information'));
	}

	protected function _beforeToHtml(){
		$this->addTab('form_section', array(
			'label'	 => Mage::helper('customerbalance')->__('Item Information'),
			'title'	 => Mage::helper('customerbalance')->__('Item Information'),
			'content'	 => $this->getLayout()->createBlock('customerbalance/adminhtml_customerbalance_edit_tab_form')->toHtml(),
		));
		return parent::_beforeToHtml();
	}
}