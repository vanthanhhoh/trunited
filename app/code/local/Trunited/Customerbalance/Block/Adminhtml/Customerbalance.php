<?php

class Trunited_Customerbalance_Block_Adminhtml_Customerbalance extends Mage_Adminhtml_Block_Widget_Grid_Container
{
	public function __construct(){
		$this->_controller = 'adminhtml_customerbalance';
		$this->_blockGroup = 'customerbalance';
		$this->_headerText = Mage::helper('customerbalance')->__('Item Manager');
		$this->_addButtonLabel = Mage::helper('customerbalance')->__('Add Item');
		parent::__construct();
	}
}