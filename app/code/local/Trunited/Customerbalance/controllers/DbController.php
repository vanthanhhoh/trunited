<?php

class Trunited_Customerbalance_DbController extends Mage_Core_Controller_Front_Action
{
    public function updateDbAction()
    {
        $setup = new Mage_Core_Model_Resource_Setup(null);
        $installer = $setup;
        $installer->startSetup();
        $installer->run("
            DROP TABLE IF EXISTS {$setup->getTable('customerbalance/customerguestpending')};
            CREATE TABLE {$setup->getTable('customerbalance/customerguestpending')} (
                `customerguestpending_id` int(11) unsigned NOT NULL auto_increment,
                `customer_id` int(10) unsigned NOT NULL,
                `guest_points_pending` DECIMAL(10,2) unsigned NOT NULL default 0,
                `created_at` datetime NULL,
                `updated_at` datetime NULL,
                PRIMARY KEY (`customerguestpending_id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
        ");
        $installer->endSetup();
        echo "success";
    }
}
