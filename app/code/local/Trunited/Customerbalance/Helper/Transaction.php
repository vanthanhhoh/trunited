<?php

class Trunited_Customerbalance_Helper_Transaction extends Mage_Core_Helper_Abstract
{
    public function createTransaction($account, $data, $type, $status, $is_pending = false)
    {
        if((!Mage::helper('customerbalance')->isEnableGuestPoints() && !$is_pending) ||
            (!Mage::helper('customerbalance')->isEnableGuestPointsPending() && $is_pending))
        {
            return null;
        }


        $result = null;
        try {
            if (!$account->getId())
                throw new Exception(
                    Mage::helper('customerbalance')->__('Customer doesn\'t exist')
                );

            $customer = Mage::getModel('customer/customer')->load($account->getCustomerId());

            if(!$is_pending)
                $transaction = Mage::getModel('customerbalance/transactionguest');
            else
                $transaction = Mage::getModel('customerbalance/transactionguestpending');

            $_data = array();
            if(!$is_pending)
                $_data['customerguest_id'] = $account->getId();
            else
                $_data['customerguestpending_id'] = $account->getId();

            $_data['customer_id'] = $customer->getId();
            $_data['customer_email'] = $customer->getEmail();
            $_data['title'] = isset($data['title']) ? $data['title'] : '';
            $_data['description'] = isset($data['description']) ? $data['description'] : '';
            $_data['action_type'] = $type;
            $_data['award_from'] = isset($data['award_from']) ? $data['award_from'] : Trunited_Customerbalance_Model_Status::AWARD_FROM_OTHER;
            $_data['status'] = $status;
            $_data['created_at'] = isset($data['created_at']) ? $data['created_at'] : now();
            $_data['updated_at'] = now();
            $_data['expiration_at'] = isset($data['expiration_at']) ? $data['expiration_at'] : '';
            $_data['order_id'] = isset($data['order_id']) ? $data['order_id'] : '';

            if(!$is_pending)
                $_data['current_credit'] = $account->getGuestPoints();
            else
                $_data['current_credit'] = $account->getGuestPointsPending();

            $_data['changed_credit'] = isset($data['changed_credit']) ? $data['changed_credit'] : '';
            $_data['receiver_email'] = isset($data['receiver_email']) ? $data['receiver_email'] : '';
            $_data['receiver_customer_id'] = isset($data['receiver_customer_id']) ? $data['receiver_customer_id'] : '';
            $_data['recipient_transaction_id'] = isset($data['recipient_transaction_id']) ? $data['recipient_transaction_id'] : '';
            $_data['credit_back'] = isset($data['credit_back']) ? $data['credit_back'] : '';
            $_data['order_filter_ids'] = isset($data['order_filter_ids']) ? $data['order_filter_ids'] : '';
            $_data['is_on_hold'] = isset($data['is_on_hold']) ? $data['is_on_hold'] : 0;
            $_data['hold_credit'] = isset($data['hold_credit']) ? $data['hold_credit'] : 0;

            $transaction->setData($_data);
            $transaction->save();

            $result = $transaction;

        } catch (Exception $ex) {
            Mage::getSingleton('adminhtml/session')->addError(
                Mage::helper('customerbalance')->__($ex->getMessage())
            );
            $result = null;
        }

        return $result;
    }

    public function checkExpiryDateOnHoldTransaction()
    {
        if(!Mage::helper('customerbalance')->isEnableGuestPointsPending())
            return null;
            
        $collection = Mage::getModel('customerbalance/transactionguestpending')->getCollection()
            ->addFieldToFilter('action_type', array('in' => array(
                Trunited_Customerbalance_Model_Type::TYPE_TRANSACTION_BY_ADMIN,
                Trunited_Customerbalance_Model_Type::TYPE_TRANSACTION_RECEIVE_FROM_GLOBAL_BRAND,
            )))
            ->addFieldToFilter('status', Trunited_Customerbalance_Model_Status::STATUS_TRANSACTION_ON_HOLD)
            ->addFieldToFilter('expiration_at', array('notnull' => true))
            ->addFieldToFilter('is_on_hold', 1)
            ->setOrder('transaction_id', 'desc')
        ;

        if(sizeof($collection) > 0)
        {
            $t = time();
            foreach ($collection as $transaction) {
                $date = $transaction->getExpirationAt();

                if(date('Y',strtotime($date)) == date('Y', $t) &&
                    date('m',strtotime($date)) == date('m', $t))
                {
                    try {
                        Mage::helper('customerbalance/account')->updatePointsPending(
                            $transaction->getCustomerId(),
                            $transaction->getHoldCredit()
                        );

                        $transaction->setData('changed_credit', $transaction->getHoldCredit());
                        $transaction->setData('updated_at', now());
                        $transaction->setData('status', Trunited_Customerbalance_Model_Status::STATUS_TRANSACTION_COMPLETED);
                        $transaction->save();

                    } catch (Exception $e) {
                        Mage::logException($e);
                    }
                }

            }

        }
    }

    public function saveTemporary($order, $customer_id)
    {
        $model = Mage::getModel('customerbalance/temporaryguest');
        $model->setData('order_id', $order->getEntityId());
        $model->setData('customer_id', $customer_id);
        $model->setData('status', Trunited_Customerbalance_Model_Status::TEMPORARY_PENDING);
        $model->save();
    }

    public function isExistOrder($order_id)
    {
        zend_debug::dump(get_class(Mage::getModel('customerbalance/temporaryguest')->getCollection()));
        $temporary = Mage::getModel('customerbalance/temporaryguest')->getCollection()
            ->addFieldToFilter('order_id', $order_id)
            ->addFieldToFilter('status', Trunited_Customerbalance_Model_Status::TEMPORARY_PENDING)
            ->getFirstItem();

        if($temporary != null && $temporary->getId())
            return $temporary;
        else
            return false;
    }

    public function createTransactionFromGuest($order)
    {
        $percent = Mage::helper('customerbalance')->getConfigData('guest_points', 'percent_earnings_from_guest');
        $_percent = $percent != '' ? $percent : 1;

        if($order->getData('customer_is_guest')){

            $temporary = $this->isExistOrder($order->getEntityId());

            if($temporary != null && $temporary->getId())
            {
                $bonus = $order->getData('rewardpoints_bonus');
                $items = $order->getAllItems();
                $points = 0;

                if(sizeof($items) > 0){
                    foreach ($items as $item) {
                        $product = Mage::getModel('catalog/product')->load($item->getProductId());
                        if($product != null && $product->getId())
                        {
                            $points += $product->getData('rewardpoints_earn') * $item->getQtyOrdered();
                        }
                    }
                }

                $total_points = $points + $bonus;
                if($total_points > 0)
                {
                    $earnings = $total_points * $_percent * 0.01;
                    $customer = Mage::getModel('customer/customer')->load($temporary->getCustomerId());

                    if ($customer != null && $customer->getId()) {

                        $customerbalanceAccount = Mage::helper('customerbalance/account')
                            ->updatePoints($customer->getId(), $earnings);

                        if ($customerbalanceAccount != null && $customerbalanceAccount->getId()) {

                            $params = array(
                                'title' => Mage::helper('customerbalance')->__('Guest Commission awarded for Guest <b>%s</b> - <a href="mailto:%s">%s</a> checkout order #<a href="%s">%s</a>',
                                    $order->getData('customer_firstname').' '.$order->getData('customer_lastname'),
                                    $order->getData('customer_email'),
                                    $order->getData('customer_email'),
                                    Mage::getUrl('sales/order/view', array('order_id' => $order->getId())),
                                    $order->getIncrementId()
                                ),
                                'award_from' => Trunited_Customerbalance_Model_Status::AWARD_FROM_GUEST_CHECKOUT,
                                'current_credit' => $customerbalanceAccount->getGuestPoints(),
                                'is_on_hold' => 0,
                                'hold_credit' => 0,
                                'changed_credit' => $earnings,
                                'expiration_at' => '',
                                'created_at' => now(),
                                'order_id' => $order->getIncrementId()
                            );

                            $this->createTransaction(
                                $customerbalanceAccount,
                                $params,
                                Trunited_Customerbalance_Model_Type::TYPE_TRANSACTION_RECEIVE_FROM_GUEST_CHECKOUT,
                                Trunited_Customerbalance_Model_Status::STATUS_TRANSACTION_COMPLETED
                            );
                        }
                    }
                }

                $temporary->setData('status', Trunited_Customerbalance_Model_Status::TEMPORARY_COMPLETE);
                $temporary->save();
            }
        }
    }
}
