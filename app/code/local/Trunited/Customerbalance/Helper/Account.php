<?php

class Trunited_Customerbalance_Helper_Account extends Mage_Core_Helper_Abstract
{
    public function updateCredit($customer_id, $credit)
    {
        if(!Mage::helper('customerbalance')->isEnable())
            return null;

        $account = $this->loadByCustomerId($customer_id);
        if($account->getId())
        {
            $_current_credit = $account->getGuestCommission();
            $new_credit = $_current_credit + $credit;
            $account->setGuestCommission($new_credit > 0 ? $new_credit : 0);
            $account->save();

            return $account;
        } else
            return null;

    }

    public function updatePoints($customer_id, $credit)
    {
        if(!Mage::helper('customerbalance')->isEnableGuestPoints())
            return null;

        $account = $this->loadByCustomerId($customer_id);
        if($account->getId())
        {
            $_current_credit = $account->getGuestPoints();
            $new_credit = $_current_credit + $credit;
            $account->setGuestPoints($new_credit > 0 ? $new_credit : 0);
            $account->save();

            return $account;
        } else
            return null;

    }

    public function updatePointsPending($customer_id, $credit)
    {
        if(!Mage::helper('customerbalance')->isEnableGuestPointsPending())
            return null;

        $account = $this->loadByCustomerId($customer_id, true);
        if($account->getId())
        {
            $_current_credit = $account->getGuestPointsPending();
            $new_credit = $_current_credit + $credit;
            $account->setGuestPointsPending($new_credit > 0 ? $new_credit : 0);
            $account->save();

            return $account;
        } else
            return null;

    }

    public function loadByCustomerId($customer_id, $is_pending = false)
    {
        if($is_pending){
            $account = Mage::getModel('customerbalance/customerguestpending')->load($customer_id, 'customer_id');
            if($account->getId())
                return $account;
            else{
                $new_account = $this->createGuestPointsPendingCustomer($customer_id);
                if($new_account != null)
                    return $new_account;
                else
                    return null;
            }
        } else {
            $account = Mage::getModel('customerbalance/customerguest')->load($customer_id, 'customer_id');
            if($account->getId())
                return $account;
            else{
                $new_account = $this->createGuestPointsCustomer($customer_id);
                if($new_account != null)
                    return $new_account;
                else
                    return null;
            }
        }

    }

    public function createGuestPointsCustomer($customer_id)
    {
        if(!Mage::helper('customerbalance')->isEnableGuestPoints())
            return null;
            
        $model = Mage::getModel('customerbalance/customerguest');

        $model->setData('customer_id', $customer_id);
        $model->setData('guest_points', 0);

        try{
            if($customer_id != 0)
                $model->save();
            else
                return null;
        } catch (Exception $ex){
            return null;
        }

        return $model;
    }

    public function createGuestPointsPendingCustomer($customer_id)
    {
        if(!Mage::helper('customerbalance')->isEnableGuestPointsPending())
            return null;

        $model = Mage::getModel('customerbalance/customerguestpending');

        $model->setData('customer_id', $customer_id);
        $model->setData('guest_points_pending', 0);

        try{
            if($customer_id != 0)
                $model->save();
            else
                return null;
        } catch (Exception $ex){
            return null;
        }

        return $model;
    }

    public function getCustomerId()
    {
        if(Mage::getSingleton('customer/session')->isLoggedIn()) {
            return Mage::getSingleton('customer/session')->getCustomer()->getId();
        } else {
            return null;
        }
    }

    public function getCurrentAccount($is_pending = false)
    {
        $customer_id = $this->getCustomerId();
        if ($customer_id != null) {
            $account = $this->loadByCustomerId($customer_id, $is_pending);

            if ($account != null) {
                return $account;
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    public function getGuestPoints($is_format = true)
    {
        $account = $this->getCurrentAccount();
        if($account != null)
        {
            if ($is_format)
                return Mage::helper('core')->currency(
                    $account->getGuestPoints(),
                    true,
                    false
                );
            else
                return $account->getGuestPoints();
        } else {
            return null;
        }
    }

    public function getGuestPointsPending($is_format = true)
    {
        $account = $this->getCurrentAccount(true);
        if($account != null)
        {
            if ($is_format)
                return Mage::helper('core')->currency(
                    $account->getGuestPointsPending(),
                    true,
                    false
                );
            else
                return $account->getGuestPointsPending();
        } else {
            return null;
        }
    }
	
}
