<?php

class Trunited_Deals_IndexController extends Mage_Core_Controller_Front_Action
{
    public function indexAction()
    {
        if(!Mage::helper('deals')->isEnable())
        {
            Mage::getSingleton('core/session')->addNotice(
                Mage::helper('deals')->__('The Deals page has been disabled.')
            );

            $this->_redirectUrl(Mage::getBaseUrl());
            return;
        }

        $this->loadLayout();

        $this->_title(Mage::helper('marketer')->__('Deals'));

        $breadcrumbs = $this->getLayout()->getBlock("breadcrumbs");

        $breadcrumbs->addCrumb("home", array(
            "label" => $this->__("Home"),
            "title" => $this->__("Home"),
            "link"  => Mage::getBaseUrl()
        ));

        $breadcrumbs->addCrumb("deals", array(
            "label" => $this->__("Deals"),
            "title" => $this->__("Deals"),
        ));

        $this->renderLayout();
    }

    public function updateDbAction()
    {
        $setup = new Mage_Core_Model_Resource_Setup(null);
        $installer = $setup;
        $installer->startSetup();
        $installer->run("

DROP TABLE IF EXISTS {$setup->getTable('deals/deals')};
CREATE TABLE {$setup->getTable('deals/deals')} (
  `deals_id` int(11) unsigned NOT NULL auto_increment,
  `title` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `start_date` datetime NULL,
  `end_date` datetime NULL,
  `is_main` smallint(6) NULL default '0',
  `is_featured` smallint(6) NULL default '0',
  `image` varchar(255) NULL default '',
  `text_color` varchar(255) NULL,
  `column` varchar(255) NULL,
  `sort_position` varchar(255) NULL,
  `customer_param` varchar(255) NULL,
  `created_at` datetime NULL,
  `updated_at` datetime NULL,
  PRIMARY KEY (`deals_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
        ");

        Mage::getModel('core/url_rewrite')
            ->setStoreId(1)
            ->setIdPath('trunited_deals')
            ->setRequestPath('deals.html')
            ->setTargetPath('deals/index/index')
            ->setIsSystem(0)
            ->save();
        $installer->endSetup();
        echo "success";
    }

    public function updateDb2Action()
    {
        $setup = new Mage_Catalog_Model_Resource_Eav_Mysql4_Setup(null);
        $installer = $setup;
        $installer->startSetup();
        $installer->run("");


        $setup->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'text_color', array(
            'group'            => 'General',
            'input'            => 'text',
            'type'             => 'text',
            'sort_order'       => 4,
            'label'            => 'Text Color',
            'backend'          => '',
            'visible'          => true,
            'required'         => false,
            'wysiwyg_enabled'  => false,
            'visible_on_front' => true,
            'default'           => '000000',
            'used_in_product_listing' => true,
            'global'           => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
            'note'             => Mage::helper('customproduct')->__('The colour of product\s name on the Deals page'),
        ));

        $installer->updateAttribute(Mage_Catalog_Model_Product::ENTITY, 'text_color', 'apply_to', Magestore_CustomProduct_Model_Type::PRODUCT_TYPE_DAILY_DEALS);
        $installer->updateAttribute(Mage_Catalog_Model_Product::ENTITY, 'text_color', 'apply_to', Magestore_CustomProduct_Model_Type::PRODUCT_TYPE_SHOP_NOW);

        $installer->endSetup();
        echo "success";
    }
}