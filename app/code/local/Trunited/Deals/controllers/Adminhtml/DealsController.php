<?php

class Trunited_Deals_Adminhtml_DealsController extends Mage_Adminhtml_Controller_Action
{
	protected function _initAction(){
		$this->loadLayout()
			->_setActiveMenu('deals/deals')
			->_addBreadcrumb(Mage::helper('adminhtml')->__('Items Manager'), Mage::helper('adminhtml')->__('Item Manager'));
		return $this;
	}
 
	public function indexAction(){
		$this->_initAction()
			->renderLayout();
	}

	public function editAction() {
		$id	 = $this->getRequest()->getParam('id');
		$model  = Mage::getModel('deals/deals')->load($id);

		if ($model->getId() || $id == 0) {
			$data = Mage::getSingleton('adminhtml/session')->getFormData(true);
			if (!empty($data))
				$model->setData($data);

			Mage::register('deals_data', $model);

			$this->loadLayout();
			$this->_setActiveMenu('deals/deals');

			$this->_addBreadcrumb(Mage::helper('adminhtml')->__('Item Manager'), Mage::helper('adminhtml')->__('Item Manager'));
			$this->_addBreadcrumb(Mage::helper('adminhtml')->__('Item News'), Mage::helper('adminhtml')->__('Item News'));

			$this->getLayout()->getBlock('head')->setCanLoadExtJs(true);
			$this->_addContent($this->getLayout()->createBlock('deals/adminhtml_deals_edit'))
				->_addLeft($this->getLayout()->createBlock('deals/adminhtml_deals_edit_tabs'));

			$this->renderLayout();
		} else {
			Mage::getSingleton('adminhtml/session')->addError(Mage::helper('deals')->__('Item does not exist'));
			$this->_redirect('*/*/');
		}
	}
 
	public function newAction() {
		$this->_forward('edit');
	}
 
	public function saveAction() {
		if ($data = $this->getRequest()->getPost()) {
			if(isset($_FILES['image']['name']) && $_FILES['image']['name'] != null){
				$data['image'] = $this->_uploadBackgroundFile('image');
			} else if(isset($data['image']) && is_array($data['image'])){

				if(isset($data['image']['delete']) && $data['image']['delete'] == 1){
					$data['image'] = '';
				} else if(isset($data['image']['value'])) {
					$data['image'] = str_replace('deals/','', $data['image']['value']);
				} else {
					$data['image'] = $this->_uploadBackgroundFile('image');
				}
			} else if ($sourceFile = $this->_uploadBackgroundFile('image')) {
				$data['image'] = $sourceFile;
			}

			$model = Mage::getModel('deals/deals');		
			$model->setData($data)
				->setId($this->getRequest()->getParam('id'));
			
			try {
				
				$model->save();
				Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('deals')->__('Item was successfully saved'));
				Mage::getSingleton('adminhtml/session')->setFormData(false);

				if ($this->getRequest()->getParam('back')) {
					$this->_redirect('*/*/edit', array('id' => $model->getId()));
					return;
				}
				$this->_redirect('*/*/');
				return;
			} catch (Exception $e) {
				Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
				Mage::getSingleton('adminhtml/session')->setFormData($data);
				$this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
				return;
			}
		}
		Mage::getSingleton('adminhtml/session')->addError(Mage::helper('deals')->__('Unable to find item to save'));
		$this->_redirect('*/*/');
	}

	/**
	 * Upload banner file to server
	 *
	 * @param string $fieldName
	 * @return string
	 */
	protected function _uploadBackgroundFile($fieldName)
	{
		if(isset($_FILES[$fieldName]['delete']) && $_FILES[$fieldName]['delete'] != 1){
			return '';
		}

		if (isset($_FILES[$fieldName]['name']) && $_FILES[$fieldName]['name'] != '') {
			try {
				$uploader = new Varien_File_Uploader($fieldName);
				$uploader->setAllowedExtensions(array('jpg', 'jpeg', 'gif', 'png', 'swf'));
				$uploader->setAllowRenameFiles(true);
				$uploader->setFilesDispersion(false);

				// We set media as the upload dir
				$path = Mage::getBaseDir('media') . DS . 'deals' . DS;
				$result = $uploader->save($path, $_FILES[$fieldName]['name']);

				return $result['file'];
			} catch (Exception $e) {
				return $_FILES[$fieldName]['name'];
			}
		}
		return '';
	}
 
	public function deleteAction() {
		if( $this->getRequest()->getParam('id') > 0 ) {
			try {
				$model = Mage::getModel('deals/deals');
				$model->setId($this->getRequest()->getParam('id'))
					->delete();
				Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Item was successfully deleted'));
				$this->_redirect('*/*/');
			} catch (Exception $e) {
				Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
				$this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
			}
		}
		$this->_redirect('*/*/');
	}

	public function massDeleteAction() {
		$dealsIds = $this->getRequest()->getParam('deals');
		if(!is_array($dealsIds)){
			Mage::getSingleton('adminhtml/session')->addError(Mage::helper('adminhtml')->__('Please select item(s)'));
		}else{
			try {
				foreach ($dealsIds as $dealsId) {
					$deals = Mage::getModel('deals/deals')->load($dealsId);
					$deals->delete();
				}
				Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Total of %d record(s) were successfully deleted', count($dealsIds)));
			} catch (Exception $e) {
				Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
			}
		}
		$this->_redirect('*/*/index');
	}
	
	public function massStatusAction() {
		$dealsIds = $this->getRequest()->getParam('deals');
		if(!is_array($dealsIds)) {
			Mage::getSingleton('adminhtml/session')->addError($this->__('Please select item(s)'));
		} else {
			try {
				foreach ($dealsIds as $dealsId) {
					$deals = Mage::getSingleton('deals/deals')
						->load($dealsId)
						->setStatus($this->getRequest()->getParam('status'))
						->setIsMassupdate(true)
						->save();
				}
				$this->_getSession()->addSuccess(
					$this->__('Total of %d record(s) were successfully updated', count($dealsIds))
				);
			} catch (Exception $e) {
				$this->_getSession()->addError($e->getMessage());
			}
		}
		$this->_redirect('*/*/index');
	}

	public function massMainAction() {
		$dealsIds = $this->getRequest()->getParam('deals');
		if(!is_array($dealsIds)) {
			Mage::getSingleton('adminhtml/session')->addError($this->__('Please select item(s)'));
		} else {
			try {
				foreach ($dealsIds as $dealsId) {
					$deals = Mage::getSingleton('deals/deals')
						->load($dealsId)
						->setIsMain($this->getRequest()->getParam('is_main'))
						->setIsMassupdate(true)
						->save();
				}
				$this->_getSession()->addSuccess(
					$this->__('Total of %d record(s) were successfully updated', count($dealsIds))
				);
			} catch (Exception $e) {
				$this->_getSession()->addError($e->getMessage());
			}
		}
		$this->_redirect('*/*/index');
	}

	public function massFeaturedAction() {
		$dealsIds = $this->getRequest()->getParam('deals');
		if(!is_array($dealsIds)) {
			Mage::getSingleton('adminhtml/session')->addError($this->__('Please select item(s)'));
		} else {
			try {
				foreach ($dealsIds as $dealsId) {
					$deals = Mage::getSingleton('deals/deals')
						->load($dealsId)
						->setIsFeatured($this->getRequest()->getParam('is_featured'))
						->setIsMassupdate(true)
						->save();
				}
				$this->_getSession()->addSuccess(
					$this->__('Total of %d record(s) were successfully updated', count($dealsIds))
				);
			} catch (Exception $e) {
				$this->_getSession()->addError($e->getMessage());
			}
		}
		$this->_redirect('*/*/index');
	}
  
	public function exportCsvAction(){
		$fileName   = 'deals.csv';
		$content	= $this->getLayout()->createBlock('deals/adminhtml_deals_grid')->getCsv();
		$this->_prepareDownloadResponse($fileName,$content);
	}

	public function exportXmlAction(){
		$fileName   = 'deals.xml';
		$content	= $this->getLayout()->createBlock('deals/adminhtml_deals_grid')->getXml();
		$this->_prepareDownloadResponse($fileName,$content);
	}
}