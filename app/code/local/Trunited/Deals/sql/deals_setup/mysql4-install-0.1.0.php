<?php

$installer = $this;
$installer->startSetup();

$installer->run("

DROP TABLE IF EXISTS {$this->getTable('deals/deals')};
CREATE TABLE {$this->getTable('deals/deals')} (
  `deals_id` int(11) unsigned NOT NULL auto_increment,
  `title` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `start_date` datetime NULL,
  `end_date` datetime NULL,
  `is_main` smallint(6) NULL default '0',
  `is_featured` smallint(6) NULL default '0',
  `image` varchar(255) NULL default '',
  `text_color` varchar(255) NULL,
  `column` varchar(255) NULL,
  `sort_position` varchar(255) NULL,
  `customer_param` varchar(255) NULL,
  `created_at` datetime NULL,
  `updated_at` datetime NULL,
  PRIMARY KEY (`deals_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

    ");

Mage::getModel('core/url_rewrite')
    ->setStoreId(1)
    ->setIdPath('trunited_deals')
    ->setRequestPath('deals.html')
    ->setTargetPath('deals/index/index')
    ->setIsSystem(0)
    ->save();

$installer->endSetup(); 