<?php

class Trunited_Deals_Block_Adminhtml_Deals_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('dealsGrid');
        $this->setDefaultSort('deals_id');
        $this->setDefaultDir('ASC');
        $this->setSaveParametersInSession(true);
    }

    protected function _prepareCollection()
    {
        $collection = Mage::getModel('deals/deals')->getCollection();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $this->addColumn('deals_id', array(
            'header' => Mage::helper('deals')->__('ID'),
            'align' => 'right',
            'width' => '50px',
            'index' => 'deals_id',
        ));

        $this->addColumn('image', array(
            'header' => Mage::helper('deals')->__('Image'),
            'width' => '50px',
            'index' => 'image',
            'filter' => false,
            'sortable' => false,
            'renderer' => 'Trunited_Deals_Block_Adminhtml_Renderer_Deals_Image',
        ));

        $this->addColumn('title', array(
            'header' => Mage::helper('deals')->__('Title'),
            'align' => 'left',
            'index' => 'title',
            'width' => '500px',
        ));

        $this->addColumn('url', array(
            'header' => Mage::helper('deals')->__('URL'),
            'width' => '50px',
            'index' => 'url',
            'renderer' => 'Trunited_Deals_Block_Adminhtml_Renderer_Deals_Url',
        ));

        $this->addColumn('start_date', array(
            'header' => Mage::helper('marketer')->__('Start Date'),
            'align' => 'left',
            'index' => 'start_date',
            'type' => 'date'
        ));

        $this->addColumn('end_date', array(
            'header' => Mage::helper('marketer')->__('End Date'),
            'align' => 'left',
            'index' => 'end_date',
            'type' => 'date'
        ));

        $this->addColumn('is_main', array(
            'header' => Mage::helper('deals')->__('Is Main'),
            'align' => 'left',
            'width' => '50px',
            'index' => 'is_main',
            'type' => 'options',
            'options' => Trunited_Deals_Model_Status::getOptionArray(),
        ));

        $this->addColumn('is_featured', array(
            'header' => Mage::helper('deals')->__('Is Featured'),
            'align' => 'left',
            'width' => '50px',
            'index' => 'is_featured',
            'type' => 'options',
            'options' => Trunited_Deals_Model_Status::getOptionArray(),
        ));

        $this->addColumn('text_color', array(
            'header' => Mage::helper('deals')->__('Text Color'),
            'align' => 'left',
            'width' => '50px',
            'class' => 'color',
            'index' => 'text_color',
        ));

        $this->addColumn('column', array(
            'header' => Mage::helper('deals')->__('Column'),
            'align' => 'left',
            'width' => '50px',
            'index' => 'column',
            'type' => 'options',
            'options' => Trunited_Deals_Model_Status::getOptionColumnArray(),
        ));

        $this->addColumn('sort_position', array(
            'header' => Mage::helper('deals')->__('Sort Position'),
            'align' => 'left',
            'width' => '50px',
            'index' => 'sort_position',
            'type' => 'number',
        ));

        $this->addColumn('created_at', array(
            'header' => Mage::helper('marketer')->__('Created At'),
            'align' => 'left',
            'index' => 'created_at',
            'type' => 'date'
        ));

        $this->addColumn('updated_at', array(
            'header' => Mage::helper('marketer')->__('Updated At'),
            'align' => 'left',
            'index' => 'updated_at',
            'type' => 'date'
        ));

        $this->addColumn('action',
            array(
                'header' => Mage::helper('deals')->__('Action'),
                'width' => '100',
                'type' => 'action',
                'getter' => 'getId',
                'actions' => array(
                    array(
                        'caption' => Mage::helper('deals')->__('Edit'),
                        'url' => array('base' => '*/*/edit'),
                        'field' => 'id'
                    )),
                'filter' => false,
                'sortable' => false,
                'index' => 'stores',
                'is_system' => true,
            ));

        $this->addExportType('*/*/exportCsv', Mage::helper('deals')->__('CSV'));
        $this->addExportType('*/*/exportXml', Mage::helper('deals')->__('XML'));

        return parent::_prepareColumns();
    }

    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('deals_id');
        $this->getMassactionBlock()->setFormFieldName('deals');

        $this->getMassactionBlock()->addItem('delete', array(
            'label' => Mage::helper('deals')->__('Delete'),
            'url' => $this->getUrl('*/*/massDelete'),
            'confirm' => Mage::helper('deals')->__('Are you sure?')
        ));

        $statuses = Mage::getSingleton('deals/status')->getOptionArray();

        array_unshift($statuses, array('label' => '', 'value' => ''));
        $this->getMassactionBlock()->addItem('is_main', array(
            'label' => Mage::helper('deals')->__('Is Main'),
            'url' => $this->getUrl('*/*/massMain', array('_current' => true)),
            'additional' => array(
                'visibility' => array(
                    'name' => 'is_main',
                    'type' => 'select',
                    'class' => 'required-entry',
                    'label' => Mage::helper('deals')->__('Is Main'),
                    'values' => $statuses
                ))
        ));

        $this->getMassactionBlock()->addItem('is_featured', array(
            'label' => Mage::helper('deals')->__('Is Featured'),
            'url' => $this->getUrl('*/*/massFeatured', array('_current' => true)),
            'additional' => array(
                'visibility' => array(
                    'name' => 'is_featured',
                    'type' => 'select',
                    'class' => 'required-entry',
                    'label' => Mage::helper('deals')->__('Is Featured'),
                    'values' => $statuses
                ))
        ));
        return $this;
    }

    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', array('id' => $row->getId()));
    }
}