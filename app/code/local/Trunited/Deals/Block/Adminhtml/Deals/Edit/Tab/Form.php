<?php

class Trunited_Deals_Block_Adminhtml_Deals_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
	protected function _prepareForm(){
		$form = new Varien_Data_Form();
		$this->setForm($form);
		
		if (Mage::getSingleton('adminhtml/session')->getDealsData()){
			$data = Mage::getSingleton('adminhtml/session')->getDealsData();
			Mage::getSingleton('adminhtml/session')->setDealsData(null);
		}elseif(Mage::registry('deals_data'))
			$data = Mage::registry('deals_data')->getData();
		
		$fieldset = $form->addFieldset('deals_form', array('legend'=>Mage::helper('deals')->__('Item information')));

		$dateTimeFormatIso = Mage::app()->getLocale()->getDateTimeFormat(Mage_Core_Model_Locale::FORMAT_TYPE_SHORT);

		$fieldset->addField('title', 'textarea', array(
			'label'		=> Mage::helper('deals')->__('Title'),
			'class'		=> 'required-entry',
			'required'	=> true,
			'name'		=> 'title',
		));

		$fieldset->addField('url', 'textarea', array(
			'label'		=> Mage::helper('deals')->__('URL'),
			'class'		=> 'required-entry validate-url',
			'required'	=> true,
			'name'		=> 'url',
		));

		$fieldset->addField('start_date', 'datetime', array(
			'label'    => Mage::helper('deals')->__('Start Date'),
			'title'    => Mage::helper('deals')->__('Start Date'),
			'time'      => true,
			'name'     => 'start_date',
			'image'    => $this->getSkinUrl('images/grid-cal.gif'),
			'format'   => $dateTimeFormatIso,
			'required' => true,

		));

		$fieldset->addField('end_date', 'datetime', array(
			'label'    => Mage::helper('deals')->__('End Date'),
			'title'    => Mage::helper('deals')->__('End Date'),
			'time'      => true,
			'name'     => 'end_date',
			'image'    => $this->getSkinUrl('images/grid-cal.gif'),
			'format'   => $dateTimeFormatIso,
			'required' => false,
			'after_element_html' => '',
		));
		if(!$this->getRequest()->getParam('id')){
			$data['is_main'] = '2';
			$data['is_featured'] = '2';
		}

		$fieldset->addField('is_main', 'select', array(
			'label'		=> Mage::helper('deals')->__('Is Main'),
			'name'		=> 'is_main',
			'values'	=> Mage::getSingleton('deals/status')->getOptionHash(),
		));

		$fieldset->addField('is_featured', 'select', array(
			'label'		=> Mage::helper('deals')->__('Is Featured'),
			'name'		=> 'is_featured',
			'values'	=> Mage::getSingleton('deals/status')->getOptionHash(),
		));

		if(isset($data['image']) && $data['image'] != ''){
			$data['image'] = 'deals'. DS . $data['image'];
		}
		$fieldset->addField('image', 'image', array(
			'label'		=> Mage::helper('deals')->__('Image'),
			'required'	=> false,
			'name'		=> 'image',
		));

		if(!$this->getRequest()->getParam('id'))
			$data['text_color'] = '000000';

		$fieldset->addField('text_color', 'text', array(
			'label'		=> Mage::helper('deals')->__('Text Color'),
			'class'		=> 'color',
			'required'	=> false,
			'name'		=> 'text_color',
			'after_element_html' => 'The colour of title: Black: 000000, Orange: f05a23, Blue: 084f85. The default is Black colour.',
		));

		$fieldset->addField('column', 'select', array(
			'label'		=> Mage::helper('deals')->__('Column'),
			'values'	=> Mage::getSingleton('deals/status')->getOptionColumnHash(),
			'name'		=> 'column',
		));

		if(!$this->getRequest()->getParam('id'))
			$data['sort_position'] = '1';

		$fieldset->addField('sort_position', 'text', array(
			'label'		=> Mage::helper('deals')->__('Sort Position'),
			'class'		=> '',
			'required'	=> false,
			'name'		=> 'sort_position',
		));

		if(!$this->getRequest()->getParam('id'))
			$data['customer_param'] = 'u1';

		$fieldset->addField('customer_param', 'text', array(
			'label'		=> Mage::helper('deals')->__('Customer Param'),
			'class'		=> '',
			'required'	=> false,
			'name'		=> 'customer_param',
		));

		$form->setValues($data);
		return parent::_prepareForm();
	}
}