<?php
/**
 * Created by PhpStorm.
 * User: anthonyvu
 * Date: 1/6/18
 * Time: 10:34 AM
 */

class Trunited_Deals_Block_Adminhtml_Renderer_Deals_Image extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {
        $filename = $row->getData($this->getColumn()->getIndex());
        $img = Mage::getBaseUrl('media') . DS . 'deals' . DS . $filename;
        if($filename != '')
            return "<img src='".$img."' width='50' height='50' />";
        else
            return '';
    }

}

?>
