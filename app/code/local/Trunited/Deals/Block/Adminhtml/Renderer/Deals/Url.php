<?php
/**
 * Created by PhpStorm.
 * User: anthonyvu
 * Date: 1/6/18
 * Time: 10:34 AM
 */

class Trunited_Deals_Block_Adminhtml_Renderer_Deals_Url extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {
        $href = $row->getData($this->getColumn()->getIndex());
        $href .= '?'.$row->getData('customer_param').'=';
        return "<a href='$href' target='_blank'>Link</a>";
    }

}

?>
