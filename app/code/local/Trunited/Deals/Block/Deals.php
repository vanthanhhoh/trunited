<?php

class Trunited_Deals_Block_Deals extends Mage_Core_Block_Template
{
	protected $_featured;
	protected $_main;

	public function _prepareLayout(){
		return parent::_prepareLayout();
	}

	public function getHelper(){
		return Mage::helper('deals');
	}

	public function getFeatured()
	{
		if($this->_featured == null){
			$this->_featured = $this->getHelper()->getFeatureDeal();
		}

		return $this->_featured;
	}

	public function getMain(){
		if($this->_main == null){
			$this->_main = $this->getHelper()->getMainDeal();
		}

		return $this->_main;
	}

	public function getFeatureDeal()
	{
		return $this->getFeatured();
	}

	public function getMainDeal()
	{
		return $this->getMain();
	}

	public function getDealsOnFirstColumn()
	{
		$featured_main = array();
		$featured = $this->getFeatured();
		if($featured != null && $featured->getId())
		{
			$featured_main[] = $featured->getId();
		}

		$main = $this->getMain();
		if($main != null && $main->getId())
		{
			$featured_main[] = $main->getId();
		}

		$collection = $this->getHelper()->getDealsOnColumn(
			Trunited_Deals_Model_Status::COLUMN_FIRST,
			$featured_main
		);

		return $collection;
	}

	public function getDealsOnSecondColumn()
	{
		$featured_main = array();
		$featured = $this->getFeatured();
		if($featured != null && $featured->getId())
		{
			$featured_main[] = $featured->getId();
		}

		$main = $this->getMain();
		if($main != null && $main->getId())
		{
			$featured_main[] = $main->getId();
		}

		$collection = $this->getHelper()->getDealsOnColumn(
			Trunited_Deals_Model_Status::COLUMN_SECOND,
			$featured_main
		);

		return $collection;
	}

	public function getDealsOnThirdColumn()
	{
		$featured_main = array();
		$featured = $this->getFeatured();
		if($featured != null && $featured->getId())
		{
			$featured_main[] = $featured->getId();
		}

		$main = $this->getMain();
		if($main != null && $main->getId())
		{
			$featured_main[] = $main->getId();
		}

		$collection = $this->getHelper()->getDealsOnColumn(
			Trunited_Deals_Model_Status::COLUMN_THIRD,
			$featured_main
		);

		return $collection;
	}

	public function isEnableGiftCardsSection()
	{
		return $this->getHelper()->getConfigData('general', 'enable_giftcard');
	}

	public function getGiftCards()
	{
		$giftCards = $this->getHelper()->getGiftCards();

		if(sizeof($giftCards) > 0){
			$list = array();
			foreach($giftCards as $product){
				$list[] = $product;
			}

			return $list;
		}
		return null;
	}
}