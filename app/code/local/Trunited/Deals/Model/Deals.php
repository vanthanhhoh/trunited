<?php

class Trunited_Deals_Model_Deals extends Mage_Core_Model_Abstract
{
	public function _construct(){
		parent::_construct();
		$this->_init('deals/deals');
	}

	/**
	 * Before object save manipulations
	 *
	 * @return Trunited_Deals_Model_Deals
	 */
	protected function _beforeSave()
	{
		parent::_beforeSave();

		$this->setStartDate(date('Y/m/d H:i:s', strtotime($this->getStartDate())));
		$this->setEndDate(date('Y/m/d H:i:s', strtotime($this->getEndDate())));

		if($this->isObjectNew())
			$this->setData('created_at', now());

		$this->setData('updated_at', now());

		return $this;
	}

	public function getDisplayUrl()
	{
		$customer_id = '';
		if (Mage::getSingleton('customer/session')->isLoggedIn()) {
			$customer_id = Mage::getSingleton('customer/session')->getCustomer()->getId();
		}
		return $this->getUrl().'?'.$this->getCustomerParam().'='.$customer_id;
	}

	public function getImageUrl()
	{
		if($this->getImage() != ''){
			return Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA) . DS . 'deals' . DS . $this->getImage();
		}

		return null;
	}

	public function getColor()
	{
		if($this->getTextColor() == '')
			return '000000';
		else
			return $this->getTextColor();
	}
}