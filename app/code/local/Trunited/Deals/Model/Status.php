<?php

class Trunited_Deals_Model_Status extends Varien_Object
{
	const STATUS_ENABLED	= 1;
	const STATUS_DISABLED	= 2;

	const COLUMN_FIRST = 1;
	const COLUMN_SECOND = 2;
	const COLUMN_THIRD = 3;

	static public function getOptionArray(){
		return array(
			self::STATUS_ENABLED	=> Mage::helper('deals')->__('Yes'),
			self::STATUS_DISABLED   => Mage::helper('deals')->__('No')
		);
	}
	
	static public function getOptionHash(){
		$options = array();
		foreach (self::getOptionArray() as $value => $label)
			$options[] = array(
				'value'	=> $value,
				'label'	=> $label
			);
		return $options;
	}

	static public function getOptionColumnArray(){
		return array(
			self::COLUMN_FIRST	=> Mage::helper('deals')->__('1'),
			self::COLUMN_SECOND   => Mage::helper('deals')->__('2'),
			self::COLUMN_THIRD   => Mage::helper('deals')->__('3')
		);
	}

	static public function getOptionColumnHash(){
		$options = array();
		foreach (self::getOptionColumnArray() as $value => $label)
			$options[] = array(
				'value'	=> $value,
				'label'	=> $label
			);
		return $options;
	}
}