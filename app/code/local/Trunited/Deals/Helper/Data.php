<?php

class Trunited_Deals_Helper_Data extends Mage_Core_Helper_Abstract
{
    /**
     * @param $group
     * @param $field
     * @param null $store
     * @return mixed
     */
    public function getConfigData($group, $field, $store = null)
    {
        return Mage::getStoreConfig('deals/' . $group . '/' . $field, $store);
    }

    /**
     * @return mixed
     */
    public function isEnable()
    {
        return $this->getConfigData('general', 'enable');
    }

	public function getFeatureDeal()
    {
        $feature = Mage::getModel('deals/deals')->getCollection()
            ->addFieldToFilter('is_featured', Trunited_Deals_Model_Status::STATUS_ENABLED)
            ->addFieldToFilter('start_date', array('lteq'   => date('Y-m-d H:i:s', time())))
            ->addFieldToFilter('end_date', array('gteq'   => date('Y-m-d H:i:s', time())))
            ->setOrder('sort_position', 'asc')
            ->getFirstItem()
            ;

        if($feature != null && $feature->getId())
            return $feature;
        else
            return null;
    }

    public function getMainDeal()
    {
        $main = Mage::getModel('deals/deals')->getCollection()
            ->addFieldToFilter('is_main', Trunited_Deals_Model_Status::STATUS_ENABLED)
            ->addFieldToFilter('start_date', array('lteq'   => date('Y-m-d H:i:s', time())))
            ->addFieldToFilter('end_date', array('gteq'   => date('Y-m-d H:i:s', time())))
            ->setOrder('sort_position', 'asc')
            ->getFirstItem()
        ;

        if($main != null && $main->getId())
            return $main;
        else
            return null;
    }

    public function getDealsOnColumn($column, $feature_main)
    {
        $deals = Mage::getModel('deals/deals')->getCollection()
            ->addFieldToFilter('start_date', array('lteq'   => date('Y-m-d H:i:s', time())))
            ->addFieldToFilter('end_date', array('gteq'   => date('Y-m-d H:i:s', time())))
            ->addFieldToFilter('is_main', Trunited_Deals_Model_Status::STATUS_DISABLED)
            ->addFieldToFilter('is_featured', Trunited_Deals_Model_Status::STATUS_DISABLED)
            ->addFieldToFilter('column', array('eq' => $column))
            ;

        if(sizeof($feature_main) > 0){
            $deals->addFieldToFilter('deals_id', array('nin'    => $feature_main));
        }

        $deals->setOrder('sort_position', 'asc');

        return $deals;
    }

    public function getGiftCards()
    {
        $products = Mage::getModel('catalog/product')
            ->getCollection()
            ->addAttributeToSelect('*')
            ->addAttributeToFilter('type_id', 'gfitcardproduct')
            ->addAttributeToFilter('visibility', Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH)
            ->addAttributeToFilter('status', 1)
            ->setOrder('name', 'asc')
            ;

        return $products;
    }

    public function isLoggedIn()
    {
        return Mage::getSingleton('customer/session')->isLoggedIn();
    }

    public function getShopNowUrl($product, $is_aff = false)
    {
        $is_help = Mage::helper('fundraiser')->isShowHelpAffiliateFundraiser();
        if($this->isLoggedIn())
        {
            if($product->getShopNow() != null)
                if($is_help)
                    return str_replace('{{customer_id}}',Mage::getSingleton('customer/session')->getCustomer()->getId().'_fun',$product->getShopNow());
                else{
                    if(!$is_aff){
                        return str_replace('{{customer_id}}',Mage::getSingleton('customer/session')->getCustomer()->getId(),$product->getShopNow());
                    } else {
                        return str_replace('{{customer_id}}',Mage::getSingleton('customer/session')->getCustomer()->getId().'_aff',$product->getShopNow());
                    }
                }
            else
                return '#';
        } else {
            $affiliate_account = Mage::helper('affiliateplus/account')->getAffiliateInfoFromCookie();

            if($affiliate_account != null && $affiliate_account->getId()){
                return str_replace('{{customer_id}}', $affiliate_account->getCustomerId().'_aff', $product->getShopNow());
            } else {
                return str_replace('{{customer_id}}','guest',$product->getShopNow());
            }
        }
    }
}