<?php

$installer = $this;
$installer->startSetup();

$installer->run("

    DROP TABLE IF EXISTS {$this->getTable('payoutperiod')};
    CREATE TABLE {$this->getTable('payoutperiod')} (
      `payout_period_id` int(11) unsigned NOT NULL auto_increment,
      `start_date` datetime NULL,
      `end_date` datetime NULL,
      `status` varchar(255) NULL,
      `point_value` decimal(12,2) NULL,
      PRIMARY KEY (`payout_period_id`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

    ");

$installer->endSetup(); 
