<?php

class Trunited_Payoutperiod_Helper_Data extends Mage_Core_Helper_Abstract
{
    const tbl_payout_period = 'tr_PayoutPeriod';

    public function getConfigData($group, $field, $store = null)
    {
        return Mage::getStoreConfig('payoutperiod/' . $group . '/' . $field, $store);
    }

    public function isEnable()
    {
        return $this->getConfigData('point_value_trend', 'enable');
    }

    public function collectData()
    {
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');

        $start_date = (date('Y', time()) - 1) . '-01-01 00:00:00';
        $end_date = (date('Y', time()) - 1) . '-12-31 23:59:59';

        $query = "SELECT * FROM tr_PayoutPeriod WHERE start_date >= '$start_date' AND end_date <= '$end_date' ORDER BY payout_period_id ASC";

        $points = $readConnection->fetchAll($query);

        $result = array();

        if (sizeof($points) > 0) {
            foreach ($points as $dt) {
                $result[] = $dt['point_value'] == null ? 0 : $dt['point_value'];
            }
        } else {
            for ($i = 0; $i < 12; $i++) {
                $result[] = 0;
            }
        }

        return $result;
    }

    public function synchronizeData()
    {
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');
        $writeConnection = $resource->getConnection('core_write');

        $query = "SELECT * FROM tr_PayoutPeriod ORDER BY payout_period_id ASC";

        $points = $readConnection->fetchAll($query);

        $result = array();

        if (sizeof($points) > 0) {
            $query = "";
            $query .= "INSERT INTO `trpayoutperiod`(`payout_period_id`, `start_date`, `end_date`, `status`, `point_value`) VALUES";

            $update = "";

            $flag = 0;
            $is_run_query = false;

            foreach ($points as $dt) {

                $period = Mage::getModel('payoutperiod/payoutperiod')
                    ->getCollection()
                    ->addFieldToFilter('payout_period_id', $dt['payout_period_id'])
                    ->getFirstItem();

                $point_value = $dt['point_value'] == null ? 0 : $dt['point_value'];

                if ($period->getId() != null) {

                    $update .= "UPDATE tr_PayoutPeriod SET `start_date`='" . $dt['start_date'] . "',`end_date`='" . $dt['end_date'] . "',`status`='" . $dt['status'] . "',`point_value`=" . $point_value . " WHERE payout_period_id = " . $dt['payout_period_id'] . "; ";

                } else {
                    $is_run_query = true;

                    $query .= "(" . $dt['payout_period_id'] . ",'" . $dt['start_date'] . "','" . $dt['end_date'] . "','" . $dt['status'] . "'," . $point_value . ")";

                    if ($flag < (sizeof($points) - 1))
                        $query .= ",";
                    else
                        $query .= ";";
                }

                $flag++;
            }

            if ($is_run_query)
                $writeConnection->query($query);

            if ($update != null)
                $writeConnection->query($update);
        }
    }

    public function import()
    {
        $fileName = $_FILES['csv_store']['tmp_name'];
        $csvObject = new Varien_File_Csv();
        $csvData = $csvObject->getData($fileName);
        $import_count = 0;

        $transactionSave = Mage::getModel('core/resource_transaction');
        $connection = Mage::getSingleton('core/resource')->getConnection('core_write');

        try {
            $connection->beginTransaction();

            if (sizeof($csvData) > 0) {
                $current_symbol = Mage::app()->getLocale()->currency(Mage::app()->getStore()->getCurrentCurrencyCode())->getSymbol();

                foreach ($csvData as $csv) {

                    $point_value = str_replace($current_symbol, '', $csv[4]);

                    if (isset($csv[0]) && !filter_var($csv[0], FILTER_VALIDATE_INT) === false
                        && isset($csv[3]) && in_array($csv[3], Trunited_Payoutperiod_Model_Status::getStatusArray())) {

                        $model = Mage::getModel('payoutperiod/payoutperiod')->load($csv[0]);

                        if($model->getId() > 0){

                        } else {
                            $model = Mage::getModel('payoutperiod/payoutperiod');
                        }

                        $model->setData('payout_period_id', $csv[0]);
                        $model->setData('start_date', $csv[1]);
                        $model->setData('end_date', $csv[2]);
                        $model->setData('status', $csv[3]);
                        $model->setData('point_value', $point_value);

                        $transactionSave->addObject($model);
                        $import_count++;
                    }
                }
            }

            $transactionSave->save();
            $connection->commit();
        } catch (Exception $e) {
            $connection->rollback();
            zend_debug::dump($e->getMessage());
            exit;
        }
        return $import_count;
    }
}
