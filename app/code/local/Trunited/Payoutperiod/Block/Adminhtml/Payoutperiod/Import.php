<?php

class Trunited_Payoutperiod_Block_Adminhtml_Payoutperiod_Import extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        parent::__construct();

        $this->_objectId = 'id';
        $this->_blockGroup = 'payoutperiod';
        $this->_controller = 'adminhtml_payoutperiod';
        $this->_mode = 'import';
        $this->_updateButton('save', 'label', Mage::helper('payoutperiod')->__('Import'));

    }

    /**
     * get text to show in header when edit an item
     *
     * @return string
     */
    public function getHeaderText()
    {
        return Mage::helper('payoutperiod')->__('Import Record(s)');
    }
}

?>
