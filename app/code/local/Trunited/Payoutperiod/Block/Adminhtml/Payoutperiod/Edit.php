<?php

class Trunited_Payoutperiod_Block_Adminhtml_Payoutperiod_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
	public function __construct(){
		parent::__construct();
		
		$this->_objectId = 'payout_period_id';
		$this->_blockGroup = 'payoutperiod';
		$this->_controller = 'adminhtml_payoutperiod';
		
		$this->_updateButton('save', 'label', Mage::helper('payoutperiod')->__('Save Payout Period'));
		$this->_updateButton('delete', 'label', Mage::helper('payoutperiod')->__('Delete Payout Period'));
		
		$this->_addButton('saveandcontinue', array(
			'label'		=> Mage::helper('adminhtml')->__('Save And Continue Edit'),
			'onclick'	=> 'saveAndContinueEdit()',
			'class'		=> 'save',
		), -100);

		$this->_formScripts[] = "
			function toggleEditor() {
				if (tinyMCE.getInstanceById('payoutperiod_content') == null)
					tinyMCE.execCommand('mceAddControl', false, 'payoutperiod_content');
				else
					tinyMCE.execCommand('mceRemoveControl', false, 'payoutperiod_content');
			}

			function saveAndContinueEdit(){
				editForm.submit($('edit_form').action+'back/edit/');
			}
		";
	}

	public function getHeaderText(){
		if(Mage::registry('payoutperiod_data') && Mage::registry('payoutperiod_data')->getId())
			return Mage::helper('payoutperiod')->__("Edit Payout Period '%s'", $this->htmlEscape(Mage::registry('payoutperiod_data')->getPayoutPeriodId()));
		return Mage::helper('payoutperiod')->__('Add Payout Period');
	}
}