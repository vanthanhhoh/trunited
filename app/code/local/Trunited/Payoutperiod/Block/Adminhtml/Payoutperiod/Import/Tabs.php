<?php

class Trunited_Payoutperiod_Block_Adminhtml_Payoutperiod_Import_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{

  public function __construct()
  {
      parent::__construct();
      $this->setId('import_tabs');
      $this->setDestElementId('edit_form');
      $this->setTitle(Mage::helper('payoutperiod')->__('Import Record(s)'));
  }

  protected function _beforeToHtml()
  {
      $this->addTab('form_section', array(
          'label'     => Mage::helper('payoutperiod')->__('Import Record(s)'),
          'title'     => Mage::helper('payoutperiod')->__('Import Record(s)'),
          'content'   => $this->getLayout()->createBlock('payoutperiod/adminhtml_payoutperiod_import_tab_form')->toHtml(),
      ));
     
      return parent::_beforeToHtml();
  }
}
