<?php

class Trunited_Payoutperiod_Block_Adminhtml_Payoutperiod_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
	public function __construct(){
		parent::__construct();
		$this->setId('payoutperiod_tabs');
		$this->setDestElementId('edit_form');
		$this->setTitle(Mage::helper('payoutperiod')->__('Payout Period Information'));
	}

	protected function _beforeToHtml(){
		$this->addTab('form_section', array(
			'label'	 => Mage::helper('payoutperiod')->__('Payout Period Information'),
			'title'	 => Mage::helper('payoutperiod')->__('Payout Period Information'),
			'content'	 => $this->getLayout()->createBlock('payoutperiod/adminhtml_payoutperiod_edit_tab_form')->toHtml(),
		));
		return parent::_beforeToHtml();
	}
}