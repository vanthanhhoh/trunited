<?php

class Trunited_Payoutperiod_Block_Adminhtml_Payoutperiod_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('payoutperiodGrid');
        $this->setDefaultSort('payout_period_id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
    }

    protected function _prepareCollection()
    {
        $collection = Mage::getModel('payoutperiod/payoutperiod')->getCollection();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $this->addColumn('payout_period_id', array(
            'header' => Mage::helper('payoutperiod')->__('Payout Period ID'),
            'align' => 'right',
            'width' => '50px',
            'index' => 'payout_period_id',
        ));

        $this->addColumn('start_date', array(
            'header' => Mage::helper('payoutperiod')->__('Start Date'),
            'index' => 'start_date',
            'type' => 'datetime'
        ));

        $this->addColumn('end_date', array(
            'header' => Mage::helper('payoutperiod')->__('End Date'),
            'index' => 'end_date',
            'type' => 'datetime'
        ));

        $this->addColumn('point_value', array(
            'header' => Mage::helper('payoutperiod')->__('Point Value'),
            'index' => 'point_value',
            'type' => 'number'
        ));

        $this->addColumn('status', array(
            'header' => Mage::helper('payoutperiod')->__('Status'),
            'index' => 'status',
            'type'      => 'options',
            'options'   => Trunited_Payoutperiod_Model_Status::getOptionArray(),
        ));

        $this->addColumn('action',
            array(
                'header' => Mage::helper('payoutperiod')->__('Action'),
                'width' => '100',
                'type' => 'action',
                'getter' => 'getId',
                'actions' => array(
                    array(
                        'caption' => Mage::helper('payoutperiod')->__('Edit'),
                        'url' => array('base' => '*/*/edit'),
                        'field' => 'id'
                    )),
                'filter' => false,
                'sortable' => false,
                'index' => 'stores',
                'is_system' => true,
            ));

        $this->addExportType('*/*/exportCsv', Mage::helper('payoutperiod')->__('CSV'));
        $this->addExportType('*/*/exportXml', Mage::helper('payoutperiod')->__('XML'));

        return parent::_prepareColumns();
    }

    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('payout_period_id');
        $this->getMassactionBlock()->setFormFieldName('payoutperiod');

        $this->getMassactionBlock()->addItem('delete', array(
            'label' => Mage::helper('payoutperiod')->__('Delete'),
            'url' => $this->getUrl('*/*/massDelete'),
            'confirm' => Mage::helper('payoutperiod')->__('Are you sure?')
        ));

        $statuses = Mage::getSingleton('payoutperiod/status')->getOptionArray();

        array_unshift($statuses, array('label' => '', 'value' => ''));
        $this->getMassactionBlock()->addItem('status', array(
            'label' => Mage::helper('payoutperiod')->__('Change status'),
            'url' => $this->getUrl('*/*/massStatus', array('_current' => true)),
            'additional' => array(
                'visibility' => array(
                    'name' => 'status',
                    'type' => 'select',
                    'class' => 'required-entry',
                    'label' => Mage::helper('payoutperiod')->__('Status'),
                    'values' => $statuses
                ))
        ));
        return $this;
    }

    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', array('id' => $row->getId()));
    }
}
