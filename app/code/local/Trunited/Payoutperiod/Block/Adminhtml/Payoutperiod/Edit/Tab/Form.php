<?php

class Trunited_Payoutperiod_Block_Adminhtml_Payoutperiod_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
	protected function _prepareForm(){
		$form = new Varien_Data_Form();
		$this->setForm($form);
		
		if (Mage::getSingleton('adminhtml/session')->getPayoutperiodData()){
			$data = Mage::getSingleton('adminhtml/session')->getPayoutperiodData();
			Mage::getSingleton('adminhtml/session')->setPayoutperiodData(null);
		}elseif(Mage::registry('payoutperiod_data'))
			$data = Mage::registry('payoutperiod_data')->getData();
		
		$fieldset = $form->addFieldset('payoutperiod_form', array('legend'=>Mage::helper('payoutperiod')->__('Payout Period information')));

		$dateFormatIso = Mage::app()->getLocale()->getDateFormat(
		    Mage_Core_Model_Locale::FORMAT_TYPE_SHORT
		);

		$fieldset->addField('start_date', 'date', array(
		    'name'      => 'start_date',
		    'label'     => Mage::helper('payoutperiod')->__('Start Date'),
		    'image'     => $this->getSkinUrl('images/grid-cal.gif'),
		    'format'    => $dateFormatIso,
		    'class'     => 'validate-date validate-date-range required-entry'
		));

		$fieldset->addField('end_date', 'date', array(
		    'name'      => 'end_date',
		    'label'     => Mage::helper('payoutperiod')->__('End Date'),
		    'image'     => $this->getSkinUrl('images/grid-cal.gif'),
		    'format'    => $dateFormatIso,
		    'class'     => 'validate-date validate-date-range required-entry'
		));

		$fieldset->addField('status', 'select', array(
			'label'		=> Mage::helper('payoutperiod')->__('Status'),
			'name'		=> 'status',
			'values'	=> Mage::getSingleton('payoutperiod/status')->getOptionHash(),
		));

		$fieldset->addField('point_value', 'text', array(
			'name'		=> 'point_value',
			'label'		=> Mage::helper('payoutperiod')->__('Point Value'),
			'title'		=> Mage::helper('payoutperiod')->__('Point Value'),
		));

		$form->setValues($data);
		return parent::_prepareForm();
	}
}