<?php

class Trunited_Payoutperiod_Block_Adminhtml_Payoutperiod extends Mage_Adminhtml_Block_Widget_Grid_Container
{
	public function __construct(){
		$this->_controller = 'adminhtml_payoutperiod';
		$this->_blockGroup = 'payoutperiod';
		$this->_headerText = Mage::helper('payoutperiod')->__('Payout Period Manager');
		$this->_addButtonLabel = Mage::helper('payoutperiod')->__('Add Payout Period');
		parent::__construct();
		$this->_addButton('import', array(
			'label' => Mage::helper('truwallet')->__('Import Record(s)'),
			'onclick' => "setLocation('" . $this->getUrl('*/*/import', array('page_key' => 'collection')) . "')",
			'class' => 'add'
		));
	}
}