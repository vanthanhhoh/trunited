<?php

class Trunited_Payoutperiod_Block_Trend extends Mage_Core_Block_Template
{
	public function _prepareLayout(){
		return parent::_prepareLayout();
	}

	 public function getCustomerStats()
    {
        return Mage::helper('badges')->getCustomerStats(
            Mage::getSingleton('customer/session')->getCustomer()->getId()
        );
    }

    public function getYtdSaving()
    {
        return Mage::helper('businessdashboard')->getYtdSaving();
    }

    public function getCurrentPointValue()
    {
        $current_point_value = Mage::helper('other')->getConfigData('general', 'current_point_value');
        return Mage::helper('core')->currency($current_point_value,true,false);
    }

    public function getCustomerId()
    {
        return Mage::getSingleton('customer/session')->getCustomer()->getId();
    }

    public function getCurrentCustomer()
    {
        return  Mage::getModel('customer/customer')->load(
            $this->getCustomerId()
        );
    }

    public function getHelper(){
        return Mage::helper('payoutperiod');
    }

    public function getTitleOfChart()
    {
        return $this->getHelper()->getConfigData('point_value_trend', 'chart_title');
    }

    public function getSubtitleOfChart()
    {
        return $this->getHelper()->getConfigData('point_value_trend', 'chart_subtitle');
    }

    public function getTitleOfVerticalLine()
    {
        return $this->getHelper()->getConfigData('point_value_trend', 'vertical_line_title');
    }

    public function getTitleOfHorizontalLine()
    {
        return $this->getHelper()->getConfigData('point_value_trend', 'horizontal_line_title');
    }

    public function getColorOfLine()
    {
        return $this->getHelper()->getConfigData('point_value_trend', 'line_color');
    }

    public function getAreas()
    {
        return unserialize($this->getHelper()->getConfigData('point_value_trend', 'areas'));
    }

    public function collectData()
    {
        $data = $this->getHelper()->collectData();
        return implode(',', $data);
    }
}