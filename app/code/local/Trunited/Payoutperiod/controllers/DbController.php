  <?php

  class Trunited_Payoutperiod_DbController extends Mage_Core_Controller_Front_Action
  {
   public function indexAction(){
    $setup = new Mage_Core_Model_Resource_Setup(null);
    $installer = $setup;
    $installer->startSetup();
    $installer->run("
      DROP TABLE IF EXISTS {$setup->getTable('payoutperiod')};
      CREATE TABLE {$setup->getTable('payoutperiod')} (
        `id` int(11) unsigned NOT NULL auto_increment,
        `payout_period_id` int(11) unsigned NOT NULL,
        `customer_id` int(11) unsigned NOT NULL,
        `name` varchar(255) NULL default '',
        `badge` varchar(255) NULL default '',
        `member_since` datetime NULL,
        `depth_credit` int(11) NULL,
        `frontline` int(11) NULL,
        `affiliate_renewal` datetime NULL,
        `parent_id` int(11) NULL,
        `district` int(11) NULL,
        `personal_points` decimal(12,2) NULL,
        `match_points` decimal(12,2) NULL,
        `guest_commission` decimal(12,2) NULL,
        `next_level_points` decimal(12,2) NULL,
        `unearned_points` decimal(12,2) NULL,
        `efficient_match_points` decimal(12,2) NULL,
        `on_hold_points` decimal(12,2) NULL,
        `trubox_points` decimal(12,2) NULL,
        `social_stock` decimal(12,2) NULL,
        `ytd_rewards` decimal(12,2) NULL,
        `trubox_engagement_pct` decimal(12,2) NULL,
        `sweet15_trubox_engagement_pct` decimal(12,2) NULL,
        `all_trubox_engagement_pct` decimal(12,2) NULL,
        `customer_engagement_pct` decimal(12,2) NULL,
        `sweet15_customer_engagement_pct` decimal(12,2) NULL,
        `all_customer_engagement_pct` decimal(12,2) NULL,
        `ems_score` decimal(12,2) NULL,
        PRIMARY KEY (`id`)
      ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
      ");
    $installer->endSetup();
    echo "success";
  }

  public function updateDbAction()
  {
    $setup = new Mage_Core_Model_Resource_Setup(null);
    $installer = $setup;
    $installer->startSetup();
    $installer->run("
      ALTER TABLE `tr_PayoutPeriod` ADD `point_value` DECIMAL(12,4) NULL;
      ");
    $installer->endSetup();
    echo "success";
  }

  public function updateDb2Action()
  {
    $setup = new Mage_Core_Model_Resource_Setup(null);
    $installer = $setup;
    $installer->startSetup();
    $installer->run("
      DROP TABLE IF EXISTS {$setup->getTable('payoutperiod')};
      CREATE TABLE {$setup->getTable('payoutperiod')} (
        `payout_period_id` int(11) unsigned NOT NULL auto_increment,
        `start_date` datetime NULL,
        `end_date` datetime NULL,
        `status` varchar(255) NULL,
        `point_value` decimal(12,2) NULL,
        PRIMARY KEY (`payout_period_id`)
      ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
    ");
    $installer->endSetup();
    echo "success";
  }
}
