<?php

class Trunited_Payoutperiod_Adminhtml_PayoutperiodController extends Mage_Adminhtml_Controller_Action
{
	protected function _initAction(){
		$this->loadLayout()
			->_setActiveMenu('payoutperiod/payoutperiod')
			->_addBreadcrumb(Mage::helper('adminhtml')->__('Payout Periods Manager'), Mage::helper('adminhtml')->__('Payout Period Manager'));
		return $this;
	}
 
	public function indexAction(){
		Mage::helper('payoutperiod')->synchronizeData();
		$this->_initAction()
			->renderLayout();
	}

	public function editAction() {
		$id	 = $this->getRequest()->getParam('id');
		$model  = Mage::getModel('payoutperiod/payoutperiod')->load($id);

		if ($model->getId() || $id == 0) {
			$data = Mage::getSingleton('adminhtml/session')->getFormData(true);
			if (!empty($data))
				$model->setData($data);

			Mage::register('payoutperiod_data', $model);

			$this->loadLayout();
			$this->_setActiveMenu('payoutperiod/payoutperiod');

			$this->_addBreadcrumb(Mage::helper('adminhtml')->__('Payout Period Manager'), Mage::helper('adminhtml')->__('Payout Period Manager'));
			$this->_addBreadcrumb(Mage::helper('adminhtml')->__('Payout Period News'), Mage::helper('adminhtml')->__('Payout Period News'));

			$this->getLayout()->getBlock('head')->setCanLoadExtJs(true);
			$this->_addContent($this->getLayout()->createBlock('payoutperiod/adminhtml_payoutperiod_edit'))
				->_addLeft($this->getLayout()->createBlock('payoutperiod/adminhtml_payoutperiod_edit_tabs'));

			$this->renderLayout();
		} else {
			Mage::getSingleton('adminhtml/session')->addError(Mage::helper('payoutperiod')->__('Payout Period does not exist'));
			$this->_redirect('*/*/');
		}
	}
 
	public function newAction() {
		$this->_forward('edit');
	}
 
	public function saveAction() {
		if ($data = $this->getRequest()->getPost()) {
			$model = Mage::getModel('payoutperiod/payoutperiod');		
			$model->setData($data)
				->setId($this->getRequest()->getParam('id'));
			
			try {
				$model->save();

				Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('payoutperiod')->__('Payout Period was successfully saved'));
				Mage::getSingleton('adminhtml/session')->setFormData(false);

				if ($this->getRequest()->getParam('back')) {
					$this->_redirect('*/*/edit', array('id' => $model->getId()));
					return;
				}
				$this->_redirect('*/*/');
				return;
			} catch (Exception $e) {
				Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
				Mage::getSingleton('adminhtml/session')->setFormData($data);
				$this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
				return;
			}
		}
		Mage::getSingleton('adminhtml/session')->addError(Mage::helper('payoutperiod')->__('Unable to find Payout Period to save'));
		$this->_redirect('*/*/');
	}
 
	public function deleteAction() {
		if( $this->getRequest()->getParam('id') > 0 ) {
			try {
				$model = Mage::getModel('payoutperiod/payoutperiod');
				$model->setId($this->getRequest()->getParam('id'))
					->delete();
				Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Payout Period was successfully deleted'));
				$this->_redirect('*/*/');
			} catch (Exception $e) {
				Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
				$this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
			}
		}
		$this->_redirect('*/*/');
	}

	public function massDeleteAction() {
		$payoutperiodIds = $this->getRequest()->getParam('payoutperiod');
		if(!is_array($payoutperiodIds)){
			Mage::getSingleton('adminhtml/session')->addError(Mage::helper('adminhtml')->__('Please select item(s)'));
		}else{
			try {
				foreach ($payoutperiodIds as $payoutperiodId) {
					$payoutperiod = Mage::getModel('payoutperiod/payoutperiod')->load($payoutperiodId);
					$payoutperiod->delete();
				}
				Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Total of %d record(s) were successfully deleted', count($payoutperiodIds)));
			} catch (Exception $e) {
				Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
			}
		}
		$this->_redirect('*/*/index');
	}
	
	public function massStatusAction() {
		$payoutperiodIds = $this->getRequest()->getParam('payoutperiod');
		if(!is_array($payoutperiodIds)) {
			Mage::getSingleton('adminhtml/session')->addError($this->__('Please select payout period(s)'));
		} else {
			try {
				foreach ($payoutperiodIds as $payoutperiodId) {
					$payoutperiod = Mage::getSingleton('payoutperiod/payoutperiod')
						->load($payoutperiodId)
						->setStatus($this->getRequest()->getParam('status'))
						->setIsMassupdate(true)
						->save();
				}
				$this->_getSession()->addSuccess(
					$this->__('Total of %d record(s) were successfully updated', count($payoutperiodIds))
				);
			} catch (Exception $e) {
				$this->_getSession()->addError($e->getMessage());
			}
		}
		$this->_redirect('*/*/index');
	}
  
	public function exportCsvAction(){
		$fileName   = 'payoutperiod.csv';
		$content	= $this->getLayout()->createBlock('payoutperiod/adminhtml_payoutperiod_grid')->getCsv();
		$this->_prepareDownloadResponse($fileName,$content);
	}

	public function exportXmlAction(){
		$fileName   = 'payoutperiod.xml';
		$content	= $this->getLayout()->createBlock('payoutperiod/adminhtml_payoutperiod_grid')->getXml();
		$this->_prepareDownloadResponse($fileName,$content);
	}

	public function importAction() {
		$this->loadLayout();
		$this->_setActiveMenu('payoutperiod/payoutperiod');

		$this->_addBreadcrumb(Mage::helper('adminhtml')->__('Import Record(s)'), Mage::helper('adminhtml')->__('Import Record(s)'));
		$this->_addBreadcrumb(Mage::helper('adminhtml')->__('Import Record(s)'), Mage::helper('adminhtml')->__('Import Record(s)'));

		$this->getLayout()->getBlock('head')->setCanLoadExtJs(true);
		$editBlock = $this->getLayout()->createBlock('payoutperiod/adminhtml_payoutperiod_import');
		$editBlock->removeButton('delete');
		$editBlock->removeButton('saveandcontinue');
		$editBlock->removeButton('reset');
		$editBlock->updateButton('back', 'onclick', 'setLocation(\'' . $this->getUrl('*/*/') . '\')');
		$editBlock->setData('form_action_url', $this->getUrl('*/*/importSave', array()));

		$this->_addContent($editBlock)
			->_addLeft($this->getLayout()->createBlock('payoutperiod/adminhtml_payoutperiod_import_tabs'));

		$this->renderLayout();
	}

	public function importSaveAction() {

		if (!empty($_FILES['csv_store']['tmp_name'])) {
			try {
				$number = Mage::helper('payoutperiod')->import();
				Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('payoutperiod')->__('You\'ve successfully imported ') . $number . Mage::helper('payoutperiod')->__(' record(s)'));
			} catch (Mage_Core_Exception $e) {
				Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
			} catch (Exception $e) {
				Mage::getSingleton('adminhtml/session')->addError(Mage::helper('payoutperiod')->__('Invalid file upload attempt'));
			}
			$this->_redirect('*/*/');
		} else {
			Mage::getSingleton('adminhtml/session')->addError(Mage::helper('payoutperiod')->__('Invalid file upload attempt'));
			$this->_redirect('*/*/importstore');
		}

	}
}
