<?php

class Trunited_Payoutperiod_Model_Payoutperiod extends Mage_Core_Model_Abstract
{
    const tbl_payout_period = 'tr_PayoutPeriod';

	public function _construct(){
		parent::_construct();
		$this->_init('payoutperiod/payoutperiod');
	}

	public function _afterDelete()
    {
        parent::_afterDelete();

        $resource = Mage::getSingleton('core/resource');
        $writeConnection = $resource->getConnection('core_write');
        $delete = "DELETE FROM ".self::tbl_payout_period." WHERE `payout_period_id` = ".$this->getId();
        $writeConnection->query($delete);
    }

    public function _afterSave()
    {
    	parent::_afterSave();

    	$resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');
        $writeConnection = $resource->getConnection('core_write');

        $query = "SELECT payout_period_id FROM ".self::tbl_payout_period." WHERE payout_period_id = ".$this->getId()." ORDER BY payout_period_id DESC LIMIT 1";

        $exist = $readConnection->fetchCol($query);

        $start_date = date('Y-m-d 00:00:00', strtotime($this->getStartDate()));
        $end_date = date('Y-m-d 23:59:59', strtotime($this->getEndDate()));

        if($exist != null && sizeof($exist) > 0){
        	$update = "UPDATE ".self::tbl_payout_period." SET `start_date`='".$start_date."',`end_date`='".$end_date."',`status`='".$this->getStatus()."',`point_value`=".$this->getPointValue()." WHERE payout_period_id = ".$this->getId();
        	
        	$writeConnection->query($update);
        } else {
        	$insert = "INSERT INTO ".self::tbl_payout_period." (`payout_period_id`, `start_date`, `end_date`, `status`, `point_value`) VALUES (".$this->getId().",'".$start_date."','".$end_date."','".$this->getStatus()."',".$this->getPointValue().")";

        	$writeConnection->query($insert);
        }
    }
}