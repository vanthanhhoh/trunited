<?php

class Trunited_Payoutperiod_Model_Status extends Varien_Object
{
	const STATUS_OPEN	= 'Open';
	const STATUS_CLOSED	= 'Closed';

	static public function getOptionArray(){
		return array(
			self::STATUS_OPEN	=> Mage::helper('payoutperiod')->__('Open'),
			self::STATUS_CLOSED   => Mage::helper('payoutperiod')->__('Closed')
		);
	}
	
	static public function getOptionHash(){
		$options = array();
		foreach (self::getOptionArray() as $value => $label)
			$options[] = array(
				'value'	=> $value,
				'label'	=> $label
			);
		return $options;
	}

	static public function getStatusArray()
    {
        return array(
            self::STATUS_OPEN,
            self::STATUS_CLOSED,
        );
    }
}
