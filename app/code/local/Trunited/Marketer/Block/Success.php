<?php

class Trunited_Marketer_Block_Success extends Mage_Core_Block_Template
{
	protected $_helper;

	public function _prepareLayout(){
		return parent::_prepareLayout();
	}

	public function getHelper()
	{
		if($this->_helper == null){
			$this->_helper = Mage::helper('marketer');
		}

		return $this->_helper;
	}

	public function getTitle()
	{
		return $this->getHelper()->getConfigData('general', 'title');
	}

	public function getHomeUrl()
	{
		return Mage::getBaseUrl();
	}

	public function getSendEmailUrl()
	{
		return Mage::getUrl('*/success/send');
	}
}