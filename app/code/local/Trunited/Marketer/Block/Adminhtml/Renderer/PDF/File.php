<?php

/**
 * Created by PhpStorm.
 * User: anthony
 * Date: 1/17/17
 * Time: 2:16 PM
 */
class Trunited_Marketer_Block_Adminhtml_Renderer_PDF_File extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {
        $value = $row->getData();

        $link = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA).'marketer' . DS . 'individual' . DS . $value['pdf_file'];

        return "<a href='$link' target='_blank'>File</a>";
    }

}