<?php

/**
 * Created by PhpStorm.
 * User: anthony
 * Date: 1/17/17
 * Time: 2:16 PM
 */
class Trunited_Marketer_Block_Adminhtml_Renderer_Member_Information extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {
        $value = $row->getData();

        $customers = json_decode($value['additional_member'], true);

        $html = '';
        if(sizeof($customers) > 0){
            foreach ($customers as $customer) {
                $html .= 'Name: '.$customer['name_member'].'<br />';
                $html .= 'Address: '.$customer['address_member'].'<br />';
                $html .= 'City: '.$customer['city_member'].'<br />';
                $html .= 'State: '.$customer['state_member'].'<br />';
                $html .= 'Zip: '.$customer['postcode_member'].'<br />';
                $html .= 'Phone: '.$customer['phone_member'].'<br />';
                $html .= '<hr />';
            }

        }

        return $html;
    }

}