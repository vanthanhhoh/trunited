<?php

/**
 * Created by PhpStorm.
 * User: anthony
 * Date: 1/17/17
 * Time: 2:16 PM
 */
class Trunited_Marketer_Block_Adminhtml_Renderer_PDF_Addition extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {
        $value = $row->getData();

        $html = '';

        $additions = json_decode($value['additional_pdf'], true);
        if(sizeof($additions) > 0){
            foreach ($additions as $add) {
                $href = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA).'marketer' . DS . 'email' . DS . $add['link'];
                $html .= "<a href='$href' target='_blank'>".$add['name']."</a>";
                $html .= "<br />";
            }
        }
        return $html;
    }

}