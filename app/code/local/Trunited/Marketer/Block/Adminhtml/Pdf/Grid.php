<?php

class Trunited_Marketer_Block_Adminhtml_Pdf_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
	public function __construct(){
		parent::__construct();
		$this->setId('marketer_pdfGrid');
		$this->setDefaultSort('pdf_id');
		$this->setDefaultDir('DESC');
		$this->setSaveParametersInSession(true);
	}

	protected function _prepareCollection(){
		$collection = Mage::getModel('marketer/pdf')->getCollection();
		$this->setCollection($collection);
		return parent::_prepareCollection();
	}

	protected function _prepareColumns(){
		$this->addColumn('pdf_id', array(
			'header'	=> Mage::helper('marketer')->__('ID'),
			'align'	 =>'right',
			'width'	 => '50px',
			'index'	 => 'pdf_id',
		));

		$this->addColumn('name', array(
			'header'	=> Mage::helper('marketer')->__('Name'),
			'align'	 =>'left',
			'index'	 => 'name',
		));

		$this->addColumn('filename', array(
			'header'	=> Mage::helper('marketer')->__('Link'),
			'index'	 => 'filename',
			'renderer' => 'Trunited_Marketer_Block_Adminhtml_Renderer_PDF_Link',
		));

		$this->addColumn('status', array(
			'header'	=> Mage::helper('marketer')->__('Status'),
			'align'	 => 'left',
			'width'	 => '80px',
			'index'	 => 'status',
			'type'		=> 'options',
			'options'	 => array(
				1 => 'Enabled',
				2 => 'Disabled',
			),
		));

		$this->addColumn('action',
			array(
				'header'	=>	Mage::helper('marketer')->__('Action'),
				'width'		=> '100',
				'type'		=> 'action',
				'getter'	=> 'getId',
				'actions'	=> array(
					array(
						'caption'	=> Mage::helper('marketer')->__('Edit'),
						'url'		=> array('base'=> '*/*/edit'),
						'field'		=> 'id'
					)),
				'filter'	=> false,
				'sortable'	=> false,
				'index'		=> 'stores',
				'is_system'	=> true,
		));

		$this->addExportType('*/*/exportCsv', Mage::helper('marketer')->__('CSV'));
		$this->addExportType('*/*/exportXml', Mage::helper('marketer')->__('XML'));

		return parent::_prepareColumns();
	}

	protected function _prepareMassaction(){
		$this->setMassactionIdField('pdf_id');
		$this->getMassactionBlock()->setFormFieldName('pdf');

		$this->getMassactionBlock()->addItem('delete', array(
			'label'		=> Mage::helper('marketer')->__('Delete'),
			'url'		=> $this->getUrl('*/*/massDelete'),
			'confirm'	=> Mage::helper('marketer')->__('Are you sure?')
		));

		$statuses = Mage::getSingleton('marketer/status')->getOptionArray();

		array_unshift($statuses, array('label'=>'', 'value'=>''));
		$this->getMassactionBlock()->addItem('status', array(
			'label'=> Mage::helper('marketer')->__('Change status'),
			'url'	=> $this->getUrl('*/*/massStatus', array('_current'=>true)),
			'additional' => array(
				'visibility' => array(
					'name'	=> 'status',
					'type'	=> 'select',
					'class'	=> 'required-entry',
					'label'	=> Mage::helper('marketer')->__('Status'),
					'values'=> $statuses
				))
		));
		return $this;
	}

	public function getRowUrl($row){
		return $this->getUrl('*/*/edit', array('id' => $row->getId()));
	}
}