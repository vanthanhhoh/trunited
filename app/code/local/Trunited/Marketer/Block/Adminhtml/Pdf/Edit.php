<?php

class Trunited_Marketer_Block_Adminhtml_Pdf_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
	public function __construct(){
		parent::__construct();
		
		$this->_objectId = 'id';
		$this->_blockGroup = 'marketer';
		$this->_controller = 'adminhtml_pdf';
		
		$this->_updateButton('save', 'label', Mage::helper('marketer')->__('Save PDF'));
		$this->_updateButton('delete', 'label', Mage::helper('marketer')->__('Delete PDF'));
		
		$this->_addButton('saveandcontinue', array(
			'label'		=> Mage::helper('adminhtml')->__('Save And Continue Edit'),
			'onclick'	=> 'saveAndContinueEdit()',
			'class'		=> 'save',
		), -100);

		$this->_formScripts[] = "
			function toggleEditor() {
				if (tinyMCE.getInstanceById('marketer_content') == null)
					tinyMCE.execCommand('mceAddControl', false, 'marketer_content');
				else
					tinyMCE.execCommand('mceRemoveControl', false, 'marketer_content');
			}

			function saveAndContinueEdit(){
				editForm.submit($('edit_form').action+'back/edit/');
			}
		";
	}

	public function getHeaderText(){
		if(Mage::registry('pdf_data') && Mage::registry('pdf_data')->getId())
			return Mage::helper('marketer')->__("Edit Item '%s'", $this->htmlEscape(Mage::registry('pdf_data')->getName()));
		return Mage::helper('marketer')->__('Add PDF');
	}
}