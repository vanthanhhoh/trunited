<?php

class Trunited_Marketer_Block_Adminhtml_Pdf_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
	protected function _prepareForm(){
		$form = new Varien_Data_Form();
		$this->setForm($form);
		
		if (Mage::getSingleton('adminhtml/session')->getPdfData()){
			$data = Mage::getSingleton('adminhtml/session')->getPdfData();
			Mage::getSingleton('adminhtml/session')->setPdfData(null);
		}elseif(Mage::registry('pdf_data'))
			$data = Mage::registry('pdf_data')->getData();
		
		$fieldset = $form->addFieldset('pdf_form', array('legend'=>Mage::helper('marketer')->__('Email PDF information')));

		$fieldset->addField('name', 'text', array(
			'label'		=> Mage::helper('marketer')->__('Name'),
			'class'		=> 'required-entry',
			'required'	=> true,
			'name'		=> 'name',
		));

		$preview_link = '';
		if(isset($data['filename']) && $data['filename'] != ''){
			$file_name = $data['filename'];
			$link = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA).'marketer'. DS . 'email' . DS . $data['filename'];
			$preview_link .= '<a href="'.$link.'" target="_blank">'.$file_name.'</a>';
		}
		$fieldset->addField('filename', 'file', array(
			'label'		=> Mage::helper('marketer')->__('File'),
			'required'	=> false,
			'name'		=> 'filename',
			'after_element_html' => $preview_link,
		));

		$fieldset->addField('status', 'select', array(
			'label'		=> Mage::helper('marketer')->__('Is it sent to emails?'),
			'name'		=> 'status',
			'values'	=> Mage::getSingleton('marketer/status')->getOptionHash(),
		));

		$form->setValues($data);
		return parent::_prepareForm();
	}
}