<?php

class Trunited_Marketer_Block_Adminhtml_Marketer_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
	public function __construct(){
		parent::__construct();
		$this->setId('marketer_tabs');
		$this->setDestElementId('edit_form');
		$this->setTitle(Mage::helper('marketer')->__('Item Information'));
	}

	protected function _beforeToHtml(){
		$this->addTab('form_section', array(
			'label'	 => Mage::helper('marketer')->__('Item Information'),
			'title'	 => Mage::helper('marketer')->__('Item Information'),
			'content'	 => $this->getLayout()->createBlock('marketer/adminhtml_marketer_edit_tab_form')->toHtml(),
		));
		return parent::_beforeToHtml();
	}
}