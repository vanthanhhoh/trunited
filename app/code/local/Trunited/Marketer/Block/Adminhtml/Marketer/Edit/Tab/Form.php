<?php

class Trunited_Marketer_Block_Adminhtml_Marketer_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
	protected function _prepareForm(){
		$form = new Varien_Data_Form();
		$this->setForm($form);
		
		if (Mage::getSingleton('adminhtml/session')->getMarketerData()){
			$data = Mage::getSingleton('adminhtml/session')->getMarketerData();
			Mage::getSingleton('adminhtml/session')->setMarketerData(null);
		}elseif(Mage::registry('marketer_data'))
			$data = Mage::registry('marketer_data')->getData();
		
		$fieldset = $form->addFieldset('marketer_form', array('legend'=>Mage::helper('marketer')->__('Item information')));

		$fieldset->addField('title', 'text', array(
			'label'		=> Mage::helper('marketer')->__('Title'),
			'class'		=> 'required-entry',
			'required'	=> true,
			'name'		=> 'title',
		));

		$fieldset->addField('filename', 'file', array(
			'label'		=> Mage::helper('marketer')->__('File'),
			'required'	=> false,
			'name'		=> 'filename',
		));

		$fieldset->addField('status', 'select', array(
			'label'		=> Mage::helper('marketer')->__('Status'),
			'name'		=> 'status',
			'values'	=> Mage::getSingleton('marketer/status')->getOptionHash(),
		));

		$fieldset->addField('content', 'editor', array(
			'name'		=> 'content',
			'label'		=> Mage::helper('marketer')->__('Content'),
			'title'		=> Mage::helper('marketer')->__('Content'),
			'style'		=> 'width:700px; height:500px;',
			'wysiwyg'	=> false,
			'required'	=> true,
		));

		$form->setValues($data);
		return parent::_prepareForm();
	}
}