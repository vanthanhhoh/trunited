<?php

class Trunited_Marketer_Block_Adminhtml_Marketer_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
	public function __construct(){
		parent::__construct();
		
		$this->_objectId = 'id';
		$this->_blockGroup = 'marketer';
		$this->_controller = 'adminhtml_marketer';
		
		$this->_updateButton('save', 'label', Mage::helper('marketer')->__('Save Item'));
		$this->_updateButton('delete', 'label', Mage::helper('marketer')->__('Delete Item'));
		
		$this->_addButton('saveandcontinue', array(
			'label'		=> Mage::helper('adminhtml')->__('Save And Continue Edit'),
			'onclick'	=> 'saveAndContinueEdit()',
			'class'		=> 'save',
		), -100);

		$this->_formScripts[] = "
			function toggleEditor() {
				if (tinyMCE.getInstanceById('marketer_content') == null)
					tinyMCE.execCommand('mceAddControl', false, 'marketer_content');
				else
					tinyMCE.execCommand('mceRemoveControl', false, 'marketer_content');
			}

			function saveAndContinueEdit(){
				editForm.submit($('edit_form').action+'back/edit/');
			}
		";
	}

	public function getHeaderText(){
		if(Mage::registry('marketer_data') && Mage::registry('marketer_data')->getId())
			return Mage::helper('marketer')->__("Edit Item '%s'", $this->htmlEscape(Mage::registry('marketer_data')->getTitle()));
		return Mage::helper('marketer')->__('Add Item');
	}
}