<?php

class Trunited_Marketer_Block_Adminhtml_Pdf extends Mage_Adminhtml_Block_Widget_Grid_Container
{
	public function __construct(){
		$this->_controller = 'adminhtml_pdf';
		$this->_blockGroup = 'marketer';
		$this->_headerText = Mage::helper('marketer')->__('Email PDF Manager');
		$this->_addButtonLabel = Mage::helper('marketer')->__('Add Email PDF');
		parent::__construct();
	}
}