<?php

class Trunited_Marketer_Block_Adminhtml_Business_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('businessGrid');
        $this->setDefaultSort('business_id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
    }

    protected function _prepareCollection()
    {
        $collection = Mage::getModel('marketer/business')
            ->getCollection()
            ->addCustomerNameToSelect()
            ->addCustomerEmailToSelect()
        ;
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $this->addColumn('business_id', array(
            'header' => Mage::helper('marketer')->__('ID'),
            'align' => 'right',
            'width' => '50px',
            'index' => 'business_id',
        ));

        $this->addColumn('customer_id', array(
            'header' => Mage::helper('marketer')->__('Customer ID'),
            'align' => 'left',
            'width' => '50px',
            'index' => 'customer_id',
        ));

        $this->addColumn('customer_name', array(
            'header' => Mage::helper('marketer')->__('Customer Name'),
            'align' => 'left',
            'width' => '150px',
            'index' => 'customer_name',
            'filter_condition_callback' => array($this, '_customerNameCondition'),
        ));

        $this->addColumn('customer_email', array(
            'header' => Mage::helper('marketer')->__('Customer Email'),
            'align' => 'left',
            'index' => 'customer_email',
            'filter_index'  => 'customer_entity.email'
        ));

        $this->addColumn('business_name', array(
            'header' => Mage::helper('marketer')->__('Business Name'),
            'width' => '100px',
            'index' => 'business_name',
        ));

        $this->addColumn('state_incorporation', array(
            'header' => Mage::helper('marketer')->__('State Incorporation'),
            'index' => 'state_incorporation',
            'width' => '150px',
            'type' => 'options',
            'options' => Trunited_Marketer_Model_Status::getStateOption(),
        ));

        $this->addColumn('type_tin', array(
            'header' => Mage::helper('marketer')->__('SSN or TIN'),
            'align' => 'left',
            'width' => '80px',
            'index' => 'type_tin',
            'type' => 'options',
            'options' => Trunited_Marketer_Model_Status::getTINOptionArray(),
        ));

        $this->addColumn('number_tin', array(
            'header' => Mage::helper('marketer')->__('Number'),
            'width' => '100px',
            'index' => 'number_tin',
        ));

        $this->addColumn('management', array(
            'header' => Mage::helper('marketer')->__('Management'),
            'align' => 'left',
            'width' => '80px',
            'index' => 'management',
            'type' => 'options',
            'options' => Trunited_Marketer_Model_Status::getManagementLLCOptionArray(),
        ));

        $this->addColumn('type_llc', array(
            'header' => Mage::helper('marketer')->__('Type of LLC'),
            'align' => 'left',
            'width' => '80px',
            'index' => 'type_llc',
            'type' => 'options',
            'options' => Trunited_Marketer_Model_Status::getTypeLLCOptionArray(),
        ));

        $this->addColumn('primary_address', array(
            'header' => Mage::helper('marketer')->__('Address'),
            'index' => 'primary_address',
        ));

        $this->addColumn('primary_city', array(
            'header' => Mage::helper('marketer')->__('City'),
            'index' => 'primary_city',
            'width' => '100px',
        ));

        $this->addColumn('primary_state', array(
            'header' => Mage::helper('marketer')->__('State'),
            'index' => 'primary_state',
            'width' => '150px',
            'type' => 'options',
            'options' => Trunited_Marketer_Model_Status::getStateOption(),
        ));

        $this->addColumn('primary_postcode', array(
            'header' => Mage::helper('marketer')->__('Zip'),
            'index' => 'primary_postcode',
            'width' => '80px',
        ));

        $this->addColumn('primary_phone', array(
            'header' => Mage::helper('marketer')->__('Phone'),
            'index' => 'primary_phone',
            'width' => '80px',
        ));

        $this->addColumn('signature_image', array(
            'header' => Mage::helper('marketer')->__('Signature'),
            'index' => 'signature_image',
            'width' => '50px',
            'renderer' => 'Trunited_Marketer_Block_Adminhtml_Renderer_Signature_Image',
        ));

        $this->addColumn('pdf_file', array(
            'header' => Mage::helper('marketer')->__('PDF'),
            'index' => 'pdf_file',
            'width' => '50px',
            'renderer' => 'Trunited_Marketer_Block_Adminhtml_Renderer_PDF_File',
        ));

        $this->addColumn('additional_pdf', array(
            'header' => Mage::helper('marketer')->__('Additional PDFs'),
            'index' => 'additional_pdf',
            'width' => '50px',
            'renderer' => 'Trunited_Marketer_Block_Adminhtml_Renderer_PDF_Addition',
        ));

        $this->addColumn('additional_member', array(
            'header' => Mage::helper('marketer')->__('Additional Member'),
            'index' => 'additional_member',
            'width' => '500px',
            'renderer' => 'Trunited_Marketer_Block_Adminhtml_Renderer_Member_Information',
        ));

        $this->addColumn('created_at', array(
            'header' => Mage::helper('marketer')->__('Created At'),
            'align' => 'left',
            'index' => 'created_at',
            'type' => 'date'
        ));

        $this->addColumn('updated_at', array(
            'header' => Mage::helper('marketer')->__('Updated At'),
            'align' => 'left',
            'index' => 'updated_at',
            'type' => 'date'
        ));


        $this->addExportType('*/*/exportCsv', Mage::helper('marketer')->__('CSV'));
        $this->addExportType('*/*/exportXml', Mage::helper('marketer')->__('XML'));

        return parent::_prepareColumns();
    }


    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('business_id');
        $this->getMassactionBlock()->setFormFieldName('business');

        $this->getMassactionBlock()->addItem('delete', array(
            'label' => Mage::helper('marketer')->__('Delete'),
            'url' => $this->getUrl('*/*/massDelete'),
            'confirm' => Mage::helper('marketer')->__('Are you sure?')
        ));

        return $this;
    }

    public function getRowUrl($row)
    {
        return '';
    }

    protected function _customerNameCondition($collection, $column)
    {
        if (!$value = trim($column->getFilter()->getValue())) {
            return $this;
        }

        if (!empty($value)) {
            $this->getCollection()->filterCustomerName($value);
        }

        return $this;
    }
}