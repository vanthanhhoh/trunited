<?php

class Trunited_Marketer_Block_Adminhtml_Individual extends Mage_Adminhtml_Block_Widget_Grid_Container
{
	public function __construct(){
		$this->_controller = 'adminhtml_individual';
		$this->_blockGroup = 'marketer';
		$this->_headerText = Mage::helper('marketer')->__('Individual History');
		$this->_addButtonLabel = Mage::helper('marketer')->__('Add Item');
		parent::__construct();
		$this->_removeButton('add');
	}
}