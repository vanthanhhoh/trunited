<?php

class Trunited_Marketer_Block_Adminhtml_Marketer extends Mage_Adminhtml_Block_Widget_Grid_Container
{
	public function __construct(){
		$this->_controller = 'adminhtml_marketer';
		$this->_blockGroup = 'marketer';
		$this->_headerText = Mage::helper('marketer')->__('Item Manager');
		$this->_addButtonLabel = Mage::helper('marketer')->__('Add Item');
		parent::__construct();
	}
}