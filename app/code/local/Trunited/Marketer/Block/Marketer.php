<?php

class Trunited_Marketer_Block_Marketer extends Mage_Core_Block_Template
{
    protected $_helper;

    public function _prepareLayout()
    {
        return parent::_prepareLayout();
    }

    public function getHelper()
    {
        if ($this->_helper == null) {
            $this->_helper = Mage::helper('marketer');
        }

        return $this->_helper;
    }

    public function getTitle()
    {
        return $this->getHelper()->getConfigData('general', 'title');
    }

    public function getRegions()
    {
        return Mage::getModel('directory/country')->load('US')->getRegions();
    }

    public function getTINOptions()
    {
        return Trunited_Marketer_Model_Status::getTINOptionArray();
    }

    public function getBackgroundSignature()
    {
        return $this->getHelper()->getConfigData('signature', 'background');
    }

    public function getColourSignature()
    {
        return $this->getHelper()->getConfigData('signature', 'signature_line');
    }

    public function getThicknessSignature()
    {
        return $this->getHelper()->getConfigData('signature', 'thickness');
    }

    public function getGuidelineSignature()
    {
        return $this->getHelper()->getConfigData('signature', 'guideline');
    }

    public function getGuidelineColourSignature()
    {
        return $this->getHelper()->getConfigData('signature', 'guideline_color');
    }

    public function getGuidelineOffsetSignature()
    {
        return $this->getHelper()->getConfigData('signature', 'guideline_offset');
    }

    public function getGuidelineIndentSignature()
    {
        return $this->getHelper()->getConfigData('signature', 'guideline_indent');
    }

    public function isEnableTermsIndividual()
    {
        return $this->getHelper()->getConfigData('general', 'enable_terms_individual');
    }

    public function getIndividualTerms()
    {
        $identifier = $this->getHelper()->getConfigData('general', 'individual_terms');
        if ($identifier != '') {
            $staticBlock = Mage::getModel('cms/block')->load($identifier);
            if ($staticBlock != null && $staticBlock->getId()) {
                return $staticBlock->getContent();
            }
        }

        return '';
    }

    public function isEnableTermsBusiness()
    {
        return $this->getHelper()->getConfigData('general', 'enable_terms_business');
    }

    public function getBusinessTerms()
    {
        $identifier = $this->getHelper()->getConfigData('general', 'business_terms');
        if ($identifier != '') {
            $staticBlock = Mage::getModel('cms/block')->load($identifier);
            if ($staticBlock != null && $staticBlock->getId()) {
                return $staticBlock->getContent();
            }
        }

        return '';
    }

    public function getIndividualAction()
    {
        return Mage::getUrl('*/index/individual');
    }

    public function getBusinessAction()
    {
        return Mage::getUrl('*/index/business');
    }

    public function getDefaultBillingAddress()
    {
        $customerAddressId = Mage::getSingleton('customer/session')->getCustomer()->getDefaultBilling();
        if ($customerAddressId) {
            $address = Mage::getModel('customer/address')->load($customerAddressId);
            return $address;
        }

        return null;
    }

    public function getDefaultName()
    {
        return Mage::getSingleton('customer/session')->getCustomer()->getName();
    }

    public function getDefaultPhone()
    {
        $default_address = $this->getDefaultBillingAddress();
        if($default_address != null){
            $_tel = str_split($default_address->getTelephone());
            return '('.$_tel[0].$_tel[1].$_tel[2].') '.$_tel[3].$_tel[4].$_tel[5].'-'.$_tel[6].$_tel[7].$_tel[8];
        }

        return '';
    }

    public function getTypeOfBusiness()
    {
        return Trunited_Marketer_Model_Status::getTypeBusinessOptionArray();
    }

    public function getTypeOfLLC()
    {
        return Trunited_Marketer_Model_Status::getTypeLLCOptionArray();
    }

    public function getManagementLLC()
    {
        return Trunited_Marketer_Model_Status::getManagementLLCOptionArray();
    }
}