<?php

class Trunited_Marketer_SuccessController extends Mage_Core_Controller_Front_Action
{
	/**
	 * check customer is logged in
	 */
	public function preDispatch()
	{
		parent::preDispatch();
		if (!$this->getRequest()->isDispatched()) {
			return;
		}
		$action = $this->getRequest()->getActionName();
		if ($action != 'policy' && $action != 'redirectLogin') {
			// Check customer authentication
			if (!Mage::getSingleton('customer/session')->isLoggedIn()) {
				Mage::getSingleton('customer/session')->setAfterAuthUrl(
					Mage::getUrl($this->getFullActionName('/'))
				);
				$this->_redirectUrl(Mage::getUrl('customer/account/login'));
				$this->setFlag('', Mage_Core_Controller_Varien_Action::FLAG_NO_DISPATCH, true);
			}
		}
	}

	public function indexAction(){

		$this->loadLayout();

		$this->_title(Mage::helper('marketer')->__('Marketer Central Agreement'));

		$breadcrumbs = $this->getLayout()->getBlock("breadcrumbs");

		$breadcrumbs->addCrumb("home", array(
			"label" => $this->__("Home"),
			"title" => $this->__("Home"),
			"link"  => Mage::getBaseUrl()
		));

		$breadcrumbs->addCrumb("marketer", array(
			"label" => $this->__("Marketer Agreement"),
			"title" => $this->__("Marketer Agreement"),
			"link"  => Mage::getUrl('marketer-agreement.html')
		));

		$breadcrumbs->addCrumb("marketer_success", array(
			"label" => $this->__("Success"),
			"title" => $this->__("Success")
		));

		$this->renderLayout();
	}

	public function sendAction()
	{
		$flag = Mage::getSingleton('core/session')->getMarketerFlag();
		if($flag != null && $flag == 1){
			Mage::helper('marketer')->sendEmail(
				Mage::getModel('customer/customer')->load(
					Mage::getSingleton('customer/session')->getCustomer()->getId()
				),
				Mage::getSingleton('core/session')->getMarketer(),
				Mage::getSingleton('core/session')->getMarketerType() == 1 ? 'individual' : 'business'
			);
		}
		Mage::getSingleton('core/session')->setMarketerFlag(2);
	}
}