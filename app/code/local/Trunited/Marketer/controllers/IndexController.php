<?php

use Symfony\Component\Config\Definition\Exception\Exception;

class Trunited_Marketer_IndexController extends Mage_Core_Controller_Front_Action
{
	/**
	 * check customer is logged in
	 */
	public function preDispatch()
	{
		parent::preDispatch();
		if (!$this->getRequest()->isDispatched()) {
			return;
		}
		$action = $this->getRequest()->getActionName();
		if ($action != 'policy' && $action != 'redirectLogin') {
			// Check customer authentication
			if (!Mage::getSingleton('customer/session')->isLoggedIn()) {
				Mage::getSingleton('customer/session')->setAfterAuthUrl(
					Mage::getUrl($this->getFullActionName('/'))
				);
				$this->_redirectUrl(Mage::getUrl('customer/account/login'));
				$this->setFlag('', Mage_Core_Controller_Varien_Action::FLAG_NO_DISPATCH, true);
			}
		}
	}

	public function indexAction(){

		$is_register = Mage::helper('marketer')->isRegister(
			Mage::getSingleton('customer/session')->getCustomer()->getId()
		);

		if($is_register)
		{
			Mage::getSingleton('core/session')->addNotice(
				Mage::helper('marketer')->getConfigData('general', 'notice_success')
			);
			$this->_redirectUrl(Mage::getBaseUrl());
			return;
		}

		$this->loadLayout();

		$this->_title(Mage::helper('marketer')->__('Marketer Central Agreement'));

		$breadcrumbs = $this->getLayout()->getBlock("breadcrumbs");

		$breadcrumbs->addCrumb("home", array(
			"label" => $this->__("Home"),
			"title" => $this->__("Home"),
			"link"  => Mage::getBaseUrl()
		));

		$breadcrumbs->addCrumb("marketer", array(
			"label" => $this->__("Marketer Agreement"),
			"title" => $this->__("Marketer Agreement"),
		));

		$this->renderLayout();
	}

	public function updateDbAction()
	{
		$setup = new Mage_Core_Model_Resource_Setup(null);
		$installer = $setup;
		$installer->startSetup();
		$installer->run("

			DROP TABLE IF EXISTS {$setup->getTable('marketer/individual')};
			CREATE TABLE {$setup->getTable('marketer/individual')} (
			  `individual_id` int(11) unsigned NOT NULL auto_increment,
			  `customer_id` int(11) unsigned NOT NULL,
			  `name` varchar(255) NOT NULL default '',
			  `type_tin` smallint(4) NULL,
			  `number_tin` VARCHAR(255) NULL,
			  `address` varchar(255) NULL default '',
			  `city` varchar(255) NULL default '',
			  `country_id` varchar(255) NULL default '',
			  `region_id` varchar(255) NULL default '',
			  `postcode` varchar(255) NULL default '',
			  `phone` VARCHAR(255) NULL,
			  `signature_image` varchar(255) NULL default '',
			  `pdf_file` varchar(255) NULL default '',
			  `created_at` datetime NULL,
			  `updated_at` datetime NULL,
			  `additional_information` text NULL,
			  `additional_pdf` text NULL,
			  PRIMARY KEY (`individual_id`)
			) ENGINE=InnoDB DEFAULT CHARSET=utf8;

			DROP TABLE IF EXISTS {$setup->getTable('marketer/business')};
			CREATE TABLE {$setup->getTable('marketer/business')} (
			  `business_id` int(11) unsigned NOT NULL auto_increment,
			  `customer_id` int(11) unsigned NOT NULL,
			  `business_name` varchar(255) NOT NULL default '',
			  `business_type` SMALLINT(4) NULL,
			  `state_incorporation` varchar(255) NULL default '',
			  `type_tin` smallint(4) NULL,
			  `number_tin` VARCHAR(255) NULL,
			  `management` varchar(255) NULL default '',
			  `type_llc` varchar(255) NULL default '',
			  `primary_name` varchar(255) NULL default '',
			  `primary_state` varchar(255) NULL default '',
			  `primary_address` varchar(255) NULL default '',
			  `primary_city` varchar(255) NULL default '',
			  `primary_country_id` varchar(255) NULL default '',
			  `primary_postcode` varchar(255) NULL default '',
			  `primary_phone` VARCHAR(255) NULL,
			  `signature_image` varchar(255) NULL default '',
			  `pdf_file` varchar(255) NULL default '',
			  `additional_member` text NULL default '',
			  `created_at` datetime NULL,
			  `updated_at` datetime NULL,
			  `additional_information` text NULL,
			  `additional_pdf` text NULL,
			  PRIMARY KEY (`business_id`)
			) ENGINE=InnoDB DEFAULT CHARSET=utf8;

			DROP TABLE IF EXISTS {$setup->getTable('marketer/pdf')};
			CREATE TABLE {$setup->getTable('marketer/pdf')} (
			  `pdf_id` int(11) unsigned NOT NULL auto_increment,
			  `name` varchar(255) NOT NULL default '',
			  `filename` varchar(255) NOT NULL default '',
			  `status` SMALLINT(4) NULL,
			  `created_at` datetime NULL,
			  `updated_at` datetime NULL,
			  PRIMARY KEY (`pdf_id`)
			) ENGINE=InnoDB DEFAULT CHARSET=utf8;
        ");
		$installer->endSetup();
		echo "success";
	}

	public function individualAction()
	{
		$data = $this->getRequest()->getParams();

		if($data != null){
			$connection = Mage::getSingleton('core/resource')->getConnection('core_write');
			try {
				$connection->beginTransaction();

				$customer_id = Mage::getSingleton('customer/session')->getCustomer()->getId();
				$customer = Mage::getModel('customer/customer')->load($customer_id);

				$img = Mage::helper('marketer')->convertImage(
					$data['individual_sync_sign'],
					$data['individual_name']
				);

				$individual = null;

				if($img != null){
					$individual = Mage::getModel('marketer/individual');

					$additional_information = array();
					$additional_information['state_name'] = $data['individual_state_name'];
					for	($i = 1; $i <= 9; $i++){
						$additional_information["individual_ssn_$i"] = $data["individual_ssn_$i"];
						$additional_information["individual_tin_$i"] = $data["individual_tin_$i"];
					}

					$_data = array(
						'customer_id'	=> $customer_id,
						'signature_image'	=> $img,
						'pdf_file'	=> $img,
						'name'	=> $data['individual_name'],
						'address'	=> $data['individual_address'],
						'city'	=> $data['individual_city'],
						'country_id'	=> $data['individual_country_id'],
						'postcode'	=> $data['individual_postcode'],
						'region_id'	=> $data['individual_state'],
						'type_tin'	=> $data['individual_type_tin'],
						'number_tin'	=> $data['individual_number'],
						'phone'	=> Mage::helper('custompromotions/verify')->formatPhoneToDatabase($data['individual_phone']),
						'additional_information'	=> json_encode($additional_information),
					);

					$pdf = Mage::helper('marketer')->writeDataToPdf($_data);

					$_data['pdf_file'] = $pdf;

					$individual->setData($_data);

					$individual->save();

//					Mage::helper('marketer')->sendEmail(
//						$customer,
//						$individual
//					);

					Mage::getSingleton('core/session')->setMarketer($individual);
					Mage::getSingleton('core/session')->setMarketerType(1);
					Mage::getSingleton('core/session')->setMarketerFlag(1);
				}

				$connection->commit();

				$this->_redirectUrl(Mage::getUrl('marketer/success'));
				return;
			} catch (Exception $ex) {
				$connection->rollback();
				Mage::getSingleton('core/session')->addError(
					$ex->getMessage()
				);
				$this->_redirectUrl(Mage::getUrl('marketer-central-agreement.html'));
				return;
			}
		}

	}

	public function businessAction()
	{
		$data = $this->getRequest()->getParams();

		if($data != null){

			$connection = Mage::getSingleton('core/resource')->getConnection('core_write');
			try {
				$connection->beginTransaction();

				$additional_member = array();
				foreach ($data as $k => $v) {
					if(strpos($k, 'name_member_') !== false){
						$id = str_replace('name_member_', '', $k);
						$additional_member[] = array(
							'name_member'	=> $v,
							'address_member'	=> $data['address_member_'.$id],
							'city_member'	=> $data['city_member_'.$id],
							'state_member'	=> $data['state_member_'.$id],
							'postcode_member'	=> $data['postcode_member_'.$id],
							'phone_member'	=> $data['phone_member_'.$id],
						);
					}
				}


				$customer_id = Mage::getSingleton('customer/session')->getCustomer()->getId();
				$customer = Mage::getModel('customer/customer')->load($customer_id);

				$img = Mage::helper('marketer')->convertImage(
					$data['business_sync_sign'],
					$data['business_name'],
					'business'
				);

				$business = null;
				if($img != null){
					$business = Mage::getModel('marketer/business');

					$additional_information = array();
					$additional_information['state_incorporation_name'] = $data['business_state_incorporation_name'];
					$additional_information['primary_state_name'] = $data['business_primary_state_name'];
					for	($i = 1; $i <= 9; $i++){
						$additional_information["business_ssn_$i"] = $data["business_ssn_$i"];
						$additional_information["business_tin_$i"] = $data["business_tin_$i"];
					}

					$_data = array(
						'customer_id'	=> $customer_id,
						'signature_image'	=> $img,
						'pdf_file'	=> $img,
						'business_type'	=> $data['type_of_business'],
						'state_incorporation'	=> $data['business_state_incorporation'],
						'business_name'	=> $data['business_name'],
						'type_tin'	=> $data['business_type_tin'],
						'number_tin'	=> $data['business_number'],
						'management'	=> $data['business_management'],
						'type_llc'	=> $data['business_type_llc'],
						'primary_name'	=> $data['business_primary_name'],
						'primary_address'	=> $data['business_primary_address'],
						'primary_state'	=> $data['business_state'],
						'primary_city'	=> $data['business_primary_city'],
						'primary_country_id'	=> $data['business_country_id'],
						'primary_postcode'	=> $data['business_primary_postcode'],
						'primary_region_id'	=> $data['business_state'],
						'primary_phone'	=> Mage::helper('custompromotions/verify')->formatPhoneToDatabase($data['business_primary_phone']),
						'additional_information'	=> json_encode($additional_information),
						'additional_member'	=> json_encode($additional_member),
					);

					$pdf = Mage::helper('marketer')->writeDataToPdf($_data, 'business');

					$_data['pdf_file'] = $pdf;

					$business->setData($_data);

					$business->save();

//					Mage::helper('marketer')->sendEmail(
//						$customer,
//						$business,
//						'business'
//					);

					Mage::getSingleton('core/session')->setMarketer($business);
					Mage::getSingleton('core/session')->setMarketerType(2);
					Mage::getSingleton('core/session')->setMarketerFlag(1);
				}

				$connection->commit();

				$this->_redirectUrl(Mage::getUrl('marketer/success'));
				return;
			} catch (Exception $ex) {
				$connection->rollback();
				Mage::getSingleton('core/session')->addError(
					$ex->getMessage()
				);
				$this->_redirectUrl(Mage::getUrl('marketer-central-agreement.html'));
				return;
			}
		}
	}
}