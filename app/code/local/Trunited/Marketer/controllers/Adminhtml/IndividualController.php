<?php

class Trunited_Marketer_Adminhtml_IndividualController extends Mage_Adminhtml_Controller_Action
{
	protected function _initAction(){
		$this->loadLayout()
			->_setActiveMenu('marketer/marketer')
			->_addBreadcrumb(Mage::helper('adminhtml')->__('Individual History'), Mage::helper('adminhtml')->__('Individual History'));
		return $this;
	}
 
	public function indexAction(){
		$this->_initAction()
			->renderLayout();
	}

	public function deleteAction() {
		if( $this->getRequest()->getParam('id') > 0 ) {
			try {
				$model = Mage::getModel('marketer/individual');
				$model->setId($this->getRequest()->getParam('id'))
					->delete();
				Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Individual was successfully deleted'));
				$this->_redirect('*/*/');
			} catch (Exception $e) {
				Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
				$this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
			}
		}
		$this->_redirect('*/*/');
	}

	public function massDeleteAction() {
		$marketerIds = $this->getRequest()->getParam('individual');
		if(!is_array($marketerIds)){
			Mage::getSingleton('adminhtml/session')->addError(Mage::helper('adminhtml')->__('Please select individual(s)'));
		}else{
			try {
				foreach ($marketerIds as $marketerId) {
					$marketer = Mage::getModel('marketer/individual')->load($marketerId);
					$marketer->delete();
				}
				Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Total of %d record(s) were successfully deleted', count($marketerIds)));
			} catch (Exception $e) {
				Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
			}
		}
		$this->_redirect('*/*/index');
	}
  
	public function exportCsvAction(){
		$fileName   = 'individual_history.csv';
		$content	= $this->getLayout()->createBlock('marketer/adminhtml_individual_grid')->getCsv();
		$this->_prepareDownloadResponse($fileName,$content);
	}

	public function exportXmlAction(){
		$fileName   = 'individual_history.xml';
		$content	= $this->getLayout()->createBlock('marketer/adminhtml_individual_grid')->getXml();
		$this->_prepareDownloadResponse($fileName,$content);
	}
}