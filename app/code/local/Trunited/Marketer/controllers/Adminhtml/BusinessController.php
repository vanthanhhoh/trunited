<?php

class Trunited_Marketer_Adminhtml_BusinessController extends Mage_Adminhtml_Controller_Action
{
	protected function _initAction(){
		$this->loadLayout()
			->_setActiveMenu('marketer/marketer')
			->_addBreadcrumb(Mage::helper('adminhtml')->__('LLC History'), Mage::helper('adminhtml')->__('LLC History'));
		return $this;
	}
 
	public function indexAction(){
		$this->_initAction()
			->renderLayout();
	}

	public function deleteAction() {
		if( $this->getRequest()->getParam('id') > 0 ) {
			try {
				$model = Mage::getModel('marketer/business');
				$model->setId($this->getRequest()->getParam('id'))
					->delete();
				Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('LLC was successfully deleted'));
				$this->_redirect('*/*/');
			} catch (Exception $e) {
				Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
				$this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
			}
		}
		$this->_redirect('*/*/');
	}

	public function massDeleteAction() {
		$marketerIds = $this->getRequest()->getParam('business');
		if(!is_array($marketerIds)){
			Mage::getSingleton('adminhtml/session')->addError(Mage::helper('adminhtml')->__('Please select LLC(s)'));
		}else{
			try {
				foreach ($marketerIds as $marketerId) {
					$marketer = Mage::getModel('marketer/business')->load($marketerId);
					$marketer->delete();
				}
				Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Total of %d record(s) were successfully deleted', count($marketerIds)));
			} catch (Exception $e) {
				Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
			}
		}
		$this->_redirect('*/*/index');
	}
  
	public function exportCsvAction(){
		$fileName   = 'llc_history.csv';
		$content	= $this->getLayout()->createBlock('marketer/adminhtml_business_grid')->getCsv();
		$this->_prepareDownloadResponse($fileName,$content);
	}

	public function exportXmlAction(){
		$fileName   = 'llc_history.xml';
		$content	= $this->getLayout()->createBlock('marketer/adminhtml_business_grid')->getXml();
		$this->_prepareDownloadResponse($fileName,$content);
	}
}