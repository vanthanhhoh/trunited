<?php

class Trunited_Marketer_Adminhtml_PdfController extends Mage_Adminhtml_Controller_Action
{
	protected function _initAction(){
		$this->loadLayout()
			->_setActiveMenu('marketer/marketer')
			->_addBreadcrumb(Mage::helper('adminhtml')->__('Email PDF Manager'), Mage::helper('adminhtml')->__('Email PDF Manager'));
		return $this;
	}
 
	public function indexAction(){
		$this->_initAction()
			->renderLayout();
	}

	public function editAction() {
		$id	 = $this->getRequest()->getParam('id');
		$model  = Mage::getModel('marketer/pdf')->load($id);

		if ($model->getId() || $id == 0) {
			$data = Mage::getSingleton('adminhtml/session')->getFormData(true);
			if (!empty($data))
				$model->setData($data);

			Mage::register('pdf_data', $model);

			$this->loadLayout();
			$this->_setActiveMenu('marketer/pdf');

			$this->_addBreadcrumb(Mage::helper('adminhtml')->__('Email PDF Manager'), Mage::helper('adminhtml')->__('Email PDF Manager'));
			$this->_addBreadcrumb(Mage::helper('adminhtml')->__('PDF News'), Mage::helper('adminhtml')->__('PDF News'));

			$this->getLayout()->getBlock('head')->setCanLoadExtJs(true);
			$this->_addContent($this->getLayout()->createBlock('marketer/adminhtml_pdf_edit'))
				->_addLeft($this->getLayout()->createBlock('marketer/adminhtml_pdf_edit_tabs'));

			$this->renderLayout();
		} else {
			Mage::getSingleton('adminhtml/session')->addError(Mage::helper('marketer')->__('PDF does not exist'));
			$this->_redirect('*/*/');
		}
	}
 
	public function newAction() {
		$this->_forward('edit');
	}
 
	public function saveAction() {
		if ($data = $this->getRequest()->getPost()) {
			if(isset($_FILES['filename']['name']) && $_FILES['filename']['name'] != null){
				$data['filename'] = $this->_uploadBackgroundFile('filename');
			} else if(isset($data['filename']) && is_array($data['filename'])){

				if(isset($data['filename']['delete']) && $data['filename']['delete'] == 1){
					$data['filename'] = '';
				} else if(isset($data['filename']['value'])) {
					$data['filename'] = str_replace('marketer' . DS . 'email' . DS,'', $data['filename']['value']);
				} else {
					$data['filename'] = $this->_uploadBackgroundFile('filename');
				}
			} else if ($sourceFile = $this->_uploadBackgroundFile('filename')) {
				$data['filename'] = $sourceFile;
			}
	  		
			$model = Mage::getModel('marketer/pdf');
			$model->setData($data)
				->setId($this->getRequest()->getParam('id'));
			
			try {

				$model->save();
				Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('marketer')->__('PDF was successfully saved'));
				Mage::getSingleton('adminhtml/session')->setFormData(false);

				if ($this->getRequest()->getParam('back')) {
					$this->_redirect('*/*/edit', array('id' => $model->getId()));
					return;
				}
				$this->_redirect('*/*/');
				return;
			} catch (Exception $e) {
				Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
				Mage::getSingleton('adminhtml/session')->setFormData($data);
				$this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
				return;
			}
		}
		Mage::getSingleton('adminhtml/session')->addError(Mage::helper('marketer')->__('Unable to find pdf to save'));
		$this->_redirect('*/*/');
	}

	/**
	 * Upload banner file to server
	 *
	 * @param string $fieldName
	 * @return string
	 */
	protected function _uploadBackgroundFile($fieldName)
	{
		if(isset($_FILES[$fieldName]['delete']) && $_FILES[$fieldName]['delete'] != 1){
			return '';
		}

		if (isset($_FILES[$fieldName]['name']) && $_FILES[$fieldName]['name'] != '') {
			try {
				$uploader = new Varien_File_Uploader($fieldName);
				$uploader->setAllowedExtensions(array('pdf'));
				$uploader->setAllowRenameFiles(true);
				$uploader->setFilesDispersion(false);

				// We set media as the upload dir
				$path = Mage::getBaseDir('media') . DS . 'marketer' . DS . 'email' . DS;
				$result = $uploader->save($path, $_FILES[$fieldName]['name']);

				return $result['file'];
			} catch (Exception $e) {
				return $_FILES[$fieldName]['name'];
			}
		}
		return '';
	}
 
	public function deleteAction() {
		if( $this->getRequest()->getParam('id') > 0 ) {
			try {
				$model = Mage::getModel('marketer/pdf');
				$model->setId($this->getRequest()->getParam('id'))
					->delete();
				Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('PDF was successfully deleted'));
				$this->_redirect('*/*/');
			} catch (Exception $e) {
				Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
				$this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
			}
		}
		$this->_redirect('*/*/');
	}

	public function massDeleteAction() {
		$marketerIds = $this->getRequest()->getParam('pdf');
		if(!is_array($marketerIds)){
			Mage::getSingleton('adminhtml/session')->addError(Mage::helper('adminhtml')->__('Please select pdf(s)'));
		}else{
			try {
				foreach ($marketerIds as $marketerId) {
					$marketer = Mage::getModel('marketer/pdf')->load($marketerId);
					$marketer->delete();
				}
				Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Total of %d record(s) were successfully deleted', count($marketerIds)));
			} catch (Exception $e) {
				Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
			}
		}
		$this->_redirect('*/*/index');
	}
	
	public function massStatusAction() {
		$marketerIds = $this->getRequest()->getParam('pdf');
		if(!is_array($marketerIds)) {
			Mage::getSingleton('adminhtml/session')->addError($this->__('Please select pdf(s)'));
		} else {
			try {
				foreach ($marketerIds as $marketerId) {
					Mage::getSingleton('marketer/pdf')
						->load($marketerId)
						->setStatus($this->getRequest()->getParam('status'))
						->setIsMassupdate(true)
						->save();
				}
				$this->_getSession()->addSuccess(
					$this->__('Total of %d record(s) were successfully updated', count($marketerIds))
				);
			} catch (Exception $e) {
				$this->_getSession()->addError($e->getMessage());
			}
		}
		$this->_redirect('*/*/index');
	}
  
	public function exportCsvAction(){
		$fileName   = 'email_pdf.csv';
		$content	= $this->getLayout()->createBlock('marketer/adminhtml_pdf_grid')->getCsv();
		$this->_prepareDownloadResponse($fileName,$content);
	}

	public function exportXmlAction(){
		$fileName   = 'email_pdf.xml';
		$content	= $this->getLayout()->createBlock('marketer/adminhtml_pdf_grid')->getXml();
		$this->_prepareDownloadResponse($fileName,$content);
	}
}