<?php

class Trunited_Marketer_Model_Status extends Varien_Object
{
	const STATUS_ENABLED	= 1;
	const STATUS_DISABLED	= 2;

	const TYPE_TIN_SSN = 1;
	const TYPE_TIN_TIN = 2;

	const TYPE_OF_BUSINESS_C_CORPORATION = 1;
	const TYPE_OF_BUSINESS_S_CORPORATION = 2;
	const TYPE_OF_BUSINESS_PARTNERSHIP = 3;
	const TYPE_OF_BUSINESS_TRUST_ESTATE = 4;
	const TYPE_OF_BUSINESS_LIMITED_LIABILITY_COMPANY = 5;

	const TYPE_OF_LLC_SINGLE_MEMBER = 1;
	const TYPE_OF_LLC_MULTI_MEMBER = 2;

	const MANAGEMENT_LLC_MEMBER = 1;
	const MANAGEMENT_LLC_MANAGER = 2;

	static public function getOptionArray(){
		return array(
			self::STATUS_ENABLED	=> Mage::helper('marketer')->__('Enabled'),
			self::STATUS_DISABLED   => Mage::helper('marketer')->__('Disabled')
		);
	}
	
	static public function getOptionHash(){
		$options = array();
		foreach (self::getOptionArray() as $value => $label)
			$options[] = array(
				'value'	=> $value,
				'label'	=> $label
			);
		return $options;
	}

	static public function getTINOptionArray(){
		return array(
			self::TYPE_TIN_SSN	=> Mage::helper('marketer')->__('Social Security Number (SSN)'),
			self::TYPE_TIN_TIN   => Mage::helper('marketer')->__('Taxpayer Identification Number (TIN)')
		);
	}

	static public function getTINOptionHash(){
		$options = array();
		foreach (self::getTINOptionArray() as $value => $label)
			$options[] = array(
				'value'	=> $value,
				'label'	=> $label
			);
		return $options;
	}

	static public function getTypeBusinessOptionArray(){
		return array(
			self::TYPE_OF_BUSINESS_C_CORPORATION	=> Mage::helper('marketer')->__('C Corporation'),
			self::TYPE_OF_BUSINESS_S_CORPORATION   => Mage::helper('marketer')->__('S Corporation'),
			self::TYPE_OF_BUSINESS_PARTNERSHIP   => Mage::helper('marketer')->__('Partnership'),
			self::TYPE_OF_BUSINESS_TRUST_ESTATE   => Mage::helper('marketer')->__('Trust/estate'),
			self::TYPE_OF_BUSINESS_LIMITED_LIABILITY_COMPANY   => Mage::helper('marketer')->__('Limited Liability Company'),
		);
	}

	static public function getTypeBusinessOptionHash(){
		$options = array();
		foreach (self::getTypeBusinessOptionArray() as $value => $label)
			$options[] = array(
				'value'	=> $value,
				'label'	=> $label
			);
		return $options;
	}

	static public function getTypeLLCOptionArray(){
		return array(
			self::TYPE_OF_LLC_SINGLE_MEMBER	=> Mage::helper('marketer')->__('Single Member LLC'),
			self::TYPE_OF_LLC_MULTI_MEMBER   => Mage::helper('marketer')->__('Multi-Member LLC')
		);
	}

	static public function getTypeLLCOptionHash(){
		$options = array();
		foreach (self::getTypeLLCOptionArray() as $value => $label)
			$options[] = array(
				'value'	=> $value,
				'label'	=> $label
			);
		return $options;
	}

	static public function getManagementLLCOptionArray(){
		return array(
			self::MANAGEMENT_LLC_MEMBER	=> Mage::helper('marketer')->__('Member-Managed LLC'),
			self::MANAGEMENT_LLC_MANAGER   => Mage::helper('marketer')->__('Manager-Managed LLC')
		);
	}

	static public function getManagementLLCOptionHash(){
		$options = array();
		foreach (self::getManagementLLCOptionArray() as $value => $label)
			$options[] = array(
				'value'	=> $value,
				'label'	=> $label
			);
		return $options;
	}

	static public function getStateOption()
	{
		$states = Mage::getModel('directory/country')->load('US')->getRegions();
		$result = array();

		foreach ($states as $state) {
			$result[$state['region_id']] = $state['name'];
		}

		return $result;
	}
}