<?php

class Trunited_Marketer_Model_Mysql4_Business_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
	public function _construct(){
		parent::_construct();
		$this->_init('marketer/business');
	}

	public function addCustomerNameToSelect()
	{
		$table_entity = Mage::getSingleton('core/resource')->getTableName('customer_entity_varchar');

		$fn = Mage::getModel('eav/entity_attribute')->loadByCode('1', 'firstname');
		$ln = Mage::getModel('eav/entity_attribute')->loadByCode('1', 'lastname');

		$this->getSelect()
			->join(array('ce1' => $table_entity), 'ce1.entity_id=main_table.customer_id', array('firstname' => 'value'))
			->where('ce1.attribute_id='.$fn->getAttributeId())
			->join(array('ce2' => $table_entity), 'ce2.entity_id=main_table.customer_id', array('lastname' => 'value'))
			->where('ce2.attribute_id='.$ln->getAttributeId())
			->columns(new Zend_Db_Expr("CONCAT(`ce1`.`value`, ' ',`ce2`.`value`) AS customer_name"));

		return $this;
	}

	public function filterCustomerName($value)
	{
		$table_entity = Mage::getSingleton('core/resource')->getTableName('customer_entity_varchar');
		$firstnameAttr = Mage::getModel('eav/entity_attribute')->loadByCode('1', 'firstname');
		$lastnameAttr = Mage::getModel('eav/entity_attribute')->loadByCode('1', 'lastname');

		$this->getSelect()
			->join(array('ce11' => $table_entity), 'ce11.entity_id=main_table.customer_id', array('firstname' => 'value'))
			->where('ce11.attribute_id=' . $firstnameAttr->getAttributeId())
			->join(array('ce21' => $table_entity), 'ce21.entity_id=main_table.customer_id', array('lastname' => 'value'))
			->where('ce21.attribute_id=' . $lastnameAttr->getAttributeId().' and (ce21.value like \'%'.$value.'%\' or ce11.value like \'%'.$value.'%\')')
			->columns(new Zend_Db_Expr("CONCAT(`ce11`.`value`, ' ',`ce21`.`value`) AS fullname"))
		;

		return $this;
	}

	public function addCustomerEmailToSelect()
	{
		$table_entity = Mage::getSingleton('core/resource')->getTableName('customer_entity');

		$this->getSelect()
			->join(
				array('customer_entity' => $table_entity),
				'main_table.customer_id = customer_entity.entity_id', array('customer_email' => 'email'));

		return $this;
	}
}