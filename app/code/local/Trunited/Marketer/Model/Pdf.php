<?php

class Trunited_Marketer_Model_Pdf extends Mage_Core_Model_Abstract
{
	public function _construct(){
		parent::_construct();
		$this->_init('marketer/pdf');
	}

	/**
	 * Before object save manipulations
	 *
	 * @return Trunited_Marketer_Model_Individual
	 */
	protected function _beforeSave()
	{
		parent::_beforeSave();

		if($this->isObjectNew())
			$this->setData('created_at', now());

		$this->setData('updated_at', now());

		return $this;
	}
}