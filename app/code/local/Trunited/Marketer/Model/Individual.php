<?php

class Trunited_Marketer_Model_Individual extends Mage_Core_Model_Abstract
{
	public function _construct(){
		parent::_construct();
		$this->_init('marketer/individual');
	}


	/**
	 * Before object save manipulations
	 *
	 * @return Trunited_Marketer_Model_Individual
	 */
	protected function _beforeSave()
	{
		parent::_beforeSave();

		if($this->isObjectNew())
			$this->setData('created_at', now());

		$this->setData('updated_at', now());

		return $this;
	}

	public function getImageUrl()
	{
		return Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA). 'marketer' . DS . 'individual' . $this->getSignatureImage();
	}

	public function getPdfPath()
	{
		return Mage::getBaseDir('media') . DS . 'marketer' . DS .'individual' . DS . $this->getPdfFile();
	}

	public function getAdditionalInformation()
	{
		return json_decode($this->getData('additional_information'), true);
	}
}