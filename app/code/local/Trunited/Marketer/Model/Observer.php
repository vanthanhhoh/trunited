<?php

class Trunited_Marketer_Model_Observer
{
    const AUTOLOADER_FILE = '/FPDF/vendor/autoload.php';

    /**
     * @return $this
     */
    public function addAutoloader()
    {
        require_once Mage::getBaseDir('lib').self::AUTOLOADER_FILE;
        return $this;
    }
}
