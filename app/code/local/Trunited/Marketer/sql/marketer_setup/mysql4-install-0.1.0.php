<?php

$installer = $this;
$installer->startSetup();

$installer->run("

          	DROP TABLE IF EXISTS {$this->getTable('marketer/individual')};
			CREATE TABLE {$this->getTable('marketer/individual')} (
			  `individual_id` int(11) unsigned NOT NULL auto_increment,
			  `customer_id` int(11) unsigned NOT NULL,
			  `name` varchar(255) NOT NULL default '',
			  `type_tin` smallint(4) NULL,
			  `number_tin` VARCHAR(255) NULL,
			  `address` varchar(255) NULL default '',
			  `city` varchar(255) NULL default '',
			  `country_id` varchar(255) NULL default '',
			  `region_id` varchar(255) NULL default '',
			  `postcode` varchar(255) NULL default '',
			  `phone` VARCHAR(255) NULL,
			  `signature_image` varchar(255) NULL default '',
			  `pdf_file` varchar(255) NULL default '',
			  `created_at` datetime NULL,
			  `updated_at` datetime NULL,
			  `additional_information` text NULL,
			  `additional_pdf` text NULL,
			  PRIMARY KEY (`individual_id`)
			) ENGINE=InnoDB DEFAULT CHARSET=utf8;

			DROP TABLE IF EXISTS {$this->getTable('marketer/business')};
			CREATE TABLE {$this->getTable('marketer/business')} (
			  `business_id` int(11) unsigned NOT NULL auto_increment,
			  `customer_id` int(11) unsigned NOT NULL,
			  `business_name` varchar(255) NOT NULL default '',
			  `business_type` SMALLINT(4) NULL,
			  `state_incorporation` varchar(255) NULL default '',
			  `type_tin` smallint(4) NULL,
			  `number_tin` VARCHAR(255) NULL,
			  `management` varchar(255) NULL default '',
			  `type_llc` varchar(255) NULL default '',
			  `primary_name` varchar(255) NULL default '',
			  `primary_state` varchar(255) NULL default '',
			  `primary_address` varchar(255) NULL default '',
			  `primary_city` varchar(255) NULL default '',
			  `primary_country_id` varchar(255) NULL default '',
			  `primary_postcode` varchar(255) NULL default '',
			  `primary_phone` VARCHAR(255) NULL,
			  `signature_image` varchar(255) NULL default '',
			  `pdf_file` varchar(255) NULL default '',
			  `additional_member` text NULL default '',
			  `created_at` datetime NULL,
			  `updated_at` datetime NULL,
			  `additional_information` text NULL,
			  `additional_pdf` text NULL,
			  PRIMARY KEY (`business_id`)
			) ENGINE=InnoDB DEFAULT CHARSET=utf8;

			DROP TABLE IF EXISTS {$this->getTable('marketer/pdf')};
			CREATE TABLE {$this->getTable('marketer/pdf')} (
			  `pdf_id` int(11) unsigned NOT NULL auto_increment,
			  `name` varchar(255) NOT NULL default '',
			  `filename` varchar(255) NOT NULL default '',
			  `status` SMALLINT(4) NULL,
			  `created_at` datetime NULL,
			  `updated_at` datetime NULL,
			  PRIMARY KEY (`pdf_id`)
			) ENGINE=InnoDB DEFAULT CHARSET=utf8;

    ");

Mage::getModel('core/url_rewrite')
    ->setStoreId(1)
    ->setIdPath('marketer_agreement')
    ->setRequestPath('marketer-agreement.html')
    ->setTargetPath('marketer/index/index')
    ->setIsSystem(0)
    ->save();

$installer->endSetup(); 