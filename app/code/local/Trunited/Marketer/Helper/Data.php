<?php

class Trunited_Marketer_Helper_Data extends Mage_Core_Helper_Abstract
{
    const PDF_W9_FILE = 'marketer' . DS . 'W9.pdf';
    const MARKETER_PATH = 'marketer';

    const XML_PATH_EMAIL_SENDER = 'marketer/email/sender';
    const XML_PATH_EMAIL_TO_INDIVIDUAL = 'marketer/email/individual';
    const XML_PATH_EMAIL_TO_BUSINESS = 'marketer/email/business';

    /**
     * @param $group
     * @param $field
     * @param null $store
     * @return mixed
     */
    public function getConfigData($group, $field, $store = null)
    {
        return Mage::getStoreConfig('marketer/' . $group . '/' . $field, $store);
    }

    /**
     * @return mixed
     */
    public function isEnable()
    {
        return $this->getConfigData('general', 'enable');
    }

    /**
     * @param $str
     * @param $name
     * @param string $type
     * @return null|string
     */
    public function convertImage($str, $name, $type = 'individual')
    {
        $str = str_replace('data:image/png;base64,', '', $str);
        $str = str_replace(' ', '+', $str);
        $str = base64_decode($str);

        $image_name = trim(str_replace(' ', '_', trim(ucwords($name)))) . '_' . date('mdY', time()) . '.png';

        $file = Mage::getBaseDir('media') . DS . 'marketer' . DS . $type . DS . $image_name;
        $success = file_put_contents($file, $str);

        if ($success !== false)
            return $image_name;
        else
            return null;
    }

    /**
     * @param $data
     * @param string $type
     * @return mixed
     * @throws \setasign\Fpdi\PdfReader\PdfReaderException
     */
    public function writeDataToPdf($data, $type = 'individual')
    {
        $filename = Mage::getBaseDir('media') . DS . self::PDF_W9_FILE;

        $_data = $this->processData($data, $type);

        $pdf = new Trunited_Marketer_Model_MarketerPDF();
        $pdf->setAutoPageBreak(false, 0);

        $pdf->AddPage();
        $pdf->setSourceFile($filename);
        $tplIdx = $pdf->ImportPage(1);
        $pdf->useTemplate($tplIdx, 0, 0, null, null, true);

        $leftMargin = $pdf->getX() + 12.8;
        $pdf->SetLeftMargin($leftMargin);
        $pdf->SetFont('helvetica', 'B', 8);

        if ($pdf->isCompressedXref()) {
            /* This document uses new PDF compression technics introduced in PDF version 1.5 ;-) */
        } else {
            $addition = json_decode($data['additional_information'], true);

            if ($type == 'individual') {
                $pdf->SetTextColor(0, 0, 0);
                $pdf->Write(50, $_data['name']);
                $pdf->Ln(0);

                $pdf->Write(95, 'x');
                $pdf->Ln(0);

                $pdf->Write(160, ucwords($_data['address']));
                $pdf->Ln(0);

                $pdf->Write(177, $_data['city_state']);
                $pdf->Ln(0);

                if ($_data['type_tin'] == Trunited_Marketer_Model_Status::TYPE_TIN_SSN) {
                    $pdf->SetLeftMargin(148);
                    $pdf->Write(225, $addition[$type . '_ssn_1']);
                    $pdf->SetLeftMargin(153);
                    $pdf->Write(225, $addition[$type . '_ssn_2']);
                    $pdf->SetLeftMargin(158);
                    $pdf->Write(225, $addition[$type . '_ssn_3']);
                    $pdf->SetLeftMargin(168.5);
                    $pdf->Write(225, $addition[$type . '_ssn_4']);
                    $pdf->SetLeftMargin(173.5);
                    $pdf->Write(225, $addition[$type . '_ssn_5']);
                    $pdf->SetLeftMargin(183.5);
                    $pdf->Write(225, $addition[$type . '_ssn_6']);
                    $pdf->SetLeftMargin(188.5);
                    $pdf->Write(225, $addition[$type . '_ssn_7']);
                    $pdf->SetLeftMargin(193.5);
                    $pdf->Write(225, $addition[$type . '_ssn_8']);
                    $pdf->SetLeftMargin(198.5);
                    $pdf->Write(225, $addition[$type . '_ssn_9']);
                } else {
                    $pdf->SetLeftMargin(148);
                    $pdf->Write(260, $addition[$type . '_tin_1']);
                    $pdf->SetLeftMargin(153);
                    $pdf->Write(260, $addition[$type . '_tin_2']);
                    $pdf->SetLeftMargin(163.3);
                    $pdf->Write(260, $addition[$type . '_tin_3']);
                    $pdf->SetLeftMargin(168.5);
                    $pdf->Write(260, $addition[$type . '_tin_4']);
                    $pdf->SetLeftMargin(173.5);
                    $pdf->Write(260, $addition[$type . '_tin_5']);
                    $pdf->SetLeftMargin(178.5);
                    $pdf->Write(260, $addition[$type . '_tin_6']);
                    $pdf->SetLeftMargin(183.5);
                    $pdf->Write(260, $addition[$type . '_tin_7']);
                    $pdf->SetLeftMargin(188.5);
                    $pdf->Write(260, $addition[$type . '_tin_8']);
                    $pdf->SetLeftMargin(193.5);
                    $pdf->Write(260, $addition[$type . '_tin_9']);
                }

                $pdf->Ln(0);
                $pdf->SetLeftMargin(147);
                $pdf->Write(124.5, $_data['created_at']);

                $pdf->Ln(0);
                $pdf->Image($_data['signature_image'], 48, 191, 30, 7.5);
            } else {
                $pdf->SetTextColor(0, 0, 0);
                $pdf->Write(67, $_data['name']);
                $pdf->Ln(0);

                if ($_data['business_type'] == Trunited_Marketer_Model_Status::TYPE_OF_BUSINESS_C_CORPORATION) {
                    $pdf->SetLeftMargin(63.2);
                    $pdf->Write(94, 'x');
                    $pdf->Ln(0);
                } else if ($_data['business_type'] == Trunited_Marketer_Model_Status::TYPE_OF_BUSINESS_S_CORPORATION) {
                    $pdf->SetLeftMargin(88.5);
                    $pdf->Write(94, 'x');
                    $pdf->Ln(0);
                } else if ($_data['business_type'] == Trunited_Marketer_Model_Status::TYPE_OF_BUSINESS_PARTNERSHIP) {
                    $pdf->SetLeftMargin(114);
                    $pdf->Write(94, 'x');
                    $pdf->Ln(0);
                } else if ($_data['business_type'] == Trunited_Marketer_Model_Status::TYPE_OF_BUSINESS_TRUST_ESTATE) {
                    $pdf->SetLeftMargin(139.5);
                    $pdf->Write(94, 'x');
                    $pdf->Ln(0);
                } else if ($_data['business_type'] == Trunited_Marketer_Model_Status::TYPE_OF_BUSINESS_LIMITED_LIABILITY_COMPANY) {
                    $pdf->SetLeftMargin(11);
                    $pdf->Write(112, 'x');
                    $pdf->Ln(0);
                }

                $pdf->SetLeftMargin(0);
                $pdf->Write(160, '');
                $pdf->Ln(0);

                $pdf->SetLeftMargin(23);
                $pdf->Write(160, '1234 Anywhere St');
                $pdf->Ln(0);

                $pdf->Write(177, $_data['city_state']);
                $pdf->Ln(0);

                if ($_data['type_tin'] == Trunited_Marketer_Model_Status::TYPE_TIN_SSN) {
                    $pdf->SetLeftMargin(148);
                    $pdf->Write(225, $addition[$type . '_ssn_1']);
                    $pdf->SetLeftMargin(153);
                    $pdf->Write(225, $addition[$type . '_ssn_2']);
                    $pdf->SetLeftMargin(158);
                    $pdf->Write(225, $addition[$type . '_ssn_3']);
                    $pdf->SetLeftMargin(168.5);
                    $pdf->Write(225, $addition[$type . '_ssn_4']);
                    $pdf->SetLeftMargin(173.5);
                    $pdf->Write(225, $addition[$type . '_ssn_5']);
                    $pdf->SetLeftMargin(183.5);
                    $pdf->Write(225, $addition[$type . '_ssn_6']);
                    $pdf->SetLeftMargin(188.5);
                    $pdf->Write(225, $addition[$type . '_ssn_7']);
                    $pdf->SetLeftMargin(193.5);
                    $pdf->Write(225, $addition[$type . '_ssn_8']);
                    $pdf->SetLeftMargin(198.5);
                    $pdf->Write(225, $addition[$type . '_ssn_9']);
                } else {
                    $pdf->SetLeftMargin(148);
                    $pdf->Write(260, $addition[$type . '_tin_1']);
                    $pdf->SetLeftMargin(153);
                    $pdf->Write(260, $addition[$type . '_tin_2']);
                    $pdf->SetLeftMargin(163.3);
                    $pdf->Write(260, $addition[$type . '_tin_3']);
                    $pdf->SetLeftMargin(168.5);
                    $pdf->Write(260, $addition[$type . '_tin_4']);
                    $pdf->SetLeftMargin(173.5);
                    $pdf->Write(260, $addition[$type . '_tin_5']);
                    $pdf->SetLeftMargin(178.5);
                    $pdf->Write(260, $addition[$type . '_tin_6']);
                    $pdf->SetLeftMargin(183.5);
                    $pdf->Write(260, $addition[$type . '_tin_7']);
                    $pdf->SetLeftMargin(188.5);
                    $pdf->Write(260, $addition[$type . '_tin_8']);
                    $pdf->SetLeftMargin(193.5);
                    $pdf->Write(260, $addition[$type . '_tin_9']);
                }

                $pdf->Ln(0);
                $pdf->SetLeftMargin(147);
                $pdf->Write(124.5, $_data['created_at']);

                $pdf->Ln(0);
                $pdf->Image($_data['signature_image'], 48, 191, 30, 7.5);
            }

        }


        $pdf->Output('F', Mage::getBaseDir('media') . DS . self::MARKETER_PATH . DS . $type . DS . $_data['pdf_name']);

        return $_data['pdf_name'];
    }

    public function processData($data, $type = 'individual')
    {
        $addition = json_decode($data['additional_information'], true);
        $origin_image = $data['signature_image'];

        if ($type == 'individual') {
            $data['name'] = ucwords(trim($data['name']));
            $data['city_state'] = ucwords($data['city'] . ', ' . $addition['state_name'] . ' ' . $data['postcode'] . ', United States');
        } else {
            $data['name'] = ucwords(trim($data['business_name']));
            $data['address'] = ucwords(trim($data['primary_address']));
            $data['city_state'] = ucwords($data['primary_city'] . ', ' . $addition['primary_state_name'] . ' ' . $data['primary_postcode'] . ', United States');
        }

        $data['created_at'] = date('m/d/Y', time());
        $data['signature_image'] = Mage::getBaseDir('media') . DS . 'marketer' . DS . $type . DS . $data['signature_image'];
        $data['pdf_name'] = str_replace('.png', '', $origin_image) . '.pdf';

        return $data;
    }

    public function sendEmail($recipient, $model, $type = 'individual')
    {
        $store = Mage::app()->getStore();
        $translate = Mage::getSingleton('core/translate');
        $translate->setTranslateInline(false);

        $name = $recipient->getName();

        $data = array(
            'store' => $store,
            'sender_name' => '',
            'customer_name' => $recipient->getFirstname(),
        );

        if ($type == 'individual')
            $email_path = Mage::getStoreConfig(self::XML_PATH_EMAIL_TO_INDIVIDUAL, $store);
        else
            $email_path = Mage::getStoreConfig(self::XML_PATH_EMAIL_TO_BUSINESS, $store);

        $email = Mage::getModel('core/email_template');

        /* Send emails with attachment */
        if ($this->getConfigData('email', 'send_w9')) {
            $email->getMail()->createAttachment(
                file_get_contents($model->getPdfPath()),
                Zend_Mime::TYPE_OCTETSTREAM,
                Zend_Mime::DISPOSITION_ATTACHMENT,
                Zend_Mime::ENCODING_BASE64,
                $model->getPdfFile()
            );
        }

        $pdfs = $this->getAttachmentFiles();

        if ($pdfs != null) {
            $additional_pdf = array();
            foreach ($pdfs as $pdf) {
                $additional_pdf[] = array(
                    'name'  => $pdf->getName(),
                    'link'  => $pdf->getFilename()
                );
                $file_path_pdf = Mage::getBaseDir('media') . DS . 'marketer' . DS . 'email' . DS . $pdf->getFilename();
                $email->getMail()->createAttachment(
                    file_get_contents($file_path_pdf),
                    Zend_Mime::TYPE_OCTETSTREAM,
                    Zend_Mime::DISPOSITION_ATTACHMENT,
                    Zend_Mime::ENCODING_BASE64,
                    $pdf->getName() . '.pdf'
                );
            }

            $model->setData('additional_pdf', json_encode($additional_pdf))->save();

        }
        /* Send emails with attachment */

        $email->setDesignConfig(array(
            'area' => 'frontend',
            'store' => Mage::app()->getStore()->getId()
        ))->sendTransactional(
            $email_path,
            Mage::getStoreConfig(self::XML_PATH_EMAIL_SENDER, $store->getId()),
            $recipient->getEmail(),
            $name,
            $data
        );

        $translate->setTranslateInline(true);

        return $this;
    }

    public function getAttachmentFiles()
    {
        $pdfs = Mage::getModel('marketer/pdf')->getCollection()
            ->addFieldToFilter('status', Trunited_Marketer_Model_Status::STATUS_ENABLED)
            ->setOrder('pdf_id', 'desc');

        if (sizeof($pdfs) > 0)
            return $pdfs;
        else
            return null;
    }

    public function isRegister($customer_id)
    {
        $individual = Mage::getModel('marketer/individual')->load($customer_id, 'customer_id');
        $llc = Mage::getModel('marketer/business')->load($customer_id, 'customer_id');

        if (($individual != null && $individual->getId()) || ($llc != null && $llc->getId())) {
            return true;
        }

        return false;
    }

}