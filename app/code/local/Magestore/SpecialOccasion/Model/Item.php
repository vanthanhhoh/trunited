<?php

class Magestore_SpecialOccasion_Model_Item extends Mage_Core_Model_Abstract
{
	public function _construct(){
		parent::_construct();
		$this->_init('specialoccasion/item');
	}

    /**
     * Before object save manipulations
     *
     * @return Magestore_SpecialOccasion_Model_Item
     */
    protected function _beforeSave()
    {
        parent::_beforeSave();

        if($this->isObjectNew()){
            $this->setData('created_at', now());
            $this->setData('is_sent_email', Magestore_SpecialOccasion_Model_Status::SEND_EMAIL_NO);
            $this->setData('status', Magestore_SpecialOccasion_Model_Status::STATUS_ITEM_PENDING);
            $this->setData('state', Magestore_SpecialOccasion_Model_Status::STATE_ACTIVE);
        }

        $this->setData('updated_at', now());

        return $this;
    }
}
