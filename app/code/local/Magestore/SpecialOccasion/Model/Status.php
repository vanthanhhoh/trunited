<?php

class Magestore_SpecialOccasion_Model_Status extends Varien_Object
{
    const STATUS_ENABLED    = 1;
    const STATUS_DISABLED   = 2;

    const STATUS_ITEM_PENDING = 1;
    const STATUS_ITEM_PROCESSING = 2;
    const STATUS_ITEM_COMPLETE = 3;
    const STATUS_ITEM_CANCEL = 4;

    const STATE_ACTIVE = 1;
    const STATE_INACTIVE = 2;

    const SEND_EMAIL_YES = 1;
    const SEND_EMAIL_NO = 2;

    const HISTORY_STATUS_SUCCESS = 1;
    const HISTORY_STATUS_FAILURE = 2;
    const HISTORY_STATUS_RESET = 3;

    static public function getOptionArray(){
        return array(
            self::STATUS_ENABLED    => Mage::helper('specialoccasion')->__('Enabled'),
            self::STATUS_DISABLED   => Mage::helper('specialoccasion')->__('Disabled')
        );
    }
    
    static public function getOptionHash(){
        $options = array();
        foreach (self::getOptionArray() as $value => $label)
            $options[] = array(
                'value' => $value,
                'label' => $label
            );
        return $options;
    }

    static public function getOptionItemArray(){
        return array(
            self::STATUS_ITEM_PENDING   => Mage::helper('specialoccasion')->__('Pending'),
            self::STATUS_ITEM_PROCESSING   => Mage::helper('specialoccasion')->__('Processing'),
            self::STATUS_ITEM_COMPLETE   => Mage::helper('specialoccasion')->__('Complete'),
            self::STATUS_ITEM_CANCEL   => Mage::helper('specialoccasion')->__('Error'),
        );
    }

    static public function getOptionItemHash(){
        $options = array();
        foreach (self::getOptionItemArray() as $value => $label)
            $options[] = array(
                'value' => $value,
                'label' => $label
            );
        return $options;
    }

    static public function getOptionStateArray(){
        return array(
            self::STATE_ACTIVE  => Mage::helper('specialoccasion')->__('Active'),
            self::STATE_INACTIVE   => Mage::helper('specialoccasion')->__('Inactive'),
        );
    }

    static public function getOptionStateHash(){
        $options = array();
        foreach (self::getOptionStateArray() as $value => $label)
            $options[] = array(
                'value' => $value,
                'label' => $label
            );
        return $options;
    }

    static public function getOptionSendEmailArray(){
        return array(
            self::SEND_EMAIL_YES    => Mage::helper('specialoccasion')->__('Yes'),
            self::SEND_EMAIL_NO   => Mage::helper('specialoccasion')->__('No'),
        );
    }

    static public function getOptionHistoryArray(){
        return array(
            self::HISTORY_STATUS_SUCCESS    => Mage::helper('specialoccasion')->__('Success'),
            self::HISTORY_STATUS_FAILURE   => Mage::helper('specialoccasion')->__('Canceled'),
            self::HISTORY_STATUS_RESET   => Mage::helper('specialoccasion')->__('Reset'),
        );
    }
}
