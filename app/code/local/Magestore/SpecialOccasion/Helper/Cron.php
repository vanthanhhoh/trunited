<?php

class Magestore_SpecialOccasion_Helper_Cron extends Mage_Core_Helper_Abstract
{
    public function getConfigData($section, $field, $store = null)
    {
        return Mage::getStoreConfig('specialoccasion/'.$section.'/'.$field, $store);
    }

    public function runCron()
    {
        $this->checkSendEmailRemind();

        $this->checkAndCreateOrder();

        $this->checkAndReset();
    }

    public function getOccasionCollection($is_create_order = false)
    {
        $collection = Mage::getModel('specialoccasion/item')
            ->getCollection()
            ->addFieldToFilter('state', Magestore_SpecialOccasion_Model_Status::STATE_ACTIVE)
            ->setOrder('item_id', 'desc')
            ;

        if(!$is_create_order)
            $collection->addFieldToFilter('status', Magestore_SpecialOccasion_Model_Status::STATUS_ITEM_PENDING);
        else
            $collection->addFieldToFilter('status', Magestore_SpecialOccasion_Model_Status::STATUS_ITEM_PROCESSING);

        return $collection;
    }

    public function checkSendEmailRemind()
    {
        $collection = $this->getOccasionCollection();

        if($collection != null && sizeof($collection) > 0) {
            $default_date = Mage::helper('specialoccasion')->getConfigData('general', 'day_send_email');
            foreach ($collection as $item) {
                $compare_date = $this->compareTime(time(), strtotime($item->getShipDate()));

                if($default_date == $compare_date){
                    $occasion = Mage::getModel('specialoccasion/specialoccasion')->load($item->getSpecialoccasionId());
                    if($occasion != null && $occasion->getId()){
                        $customer = Mage::getModel('customer/customer')->load($occasion->getCustomerId());

                        if($customer != null && $customer->getId()){

                            Mage::helper('specialoccasion')->sendEmailRemind($customer, $item);

                            $item->setStatus(Magestore_SpecialOccasion_Model_Status::STATUS_ITEM_PROCESSING);
                            $item->setIsSentEmail(Magestore_SpecialOccasion_Model_Status::SEND_EMAIL_YES);
                            $item->setSentEmailDate(now());
                            $item->save();
                        }
                    }

                }
            }
        }

        echo 'success';
    }

    public function checkAndCreateOrder()
    {
        $collection = $this->getOccasionCollection(true);

        if($collection != null && sizeof($collection) > 0) {
            $default_date = Mage::helper('specialoccasion')->getConfigData('general', 'day_create_order');
            $prepare_data = array();
            foreach ($collection as $item) {
                $compare_date = $this->compareTime(time(), strtotime($item->getShipDate()));

                $is_valentine_days = $this->checkValentineDay($item->getShipDate());
                if ($default_date == $compare_date || $is_valentine_days) {
                    if(!array_key_exists($item->getSpecialoccasionId(), $prepare_data)){
                        $prepare_data[$item->getSpecialoccasionId()] = array(
                            $item->getId()
                        );
                    } else {
                        $prepare_data[$item->getSpecialoccasionId()][] = $item->getId();
                    }
                }
            }

            if(sizeof($prepare_data) > 0)
            {
                Mage::helper('specialoccasion/order')->prepareOrder($prepare_data);
            }
        }
    }

    /**
     * The orders will be created on Feb 5 for Valentine's Day Feb 13 or 14
     * @param $ship_date
     * @return true | false
     */
    public function checkValentineDay($ship_date)
    {
        $time_stamp = strtotime($ship_date);
        if(date('m', $time_stamp) == 2 && (date('d', $time_stamp) == 13 || date('d', $time_stamp) == 14))
        {
            if(date('m', time()) == 2 && date('d', time()) == 5){
                return true;
            }
        }

        return false;
    }

    public function checkAndReset()
    {
        $collection = Mage::getModel('specialoccasion/item')
            ->getCollection()
            ->addFieldToFilter('state', Magestore_SpecialOccasion_Model_Status::STATE_ACTIVE)
            ->addFieldToFilter('status', array('in' => array(
                Magestore_SpecialOccasion_Model_Status::STATUS_ITEM_CANCEL,
                Magestore_SpecialOccasion_Model_Status::STATUS_ITEM_COMPLETE,
                Magestore_SpecialOccasion_Model_Status::STATUS_ITEM_PENDING,
            )))
            ->setOrder('item_id', 'desc')
        ;

        $default_date_to_expire = Mage::helper('specialoccasion')->getConfigData('general', 'day_to_expire');

        if(sizeof($collection) > 0)
        {
            $transactionSave = Mage::getModel('core/resource_transaction');

            foreach ($collection as $item) {
                $days_to_expire = $this->compareTime(strtotime($item->getShipDate()), time());

                if(date('Y', time()) > date('Y', strtotime($item->getShipDate())) ||
                    $days_to_expire == $default_date_to_expire || $default_date_to_expire == '' ||
                    (date('Y', time()) == date('Y', strtotime($item->getShipDate())) &&
                    (date('m', time()) > date('m', strtotime($item->getShipDate())) ||
                        (date('m', time()) == date('m', strtotime($item->getShipDate())) &&
                            date('d', time()) >= date('d', strtotime($item->getShipDate())))))
                ){
                    $item->setStatus(Magestore_SpecialOccasion_Model_Status::STATUS_ITEM_PENDING);
                    $item->setIsSentEmail(Magestore_SpecialOccasion_Model_Status::SEND_EMAIL_NO);
                    $item->setSentEmailDate(null);
                    $item->setShipDate(date("Y-m-d H:i:s", strtotime("+1 years", strtotime($item->getShipDate()))));
                    $transactionSave->addObject($item);

                    $occasionObj = Mage::getModel('specialoccasion/specialoccasion')
                    ->load($item->getData('specialoccasion_id'));

                    $customer = Mage::getModel('customer/customer')->load($occasionObj->getCustomerId());

                    $occasion_history = Mage::getModel('specialoccasion/history');
                    $history_data = array(
                        'customer_id' => $customer->getId(),
                        'customer_name' => $customer->getName(),
                        'customer_email' => $customer->getEmail(),
                        'order_id' => '',
                        'order_increment_id' => '',
                        'updated_at' => now(),
                        'created_at' => now(),
                        'points' => '',
                        'cost' => '',
                        'products' => '',
                        'status'    => Magestore_SpecialOccasion_Model_Status::HISTORY_STATUS_RESET,
                        'reason'    => Mage::helper('specialoccasion')->__('The event is refreshed at %s', $item->getShipDate())
                    );
                    $occasion_history->setData($history_data);
                    $occasion_history->save();
                }

            }

            try{
                $transactionSave->save();
            } catch (Exception $ex) {
            }
        }
    }

    public function compareTime($start_time, $end_time)
    {
        $diff = abs($end_time - $start_time);

        $years = floor($diff / (365 * 60 * 60 * 24));
        $months = floor(($diff - $years * 365 * 60 * 60 * 24) / (30 * 60 * 60 * 24));
        $days = floor(($diff - $years * 365 * 60 * 60 * 24 - $months * 30 * 60 * 60 * 24) / (60 * 60 * 24));
        $hours = floor(($diff - $years * 365 * 60 * 60 * 24 - $months * 30 * 60 * 60 * 24 - $days * 60 * 60 * 24) / (60 * 60));
        $minutes = floor(($diff - $years * 365 * 60 * 60 * 24 - $months * 30 * 60 * 60 * 24 - $days * 60 * 60 * 24 - $hours * 60 * 60) / 60);
        $seconds = floor(($diff - $years * 365 * 60 * 60 * 24 - $months * 30 * 60 * 60 * 24 - $days * 60 * 60 * 24 - $hours * 60 * 60 - $minutes * 60));

        return $years * 365 + $months * 30 + $days;
    }
}
