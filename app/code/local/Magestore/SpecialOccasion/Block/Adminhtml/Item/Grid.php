<?php

class Magestore_SpecialOccasion_Block_Adminhtml_Item_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
	public function __construct(){
		parent::__construct();
		$this->setId('itemGrid');
		$this->setDefaultSort('item_id');
		$this->setDefaultDir('DESC');
		$this->setSaveParametersInSession(true);
	}

	protected function _prepareCollection(){
        $productsTableName = Mage::getSingleton('core/resource')->getTableName('catalog_product_entity');
        $productsVarcharTableName = Mage::getSingleton('core/resource')->getTableName('catalog_product_entity_varchar');
        $specialOccasionTableName = Mage::getSingleton('core/resource')->getTableName('specialoccasion');
        $firstnameAttr = Mage::getModel('eav/entity_attribute')->loadByCode('1', 'firstname');
        $lastnameAttr = Mage::getModel('eav/entity_attribute')->loadByCode('1', 'lastname');
        $customerVarcharTableName = Mage::getSingleton('core/resource')->getTableName('customer_entity_varchar');
        $customerTableName = Mage::getSingleton('core/resource')->getTableName('customer_entity');

        $collection = Mage::getModel('specialoccasion/item')->getCollection();

        $collection->getSelect()
            ->joinLeft(
                array('spo' => $specialOccasionTableName),
                'spo.specialoccasion_id = main_table.specialoccasion_id',
                array('cid' => 'customer_id')
            );

        $collection->getSelect()
            ->join(
                array('customer_entity' => $customerTableName),
                'spo.customer_id = customer_entity.entity_id', array('customer_email' => 'email'));

        $collection->getSelect()
            ->join(
                array('ce1' => $customerVarcharTableName),
                'ce1.entity_id=spo.customer_id',
                array('firstname' => 'value')
            )
            ->where('ce1.attribute_id=' . $firstnameAttr->getAttributeId())
            ->join(
                array('ce2' => $customerVarcharTableName),
                'ce2.entity_id=spo.customer_id',
                array('lastname' => 'value')
            )
            ->where('ce2.attribute_id=' . $lastnameAttr->getAttributeId())
            ->columns(new Zend_Db_Expr("CONCAT(`ce1`.`value`, ' ',`ce2`.`value`) AS fullname"));

        $entityTypeId = Mage::getModel('eav/entity')
            ->setType('catalog_product')
            ->getTypeId();
        $prodNameAttrId = Mage::getModel('eav/entity_attribute')
            ->loadByCode($entityTypeId, 'name')
            ->getAttributeId();
        $collection->getSelect()
            ->joinLeft(
                array('prod' => $productsTableName),
                'prod.entity_id = main_table.product_id',
                array('sku')
            )
            ->joinLeft(
                array('cpev' => $productsVarcharTableName),
                'cpev.entity_id=prod.entity_id AND cpev.attribute_id='.$prodNameAttrId.'',
                array('name' => 'value')
            );

        $this->setCollection($collection);
		return parent::_prepareCollection();
	}

	protected function _prepareColumns(){
		$this->addColumn('item_id', array(
			'header'	=> Mage::helper('specialoccasion')->__('ID'),
			'align'	 =>'right',
			'width'	 => '20px',
			'index'	 => 'item_id',
		));

//        $this->addColumn('cid', array(
//            'header' => Mage::helper('specialoccasion')->__('Customer ID'),
//			'align'	 =>'left',
//            'width'	 => '40px',
//            'index' => 'cid',
//            'filter_index' => 'spo.customer_id',
//            'renderer' => 'Magestore_SpecialOccasion_Block_Adminhtml_Renderer_Customer_Id',
//		));

        $this->addColumn('fullname', array(
            'header' => Mage::helper('specialoccasion')->__('Customer Name'),
            'align' => 'left',
            'width'	 => '40px',
            'index' => 'fullname',
            'renderer' => 'Magestore_SpecialOccasion_Block_Adminhtml_Renderer_Customer_Name',
            'filter_condition_callback' => array($this, '_filterCustomerNameCallback')
        ));

        $this->addColumn('customer_email', array(
            'header' => Mage::helper('specialoccasion')->__('Customer Email'),
            'align' => 'left',
            'index' => 'customer_email',
            'filter_index' => 'customer_entity.email',
            'renderer' => 'Magestore_SpecialOccasion_Block_Adminhtml_Renderer_Customer_Email',
        ));

        $this->addColumn('name', array(
            'header'	=> Mage::helper('specialoccasion')->__('Product Name'),
            'align'	 =>'left',
            'index'	 => 'name',
            'filter_index'  => 'cpev.value',
            'renderer' => 'Magestore_SpecialOccasion_Block_Adminhtml_Renderer_Product_Name',
        ));

        $this->addColumn('sku', array(
            'header'	=> Mage::helper('specialoccasion')->__('Product SKU'),
            'align'	 =>'left',
            'index'	 => 'sku',
            'filter_index'  => 'prod.sku',
        ));

		$this->addColumn('occasion', array(
			'header'	=> Mage::helper('specialoccasion')->__('Occasion'),
			'width'	 => '150px',
			'index'	 => 'occasion',
		));

        $this->addColumn('ship_date', array(
            'header'    => Mage::helper('trubox')->__('Ship Date'),
            'align'     =>'right',
            'index'     => 'ship_date',
            'type'		=> 'date'
        ));

        $this->addColumn('message', array(
            'header'	=> Mage::helper('specialoccasion')->__('Message'),
            'index'	 => 'message',
        ));

		$this->addColumn('status', array(
			'header'	=> Mage::helper('specialoccasion')->__('Status'),
			'align'	 => 'left',
			'width'	 => '80px',
			'index'	 => 'status',
            'filter_index'  => 'main_table.status',
			'type'		=> 'options',
			'options'	 => Magestore_SpecialOccasion_Model_Status::getOptionItemArray(),
		));

        $this->addColumn('state', array(
            'header'	=> Mage::helper('specialoccasion')->__('State'),
            'align'	 => 'left',
            'width'	 => '80px',
            'index'	 => 'state',
            'type'		=> 'options',
            'options'	 => Magestore_SpecialOccasion_Model_Status::getOptionStateArray(),
        ));

        $this->addColumn('is_sent_email', array(
            'header'	=> Mage::helper('specialoccasion')->__('Is Sent Email'),
            'align'	 => 'left',
            'width'	 => '80px',
            'index'	 => 'is_sent_email',
            'type'		=> 'options',
            'options'	 => Magestore_SpecialOccasion_Model_Status::getOptionSendEmailArray(),
        ));

        $this->addColumn('sent_email_date', array(
            'header'    => Mage::helper('trubox')->__('Email Sent At'),
            'align'     =>'right',
            'index'     => 'sent_email_date',
            'type'		=> 'date'
        ));

        $this->addColumn('created_at', array(
            'header'    => Mage::helper('trubox')->__('Created At'),
            'align'     =>'right',
            'index'     => 'created_at',
            'type'		=> 'date'
        ));

        $this->addColumn('updated_at', array(
            'header'    => Mage::helper('trubox')->__('Updated At'),
            'align'     =>'right',
            'index'     => 'updated_at',
            'type'		=> 'date'
        ));

		$this->addExportType('*/*/exportCsv', Mage::helper('specialoccasion')->__('CSV'));
		$this->addExportType('*/*/exportXml', Mage::helper('specialoccasion')->__('XML'));

		return parent::_prepareColumns();
	}

	protected function _prepareMassaction(){
		$this->setMassactionIdField('item_id');
		$this->getMassactionBlock()->setFormFieldName('specialoccasion');

		$this->getMassactionBlock()->addItem('delete', array(
			'label'		=> Mage::helper('specialoccasion')->__('Delete'),
			'url'		=> $this->getUrl('*/*/massDelete'),
			'confirm'	=> Mage::helper('specialoccasion')->__('Are you sure?')
		));

		return $this;
	}

    protected function _filterCustomerNameCallback($collection, $column)
    {
        if (!$value = $column->getFilter()->getValue()) {
            return $this;
        }

        $firstnameAttr = Mage::getModel('eav/entity_attribute')->loadByCode('1', 'firstname');
        $lastnameAttr = Mage::getModel('eav/entity_attribute')->loadByCode('1', 'lastname');
        $customerVarcharTableName = Mage::getSingleton('core/resource')->getTableName('customer_entity_varchar');

        if (empty($value)) {
            $collection->getSelect()
                ->join(array('ce11' => $customerVarcharTableName), 'ce11.entity_id=spo.customer_id', array('firstname' => 'value'))
                ->where('ce11.attribute_id=' . $firstnameAttr->getAttributeId())
                ->join(array('ce21' => $customerVarcharTableName), 'ce21.entity_id=spo.customer_id', array('lastname' => 'value'))
                ->where('ce21.attribute_id=' . $lastnameAttr->getAttributeId())
                ->columns(new Zend_Db_Expr("CONCAT(`ce11`.`value`, ' ',`ce21`.`value`) AS fullname"))
            ;
        } else {
            $collection->getSelect()
                ->join(array('ce11' => $customerVarcharTableName), 'ce11.entity_id=spo.customer_id', array('firstname' => 'value'))
                ->where('ce11.attribute_id=' . $firstnameAttr->getAttributeId())
                ->join(array('ce21' => $customerVarcharTableName), 'ce21.entity_id=spo.customer_id', array('lastname' => 'value'))
                ->where('ce21.attribute_id=' . $lastnameAttr->getAttributeId().' and (ce21.value like \'%'.$value.'%\' or ce11.value like \'%'.$value.'%\')')
                ->columns(new Zend_Db_Expr("CONCAT(`ce11`.`value`, ' ',`ce21`.`value`) AS fullname"))
            ;
        }

        return $this;
    }
}
