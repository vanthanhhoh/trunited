<?php
/**
 * Created by PhpStorm.
 * User: longvuxuan
 * Date: 8/31/17
 * Time: 5:39 PM
 */

class Magestore_SpecialOccasion_Block_Adminhtml_Renderer_History_Product extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {
        $value = $row->getData($this->getColumn()->getIndex());
        $data = json_decode($value, true);

        $html = '';

        if($data[0]['product_id'] > 0){
            $html .= '<a href="'.Mage::helper("adminhtml")->getUrl("adminhtml/catalog_product/edit", array('id'=>$data[0]['product_id'])).'" target="_blank">'.$data[0]['product_name'].'</a> <br /> ';
            $html .= 'ID: <b>'.$data[0]['product_id'].'</b> - ';
            $html .= 'Qty: <b>'.$data[0]['qty'].' </b> - ';
            $html .= 'Price: <b>'.$data[0]['price'].'</b>';
        }

        return '<span>' . $html . '</span>';

    }

}
