<?php

class Magestore_SpecialOccasion_Block_Specialoccasion extends Mage_Core_Block_Template
{
	public function _prepareLayout(){
		return parent::_prepareLayout();
	}

    public function getAddNewAction()
    {
        return $this->getUrl('*/*/add');
    }

    public function getUpdateAction()
    {
        return $this->getUrl('*/*/update');
    }

    public function getOccasionsCollection()
    {
        return Mage::helper('specialoccasion')->getItemCollectionByCustomerId();
    }

    public function getCurrentId()
    {
        return Mage::getSingleton('customer/session')->getCustomer()->getId();
    }

    public function getShipName($item_id)
    {
        return Mage::helper('specialoccasion')->getShipName($item_id);
    }

    public function getErrorMessage($item_id, $customer_id = null)
    {
        if($customer_id == null)
            $customer_id = $this->getCurrentId();

        $history = Mage::getModel('specialoccasion/history')->getCollection()
            ->addFieldToFilter('customer_id', $customer_id)
            ->addFieldToFilter('reason', array('like'   => '%ITEM ID: '.$item_id.' -%'))
            ->setOrder('history_id', 'desc')
            ->getFirstItem()
            ;

        if($history->getId()) {
            $mess = trim(str_replace('ITEM ID: '.$item_id.' -','',$history->getReason()));
            $mess = trim(str_replace('Authorize.Net CIM Gateway','',$mess));
            return $mess;
        }
        else
            return null;
    }
}
