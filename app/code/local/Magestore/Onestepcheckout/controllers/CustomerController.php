<?php
require_once   Mage::getBaseDir('lib').'/Twilio/autoload.php';
use Twilio\Rest\Client;

class Magestore_Onestepcheckout_CustomerController extends Mage_Core_Controller_Front_Action
{
	/**
     * Retrieve customer session model object
     *
     * @return Mage_Customer_Model_Session
     */
    protected function _getSession()
    {
        return Mage::getSingleton('customer/session');
    }

    /**
     * Get model by path
     *
     * @param string $path
     * @param array|null $arguments
     * @return false|Mage_Core_Model_Abstract
     */
    public function _getModel($path, $arguments = array())
    {
        return Mage::getModel($path, $arguments);
    }

    /**
     * Get Helper
     *
     * @param string $path
     * @return Mage_Core_Helper_Abstract
     */
    protected function _getHelper($path)
    {
        return Mage::helper($path);
    }

	public function loginPostAction()
	{
		if ($this->_getSession()->isLoggedIn()) {
            $result = array(
            	'success'	=> 1,
            	'url'	=> Mage::getUrl('onestepcheckout')
            );
        } else {
        	$session = $this->_getSession();

	        if ($this->getRequest()->isPost()) {
	            $login = $this->getRequest()->getPost('login');
	            if (!empty($login['username']) && !empty($login['password'])) {
	                try {
	                    $session->login($login['username'], $login['password']);
	                    $result = array(
			            	'success'	=> 1,
			            	'url'	=> Mage::getUrl('onestepcheckout')
			            );
	                } catch (Mage_Core_Exception $e) {
	                    switch ($e->getCode()) {
	                        case Mage_Customer_Model_Customer::EXCEPTION_EMAIL_NOT_CONFIRMED:
	                            $value = $this->_getHelper('customer')->getEmailConfirmationUrl($login['username']);
	                            $message = $this->_getHelper('customer')->__('This account is not confirmed. <a href="%s">Click here</a> to resend confirmation email.', $value);
	                            break;
	                        case Mage_Customer_Model_Customer::EXCEPTION_INVALID_EMAIL_OR_PASSWORD:
	                            $message = $e->getMessage();
	                            break;
	                        default:
	                            $message = $e->getMessage();
	                    }
	                    $session->addError($message);
	                    $session->setUsername($login['username']);

	                    $result = array(
			            	'success'	=> 0,
			            	'message'	=> $message
			            );
	                } catch (Exception $e) {
	                    $result = array(
			            	'success'	=> 0,
			            	'message'	=> $message
			            );
	                }
	            } else {
	            	$result = array(
		            	'success'	=> 0,
		            	'message'	=> $this->__('Login and password are required.')
		            );
	            }
	        }
        }

        echo json_encode($result);
	}

	public function forgotPasswordPostAction()
	{
		$email = (string) $this->getRequest()->getPost('forgot_email');

        if ($email) {
            
            if (!Zend_Validate::is($email, 'EmailAddress')) {
                $result = array(
	            	'success'	=> 0,
	            	'message'	=> $this->__('Invalid email address.')
	            );
            }

            /** @var $customer Mage_Customer_Model_Customer */
            $customer = $this->_getModel('customer/customer')
                ->setWebsiteId(Mage::app()->getStore()->getWebsiteId())
                ->loadByEmail($email);

            if ($customer->getId()) {
                try {
                    $newResetPasswordLinkToken =  $this->_getHelper('customer')->generateResetPasswordLinkToken();
                    $customer->changeResetPasswordLinkToken($newResetPasswordLinkToken);
                    $customer->sendPasswordResetConfirmationEmail();
                } catch (Exception $exception) {
                	$result = array(
		            	'success'	=> 0,
		            	'message'	=> $exception->getMessage()
		            );
                }
            }

            $result = array(
            	'success'	=> 1,
            	'message'	=>  $this->_getHelper('customer')
                ->__('If there is an account associated with %s, you will receive an email with a link to reset your password.',
                    $this->_getHelper('customer')->escapeHtml($email))
            );
        } else {
        	$result = array(
            	'success'	=> 0,
            	'message'	=> $this->__('Please enter your email.')
            );
        }

        echo json_encode($result);
	}

    public function registerPostAction()
    {
        $session = Mage::getSingleton('customer/session');
        $firstName = $this->getRequest()->getPost('firstname');
        $lastName = $this->getRequest()->getPost('lastname');
        $pass = $this->getRequest()->getPost('password');
        $passConfirm = $this->getRequest()->getPost('confirmation');
        $email = $this->getRequest()->getPost('email');
        $preferred_language = $this->getRequest()->getPost('preferred_language');

        $customer = Mage::getModel('customer/customer')
                ->setFirstname($firstName)
                ->setLastname($lastName)
                ->setEmail($email)
                ->setPassword($pass)
                ->setConfirmation($passConfirm)
                ->setPreferredLanguage($preferred_language)
                ->setIsSubscribed(1)
                ;

        try {
            $customer->save();
            
            Mage::dispatchEvent('customer_register_success', array(
                'account_controller' => $this, 
                'customer' => $customer,
                'no_redirect'   => 1
            ));
            $result = array(
                'success' => 1,
                'url'   => Mage::getUrl('onestepcheckout')
            );
            $session->setCustomerAsLoggedIn($customer);
        } catch (Exception $e) {
            $result = array('success' => 0, 'error' => $e->getMessage());
        }
        // if (isset($result['error']))
        //     $session->setData('register_result_error', $result['error']);

        echo json_encode($result);
    }

    public function sendCodeAction()
    {
        $verify_helper = Mage::helper('custompromotions/verify');
        $mobile_code = $verify_helper->getMobileCode();

        if(!$verify_helper->isEnable()){
            $result = array(
                'error' => 1,
                'message'   => Mage::helper('custompromotions')->__('This feature is disabled'),
            );
        } else {
            $data = $this->getRequest()->getParams();

            if(!$data['phone_number']){
                $result = array(
                    'error' => 1,
                    'message'   => Mage::helper('custompromotions')->__('The phone is not exists'),
                );
            } else if(substr($data['phone_number'],1,1) == $mobile_code) {                
                $result = array(
                    'error' => 1,
                    'message'   => Mage::helper('custompromotions')->__('The mobile number only has 10 digits and don\'t allow begin with %s',$mobile_code),
                );
            } else {
                $mobile_prefix = $verify_helper->getMobileCode();
                $phone = $verify_helper->getPhoneNumberFormat($mobile_prefix,$data['phone_number']);

                $is_verified = $verify_helper->isVerified($data['phone_number']);

                if($is_verified){
                    $result = array(
                        'error' => 1,
                        'message'   => Mage::helper('custompromotions')->__('This mobile number already exists on a customer account. Please enter a new mobile number.')
                    );
                } else {

                    try{
                        /* sending code to customer */
                        $sid = $verify_helper->getAccountSID();
                        $token = $verify_helper->getAuthToken();
                        $from = $verify_helper->getSenderNumber();
                        $new_code = $verify_helper->generateRandomString();
                        $message = Mage::helper('custompromotions')->__('Here is your Trunited verification code: %s',$new_code);
                        $client = new Client($sid, $token);
                        $client->messages->create(
                            $phone,
                            array(
                                'messagingServiceSid' => "MGb9626abfac0e54ccc6b424dcd3dc325d",
                                'body' => $message
                            )
                        );
                        /* end sending sms */
                        Mage::log('Mobile Code: - '.$phone.' - Quantity: '.$new_code, null, 'mobileCode.log');
                        /* save to database: customer_verify_mobile table */
                        $phone_to_database = $verify_helper->formatPhoneToDatabase($data['phone_number']);
                        $verify_helper->saveVerify($phone_to_database, $new_code);
                        /* end save to database: customer_verify_mobile table */

                        $result = array(
                            'error' => 0,
                            'message'   => Mage::helper('custompromotions')->__('The verification code has been sent to your mobile successfully.')
                        );
                        /* set data to session */
                        Mage::getSingleton('core/session')->setPhoneActive($data['phone_number']);
                        Mage::getSingleton('core/session')->setCodeActive($new_code);
                        /* end set data to session */
                    } catch (Exception $ex){
                        $result = array(
                            'error' => 1,
                            'message'   => Mage::helper('custompromotions')->__('Error sending message: ' . str_replace('[HTTP 400] Unable to create record:','',$ex->getMessage()))
                        );
                    }
                }
            }
        }
        
        echo json_encode($result);
    }

    public function backAction()
    {
        Mage::getSingleton('core/session')->unsPhoneActive();
        Mage::getSingleton('core/session')->unsCodeActive();
        Mage::getSingleton('core/session')->unsVerify();
    }

    public function verifyCodeAction()
    {
        $verify_helper = Mage::helper('custompromotions/verify');
        $data = $this->getRequest()->getParams();
        if(!isset($data['verify_code']) || $data['verify_code'] == null)
        {
            $result = array(
                'error' => 1,
                'message'  => Mage::helper('custompromotions')->__('The code was wrong.')
            );
        } else {
            $phone = Mage::getSingleton('core/session')->getPhoneActive();
            $phone_to_database = $verify_helper->formatPhoneToDatabase($phone);
            $check_verified = $verify_helper->verify($phone_to_database, $data['verify_code']);
            if($check_verified == Magestore_Custompromotions_Model_Verifymobile::VERIFY_SUCCESS)
            {
                Mage::getSingleton('core/session')->setVerify(true);
                $result = array(
                    'error' => 0,
                    'message'  => Mage::helper('custompromotions')->__('Thank you! Your mobile number has been verified.')
                );
            } else if($check_verified == Magestore_Custompromotions_Model_Verifymobile::VERIFY_ERROR_NON_EXIST) {
                $result = array(
                    'error' => 1,
                    'message'  => Mage::helper('custompromotions')->__('Verification code is incorrect. Please enter it again or click <b><a href="'.Mage::getUrl('*/*/resendCode').'" title="Resend Code" class="resend_code">RESEND CODE</a></b>')
                );
            } else if($check_verified == Magestore_Custompromotions_Model_Verifymobile::VERIFY_ERROR_VERIFIED) {
                $result = array(
                    'error' => 1,
                    'message'  => Mage::helper('custompromotions')->__('This mobile number has already been verified.')
                );
            } else if($check_verified == Magestore_Custompromotions_Model_Verifymobile::VERIFY_ERROR_OTHER) {
                $result = array(
                    'error' => 1,
                    'message'  => Mage::helper('custompromotions')->__('Verification code incorrect, please try again.')
                );
            }
        }

        echo json_encode($result);
    }

    public function resendCodeAction()
    {
        $verify_helper = Mage::helper('custompromotions/verify');
        $phone_number = Mage::getSingleton('core/session')->getPhoneActive();
        if($phone_number == null){
            $result = array(
                'error' => 1,
                'message'  => Mage::helper('custompromotions')->__('The mobile number does not exist.')
            );
        }

        $mobile_prefix = $verify_helper->getMobileCode();
        $phone = $verify_helper->getPhoneNumberFormat($mobile_prefix,$phone_number);

        $is_verified = $verify_helper->isVerified($phone_number);
        if($is_verified){
            $result = array(
                'error' => 1,
                'message'  => Mage::helper('custompromotions')->__('This mobile number already exists on a customer account. Please enter a new mobile number.')
            );
        } else {
            try{
                /* sending code to customer */
                $sid = $verify_helper->getAccountSID();
                $token = $verify_helper->getAuthToken();
                $from = $verify_helper->getSenderNumber();
                $new_code = $verify_helper->generateRandomString();
                $message = Mage::helper('custompromotions')->__('Here is your Trunited verification code: %s',$new_code);
                $client = new Client($sid, $token);
                $client->messages->create(
                    $phone,
                    array(
                        'messagingServiceSid' => "MGb9626abfac0e54ccc6b424dcd3dc325d",
                        'body' => $message
                    )
                );
                /* end sending sms */

                /* save to database: customer_verify_mobile table */
                $verify_helper->saveVerify($phone_number, $new_code);
                /* end save to database: customer_verify_mobile table */

                /* set data to session */
                Mage::getSingleton('core/session')->setPhoneActive($phone_number);
                Mage::getSingleton('core/session')->setCodeActive($new_code);
                /* end set data to session */
                
                $result = array(
                    'error' => 0,
                    'message'  => Mage::helper('custompromotions')->__('The verification code has been sent to your mobile successfully.')
                );

            } catch (Exception $ex){
                $result = array(
                    'error' => 1,
                    'message'  => Mage::helper('custompromotions')->__('Error sending message: ' . str_replace('[HTTP 400] Unable to create record:','',$ex->getMessage()))
                );
            }
        }

        echo json_encode($result);
    }
}