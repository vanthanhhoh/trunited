<?php
/**
 * Created by PhpStorm.
 * User: anthony
 * Date: 3/17/17
 * Time: 11:27 AM
 */

class Magestore_Onestepcheckout_Block_Signin extends Mage_Core_Block_Template
{
    public function _prepareLayout(){
        return parent::_prepareLayout();
    }

    public function getCheckoutUrl(){
    	return Mage::getUrl('onestepcheckout', array('guest'   => true));
    }

    public function getLoginUrl(){
    	return Mage::getUrl('onestepcheckout/customer/loginPost');
    }

    public function getRegisterUrl()
    {
        return Mage::getUrl('onestepcheckout/customer/registerPost');
    }

    public function getForgotPasswordUrl()
    {
    	return Mage::getUrl('onestepcheckout/customer/forgotPasswordPost');
    }

    public function getSendCodeUrl()
    {
        return Mage::getUrl('onestepcheckout/customer/sendCode');
    }

    public function getVerifyCodeUrl()
    {
        return Mage::getUrl('onestepcheckout/customer/verifyCode');
    }

    public function getMobileBackUrl()
    {
        return Mage::getUrl('onestepcheckout/customer/back');
    }

    public function isEnableVerifyMobile()
    {
    	return Mage::helper('custompromotions/verify')->isEnable();
    }

    public function getAffiliateName(){

        $models = Mage::getModel('affiliateplus/account')->getCollection()
            ->addFieldToSeldct('name')
            ->addFieldToSelect('account_id')
        ;
        $jsonArray = array();
        $jsonAffiliateName = array();

        foreach ($models as $model){
            $jsonArray['label'] = $model->getName();
            $jsonArray['value'] = $model->getAccountId();
            array_push($jsonAffiliateName,$jsonArray);
        }

        return json_encode($jsonAffiliateName);
    }

    public function getAffiliateInfoFromCookie()
    {
        return  Mage::helper('affiliateplus/account')->getAffiliateInfoFromCookie();
    }

    public function getCookie()
    {
        return Mage::getSingleton('core/cookie');
    }
}