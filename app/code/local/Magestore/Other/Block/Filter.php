<?php

class Magestore_Other_Block_Filter extends Mage_Core_Block_Template
{
    protected $_category;

    protected $_merchant;

    protected $_merchanttype;

    public function _prepareLayout()
    {
        return parent::_prepareLayout();
    }

    public function setCategory($cat_id)
    {
        $this->_category = Mage::getModel('catalog/category')->load($cat_id);
        return $this;
    }

    public function getCategory()
    {
        if ($this->_category == null)
            $this->setCategory(71);

        return $this->_category;
    }

    public function setMerchant($merchants)
    {
        $this->_merchant = $merchants;
        return $this;
    }

    public function getMerchant()
    {
        if ($this->_merchant == null){
            $collection = Mage::getModel('manageapi/popshopsmerchant')->getCollection()
                ->addFieldToSelect('merchant_id')
                ->addFieldToSelect('name')
                ->setOrder('name', 'asc')
            ;

            $data = array();

            if(sizeof($collection) > 0){
                foreach ($collection as $mer) {
                    $data[] = array(
                        'id'    => $mer->getMerchantId(),
                        'name'  => $mer->getName(),
                    );
                }
            }

            $this->setMerchant($data);
        }

        return $this->_merchant;
    }

    public function setMerchanttype($merchanttypes)
    {
        $this->_merchanttype = $merchanttypes;
        return $this;
    }

    public function getMerchanttype()
    {
        if ($this->_merchanttype == null){
            $collection = Mage::getModel('manageapi/merchanttype')->getCollection()
                ->addFieldToSelect('type_id')
                ->addFieldToSelect('name')
                ->addFieldToFilter('name', array('nlike' => '%Adult%'))
                ->setOrder('name', 'asc')
            ;

            $data = array();

            if(sizeof($collection) > 0){
                foreach ($collection as $mer) {
                    $data[] = array(
                        'id'    => $mer->getTypeId(),
                        'name'  => $mer->getName(),
                    );
                }
            }
            
            $this->setMerchanttype($data);
        }

        return $this->_merchanttype;
    }

    public function getMainPage()
    {
        return $this->getCategory()->getUrl();
    }

    public function getCountProduct($value)
    {
        $todayStartOfDayDate = Mage::app()->getLocale()->date()
            ->setTime('00:00:00')
            ->toString(Varien_Date::DATETIME_INTERNAL_FORMAT);

        $todayEndOfDayDate = Mage::app()->getLocale()->date()
            ->setTime('23:59:59')
            ->toString(Varien_Date::DATETIME_INTERNAL_FORMAT);
        $_productCollection = $this->getCategory()->getProductCollection();
        $_productCollection->addAttributeToFilter('manufacturer', $value)
            ->addAttributeToFilter('status', 1)
            ->addAttributeToFilter('visibility', 4);

        $_productCollection->clear()->addAttributeToFilter('news_from_date', array('or' => array(
            0 => array('date' => true, 'to' => $todayEndOfDayDate),
            1 => array('is' => new Zend_Db_Expr('null')))
        ), 'left')
            ->addAttributeToFilter('news_to_date', array('or' => array(
                0 => array('date' => true, 'from' => $todayStartOfDayDate),
                1 => array('is' => new Zend_Db_Expr('null')))
            ), 'left')->load();

        return sizeof($_productCollection);

    }

    public function getUrlPage($params = array())
    {
        $urlParams = array();
        $urlParams['_current'] = true;
        $urlParams['_escape'] = true;
        $urlParams['_use_rewrite'] = true;
        $urlParams['_query'] = $params;
        return $this->getUrl('*/*/*', $urlParams);
    }

    public function getUrlManufacturer($manufacturer)
    {
        return $this->getUrlPage(
            array(
                'manufacturer' => base64_encode($manufacturer)
            )
        );
    }

    public function getUrlCategory($cat)
    {
        return $this->getUrlPage(
            array(
                'cat' => base64_encode($cat)
            )
        );
    }

    public function getUrlWalmart($walmart)
    {
        return $this->getUrlPage(
            array(
                'walmart' => base64_encode($walmart)
            )
        );
    }

    public function getUrlBestseller()
    {
        return $this->getUrlPage(
            array(
                'bestsellers' => 1
            )
        );
    }

    public function getParamManufacturer()
    {
        $manufacturer = $this->getRequest()->getParam('manufacturer');
        if (isset($manufacturer) && base64_decode($manufacturer)) {
            return base64_decode($manufacturer);
        } else {
            return null;
        }
    }

    public function getParamWalmart()
    {
        $walmart = $this->getRequest()->getParam('walmart');
        if (isset($walmart) && base64_decode($walmart)) {
            return base64_decode($walmart);
        } else {
            return null;
        }
    }

    public function getParamPromotionType()
    {
        $promotion_type_param = $this->getRequest()->getParam('promotion_type');
        if (isset($promotion_type_param) && base64_decode($promotion_type_param)) {
            return base64_decode($promotion_type_param);
        } else {
            return null;
        }
    }

    public function getAllCategoryPaths()
    {
        return Mage::helper('manageapi')->getAllCategoryPaths();
    }

    public function getParamCategoryPath()
    {
        $cat = $this->getRequest()->getParam('cat');
        if (isset($cat) && base64_decode($cat)) {
            return base64_decode($cat);
        } else {
            return null;
        }
    }

    public function getSpecialOfferData()
    {
        return Mage::helper('other')->getSpecialOfferData();
    }

    public function getCountProductSpecialOffer($special_offer)
    {
        $todayStartOfDayDate = Mage::app()->getLocale()->date()
            ->setTime('00:00:00')
            ->toString(Varien_Date::DATETIME_INTERNAL_FORMAT);

        $todayEndOfDayDate = Mage::app()->getLocale()->date()
            ->setTime('23:59:59')
            ->toString(Varien_Date::DATETIME_INTERNAL_FORMAT);

        $_productCollection = $this->getCategory()->getProductCollection();
        $_productCollection->addAttributeToFilter('specialOffer', array('like' => '%' . $special_offer . '%'))
            ->addAttributeToFilter('status', 1)
            ->addAttributeToFilter('visibility', 4);

        $_productCollection->clear()->addAttributeToFilter('news_from_date', array('or' => array(
            0 => array('date' => true, 'to' => $todayEndOfDayDate),
            1 => array('is' => new Zend_Db_Expr('null')))
        ), 'left')
            ->addAttributeToFilter('news_to_date', array('or' => array(
                0 => array('date' => true, 'from' => $todayStartOfDayDate),
                1 => array('is' => new Zend_Db_Expr('null')))
            ), 'left')->load();

        return sizeof($_productCollection);

    }

    public function getBestsellersData()
    {
        $collection = Mage::getModel('catalog/product')
            ->getCollection()
            ->addAttributeToSelect('entity_id')
            ->addAttributeToFilter('is_bestsellers', array("eq" => 1))
            ;

        return sizeof($collection);
    }

    public function getBestsellerParam()
    {
        $bestsellers = $this->getRequest()->getParam('bestsellers');
        if (isset($bestsellers) && base64_decode($bestsellers)) {
            return base64_decode($bestsellers);
        } else {
            return null;
        }
    }

    public function getCountProductBestsellers($best_seller)
    {
        $todayStartOfDayDate = Mage::app()->getLocale()->date()
            ->setTime('00:00:00')
            ->toString(Varien_Date::DATETIME_INTERNAL_FORMAT);

        $todayEndOfDayDate = Mage::app()->getLocale()->date()
            ->setTime('23:59:59')
            ->toString(Varien_Date::DATETIME_INTERNAL_FORMAT);

        $_productCollection = $this->getCategory()->getProductCollection();
        $_productCollection->addAttributeToFilter('bestsellers', array('like' => '%' . $best_seller . '%'))
            ->addAttributeToFilter('status', 1)
            ->addAttributeToFilter('visibility', 4);

        $_productCollection->clear()->addAttributeToFilter('news_from_date', array('or' => array(
            0 => array('date' => true, 'to' => $todayEndOfDayDate),
            1 => array('is' => new Zend_Db_Expr('null')))
        ), 'left')
            ->addAttributeToFilter('news_to_date', array('or' => array(
                0 => array('date' => true, 'from' => $todayStartOfDayDate),
                1 => array('is' => new Zend_Db_Expr('null')))
            ), 'left')->load();

        return sizeof($_productCollection);

    }

    public function getSearchApiUrl()
    {
        $helper = Mage::helper('manageapi');
        $url = '';

        if($helper->getDataConfig('enable', 'popshops_api')){
            $url .= $helper->getDataConfig('product_api', 'popshops_api');

            $url .= '&account=' . $helper->getDataConfig('account_public_api_key', 'popshops_api');
            $url .= '&catalog=' . $helper->getDataConfig('catalog_key', 'popshops_api');
            $url .= '&keyword=';

            $url = str_replace('?&', '?', $url);

        }

        return $url;
    }

    public function getResultFromSearchingAPI()
    {
        $url = $this->getSearchApiUrl();
        $url .= $this->getRequest()->getPost('q') != null ? $this->getRequest()->getPost('q') : '';
        $url .= '&page=1&results_per_page=100';

        $_data = Mage::helper('manageapi')->getContentByCurl($url);
        $dt = json_decode($_data, true);

        zend_debug::dump($dt);
    }

    public function getDataPost()
    {
        return $this->getRequest()->getPost();
    }

    public function getPercentOffMin()
    {
        $data = $this->getDataPost();
        if ($data['is_advanced_search'] == 0 || !isset($data['percent_off']) || $data['percent_off'] == '') {
            return Mage::helper('manageapi')->getDataConfig('percent_off_min', 'product_popshops_api');
        } else {
            $percent = explode('-', str_replace('%', '', $data['percent_off']));
            return trim($percent[0]);
        }
    }

    public function getPercentOffMax()
    {
        $data = $this->getDataPost();
    
        if ($data['is_advanced_search'] == 0 || !isset($data['percent_off']) || $data['percent_off'] == '') {
            return Mage::helper('manageapi')->getDataConfig('percent_off_max', 'product_popshops_api');
        } else {
            $percent = explode('-', str_replace('%', '', $data['percent_off']));
            return trim($percent[1]);
        }
    }

    public function getPriceMin()
    {
        $data = $this->getDataPost();
        if ($data['is_advanced_search'] == 0 || !isset($data['price']) || $data['price'] == '') {
            return Mage::helper('manageapi')->getDataConfig('price_min', 'product_popshops_api');
        } else {
            $percent = explode('-', str_replace('$', '', $data['price']));
            return trim($percent[0]);
        }
    }

    public function getPriceMax()
    {
        $data = $this->getDataPost();
        if ($data['is_advanced_search'] == 0 || !isset($data['price']) || $data['price'] == '') {
            return Mage::helper('manageapi')->getDataConfig('price_max', 'product_popshops_api');
        } else {
            $percent = explode('-', str_replace('$', '', $data['price']));
            return trim($percent[1]);
        }
    }
}
