<?php

class Magestore_Other_Block_Toolbar extends Mage_Catalog_Block_Product_List_Toolbar
{
	/**
	 * Init Toolbar
	 *
	 */
	protected function _construct()
	{
		parent::_construct();

		$this->_availableMode = array('grid' => $this->__('Grid'));

		$this->disableViewSwitcher();

		$this->setTemplate('other/toolbar.phtml');
	}

	public function getPagerHtml()
	{
		$pagerBlock = $this->getLayout()->createBlock('page/html_pager');
		if ($pagerBlock instanceof Varien_Object)
		{
			$pagerBlock->setTemplate('other/pager.phtml');
			$pagerBlock->setAvailableLimit($this->getAvailableLimit());
			$pagerBlock->setUseContainer(false)
				->setShowPerPage(false)
				->setShowAmounts(false)
				->setLimitVarName($this->getLimitVarName())
				->setPageVarName($this->getPageVarName())
				->setLimit($this->getLimit())
				->setFrameLength(Mage::getStoreConfig('design/pagination/pagination_frame'))
				->setJump(Mage::getStoreConfig('design/pagination/pagination_frame_skip'))
				->setCollection($this->getCollection());
			return $pagerBlock->toHtml();
		}

		return '';
	}

}