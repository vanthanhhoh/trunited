<?php

class Magestore_Other_Block_Giftcard extends Mage_Core_Block_Template
{
    public function __construct()
    {
        parent::__construct();

        $data = $this->getRequest()->getParams();

        $filter_cat = $this->filterCurrentCat();
        if(isset($data['c']) && $filter_cat != null){
            $list_categories = array(
                $filter_cat
            );
        } else {
            $list_categories = $this->getAllCategories();
        }

        $category_table_name = Mage::getSingleton('core/resource')->getTableName('catalog/category_product');

        $collection = Mage::getResourceModel('catalog/product_collection')
            ->distinct(true)
            ->joinField('category_id', $category_table_name, 'category_id', 'product_id=entity_id', null, 'left')
            ->addAttributeToFilter('category_id', array('in' => $list_categories))
            ->addAttributeToSelect('*')
            ->addAttributeToFilter('status', 1)
            ->addAttributeToFilter('visibility', 4)
        ;

        if(isset($data['a']) && $data['a'] != '#')
        {
            $collection->addAttributeToFilter('name', array('like'  => ''.$data['a'].'%'));
        }

        if(isset($data['order']))
        {
            switch ($data['order']){
                case 1:
                    $collection->setOrder('name', 'asc');
                    break;

                case 2:
                    $collection->setOrder('name', 'desc');
                    break;

                case 3:
                    $collection->setOrder('created_at', 'desc');
                    break;

                default:
                    $collection->setOrder('name', 'asc');
                    break;
            }
        }

        $collection->getSelect()->group('e.entity_id');

        $this->setCollection($collection);
    }

    protected function _prepareLayout()
    {
        parent::_prepareLayout();
        $toolbar = $this->getLayout()->createBlock('other/toolbar');

        $toolbar->setAvailableOrders(array(
            '1' => 'Name A to Z',
            '2' => 'Name Z to A',
            '3' => 'Recently Added',
        ));
        $toolbar->setDefaultOrder('entity_id');
        $toolbar->setDefaultDirection("asc");
        $toolbar->setCollection($this->getCollection());
        $this->setChild('toolbar', $toolbar);

        $this->getCollection()->load();

        return $this;
    }

    public function getToolbarHtml()
    {
        return $this->getChildHtml('toolbar');
    }

    public function getPagerHtmlFooter()
    {
        return $this->getChildHtml('toolbar.footer');
    }

    public function getMainPage()
    {
        $cms_page = Mage::helper('other')->getConfigData('gift_card_new', 'cms_page');
        return $this->getUrl($cms_page);
    }

    public function getTopHtml()
    {
        $top_block = Mage::helper('other')->getConfigData('gift_card_new', 'banner_block');
        return $this->getLayout()->createBlock('cms/block')
            ->setBlockId($top_block)
            ->toHtml();
    }

    public function getMainCategory()
    {
        return Mage::helper('other')->getConfigData('gift_card_new', 'main_category');
    }

    public function getAllCategories()
    {
        $main_category = Mage::getModel('catalog/category')->load($this->getMainCategory());

        $default_ids = array();
        if($main_category != null && $main_category->getId())
        {
            $default_ids[] = $main_category->getId();
            $default_ids = array_merge($default_ids, $this->recursiveCategories($main_category));
        }

        return $default_ids;
    }

    public function recursiveCategories($cat){
        $rs = array();
        $children = $this->getChildCategories($cat);
        if(sizeof($children) > 0)
        {
            foreach ($children as $child) {
                $rs[] = $child->getId();
                $rs = array_merge($rs, $this->recursiveCategories($child));
            }
        }

        return $rs;
    }

    public function getDataLeft()
    {
        $main_category = Mage::getModel('catalog/category')->load($this->getMainCategory());
        if ($main_category != null && $main_category->getId()) {
            return $this->getChildCategories($main_category);
        }

        return null;
    }

    public function getChildCategories($category)
    {
        return $category->getChildrenCategories();
    }

    public function filterCat($category_id)
    {
        $current_url = Mage::helper('core/url')->getCurrentUrl();
        if (strpos($current_url, '?') > 0) {
            if (strpos($current_url, 'cat=') > 0) {

                return $current_url . '&cat=' . base64_encode($category_id);
            } else {
                return $current_url . '&cat=' . base64_encode($category_id);
            }
        } else {
            return Mage::helper('core/url')->getCurrentUrl() . '?cat=' . base64_encode($category_id);
        }
    }

    public function displayName($name)
    {
        if(strlen($name) > 18)
            $name = substr($name, 0, 18).'...';

        return $name;
    }

    public function getUrlPage($params=array())
    {
        $urlParams = array();
        $urlParams['_current']  = true;
        $urlParams['_escape']   = true;
        $urlParams['_use_rewrite']   = true;
        $urlParams['_query']    = $params;
        return $this->getUrl('*/*/*', $urlParams);
    }

    public function getUrlCategoryPage($category_id)
    {
        return $this->getUrlPage(
            array(
                'c' => base64_encode($category_id)
            )
        );
    }

    public function handleCat()
    {
        $filter_cat = $this->filterCurrentCat();
        if($filter_cat != null)
            return Mage::getModel('catalog/category')->load($filter_cat);
        return null;
    }

    public function filterCurrentCat()
    {
        $c = $this->getRequest()->getParam('c');
        if(isset($c) && base64_decode($c)){
            return base64_decode($c);
        } else {
            return null;
        }
    }

    public function getActiveCategory()
    {
        $current_cat = $this->handleCat();
        if($current_cat != null && $current_cat->getId())
            return $current_cat->getParentCategory()->getId();
        else
            return null;
    }

}