<?php

class Magestore_Other_Block_Adminhtml_System_Config_Form_Fieldset_Area extends
    Mage_Adminhtml_Block_System_Config_Form_Field_Array_Abstract
{
    public function _prepareToRender()
    {
        $this->addColumn('from_area', array(
            'label' => Mage::helper('other')->__('From'),
            'style' => 'width:100px',
        ));
        $this->addColumn('to_area', array(
            'label' => Mage::helper('other')->__('To'),
            'style' => 'width:100px',
        ));
        $this->addColumn('color_area', array(
            'label' => Mage::helper('other')->__('Color'),
            'style' => 'width:100px',
            'class' => 'color'
        ));
        $this->addColumn('label_area', array(
            'label' => Mage::helper('other')->__('Label Area'),
            'style' => 'width:100px',
        ));
        $this->addColumn('color_label', array(
            'label' => Mage::helper('other')->__('Color Label'),
            'style' => 'width:100px',
            'class' => 'color'
        ));
        $this->_addAfter = false;
        $this->_addButtonLabel = Mage::helper('other')->__('Add');
    }

}
