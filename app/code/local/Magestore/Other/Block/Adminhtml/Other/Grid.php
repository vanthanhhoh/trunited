<?php

class Magestore_Other_Block_Adminhtml_Other_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
	public function __construct(){
		parent::__construct();
		$this->setId('otherGrid');
		$this->setDefaultSort('other_id');
		$this->setDefaultDir('ASC');
		$this->setSaveParametersInSession(true);
	}

	protected function _prepareCollection(){
		$collection = new Varien_Data_Collection();
		$csvObject = new Varien_File_Csv();
		$file = Mage::getBaseDir() . '/mobileRegistrationLog.csv';
		$_data = $csvObject->getData($file);
		
		$flag = 0;
		foreach ($_data as $dt) {
			if($flag > 0){
				$data = new Varien_Object();
	            $data->setStatus($dt[0]);
	            $data->setNewCustomerId($dt[1]);
	            $data->setNewCustomerName($dt[2]);
	            $data->setNewCustomerEmail($dt[3]);
	            $data->setAffiliateReferredId($dt[4]);
	            $data->setAffiliateReferredEmail($dt[5]);
	            $data->setAffiliateCustomerId($dt[6]);
	            $data->setAffiliateCustomerEmail($dt[7]);
	            $data->setSentTo($dt[8]);
	            $data->setNoRefersMe($dt[9]);
	            $data->setEnteredValue($dt[10]);
	            $data->setCreatedAt($dt[11]);
	            $data->setMessage($dt[12]);
	            $collection->addItem($data);
			}
           
            $flag++;
        }

		$this->setCollection($collection);
		return parent::_prepareCollection();
	}

	protected function _prepareColumns(){
		$this->addColumn('status', array(
			'header'	=> Mage::helper('other')->__('Status'),
			'align'	 =>'right',
			'index'	 => 'status',
		));

		$this->addColumn('new_customer_id', array(
			'header'	=> Mage::helper('other')->__('Customer ID'),
			'align'	 =>'left',
			'width'	 => '20px',
			'index'	 => 'new_customer_id',
		));

		$this->addColumn('new_customer_name', array(
			'header'	=> Mage::helper('other')->__('Customer Name'),
			'index'	 => 'new_customer_name',
		));

		$this->addColumn('new_customer_email', array(
			'header'	=> Mage::helper('other')->__('Customer Email'),
			'index'	 => 'new_customer_email',
		));

		$this->addColumn('affiliate_referred_id', array(
			'header'	=> Mage::helper('other')->__('Affiliate ID'),
			'index'	 => 'affiliate_referred_id',
			'width'	 => '50px',
		));

		$this->addColumn('affiliate_referred_email', array(
			'header'	=> Mage::helper('other')->__('Affiliate Email'),
			'index'	 => 'affiliate_referred_email',
		));

		$this->addColumn('entered_value', array(
			'header'	=> Mage::helper('other')->__('Entered Value'),
			'index'	 => 'entered_value',
		));

		$this->addColumn('sent_to', array(
			'header'	=> Mage::helper('other')->__('Sent To'),
			'index'	 => 'sent_to',
		));

		$this->addColumn('no_refers_me', array(
			'header'	=> Mage::helper('other')->__('No Refers Me'),
			'align'	 => 'left',
			'width'	 => '80px',
			'index'	 => 'no_refers_me',
			'type'		=> 'options',
			'options'	 => array(
				1 => 'Yes',
				2 => 'No',
			),
		));

		$this->addColumn('created_at', array(
            'header' => Mage::helper('manageapi')->__('Created At'),
            'align' => 'left',
            'index' => 'created_at',
            'type' => 'date',
            'width'	 => '40px',
        ));

		$this->addColumn('affiliate_customer_id', array(
			'header'	=> Mage::helper('other')->__('Affiliate Customer ID'),
			'index'	 => 'affiliate_customer_id',
			'width'	 => '50px',
		));

		$this->addColumn('affiliate_referred_email', array(
			'header'	=> Mage::helper('other')->__('Affiliate Customer Email'),
			'index'	 => 'affiliate_referred_email',
		));

		$this->addColumn('message', array(
			'header'	=> Mage::helper('other')->__('SMS Content'),
			'index'	 => 'message',
		));

		return parent::_prepareColumns();
	}

	public function getRowUrl($row){}
}