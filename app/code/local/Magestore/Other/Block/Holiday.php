<?php

class Magestore_Other_Block_Holiday extends Mage_Core_Block_Template
{
    public function _prepareLayout()
    {
        return parent::_prepareLayout();
    }

    public function getCustomerIdInUrl()
    {
    	$is_help = Mage::helper('fundraiser')->isShowHelpAffiliateFundraiser();
        if(Mage::getSingleton('customer/session')->isLoggedIn())
        {
            if($is_help)
                return Mage::getSingleton('customer/session')->getCustomer()->getId().'_fun';
            else{
                return Mage::getSingleton('customer/session')->getCustomer()->getId();
            }
        } else {
            $affiliate_account = Mage::helper('affiliateplus/account')->getAffiliateInfoFromCookie();

            if($affiliate_account != null && $affiliate_account->getId()){
                return $affiliate_account->getCustomerId().'_aff';
            } else {
                return 'guest';
            }
        }
    }
}
