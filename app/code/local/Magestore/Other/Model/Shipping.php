<?php
/**
 *
 * @category    Magestore
 * @package     Magestore_Other
 * @author      Magestore Developer
 */
class Magestore_Other_Model_Shipping
{

    public function toOptionArray()
    {
        $collection = Mage::getSingleton('shipping/config')->getActiveCarriers();

        $rs = array();
        if(sizeof($collection) > 0){
            foreach ($collection as $shippingCode => $shippingModel)
            {
                $shippingTitle = Mage::getStoreConfig('carriers/'.$shippingCode.'/title');

                $rs[] = array(
                    'value' => $shippingCode,
                    'label' => $shippingTitle
                );
            }


        }

        return $rs;


    }
}
