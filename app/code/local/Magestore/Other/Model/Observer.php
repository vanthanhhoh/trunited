<?php

class Magestore_Other_Model_Observer
{
    public function customerRegisterSuccess($observer)
    {
        $customer_reg = $observer->getCustomer();
        $customer_reg->setIsActive(Magestore_Other_Model_Status::STATUS_CUSTOMER_ACTIVE);
        $customer_reg->setIsSuspended(Magestore_Other_Model_Status::STATUS_CUSTOMER_SUSPENDED_NO);
        $customer_reg->save();

        Mage::getSingleton('core/session')->unsIsCreatedAjax();
    }

    public function predispatchCheckoutCartAdd(Varien_Event_Observer $observer)
    {
        $session = Mage::getSingleton('checkout/session');

        if ($observer->getEvent()->getControllerAction()->getFullActionName() == 'checkout_cart_add') {
            $productId = Mage::app()->getRequest()->getParam('product');
            $product = Mage::getModel('catalog/product')->load($productId);

            if (Mage::helper('other')->enableDropShip() && Mage::helper('other')->isInDropShipList($product)) {
                $session->setBaseTruwalletCreditAmount(0);
                $session->setUseTruwalletCredit(false);
                $session->setData('delivery_type', null);
            }
        }
    }

    public function preDispatchLoginPost(Varien_Event_Observer $observer)
    {
        $session = Mage::getSingleton('customer/session');
        if ($observer->getEvent()->getControllerAction()->getFullActionName() == 'customer_account_loginPost') {
            $login = Mage::app()->getRequest()->getParam('login');

            if (!empty($login['username']) && !empty($login['password'])) {

                // Is Active Status
                $collection = Mage::getModel('customer/customer')->getCollection()
                    ->addAttributeToSelect('entity_id')
                    ->addAttributeToSelect('email')
                    ->addAttributeToSelect('is_active')
                    ->addAttributeToFilter('email', array('like' => '%'.$login['username'].'%'))
                    ;
                if(sizeof($collection) > 0)
                {
                    $is_error_message = false;
                    foreach ($collection as $customer) {
                        if(strcasecmp($customer->getEmail(), $login['username']) == 0 && $customer->getData('is_active') == Magestore_Other_Model_Status::STATUS_CUSTOMER_INACTIVE)
                        {
                            $is_error_message = true;
                            break;
                        }
                    }
                    if($is_error_message){
                        Mage::getSingleton('core/session')->addError(
                            Mage::helper('other')->getErrorMessageCustomer()
                        );
                        $session->setId(null)
                            ->getCookie()->delete('customer');
                        $session->logout();
                        Mage::app()->getFrontController()->getResponse()->setRedirect(Mage::getUrl('customer/account/login'));
                        Mage::app()->getResponse()->sendResponse();
                        exit;
                    }
                }

                // Suspended Status
                $collection_suspended = Mage::getModel('customer/customer')->getCollection()
                    ->addAttributeToSelect('entity_id')
                    ->addAttributeToSelect('email')
                    ->addAttributeToSelect('is_suspended')
                    ->addAttributeToFilter('email', array('like' => '%'.$login['username'].'%'))
                    ;
                if(sizeof($collection_suspended) > 0)
                {
                    $is_error_message_suspended = false;
                    foreach ($collection_suspended as $customer) {
                        if(strcasecmp($customer->getEmail(), $login['username']) == 0 && $customer->getData('is_suspended') == Magestore_Other_Model_Status::STATUS_CUSTOMER_SUSPENDED_YES)
                        {
                            $is_error_message_suspended = true;
                            break;
                        }
                    }
                    if($is_error_message_suspended){

                        $session->setId(null)
                            ->getCookie()->delete('customer');
                        $session->logout();
                        
                        Mage::getSingleton('core/session')->setIsPopupSuspended(1);

                        Mage::app()->getFrontController()->getResponse()->setRedirect(Mage::getUrl('customer/account/login'));
                        Mage::app()->getResponse()->sendResponse();
                        exit;
                    }
                }
            } else {
                $session->addError(Mage::helper('other')->__('Login and password are required.'));
            }
        }
    }

    public function customerLogin(Varien_Event_Observer $observer)
    {
        $customer = $observer->getEvent()->getCustomer();
        $session = Mage::getSingleton('customer/session');
        if($customer->getData('is_active') == Magestore_Other_Model_Status::STATUS_CUSTOMER_INACTIVE){
            Mage::getSingleton('core/session')->addError(
                Mage::helper('other')->getErrorMessageCustomer()
            );
            $session->setId(null)
                ->getCookie()->delete('customer');
            $session->logout();
            Mage::app()->getFrontController()->getResponse()->setRedirect(Mage::getUrl('customer/account/login'));
            Mage::app()->getResponse()->sendResponse();
            return;
        }

        if($customer->getData('is_suspended') == Magestore_Other_Model_Status::STATUS_CUSTOMER_SUSPENDED_YES){
            
            $session->setId(null)
                ->getCookie()->delete('customer');
            $session->logout();

            Mage::getSingleton('core/session')->setIsPopupSuspended(1);
            
            Mage::app()->getFrontController()->getResponse()->setRedirect(Mage::getUrl('customer/account/login'));
            Mage::app()->getResponse()->sendResponse();
            return;
        }
    }

}
