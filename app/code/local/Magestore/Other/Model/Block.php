<?php
/**
 *
 * @category    Magestore
 * @package     Magestore_Other
 * @author      Magestore Developer
 */
class Magestore_Other_Model_Block
{

    public function toOptionArray()
    {
        $collection = Mage::getModel('cms/block')->getCollection()
            ->addFieldToSelect('identifier')
            ->addFieldToSelect('title')
            ->setOrder('block_id','desc')
        ;

        $rs = array();
        if(sizeof($collection) > 0){
            foreach($collection as $cms)
            {
                $rs[] = array(
                    'value' => $cms->getIdentifier(),
                    'label' => $cms->getTitle()
                );
            }


        }

        return $rs;

    }
}
