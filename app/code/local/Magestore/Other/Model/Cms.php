<?php
/**
 *
 * @category    Magestore
 * @package     Magestore_Other
 * @author      Magestore Developer
 */
class Magestore_Other_Model_Cms
{

    public function toOptionArray()
    {
        $collection = Mage::getModel('cms/page')->getCollection()
            ->addFieldToSelect('identifier')
            ->addFieldToSelect('title')
            ->setOrder('page_id','desc')
        ;

        $rs = array();
        if(sizeof($collection) > 0){
            foreach($collection as $cms)
            {
                $rs[] = array(
                    'value' => $cms->getIdentifier(),
                    'label' => $cms->getTitle()
                );
            }


        }

        return $rs;


    }
}
