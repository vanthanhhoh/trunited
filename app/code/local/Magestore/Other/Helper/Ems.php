<?php

class Magestore_Other_Helper_Ems extends Mage_Core_Helper_Abstract
{
    const TBL_PAYOUT_PERIOD_STATS = 'tr_payoutperiod_stats';
    const TBL_PAYOUT_PERIOD = 'tr_PayoutPeriod';

    public function isEnable()
    {
        return Mage::helper('other')->getConfigData('ems_trend', 'enable');
    }

    public function getCoreResource()
    {
        return Mage::getSingleton('core/resource');
    }

    public function getReadConnection()
    {
        return $this->getCoreResource()->getConnection('core_read');
    }

    public function getWriteConnection()
    {
        return $this->getCoreResource()->getConnection('core_write');
    }

    public function getAllPayoutPeriod()
    {
        $start_time = date('Y-01-01 00:00:00', time());
        $end_time = date('Y-12-31 23:59:59', time());
        $readConnection = $this->getReadConnection();

        $query = "SELECT payout_period_id";
        $query .= " FROM ".self::TBL_PAYOUT_PERIOD."";
        $query .= " WHERE start_date >= '$start_time' and end_date <= '$end_time'";
        $query .= " ORDER BY payout_period_id ASC";

        $result = $readConnection->fetchCol($query);

        return $result;
    }

    public function collectEmsData($customer_id = null)
    {
        if($customer_id == null)
            $customer_id = Mage::getSingleton('customer/session')->getCustomer()->getId();

        $payout_period_ids = $this->getAllPayoutPeriod();

        $result = array();

        if($payout_period_ids != null && sizeof($payout_period_ids))
        {
            $readConnection = $this->getReadConnection();

            $ids = implode(", ", $payout_period_ids);

            $query = "SELECT payout_period_id, ems_score";
            $query .= " FROM ".self::TBL_PAYOUT_PERIOD_STATS."";
            $query .= " WHERE customer_id = $customer_id and payout_period_id in ($ids)";
            $query .= " ORDER BY payout_period_id ASC";

            $result = $readConnection->fetchAll($query);
        }

        $data = array();
        if(sizeof($result) > 0)
        {

            foreach ($payout_period_ids as $payout_period_id) {
                $score = 0;
                foreach ($result as $rs) {
                    if($rs['payout_period_id'] == $payout_period_id)
                    {
                        $score = ceil($rs['ems_score']);
                        break;
                    }
                }
                $data[$payout_period_id] = $score != null ? $score : 0;
            }
        }

        return $data;
    }
}
