<?php

class Magestore_Other_IndexController extends Mage_Core_Controller_Front_Action
{
	public function indexAction(){
	
		zend_debug::dump($_SERVER['REMOTE_ADDR']);
		zend_debug::dump( 'Current PHP version: ' . phpversion());
		
		echo phpinfo();
		exit;
		$this->loadLayout();
		$this->renderLayout();
	}

	public function customerActiveAction()
	{
		$setup = new Mage_Eav_Model_Entity_Setup();
		$installer = $setup;
		$installer->startSetup();
		$installer->run("");

		/*$setup->removeAttribute('customer','status_active');*/
		$setup->removeAttribute('customer','is_active');

		$entityTypeId = $setup->getEntityTypeId('customer');
		$attributeSetId = $setup->getDefaultAttributeSetId($entityTypeId);
		$attributeGroupId = $setup->getDefaultAttributeGroupId($entityTypeId, $attributeSetId);


		$installer->addAttribute("customer", "is_active",  array(
			"type"     => "int",
			"backend"  => "",
			"label"    => "Is Active",
			"input"    => "select",
			"source"   => "eav/entity_attribute_source_boolean",
			"visible"  => true,
			"required" => true,
			"default"   => 1,
			"frontend" => "",
			"unique"     => true,
			"note"       => "Allow admin to active or inactive the customer. When the customer is changed to Inactive status, their email will be added 'inactive-' prefix automatically.",
		));

		$attribute   = Mage::getSingleton("eav/config")->getAttribute("customer", "is_active");
		$setup->addAttributeToGroup(
			$entityTypeId,
			$attributeSetId,
			$attributeGroupId,
			'is_active',
			'102'
		);

		$used_in_forms=array();
		$used_in_forms[]="adminhtml_customer";
		$used_in_forms[]="checkout_register";
		$used_in_forms[]="customer_account_create";
		$used_in_forms[]="customer_account_edit";
		$used_in_forms[]="adminhtml_checkout";

		$attribute->setData("used_in_forms", $used_in_forms)
			->setData("is_used_for_customer_segment", true)
			->setData("is_system", 0)
			->setData("is_user_defined", 1)
			->setData("is_visible", 1)
			->setData("sort_order", 102)
		;
		$attribute->save();

		$customers = Mage::getModel('customer/customer')->getCollection();
		Mage::getSingleton('core/resource_iterator')->walk($customers->getSelect(), array('callback'));

		$installer->endSetup();
		echo "success";
	}

	function callback($args)
	{
		$customer = Mage::getModel('customer/customer');
		$customer->setData($args['row']);
		$customer->setStatusActive(1);
		$customer->getResource()->saveAttribute($customer, 'status_active');
	}

	public function updateDbAction()
	{
		/**
			UPDATE `trcatalog_product_entity_decimal` as d
			SET `value`= (SELECT value from trcatalog_product_entity_int as i
			where i.entity_type_id = 4 and i.attribute_id = 166 and i.entity_id = d.entity_id);

			DELETE FROM `trcatalog_product_entity_int` WHERE `entity_type_id` = 4 AND `attribute_id` = 166;
		 **/
		$setup = new Mage_Eav_Model_Entity_Setup('core_setup');
		$installer = $setup;
		$installer->startSetup();
		$installer->run("


			ALTER TABLE {$setup->getTable('sales/order')} MODIFY COLUMN rewardpoints_earn DECIMAL(12,2) NOT NULL default 0;
			ALTER TABLE {$setup->getTable('sales/order_item')} MODIFY COLUMN rewardpoints_earn DECIMAL(12,2) NOT NULL default 0;

			ALTER TABLE {$setup->getTable('rewardpoints/transaction')} MODIFY COLUMN point_amount DECIMAL(12,2) NOT NULL default 0;
			ALTER TABLE {$setup->getTable('rewardpoints/transaction')} MODIFY COLUMN point_used DECIMAL(12,2) NOT NULL default 0;
			ALTER TABLE {$setup->getTable('rewardpoints/transaction')} MODIFY COLUMN real_point DECIMAL(12,2) NOT NULL default 0;

			ALTER TABLE {$setup->getTable('rewardpoints/customer')} MODIFY COLUMN point_balance DECIMAL(12,2) NOT NULL default 0;
		");

		$entityTypeId = $installer->getEntityTypeId('catalog_product');

		$idAttribute = $installer->getAttribute($entityTypeId, 'rewardpoints_earn', 'attribute_id');
		$installer->updateAttribute($entityTypeId, $idAttribute, array(
			'backend_type' => 'decimal'
		));

		$attribute = Mage::getModel('eav/entity_attribute')->loadByCode('catalog_product', 'rewardpoints_earn');
		Zend_Debug::dump($attribute->getData());

		$installer->endSetup();
		echo "success";
	}

	public function customerSuspendedAction()
	{
		$setup = new Mage_Eav_Model_Entity_Setup(null);
		$installer = $setup;
		$installer->startSetup();
		$installer->run("");

		/*$setup->removeAttribute('customer','status_active');*/
		$setup->removeAttribute('customer','is_suspended');

		$entityTypeId = $setup->getEntityTypeId('customer');
		$attributeSetId = $setup->getDefaultAttributeSetId($entityTypeId);
		$attributeGroupId = $setup->getDefaultAttributeGroupId($entityTypeId, $attributeSetId);


		$installer->addAttribute("customer", "is_suspended",  array(
			"type"     => "int",
			"backend"  => "",
			"label"    => "Is Suspended",
			"input"    => "select",
			"source"   => "eav/entity_attribute_source_boolean",
			"visible"  => true,
			"required" => true,
			"default"   => 0,
			"frontend" => "",
			"unique"     => true,
			"note"       => "Allow admin to put a customer account into a “suspended” status. When the customer has Suspended status, the popup will be shown when the customer logs in and don\'t allow the customer to continue.",
		));

		$attribute   = Mage::getSingleton("eav/config")->getAttribute("customer", "is_suspended");
		$setup->addAttributeToGroup(
			$entityTypeId,
			$attributeSetId,
			$attributeGroupId,
			'is_suspended',
			'102'
		);

		$used_in_forms=array();
		$used_in_forms[]="adminhtml_customer";
		$used_in_forms[]="checkout_register";
		$used_in_forms[]="customer_account_create";
		$used_in_forms[]="customer_account_edit";
		$used_in_forms[]="adminhtml_checkout";

		$attribute->setData("used_in_forms", $used_in_forms)
			->setData("is_used_for_customer_segment", true)
			->setData("is_system", 0)
			->setData("is_user_defined", 1)
			->setData("is_visible", 1)
			->setData("sort_order", 102)
		;
		$attribute->save();

		$customers = Mage::getModel('customer/customer')->getCollection();
		Mage::getSingleton('core/resource_iterator')->walk($customers->getSelect(), array('callback'));

		$installer->endSetup();
		echo "success";
	}

	public function getContentSuspendedPopupAction()
	{
		$content_popup = Mage::helper('other')->getConfigData('customer_suspended', 'content_popup');
		echo str_replace(array("\r\n","\r"),"",$content_popup);
	}
}