<?php
/**
 * Magestore
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magestore.com license that is
 * available through the world-wide-web at this URL:
 * http://www.magestore.com/license-agreement.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Magestore
 * @package     Magestore_RewardPoints
 * @copyright   Copyright (c) 2012 Magestore (http://www.magestore.com/)
 * @license     http://www.magestore.com/license-agreement.html
 */

/**
 * Rewardpoints Account Dashboard Recent Transactions
 *
 * @category    Magestore
 * @package     Magestore_RewardPoints
 * @author      Magestore Developer
 */
class Magestore_RewardPoints_Block_Account_Dashboard_Transactions extends Magestore_RewardPoints_Block_Template
{
    protected function _construct()
    {
        parent::_construct();
        $status = $this->getStatusParam();
        $customerId = Mage::getSingleton('customer/session')->getCustomerId();
        $collection = Mage::getResourceModel('rewardpoints/transaction_collection')
            ->addFieldToFilter('customer_id', $customerId)
        ;

        if($status != '')
        {
            $collection->addFieldToFilter('status', $status);
        }

        $collection->setOrder('created_time', 'DESC');
        /*$collection->getSelect()->limit(5)
            ->order('created_time DESC');*/
        $collection->setOrder('transaction_id', 'DESC');
        $this->setCollection($collection);
    }

    public function getStatusParam()
    {
        return $this->getRequest()->getParam('status');
    }

    public function getHelperTransaction()
    {
        return Mage::helper('rewardpoints/transaction');
    }

    public function isShowOnHold()
    {
        return $this->getHelperTransaction()->isShowOnHold();
    }

    public function getDaysOfHold()
    {
        return $this->getHelperTransaction()->getDaysOfHold();
    }

    public function getDataOnHold()
    {
        return $this->getHelperTransaction()->getDataOnHold();
    }

    public function getStatusTransaction()
    {
        $model = Mage::getModel('rewardpoints/transaction');
        return $model->getStatusHash();
    }

    public function _prepareLayout() {
        parent::_prepareLayout();
        $pager = $this->getLayout()->createBlock('page/html_pager', 'transactions_pager')
                ->setCollection($this->getCollection());
        $this->setChild('transactions_pager', $pager);
        return $this;
    }

    public function getPagerHtml() {
        return $this->getChildHtml('transactions_pager');
    }

    public function totalOnHold()
    {
        $total = 0;

        if($this->isShowOnHold())
        {
            $data = $this->getDataOnHold();
            if(sizeof($data) > 0)
            {
                foreach ($data as $k => $v) {
                    $total += $v;
                }

            }
        }

        return $total;
    }

    public function getCustomerStats()
    {
        return Mage::helper('badges')->getCustomerStats(
            Mage::getSingleton('customer/session')->getCustomer()->getId()
        );
    }
}
