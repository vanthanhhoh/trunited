<?php

class Magestore_Customerdashboard_Block_Customerdashboard extends Mage_Core_Block_Template
{
	protected $_dataChart = null;

	public function _prepareLayout(){
		if($this->_dataChart == null)
			$this->_dataChart = $this->getTotalPointsForChart();
		return parent::_prepareLayout();
	}

	public function getCustomerStats()
	{
		return Mage::helper('badges')->getCustomerStats(
			Mage::getSingleton('customer/session')->getCustomer()->getId()
		);
	}

	public function getYtdSaving()
	{
		return Mage::helper('businessdashboard')->getYtdSaving();
	}

	public function getTruWalletCredit()
	{
		return Mage::helper('truwallet/account')->getTruWalletCredit();
	}

	public function getTruGiftCardCredit()
	{
		return Mage::helper('trugiftcard/account')->getTruGiftCardCredit();
	}

	public function getPurchasedGiftCard()
	{
		return Mage::helper('customerdashboard')->getPurchasedGiftCard();
	}

	public function getTotalPointsForChart()
	{
		return Mage::helper('customerdashboard')->getTotalPointsForChart();
	}

	public function formatX()
	{
		$data = $this->_dataChart;
		return implode(',', $data['x']);
	}

	public function formatY()
	{
		$data = $this->_dataChart;
		return implode(',', $data['y']);
	}

	public function getCurrentPointValue()
    {
        $current_point_value = Mage::helper('other')->getConfigData('general', 'current_point_value');
        return Mage::helper('core')->currency($current_point_value,true,false);
    }


}
