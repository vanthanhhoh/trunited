<?php

class Magestore_Customerdashboard_IndexController extends Mage_Core_Controller_Front_Action
{
	/**
	 * check customer is logged in
	 */
	public function preDispatch()
	{
		parent::preDispatch();
		if (!$this->getRequest()->isDispatched()) {
			return;
		}
		$action = $this->getRequest()->getActionName();
		if ($action != 'policy' && $action != 'redirectLogin') {
			// Check customer authentication
			if (!Mage::getSingleton('customer/session')->isLoggedIn()) {
				Mage::getSingleton('customer/session')->setAfterAuthUrl(
					Mage::getUrl($this->getFullActionName('/'))
				);
				$this->_redirect('customer/account/login');
				$this->setFlag('', Mage_Core_Controller_Varien_Action::FLAG_NO_DISPATCH, true);
			}
		}
	}

	public function indexAction(){
		$this->loadLayout();

		$this->_title(Mage::helper('businessdashboard')->__('Customer Dashboard'));

		$breadcrumbs = $this->getLayout()->getBlock("breadcrumbs");
		$breadcrumbs->addCrumb("home", array(
			"label" => $this->__("Home"),
			"title" => $this->__("Home"),
			"link"  => Mage::getBaseUrl()
		));

		$breadcrumbs->addCrumb("my_account", array(
			"label" => $this->__("My Account"),
			"title" => $this->__("My Account"),
			"link"  => Mage::getUrl('customer/account/')
		));

		$breadcrumbs->addCrumb("customer_dashboard", array(
			"label" => $this->__("Customer Dashboard"),
			"title" => $this->__("Customer Dashboard"),
		));

		$this->renderLayout();
	}
}