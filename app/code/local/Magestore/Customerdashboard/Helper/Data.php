<?php

class Magestore_Customerdashboard_Helper_Data extends Mage_Core_Helper_Abstract
{
    public function getCoreResource()
    {
        return Mage::getSingleton('core/resource');
    }

    public function getReadConnection()
    {
        return $this->getCoreResource()->getConnection('core_read');
    }

    public function getPurchasedGiftCard()
    {
        if(Mage::getSingleton('customer/session')->isLoggedIn()){
            $current_customer_id = Mage::getSingleton('customer/session')->getCustomer()->getId();

            $orders = Mage::getModel('sales/order')->getCollection()
                ->addAttributeToFilter('customer_id', $current_customer_id)
                ->addAttributeToFilter('status', array('in' => array(
                    Mage_Sales_Model_Order::STATE_COMPLETE,
                    Mage_Sales_Model_Order::STATE_PROCESSING,
                )))
                ->setOrder('entity_id', 'desc')
                ;

            $count = 0;
            if(sizeof($orders) > 0)
            {
                foreach ($orders as $order) {
                    $items = $order->getAllItems();
                    if(sizeof($items) > 0)
                    {
                        foreach ($items as $item) {
                            if(strcasecmp(Mage::helper('trugiftcard')->getTruGiftCardSku(), $item->getSku()) == 0)
                                $count++;
                        }

                    }
                }
            }

            return $count;
        }else{
            return 0;
        }

    }

    public function getTotalPointsForChart()
    {
        $current_month = date('m', time()) == 1 ? 12 : date('m', time());
        $data = array();

        for ($i = 1; $i <= $current_month; $i++)
        {
            $start_time = (date('Y', time()) - 1).'-'.$i.'-01 00:00:00';
            $end_time = (date('Y', time()) - 1).'-'.$i.'-'.date('t', strtotime($start_time)).' 23:59:59';
            $data['x'][] = '\''.date('M', strtotime($start_time)).'\'';
            if($i == 1) {
                $data['y'][] = floatval($this->getTotalPointForMonth($start_time, $end_time));
            } else {
                $data['y'][] = floatval($this->getTotalPointForMonth($start_time, $end_time)) + $data['y'][$i - 2];
            }
        }

        return $data;
    }




    public function getTotalPointForMonth($start_time, $end_time)
    {
        $readConnection = $this->getReadConnection();

        $rewardPoint_transaction_table = $this->getCoreResource()->getTableName('rewardpoints/transaction');

        $query = "SELECT sum(point_amount)";
        $query .= " FROM $rewardPoint_transaction_table";
        $query .= " WHERE status = 3 AND action_type != 10 AND action != 'reset_point' AND" ;
        $query .= "       created_time >= '$start_time' AND created_time <= '$end_time'";
        $query .= " ORDER BY point_amount DESC";

        $result = $readConnection->fetchOne($query);

        return $result;

    }
	
}
