<?php

class Magestore_Customerdashboard_Model_Mysql4_Customerdashboard_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
	public function _construct(){
		parent::_construct();
		$this->_init('customerdashboard/customerdashboard');
	}
}