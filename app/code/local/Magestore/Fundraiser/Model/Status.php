<?php

class Magestore_Fundraiser_Model_Status extends Varien_Object
{
    const STATUS_ENABLED	= 1;
    const STATUS_DISABLED	= 2;

    const STATUS_TRANSACTION_PENDING = 1;
    const STATUS_TRANSACTION_COMPLETED = 2;
    const STATUS_TRANSACTION_CANCELLED = 3;
    const STATUS_TRANSACTION_EXPIRED = 4;
    const STATUS_TRANSACTION_ON_HOLD = 5;

    const CREATED_BY_ADMIN = 1;
    const CREATED_BY_CUSTOMER = 2;

    static public function getOptionArray(){
        return array(
            self::STATUS_ENABLED	=> Mage::helper('fundraiser')->__('Enabled'),
            self::STATUS_DISABLED   => Mage::helper('fundraiser')->__('Disabled')
        );
    }

    static public function getOptionHash(){
        $options = array();
        foreach (self::getOptionArray() as $value => $label)
            $options[] = array(
                'value'	=> $value,
                'label'	=> $label
            );
        return $options;
    }

    static public function getTransactionOptionArray(){
        return array(
            self::STATUS_TRANSACTION_PENDING	=> Mage::helper('fundraiser')->__('Pending'),
            self::STATUS_TRANSACTION_COMPLETED   => Mage::helper('fundraiser')->__('Completed'),
            self::STATUS_TRANSACTION_CANCELLED   => Mage::helper('fundraiser')->__('Canceled'),
            self::STATUS_TRANSACTION_EXPIRED   => Mage::helper('fundraiser')->__('Expired'),
            self::STATUS_TRANSACTION_ON_HOLD   => Mage::helper('fundraiser')->__('On Hold'),
        );
    }

    static public function getTransactionOptionHash(){
        $options = array();
        foreach (self::getTransactionOptionArray() as $value => $label)
            $options[] = array(
                'value'	=> $value,
                'label'	=> $label
            );
        return $options;
    }

    static public function getCreatedByArray(){
        return array(
            self::CREATED_BY_ADMIN	=> Mage::helper('fundraiser')->__('Admin'),
            self::CREATED_BY_CUSTOMER   => Mage::helper('fundraiser')->__('Customer')
        );
    }

    static public function getCreatedByHash(){
        $options = array();
        foreach (self::getCreatedByArray() as $value => $label)
            $options[] = array(
                'value'	=> $value,
                'label'	=> $label
            );
        return $options;
    }
}
