<?php

class Magestore_Fundraiser_Model_Mysql4_Customer extends Mage_Core_Model_Mysql4_Abstract
{
	public function _construct(){
		$this->_init('fundraiser/customer', 'fundraiser_id');
	}
}