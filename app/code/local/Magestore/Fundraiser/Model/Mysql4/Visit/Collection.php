<?php

class Magestore_Fundraiser_Model_Mysql4_Visit_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
	public function _construct(){
		parent::_construct();
		$this->_init('fundraiser/visit');
	}
}