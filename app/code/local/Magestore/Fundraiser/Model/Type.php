<?php

class Magestore_Fundraiser_Model_Type extends Varien_Object
{
	const TYPE_TRANSACTION_BY_ADMIN	= 1;
	const TYPE_TRANSACTION_SHARING	= 2;
	const TYPE_TRANSACTION_RECEIVE_FROM_SHARING	= 3;
	const TYPE_TRANSACTION_TRANSFER	= 4;
	const TYPE_TRANSACTION_PAYOUT_EARNING	= 5;
	const TYPE_TRANSACTION_CANCEL_ORDER	= 6;
	const TYPE_TRANSACTION_CHECKOUT_ORDER = 7;
	const TYPE_TRANSACTION_REFUND_ORDER = 8;
	const TYPE_TRANSACTION_PURCHASE_GIFT_CARD = 9;
	const TYPE_TRANSACTION_RECEIVE_FROM_CUSTOMER_HELP = 10;

	static public function getOptionArray(){
		return array(
			self::TYPE_TRANSACTION_BY_ADMIN	=> Mage::helper('fundraiser')->__('Changed by Admin'),
//			self::TYPE_TRANSACTION_SHARING   => Mage::helper('fundraiser')->__('Sharing Fundraiser Points'),
			self::TYPE_TRANSACTION_RECEIVE_FROM_SHARING   => Mage::helper('fundraiser')->__('Received Fundraiser Points from Sharing'),
//			self::TYPE_TRANSACTION_TRANSFER   => Mage::helper('fundraiser')->__('Transfer dollars from balance to Fundraiser Points'),
//			self::TYPE_TRANSACTION_PAYOUT_EARNING   => Mage::helper('fundraiser')->__('Payout earnings'),
			self::TYPE_TRANSACTION_CANCEL_ORDER   => Mage::helper('fundraiser')->__('Retrieve Fundraiser Points earned on cancelled order'),
//			self::TYPE_TRANSACTION_CHECKOUT_ORDER   => Mage::helper('fundraiser')->__('Spend Fundraiser Points to purchase order'),
			self::TYPE_TRANSACTION_REFUND_ORDER   => Mage::helper('fundraiser')->__('Retrieve Fundraiser Points earned on refunded order'),
//			self::TYPE_TRANSACTION_PURCHASE_GIFT_CARD   => Mage::helper('fundraiser')->__('Purchased Fundraiser Points on order'),
			self::TYPE_TRANSACTION_RECEIVE_FROM_CUSTOMER_HELP   => Mage::helper('fundraiser')->__('Received Fundraiser Points from helping of customer'),
		);
	}
	
	static public function getOptionHash(){
		$options = array();
		foreach (self::getOptionArray() as $value => $label)
			$options[] = array(
				'value'	=> $value,
				'label'	=> $label
			);
		return $options;
	}

	static public function getDataType()
	{
		return array(
			self::TYPE_TRANSACTION_BY_ADMIN,
			self::TYPE_TRANSACTION_SHARING,
			self::TYPE_TRANSACTION_RECEIVE_FROM_SHARING,
			self::TYPE_TRANSACTION_TRANSFER,
			self::TYPE_TRANSACTION_PAYOUT_EARNING,
		);
	}

}