<?php

class Magestore_Fundraiser_Model_Observer
{
    public function affiliateCreated($observer)
    {
        $affiliate = $observer->getAffiliate();

        if($affiliate != null && $affiliate->getId()){
            Mage::helper('fundraiser')->createNewObj($affiliate->getCustomerId(), $affiliate->getId());
        }

        return $this;
    }

    public function controllerActionPredispatch($event)
    {
        if(!Mage::helper('core')->isModuleEnabled('Magestore_Fundraiser'))
            return $this;

        $controller = $event['controller_action'];

        if(strcasecmp($controller->getRequest()->getControllerModule(),'Mage_Cms') == 0 &&
            strcasecmp($controller->getRequest()->getControllerName(),'page') == 0 &&
            strcasecmp($controller->getRequest()->getRouteName(),'cms') == 0 &&
            strcasecmp($controller->getRequest()->getActionName(),'view') == 0
        ) {
            $cms_page_id = $controller->getRequest()->getParam('page_id');
            $cms_page = Mage::getModel('cms/page')->load($cms_page_id);
            if($cms_page != null && $cms_page->getId() && Mage::helper('fundraiser')->getConfigData('general', 'enable')){
                $cms_affiliate = Mage::getModel('fundraiser/cms')->load($cms_page->getId(), 'cms_page_id');
                if($cms_affiliate != null && $cms_affiliate->getId())
                {
                    $expiredTime = Mage::helper('fundraiser')->getConfigData('general', 'cookie_expired_time');
                    Mage::helper('fundraiser')->saveCookie($cms_affiliate, $expiredTime);
                }
            }
        }
    }

    public function orderPlaceAfter($observer)
    {
        $order = $observer['order'];
        $customer_id = Mage::getSingleton('customer/session')->getCustomerId();
        if (!$customer_id) {
            $customer_id = $order->getCustomer()->getId();
        }
        $customer = Mage::getModel('customer/customer')->load($customer_id);

        $session = Mage::getSingleton('checkout/session');
        $amount = $order->getFundraiserEarn();
        if ($amount > 0 && $customer != null && $customer->getId()) {
            $customer_helped_id = Mage::helper('fundraiser')->findFundraiserByCustomerId($customer_id);
            if($customer_helped_id != null){
                $fundraiserAccount = Mage::helper('fundraiser/account')->updatePoint($customer_helped_id, $amount);
                $data = array(
                    'title' => Mage::helper('fundraiser')->__('Received the points from helping of <b>'.$customer->getName().'</b> (<a href="mailto:'.$customer->getEmail().'">'.$customer->getEmail().'</a> with #<a href="' . Mage::getUrl('sales/order/view', array('order_id' => $order->getId())) . '">' . $order->getIncrementId() . '</a>'),
                    'order_id' => $order->getEntityId(),
                    'changed_point' => $amount,
                );
                Mage::helper('fundraiser/transaction')->createTransaction(
                    $fundraiserAccount,
                    $data,
                    Magestore_Fundraiser_Model_Type::TYPE_TRANSACTION_RECEIVE_FROM_CUSTOMER_HELP,
                    Magestore_Fundraiser_Model_Status::STATUS_TRANSACTION_COMPLETED
                );
            }

        }

        $session->unsIsHelpFundraiser();
    }

    public function orderCancelAfter($observer)
    {
        $order = $observer->getOrder();
        $order_id = $order->getEntityId();
        $customer_id = $order->getCustomerId();

        $transactions = Mage::getModel('fundraiser/transaction')->getCollection()
            ->addFieldToFilter('order_id', $order_id)
            ->addFieldToFilter('status', Magestore_Fundraiser_Model_Status::STATUS_TRANSACTION_COMPLETED)
            ->addFieldToFilter('action_type', array('in' => array(
                Magestore_Fundraiser_Model_Type::TYPE_TRANSACTION_RECEIVE_FROM_CUSTOMER_HELP,
            )))
            ->setOrder('transaction_id', 'desc')
        ;

        if(sizeof($transactions) > 0)
        {
            $connection = Mage::getSingleton('core/resource')->getConnection('core_write');
            try{
                $connection->beginTransaction();

                foreach ($transactions as $transaction) {
                    $amount_point = $transaction->getData('changed_point');
                    $transaction->setData('status', Magestore_Fundraiser_Model_Status::STATUS_TRANSACTION_CANCELLED);
                    $transaction->setData('updated_at', now());
                    $transaction->save();

                    $customer_helped_id = Mage::helper('fundraiser')->getAffiliateFundraiserNameByOrder($order_id, false);
                    if($customer_helped_id != null) {
                        $fundraiserAccount = Mage::helper('fundraiser/account')->updatePoint($customer_helped_id, $amount_point);
                        $data = array(
                            'title' => Mage::helper('fundraiser')->__('Cancel order #<a href="' . Mage::getUrl('sales/order/view', array('order_id' => $order->getId())) . '">' . $order->getIncrementId() . '</a>'),
                            'order_id' => $order->getEntityId(),
                            'changed_point' => -$amount_point,
                        );
                        Mage::helper('fundraiser/transaction')->createTransaction(
                            $fundraiserAccount,
                            $data,
                            Magestore_Fundraiser_Model_Type::TYPE_TRANSACTION_CANCEL_ORDER,
                            Magestore_Fundraiser_Model_Status::STATUS_TRANSACTION_COMPLETED
                        );
                    }
                }

                $connection->commit();
            } catch (Exception $e) {
                $connection->rollback();
            }

        }
    }

    public function salesOrderCreditmemoRegisterBefore($observer)
    {
        $request = $observer['request'];
        if ($request->getActionName() == "updateQty") return $this;

        $creditmemo = $observer['creditmemo'];

        $input = $request->getParam('creditmemo');
        $order = $creditmemo->getOrder();

        // Refund point to customer (that he used to spend)
        if (isset($input['refund_fundraiser_points']) && $input['refund_fundraiser_points'] > 0) {
            $refundFundraiserEarn = $input['refund_fundraiser_points'];
            $maxPoint = $order->getData('fundraiser_earn');
            $refundBalances = min($refundFundraiserEarn, $maxPoint);
            $creditmemo->setFundraiserEarn(max($refundBalances, 0));
        }

        //Brian allow creditmemo when creditmemo total equal zero
        if (Mage::app()->getStore()->roundPrice($creditmemo->getGrandTotal()) <= 0) {
            $creditmemo->setAllowZeroGrandTotal(true);
        }

        return $this;
    }

    public function creditmemoSaveAfter(Varien_Event_Observer $observer)
    {
        $creditmemo = $observer->getEvent()->getCreditmemo();
        $order_id = $creditmemo->getOrderId();

        $order = Mage::getSingleton('sales/order');
        $order->load($order_id);
        $amount_credit = $creditmemo->getFundraiserEarn();
        $customer_id = $creditmemo->getCustomerId();

        $memo_items = $creditmemo->getAllItems();
        if (sizeof($memo_items) > 0) {
            if ($creditmemo->getFundraiserEarn() > 0) {
                $customer_helped_id = Mage::helper('fundraiser')->getAffiliateFundraiserNameByOrder($order_id, false);
                if($customer_helped_id != null) {
                    $fundraiserAccount = Mage::helper('fundraiser/account')->updatePoint($customer_helped_id, -$amount_credit);
                    $data = array(
                        'title' => Mage::helper('fundraiser')->__('Refund order #<a href="' . Mage::getUrl('sales/order/view', array('order_id' => $order->getId())) . '">' . $order->getIncrementId() . '</a>'),
                        'order_id' => $order->getEntityId(),
                        'changed_point' => -$amount_credit,
                    );
                    Mage::helper('fundraiser/transaction')->createTransaction(
                        $fundraiserAccount,
                        $data,
                        Magestore_Fundraiser_Model_Type::TYPE_TRANSACTION_REFUND_ORDER,
                        Magestore_Fundraiser_Model_Status::STATUS_TRANSACTION_COMPLETED
                    );
                }
            }
        }
    }

    public function addAutoloader(Varien_Event_Observer $observer)
    {
        require_once Mage::getBaseDir().'/phpGrid/conf.php';
        return $this;
    }
}
