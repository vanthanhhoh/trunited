<?php

class Magestore_Fundraiser_Model_Admin_Observer
{

    /**
     * @param $observer
     * @return $this
     * @throws Exception
     */
    public function customerSaveAfter($observer)
    {
        $customer = $observer['customer'];
        $params = Mage::app()->getRequest()->getParam('Fundraiser');

        if(!isset($params) || $params['fundraiser_point'] == '')
            return $this;

        if(filter_var($params['fundraiser_point'], FILTER_VALIDATE_FLOAT) === false)
            throw new Exception(
                Mage::helper('fundraiser')->__('Data invalid')
            );

        $connection = Mage::getSingleton('core/resource')->getConnection('core_write');

        try {
            $connection->beginTransaction();

            $fundraiserAccount = Mage::helper('fundraiser/account')->updatePoint($customer->getId(), $params['fundraiser_point']);
            if($fundraiserAccount != null)
            {
                $params['changed_point'] = $params['fundraiser_point'];
                Mage::helper('fundraiser/transaction')->createTransaction(
                    $fundraiserAccount,
                    $params,
                    Magestore_Fundraiser_Model_Type::TYPE_TRANSACTION_BY_ADMIN,
                    Magestore_Fundraiser_Model_Status::STATUS_TRANSACTION_COMPLETED
                );
            }

            $connection->commit();

        } catch (Exception $e) {
            $connection->rollback();
            Mage::getSingleton('adminhtml/session')->addError(
                Mage::helper('fundraiser')->__($e->getMessage())
            );
        }

    }
}
