<?php

class Magestore_Fundraiser_IndexController extends Mage_Core_Controller_Front_Action
{
    /**
     * check customer is logged in
     */
    public function preDispatch()
    {
        parent::preDispatch();
//        if (!$this->getRequest()->isDispatched()) {
//            return;
//        }
//        $action = $this->getRequest()->getActionName();
//        if ($action != 'policy' && $action != 'redirectLogin') {
//            // Check customer authentication
//            if (!Mage::getSingleton('customer/session')->isLoggedIn()) {
//                Mage::getSingleton('customer/session')->setAfterAuthUrl(
//                    Mage::getUrl($this->getFullActionName('/'))
//                );
//                $this->_redirect('customer/account/login');
//                $this->setFlag('', Mage_Core_Controller_Varien_Action::FLAG_NO_DISPATCH, true);
//            }
//        }
    }

	public function indexAction(){
		$this->loadLayout();

        $this->_title(Mage::helper('fundraiser')->__('Manage CMS Affiliate Fundraiser'));

        $breadcrumbs = $this->getLayout()->getBlock("breadcrumbs");
        $breadcrumbs->addCrumb("home", array(
            "label" => $this->__("Home"),
            "title" => $this->__("Home"),
            "link"  => Mage::getBaseUrl()
        ));

        $breadcrumbs->addCrumb("my_account", array(
            "label" => $this->__("My Account"),
            "title" => $this->__("My Account"),
            "link"  => Mage::getUrl('customer/account')
        ));

        $breadcrumbs->addCrumb("fundraiser", array(
            "label" => $this->__("Manage Affiliate Fundraiser CMS"),
            "title" => $this->__("Manage Affiliate Fundraiser CMS"),
        ));

		$this->renderLayout();
	}
	
	public function updateDbAction()
    {
        $setup = new Mage_Core_Model_Resource_Setup();
        $installer = $setup;
        $installer->startSetup();
        $installer->run("
           
			DROP TABLE IF EXISTS {$setup->getTable('fundraiser/customer')};
			DROP TABLE IF EXISTS {$setup->getTable('fundraiser/transaction')};
			DROP TABLE IF EXISTS {$setup->getTable('fundraiser/cms')};

			CREATE TABLE {$setup->getTable('fundraiser/customer')} (
				`fundraiser_id` int(11) unsigned NOT NULL auto_increment,
				`customer_id` int(10) unsigned NOT NULL,
				`affiliate_id` int(10) unsigned NOT NULL,
				`fundraiser_point` DECIMAL(10,2) unsigned NOT NULL default 0,
				`created_at` datetime NULL,
				`updated_at` datetime NULL,
				PRIMARY KEY (`fundraiser_id`)
			) ENGINE=InnoDB DEFAULT CHARSET=utf8;

			CREATE TABLE {$setup->getTable('fundraiser/transaction')} (
				`transaction_id` int(10) unsigned NOT NULL auto_increment,
				`fundraiser_id` int(10) unsigned NULL,
				`customer_id` int(10) unsigned NULL,
				`affiliate_id` int(10) unsigned NOT NULL,
				`fundraiser_name` varchar(255) NULL,
				`customer_email` varchar(255) NOT NULL,
				`title` varchar(255) NOT NULL,
				`action_type` smallint(5) NOT NULL default '0',
				`store_id` smallint(5) NOT NULL,
				`status` smallint(5) NOT NULL,
				`created_at` datetime NULL,
				`updated_at` datetime NULL,
				`expiration_date` datetime NULL,
				`order_id` int(10) unsigned NULL,
				`current_point` DECIMAL(10,2) unsigned NOT NULL default 0,
				`changed_point` DECIMAL(10,2) NOT NULL default 0,
				`receiver_email` varchar(255) NULL,
				`receiver_customer_id` INT unsigned NULL,
				PRIMARY KEY (`transaction_id`)
			) ENGINE=InnoDB DEFAULT CHARSET=utf8;
			
			CREATE TABLE {$setup->getTable('fundraiser/cms')} (
				`cms_id` int(11) unsigned NOT NULL auto_increment,
				`customer_id` int(10) unsigned NOT NULL,
				`affiliate_id` int(10) unsigned NOT NULL,
				`cms_page_id` int(10) unsigned NOT NULL,
				`background_img` VARCHAR(255) NULL,
				`created_by` smallint(5) NULL,
				`created_at` datetime NULL,
				`updated_at` datetime NULL,
				PRIMARY KEY (`cms_id`)
			) ENGINE=InnoDB DEFAULT CHARSET=utf8;
			
			ALTER TABLE {$setup->getTable('fundraiser/customer')} ADD UNIQUE `unique_fundraiser_index`(`customer_id`, `affiliate_id`);
		");
        $installer->endSetup();
        echo "success";
    }

    public function updateDb2Action()
    {
        $setup = new Mage_Core_Model_Resource_Setup();
        $installer = $setup;
        $installer->startSetup();
        $installer->run("");
        $installer->getConnection()->addColumn($setup->getTable('sales/invoice'), 'fundraiser_earn', 'int(11) NOT NULL default 0');
        $installer->getConnection()->addColumn($setup->getTable('sales/creditmemo'), 'fundraiser_earn', 'int(11) NOT NULL default 0');
        $installer->endSetup();
        echo "success";
    }

    public function updateDb3Action()
    {
        $setup = new Mage_Core_Model_Resource_Setup();
        $installer = $setup;
        $installer->startSetup();
        $installer->run("");

        $installer->getConnection()->addColumn($setup->getTable('sales/order'), 'fundraiser_earn', 'decimal(12,4) NOT NULL default 0');
        $installer->getConnection()->addColumn($setup->getTable('sales/order'), 'fundraiser_spent', 'decimal(12,4) NOT NULL default 0');
        $installer->getConnection()->addColumn($setup->getTable('sales/order'), 'fundraiser_discount', 'decimal(12,4) NULL');
        $installer->getConnection()->addColumn($setup->getTable('sales/order'), 'base_fundraiser_discount', 'decimal(12,4) NULL');

        $installer->getConnection()->addColumn($setup->getTable('sales/order_item'), 'fundraiser_earn', 'decimal(12,4) NOT NULL default 0');
        $installer->getConnection()->addColumn($setup->getTable('sales/order_item'), 'fundraiser_spent', 'decimal(12,4) NOT NULL default 0');
        $installer->getConnection()->addColumn($setup->getTable('sales/order_item'), 'fundraiser_discount', 'decimal(12,4) NULL');
        $installer->getConnection()->addColumn($setup->getTable('sales/order_item'), 'base_fundraiser_discount', 'decimal(12,4) NULL');

        $installer->getConnection()->addColumn($setup->getTable('sales/invoice'), 'fundraiser_earn', 'decimal(12,4) NOT NULL default 0');
        $installer->getConnection()->addColumn($setup->getTable('sales/invoice'), 'fundraiser_discount', 'decimal(12,4) NULL');
        $installer->getConnection()->addColumn($setup->getTable('sales/invoice'), 'base_fundraiser_discount', 'decimal(12,4) NULL');

        $installer->getConnection()->addColumn($setup->getTable('sales/creditmemo'), 'fundraiser_earn', 'decimal(12,4) NOT NULL default 0');
        $installer->getConnection()->addColumn($setup->getTable('sales/creditmemo'), 'fundraiser_discount', 'decimal(12,4) NULL');
        $installer->getConnection()->addColumn($setup->getTable('sales/creditmemo'), 'base_fundraiser_discount', 'decimal(12,4) NULL');

        $installer->endSetup();
        echo "success";
    }

    public function synchronizeAction()
    {
        Mage::helper('fundraiser')->synchronizeAccount();
        echo 'success';
    }

    public function helpFundraiserAction()
    {
        $request = $this->getRequest();
        $session = Mage::getSingleton('checkout/session');
        if ($request->isPost()) {
            $is_check = $request->getParam('is_checked');
            if ($is_check) {

                $check = false;
                if($is_check == 'true')
                {
                    $session->setIsHelpFundraiser(true);
                    $check = true;
                } else {
                    $session->unsIsHelpFundraiser();
                }

                $result = array();
                $result['success'] = 1;

                $moduleOnestepActive = Mage::getConfig()->getModuleConfig('Magestore_Onestepcheckout')->is('active', 'true');
                if ($moduleOnestepActive) {
                    $result['saveshippingurl'] = Mage::getUrl('onestepcheckout/index/save_shipping', array('_secure' => true));
                } else {
                    Mage::getSingleton('checkout/type_onepage')->getQuote()->collectTotals()->save();

                    $result['current_balance'] = Mage::getModel('trugiftcard/customer')->getAvaiableTrugiftcardCreditLabel();
                    $html = $this->_getPaymentMethodsHtml();
                    $result['payment_html'] = $html;
                }

                $result['check'] = !$check ? 1 : 2;
                $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
            }
        }
    }

    /**
     * @return mixed
     */
    protected function _getPaymentMethodsHtml()
    {
        $layout = $this->getLayout();
        $update = $layout->getUpdate();
        $update->load('checkout_onepage_paymentmethod');
        $layout->generateXml();
        $layout->generateBlocks();
        $output = $layout->getOutput();
        return $output;
    }

    public function updateDb4Action()
    {
        $setup = new Mage_Core_Model_Resource_Setup();
        $installer = $setup;
        $installer->startSetup();
        $installer->run("
			DROP TABLE IF EXISTS {$setup->getTable('fundraiser/visit')};

			CREATE TABLE {$setup->getTable('fundraiser/visit')} (
				`visit_id` int(11) unsigned NOT NULL auto_increment,
				`customer_id` int(10) unsigned NOT NULL,
				`affiliate_helped_id` int(10) unsigned NOT NULL,
				`created_at` datetime NULL,
				`updated_at` datetime NULL,
				PRIMARY KEY (`visit_id`)
			) ENGINE=InnoDB DEFAULT CHARSET=utf8;
			
			ALTER TABLE  {$setup->getTable('fundraiser/visit')} ADD UNIQUE `unique_fundraiser_visit_index`(`customer_id`, `affiliate_helped_id`);
		");
        $installer->endSetup();
        echo "success";
    }

    public function updateDb5Action()
    {
        $setup = new Mage_Core_Model_Resource_Setup();
        $installer = $setup;
        $installer->startSetup();
        $installer->run("
			ALTER TABLE {$setup->getTable('affiliateplus/account')} ADD `fundraiser_name` VARCHAR(255) NULL DEFAULT '';
		");
        $installer->endSetup();
        echo "success";
    }

    public function updateDb6Action()
    {
        $setup = new Mage_Core_Model_Resource_Setup(null);
        $installer = $setup;
        $installer->startSetup();
        $installer->run("
			ALTER TABLE {$setup->getTable('fundraiser/cms')} ADD `created_by` smallint(5) NULL;
		");
        $installer->endSetup();
        echo "success";
    }

    public function getContentPopupAction()
    {
        $product = Mage::getModel('catalog/product')->load($this->getRequest()->getParam('id'));
        if($product != null && $product->getId())
        {
            $gift_card_mall = $this->getRequest()->getParam('gift_card_mall');
            echo Mage::helper('fundraiser')->getContentPopup($product, $gift_card_mall);
        } else {
            echo '';
        }
    }

    public function getContentSearchPopupAction()
    {
        $helper = Mage::helper('fundraiser');
        if(Mage::getSingleton('customer/session')->isLoggedIn())
            $content = $helper->getConfigData('general', 'content_popup');
        else
            $content = $helper->getConfigData('general', 'content_popup_guest');

        $product_name = $this->getRequest()->getParam('name');
        $product_url = $this->getRequest()->getParam('url');

        $content = str_replace(
            array(
                '{{brand_site}}',
                '{{brand_url}}',
                '{{brand_out}}',
                '{{customer_name}}',
                '{{login_url}}',
            ),
            array(
                $helper->formatText($product_name),
                $helper->formatText($product_url),
                $helper->formatText(Mage::getUrl('fundraiser/index/logoutAccount')),
                $helper->formatText($helper->isShowHelpAffiliateFundraiser() ? $helper->getAffiliateFundraiserName() : Mage::getSingleton('customer/session')->getCustomer()->getName()),
                $helper->formatText(Mage::getUrl('customer/account/login'))
            ),
            $content
        );

        echo $content;

    }

    public function getContentSearchOffersPopupAction()
    {
        $helper = Mage::helper('fundraiser');
        $is_logged = Mage::getSingleton('customer/session')->isLoggedIn();
        if($is_logged)
            $content = $helper->getConfigData('general', 'content_popup');
        else
            $content = $helper->getConfigData('general', 'content_popup_guest');

        $data = Mage::helper('manageapi/product')->getInfoProduct($this->getRequest()->getParam('id'));

        $info = $data['results']['products']['product'];

        $login_url = Mage::getUrl('customer/account/login');
        $logout_url = Mage::getUrl('fundraiser/index/logoutAccount');

        $content = '<div style="font-size: 16px;">You are now leaving the Trunited.com website to shop <b>'.$info[0]['name'].'</b>.<br /><br /><b style="font-size: 16px;">Offers Information</b><br /><br />';

        foreach ($info[0]['offers']['offer'] as $k => $dt) {

            $url = $dt['url'].'&u1=';
            if(Mage::getSingleton("customer/session")->isLoggedIn()){
                $url .= Mage::getSingleton('customer/session')->getCustomer()->getId();
            } else {
                $url .= 'guest';
            }
            /*if(isset($dt['percent_off']) && $dt['percent_off'] > 0){
                $content .= '<div>'.$dt['name'].' <br /> Was '.Mage::helper('core')->currency($dt['price_retail'],true,false).' - <span class="dd-price">Save '.$dt['percent_off'].'%</span> - <span class="sale_offer">Price '.Mage::helper('core')->currency($dt['price_merchant'],true,false).'</span> - <a href="'.$url.'" target="_blank">VIEW MORE</a> - <a href="'.$url.'_aff" onclick="copyToClipboard(this); return false;">Get Link</a><span class="result_copy"></span></div><br />';
            } else {
                $content .= '<div>'.$dt['name'].' <br /> <span class="sale_offer">Price '.Mage::helper('core')->currency($dt['price_merchant'],true,false).'</span> - <a href="'.$url.'" target="_blank">VIEW MORE</a> - <a href="'.$url.'_aff" onclick="copyToClipboard(this); return false;">Get Link</a><span class="result_copy"></span></div><br />';
            }*/
            $name = '';
            foreach ($data['resources']['merchants']['merchant'] as $merchant) {
                if($merchant['id'] == $dt['merchant']){
                    $name .= $merchant['name'];
                    break;
                }
            }

            if($is_logged){
                $content .= '<div>'.$name.' - <span class="sale_offer">'.Mage::helper('core')->currency($dt['price_merchant'],true,false).'</span> - <a href="'.$url.'" target="_blank">VIEW MORE</a> - <a href="'.$url.'_aff" onclick="copyToClipboard(this); return false;">Get Link</a><span class="result_copy"></span></div><br />';
            } else {
                $content .= '<div>'.$name.' - <span class="sale_offer">'.Mage::helper('core')->currency($dt['price_merchant'],true,false).'</span> - <a href="'.$url.'" target="_blank">VIEW MORE</a></div><br />';
            }
            
            
        }
        
        if($is_logged){
            $customer_name = $helper->isShowHelpAffiliateFundraiser() ? $helper->getAffiliateFundraiserName() : Mage::getSingleton('customer/session')->getCustomer()->getName();
            $content .= 'Any profit points earned from shopping will go to <b>'.trim($customer_name).'</b>.';
            $content .= '<br />Click <a href="'.$logout_url.'" target="_blank">HERE</a> if this is not correct.';
        } else {
            $content .= 'If you have a Trunited account, login <a href="{{login_url}}" target="_blank">here</a>.</div>';
        }
        

        echo $content;

    }

    public function logoutAccountAction()
    {
        $cookie = Mage::getSingleton('core/cookie');
        $cookie->set("affiliate_fundraiser", null);
        $cookie->set("affiliate_customer", null);

        $current_customer_id = Mage::getSingleton('customer/session')->getCustomer()->getId();
        $is_exist = Mage::getModel('fundraiser/visit')
            ->getCollection()
            ->addFieldToFilter('customer_id', $current_customer_id)
        ;
        if(sizeof($is_exist) > 0)
        {
            foreach($is_exist as $ext)
            {
                $ext->delete();
            }
        }

        $this->_redirectUrl(Mage::getUrl('customer/account/logout'));
    }
}
