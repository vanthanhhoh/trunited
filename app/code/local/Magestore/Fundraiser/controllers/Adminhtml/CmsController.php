<?php

class Magestore_Fundraiser_Adminhtml_CmsController extends Mage_Adminhtml_Controller_Action
{
    protected function _initAction()
    {
        $this->loadLayout()
            ->_setActiveMenu('fundraiser/cms')
            ->_addBreadcrumb(Mage::helper('adminhtml')->__('Items Manager'), Mage::helper('adminhtml')->__('Item Manager'));
        return $this;
    }

    public function indexAction()
    {
        $this->_initAction()
            ->renderLayout();
    }

    public function editAction()
    {
        $id = $this->getRequest()->getParam('id');
        $model = Mage::getModel('fundraiser/cms')->load($id);

        if ($model->getId() || $id == 0) {
            $data = Mage::getSingleton('adminhtml/session')->getFormData(true);
            if (!empty($data)){
                $data = $this->getFullData($data);
                $model->setData($data);
            }

            if($model->getId()) {
                $model->setData($this->getFullData($model->getData()));
            }

            Mage::register('cms_data', $model);

            $this->loadLayout();
            $this->_setActiveMenu('fundraiser/cms');

            $this->_addBreadcrumb(Mage::helper('adminhtml')->__('CMS Pages Manager'), Mage::helper('adminhtml')->__('CMS Pages Manager'));
            $this->_addBreadcrumb(Mage::helper('adminhtml')->__('CMS Page News'), Mage::helper('adminhtml')->__('CMS Page News'));

            $this->getLayout()->getBlock('head')->setCanLoadExtJs(true);
            $this->_addContent($this->getLayout()->createBlock('fundraiser/adminhtml_cms_edit'))
                ->_addLeft($this->getLayout()->createBlock('fundraiser/adminhtml_cms_edit_tabs'));

            $this->renderLayout();
        } else {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('fundraiser')->__('CMS Page does not exist'));
            $this->_redirect('*/*/');
        }
    }

    public function getFullData($data)
    {
        $cms_page = Mage::getModel('cms/page')->load($data['cms_page_id']);

        $data['title'] = $cms_page->getTitle();
        $data['identifier'] = $cms_page->getIdentifier();
        $data['is_active'] = $cms_page->getIsActive();
        $data['cms_content'] = $cms_page->getContent();
        $data['meta_keywords'] = $cms_page->getMetaKeywords();
        $data['meta_description'] = $cms_page->getMetaDescription();
        return $data;
    }

    public function newAction()
    {
        $this->_forward('edit');
    }

    public function saveAction()
    {
        if ($data = $this->getRequest()->getPost()) {

            if(isset($_FILES['background_img']['name']) && $_FILES['background_img']['name'] != null){
                $data['background_img'] = $this->_uploadBackgroundFile('background_img');
            } else if(isset($data['background_img']) && is_array($data['background_img'])){

                if(isset($data['background_img']['delete']) && $data['background_img']['delete'] == 1){
                    $data['background_img'] = '';
                } else if(isset($data['background_img']['value'])) {
                    $data['background_img'] = str_replace('cms/fundraiser/','', $data['background_img']['value']);
                } else {
                    $data['background_img'] = $this->_uploadBackgroundFile('background_img');
                }
            } else if ($sourceFile = $this->_uploadBackgroundFile('background_img')) {
                $data['background_img'] = $sourceFile;
            }

            $data = $this->_processParams($data);

            $model = Mage::getModel('fundraiser/cms');

            $connection = Mage::getSingleton('core/resource')->getConnection('core_write');
            try {
                $connection->beginTransaction();

                $affiliate = Mage::getModel('affiliateplus/account')->load($data['affiliate_id']);

                if($this->getRequest()->getParam('id')){
                    $_model = Mage::getModel('fundraiser/cms')->load($this->getRequest()->getParam('id'));
                    $cms_page = Mage::getModel('cms/page')->load($_model->getCmsPageId());
                    $data['page_id'] = $cms_page->getId();
                } else {
                    $cms_page = Mage::getModel('cms/page');
                    $data['created_by'] = Magestore_Fundraiser_Model_Status::CREATED_BY_ADMIN;
                }

                $cms_page->setData($data);
                Mage::dispatchEvent('cms_page_prepare_save', array('page' => $model, 'request' => $this->getRequest()));

                $cms_page->save();

                $data['cms_page_id'] = $cms_page->getId();
                $data['customer_id'] = $affiliate->getCustomerId();

                $model->setData($data)
                    ->setId($this->getRequest()->getParam('id'));

                $model->save();

                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('fundraiser')->__('CMS Page was successfully saved'));
                Mage::getSingleton('adminhtml/session')->setFormData(false);

                $connection->commit();

                if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('*/*/edit', array('id' => $model->getId()));
                    return;
                }

                $this->_redirect('*/*/');
                return;
            } catch (Exception $e) {
                $connection->rollback();
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setFormData($data);

                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            }
        }
        Mage::getSingleton('adminhtml/session')->addError(Mage::helper('fundraiser')->__('Unable to find CMS Page to save'));
        $this->_redirect('*/*/');
    }

    public function _processParams($data)
    {
        $data['stores'] = array(1);
        $data['content_heading'] = '';
        $data['content'] = $data['cms_content'];
        $data['root_template'] = 'custom_static_page_empty';
        $data['layout_update_xml'] = '';
        $data['custom_theme_from'] = '';
        $data['custom_theme_to'] = '';
        $data['custom_theme'] = '';
        $data['custom_root_template'] = '';
        $data['custom_layout_update_xml'] = '';
        return $data;
    }

    /**
     * Upload banner file to server
     *
     * @param string $fieldName
     * @return string
     */
    protected function _uploadBackgroundFile($fieldName)
    {
        if(isset($_FILES[$fieldName]['delete']) && $_FILES[$fieldName]['delete'] != 1){
            return '';
        }

        if (isset($_FILES[$fieldName]['name']) && $_FILES[$fieldName]['name'] != '') {
            try {
                $uploader = new Varien_File_Uploader($fieldName);
                $uploader->setAllowedExtensions(array('jpg', 'jpeg', 'gif', 'png', 'swf'));
                $uploader->setAllowRenameFiles(true);
                $uploader->setFilesDispersion(false);

                // We set media as the upload dir
                $path = Mage::getBaseDir('media') . DS . 'cms' . DS . 'fundraiser' . DS;
                $result = $uploader->save($path, $_FILES[$fieldName]['name']);

                return $result['file'];
            } catch (Exception $e) {
                return $_FILES[$fieldName]['name'];
            }
        }
        return '';
    }

    public function deleteAction()
    {
        if ($this->getRequest()->getParam('id') > 0) {
            try {
                $model = Mage::getModel('fundraiser/cms');
                $model->setId($this->getRequest()->getParam('id'))
                    ->delete();
                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('CMS Page was successfully deleted'));
                $this->_redirect('*/*/');
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
            }
        }
        $this->_redirect('*/*/');
    }

    public function massDeleteAction()
    {
        $fundraiserIds = $this->getRequest()->getParam('cms'); 
        if (!is_array($fundraiserIds)) {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('adminhtml')->__('Please select CMS Page(s)'));
        } else {
            try {
                foreach ($fundraiserIds as $fundraiserId) {
                    $fundraiser = Mage::getModel('fundraiser/cms')->load($fundraiserId);
                    $fundraiser->delete();
                }
                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Total of %d record(s) were successfully deleted', count($fundraiserIds)));
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
        }
        $this->_redirect('*/*/index');
    }

    public function massStatusAction()
    {
        $fundraiserIds = $this->getRequest()->getParam('cms');
        if (!is_array($fundraiserIds)) {
            Mage::getSingleton('adminhtml/session')->addError($this->__('Please select CMS Page(s)'));
        } else {
            try {
                foreach ($fundraiserIds as $fundraiserId) {
                    $fundraiser = Mage::getSingleton('fundraiser/cms')
                        ->load($fundraiserId)
                        ->setStatus($this->getRequest()->getParam('status'))
                        ->setIsMassupdate(true)
                        ->save();
                }
                $this->_getSession()->addSuccess(
                    $this->__('Total of %d record(s) were successfully updated', count($fundraiserIds))
                );
            } catch (Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            }
        }
        $this->_redirect('*/*/index');
    }

    public function exportCsvAction()
    {
        $fileName = 'fundraiser_cms.csv';
        $content = $this->getLayout()->createBlock('fundraiser/adminhtml_cms_grid')->getCsv();
        $this->_prepareDownloadResponse($fileName, $content);
    }

    public function exportXmlAction()
    {
        $fileName = 'fundraiser_cms.xml';
        $content = $this->getLayout()->createBlock('fundraiser/adminhtml_cms_grid')->getXml();
        $this->_prepareDownloadResponse($fileName, $content);
    }

    public function chooseAction()
    {
        $uniqId = $this->getRequest()->getParam('uniq_id');
        $pagesGrid = $this->getLayout()->createBlock('fundraiser/adminhtml_widget_affiliate_chooser', '', array(
            'id' => $uniqId,
        ));
        $this->getResponse()->setBody($pagesGrid->toHtml());
    }
}
