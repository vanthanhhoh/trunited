<?php

class Magestore_Fundraiser_CmsController extends Mage_Core_Controller_Front_Action
{
    /**
     * check customer is logged in
     */
    public function preDispatch()
    {
        parent::preDispatch();
        if (!$this->getRequest()->isDispatched()) {
            return;
        }
        $action = $this->getRequest()->getActionName();
        if ($action != 'policy' && $action != 'redirectLogin') {
            // Check customer authentication
            if (!Mage::getSingleton('customer/session')->isLoggedIn()) {
                Mage::getSingleton('customer/session')->setAfterAuthUrl(
                    Mage::getUrl($this->getFullActionName('/'))
                );
                $this->_redirect('customer/account/login');
                $this->setFlag('', Mage_Core_Controller_Varien_Action::FLAG_NO_DISPATCH, true);
            }
        }
    }

    public function saveAction()
    {
        if ($data = $this->getRequest()->getPost()) {

            if(isset($_FILES['background_img']['name']) && $_FILES['background_img']['name'] != null){
                $_upload = $this->_uploadBackgroundFile('background_img');
                if($_upload != null && $_upload != '')
                    $data['background_img'] = $_upload;
            } else if(isset($data['background_img']) && is_array($data['background_img'])){

                if(isset($data['background_img']['delete']) && $data['background_img']['delete'] == 1){
                    $data['background_img'] = '';
                } else if(isset($data['background_img']['value'])) {
                    $data['background_img'] = str_replace('cms/fundraiser/','', $data['background_img']['value']);
                } else {
                    $_upload = $this->_uploadBackgroundFile('background_img');
                    if($_upload != null && $_upload != '')
                        $data['background_img'] = $_upload;
                }
            } else if ($sourceFile = $this->_uploadBackgroundFile('background_img')) {
                if($sourceFile != null && $sourceFile != '')
                    $data['background_img'] = $sourceFile;
            }

            $data = $this->_processParams($data);

            $model = Mage::getModel('fundraiser/cms');

            $connection = Mage::getSingleton('core/resource')->getConnection('core_write');
            try {
                $connection->beginTransaction();

                $affiliate = Mage::getModel('affiliateplus/account')->load($data['affiliate_id']);

                if($this->getRequest()->getParam('id') > 0){
                    $_model = Mage::getModel('fundraiser/cms')->load($this->getRequest()->getParam('id'));
                    $cms_page = Mage::getModel('cms/page')->load($_model->getCmsPageId());
                    $data['page_id'] = $cms_page->getId();
                } else {
                    $cms_page = Mage::getModel('cms/page');
                    $data['created_by'] = Magestore_Fundraiser_Model_Status::CREATED_BY_CUSTOMER;
                }

                $cms_page->setData($data);
                $cms_page->save();

                $data['cms_page_id'] = $cms_page->getId();
                $data['customer_id'] = $affiliate->getCustomerId();

                $model->setData($data)
                    ->setId($this->getRequest()->getParam('id') > 0 ? $this->getRequest()->getParam('id') : null);

                $model->save();

                Mage::getSingleton('core/session')->addSuccess(Mage::helper('fundraiser')->__('CMS Page was successfully saved'));
                Mage::getSingleton('core/session')->setFormData(false);

                $connection->commit();

                $this->_redirect('*/index/');
                return;
            } catch (Exception $e) {
                $connection->rollback();
                Mage::getSingleton('core/session')->addError($e->getMessage());
                Mage::getSingleton('core/session')->setFormData($data);

                $this->_redirect('*/index/');
                return;
            }
        }
        Mage::getSingleton('core/session')->addError(
            Mage::helper('fundraiser')->__('Unable to find CMS Page to save')
        );
        $this->_redirect('*/index/');
    }

    /**
     * Upload banner file to server
     *
     * @param string $fieldName
     * @return string
     */
    protected function _uploadBackgroundFile($fieldName)
    {

        if(isset($_FILES[$fieldName]['delete']) && $_FILES[$fieldName]['delete'] != 1){
            return '';
        }

        if (isset($_FILES[$fieldName]['name']) && $_FILES[$fieldName]['name'] != '') {
            try {
                $uploader = new Varien_File_Uploader($fieldName);
                $uploader->setAllowedExtensions(array('jpg', 'jpeg', 'gif', 'png', 'swf'));
                $uploader->setAllowRenameFiles(true);
                $uploader->setFilesDispersion(false);

                // We set media as the upload dir
                $path = Mage::getBaseDir('media') . DS . 'cms' . DS . 'fundraiser' . DS;

                $result = $uploader->save($path, $_FILES[$fieldName]['name']);

                return $result['file'];
            } catch (Exception $e) {
                Mage::getSingleton('core/session')->addError($e->getMessage().'. Invalid file Size or Type');
            }
        }
        return '';
    }

    function generateRandomString($length = 5)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public function _processParams($data)
    {
        $data['stores'] = array(Mage::app()->getStore()->getStoreId());
        $data['content_heading'] = '';
        $data['content'] = $data['cms_content'];
        $data['root_template'] = 'custom_static_page_empty';
        $data['layout_update_xml'] = '';
        $data['custom_theme_from'] = '';
        $data['custom_theme_to'] = '';
        $data['custom_theme'] = '';
        $data['custom_root_template'] = '';
        $data['custom_layout_update_xml'] = '';
        return $data;
    }

}
