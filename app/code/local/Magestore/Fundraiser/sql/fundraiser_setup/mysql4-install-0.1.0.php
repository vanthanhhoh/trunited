<?php

$installer = $this;
$installer->startSetup();

$installer->run("
    ALTER TABLE {$this->getTable('affiliateplus/account')} ADD `fundraiser_name` VARCHAR(255) NULL DEFAULT '';
    
	DROP TABLE IF EXISTS {$this->getTable('fundraiser/customer')};
	DROP TABLE IF EXISTS {$this->getTable('fundraiser/transaction')};
	DROP TABLE IF EXISTS {$this->getTable('fundraiser/cms')};
	DROP TABLE IF EXISTS {$this->getTable('fundraiser/visit')};

	CREATE TABLE {$this->getTable('fundraiser/customer')} (
        `fundraiser_id` int(11) unsigned NOT NULL auto_increment,
        `customer_id` int(10) unsigned NOT NULL,
        `affiliate_id` int(10) unsigned NOT NULL,
        `fundraiser_point` DECIMAL(10,2) unsigned NOT NULL default 0,
        `created_at` datetime NULL,
        `updated_at` datetime NULL,
        PRIMARY KEY (`fundraiser_id`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

    CREATE TABLE {$this->getTable('fundraiser/transaction')} (
        `transaction_id` int(10) unsigned NOT NULL auto_increment,
        `fundraiser_id` int(10) unsigned NULL,
        `customer_id` int(10) unsigned NULL,
        `affiliate_id` int(10) unsigned NOT NULL,
        `fundraiser_name` varchar(255) NULL,
        `customer_email` varchar(255) NOT NULL,
        `title` varchar(255) NOT NULL,
        `action_type` smallint(5) NOT NULL default '0',
        `store_id` smallint(5) NOT NULL,
        `status` smallint(5) NOT NULL,
        `created_at` datetime NULL,
        `updated_at` datetime NULL,
        `expiration_date` datetime NULL,
        `order_id` int(10) unsigned NULL,
        `current_point` DECIMAL(10,2) unsigned NOT NULL default 0,
        `changed_point` DECIMAL(10,2) NOT NULL default 0,
        `receiver_email` varchar(255) NULL,
        `receiver_customer_id` INT unsigned NULL,
        PRIMARY KEY (`transaction_id`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
    
    CREATE TABLE {$this->getTable('fundraiser/cms')} (
        `cms_id` int(11) unsigned NOT NULL auto_increment,
        `customer_id` int(10) unsigned NOT NULL,
        `affiliate_id` int(10) unsigned NOT NULL,
        `cms_page_id` int(10) unsigned NOT NULL,
        `background_img` VARCHAR(255) NULL,
        `created_at` datetime NULL,
        `updated_at` datetime NULL,
        PRIMARY KEY (`cms_id`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
    
    CREATE TABLE {$this->getTable('fundraiser/visit')} (
        `visit_id` int(11) unsigned NOT NULL auto_increment,
        `customer_id` int(10) unsigned NOT NULL,
        `affiliate_helped_id` int(10) unsigned NOT NULL,
        `created_at` datetime NULL,
        `updated_at` datetime NULL,
        PRIMARY KEY (`visit_id`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
    
    ALTER TABLE {$this->getTable('fundraiser/visit')} ADD UNIQUE `unique_fundraiser_visit_index`(`customer_id`, `affiliate_helped_id`);
    ALTER TABLE {$this->getTable('fundraiser/customer')} ADD UNIQUE `unique_fundraiser_index`(`customer_id`, `affiliate_id`);

");

$installer->getConnection()->addColumn($this->getTable('sales/order'), 'fundraiser_earn', 'decimal(12,4) NOT NULL default 0');
$installer->getConnection()->addColumn($this->getTable('sales/order'), 'fundraiser_spent', 'decimal(12,4) NOT NULL default 0');
$installer->getConnection()->addColumn($this->getTable('sales/order'), 'fundraiser_discount', 'decimal(12,4) NULL');
$installer->getConnection()->addColumn($this->getTable('sales/order'), 'base_fundraiser_discount', 'decimal(12,4) NULL');

$installer->getConnection()->addColumn($this->getTable('sales/order_item'), 'fundraiser_earn', 'decimal(12,4) NOT NULL default 0');
$installer->getConnection()->addColumn($this->getTable('sales/order_item'), 'fundraiser_spent', 'decimal(12,4) NOT NULL default 0');
$installer->getConnection()->addColumn($this->getTable('sales/order_item'), 'fundraiser_discount', 'decimal(12,4) NULL');
$installer->getConnection()->addColumn($this->getTable('sales/order_item'), 'base_fundraiser_discount', 'decimal(12,4) NULL');

$installer->getConnection()->addColumn($this->getTable('sales/invoice'), 'fundraiser_earn', 'decimal(12,4) NOT NULL default 0');
$installer->getConnection()->addColumn($this->getTable('sales/invoice'), 'fundraiser_discount', 'decimal(12,4) NULL');
$installer->getConnection()->addColumn($this->getTable('sales/invoice'), 'base_fundraiser_discount', 'decimal(12,4) NULL');

$installer->getConnection()->addColumn($this->getTable('sales/creditmemo'), 'fundraiser_earn', 'decimal(12,4) NOT NULL default 0');
$installer->getConnection()->addColumn($this->getTable('sales/creditmemo'), 'fundraiser_discount', 'decimal(12,4) NULL');
$installer->getConnection()->addColumn($this->getTable('sales/creditmemo'), 'base_fundraiser_discount', 'decimal(12,4) NULL');



$installer->endSetup(); 