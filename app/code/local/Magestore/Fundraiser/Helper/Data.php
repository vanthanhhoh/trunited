<?php

class Magestore_Fundraiser_Helper_Data extends Mage_Core_Helper_Abstract
{
    public function getConfigData($group, $field, $store = null)
    {
        return Mage::getStoreConfig('fundraiser/'.$group.'/'.$field, $store);
    }

    public function isEnable()
    {
        return $this->getConfigData('general', 'enable');
    }

	public function createNewObj($customer_id, $affiliate_id)
    {
        $model =  Mage::getModel('fundraiser/customer');
        $data = array(
            'customer_id'   => $customer_id,
            'affiliate_id'  => $affiliate_id,
            'fundraiser_point'  => 0,
            'created_at'    => now(),
            'updated_at'    => now(),
        );

        $model->setData($data);

        try{
            $model->save();
        } catch (Exception $ex) {
            return null;
        }

        return $model;
    }

    public function synchronizeAccount()
    {
        $customer_collection = Mage::getModel('customer/customer')->getCollection()
            ->addAttributeToSelect('entity_id')
            ;

        if(sizeof($customer_collection) > 0)
        {
            $transactionSave = Mage::getModel('core/resource_transaction');
            foreach ($customer_collection as $customer) {
                $affiliate = Mage::getModel('affiliateplus/account')->load($customer->getId(), 'customer_id');
                if($affiliate != null && $affiliate->getId())
                {
                    try{
                        $obj = Mage::getModel('fundraiser/customer');
                        $data = array(
                            'customer_id'   => $customer->getId(),
                            'affiliate_id'  => $affiliate->getId(),
                            'fundraiser_point'  => 0,
                        );

                        $obj->setData($data);
                        $transactionSave->addObject($obj);
                    } catch (Exception $ex) {

                    }
                }
            }
            $transactionSave->save();
        }
    }

    public function saveCookie($cms_affiliate, $expiredTime)
    {
        $cookie = Mage::getSingleton('core/cookie');
        if ($expiredTime)
            $cookie->setLifeTime(intval($expiredTime) * 86400);
        $cookie->set("affiliate_fundraiser", $cms_affiliate->getAffiliateId());
        $cookie->set("affiliate_customer", $cms_affiliate->getCustomerId());

        $this->saveVisit($cms_affiliate->getAffiliateId());
    }

    public function saveVisit($affiliate_helped_id)
    {
        if($affiliate_helped_id == null)
            return;

        $current_customer_id = null;
        if(Mage::getSingleton('customer/session')->isLoggedIn()) {
            $current_customer_id = Mage::getSingleton('customer/session')->getCustomer()->getId();
        }

        if($current_customer_id != null)
        {
            $is_exist = Mage::getModel('fundraiser/visit')
                ->getCollection()
                ->addFieldToFilter('customer_id', $current_customer_id)
                ->getFirstItem()
                ;

            if($is_exist != null && $is_exist->getId())
            {
                if($is_exist->getData('affiliate_helped_id') != $affiliate_helped_id){
                    $is_exist->setData('affiliate_helped_id', $affiliate_helped_id);
                    $is_exist->save();
                }
            } else {

                $current_affiliate = Mage::getModel('affiliateplus/account')->load($current_customer_id, 'customer_id');
                if($current_affiliate == null || ($current_affiliate != null && $current_affiliate->getId() != $affiliate_helped_id))
                {
                    $model = Mage::getModel('fundraiser/visit');
                    $data = array(
                        'customer_id'   => $current_customer_id,
                        'affiliate_helped_id'   => $affiliate_helped_id
                    );

                    $model->setData($data);
                    $model->save();
                }
            }
        }
    }

    public function isShowHelpAffiliateFundraiser()
    {
        $cookie = Mage::getModel('core/cookie');
        if(Mage::getSingleton('customer/session')->isLoggedIn()) {
            $current_customer_id = Mage::getSingleton('customer/session')->getCustomer()->getId();
            $current_affiliate = Mage::getModel('affiliateplus/account')->load($current_customer_id, 'customer_id');

            $is_exist = Mage::getModel('fundraiser/visit')
                ->getCollection()
                ->addFieldToFilter('customer_id', $current_customer_id)
                ->getFirstItem()
            ;

            if($is_exist != null && $is_exist->getId()){
                $affiliate = Mage::getModel('affiliateplus/account')->load($is_exist->getData('affiliate_helped_id'));
                if($affiliate != null && $affiliate->getId()){
                    if($current_affiliate != null && $current_affiliate->getId() == $affiliate->getId())
                        return false;

                    if(($cookie->get('affiliate_fundraiser') != null && $is_exist->getData('affiliate_helped_id') != $cookie->get('affiliate_fundraiser')) ||
                        $cookie->get('affiliate_fundraiser') == null
                    ){
                        $cookie->setLifeTime(intval(Mage::helper('fundraiser')->getConfigData('general', 'cookie_expired_time')) * 86400);
                        $cookie->set("affiliate_fundraiser", $affiliate->getId());
                        $cookie->set("affiliate_customer", $affiliate->getCustomerId());
                    }
                    return true;
                } else {
                    return false;
                }
            } else {
                if($cookie->get('affiliate_fundraiser') != null){
                    $model = Mage::getModel('fundraiser/visit');
                    $model->setData('affiliate_helped_id', $cookie->get('affiliate_fundraiser'));
                    $model->setData('customer_id', $current_customer_id);
                    $model->save();

                    $affiliate = Mage::getModel('affiliateplus/account')->load($cookie->get('affiliate_fundraiser'));
                    if($affiliate != null && $affiliate->getId()){
                        return true;
                    } else
                        return false;
                } else
                    return false;
            }
        } else {
            if($cookie->get('affiliate_fundraiser') != null){
                $affiliate = Mage::getModel('affiliateplus/account')->load($cookie->get('affiliate_fundraiser'));
                if($affiliate != null && $affiliate->getId()){
                    return true;
                } else
                    return false;

            } else{
                return false;
            }
        }
    }

    public function getAffiliateFundraiserName()
    {
        if(!$this->isShowHelpAffiliateFundraiser())
            return '';

        $cookie = Mage::getModel('core/cookie');
        $name = $this->__('DEFAULT FUNDRAISER');
        if(Mage::getSingleton('customer/session')->isLoggedIn()) {
            $current_customer_id = Mage::getSingleton('customer/session')->getCustomer()->getId();
            $is_exist = Mage::getModel('fundraiser/visit')
                ->getCollection()
                ->addFieldToFilter('customer_id', $current_customer_id)
                ->getFirstItem()
            ;

            if($is_exist != null && $is_exist->getId()){
                $affiliate = Mage::getModel('affiliateplus/account')->load($is_exist->getData('affiliate_helped_id'));
                if($affiliate != null && $affiliate->getId()){
                    $name = $affiliate->getFundraiserName();
                }
            }
        } else {
            if($cookie->get('affiliate_fundraiser') != null){
                $affiliate = Mage::getModel('affiliateplus/account')->load($cookie->get('affiliate_fundraiser'));
                if($affiliate != null && $affiliate->getId()){
                    $name = $affiliate->getFundraiserName();
                }
            }
        }

        return $name;
    }

    public function getAffiliateFundraiserNameByOrder($order_id, $return_name = true)
    {
        $transaction = Mage::getModel('fundraiser/transaction')
            ->getCollection()
            ->addFieldToFilter('order_id', $order_id)
            ->addFieldToFilter('action_type', Magestore_Fundraiser_Model_Type::TYPE_TRANSACTION_RECEIVE_FROM_CUSTOMER_HELP)
            ->getFirstItem()
            ;

        if($transaction != null && $transaction->getId()){
            $affiliate = Mage::getModel('affiliateplus/account')->load($transaction->getAffiliateId());
            if($affiliate != null && $affiliate->getId())
                if($return_name)
                    return $affiliate->getFundraiserName();
                else
                    return $affiliate->getCustomerId();
        }

        return $this->__('DEFAULT FUNDRAISER');
    }

    public function findFundraiserByCustomerId($customer_id)
    {
        $is_exist = Mage::getModel('fundraiser/visit')
            ->getCollection()
            ->addFieldToFilter('customer_id', $customer_id)
            ->getFirstItem()
        ;

        if($is_exist != null && $is_exist->getId())
        {
            $affiliate = Mage::getModel('affiliateplus/account')->load($is_exist->getData('affiliate_helped_id'));
            if($affiliate != null && $affiliate->getId())
                return $affiliate->getCustomerId();
        }

        return null;
    }

    public function getContentPopup($product, $gift_card_mall = null)
    {
        if(Mage::getSingleton('customer/session')->isLoggedIn())
            $content = $this->getConfigData('general', 'content_popup');
        else
            $content = $this->getConfigData('general', 'content_popup_guest');

        $content = str_replace(
            array(
                '{{brand_site}}',
                '{{brand_url}}',
                '{{brand_out}}',
                '{{customer_name}}',
                '{{login_url}}',
            ),
            array(
                $this->formatText($gift_card_mall == null ? $product->getName() : 'GiftCardMall.com'),
                $this->formatText(Mage::helper('customproduct')->getShopNowUrl($product)),
                $this->formatText(Mage::getUrl('fundraiser/index/logoutAccount')),
                $this->formatText($this->isShowHelpAffiliateFundraiser() ? $this->getAffiliateFundraiserName() : Mage::getSingleton('customer/session')->getCustomer()->getName()),
                $this->formatText(Mage::getUrl('customer/account/login'))
            ),
            $content
        );

        return $content;
    }

    public function formatText($txt)
    {
        return str_replace("'", "'", $txt);
    }
}
