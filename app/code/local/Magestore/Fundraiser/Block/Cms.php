<?php

class Magestore_Fundraiser_Block_Cms extends Mage_Core_Block_Template
{
	public function _prepareLayout(){
		return parent::_prepareLayout();
	}

	public function getCmsId()
    {
        return $this->getRequest()->getParam('page_id');
    }

    public function getCms()
    {
        return Mage::getModel('cms/page')->load($this->getCmsId());
    }

    public function getFundraiserCms()
    {
        $cms = Mage::getModel('fundraiser/cms')->getCollection()
            ->addFieldToFilter('cms_page_id', $this->getCmsId())
            ->setOrder('cms_id', 'desc')
            ->getFirstItem()
            ;

        if($cms != null && $cms->getId())
            return $cms;
        else
            return null;
    }

    public function getBackgroundImage()
    {
        $cms = $this->getFundraiserCms();
        if($cms != null) {
            return  Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA).'cms'. DS .'fundraiser' . DS .  $cms->getBackgroundImg();
        }

        return '';
    }
}