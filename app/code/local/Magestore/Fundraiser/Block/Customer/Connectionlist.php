<?php
use phpGrid\C_DataGrid;
use phpGrid\C_DataBase;

class Magestore_Fundraiser_Block_Customer_Connectionlist extends Mage_Core_Block_Template
{
    protected $_dataGrid;
    protected $_coreResource;
    protected $_coreRead;

    public function _prepareLayout()
    {
        return parent::_prepareLayout();
    }

    public function getDataGrid()
    {
        if ($this->_dataGrid === null) {
            $this->_dataGrid = $this->getGridObject();
        }

        return $this->_dataGrid;
    }

    public function getCoreResource()
    {
        if ($this->_coreResource === null) {
            $this->_coreResource = Mage::getSingleton('core/resource');
        }

        return $this->_coreResource;
    }

    public function getCoreRead()
    {
        if ($this->_coreRead === null) {
            $this->_coreRead = $this->_coreResource->getConnection('core_read');
        }

        return $this->_coreRead;
    }

    public function getCurrentCustomerId()
    {
        return Mage::getSingleton('customer/session')->getCustomer()->getId();
    }

    public function getQueryData()
    {
        $customer_id = $this->getCurrentCustomerId();

        $sql = "SELECT g.PersonID                            AS 'key',";
        $sql .= " cC.child_id,";
        $sql .= " cC.customer_name                      AS person_name,";
        $sql .= " pC.Title                              AS person_title,";
        $sql .= " IFNULL(pC.Points, 0)                  AS person_points,";
        $sql .= " DATE_FORMAT(cC.StartDate, '%m-%d-%Y') AS person_start_date,";
        $sql .= " g.Level                               AS level_from_treerootid,";
        $sql .= " pC.DepthCredit                        AS depth_credit,";
        $sql .= " IFNULL(tb.trubox_points, 0)           AS trubox_points,";
        $sql .= " (top.Level - 1)                       AS PODS_Level,";
        $sql .= " ps.frontline,";
        $sql .= " IFNULL(nxt.maxrewardpts, 0)           AS next_level_points,";
        $sql .= " CONCAT('https://trunited.com/?acc=', cC.child_id) AS purl,";
        $sql .= " m.max_match_points,";
        $sql .= " ps.unearned_points,";
        $sql .= " tw.truwallet_credit AS truwallet,";
        $sql .= " IFNULL(tgc.trugiftcard_credit, 0) AS tgc_balance,";
        $sql .= " (CASE WHEN cC.parent_name LIKE 'SoComm%' THEN 'Trunited' ELSE cC.parent_name END) AS 'parent_name',";
        $sql .= " cC.parent_id";

        $sql .= " FROM tr_Genealogy    g";
        $sql .= " LEFT JOIN trtrugiftcard_customer tgc ON tgc.customer_id = g.personid";
        $sql .= " LEFT JOIN tr_People pP ON g.ParentId = pP.PersonID";
        $sql .= " JOIN tr_People pC ON g.PersonID = pC.PersonID";
        $sql .= " JOIN";
        $sql .= " (SELECT t.customer_id,t.customer_email,c.name AS customer_name,c.created_time   AS StartDate,c.identify_code  AS child_id, p.name AS parent_name, p.email          AS parent_email, p.identify_code   AS parent_id";
        $sql .= " FROM traffiliateplus_tracking      t JOIN traffiliateplus_account p ON t.account_id = p.account_id JOIN traffiliateplus_account c";
        $sql .= " ON t.customer_id = c.customer_id) cC ON pC.PersonID = cC.customer_id";
        $sql .= " JOIN tr_Titles tC ON pC.Title = tC.title";
        $sql .= " JOIN (SELECT PersonID, Level FROM tr_Genealogy WHERE TreeRootID = 2401) top ON pC.PersonID = top.PersonID";
        $sql .= " JOIN (SELECT t.TreeRootID,";
        $sql .= "  MAX( CASE WHEN t.pods_points_available > 0 AND t.Level = 1 THEN t.max_level_points";
        $sql .= " WHEN t.PersonalPts = 0 AND t.Level = 1 THEN t.max_level_points";
        $sql .= " WHEN t.Level > 0 AND t.Level <= d.DepthCredit THEN t.max_level_points ELSE 0";
        $sql .= " END) AS max_match_points";
        $sql .= " FROM tr_TreeSummary t JOIN (SELECT TreeRootID, DepthCredit FROM tr_TreeSummary WHERE Level = 0) d ON t.TreeRootID = d.TreeRootID";
        $sql .= " GROUP BY t.TreeRootID) m ON pC.PersonID = m.TreeRootID";
        $sql .= " JOIN tr_PayoutSummary ps ON g.PersonID = ps.PersonID";
        $sql .= " LEFT JOIN (SELECT t.customer_id, SUM(FORMAT(i.qty * i.price, 2))   AS trubox_total, SUM(i.qty * p.value)              AS trubox_points";
        $sql .= " FROM trtrubox      t JOIN trtrubox_item i ON t.trubox_id = i.trubox_id";
        $sql .= " JOIN trcatalog_product_entity_decimal p ON i.product_id = p.entity_id AND p.attribute_id = 166";
        $sql .= " GROUP BY t.customer_id) tb ON pC.PersonID = tb.customer_id";
        $sql .= " LEFT JOIN trtruwallet_customer tw ON g.PersonID = tw.customer_id";
        $sql .= " LEFT JOIN (SELECT treerootid, maxrewardpts FROM tr_TreeSummary WHERE Level = DepthCredit + 1) nxt ON g.PersonID = nxt.treerootid";
        $sql .= " WHERE g.TreeRootID = $customer_id";
        $sql .= " ORDER BY IF(g.TreeRootID = g.PersonID, 0, 1), g.Level";

        return $sql;

    }

    public function getGridObject()
    {
        $db = new C_DataBase(
            PHPGRID_DB_HOSTNAME,
            PHPGRID_DB_USERNAME,
            PHPGRID_DB_PASSWORD,
            PHPGRID_DB_NAME,
            PHPGRID_DB_TYPE,
            PHPGRID_DB_CHARSET
        );

        $results = $db->db_query($this->getQueryData());
        $data1 = array();
        $count = 0;
        while ($row = $db->fetch_array_assoc($results)) {
            for ($i = 0; $i < $db->num_fields($results); $i++) {
                $col_name = $db->field_name($results, $i);
                $data1[$count][$col_name] = $row[$col_name];
            }
            $count++;
        }
        if(sizeof($data1) == 0)
            return null;

        $dg = new C_DataGrid($data1, "key", "connectionList");

        $dg->enable_autowidth(true);

        $dg->before_script_end .= 'setTimeout(function(){$(window).bind("resize", function() {
        phpGrid_connectionList.setGridWidth($(".right_block_in").width());
    }).trigger("resize");}, 0)';

        $dg->add_column(
            "actions",
            array(
                'name' => 'actions',
                'index' => 'actions',
                'width' => '55',
                'formatter' => '###ratingformatter###'
            ),
            'Actions',
            1
        );

        $dg->set_col_title("key", "No.");
        $dg->set_col_title("child_id", "ID");
        $dg->set_col_title("person_name", "Name");
        $dg->set_col_title("person_title", "Badge");
        $dg->set_col_title("person_points", "Points");
        $dg->set_col_title("person_start_date", "Start Date");
        $dg->set_col_title("level_from_treerootid", "Level");
        $dg->set_col_title("depth_credit", "Depth Credit");
        $dg->set_col_title("parent_name", "Referred By");
        $dg->set_col_title("trubox_points", "TruBox Pts");
        $dg->set_col_title("PODS_Level", "PODS Level");
        $dg->set_col_title("unearned_points", "Unearned Points");
        $dg->set_col_title("max_match_points", "Optimize Match");
        $dg->set_col_title("truwallet", "TruWallet");
        $dg->set_col_title("next_level_points", "Next Lvl Pts");
        $dg->set_col_title("frontline", "Frontline");
        $dg->set_col_title("purl", "PURL");
        $dg->set_col_title("tgc_balance", "Trunited GC");
        $dg->set_col_title("parent_id", "Parent ID");

        $dg->set_col_align('child_id', 'center');
        $dg->set_col_align('person_name', 'center');
        $dg->set_col_align('person_title', 'center');
        $dg->set_col_align('person_points', 'center');
        $dg->set_col_align('person_start_date', 'center');
        $dg->set_col_align('level_from_treerootid', 'center');
        $dg->set_col_align('depth_credit', 'center');
        $dg->set_col_align('parent_name', 'center');
        $dg->set_col_align('trubox_points', 'center');
        $dg->set_col_align('PODS_Level', 'center');
        $dg->set_col_align('unearned_points', 'center');
        $dg->set_col_align('max_match_points', 'center');
        $dg->set_col_align('truwallet', 'center');
        $dg->set_col_align('next_level_points', 'center');
        $dg->set_col_align('frontline', 'center');
        $dg->set_col_align('purl', 'center');
        $dg->set_col_align('tgc_balance', 'center');
        $dg->set_col_align('parent_id', 'center');

        $dg->set_col_width("child_id", 40);
        $dg->set_col_width("person_title", 70);
        $dg->set_col_width("person_start_date", 60);
        $dg->set_col_width("level_from_treerootid", 50);
        $dg->set_col_width("person_points", 40);
        $dg->set_col_width("depth_credit", 50);
        $dg->set_col_width("trubox_points", 55);
        $dg->set_col_width("PODS_Level", 50);
        $dg->set_col_width("next_level_points", 50);
        $dg->set_col_width("unearned_points", 60);
        $dg->set_col_width("max_match_points", 60);
        $dg->set_col_width("truwallet", 50);
        $dg->set_col_width("frontline", 50);
        $dg->set_col_width("tgc_balance", 50);
        $dg->set_col_width("purl", 40);
        $dg->set_col_width("parent_id", 40);

        $dg->set_col_hidden("key", false);

        $dg->enable_pagecount(true);

        $dg->set_col_property('purl',
            array(
                "formatter" => "###urlformatter###",
            )
        );

        $dg->set_col_property("person_points",
            array("sorttype" => "integer")
        );

        $dg->set_col_property("level_from_treerootid",
            array("sorttype" => "integer")
        );

        $dg->set_col_property("depth_credit",
            array("sorttype" => "integer")
        );

        $dg->set_col_property("trubox_points",
            array("sorttype" => "integer")
        );

        $dg->set_col_property("PODS_Level",
            array("sorttype" => "integer")
        );

        $dg->set_col_property("frontline",
            array("sorttype" => "integer")
        );

        $dg->set_col_property("next_level_points",
            array("sorttype" => "integer")
        );

        $dg->set_col_property("max_match_points",
            array("sorttype" => "integer")
        );

        $dg->set_col_property("unearned_points",
            array("sorttype" => "integer")
        );

        $dg->set_col_property("truwallet",
            array("sorttype" => "integer")
        );

        $dg->set_col_property("tgc_balance",
            array("sorttype" => "integer")
        );

        /*$_dir = Mage::getBaseUrl() . 'phpGrid' . DS . 'export.php';
        $exportDropdown = <<< EXPORTDROPDOWN
        $('#connectionList_pager1_left').append ('<div style=padding-right: 16px;>Export:\
            <select onchange=document.location.href=this.options[this.selectedIndex].value;>\
                <option>---</option>\
                <option value=$_dir?gn=connectionList&export_type=excel>Excel</option>\
                <option value=$_dir?gn=connectionList&export_type=csv>CSV</option>\
            </select></div>');
EXPORTDROPDOWN;

        $dg->before_script_end = $exportDropdown;
        $dg->enable_export('PDF');*/

        $dg->set_caption("Connection");

        $dg->enable_search(true);
        $dg->enable_resize(false);

        $dg->enable_kb_nav(true);

        $dg->set_col_edittype('person_title', 'select', $this->getPersonTitle());
        $dg->set_col_edittype('level_from_treerootid', 'select', $this->getLevel());
        $dg->set_col_edittype('depth_credit', 'select', $this->getDeptCredit());
        $dg->set_col_edittype('PODS_Level', 'select', $this->getPodsLevel());

        return $dg;
    }

    public function getPersonTitle()
    {
        /*$sql_title = 'SELECT DISTINCT Title FROM `tr_People` ORDER BY `tr_People`.`Title` ASC';
        $title = '';
         $titles = $this->_coreRead->fetchCol($sql_title);
        if ($titles != null && is_array($titles)) {
            $i = 0;
            $count = sizeof($titles);
            foreach ($titles as $tit) {
                if ($i == $count - 1)
                    $title .= $tit . ':' . $tit;
                else
                    $title .= $tit . ':' . $tit . ';';
                $i++;
            }

        }*/

        $title = 'Affiliate:Affiliate;Customer:Customer;Influencer:Influencer;PaceSetter:PaceSetter;Partner:Partner;Promoter:Promoter';

        return $title;
    }

    public function getLevel()
    {
        /*$sql_level = 'SELECT DISTINCT Level FROM `tr_Genealogy`';
        $levels = $this->_coreRead->fetchCol($sql_level);
        $level = '0:0;1:1;2:2;3:3;4:4;5:5;6:6;7:7;8:8;9:9;10:10;11:11;12:12;13:13;14:14;15:15;16:16;17:17;18:18;19:19;20:20;21:21;22:22;23:23;24:24;25:25';
        if ($levels != null && is_array($levels)) {
            $i = 0;
            $count = sizeof($levels);
            foreach ($levels as $tit) {
                if ($i == $count - 1)
                    $level .= $tit . ':' . $tit;
                else
                    $level .= $tit . ':' . $tit . ';';
                $i++;
            }

        }*/

        $level = '0:0;1:1;2:2;3:3;4:4;5:5;6:6;7:7;8:8;9:9;10:10;11:11;12:12;13:13;14:14;15:15;16:16;17:17;18:18;19:19;20:20;21:21;22:22;23:23;24:24;25:25';

        return $level;
    }

    public function getDeptCredit()
    {
        /*$sql_dept = 'SELECT DISTINCT DepthCredit FROM `tr_People` ORDER BY `tr_People`.`DepthCredit` ASC';
// $depts = $readConnection->fetchCol($sql_dept);
        $dept = '0:0;1:1;2:2;3:3;4:4;5:5;6:6;7:7;9:9;10:10';
        if ($depts != null && is_array($depts)) {
            $i = 0;
            $count = sizeof($depts);
            foreach ($depts as $tit) {
                if ($i == $count - 1)
                    $dept .= $tit . ':' . $tit;
                else
                    $dept .= $tit . ':' . $tit . ';';
                $i++;
            }

        }*/

        $dept = '0:0;1:1;2:2;3:3;4:4;5:5;6:6;7:7;9:9;10:10';
        return $dept;
    }

    public function getPodsLevel()
    {
        /*$sql_pos = 'SELECT DISTINCT Level - 1 as Level FROM tr_Genealogy WHERE TreeRootID = 2401 AND Level >= 1 ORDER BY Level ASC';
        $pods = $this->_coreRead->fetchCol($sql_pos);
        $pod = '0:0;1:1;2:2;3:3;4:4;5:5;6:6;7:7;8:8;9:9;10:10;11:11;12:12;13:13;14:14;15:15;16:16;17:17;18:18;19:19;20:20;21:21;22:22;23:23;24:24';
        if ($pods != null && is_array($pods)) {
            $i = 0;
            $count = sizeof($pods);
            foreach ($pods as $tit) {
                if ($i == $count - 1)
                    $pod .= $tit . ':' . $tit;
                else
                    $pod .= $tit . ':' . $tit . ';';
                $i++;
            }

        }*/

        $pod = '0:0;1:1;2:2;3:3;4:4;5:5;6:6;7:7;8:8;9:9;10:10;11:11;12:12;13:13;14:14;15:15;16:16;17:17;18:18;19:19;20:20;21:21;22:22;23:23;24:24';

        return $pod;
    }
}
