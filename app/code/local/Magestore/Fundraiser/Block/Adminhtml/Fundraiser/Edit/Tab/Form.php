<?php

class Magestore_Fundraiser_Block_Adminhtml_Fundraiser_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
	protected function _prepareForm(){
		$form = new Varien_Data_Form();
		$this->setForm($form);
		
		if (Mage::getSingleton('adminhtml/session')->getFundraiserData()){
			$data = Mage::getSingleton('adminhtml/session')->getFundraiserData();
			Mage::getSingleton('adminhtml/session')->setFundraiserData(null);
		}elseif(Mage::registry('fundraiser_data'))
			$data = Mage::registry('fundraiser_data')->getData();
		
		$fieldset = $form->addFieldset('fundraiser_form', array('legend'=>Mage::helper('fundraiser')->__('Item information')));

		$fieldset->addField('title', 'text', array(
			'label'		=> Mage::helper('fundraiser')->__('Title'),
			'class'		=> 'required-entry',
			'required'	=> true,
			'name'		=> 'title',
		));

		$fieldset->addField('filename', 'file', array(
			'label'		=> Mage::helper('fundraiser')->__('File'),
			'required'	=> false,
			'name'		=> 'filename',
		));

		$fieldset->addField('status', 'select', array(
			'label'		=> Mage::helper('fundraiser')->__('Status'),
			'name'		=> 'status',
			'values'	=> Mage::getSingleton('fundraiser/status')->getOptionHash(),
		));

		$fieldset->addField('content', 'editor', array(
			'name'		=> 'content',
			'label'		=> Mage::helper('fundraiser')->__('Content'),
			'title'		=> Mage::helper('fundraiser')->__('Content'),
			'style'		=> 'width:700px; height:500px;',
			'wysiwyg'	=> false,
			'required'	=> true,
		));

		$form->setValues($data);
		return parent::_prepareForm();
	}
}