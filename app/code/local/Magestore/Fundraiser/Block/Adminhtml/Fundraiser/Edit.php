<?php

class Magestore_Fundraiser_Block_Adminhtml_Fundraiser_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
	public function __construct(){
		parent::__construct();
		
		$this->_objectId = 'id';
		$this->_blockGroup = 'fundraiser';
		$this->_controller = 'adminhtml_fundraiser';
		
		$this->_updateButton('save', 'label', Mage::helper('fundraiser')->__('Save Item'));
		$this->_updateButton('delete', 'label', Mage::helper('fundraiser')->__('Delete Item'));
		
		$this->_addButton('saveandcontinue', array(
			'label'		=> Mage::helper('adminhtml')->__('Save And Continue Edit'),
			'onclick'	=> 'saveAndContinueEdit()',
			'class'		=> 'save',
		), -100);

		$this->_formScripts[] = "
			function toggleEditor() {
				if (tinyMCE.getInstanceById('fundraiser_content') == null)
					tinyMCE.execCommand('mceAddControl', false, 'fundraiser_content');
				else
					tinyMCE.execCommand('mceRemoveControl', false, 'fundraiser_content');
			}

			function saveAndContinueEdit(){
				editForm.submit($('edit_form').action+'back/edit/');
			}
		";
	}

	public function getHeaderText(){
		if(Mage::registry('fundraiser_data') && Mage::registry('fundraiser_data')->getId())
			return Mage::helper('fundraiser')->__("Edit Item '%s'", $this->htmlEscape(Mage::registry('fundraiser_data')->getTitle()));
		return Mage::helper('fundraiser')->__('Add Item');
	}
}