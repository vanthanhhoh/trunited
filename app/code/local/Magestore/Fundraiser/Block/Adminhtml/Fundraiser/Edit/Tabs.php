<?php

class Magestore_Fundraiser_Block_Adminhtml_Fundraiser_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
	public function __construct(){
		parent::__construct();
		$this->setId('fundraiser_tabs');
		$this->setDestElementId('edit_form');
		$this->setTitle(Mage::helper('fundraiser')->__('Item Information'));
	}

	protected function _beforeToHtml(){
		$this->addTab('form_section', array(
			'label'	 => Mage::helper('fundraiser')->__('Item Information'),
			'title'	 => Mage::helper('fundraiser')->__('Item Information'),
			'content'	 => $this->getLayout()->createBlock('fundraiser/adminhtml_fundraiser_edit_tab_form')->toHtml(),
		));
		return parent::_beforeToHtml();
	}
}