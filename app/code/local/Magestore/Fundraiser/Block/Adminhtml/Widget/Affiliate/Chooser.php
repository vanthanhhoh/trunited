<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Adminhtml
 * @copyright  Copyright (c) 2006-2016 X.commerce, Inc. and affiliates (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * CMS block chooser for Wysiwyg CMS widget
 *
 * @category   Mage
 * @package    Mage_Adminhtml
 * @author     Magento Core Team <core@magentocommerce.com>
 */
class Magestore_Fundraiser_Block_Adminhtml_Widget_Affiliate_Chooser extends Mage_Adminhtml_Block_Widget_Grid
{
    /**
     * Block construction, prepare grid params
     *
     * @param array $arguments Object data
     */
    public function __construct($arguments=array())
    {
        parent::__construct($arguments);
        $this->setDefaultSort('account_id');
        $this->setDefaultDir('ASC');
        $this->setUseAjax(true);
        $this->setDefaultFilter(array('status' => '1'));
    }

    /**
     * Prepare chooser element HTML
     *
     * @param Varien_Data_Form_Element_Abstract $element Form Element
     * @return Varien_Data_Form_Element_Abstract
     */
    public function prepareElementHtml(Varien_Data_Form_Element_Abstract $element)
    {
        $uniqId = Mage::helper('core')->uniqHash($element->getId());

        $sourceUrl = $this->getUrl('*/cms/choose', array('uniq_id' => $uniqId));

        $chooser = $this->getLayout()->createBlock('widget/adminhtml_widget_chooser')
            ->setElement($element)
            ->setTranslationHelper($this->getTranslationHelper())
            ->setConfig($this->getConfig())
            ->setFieldsetId($this->getFieldsetId())
            ->setSourceUrl($sourceUrl)
            ->setUniqId($uniqId);


        if ($element->getValue()) {
            $block = Mage::getModel('affiliateplus/account')->load($element->getValue());
            if ($block->getId()) {
                $chooser->setLabel($block->getName());
            }
        }

        $element->setData('after_element_html', $chooser->toHtml());
        return $element;
    }

    /**
     * Grid Row JS Callback
     *
     * @return string
     */
    public function getRowClickCallback()
    {
        $chooserJsObject = $this->getId();
        $js = '
            function (grid, event) {
                var trElement = Event.findElement(event, "tr");
                var blockId = trElement.down("td").innerHTML.replace(/^\s+|\s+$/g,"");
                var blockTitle = trElement.down("td").next().innerHTML;
                '.$chooserJsObject.'.setElementValue(blockId);
                '.$chooserJsObject.'.setElementLabel(blockTitle);
                '.$chooserJsObject.'.close();
            }
        ';
        return $js;
    }

    /**
     * Prepare Cms static blocks collection
     *
     * @return Mage_Adminhtml_Block_Widget_Grid
     */
    protected function _prepareCollection()
    {
        $collection = Mage::getModel('affiliateplus/account')->getCollection();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    /**
     * Prepare columns for Cms blocks grid
     *
     * @return Mage_Adminhtml_Block_Widget_Grid
     */
    protected function _prepareColumns()
    {
        $currencyCode = Mage::app()->getStore()->getBaseCurrency()->getCode();
        $this->addColumn('account_id', array(
            'header'    => Mage::helper('cms')->__('ID'),
            'align'     => 'right',
            'index'     => 'account_id',
            'width'     => 50
        ));

        $this->addColumn('name', array(
            'header'    => Mage::helper('cms')->__('Name'),
            'align'     => 'left',
            'index'     => 'name',
        ));

        $this->addColumn('fundraiser_name', array(
            'header'    => Mage::helper('affiliateplus')->__('Fundraiser Name'),
            'align'     =>'left',
            'index'     => 'fundraiser_name',
            'filter_index'	=> 'fundraiser_name'
        ));

        $this->addColumn('email', array(
            'header'    => Mage::helper('affiliateplus')->__('Email Address'),
            'index'     => 'email',
            'filter_index'	=> 'email'
        ));

        $this->addColumn('balance', array(
            'header'    => Mage::helper('affiliateplus')->__('Balance'),
            'width'     => '100px',
            'align'     =>'right',
            'index'     => 'balance',
            'type'		=> 'price',
            'currency_code' => $currencyCode,
            'filter_index'	=> 'main_table.balance'
        ));

        $this->addColumn('status', array(
            'header'    => Mage::helper('affiliateplus')->__('Status'),
            'align'     => 'left',
            'width'     => '80px',
            'index'     => 'status',
            'filter_index'	=> 'main_table.status',
            'type'      => 'options',
            'options'   => array(
                1 => 'Enabled',
                2 => 'Disabled',
            ),

        ));

        $this->addColumn('approved', array(
            'header'    => Mage::helper('affiliateplus')->__('Approved'),
            'align'     => 'left',
            'width'     => '80px',
            'index'     => 'approved',
            'filter_index'	=> 'main_table.approved',
            'type'      => 'options',
            'options'   => array(
                1 => 'Yes',
                2 => 'No',
            ),
        ));

        return parent::_prepareColumns();
    }

    public function getGridUrl()
    {
        return $this->getUrl('*/cms/choose', array('_current' => true));
    }
}
