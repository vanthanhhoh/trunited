<?php
/**
 * Magestore
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magestore.com license that is
 * available through the world-wide-web at this URL:
 * http://www.magestore.com/license-agreement.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Magestore
 * @package     Magestore_fundraiser
 * @copyright   Copyright (c) 2012 Magestore (http://www.magestore.com/)
 * @license     http://www.magestore.com/license-agreement.html
 */

/**
 * fundraiser Tab on Customer Edit Form Block
 *
 * @category    Magestore
 * @package     Magestore_fundraiser
 * @author      Magestore Developer
 */
class Magestore_Fundraiser_Block_Adminhtml_Customer_Edit_Tab_Fundraiser
    extends Mage_Adminhtml_Block_Widget_Form
    implements Mage_Adminhtml_Block_Widget_Tab_Interface
{
    protected $_fundraiserAccount = null;

    /**
     * @return Mage_Core_Model_Abstract|null
     * @throws Exception
     */
    public function getFundraiserAccount()
    {
        if (is_null($this->_fundraiserAccount)) {
            $customerId = $this->getRequest()->getParam('id');
            $this->_fundraiserAccount = Mage::getModel('fundraiser/customer')
                ->load($customerId, 'customer_id');
        }
        return $this->_fundraiserAccount;
    }

    public function getFundraiserPoint()
    {
        return Mage::helper('core')->currency(
            $this->getFundraiserAccount()->getFundraiserPoint(),
            true,
            false
        );
    }

    /**
     * @return Mage_Adminhtml_Block_Widget_Form
     */
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form();
        $form->setHtmlIdPrefix('fundraiser_');
        $this->setForm($form);

        $fieldset = $form->addFieldset('fundraiser_form', array(
            'legend' => Mage::helper('fundraiser')->__('Affiliate Fundraiser Information')
        ));

        $fieldset->addField('fundraiser_balance', 'note', array(
            'label' => Mage::helper('fundraiser')->__('Fundraiser Points'),
            'title' => Mage::helper('fundraiser')->__('Fundraiser Points'),
            'text' => '<strong>' . $this->getFundraiserPoint() . '</strong>',
        ));

        $fieldset->addField('fundraiser_point', 'text', array(
            'label' => Mage::helper('fundraiser')->__('Change Fundraiser Points'),
            'title' => Mage::helper('fundraiser')->__('Change Fundraiser Points'),
            'name' => 'Fundraiser[fundraiser_point]',
            'class' => 'validate-number',
            'note' => Mage::helper('fundraiser')->__('Add or subtract customer\'s fundraiser points. For ex: 99 or -99 fundraiser points.'),
        ));

        $fieldset->addField('title_point', 'textarea', array(
            'label' => Mage::helper('fundraiser')->__('Change Transaction Title'),
            'title' => Mage::helper('fundraiser')->__('Change Transaction Title'),
            'name' => 'Fundraiser[title]',
            'style' => 'height: 5em;'
        ));

        $fieldset = $form->addFieldset('fundraiser_history_fieldset', array(
            'legend' => Mage::helper('fundraiser')->__('Transaction History')
        ))->setRenderer($this->getLayout()->createBlock('adminhtml/widget_form_renderer_fieldset')->setTemplate(
            'fundraiser/history.phtml'
        ));

        return parent::_prepareForm();
    }

    public function getTabLabel()
    {
        return Mage::helper('fundraiser')->__('Affiliate Fundraiser');
    }

    public function getTabTitle()
    {
        return Mage::helper('fundraiser')->__('Affiliate Fundraiser');
    }

    public function canShowTab()
    {
        return true;
    }

    public function isHidden()
    {
        return false;
    }
}
