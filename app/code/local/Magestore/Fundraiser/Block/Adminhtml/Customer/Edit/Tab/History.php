<?php
/**
 * Magestore
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magestore.com license that is
 * available through the world-wide-web at this URL:
 * http://www.magestore.com/license-agreement.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Magestore
 * @package     Magestore_fundraiser
 * @copyright   Copyright (c) 2012 Magestore (http://www.magestore.com/)
 * @license     http://www.magestore.com/license-agreement.html
 */

/**
 * fundraiser Tab on Customer Edit Form Block
 *
 * @category    Magestore
 * @package     Magestore_fundraiser
 * @author      Magestore Developer
 */
class Magestore_Fundraiser_Block_Adminhtml_Customer_Edit_Tab_History
    extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('fundraiserTransactionGrid');
        $this->setDefaultSort('transaction_id');
        $this->setDefaultDir('DESC');
        $this->setUseAjax(true);
    }

    /**
     * prepare collection for block to display
     *
     * @return Magestore_fundraiser_Block_Adminhtml_Customer_Edit_Tab_History
     */
    protected function _prepareCollection()
    {
        $collection = Mage::getSingleton('fundraiser/transaction')->getCollection()
            ->addFieldToFilter('customer_id', $this->getRequest()->getParam('id'))
            ->setOrder('transaction_id', 'desc');
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    /**
     * prepare columns for this grid
     *
     * @return Magestore_fundraiser_Block_Adminhtml_Customer_Edit_Tab_History
     */
    protected function _prepareColumns()
    {
        $this->addColumn('transaction_id', array(
            'header' => Mage::helper('fundraiser')->__('ID'),
            'align' => 'right',
            'width' => '50px',
            'index' => 'transaction_id',
            'type' => 'number',
        ));

        $this->addColumn('title', array(
            'header' => Mage::helper('fundraiser')->__('Title'),
            'align' => 'left',
            'index' => 'title',
        ));

        $this->addColumn('action_type', array(
            'header' => Mage::helper('fundraiser')->__('Action'),
            'align' => 'left',
            'index' => 'action_type',
            'type' => 'options',
            'options' => Mage::getSingleton('fundraiser/type')->getOptionArray(),
        ));

        $this->addColumn('current_point', array(
            'header' => Mage::helper('fundraiser')->__('Current Points'),
            'align' => 'right',
            'index' => 'current_point',
            'type' => 'number',
        ));

        $this->addColumn('changed_point', array(
            'header' => Mage::helper('fundraiser')->__('Updated Points'),
            'align' => 'right',
            'index' => 'changed_point',
            'type' => 'number',
        ));

        $this->addColumn('created_at', array(
            'header' => Mage::helper('fundraiser')->__('Created On'),
            'index' => 'created_at',
            'type' => 'datetime',
        ));

        $this->addColumn('updated_at', array(
            'header' => Mage::helper('fundraiser')->__('Updated On'),
            'index' => 'updated_at',
            'type' => 'datetime',
        ));

        $this->addColumn('expiration_date', array(
            'header' => Mage::helper('fundraiser')->__('Expires On'),
            'index' => 'expiration_date',
            'type' => 'datetime',
        ));

        $this->addColumn('receiver_email', array(
            'header' => Mage::helper('fundraiser')->__('Receiver Email'),
            'index' => 'receiver_email',
        ));

        $this->addColumn('status', array(
            'header' => Mage::helper('fundraiser')->__('Status'),
            'align' => 'left',
            'index' => 'status',
            'type' => 'options',
            'options' => Mage::getSingleton('fundraiser/status')->getTransactionOptionArray(),
        ));

        $this->addColumn('store_id', array(
            'header' => Mage::helper('fundraiser')->__('Store View'),
            'align' => 'left',
            'index' => 'store_id',
            'type' => 'options',
            'options' => Mage::getModel('adminhtml/system_store')->getStoreOptionHash(true),
        ));

        $this->addExportType('adminhtml/reward_customer/exportCsv'
            , Mage::helper('fundraiser')->__('CSV')
        );
        $this->addExportType('adminhtml/reward_customer/exportXml'
            , Mage::helper('fundraiser')->__('XML')
        );
        return parent::_prepareColumns();
    }

    /**
     * Add column to grid
     *
     * @param   string $columnId
     * @param   array || Varien_Object $column
     * @return  Magestore_fundraiser_Block_Adminhtml_Customer_Edit_Tab_History
     */
    public function addColumn($columnId, $column)
    {
        $columnId = 'fundraiser_transaction_' . $columnId;
        return parent::addColumn($columnId, $column);
    }

    /**
     * get url for each row in grid
     *
     * @return string
     */
    public function getRowUrl($row)
    {
        return Mage::helper('adminhtml')->getUrl('fundraiseradmin/adminhtml_transaction/edit', array('id' => $row->getId()));
    }

    /**
     * get grid url (use for ajax load)
     *
     * @return string
     */
    public function getGridUrl()
    {
        return Mage::helper('adminhtml')->getUrl('fundraiseradmin/adminhtml_customer/grid', array('_current' => true));
    }
}
