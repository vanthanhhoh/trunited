<?php

class Magestore_Fundraiser_Block_Adminhtml_Fundraiser extends Mage_Adminhtml_Block_Widget_Grid_Container
{
	public function __construct(){
		$this->_controller = 'adminhtml_fundraiser';
		$this->_blockGroup = 'fundraiser';
		$this->_headerText = Mage::helper('fundraiser')->__('Item Manager');
		$this->_addButtonLabel = Mage::helper('fundraiser')->__('Add Item');
		parent::__construct();
	}
}