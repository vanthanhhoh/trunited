<?php

class Magestore_Fundraiser_Block_Adminhtml_Cms extends Mage_Adminhtml_Block_Widget_Grid_Container
{
	public function __construct(){
		$this->_controller = 'adminhtml_cms';
		$this->_blockGroup = 'fundraiser';
		$this->_headerText = Mage::helper('fundraiser')->__('CMS Pages Manager');
		$this->_addButtonLabel = Mage::helper('fundraiser')->__('Add CMS Page');
		parent::__construct();
	}
}