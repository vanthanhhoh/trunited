<?php

/**
 * Created by PhpStorm.
 * User: anthony
 * Date: 2/14/17
 * Time: 10:40 AM
 */
class Magestore_Fundraiser_Block_Adminhtml_Renderer_Affiliate_Name extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {
        $title = $row->getData($this->getColumn()->getIndex());
        $url = Mage::helper("adminhtml")->getUrl("adminhtml/affiliateplus_account/edit", array('id'=>$row->getData('affiliate_id')));
        return "<a href='$url' target='_blank'>$title</a>";
    }

}

?>