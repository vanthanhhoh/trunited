<?php

/**
 * Created by PhpStorm.
 * User: anthony
 * Date: 2/14/17
 * Time: 10:40 AM
 */
class Magestore_Fundraiser_Block_Adminhtml_Renderer_Cms_Preview extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {
        $title = $row->getData($this->getColumn()->getIndex());

        $href = Mage::getUrl($row->getData('identifier'));

        return "<a href='$href' target='_blank'>Preview</a>";
    }

}

?>