<?php

/**
 * Created by PhpStorm.
 * User: anthony
 * Date: 2/14/17
 * Time: 10:40 AM
 */
class Magestore_Fundraiser_Block_Adminhtml_Renderer_Cms_Title extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {
        $title = $row->getData($this->getColumn()->getIndex());
        $url = Mage::helper("adminhtml")->getUrl("adminhtml/cms_page/edit", array('page_id'=>$row->getData('cms_page_id')));
        return "<a href='$url' target='_blank'>$title</a>";
    }

}

?>