<?php

class Magestore_Fundraiser_Block_Adminhtml_Cms_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
	public function __construct(){
		parent::__construct();
		$this->setId('cmsGrid');
		$this->setDefaultSort('cms_id');
		$this->setDefaultDir('ASC');
		$this->setSaveParametersInSession(true);
	}

	protected function _prepareCollection(){
		$customerTableName = Mage::getSingleton('core/resource')->getTableName('customer_entity');
        $customerVarcharTableName = Mage::getSingleton('core/resource')->getTableName('customer_entity_varchar');
        $cmsPageTableName = Mage::getSingleton('core/resource')->getTableName('cms/page');
        $affiliateAccountTableName = Mage::getSingleton('core/resource')->getTableName('affiliateplus/account');

		$collection = Mage::getModel('fundraiser/cms')->getCollection();

		$collection->getSelect()
			->join(array('cm' => $cmsPageTableName), 'cm.page_id=main_table.cms_page_id', array('cms_title' => 'title', 'status' => 'is_active','identifier' => 'identifier'));

        $collection->getSelect()
            ->join(array('ce1' => $customerTableName), 'ce1.entity_id=main_table.customer_id', array('customer_email' => 'email'));

        $fn = Mage::getModel('eav/entity_attribute')->loadByCode('1', 'firstname');
        $ln = Mage::getModel('eav/entity_attribute')->loadByCode('1', 'lastname');
        $collection->getSelect()
            ->join(array('ce2' => $customerVarcharTableName), 'ce2.entity_id=main_table.customer_id', array('firstname' => 'value'))
            ->where('ce2.attribute_id=' . $fn->getAttributeId())
            ->join(array('ce3' => $customerVarcharTableName), 'ce3.entity_id=main_table.customer_id', array('lastname' => 'value'))
            ->where('ce3.attribute_id=' . $ln->getAttributeId())
            ->columns(new Zend_Db_Expr("CONCAT(`ce2`.`value`, ' ',`ce3`.`value`) AS customer_name"));


		$this->setCollection($collection);
		return parent::_prepareCollection();
	}

	protected function _prepareColumns(){
		$this->addColumn('cms_id', array(
			'header'	=> Mage::helper('fundraiser')->__('ID'),
			'align'	 =>'right',
			'width'	 => '50px',
			'index'	 => 'cms_id',
		));

        $this->addColumn('cms_page_id', array(
            'header'	=> Mage::helper('fundraiser')->__('CMS Page ID'),
            'align'	 =>'left',
            'width'	 => '50px',
            'index'	 => 'cms_page_id',
        ));

        $this->addColumn('cms_title', array(
            'header'	=> Mage::helper('fundraiser')->__('CMS Title'),
            'align'	 =>'left',
            'index'	 => 'cms_title',
            'renderer' => 'Magestore_Fundraiser_Block_Adminhtml_Renderer_Cms_Title',
            'filter_index'  => 'cm.title',
        ));

		$this->addColumn('customer_id', array(
            'header' => Mage::helper('fundraiser')->__('Customer ID'),
            'width' => '50px',
            'index' => 'customer_id',
        ));

        $this->addColumn('customer_name', array(
            'header' => Mage::helper('fundraiser')->__('Customer Name'),
            'index' => 'customer_name',
            'renderer' => 'Magestore_Fundraiser_Block_Adminhtml_Renderer_Customer_Name',
            'filter_name' => 'customer_name',
            'filter_condition_callback' => array($this, '_filterCustomerNameCallback')
        ));

        $this->addColumn('customer_email', array(
            'header' => Mage::helper('fundraiser')->__('Customer Email'),
            'index' => 'customer_email',
            'type' => 'text',
            'renderer' => 'Magestore_Fundraiser_Block_Adminhtml_Renderer_Customer_Email',
            'filter_index'  => 'ce1.email',
        ));

        $this->addColumn('affiliate_id', array(
            'header'	=> Mage::helper('fundraiser')->__('Affiliate ID'),
            'align'	 =>'left',
            'width'	 => '50px',
            'index'	 => 'affiliate_id',
            'renderer' => 'Magestore_Fundraiser_Block_Adminhtml_Renderer_Affiliate_Name',
        ));

		$this->addColumn('status', array(
			'header'	=> Mage::helper('fundraiser')->__('Status'),
			'align'	 => 'left',
			'width'	 => '80px',
			'index'	 => 'status',
			'filter_index'  => 'cm.is_active',
			'type'		=> 'options',
			'options'	 => Magestore_Fundraiser_Model_Status::getOptionArray(),
		));

		$this->addColumn('created_by', array(
			'header'	=> Mage::helper('fundraiser')->__('Created By'),
			'align'	 => 'left',
			'width'	 => '80px',
			'index'	 => 'created_by',
			'type'		=> 'options',
			'options'	 => Magestore_Fundraiser_Model_Status::getCreatedByArray(),
		));

		$this->addColumn('created_at', array(
            'header' => Mage::helper('manageapi')->__('Created At'),
            'align' => 'left',
            'index' => 'created_at',
            'type' => 'date'
        ));


		$this->addColumn('updated_at', array(
            'header' => Mage::helper('manageapi')->__('Created At'),
            'align' => 'left',
            'index' => 'updated_at',
            'type' => 'date'
        ));


		$this->addColumn('preview', array(
			'header'	=> Mage::helper('fundraiser')->__('Preview'),
			'align'	 => 'left',
			'width'	 => '50px',
			'filter'	=> false,
			'sortable'	=> false,
			'is_system'	=> true,
			'renderer' => 'Magestore_Fundraiser_Block_Adminhtml_Renderer_Cms_Preview',
		));


		$this->addColumn('action',
			array(
				'header'	=>	Mage::helper('fundraiser')->__('Action'),
				'width'		=> '100',
				'type'		=> 'action',
				'getter'	=> 'getId',
				'actions'	=> array(
					array(
						'caption'	=> Mage::helper('fundraiser')->__('Edit'),
						'url'		=> array('base'=> '*/*/edit'),
						'field'		=> 'id'
					)),
				'filter'	=> false,
				'sortable'	=> false,
				'index'		=> 'stores',
				'is_system'	=> true,
		));

		$this->addExportType('*/*/exportCsv', Mage::helper('fundraiser')->__('CSV'));
		$this->addExportType('*/*/exportXml', Mage::helper('fundraiser')->__('XML'));

		return parent::_prepareColumns();
	}

	protected function _prepareMassaction(){
		$this->setMassactionIdField('cms_id');
		$this->getMassactionBlock()->setFormFieldName('cms');

		$this->getMassactionBlock()->addItem('delete', array(
			'label'		=> Mage::helper('fundraiser')->__('Delete'),
			'url'		=> $this->getUrl('*/*/massDelete'),
			'confirm'	=> Mage::helper('fundraiser')->__('Are you sure?')
		));

		// $statuses = Mage::getSingleton('fundraiser/status')->getOptionArray();

		// array_unshift($statuses, array('label'=>'', 'value'=>''));
		// $this->getMassactionBlock()->addItem('status', array(
		// 	'label'=> Mage::helper('fundraiser')->__('Change status'),
		// 	'url'	=> $this->getUrl('*/*/massStatus', array('_current'=>true)),
		// 	'additional' => array(
		// 		'visibility' => array(
		// 			'name'	=> 'status',
		// 			'type'	=> 'select',
		// 			'class'	=> 'required-entry',
		// 			'label'	=> Mage::helper('fundraiser')->__('Status'),
		// 			'values'=> $statuses
		// 		))
		// ));
		return $this;
	}

	public function getRowUrl($row){
		return $this->getUrl('*/*/edit', array('id' => $row->getId()));
	}

	protected function _filterCustomerNameCallback($collection, $column)
    {
        if (!$value = $column->getFilter()->getValue()) {
            return $this;
        }

        $dir = $this->getParam($this->getVarNameDir(), $this->_defaultDir);

        if (!empty($value)) {
            $_customers = Mage::getModel('customer/customer')->getCollection()
                ->addAttributeToSelect('entity_id')
                ->addAttributeToSelect('firstname')
                ->addAttributeToFilter('firstname', array('like' => '%' . $value . '%'))
                ->setOrder('firstname', $dir);

            $rs_firstname = $_customers->getColumnValues('entity_id');

            $_customers_ = Mage::getModel('customer/customer')->getCollection()
                ->addAttributeToSelect('entity_id')
                ->addAttributeToSelect('lastname')
                ->addAttributeToFilter('lastname', array('like' => '%' . $value . '%'))
                ->setOrder('lastname', $dir);

            $rs_lastname = $_customers_->getColumnValues('entity_id');

            $rs = array_merge($rs_firstname, $rs_lastname);
            if (sizeof($rs) == 0)
                $collection->getSelect()->where('main_table.customer_id IS NULL');
            else
                $collection->getSelect()->where('main_table.customer_id IN (' . implode(',', $rs) . ')');
        }


        return $this;
    }
}