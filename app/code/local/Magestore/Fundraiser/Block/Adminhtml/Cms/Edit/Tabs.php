<?php

class Magestore_Fundraiser_Block_Adminhtml_Cms_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
	public function __construct(){
		parent::__construct();
		$this->setId('cms_tabs');
		$this->setDestElementId('edit_form');
		$this->setTitle(Mage::helper('fundraiser')->__('CMS Page Information'));
	}

	protected function _beforeToHtml(){
		$this->addTab('form_section', array(
			'label'	 => Mage::helper('fundraiser')->__('CMS Page Information'),
			'title'	 => Mage::helper('fundraiser')->__('CMS Page Information'),
			'content'	 => $this->getLayout()->createBlock('fundraiser/adminhtml_cms_edit_tab_form')->toHtml(),
		));
		return parent::_beforeToHtml();
	}
}