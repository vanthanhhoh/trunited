<?php

class Magestore_Fundraiser_Block_Adminhtml_Cms_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        parent::__construct();

        $this->_objectId = 'id';
        $this->_blockGroup = 'fundraiser';
        $this->_controller = 'adminhtml_cms';

        $this->_updateButton('save', 'label', Mage::helper('fundraiser')->__('Save CMS Page'));
        $this->_updateButton('delete', 'label', Mage::helper('fundraiser')->__('Delete CMS Page'));

        $this->_addButton('saveandcontinue', array(
            'label' => Mage::helper('adminhtml')->__('Save And Continue Edit'),
            'onclick' => 'saveAndContinueEdit()',
            'class' => 'save',
        ), -100);

        $this->_formScripts[] = "
			function toggleEditor() {
				if (tinyMCE.getInstanceById('fundraiser_content') == null)
					tinyMCE.execCommand('mceAddControl', false, 'fundraiser_content');
				else
					tinyMCE.execCommand('mceRemoveControl', false, 'fundraiser_content');
			}

			function saveAndContinueEdit(){
				editForm.submit($('edit_form').action+'back/edit/');
			}
		";
    }

    protected function _prepareLayout()
    {
        if (Mage::getSingleton('cms/wysiwyg_config')->isEnabled()) {
            $this->getLayout()->getBlock('head')->setCanLoadTinyMce(true);
            $this->getLayout()->getBlock('head')->setCanLoadExtJs(true);
            if ($head = $this->getLayout()->getBlock('head')) {
                $head->addItem('js', 'prototype/window.js')
                    ->addItem('js_css', 'prototype/windows/themes/default.css')
                    ->addCss('lib/prototype/windows/themes/magento.css')
                    ->addItem('js', 'mage/adminhtml/variables.js');
            }
            return parent::_prepareLayout();
        }
    }

    public function getHeaderText()
    {
        if (Mage::registry('cms_data') && Mage::registry('cms_data')->getId())
            return Mage::helper('fundraiser')->__("Edit CMS Page '%s'", $this->htmlEscape(Mage::registry('cms_data')->getTitle()));
        return Mage::helper('fundraiser')->__('Add CMS Page');
    }
}