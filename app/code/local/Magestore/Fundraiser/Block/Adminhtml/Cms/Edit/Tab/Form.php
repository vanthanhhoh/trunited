<?php

class Magestore_Fundraiser_Block_Adminhtml_Cms_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
    const XML_PATH_AFFILIATE_CHOOSER = 'fundraiser/adminhtml_widget_affiliate_chooser';

    protected function _prepareForm()
    {
        $form = new Varien_Data_Form();
        $this->setForm($form);

        if (Mage::getSingleton('adminhtml/session')->getCmsData()) {
            $data = Mage::getSingleton('adminhtml/session')->getCmsData();
            Mage::getSingleton('adminhtml/session')->setCmsData(null);
        } elseif (Mage::registry('cms_data'))
            $data = Mage::registry('cms_data')->getData();

        if ($this->getRequest()->getParam('id'))
            $model = Mage::getModel('fundraiser/cms')->load($this->getRequest()->getParam('id'));
        else
            $model = Mage::getModel('fundraiser/cms');

        $wysiwygConfig = Mage::getSingleton('cms/wysiwyg_config')->getConfig(
            array('add_variables' => false, 'add_widgets' => false, 'files_browser_window_url' => $this->getBaseUrl() . 'admin/cms_wysiwyg_images/index/'));


        $fieldset = $form->addFieldset('cms_form', array('legend' => Mage::helper('fundraiser')->__('CMS Page information')));

        /** Affiliate Chooser */
        $chooserButtonHelper = Mage::helper('fundraiser/chooser');

        $affiliateChooserConfig = array(
            'input_name' => 'affiliate_id',
            'input_label' => $this->__('Choose Affiliate'),
            'button_text' => $this->__('Select Affiliate…'),
            'required' => true
        );

        $chooserButtonHelper->createAffiliateChooser(
            $model,
            $fieldset,
            $affiliateChooserConfig
        );
        /** END Affiliate Chooser */

        /*$imageConfig = array(
            'input_name' => 'background_page',
            'input_label' => $this->__('Background Image'),
            'button_text' => $this->__('Select Image...'),
            'required' => true
        );

        $chooserButtonHelper->createImageChooser(
            $model,
            $fieldset,
            $imageConfig
        );*/
        if (isset($data['background_img']) && $data['background_img']) {
            $data['background_img'] = 'cms/fundraiser/' . $data['background_img'];
        }
        $fieldset->addField('background_img', 'image', array(
            'label' => Mage::helper('fundraiser')->__('Background Image'),
            'required' => false,
            'name' => 'background_img',
            'note' => $this->__('Supported format: .jpeg, .jpg, .png'),
        ));

        $fieldset->addField('title', 'text', array(
            'label' => Mage::helper('fundraiser')->__('Page Title'),
            'class' => 'required-entry',
            'required' => true,
            'name' => 'title',
        ));

        $fieldset->addField('identifier', 'text', array(
            'label' => Mage::helper('fundraiser')->__('URL Key'),
            'class' => 'required-entry',
            'required' => true,
            'name' => 'identifier',
        ));

        $fieldset->addField('is_active', 'select', array(
            'label'     => Mage::helper('fundraiser')->__('Status'),
            'title'     => Mage::helper('fundraiser')->__('Status'),
            'name'      => 'is_active',
            'required'  => true,
            'options'   => array(
                '1' => Mage::helper('fundraiser')->__('Enabled'),
                '2' => Mage::helper('fundraiser')->__('Disabled'),
            ),
        ));

        $fieldset->addField('cms_content', 'editor', array(
            'name' => 'cms_content',
            'label' => Mage::helper('fundraiser')->__('Content'),
            'title' => Mage::helper('fundraiser')->__('Content'),
            'style' => 'width:700px; height:500px;',
            'config'    => Mage::getSingleton('cms/wysiwyg_config')->getConfig(),
            'required' => true,
            'wysiwyg' => true,
        ));

        $fieldset->addField('meta_keywords', 'textarea', array(
            'name' => 'meta_keywords',
            'label' => Mage::helper('fundraiser')->__('Meta Keywords'),
            'title' => Mage::helper('fundraiser')->__('Meta Keywords'),
        ));

        $fieldset->addField('meta_description', 'textarea', array(
            'name' => 'meta_description',
            'label' => Mage::helper('fundraiser')->__('Meta Description'),
            'title' => Mage::helper('fundraiser')->__('Meta Description'),
        ));

        $form->setValues($data);
        return parent::_prepareForm();
    }
}
