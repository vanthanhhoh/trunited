<?php

class Magestore_Fundraiser_Block_Fundraiser extends Mage_Core_Block_Template
{
    protected $_cms;
    protected $_fundraiser_cms;

    public function _prepareLayout()
    {
        return parent::_prepareLayout();
    }

    public function setCms($cms)
    {
        $this->_cms = $cms;
    }

    public function setFundraiserCms($fundraiser_cms)
    {
        $this->_fundraiser_cms = $fundraiser_cms;
    }

    public function getCms()
    {
        return $this->_cms;
    }

    public function getCustomerLogged()
    {
        return Mage::getSingleton('customer/session')->getCustomer();
    }

    public function getStatusLabel($status)
    {
        $statues = Magestore_Fundraiser_Model_Status::getOptionArray();
        return $statues[$status];
    }

    public function getAffiliateId()
    {
        $affiliate = Mage::getModel('affiliateplus/account')->load($this->getCustomerLogged()->getId(), 'customer_id');
        return $affiliate->getId();
    }

    public function getUrlPage($url)
    {
        return Mage::getUrl($url);
    }

    public function getDefaultUrlPage()
    {
        if($this->getCmsPage() != null && $this->getCmsPage()->getIdentifier() != null)
            return $this->getUrlPage($this->getCmsPage()->getIdentifier());
        else
            return '';
    }

    public function getBackgroundImage()
    {
        if($this->getCmsFundraiser() != null && $this->getCmsFundraiser()->getBackgroundImg() != null)
            return  Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA).'cms'. DS .'fundraiser' . DS . $this->_fundraiser_cms->getBackgroundImg();
        else
            return '';
    }

    public function getCmsFundraiser()
    {
        if ($this->_fundraiser_cms == null) {
            $customer_id = $this->getCustomerLogged()->getId();

            $model = Mage::getModel('fundraiser/cms')->load($customer_id, 'customer_id');
            if ($model != null && $model->getId())
                $this->setFundraiserCms($model);
        }

        return $this->_fundraiser_cms;
    }

    public function getCmsPage()
    {
        if ($this->_cms == null) {
            if ($this->_fundraiser_cms == null) {
                $this->setFundraiserCms($this->getCmsFundraiser());
                if ($this->_fundraiser_cms != null)
                    $this->_cms = Mage::getModel('cms/page')->load($this->_fundraiser_cms->getCmsPageId());
            } else {
                $this->_cms = Mage::getModel('cms/page')->load($this->_fundraiser_cms->getCmsPageId());
            }
        }

        return $this->_cms;
    }

    public function getResetContent()
    {
        if ($this->_cms == null)
            return $this->getDefaultContent();
        else {
            return $this->_cms->getCmsContent();
        }
    }

    public function getResetPageTitle()
    {
        if ($this->_cms == null)
            return $this->__('Page Title Default');
        else {
            return $this->_cms->getTitle();
        }
    }

    public function getResetUrlKey()
    {
        if ($this->_cms == null)
            return $this->__('url_key_default_' . $this->generateRandomString(5));
        else {
            return $this->_cms->getIdentifier();
        }
    }

    public function getDefaultContent()
    {
        $content = '<div class="wrapper home clearfix">
    <figure></figure>
    <section class="left_sec">
        <div class="video_sec">
        <div class="video_blk">
        <h2>Help single mom to send kids<br>
to be able to go part time at work</h2>
        <iframe id="video" src="https://www.youtube.com/embed/RlqCDzuodQA?rel=0&amp;autoplay=1"></iframe>
        <div class="media clearfix">
            <label>$6,000<span>of $10,000 Goals</span></label>
            <ul class="social">
                <li class="twitter"><a href="#"><i class="fa fa-twitter-square" aria-hidden="true"></i></a></li>
                <li class="facebk"><a href="#"><i class="fa fa-facebook-square" aria-hidden="true"></i></a></li>
                <li class="pint"><a href="#"><i class="fa fa-pinterest-square" aria-hidden="true"></i></a></li>
                <li class="linkedin"><a href="#"><i class="fa fa-linkedin-square" aria-hidden="true"></i></a></li>
                <li class="plus"><a href="#"><i class="fa fa-plus-square" aria-hidden="true"></i></a></li>
            </ul>
        </div>
        <div class="progress">
            <div class="progress-bar" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100">
            <span class="sr-only">70% Complete</span>
            </div>
        </div>
        <p>Lorem ipsum dolor sit amet, te cum fabulas utroque, homero tacimates quaerendum vis ne. Nec nobis choro essent ne, cu sea tota inciderint. Facilis volumus an his. Per te agam posse splendide, at usu quis quaeque. Pri ad oporteat volutpat intellegat, an elit dicant ius.</p>
        <a href="#" class="donate"><i class="fa fa-heart" aria-hidden="true"></i>DONATE</a>
        </div>
        <div class="thanks">
            <a href="#">No thanks, just take me to the site</a>
        </div>
        </div>
    </section>
    <div class="overlay1"></div>
</div>';
        return $this->formatContent($content);
    }

    function generateRandomString($length = 5)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return strtolower($randomString);
    }

    public function getActionUrl()
    {
        return Mage::getUrl('*/cms/save');
    }

    public function getImagePath($image_name)
    {
        return Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA) . 'cms' . DS . 'fundraiser' . DS . $image_name;
    }

    public function formatContent($content)
    {
        return preg_replace('/^\s+|\n|\r|\s+$/m', '', $content);
    }
}
