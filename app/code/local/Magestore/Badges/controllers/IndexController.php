<?php

class Magestore_Badges_IndexController extends Mage_Core_Controller_Front_Action
{
	public function indexAction(){
		$this->loadLayout();
		$this->renderLayout();
	}

	public function updateDbAction()
	{
		$setup = new Mage_Core_Model_Resource_Setup();
		$installer = $setup;
		$installer->startSetup();
		$installer->run("
            ALTER TABLE `tr_payoutperiod_stats` ADD `member_since` datetime  NULL;
            ALTER TABLE `tr_payoutperiod_stats` ADD `trubox_points` DECIMAL(12,4) NULL ;
            ALTER TABLE `tr_payoutperiod_stats` ADD `social_stock` INT NULL ;
            ALTER TABLE `tr_payoutperiod_stats` ADD `ytd_rewards` DECIMAL(12,4) NULL ;
		");
		$installer->endSetup();
		echo "success";
	}

	public function updateDb2Action()
	{
		$setup = new Mage_Core_Model_Resource_Setup();
		$installer = $setup;
		$installer->startSetup();
		$installer->run("
            ALTER TABLE `tr_payoutperiod_stats` ADD `ytd_rewards` DECIMAL(12,4) NULL ;
		");
		$installer->endSetup();
		echo "success";
	}

	public function updateDb3Action()
	{
		$setup = new Mage_Core_Model_Resource_Setup();
		$installer = $setup;
		$installer->startSetup();
		$installer->run("
            DROP TABLE IF EXISTS {$setup->getTable('badges/popup')};
			CREATE TABLE {$setup->getTable('badges/popup')} (
			  `popup_id` int(11) unsigned NOT NULL auto_increment,
			  `customer_id` int(11) unsigned NOT NULL,
			  `opened_time` datetime NULL,
			  `created_at` datetime NULL,
			  `updated_at` datetime NULL,
			  PRIMARY KEY (`popup_id`)
			) ENGINE=InnoDB DEFAULT CHARSET=utf8;
		");
		$installer->endSetup();
		echo "success";
	}

	public function updateDb4Action()
	{
		$setup = new Mage_Core_Model_Resource_Setup(NULL);
		$installer = $setup;
		$installer->startSetup();
		$installer->run("
            ALTER TABLE `tr_payoutperiod_stats` ADD `ems_score` DECIMAL(12,4) NULL;
		");
		$installer->endSetup();
		echo "success";
	}
}