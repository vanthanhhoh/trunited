<?php

class Magestore_Badges_Helper_Data extends Mage_Core_Helper_Abstract
{
	const TBL_PAYOUT_PERIOD_STATS = 'tr_payoutperiod_stats';
	const TBL_PAYOUT_PERIOD = 'tr_PayoutPeriod';

    const PATH_BADGES_ICON = 'badges/icon/';

    public function getCoreResource()
    {
        return Mage::getSingleton('core/resource');
    }

    public function getReadConnection()
    {
        return $this->getCoreResource()->getConnection('core_read');
    }

    public function getWriteConnection()
    {
        return $this->getCoreResource()->getConnection('core_write');
    }

    public function getCurrentPayoutPeriod()
    {
        $current_time = date('Y-m-d H:i:s', time());
        $readConnection = $this->getReadConnection();

        $query = "SELECT payout_period_id";
        $query .= " FROM ".self::TBL_PAYOUT_PERIOD."";
        $query .= " WHERE start_date <= '$current_time' and end_date >= '$current_time' and status = 'Open'";
        $query .= " ORDER BY payout_period_id ASC";
        $query .= " LIMIT 1";

        $result = $readConnection->fetchOne($query);

        return $result;
    }

    public function getPreviousPayoutPeriod()
    {
        $current_period = $this->getCurrentPayoutPeriod();

        $previous_period = null;

        if($current_period != null && $current_period > 0)
        {
            if($current_period > 1)
                $previous_period = $current_period - 1;
            else {
                $year = intval(date('Y', time())) - 1;
                $previous_days = "$year-12-02";

                $readConnection = $this->getReadConnection();

                $query = "SELECT payout_period_id";
                $query .= " FROM ".self::TBL_PAYOUT_PERIOD."";
                $query .= " WHERE start_date <= '$previous_days' and end_date >= '$previous_days'";
                $query .= " ORDER BY payout_period_id ASC";
                $query .= " LIMIT 1";

                $previous_period = $readConnection->fetchOne($query);
            }
        }

        return $previous_period;
    }

    public function getPeriodIdByTime($custom_time)
    {
        $current_time = date('Y-m-d 11:00:00', strtotime($custom_time));
        $readConnection = $this->getReadConnection();

        $query = "SELECT payout_period_id";
        $query .= " FROM ".self::TBL_PAYOUT_PERIOD."";
        $query .= " WHERE start_date <= '$current_time' and end_date >= '$current_time'";
        $query .= " ORDER BY payout_period_id ASC";
        $query .= " LIMIT 1";

        $result = $readConnection->fetchOne($query);

        return $result;
    }

    public function getCustomerStats($customer_id, $previous_period = false, $period_id = false)
    {
        if($period_id)
            $current_payout_period_id = $period_id;
        else {
            if(!$previous_period)
                $current_payout_period_id = $this->getCurrentPayoutPeriod();
            else {
                $current_payout_period_id = $this->getPreviousPayoutPeriod();
            }
        }

        if($current_payout_period_id != null && $current_payout_period_id > 0)
        {
            $readConnection = $this->getReadConnection();

            $query = "SELECT *";
            $query .= " FROM ".self::TBL_PAYOUT_PERIOD_STATS."";
            $query .= " WHERE customer_id = $customer_id AND payout_period_id = $current_payout_period_id";
            $query .= " ORDER BY payout_period_id ASC";
            $query .= " LIMIT 1";

            $result = $readConnection->fetchAll($query);

            if($result != null && is_array($result[0]))
                return $result[0];
        }

        return null;
    }

    public function getCustomerBadgesHistory($customer_id, $previous_period = false)
    {
        if(!$previous_period)
            $current_payout_period_id = $this->getCurrentPayoutPeriod();
        else {
            $current_payout_period_id = $this->getPreviousPayoutPeriod();
        }

        if($current_payout_period_id != null && $current_payout_period_id > 0)
        {
            $collection = Mage::getModel('badges/transaction')
                ->getCollection()
                ->addFieldToFilter('customer_id', $customer_id)
                ->addFieldToFilter('payout_period_id', $current_payout_period_id)
                ->setOrder('transaction_id', 'desc')
                ;

            if($collection != null && sizeof($collection) > 0)
            {
                $badge_ids = $collection->getColumnValues('badges_id');
                if(is_array($badge_ids) && sizeof($badge_ids) > 0)
                {
                    $badges_collection = Mage::getModel('badges/badges')
                        ->getCollection()
                        ->addFieldToFilter('badges_id', array('in'  => $badge_ids))
                        ->setOrder('badges_id', 'desc')
                        ;

                    return $badges_collection;
                }
            }
        }

        return null;
    }

    public function getImagePath($image_name)
    {
        return Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA) . self::PATH_BADGES_ICON . $image_name;
    }

    public function getBadgeFromStats($badge_name)
    {
        if($badge_name != null){
            $model = Mage::getModel('badges/badges')
                ->getCollection()
                ->addFieldToFilter('name', array('like' => '%'.$badge_name.'%'))
                ->addFieldToFilter('is_title', array('eq'   => 'Y'))
                ->addFieldToFilter('status', array('eq'   => 'active'))
                ->setOrder('badges_id', 'desc')
                ->getFirstItem()
            ;

        } else {
            $model = Mage::getModel('badges/badges')
                ->getCollection()
                ->addFieldToFilter('name', array('like' => '%Customer%'))
                ->addFieldToFilter('is_title', array('eq'   => 'Y'))
                ->addFieldToFilter('status', array('eq'   => 'active'))
                ->setOrder('badges_id', 'desc')
                ->getFirstItem()
            ;
        }

        if($model != null && $model->getId())
            return $model;
        else
            return null;
    }
}