<?php

class Magestore_Badges_Block_Adminhtml_Badges extends Mage_Adminhtml_Block_Widget_Grid_Container
{
	public function __construct(){
		$this->_controller = 'adminhtml_badges';
		$this->_blockGroup = 'badges';
		$this->_headerText = Mage::helper('badges')->__('Item Manager');
		$this->_addButtonLabel = Mage::helper('badges')->__('Add Item');
		parent::__construct();
	}
}