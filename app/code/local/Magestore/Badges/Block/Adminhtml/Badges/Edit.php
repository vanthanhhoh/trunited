<?php

class Magestore_Badges_Block_Adminhtml_Badges_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
	public function __construct(){
		parent::__construct();
		
		$this->_objectId = 'id';
		$this->_blockGroup = 'badges';
		$this->_controller = 'adminhtml_badges';
		
		$this->_updateButton('save', 'label', Mage::helper('badges')->__('Save Item'));
		$this->_updateButton('delete', 'label', Mage::helper('badges')->__('Delete Item'));
		
		$this->_addButton('saveandcontinue', array(
			'label'		=> Mage::helper('adminhtml')->__('Save And Continue Edit'),
			'onclick'	=> 'saveAndContinueEdit()',
			'class'		=> 'save',
		), -100);

		$this->_formScripts[] = "
			function toggleEditor() {
				if (tinyMCE.getInstanceById('badges_content') == null)
					tinyMCE.execCommand('mceAddControl', false, 'badges_content');
				else
					tinyMCE.execCommand('mceRemoveControl', false, 'badges_content');
			}

			function saveAndContinueEdit(){
				editForm.submit($('edit_form').action+'back/edit/');
			}
		";
	}

	public function getHeaderText(){
		if(Mage::registry('badges_data') && Mage::registry('badges_data')->getId())
			return Mage::helper('badges')->__("Edit Item '%s'", $this->htmlEscape(Mage::registry('badges_data')->getTitle()));
		return Mage::helper('badges')->__('Add Item');
	}
}