<?php

class Magestore_Badges_Block_Adminhtml_Badges_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
	public function __construct(){
		parent::__construct();
		$this->setId('badges_tabs');
		$this->setDestElementId('edit_form');
		$this->setTitle(Mage::helper('badges')->__('Item Information'));
	}

	protected function _beforeToHtml(){
		$this->addTab('form_section', array(
			'label'	 => Mage::helper('badges')->__('Item Information'),
			'title'	 => Mage::helper('badges')->__('Item Information'),
			'content'	 => $this->getLayout()->createBlock('badges/adminhtml_badges_edit_tab_form')->toHtml(),
		));
		return parent::_beforeToHtml();
	}
}