<?php

class Magestore_Badges_Block_Adminhtml_Badges_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
	protected function _prepareForm(){
		$form = new Varien_Data_Form();
		$this->setForm($form);
		
		if (Mage::getSingleton('adminhtml/session')->getBadgesData()){
			$data = Mage::getSingleton('adminhtml/session')->getBadgesData();
			Mage::getSingleton('adminhtml/session')->setBadgesData(null);
		}elseif(Mage::registry('badges_data'))
			$data = Mage::registry('badges_data')->getData();
		
		$fieldset = $form->addFieldset('badges_form', array('legend'=>Mage::helper('badges')->__('Item information')));

		$fieldset->addField('title', 'text', array(
			'label'		=> Mage::helper('badges')->__('Title'),
			'class'		=> 'required-entry',
			'required'	=> true,
			'name'		=> 'title',
		));

		$fieldset->addField('filename', 'file', array(
			'label'		=> Mage::helper('badges')->__('File'),
			'required'	=> false,
			'name'		=> 'filename',
		));

		$fieldset->addField('status', 'select', array(
			'label'		=> Mage::helper('badges')->__('Status'),
			'name'		=> 'status',
			'values'	=> Mage::getSingleton('badges/status')->getOptionHash(),
		));

		$fieldset->addField('content', 'editor', array(
			'name'		=> 'content',
			'label'		=> Mage::helper('badges')->__('Content'),
			'title'		=> Mage::helper('badges')->__('Content'),
			'style'		=> 'width:700px; height:500px;',
			'wysiwyg'	=> false,
			'required'	=> true,
		));

		$form->setValues($data);
		return parent::_prepareForm();
	}
}