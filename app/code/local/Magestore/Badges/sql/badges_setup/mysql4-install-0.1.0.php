<?php

$installer = $this;
$installer->startSetup();

$installer->run("

DROP TABLE IF EXISTS {$this->getTable('badges/badges')};
CREATE TABLE {$this->getTable('badges/badges')} (
  `badges_id` int(11) unsigned NOT NULL auto_increment,
  `name` varchar(255) NOT NULL default '',
  `value` FLOAT NULL,
  `description` text NULL default '',
  `category` varchar(255) NULL default '',
  `image_name` varchar(255) NULL default '',
  `is_title` varchar(255) NULL default '',
  `status` varchar(255) NULL default '',
  `hex_color` varchar(255) NULL default '',
  `created_at` datetime NULL,
  `updated_at` datetime NULL,
  PRIMARY KEY (`badges_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS {$this->getTable('badges/transaction')};
CREATE TABLE {$this->getTable('badges/transaction')} (
  `transaction_id` int(11) unsigned NOT NULL auto_increment,
  `customer_id` int(11) unsigned NOT NULL,
  `payout_period_id` int(11) unsigned NOT NULL,
  `badges_id` int(11) unsigned NOT NULL,
  `created_at` datetime NULL,
  `updated_at` datetime NULL,
  PRIMARY KEY (`transaction_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS {$this->getTable('badges/popup')};
CREATE TABLE {$this->getTable('badges/popup')} (
  `popup_id` int(11) unsigned NOT NULL auto_increment,
  `customer_id` int(11) unsigned NOT NULL,
  `opened_time` datetime NULL,
  `created_at` datetime NULL,
  `updated_at` datetime NULL,
  PRIMARY KEY (`popup_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

    ");

$file = Mage::getBaseDir('media') . DS . 'badges' . DS . 'csv' . DS . 'badges-new.csv';
$csvObject = new Varien_File_Csv();
try {
    $data = $csvObject->getData($file);
    if (sizeof($data) > 0) {
        $i = 0;
        $transactionSave = Mage::getModel('core/resource_transaction');
        foreach ($data as $ls) {
            if ($i > 0) {
                $model = Mage::getModel('badges/badges');
                $_dt = array(
                    'name' => $ls[1],
                    'value' => $ls[2],
                    'description' => $ls[3],
                    'category' => $ls[4],
                    'image_name' => $ls[5],
                    'is_title' => $ls[6],
                    'status' => $ls[7],
                    'hex_color' => $ls[8],
                );
                $model->setData($_dt);
                $transactionSave->addObject($model);
            }
            $i++;
        }
        $transactionSave->save();
    }
} catch (Exception $e) {

}

$installer->endSetup(); 