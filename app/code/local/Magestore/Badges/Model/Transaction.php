<?php

class Magestore_Badges_Model_Transaction extends Mage_Core_Model_Abstract
{
	public function _construct(){
		parent::_construct();
		$this->_init('badges/transaction');
	}

	/**
	 * Before object save manipulations
	 *
	 * @return Magestore_Badges_Model_Transaction
	 */
	protected function _beforeSave()
	{
		parent::_beforeSave();

		if($this->isObjectNew())
			$this->setData('created_at', now());

		$this->setData('updated_at', now());

		return $this;
	}
}
