<?php

class Magestore_Badges_Model_Popup extends Mage_Core_Model_Abstract
{
	public function _construct(){
		parent::_construct();
		$this->_init('badges/popup');
	}

	/**
	 * Before object save manipulations
	 *
	 * @return Magestore_Badges_Model_Popup
	 */
	protected function _beforeSave()
	{
		parent::_beforeSave();

		if($this->isObjectNew()) {
			$this->setData('created_at', now());
			$this->setData('opened_time', now());
		}

		$this->setData('updated_at', now());

		return $this;
	}
}
