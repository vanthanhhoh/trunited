<?php

class Magestore_Affiliatepoints_Helper_Transaction extends Mage_Core_Helper_Abstract
{
    public function createTransaction($account, $data, $type, $status)
    {
        $result = null;
        try {
            if (!$account->getId())
                throw new Exception(
                    Mage::helper('affiliatepoints')->__('Customer doesn\'t exist')
                );

            $customer = Mage::getModel('customer/customer')->load($account->getCustomerId());
            $transaction = Mage::getModel('affiliatepoints/transaction');

            $_data = array();
            $_data['affiliatepoints_id'] = $account->getId();
            $_data['customer_id'] = $customer->getId();
            $_data['customer_email'] = $customer->getEmail();
            $_data['title'] = isset($data['title']) ? $data['title'] : '';
            $_data['description'] = isset($data['description']) ? $data['description'] : '';
            $_data['action_type'] = $type;
            $_data['award_from'] = isset($data['award_from']) ? $data['award_from'] : Magestore_Affiliatepoints_Model_Status::AWARD_FROM_OTHER;
            $_data['status'] = $status;
            $_data['created_at'] = isset($data['created_at']) ? $data['created_at'] : now();
            $_data['updated_at'] = now();
            $_data['expiration_at'] = isset($data['expiration_at']) ? $data['expiration_at'] : '';
            $_data['order_id'] = isset($data['order_id']) ? $data['order_id'] : '';
            $_data['current_point'] = $account->getAffiliatePoints();
            $_data['changed_point'] = isset($data['changed_point']) ? $data['changed_point'] : '';
            $_data['receiver_email'] = isset($data['receiver_email']) ? $data['receiver_email'] : '';
            $_data['receiver_customer_id'] = isset($data['receiver_customer_id']) ? $data['receiver_customer_id'] : '';
            $_data['recipient_transaction_id'] = isset($data['recipient_transaction_id']) ? $data['recipient_transaction_id'] : '';
            $_data['point_back'] = isset($data['point_back']) ? $data['point_back'] : '';
            $_data['order_filter_ids'] = isset($data['order_filter_ids']) ? $data['order_filter_ids'] : '';
            $_data['is_on_hold'] = isset($data['is_on_hold']) ? $data['is_on_hold'] : 0;
            $_data['hold_point'] = isset($data['hold_point']) ? $data['hold_point'] : 0;

            $transaction->setData($_data);
            $transaction->save();

            $result = $transaction;

        } catch (Exception $ex) {
            Mage::getSingleton('adminhtml/session')->addError(
                Mage::helper('affiliatepoints')->__($ex->getMessage())
            );
            $result = null;
        }

        return $result;
    }

    public function checkExpiryDateOnHoldTransaction()
    {
        $collection = Mage::getModel('affiliatepoints/transaction')->getCollection()
            ->addFieldToFilter('action_type', array('in' => array(
                Magestore_Affiliatepoints_Model_Type::TYPE_TRANSACTION_BY_ADMIN,
                Magestore_Affiliatepoints_Model_Type::TYPE_TRANSACTION_RECEIVE_FROM_GLOBAL_BRAND,
            )))
            ->addFieldToFilter('status', Magestore_Affiliatepoints_Model_Status::STATUS_TRANSACTION_ON_HOLD)
            ->addFieldToFilter('expiration_at', array('notnull' => true))
            ->addFieldToFilter('is_on_hold', 1)
            ->setOrder('transaction_id', 'desc')
        ;

        if(sizeof($collection) > 0)
        {
            $t = time();
            foreach ($collection as $transaction) {
                $date = $transaction->getExpirationAt();

                if(date('Y',strtotime($date)) == date('Y', $t) &&
                    date('m',strtotime($date)) == date('m', $t))
                {
                    try {
                        Mage::helper('affiliatepoints/account')->updatePoint(
                            $transaction->getCustomerId(),
                            $transaction->getHoldPoint()
                        );

                        $transaction->setData('changed_point', $transaction->getHoldPoint());
                        $transaction->setData('updated_at', now());
                        $transaction->setData('status', Magestore_Affiliatepoints_Model_Status::STATUS_TRANSACTION_COMPLETED);
                        $transaction->save();

                    } catch (Exception $e) {
                        Mage::logException($e);
                    }
                }

            }

        }
    }



}
