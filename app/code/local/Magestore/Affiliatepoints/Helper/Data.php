<?php

class Magestore_Affiliatepoints_Helper_Data extends Mage_Core_Helper_Abstract
{
    public function getConfigData($group, $field, $store = null)
    {
        return Mage::getStoreConfig('affiliatepoints/' . $group . '/' . $field, $store);
    }

    public function isEnable()
    {
        return $this->getConfigData('general', 'enable');
    }

    public function createNewObj($customer_id)
    {
        $model = Mage::getModel('affiliatepoints/customer');
        $data = array(
            'customer_id' => $customer_id,
            'affiliate_points' => 0,
        );

        $model->setData($data);

        try {
            $model->save();
        } catch (Exception $ex) {
            return null;
        }

        return $model;
    }

    public function synchronizeAccount()
    {
        $customer_collection = Mage::getModel('customer/customer')->getCollection()
            ->addAttributeToSelect('entity_id');

        if (sizeof($customer_collection) > 0) {
            $result = 0;
            $transactionSave = Mage::getModel('core/resource_transaction');
            foreach ($customer_collection as $customer) {
                try {
                    $obj = Mage::getModel('fundraiser/customer');
                    $data = array(
                        'customer_id' => $customer->getId(),
                        'affiliate_points' => 0,
                    );

                    $obj->setData($data);
                    $result++;
                    $transactionSave->addObject($obj);
                } catch (Exception $ex) {

                }
            }
            $transactionSave->save();

            zend_debug::dump($result);
        }
    }

    public function compareExpireDate($start_time, $end_time)
    {
        $sub = $end_time - $start_time;
        if ($sub < 0)
            return false;

        $diff = abs($sub);

        $years = floor($diff / (365 * 60 * 60 * 24));
        $months = floor(($diff - $years * 365 * 60 * 60 * 24) / (30 * 60 * 60 * 24));
        $days = floor(($diff - $years * 365 * 60 * 60 * 24 - $months * 30 * 60 * 60 * 24) / (60 * 60 * 24));

        if ($years > 0 || $months > 0) {
            return false;
        } else {
            return $days;
        }
    }

    public function compareTime($start_time, $end_time)
    {
        $diff = abs($end_time - $start_time);

        $years = floor($diff / (365 * 60 * 60 * 24));
        $months = floor(($diff - $years * 365 * 60 * 60 * 24) / (30 * 60 * 60 * 24));
        $days = floor(($diff - $years * 365 * 60 * 60 * 24 - $months * 30 * 60 * 60 * 24) / (60 * 60 * 24));
        $hours = floor(($diff - $years * 365 * 60 * 60 * 24 - $months * 30 * 60 * 60 * 24 - $days * 60 * 60 * 24) / (60 * 60));
        $minutes = floor(($diff - $years * 365 * 60 * 60 * 24 - $months * 30 * 60 * 60 * 24 - $days * 60 * 60 * 24 - $hours * 60 * 60) / 60);
        $seconds = floor(($diff - $years * 365 * 60 * 60 * 24 - $months * 30 * 60 * 60 * 24 - $days * 60 * 60 * 24 - $hours * 60 * 60 - $minutes * 60));

        if ($years > 0 || $months > 0) {
            return false;
        } else {
            return $days;
        }
    }

    public function addDaysToDate($date, $days, $operator = '+')
    {
        $date = strtotime($operator . "" . $days . " days", strtotime($date));
        return date("Y-m-d H:i:s", $date);
    }

    public function getDaysOfHold()
    {
        return Mage::helper('manageapi')->getDataConfig('day_hold_points', 'affiliate_points');
    }
}
