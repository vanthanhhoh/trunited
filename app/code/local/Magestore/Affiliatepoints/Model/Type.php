<?php

class Magestore_Affiliatepoints_Model_Type extends Varien_Object
{
	const TYPE_TRANSACTION_BY_ADMIN	= 1;
	const TYPE_TRANSACTION_RECEIVE_FROM_GLOBAL_BRAND = 2;

	static public function getOptionArray(){
		return array(
			self::TYPE_TRANSACTION_BY_ADMIN	=> Mage::helper('affiliatepoints')->__('Changed by Admin'),
			self::TYPE_TRANSACTION_RECEIVE_FROM_GLOBAL_BRAND   => Mage::helper('affiliatepoints')->__('Received Affiliate Points from Global Brand'),
		);
	}
	
	static public function getOptionHash(){
		$options = array();
		foreach (self::getOptionArray() as $value => $label)
			$options[] = array(
				'value'	=> $value,
				'label'	=> $label
			);
		return $options;
	}

	static public function getDataType()
	{
		return array(
			self::TYPE_TRANSACTION_BY_ADMIN,
			self::TYPE_TRANSACTION_RECEIVE_FROM_GLOBAL_BRAND,
		);
	}

}
