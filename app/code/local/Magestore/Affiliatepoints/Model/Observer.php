<?php

class Magestore_Affiliatepoints_Model_Observer
{
    public function affiliatepointCreated($observer)
    {
        $customer = $observer->getCustomer();

        if($customer != null && $customer->getId()){
            Mage::helper('affiliatepoints')->createNewObj($customer->getId());
        }

        return $this;
    }
}
