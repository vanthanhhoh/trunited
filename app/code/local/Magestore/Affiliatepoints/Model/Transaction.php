<?php

class Magestore_Affiliatepoints_Model_Transaction extends Mage_Core_Model_Abstract
{
	public function _construct(){
		parent::_construct();
		$this->_init('affiliatepoints/transaction');
	}

	public function completeTransaction()
	{
		try {
			Mage::helper('affiliatepoints/account')->updatePoint(
				$this->getCustomerId(),
				$this->getHoldPoint()
			);

			$this->setData('changed_point', $this->getHoldPoint());
			$this->setData('updated_at', now());
			$this->setData('status', Magestore_Affiliatepoints_Model_Status::STATUS_TRANSACTION_COMPLETED);
			$this->save();

		} catch (Exception $e) {
			Mage::logException($e);
		}
	}
}
