<?php

class Magestore_Affiliatepoints_Model_Status extends Varien_Object
{
	const STATUS_ENABLED	= 1;
	const STATUS_DISABLED	= 2;

    const STATUS_TRANSACTION_PENDING = 1;
    const STATUS_TRANSACTION_COMPLETED = 2;
    const STATUS_TRANSACTION_CANCELED = 3;
    const STATUS_TRANSACTION_EXPIRED = 4;
    const STATUS_TRANSACTION_ON_HOLD = 5;

    const AWARD_FROM_OTHER	= 0;
    const AWARD_FROM_FUNDRAISER	= 1;
    const AWARD_FROM_DAILY_DEALS = 2;

	static public function getOptionArray(){
		return array(
			self::STATUS_ENABLED	=> Mage::helper('affiliatepoints')->__('Enabled'),
			self::STATUS_DISABLED   => Mage::helper('affiliatepoints')->__('Disabled')
		);
	}
	
	static public function getOptionHash(){
		$options = array();
		foreach (self::getOptionArray() as $value => $label)
			$options[] = array(
				'value'	=> $value,
				'label'	=> $label
			);
		return $options;
	}

    static public function getTransactionOptionArray(){
        return array(
            self::STATUS_TRANSACTION_PENDING	=> Mage::helper('affiliatepoints')->__('Pending'),
            self::STATUS_TRANSACTION_COMPLETED   => Mage::helper('affiliatepoints')->__('Completed'),
            self::STATUS_TRANSACTION_CANCELED   => Mage::helper('affiliatepoints')->__('Canceled'),
            self::STATUS_TRANSACTION_EXPIRED   => Mage::helper('affiliatepoints')->__('Expired'),
            self::STATUS_TRANSACTION_ON_HOLD   => Mage::helper('affiliatepoints')->__('On Hold'),
        );
    }

    static public function getTransactionOptionHash(){
        $options = array();
        foreach (self::getTransactionOptionArray() as $value => $label)
            $options[] = array(
                'value'	=> $value,
                'label'	=> $label
            );
        return $options;
    }

    static public function getOptionAwardFromArray(){
        return array(
            self::AWARD_FROM_OTHER	=> Mage::helper('affiliatepoints')->__('Other'),
            self::AWARD_FROM_FUNDRAISER	=> Mage::helper('affiliatepoints')->__('Fundraiser'),
            self::AWARD_FROM_DAILY_DEALS   => Mage::helper('affiliatepoints')->__('Daily Deals')
        );
    }

    static public function getOptionAwardFromHas(){
        $options = array();
        foreach (self::getOptionArray() as $value => $label)
            $options[] = array(
                'value'	=> $value,
                'label'	=> $label
            );
        return $options;
    }
}
