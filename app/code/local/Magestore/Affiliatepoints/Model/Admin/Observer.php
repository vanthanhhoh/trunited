<?php

class Magestore_Affiliatepoints_Model_Admin_Observer
{

    /**
     * @param $observer
     * @return $this
     * @throws Exception
     */
    public function customerSaveAfter($observer)
    {
        $customer = $observer['customer'];
        $params = Mage::app()->getRequest()->getParam('Affiliatepoints');
        $helper = Mage::helper('affiliatepoints');

        if(!isset($params) || $params['affiliate_point'] == '')
            return $this;

        if(filter_var($params['affiliate_point'], FILTER_VALIDATE_FLOAT) === false)
            throw new Exception(
                Mage::helper('affiliatepoints')->__('Data invalid')
            );

        $params['is_on_hold'] = empty($params['is_on_hold']) ? 0 : 1;

        $connection = Mage::getSingleton('core/resource')->getConnection('core_write');

        try {
            $connection->beginTransaction();

            if($params['is_on_hold'] == 1){
                $affiliatepointsAccount = Mage::helper('affiliatepoints/account')->updatePoint(
                    $customer->getId(),
                    0
                );

                if($params['expiration_at'] != null)
                    $params['expiration_at'] = $helper->addDaysToDate(
                        now(),
                        $params['expiration_at']
                    );

                $params['hold_point'] = $params['affiliate_point'];
            } else {
                $affiliatepointsAccount = Mage::helper('affiliatepoints/account')->updatePoint(
                    $customer->getId(),
                    $params['affiliate_point']
                );
            }

            if($affiliatepointsAccount != null)
            {
                if($params['is_on_hold'] == 1){
                    $status = Magestore_Affiliatepoints_Model_Status::STATUS_TRANSACTION_ON_HOLD;
                } else {
                    $params['changed_point'] = $params['affiliate_point'];
                    $status = Magestore_Affiliatepoints_Model_Status::STATUS_TRANSACTION_COMPLETED;
                }
                Mage::helper('affiliatepoints/transaction')->createTransaction(
                    $affiliatepointsAccount,
                    $params,
                    Magestore_Affiliatepoints_Model_Type::TYPE_TRANSACTION_BY_ADMIN,
                    $status
                );
            }

            $connection->commit();

        } catch (Exception $e) {
            $connection->rollback();
            Mage::getSingleton('adminhtml/session')->addError(
                Mage::helper('affiliatepoints')->__($e->getMessage())
            );
        }

    }
}
