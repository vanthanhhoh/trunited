<?php
/**
 * Magestore
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magestore.com license that is
 * available through the world-wide-web at this URL:
 * http://www.magestore.com/license-agreement.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Magestore
 * @package     Magestore_affiliatepoints
 * @copyright   Copyright (c) 2012 Magestore (http://www.magestore.com/)
 * @license     http://www.magestore.com/license-agreement.html
 */

/**
 * affiliatepoints Tab on Customer Edit Form Block
 *
 * @category    Magestore
 * @package     Magestore_affiliatepoints
 * @author      Magestore Developer
 */
class Magestore_Affiliatepoints_Block_Adminhtml_Customer_Edit_Tab_Affiliatepoints
    extends Mage_Adminhtml_Block_Widget_Form
    implements Mage_Adminhtml_Block_Widget_Tab_Interface
{
    protected $_affiliatepointsAccount = null;

    /**
     * @return Mage_Core_Model_Abstract|null
     * @throws Exception
     */
    public function getAffiliatepointsAccount()
    {
        if (is_null($this->_affiliatepointsAccount)) {
            $customerId = $this->getRequest()->getParam('id');
            $this->_affiliatepointsAccount = Mage::getModel('affiliatepoints/customer')
                ->load($customerId, 'customer_id');
        }
        return $this->_affiliatepointsAccount;
    }

    public function getAffiliatePoints()
    {
        return $this->getAffiliatepointsAccount()->getAffiliatePoints();
    }

    public function getOnHoldAffiliatePoints()
    {
        $affiliatepoints_id = $this->getAffiliatepointsAccount()->getId();

        $collection = Mage::getModel('affiliatepoints/transaction')->getCollection()
            ->addFieldToSelect('transaction_id')
            ->addFieldToSelect('hold_point')
            ->addFieldToFilter('affiliatepoints_id', $affiliatepoints_id)
            ->addFieldToFilter('customer_id', $this->getRequest()->getParam('id'))
            ->addFieldToFilter('status', Magestore_Affiliatepoints_Model_Status::STATUS_TRANSACTION_ON_HOLD)
            ->addFieldToFilter('action_type', array(
                'in'    => Magestore_Affiliatepoints_Model_Type::getDataType()
            ))
            ;

        $total = 0;

        if(sizeof($collection) > 0)
        {
            foreach ($collection as $item) {
                $total += $item->getData('hold_point');
            }
        }

        return number_format($total, 2);
    }

    /**
     * @return Mage_Adminhtml_Block_Widget_Form
     */
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form();
        $form->setHtmlIdPrefix('affiliatepoints_');
        $this->setForm($form);

        $fieldset = $form->addFieldset('affiliatepoints_form', array(
            'legend' => Mage::helper('affiliatepoints')->__('Affiliate Points Information')
        ));

        $fieldset->addField('affiliatepoints_balance', 'note', array(
            'label' => Mage::helper('affiliatepoints')->__('Affiliate Points'),
            'title' => Mage::helper('affiliatepoints')->__('Affiliate Points'),
            'text' => '<strong>' . $this->getAffiliatePoints() . '</strong>',
        ));

        $fieldset->addField('holding_point', 'note', array(
            'label' => Mage::helper('rewardpoints')->__('On Hold Points Balance'),
            'title' => Mage::helper('rewardpoints')->__('On Hold Points Balance'),
            'text' => '<strong>' . $this->getOnHoldAffiliatePoints() . '</strong>',
        ));

        $fieldset->addField('affiliatepoints_point', 'text', array(
            'label' => Mage::helper('affiliatepoints')->__('Change Affiliate Points'),
            'title' => Mage::helper('affiliatepoints')->__('Change Affiliate Points'),
            'name' => 'Affiliatepoints[affiliate_point]',
            'class' => 'validate-number',
            'note' => Mage::helper('affiliatepoints')->__('Add or subtract customer\'s affiliate points. For ex: 99 or -99 affiliate points.'),
        ));

        $fieldset->addField('title_point', 'textarea', array(
            'label' => Mage::helper('affiliatepoints')->__('Change Transaction Title'),
            'title' => Mage::helper('affiliatepoints')->__('Change Transaction Title'),
            'name' => 'Affiliatepoints[title]',
            'style' => 'height: 5em;'
        ));

        $fieldset->addField('description_point', 'textarea', array(
            'label' => Mage::helper('affiliatepoints')->__('Change Transaction Description'),
            'title' => Mage::helper('affiliatepoints')->__('Change Transaction Description'),
            'name' => 'Affiliatepoints[description]',
            'style' => 'height: 5em;'
        ));

        $fieldset->addField('expiration_at', 'text', array(
            'label' => Mage::helper('affiliatepoints')->__('Points Expire On'),
            'title' => Mage::helper('affiliatepoints')->__('Points Expire On'),
            'name'  => 'Affiliatepoints[expiration_at]',
            'note'  => Mage::helper('affiliatepoints')->__('day(s) since the transaction date. If empty or zero, there is no limitation.')
        ));

        $fieldset->addField('is_on_hold', 'checkbox', array(
            'label' => Mage::helper('affiliatepoints')->__('Points On Hold'),
            'title' => Mage::helper('affiliatepoints')->__('Points On Hold'),
            'name'  => 'Affiliatepoints[is_on_hold]',
            'value' => 1,
        ));

        $fieldset = $form->addFieldset('affiliatepoints_history_fieldset', array(
            'legend' => Mage::helper('affiliatepoints')->__('Transaction History')
        ))->setRenderer($this->getLayout()->createBlock('adminhtml/widget_form_renderer_fieldset')->setTemplate(
            'affiliatepoints/history.phtml'
        ));

        return parent::_prepareForm();
    }

    public function getTabLabel()
    {
        return Mage::helper('affiliatepoints')->__('Affiliate Points');
    }

    public function getTabTitle()
    {
        return Mage::helper('affiliatepoints')->__('Affiliate Points');
    }

    public function canShowTab()
    {
        return true;
    }

    public function isHidden()
    {
        return false;
    }
}
