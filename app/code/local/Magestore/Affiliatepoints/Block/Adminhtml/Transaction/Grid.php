<?php

class Magestore_Affiliatepoints_Block_Adminhtml_Transaction_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
	public function __construct(){
		parent::__construct();
		$this->setId('affiliatepoints_transactionGrid');
		$this->setDefaultSort('transaction_id');
		$this->setDefaultDir('DESC');
		$this->setSaveParametersInSession(true);
	}

	protected function _prepareCollection(){
        $collection = Mage::getModel('affiliatepoints/transaction')->getCollection()
            ->setOrder('transaction_id', 'desc');
        $this->setCollection($collection);
		return parent::_prepareCollection();
	}

	protected function _prepareColumns(){
        $this->addColumn('transaction_id', array(
            'header' => Mage::helper('affiliatepoints')->__('ID'),
            'align' => 'right',
            'width' => '50px',
            'index' => 'transaction_id',
//            'type' => 'number',
        ));

        $this->addColumn('title', array(
            'header' => Mage::helper('affiliatepoints')->__('Title'),
            'align' => 'left',
            'index' => 'title',
        ));

        $this->addColumn('action_type', array(
            'header' => Mage::helper('affiliatepoints')->__('Action'),
            'align' => 'left',
            'index' => 'action_type',
            'type' => 'options',
            'options' => Mage::getSingleton('affiliatepoints/type')->getOptionArray(),
        ));

        $this->addColumn('current_point', array(
            'header' => Mage::helper('affiliatepoints')->__('Current Points'),
            'align' => 'right',
            'index' => 'current_point',
            'type' => 'number',
        ));

        $this->addColumn('changed_point', array(
            'header' => Mage::helper('affiliatepoints')->__('Updated Points'),
            'align' => 'right',
            'index' => 'changed_point',
            'type' => 'number',
        ));

        $this->addColumn('created_at', array(
            'header' => Mage::helper('affiliatepoints')->__('Created On'),
            'index' => 'created_at',
            'type' => 'datetime',
        ));

        $this->addColumn('updated_at', array(
            'header' => Mage::helper('affiliatepoints')->__('Updated On'),
            'index' => 'updated_at',
            'type' => 'datetime',
        ));

        $this->addColumn('expiration_at', array(
            'header' => Mage::helper('affiliatepoints')->__('Expires On'),
            'index' => 'expiration_at',
            'type' => 'datetime',
        ));

        $this->addColumn('status', array(
            'header' => Mage::helper('affiliatepoints')->__('Status'),
            'align' => 'left',
            'index' => 'status',
            'type' => 'options',
            'options' => Mage::getSingleton('affiliatepoints/status')->getTransactionOptionArray(),
        ));

        $this->addColumn('is_on_hold', array(
            'header' => Mage::helper('affiliatepoints')->__('Is On Hold'),
            'align' => 'left',
            'index' => 'is_on_hold',
            'type' => 'options',
            'options' => array(
                '1' => 'Yes',
                '0' =>  'No',
            ),
        ));

        $this->addColumn('hold_point', array(
            'header' => Mage::helper('affiliatepoints')->__('Hold Points'),
            'align' => 'left',
            'index' => 'hold_point',
        ));

        $this->addColumn('award_from', array(
            'header' => Mage::helper('affiliatepoints')->__('Award From'),
            'align' => 'left',
            'index' => 'award_from',
            'type' => 'options',
            'options' => Mage::getSingleton('affiliatepoints/status')->getOptionAwardFromArray(),
        ));

		$this->addExportType('*/*/exportCsv', Mage::helper('affiliatepoints')->__('CSV'));
		$this->addExportType('*/*/exportXml', Mage::helper('affiliatepoints')->__('XML'));

		return parent::_prepareColumns();
	}

	protected function _prepareMassaction(){
		$this->setMassactionIdField('transaction_id');
		$this->getMassactionBlock()->setFormFieldName('transaction');

		$this->getMassactionBlock()->addItem('delete', array(
			'label'		=> Mage::helper('affiliatepoints')->__('Delete'),
			'url'		=> $this->getUrl('*/*/massDelete'),
			'confirm'	=> Mage::helper('affiliatepoints')->__('Are you sure?')
		));

		return $this;
	}

	public function getRowUrl($row){
//		return $this->getUrl('*/*/edit', array('id' => $row->getId()));
	}
}
