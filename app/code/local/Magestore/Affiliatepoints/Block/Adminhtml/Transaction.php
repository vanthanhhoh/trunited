
<?php

class Magestore_Affiliatepoints_Block_Adminhtml_Transaction extends Mage_Adminhtml_Block_Widget_Grid_Container
{
	public function __construct(){
		$this->_controller = 'adminhtml_transaction';
		$this->_blockGroup = 'affiliatepoints';
		$this->_headerText = Mage::helper('affiliatepoints')->__('Transaction Manager');
		$this->_addButtonLabel = Mage::helper('affiliatepoints')->__('Add Transaction');
		parent::__construct();
		$this->_removeButton('add');
	}
}
