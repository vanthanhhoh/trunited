<?php

class Magestore_Affiliatepoints_Block_Adminhtml_Affiliatepoints_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
	protected function _prepareForm(){
		$form = new Varien_Data_Form();
		$this->setForm($form);
		
		if (Mage::getSingleton('adminhtml/session')->getAffiliatepointsData()){
			$data = Mage::getSingleton('adminhtml/session')->getAffiliatepointsData();
			Mage::getSingleton('adminhtml/session')->setAffiliatepointsData(null);
		}elseif(Mage::registry('affiliatepoints_data'))
			$data = Mage::registry('affiliatepoints_data')->getData();
		
		$fieldset = $form->addFieldset('affiliatepoints_form', array('legend'=>Mage::helper('affiliatepoints')->__('Item information')));

		$fieldset->addField('title', 'text', array(
			'label'		=> Mage::helper('affiliatepoints')->__('Title'),
			'class'		=> 'required-entry',
			'required'	=> true,
			'name'		=> 'title',
		));

		$fieldset->addField('filename', 'file', array(
			'label'		=> Mage::helper('affiliatepoints')->__('File'),
			'required'	=> false,
			'name'		=> 'filename',
		));

		$fieldset->addField('status', 'select', array(
			'label'		=> Mage::helper('affiliatepoints')->__('Status'),
			'name'		=> 'status',
			'values'	=> Mage::getSingleton('affiliatepoints/status')->getOptionHash(),
		));

		$fieldset->addField('content', 'editor', array(
			'name'		=> 'content',
			'label'		=> Mage::helper('affiliatepoints')->__('Content'),
			'title'		=> Mage::helper('affiliatepoints')->__('Content'),
			'style'		=> 'width:700px; height:500px;',
			'wysiwyg'	=> false,
			'required'	=> true,
		));

		$form->setValues($data);
		return parent::_prepareForm();
	}
}