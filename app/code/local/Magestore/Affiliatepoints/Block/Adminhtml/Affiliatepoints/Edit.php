<?php

class Magestore_Affiliatepoints_Block_Adminhtml_Affiliatepoints_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
	public function __construct(){
		parent::__construct();
		
		$this->_objectId = 'id';
		$this->_blockGroup = 'affiliatepoints';
		$this->_controller = 'adminhtml_affiliatepoints';
		
		$this->_updateButton('save', 'label', Mage::helper('affiliatepoints')->__('Save Item'));
		$this->_updateButton('delete', 'label', Mage::helper('affiliatepoints')->__('Delete Item'));
		
		$this->_addButton('saveandcontinue', array(
			'label'		=> Mage::helper('adminhtml')->__('Save And Continue Edit'),
			'onclick'	=> 'saveAndContinueEdit()',
			'class'		=> 'save',
		), -100);

		$this->_formScripts[] = "
			function toggleEditor() {
				if (tinyMCE.getInstanceById('affiliatepoints_content') == null)
					tinyMCE.execCommand('mceAddControl', false, 'affiliatepoints_content');
				else
					tinyMCE.execCommand('mceRemoveControl', false, 'affiliatepoints_content');
			}

			function saveAndContinueEdit(){
				editForm.submit($('edit_form').action+'back/edit/');
			}
		";
	}

	public function getHeaderText(){
		if(Mage::registry('affiliatepoints_data') && Mage::registry('affiliatepoints_data')->getId())
			return Mage::helper('affiliatepoints')->__("Edit Item '%s'", $this->htmlEscape(Mage::registry('affiliatepoints_data')->getTitle()));
		return Mage::helper('affiliatepoints')->__('Add Item');
	}
}