<?php

class Magestore_Affiliatepoints_Block_Adminhtml_Affiliatepoints_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
	public function __construct(){
		parent::__construct();
		$this->setId('affiliatepoints_tabs');
		$this->setDestElementId('edit_form');
		$this->setTitle(Mage::helper('affiliatepoints')->__('Item Information'));
	}

	protected function _beforeToHtml(){
		$this->addTab('form_section', array(
			'label'	 => Mage::helper('affiliatepoints')->__('Item Information'),
			'title'	 => Mage::helper('affiliatepoints')->__('Item Information'),
			'content'	 => $this->getLayout()->createBlock('affiliatepoints/adminhtml_affiliatepoints_edit_tab_form')->toHtml(),
		));
		return parent::_beforeToHtml();
	}
}