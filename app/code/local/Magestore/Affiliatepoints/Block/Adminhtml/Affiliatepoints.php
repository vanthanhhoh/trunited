<?php

class Magestore_Affiliatepoints_Block_Adminhtml_Affiliatepoints extends Mage_Adminhtml_Block_Widget_Grid_Container
{
	public function __construct(){
		$this->_controller = 'adminhtml_affiliatepoints';
		$this->_blockGroup = 'affiliatepoints';
		$this->_headerText = Mage::helper('affiliatepoints')->__('Item Manager');
		$this->_addButtonLabel = Mage::helper('affiliatepoints')->__('Add Item');
		parent::__construct();
	}
}