<?php

$installer = $this;
$installer->startSetup();

$installer->run("

	DROP TABLE IF EXISTS {$this->getTable('affiliatepoints/customer')};
	DROP TABLE IF EXISTS {$this->getTable('affiliatepoints/transaction')};	

	CREATE TABLE {$this->getTable('affiliatepoints/customer')} (
		`affiliatepoints_id` int(11) unsigned NOT NULL auto_increment,
		`customer_id` int(10) unsigned NOT NULL,
		`affiliate_points` DECIMAL(10,2) unsigned NOT NULL default 0,
		`created_at` datetime NULL,
		`updated_at` datetime NULL,
		PRIMARY KEY (`affiliatepoints_id`)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8;

	CREATE TABLE {$this->getTable('affiliatepoints/transaction')} (
		`transaction_id` int(10) unsigned NOT NULL auto_increment,
		`affiliatepoints_id` int(10) unsigned NULL,
		`customer_id` int(10) unsigned NULL,
		`customer_email` varchar(255) NOT NULL,
		`title` varchar(255) NOT NULL,
		`description` varchar(255) NOT NULL,
		`action_type` smallint(5) NOT NULL,
		`award_from` smallint(5) NOT NULL,
		`status` smallint(5) NOT NULL,
		`current_point` DECIMAL(10,2) unsigned NOT NULL default 0,
		`changed_point` DECIMAL(10,2) NOT NULL default 0,
		`is_on_hold` TINYINT(4) NULL default 0,
		`hold_point` DECIMAL(10,2) NULL default 0,
		`expiration_at` datetime NULL,	
		`created_at` datetime NULL,
		`updated_at` datetime NULL,
		`order_id` int(10) unsigned NULL,
		`receiver_email` varchar(255) NULL,
		`receiver_customer_id` INT unsigned NULL,
		`recipient_transaction_id` int(10) unsigned,
		`point_back` FLOAT,
		`order_filter_ids` text,
		PRIMARY KEY (`transaction_id`)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8;	

	ALTER TABLE {$this->getTable('affiliatepoints/customer')} ADD UNIQUE `unique_affiliatepoints_index`(`customer_id`);

");

$installer->getConnection()->addColumn($this->getTable('sales/order'), 'affiliatepoints_earn', 'decimal(12,4) NOT NULL default 0');
$installer->getConnection()->addColumn($this->getTable('sales/order'), 'affiliatepoints_spent', 'decimal(12,4) NOT NULL default 0');
$installer->getConnection()->addColumn($this->getTable('sales/order'), 'affiliatepoints_discount', 'decimal(12,4) NULL');
$installer->getConnection()->addColumn($this->getTable('sales/order'), 'base_affiliatepoints_discount', 'decimal(12,4) NULL');

$installer->getConnection()->addColumn($this->getTable('sales/order_item'), 'affiliatepoints_earn', 'decimal(12,4) NOT NULL default 0');
$installer->getConnection()->addColumn($this->getTable('sales/order_item'), 'affiliatepoints_spent', 'decimal(12,4) NOT NULL default 0');
$installer->getConnection()->addColumn($this->getTable('sales/order_item'), 'affiliatepoints_discount', 'decimal(12,4) NULL');
$installer->getConnection()->addColumn($this->getTable('sales/order_item'), 'base_affiliatepoints_discount', 'decimal(12,4) NULL');

$installer->getConnection()->addColumn($this->getTable('sales/invoice'), 'affiliatepoints_earn', 'decimal(12,4) NOT NULL default 0');
$installer->getConnection()->addColumn($this->getTable('sales/invoice'), 'affiliatepoints_discount', 'decimal(12,4) NULL');
$installer->getConnection()->addColumn($this->getTable('sales/invoice'), 'base_affiliatepoints_discount', 'decimal(12,4) NULL');

$installer->getConnection()->addColumn($this->getTable('sales/creditmemo'), 'affiliatepoints_earn', 'decimal(12,4) NOT NULL default 0');
$installer->getConnection()->addColumn($this->getTable('sales/creditmemo'), 'affiliatepoints_discount', 'decimal(12,4) NULL');
$installer->getConnection()->addColumn($this->getTable('sales/creditmemo'), 'base_affiliatepoints_discount', 'decimal(12,4) NULL');

$installer->endSetup(); 