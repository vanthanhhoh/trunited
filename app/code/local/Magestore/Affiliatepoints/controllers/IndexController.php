<?php

class Magestore_Affiliatepoints_IndexController extends Mage_Core_Controller_Front_Action
{
	public function indexAction(){
		$this->loadLayout();
		$this->renderLayout();
	}

	public function accountAction(){
	    Mage::helper('affiliatepoints')->synchronizeAccount();
    }
}
