<?php
/**
 * Created by PhpStorm.
 * User: longvuxuan
 * Date: 8/30/17
 * Time: 11:21 AM
 */

class Magestore_TruGiftCard_Block_Adminhtml_System_Config_Form_Fieldset_Promotion extends
    Mage_Adminhtml_Block_System_Config_Form_Field_Array_Abstract
{
    public function _prepareToRender()
    {
        $this->addColumn('product_promotion_sku', array(
            'label' => Mage::helper('trugiftcard')->__('Product (SKU)'),
            'style' => 'width:300px',
        ));

        $this->addColumn('product_promotion_tgc', array(
            'label' => Mage::helper('trugiftcard')->__('Trunited Gift Card Award'),
            'style' => 'width:300px',
        ));

        $this->_addAfter = false;
        $this->_addButtonLabel = Mage::helper('trugiftcard')->__('Add');
    }
}
