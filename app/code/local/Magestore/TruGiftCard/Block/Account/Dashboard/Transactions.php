<?php
/**
 * Magestore
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the Magestore.com license that is
 * available through the world-wide-web at this URL:
 * http://www.magestore.com/license-agreement.html
 * 
 * DISCLAIMER
 * 
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 * 
 * @category    Magestore
 * @package     Magestore_RewardPoints
 * @copyright   Copyright (c) 2012 Magestore (http://www.magestore.com/)
 * @license     http://www.magestore.com/license-agreement.html
 */

/**
 * Rewardpoints Account Dashboard Recent Transactions
 * 
 * @category    Magestore
 * @package     Magestore_RewardPoints
 * @author      Magestore Developer
 */
class Magestore_TruGiftCard_Block_Account_Dashboard_Transactions extends Mage_Core_Block_Template
{
    protected function _construct()
    {
        parent::_construct();
        $status = $this->getStatusParam();
        $customerId = Mage::getSingleton('customer/session')->getCustomerId();
        $collection = Mage::getResourceModel('trugiftcard/transaction_collection')
            ->addFieldToFilter('customer_id', $customerId);
        if($status != '')
        {
            $collection->addFieldToFilter('status', $status);
        }

        /*$collection->getSelect()->limit(7)
            ->order('created_time DESC');*/
        $collection->setOrder('created_time','DESC');
        $collection->setOrder('transaction_id','DESC');
        $this->setCollection($collection);
    }
    public function getStatusParam()
    {
        return $this->getRequest()->getParam('status');
    }
    
    public function _prepareLayout() {
        parent::_prepareLayout();
        $pager = $this->getLayout()->createBlock('page/html_pager', 'transactions_pager')
                ->setCollection($this->getCollection());
        $this->setChild('transactions_pager', $pager);
        return $this;
    }

    public function getPagerHtml() {
        return $this->getChildHtml('transactions_pager');
    }
}
