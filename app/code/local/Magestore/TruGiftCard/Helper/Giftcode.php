<?php

class Magestore_TruGiftCard_Helper_Giftcode extends Mage_Core_Helper_Abstract
{
    const XML_PATH_EMAIL_NEW_CODE_FROM_CANCELED_ORDER = 'trugiftcard/email/new_code_from_canceld_order';
    const XML_PATH_EMAIL_NEW_CODE_FROM_REFUNDED_ORDER = 'trugiftcard/email/new_code_from_refunded_order';

    public function loadSaleRuleByCoupon($code)
    {
        $oCoupon = Mage::getModel('salesrule/coupon')->load($code, 'code');

        if($oCoupon != null && $oCoupon->getId()){
            $oRule = Mage::getModel('salesrule/rule')->load($oCoupon->getRuleId());

            if($oRule != null && $oRule->getId())
                return $oRule;
        }

        return null;
    }

    /**
     * @return string
     */
    public function generateRandomString()
    {
        $length = Mage::getStoreConfig('trugiftcard/sharegiftcard/length_code') != null ? Mage::getStoreConfig('trugiftcard/sharegiftcard/length_code') : 7;
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return strtoupper($randomString);
    }

    public function createCouponCode($data)
    {
        try {
            /** @var $model Mage_SalesRule_Model_Rule */
            $model = Mage::getModel('salesrule/rule');
            $data = array(
                'product_ids' => null,
                'name' => $data['name'],
                'description' => $data['description'],
                'is_active' => 1,
                'website_ids' => array(1),
                'customer_group_ids' => array(0, 1, 2, 3, 5, 6, 7, 8),
                'coupon_type' => 2,
                'coupon_code' => $this->generateRandomString(),
                'uses_per_coupon' => null,
                'uses_per_customer' => null,
                'from_date' => isset($data['from_date']) ? $data['from_date'] : '',
                'to_date' =>  isset($data['to_date']) ? $data['to_date'] : '',
                'sort_order' => null,
                'is_rss' => 0,
                'simple_action' => 'cart_fixed',
                'discount_amount' => $data['amount'],
                'discount_qty' => 0,
                'discount_step' => null,
                'apply_to_shipping' => 1,
                'simple_free_shipping' => 0,
                'stop_rules_processing' => 0,
                'store_labels' => array('', '')
            );

            $validateResult = $model->validateData(new Varien_Object($data));

            if ($validateResult !== true) {
                if (sizeof($validateResult) > 0)
                    return false;
            }

            $data['conditions'][1] = array(
                'type' => 'salesrule/rule_condition_combine',
                'aggregator' => 'all',
                'value' => 1,
                'new_child' => null
            );

            $data['actions'][1] = array(
                'type' => 'salesrule/rule_condition_product_combine',
                'aggregator' => 'all',
                'value' => 1,
                'new_child' => null
            );

            $model->loadPost($data);
            $model->setUseAutoGeneration(0);

            $model->save();

            if($model != null && $model->getId())
                return $model;
            else
                return false;
        } catch (Exception $e) {
            Mage::getSingleton('core/session')->addError(
                $e->getMessage()
            );
            return false;
        }
    }

    public function createGiftCode($data, $rule)
    {
        try{
            $model = Mage::getModel('trugiftcard/giftcode');

            $_data = array(
                'rule_id'   => $rule->getId(),
                'transaction_sender_id' => $data['sender_transaction_id'],
                'transaction_recipient_id' => $data['recipient_transaction_id'],
                'customer_id'   => isset($data['customer_id']) ? $data['customer_id'] : null,
                'email' => $data['email'],
                'name'  => isset($data['name']) ? $data['name'] : null,
                'coupon_code'  => $rule->getCouponCode(),
                'coupon_value'  => $rule->getDiscountAmount(),
                'remaining_value'  => $rule->getDiscountAmount(),
                'customer_is_guest'  => $data['customer_is_guest'],
                'description'  => $data['description'],
                'expiration_from'  => $data['from_date'],
                'to_date'  => $data['to_date'],
                'status'  => Magestore_TruGiftCard_Model_Status::STATUS_ENABLED,
                'can_use'  => Magestore_TruGiftCard_Model_Status::GIFT_CODE_CAN_USE_YES,
                'type'  => isset($data['type']) ? $data['type'] : Magestore_TruGiftCard_Model_Type::GIFT_CODE_TYPE_SHARING,
                'order_id'  => isset($data['order_id']) ? $data['order_id'] : null,
            );

            $model->setData($_data)->save();

            if($model != null && $model->getId())
                return $model;
            return false;
        } catch (Exception $ex) {
            return false;
        }
    }

    public function createGiftCodeAfterSharing($data)
    {
        $couponCode = $this->createCouponCode($data);
        if($couponCode != null && $couponCode->getId())
        {

            $giftCode = $this->createGiftCode($data, $couponCode);

            if($giftCode != null && $giftCode->getId())
            {
                return $giftCode;
            }
        }

        return null;
    }

    public function usedCode($data)
    {
        try{
            $history = Mage::getModel('trugiftcard/history');

            $_data = array(
                'giftcode_id'   => $data['giftcode_id'],
                'applied_amount'    => $data['applied_amount'],
                'order_id'  => isset($data['order_id']) ? $data['order_id'] : null,
                'type'  => $data['type'],
                'status'    => $data['status'],
                'customer_id'    => isset($data['customer_id']) ? $data['customer_id'] : null,
                'email'    => $data['email'],
                'name'    => $data['name'],
            );

            $history->setData($_data);

            $history->save();

        } catch (Exception $ex) {

        }
    }

    public function getGiftCodeByCode($coupon_code)
    {
        $collection = Mage::getModel('trugiftcard/giftcode')
            ->getCollection()
            ->addFieldToFilter('coupon_code', array('eq'    => $coupon_code))
            ->addFieldToFilter('status', array('eq'    => Magestore_TruGiftCard_Model_Status::GIFT_CODE_STATUS_FINISH))
//            ->addFieldToFilter('can_use', array('eq'    => Magestore_TruGiftCard_Model_Status::GIFT_CODE_CAN_USE_YES))
            ->setOrder('giftcode_id', 'desc')
            ->getFirstItem()
            ;

        if($collection != null && $collection->getId())
            return $collection;
        else
            return null;
    }

    public function updateSaleRule($rule_id, $amount)
    {
        $sale_rule = Mage::getModel('salesrule/rule')->load($rule_id);

        if($sale_rule != null && $sale_rule->getId())
        {
            $new_amount = ($sale_rule->getData('discount_amount') + $amount) > 0 ? ($sale_rule->getData('discount_amount') + $amount) : 0;

            if($new_amount == 0)
                $sale_rule->setData('is_active', 0);
            else
                $sale_rule->setData('is_active', 1);

            $sale_rule->setData('discount_amount', $new_amount);

            $sale_rule->save();

            return $sale_rule;

        } else {
            return null;
        }
    }

    public function processOrderPlaceAfter($order)
    {
        $giftCode = $this->getGiftCodeByCode($order->getCouponCode());

        if($giftCode != null)
        {
            $saleRule = $this->updateSaleRule($giftCode->getRuleId(), $order->getDiscountAmount());

            if($saleRule != null)
            {
                $giftCode->setData('remaining_value', $saleRule->getDiscountAmount());

                if($giftCode->getData('remaining_value') == 0)
                    $giftCode->setData('can_use', Magestore_TruGiftCard_Model_Status::GIFT_CODE_CAN_USE_NO);
                else
                    $giftCode->setData('can_use', Magestore_TruGiftCard_Model_Status::GIFT_CODE_CAN_USE_YES);

                $giftCode->save();

                // create history
                $_data = array(
                    'giftcode_id'   => $giftCode->getId(),
                    'applied_amount'    => $order->getDiscountAmount(),
                    'order_id'  => $order->getId(),
                    'type'  => Magestore_TruGiftCard_Model_Type::GIFT_CODE_DISCOUNT_ORDER,
                    'status'    => Magestore_TruGiftCard_Model_Status::GIFT_CODE_STATUS_FINISH,
                    'customer_id'    => $order->getCustomerId(),
                    'email'    => $order->getCustomerEmail(),
                    'name'    => $order->getCustomerFirstname().' '.$order->getCustomerLastname(),
                );

                $this->usedCode($_data);
            }
        }
    }

    public function getDiffDays($start_date, $end_date)
    {
        $difference = strtotime($end_date) - strtotime($start_date);

        return floor($difference / (60*60*24) );
    }

    public function processOrderCanceled($order)
    {
        $giftCode = $this->getGiftCodeByCode($order->getCouponCode());

        if($giftCode != null)
        {
            $amount = abs($order->getDiscountAmount());

            $expiration_date = null;

            if($giftCode->getToDate() != null)
            {
                $days = $this->getDiffDays($giftCode->getData('expiration_from'), $giftCode->getData('to_date'));
                $expiration_date = Mage::helper('trugiftcard/transaction')->addDaysToDate(now(), $days);
            }

            $_data = array(
                'name'  => Mage::helper('trugiftcard')->__('[Trugiftcard] Created by canceling %s order %s to %s',  Mage::helper('core')->currency($amount, true, false), $order->getIncrementId(),  $order->getCustomerEmail()),
                'description'   => Mage::helper('trugiftcard')->__('Created by canceling order #'.$order->getIncrementId()),
                'from_date' => Mage::helper('trugiftcard/transaction')->addDaysToDate(now(), 1, '-'),
                'to_date'   => $expiration_date,
                'email'   => $order->getCustomerEmail(),
                'amount'    => $amount,
                'sender_transaction_id' => $giftCode->getData('transaction_sender_id'),
                'recipient_transaction_id' => $giftCode->getData('transaction_recipient_id'),
                'customer_is_guest' =>  $order->getCustomerId() > 0 ? 0 : 1,
                'customer_id'   => $order->getCustomerId(),
                'order_id'  => $order->getEntityId(),
                'type'  => Magestore_TruGiftCard_Model_Type::GIFT_CODE_TYPE_NEW_FROM_CANCEL_ORDER
            );

            $giftCode = $this->createGiftCodeAfterSharing($_data);

            if($giftCode != null && $giftCode->getId())
                $this->sendEmailNewCodeFromCancelingOrder($giftCode, $order->getIncrementId());
        }
    }

    public function checkIsRefunded($order_id)
    {
        $collection = Mage::getModel('trugiftcard/giftcode')
            ->getCollection()
            ->addFieldToFilter('order_id', $order_id)
            ->getFirstItem()
            ;

        if($collection != null && $collection->getId())
            return true;
        else
            return false;
    }

    public function processOrderRefunded($order)
    {
        $is_refunded = $this->checkIsRefunded($order->getEntityId());

        if($is_refunded)
            return $this;

        $giftCode = $this->getGiftCodeByCode($order->getCouponCode());

        if($giftCode != null)
        {
            $amount = abs($order->getDiscountAmount());

            $expiration_date = null;

            if($giftCode->getToDate() != null)
            {
                $days = $this->getDiffDays($giftCode->getData('expiration_from'), $giftCode->getData('to_date'));
                $expiration_date = Mage::helper('trugiftcard/transaction')->addDaysToDate(now(), $days);
            }

            $_data = array(
                'name'  => Mage::helper('trugiftcard')->__('[Trugiftcard] Created by refunding %s order %s to %s',  Mage::helper('core')->currency($amount, true, false), $order->getIncrementId(),  $order->getCustomerEmail()),
                'description'   => Mage::helper('trugiftcard')->__('Created by refunding order #'.$order->getIncrementId()),
                'from_date' => Mage::helper('trugiftcard/transaction')->addDaysToDate(now(), 1, '-'),
                'to_date'   => $expiration_date,
                'email'   => $order->getCustomerEmail(),
                'amount'    => $amount,
                'sender_transaction_id' => $giftCode->getData('transaction_sender_id'),
                'recipient_transaction_id' => $giftCode->getData('transaction_recipient_id'),
                'customer_is_guest' =>  $order->getCustomerId() > 0 ? 0 : 1,
                'customer_id'   => $order->getCustomerId(),
                'order_id'  => $order->getEntityId(),
                'type'  => Magestore_TruGiftCard_Model_Type::GIFT_CODE_TYPE_NEW_FROM_REFUND_ORDER
            );

            $giftCode = $this->createGiftCodeAfterSharing($_data);

            if($giftCode != null && $giftCode->getId())
                $this->sendEmailNewCodeFromRefundingOrder($giftCode, $order->getIncrementId());
        }
    }

    public function sendEmailNewCodeFromCancelingOrder($giftcode, $order_id)
    {
        $store = Mage::app()->getStore();
        if (!Mage::getStoreConfigFlag(Magestore_TruGiftCard_Helper_Transaction::XML_PATH_EMAIL_ENABLE, $store->getId())) {
            return $this;
        }

        $translate = Mage::getSingleton('core/translate');
        $translate->setTranslateInline(false);

        $sender_transaction = Mage::getModel('trugiftcard/transaction')->load(
            $giftcode->getData('transaction_sender_id')
        );

        $name = Mage::helper('trugiftcard')->__('There');

        $data = array(
            'store' => $store,
            'coupon_code' => $giftcode->getCouponCode(),
            'amount' => Mage::helper('core')->currency($giftcode->getCouponValue(), true, false),
            'order_id' => $order_id,
            'sender_email' => $sender_transaction->getCustomerEmail(),
        );

        $email_path = Mage::getStoreConfig(self::XML_PATH_EMAIL_NEW_CODE_FROM_CANCELED_ORDER, $store);

        Mage::getModel('core/email_template')
            ->setDesignConfig(array(
                'area' => 'frontend',
                'store' => Mage::app()->getStore()->getId()
            ))->sendTransactional(
                $email_path,
                Mage::getStoreConfig(Magestore_TruGiftCard_Helper_Transaction::XML_PATH_EMAIL_SENDER, $store->getId()),
                $giftcode->getEmail(),
                $name,
                $data
            );

        $translate->setTranslateInline(true);

        return $this;
    }

    public function sendEmailNewCodeFromRefundingOrder($giftcode, $order_id)
    {
        $store = Mage::app()->getStore();
        if (!Mage::getStoreConfigFlag(Magestore_TruGiftCard_Helper_Transaction::XML_PATH_EMAIL_ENABLE, $store->getId())) {
            return $this;
        }

        $translate = Mage::getSingleton('core/translate');
        $translate->setTranslateInline(false);

        $sender_transaction = Mage::getModel('trugiftcard/transaction')->load(
            $giftcode->getData('transaction_sender_id')
        );

        $name = Mage::helper('trugiftcard')->__('There');

        $data = array(
            'store' => $store,
            'coupon_code' => $giftcode->getCouponCode(),
            'amount' => Mage::helper('core')->currency($giftcode->getCouponValue(), true, false),
            'order_id' => $order_id,
            'sender_email' => $sender_transaction->getCustomerEmail(),
        );

        $email_path = Mage::getStoreConfig(self::XML_PATH_EMAIL_NEW_CODE_FROM_REFUNDED_ORDER, $store);

        Mage::getModel('core/email_template')
            ->setDesignConfig(array(
                'area' => 'frontend',
                'store' => Mage::app()->getStore()->getId()
            ))->sendTransactional(
                $email_path,
                Mage::getStoreConfig(Magestore_TruGiftCard_Helper_Transaction::XML_PATH_EMAIL_SENDER, $store->getId()),
                $giftcode->getEmail(),
                $name,
                $data
            );

        $translate->setTranslateInline(true);

        return $this;
    }

    public function isCanCancel($sender_transaction)
    {
        if($sender_transaction->getStatus() != Magestore_TruGiftCard_Model_Status::STATUS_TRANSACTION_PENDING)
            return false;

        if($sender_transaction->getData('action_type') == Magestore_TruGiftCard_Model_Type::TYPE_TRANSACTION_SHARING){

            $giftCode_collection = Mage::getModel('trugiftcard/giftcode')
                ->getCollection()
                ->addFieldToSelect('giftcode_id')
                ->addFieldToSelect('coupon_code')
                ->addFieldToFilter('transaction_sender_id', $sender_transaction->getId())
                ->setOrder('giftcode_id', 'desc')
                ;

            if(sizeof($giftCode_collection) > 0)
            {
                $giftCodes = $giftCode_collection->getColumnValues('giftcode_id');
                $histories = Mage::getModel('trugiftcard/history')
                    ->getCollection()
                    ->addFieldToSelect('history_id')
                    ->addFieldToSelect('giftcode_id')
                    ->addFieldToSelect('type')
                    ->addFieldToFilter('giftcode_id', array('in'    => $giftCodes))
                    ;

                if(sizeof($histories) > 0)
                {
                    $is_cancel = true;
                    foreach ($histories as $history) {
                        if($history->getType() == Magestore_TruGiftCard_Model_Type::GIFT_CODE_HISTORY_REDEEM_BALANCE)
                        {
                            $is_cancel = false;
                            break;
                        }
                    }

                    if(!$is_cancel)
                        return false;
                }

                $codes = $giftCode_collection->getColumnValues('coupon_code');

                $orders_collection = Mage::getModel('sales/order')
                    ->getCollection()
                    ->addAttributeToFilter('coupon_code', array('in'    => $codes))
                    ->addAttributeToFilter('status', array('nin' => array(
                        Mage_Sales_Model_Order::STATE_CANCELED,
                        Mage_Sales_Model_Order::STATE_CLOSED,
                    )))
                    ->getFirstItem()
                    ;

                if($orders_collection != null && $orders_collection->getId())
                    return false;
            }
        }

        return true;
    }

}
