<?php

class Magestore_TruGiftCard_Model_Type extends Varien_Object
{
	const TYPE_TRANSACTION_BY_ADMIN	= 1;
	const TYPE_TRANSACTION_SHARING	= 2;
	const TYPE_TRANSACTION_RECEIVE_FROM_SHARING	= 3;
	const TYPE_TRANSACTION_TRANSFER	= 4;
	const TYPE_TRANSACTION_PAYOUT_EARNING	= 5;
	const TYPE_TRANSACTION_CANCEL_ORDER	= 6;
	const TYPE_TRANSACTION_CHECKOUT_ORDER = 7;
	const TYPE_TRANSACTION_REFUND_ORDER = 8;
	const TYPE_TRANSACTION_PURCHASE_GIFT_CARD = 9;
	const TYPE_TRANSACTION_RECEIVE_REWARD_FROM_PROMOTION = 10;
	const TYPE_TRANSACTION_RECEIVE_REWARD_FROM_REFERRED_PROMOTION = 11;
	const TYPE_TRANSACTION_RECEIVE_REWARD_FROM_LOGIN_EVENT = 12;
	const TYPE_TRANSACTION_RECEIVE_REWARD_FROM_REFERRED_LOGIN_EVENT = 13;
    const TYPE_TRANSACTION_RECEIVE_REWARD_FROM_NEW_REGISTRATION_EVENT = 14;
    const TYPE_TRANSACTION_RECEIVE_REWARD_FROM_REFERRED_NEW_REGISTRATION_EVENT = 14;
    const TYPE_TRANSACTION_RECEIVE_REWARD_FROM_PURCHASED_PRODUCT_PROMOTION = 15;
    const TYPE_TRANSACTION_REDEEM_BY_USING_GIFT_CARD_CODE = 16;

    const GIFT_CODE_TYPE_SHARING = 1;
    const GIFT_CODE_TYPE_NEW_FROM_REFUND_ORDER = 2;
    const GIFT_CODE_TYPE_NEW_FROM_CANCEL_ORDER = 3;

    const GIFT_CODE_HISTORY_REDEEM_BALANCE = 1;
    const GIFT_CODE_DISCOUNT_ORDER = 2;

	static public function getOptionArray(){
		return array(
			self::TYPE_TRANSACTION_BY_ADMIN	=> Mage::helper('trugiftcard')->__('Changed by Admin'),
			self::TYPE_TRANSACTION_SHARING   => Mage::helper('trugiftcard')->__('Sharing Trunited Gift Card'),
			self::TYPE_TRANSACTION_RECEIVE_FROM_SHARING   => Mage::helper('trugiftcard')->__('Received Trunited Gift Card from sharing'),
			self::TYPE_TRANSACTION_TRANSFER   => Mage::helper('trugiftcard')->__('Transfer dollars from balance to truGiftCard'),
			self::TYPE_TRANSACTION_PAYOUT_EARNING   => Mage::helper('trugiftcard')->__('Payout earnings'),
			self::TYPE_TRANSACTION_CANCEL_ORDER   => Mage::helper('trugiftcard')->__('Retrieve Trunited Gift Card balance spent on cancelled order'),
			self::TYPE_TRANSACTION_CHECKOUT_ORDER   => Mage::helper('trugiftcard')->__('Spend Trunited Gift Card balance to purchase order'),
			self::TYPE_TRANSACTION_REFUND_ORDER   => Mage::helper('trugiftcard')->__('Retrieve Trunited Gift Card balance spent on refunded order'),
			self::TYPE_TRANSACTION_PURCHASE_GIFT_CARD   => Mage::helper('trugiftcard')->__('Purchased Trunited Gift Card on order'),
			self::TYPE_TRANSACTION_RECEIVE_REWARD_FROM_PROMOTION   => Mage::helper('trugiftcard')->__('Received Trunited Gift Card rewards from promotion'),
			self::TYPE_TRANSACTION_RECEIVE_REWARD_FROM_REFERRED_PROMOTION   => Mage::helper('trugiftcard')->__('Received Trunited Gift Card rewards from referring the customers on the promotion'),
			self::TYPE_TRANSACTION_RECEIVE_REWARD_FROM_LOGIN_EVENT   => Mage::helper('trugiftcard')->__('Received Trunited Gift Card rewards from Login Event'),
			self::TYPE_TRANSACTION_RECEIVE_REWARD_FROM_REFERRED_LOGIN_EVENT   => Mage::helper('trugiftcard')->__('Received Trunited Gift Card rewards from referring the customers on the Login Event'),
            self::TYPE_TRANSACTION_RECEIVE_REWARD_FROM_NEW_REGISTRATION_EVENT   => Mage::helper('trugiftcard')->__('Received Trunited Gift Card rewards from New Registration Event'),
            self::TYPE_TRANSACTION_RECEIVE_REWARD_FROM_REFERRED_NEW_REGISTRATION_EVENT   => Mage::helper('trugiftcard')->__('Received Trunited Gift Card rewards from referring the customers on the New Registration Event'),
			self::TYPE_TRANSACTION_RECEIVE_REWARD_FROM_PURCHASED_PRODUCT_PROMOTION   => Mage::helper('trugiftcard')->__('Received Trunited Gift Card rewards from purchasing the product promotion'),
			self::TYPE_TRANSACTION_REDEEM_BY_USING_GIFT_CARD_CODE   => Mage::helper('trugiftcard')->__('Redeemed Trunited Gift Card'),
		);
	}
	
	static public function getOptionHash(){
		$options = array();
		foreach (self::getOptionArray() as $value => $label)
			$options[] = array(
				'value'	=> $value,
				'label'	=> $label
			);
		return $options;
	}

	static public function getDataType()
	{
		return array(
			self::TYPE_TRANSACTION_BY_ADMIN,
			self::TYPE_TRANSACTION_SHARING,
			self::TYPE_TRANSACTION_RECEIVE_FROM_SHARING,
			self::TYPE_TRANSACTION_TRANSFER,
			self::TYPE_TRANSACTION_PAYOUT_EARNING,
		);
	}

	static public function getHistoryOption()
    {
        return array(
            self::GIFT_CODE_HISTORY_REDEEM_BALANCE => Mage::helper('trugiftcard')->__('Redeem Trunited Gift Card Balance'),
            self::GIFT_CODE_DISCOUNT_ORDER => Mage::helper('trugiftcard')->__('Apply To Discount Order'),
        );
    }

}
