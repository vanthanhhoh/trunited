<?php

class Magestore_TruGiftCard_Model_Giftcode extends Mage_Core_Model_Abstract
{
	public function _construct(){
		parent::_construct();
		$this->_init('trugiftcard/giftcode');
	}

    /**
     * Before object save manipulations
     *
     * @return Magestore_TruGiftCard_Model_Giftcode
     */
    protected function _beforeSave()
    {
        parent::_beforeSave();

        if ($this->isObjectNew()) {
            $this->setData('created_at', now());
        }

        $this->setData('updated_at', now());

        return $this;
    }
}
