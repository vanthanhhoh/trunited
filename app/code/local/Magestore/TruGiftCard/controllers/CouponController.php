<?php

/**
 * Magestore
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magestore.com license that is
 * available through the world-wide-web at this URL:
 * http://www.magestore.com/license-agreement.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Magestore
 * @package     Magestore_Storecredit
 * @module      Storecredit
 * @author      Magestore Developer
 *
 * @copyright   Copyright (c) 2016 Magestore (http://www.magestore.com/)
 * @license     http://www.magestore.com/license-agreement.html
 *
 */

/**
 * TruWallet Controller
 * 
 * @category    Magestore
 * @package     Magestore_TruGiftCard
 * @author      Magestore Developer
 */
class Magestore_TruGiftCard_CouponController extends Mage_Core_Controller_Front_Action
{
    /**
     * check customer is logged in
     */
    public function preDispatch()
    {
        parent::preDispatch();
        if (!$this->getRequest()->isDispatched()) {
            return;
        }
        $action = $this->getRequest()->getActionName();
        if ($action != 'policy' && $action != 'redirectLogin') {
            // Check customer authentication
            if (!Mage::getSingleton('customer/session')->isLoggedIn()) {
                Mage::getSingleton('customer/session')->setAfterAuthUrl(
                    Mage::getUrl($this->getFullActionName('/'))
                );
                $this->_redirect('customer/account/login');
                $this->setFlag('', Mage_Core_Controller_Varien_Action::FLAG_NO_DISPATCH, true);
            }
        }
    }

    public function indexAction()
    {
        try {
            /** @var $model Mage_SalesRule_Model_Rule */
            $model = Mage::getModel('salesrule/rule');
            $data = array(
                'product_ids'   => '',
                'name'  => 'Test1',
                'description'   => 'Test description',
                'is_active' => 1,
                'website_ids'   => array(Mage::app()->getWebsite()->getId()),
                'customer_group_ids' => array(0,1,2,3,5,6,7,8),
                'coupon_type'   => 2,
                'coupon_code'   => 'ZZZ1',
                'uses_per_coupon'   => '',
                'uses_per_customer' => '',
                'from_date' => '2017-12-01',
                'to_date'   => '2017-12-30',
                'sort_order'    => '',
                'is_rss'    => 0,
                'rule'  => array(
                    'conditions'    => array(
                        array(
                            'type'  => 'salesrule/rule_condition_combine',
                            'aggregator'    => 'all',
                            'value' => 1,
                            'new_child' => '',
                        )
                    ),

                    'actions' => array(
                        array(
                            'type'  => 'salesrule/rule_condition_product_combine',
                            'aggregator'    => 'all',
                            'value' => 1,
                            'new_child' => '',
                        )
                    )
                ),
                'simple_action' => 'by_fixed',
                'discount_amount'   => 34.6,
                'discount_qty'  => 0,
                'discount_step' => '',
                'apply_to_shipping' => 0,
                'simple_free_shipping'  => 0,
                'stop_rules_processing' => 0,
                'store_labels'  => array('', '')
            );

            $validateResult = $model->validateData(new Varien_Object($data));
            if ($validateResult !== true) {
                foreach($validateResult as $errorMessage) {
                    zend_debug::dump($errorMessage);
                }
                exit;
            }

            if (isset($data['simple_action']) && $data['simple_action'] == 'by_percent'
            && isset($data['discount_amount'])) {
                $data['discount_amount'] = min(100,$data['discount_amount']);
            }
            if (isset($data['rule']['conditions'])) {
                $data['conditions'] = $data['rule']['conditions'];
            }
            if (isset($data['rule']['actions'])) {
                $data['actions'] = $data['rule']['actions'];
            }
            unset($data['rule']);
            $model->loadPost($data);

            $useAutoGeneration = (int)!empty($data['use_auto_generation']);
            $model->setUseAutoGeneration($useAutoGeneration);

            $model->save();
            zend_debug::dump('SUCCESS');
            zend_debug::dump($model->debug());
            return;
        } catch (Mage_Core_Exception $e) {
            zend_debug::dump($e->getMessage());
        } catch (Exception $e) {
            zend_debug::dump($e->getMessage());
        }
    }

    public function redeemAction()
    {
        if($this->getRequest()->isPost()){
            $couponCode = $this->getRequest()->getParam('giftcode');

            $giftCode_collection = Mage::getModel('trugiftcard/giftcode')->getCollection()
                    ->addFieldToFilter('coupon_code', array('like'  => '%'.$couponCode.'%'))
                    ->setOrder('giftcode_id', 'desc')
                    ->getFirstItem()
                ;

            if($giftCode_collection != null && $giftCode_collection->getId())
            {
                if($giftCode_collection->getStatus() == Magestore_TruGiftCard_Model_Status::STATUS_DISABLED
                || $giftCode_collection->getCanUse() == Magestore_TruGiftCard_Model_Status::GIFT_CODE_CAN_USE_NO){
                    Mage::getSingleton('core/session')->addError(
                        Mage::helper('trugiftcard')->__('You can not use this gift card code because it has been disabled')
                    );
                    $this->_redirectUrl(Mage::getUrl('trugiftcard'));
                    return;
                }

                $customer = Mage::getModel('customer/customer')->load(
                    Mage::getSingleton('customer/session')->getCustomerId()
                );

                $sender_transaction = Mage::getModel('trugiftcard/transaction')->load(
                    $giftCode_collection->getData('transaction_sender_id')
                );

                if($sender_transaction != null && $sender_transaction->getId())
                {
                    if($sender_transaction->getCustomerId() == $customer->getId())
                    {
                        Mage::getSingleton('core/session')->addError(
                            Mage::helper('trugiftcard')->__('You can not use this gift card code. Please try another code!')
                        );
                        $this->_redirectUrl(Mage::getUrl('trugiftcard'));
                        return;
                    }
                }

                $truGiftCardAccount = null;

                $connection = Mage::getSingleton('core/resource')->getConnection('core_write');
                try {
                    $connection->beginTransaction();

                    $applied_amount = $giftCode_collection->getRemainingValue();

                    $truGiftCardAccount = Mage::helper('trugiftcard/account')->updateCredit(
                        $customer->getId(),
                        $applied_amount
                    );

                    $params = array(
                        'credit' => $applied_amount,
                        'title' => Mage::helper('trugiftcard')->__('Gift Card Code <b>%s</b>', $couponCode),
                    );

                    Mage::helper('trugiftcard/transaction')->createTransaction(
                        $truGiftCardAccount,
                        $params,
                        Magestore_TruGiftCard_Model_Type::TYPE_TRANSACTION_REDEEM_BY_USING_GIFT_CARD_CODE,
                        Magestore_TruGiftCard_Model_Status::STATUS_TRANSACTION_COMPLETED
                    );

                    $new_remaining = ($giftCode_collection->getRemainingValue() - $applied_amount) > 0 ?  ($giftCode_collection->getRemainingValue() - $applied_amount) : 0;

                    $giftCode_collection->setData('remaining_value', $new_remaining);
                    $giftCode_collection->setData('updated_at', now());

                    if($new_remaining <= 0){
                        $giftCode_collection->setData('can_use', Magestore_TruGiftCard_Model_Status::GIFT_CODE_CAN_USE_NO);

                        $saleRule = Mage::getModel('salesrule/rule')->load($giftCode_collection->getRuleId());

                        if($saleRule != null && $saleRule->getId())
                        {
                            $saleRule->setData('is_active', 0);
                            $saleRule->save();
                        }
                    }
                    $giftCode_collection->save();

                    $_data = array(
                        'giftcode_id'  => $giftCode_collection->getId(),
                        'applied_amount'    => $applied_amount,
                        'type'  => Magestore_TruGiftCard_Model_Type::GIFT_CODE_HISTORY_REDEEM_BALANCE,
                        'status'    => Magestore_TruGiftCard_Model_Status::GIFT_CODE_STATUS_FINISH,
                        'customer_id'   => $customer->getId(),
                        'email' => $customer->getEmail(),
                        'name'  => $customer->getName()
                    );
                    Mage::helper('trugiftcard/giftcode')->usedCode($_data);

                    $connection->commit();
                } catch (Exception $e) {
                    $connection->rollback();
                    Mage::getSingleton('core/session')->addError(
                        Mage::helper('trugiftcard')->__($e->getMessage())
                    );
                }


                if($truGiftCardAccount != null){
                    Mage::getSingleton('core/session')->addSuccess(
                        Mage::helper('trugiftcard')->__('Congrats! You have just applied  <b>' . $couponCode . '</b> successfully!')
                    );
                    $this->_redirectUrl(Mage::getUrl('trugiftcard'));
                    return;
                }
            }
        }

        Mage::getSingleton('core/session')->addError(
            Mage::helper('trugiftcard')->__('Something was wrong with this action. Please try again!')
        );
        $this->_redirectUrl(Mage::getUrl('trugiftcard'));
    }
}
