<?php

class Magestore_TruGiftCard_ReportController extends Mage_Core_Controller_Front_Action
{
	public function indexAction(){
		$this->loadLayout();
		$this->_title(Mage::helper('trugiftcard')->__('My Trunited Gift Card'));
		$this->renderLayout();
	}

	public function checkAction()
	{
		$helper = Mage::helper('trugiftcard/transaction');
		$collection = Mage::getModel('trugiftcard/transaction')->getCollection()
			->addFieldToFilter('action_type', Magestore_TruGiftCard_Model_Type::TYPE_TRANSACTION_SHARING)
			->addFieldToFilter('status', Magestore_TruGiftCard_Model_Status::STATUS_TRANSACTION_CANCELLED)
			->addFieldToFilter('created_time', array(
				'from' => '2017-11-12 00:00:00',
				'to'	=> date('Y-m-d 23:59:59', time()))
			)
			->setOrder('transaction_id', 'desc')
		;

		echo $collection->getSelect();
		zend_debug::dump(sizeof($collection));

        $correct_data = array();
		if (sizeof($collection) > 0) {

			foreach ($collection as $transaction) {

				if (true) {
					/* user still dont register an new account */
					if ($transaction->getReceiverCustomerId() == 0) {
						if($transaction->getPointBack() == abs($transaction->getChangedCredit())){
						    $correct_data[] = array(
                                'is_correct'    => true,
                                'transaction_id'    => $transaction->getId(),
                                'sender_id' => $transaction->getCustomerId(),
                                'sender_email'  => $transaction->getCustomerEmail(),
                                'shared_amount'   => $transaction->getChangedCredit(),
                                'returned_amount'   => $transaction->getPointBack(),
                                'recipient_used_amount'   => 0,
                                'wrong_amount'   => 0,
                                'correct_amount'   => 0,
                                'sent_at'   => $transaction->getCreatedTime(),
                                'expiration_at' => $transaction->getExpirationDate(),
                                'recipient_email'   =>  $transaction->getReceiverEmail(),
                                'recipient_id'   =>  $transaction->getReceiverCustomerId(),
                                'order'   =>  $transaction->getData('order_filter_ids'),
                            );
                        } else {
						    zend_debug::dump('Guest is not correct!');
						    exit;
                        }
					} /* User created an new account and check the amount of truGiftCard in order to return back */
					else {
						$orders = $helper->getCollectionOrderByCustomer(
							$transaction->getReceiverCustomerId(),
							strtotime($transaction->getCreatedTime()),
							$transaction
						);

						$truGiftCard_used = 0;
						$order_filter_ids = array();
						if ($orders != null && sizeof($orders) > 0) {
							foreach ($orders as $order) {
								$truGiftCard_used += $order->getTrugiftcardDiscount();
								$order_filter_ids[] = $order->getEntityId();
							}
						}

						if ($truGiftCard_used >= abs($transaction->getChangedCredit())) {
							// the recipient used all shared amount
                            $correct_data[] = array(
                                'is_correct'    => $transaction->getData('point_back') == 0 ? true : false,
                                'transaction_id'    => $transaction->getId(),
                                'sender_id' => $transaction->getCustomerId(),
                                'sender_email'  => $transaction->getCustomerEmail(),
                                'shared_amount'   => $transaction->getChangedCredit(),
                                'returned_amount'   => $transaction->getPointBack(),
                                'recipient_used_amount'   => $transaction->getPointBack(),
                                'wrong_amount'   => $transaction->getPointBack(),
                                'correct_amount'   => 0,
                                'sent_at'   => $transaction->getCreatedTime(),
                                'expiration_at' => $transaction->getExpirationDate(),
                                'recipient_email'   =>  $transaction->getReceiverEmail(),
                                'recipient_id'   =>  $transaction->getReceiverCustomerId(),
                                'order'   =>  $transaction->getData('order_filter_ids'),
                            );

						} else {
						    // the recipient has not used all the shared amount
							$return_points = abs($transaction->getChangedCredit()) - $truGiftCard_used;

                            $correct_data[] = array(
                                'is_correct'    => $transaction->getData('point_back') == $return_points ? true : false,
                                'transaction_id'    => $transaction->getId(),
                                'sender_id' => $transaction->getCustomerId(),
                                'sender_email'  => $transaction->getCustomerEmail(),
                                'shared_amount'   => $transaction->getChangedCredit(),
                                'returned_amount'   => $transaction->getPointBack(),
                                'recipient_used_amount'   => $truGiftCard_used,
                                'wrong_amount'   => $transaction->getPointBack(),
                                'correct_amount'   => $return_points,
                                'sent_at'   => $transaction->getCreatedTime(),
                                'expiration_at' => $transaction->getExpirationDate(),
                                'recipient_email'   =>  $transaction->getReceiverEmail(),
                                'recipient_id'   =>  $transaction->getReceiverCustomerId(),
                                'order'   =>  $transaction->getData('order_filter_ids'),
                            );

						}
					}
				}
			}
		}

        $file = Mage::getBaseDir('media') . DS . 'trugiftcard'. DS .'sharingTGC.csv';

        $mage_csv = new Varien_File_Csv();

        $rs[] = array(
            'is_correct'    => 'Is Correct',
            'transaction_id'    => 'Transaction ID',
            'sender_id' => 'Sender ID',
            'sender_email'  => 'Sender Email',
            'shared_amount'   => 'Shared Amount',
            'returned_amount'   => 'Returned back Amount',
            'recipient_used_amount'   => 'Recipient Used Amount',
            'wrong_amount'   => 'Wrong Amount',
            'correct_amount'   => 'Correct Amount',
            'sent_at'   => 'Shared At',
            'expiration_at' => 'Expiration At',
            'recipient_email'   =>  'Recipient Email',
            'recipient_id'   =>  'Recipient ID',
            'order'   =>  'Order'
        );

//        $_data = $mage_csv->getData($file);

        zend_debug::dump($correct_data);

        $rs[] = $correct_data;

        $mage_csv->saveData($file, $rs);

		zend_debug::dump('----------RESULT----------------');
		echo 'Success';
	}
}
