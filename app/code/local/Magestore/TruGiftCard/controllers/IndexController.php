<?php

class Magestore_TruGiftCard_IndexController extends Mage_Core_Controller_Front_Action
{
	public function indexAction(){


		$this->loadLayout();
		$this->_title(Mage::helper('trugiftcard')->__('My Trunited Gift Card'));
		$this->renderLayout();
	}

	public function updateDbAction(){
		$setup = new Mage_Core_Model_Resource_Setup();
		$installer = $setup;
		$installer->startSetup();
		$installer->run("
			  DROP TABLE IF EXISTS {$setup->getTable('trugiftcard/customer')};
			  DROP TABLE IF EXISTS {$setup->getTable('trugiftcard/transaction')};

			  CREATE TABLE {$setup->getTable('trugiftcard/customer')} (
				`trugiftcard_id` int(11) unsigned NOT NULL auto_increment,
				`customer_id` int(10) unsigned NOT NULL,
				`trugiftcard_credit` DECIMAL(10,2) unsigned NOT NULL default 0,
				`created_time` datetime NULL,
				`updated_time` datetime NULL,
				PRIMARY KEY (`trugiftcard_id`)
			  ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

			  CREATE TABLE {$setup->getTable('trugiftcard/transaction')} (
				`transaction_id` int(10) unsigned NOT NULL auto_increment,
				`trugiftcard_id` int(10) unsigned NULL,
				`customer_id` int(10) unsigned NULL,
				`customer_email` varchar(255) NOT NULL,
				`title` varchar(255) NOT NULL,
				`action_type` smallint(5) NOT NULL default '0',
				`store_id` smallint(5) NOT NULL,
				`status` smallint(5) NOT NULL,
				`created_time` datetime NULL,
				`updated_time` datetime NULL,
				`expiration_date` datetime NULL,
				`order_id` int(10) unsigned NULL,
				`current_credit` DECIMAL(10,2) unsigned NOT NULL default 0,
				`changed_credit` DECIMAL(10,2) NOT NULL default 0,
				`receiver_email` varchar(255) NULL,
				`receiver_customer_id` INT unsigned NULL,
				`recipient_transaction_id` int(10) unsigned,
				`point_back` FLOAT,
				`order_filter_ids` text,
				PRIMARY KEY (`transaction_id`)
			  ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
        ");
		$installer->endSetup();
		echo "success";
	}

	public function updateDb2Action(){
		$setup = new Mage_Core_Model_Resource_Setup();
		$installer = $setup;
		$installer->startSetup();
		$installer->run("");

		$installer->getConnection()->addColumn($setup->getTable('sales/order'), 'trugiftcard_earn', 'int(11) NOT NULL default 0');
		$installer->getConnection()->addColumn($setup->getTable('sales/order'), 'trugiftcard_spent', 'int(11) NOT NULL default 0');
		$installer->getConnection()->addColumn($setup->getTable('sales/order'), 'trugiftcard_base_discount', 'decimal(12,4) NOT NULL default 0');
		$installer->getConnection()->addColumn($setup->getTable('sales/order'), 'trugiftcard_discount', 'decimal(12,4) NOT NULL default 0');
		$installer->getConnection()->addColumn($setup->getTable('sales/order'), 'trugiftcard_base_amount', 'decimal(12,4) NOT NULL default 0');
		$installer->getConnection()->addColumn($setup->getTable('sales/order'), 'trugiftcard_amount', 'decimal(12,4) NOT NULL default 0');

		$installer->getConnection()->addColumn($setup->getTable('sales/order_item'), 'trugiftcard_earn', 'int(11) NOT NULL default 0');
		$installer->getConnection()->addColumn($setup->getTable('sales/order_item'), 'trugiftcard_spent', 'int(11) NOT NULL default 0');
		$installer->getConnection()->addColumn($setup->getTable('sales/order_item'), 'trugiftcard_base_discount', 'decimal(12,4) NOT NULL default 0');
		$installer->getConnection()->addColumn($setup->getTable('sales/order_item'), 'trugiftcard_discount', 'decimal(12,4) NOT NULL default 0');

		$installer->getConnection()->addColumn($setup->getTable('sales/invoice'), 'trugiftcard_base_discount', 'decimal(12,4) NOT NULL default 0');
		$installer->getConnection()->addColumn($setup->getTable('sales/invoice'), 'trugiftcard_discount', 'decimal(12,4) NOT NULL default 0');
		$installer->getConnection()->addColumn($setup->getTable('sales/creditmemo'), 'trugiftcard_base_discount', 'decimal(12,4) NOT NULL default 0');
		$installer->getConnection()->addColumn($setup->getTable('sales/creditmemo'), 'trugiftcard_discount', 'decimal(12,4) NOT NULL default 0');

		$installer->getConnection()->addColumn($setup->getTable('sales/order'), 'trugiftcard_discount', 'decimal(12,4) NULL');
		$installer->getConnection()->addColumn($setup->getTable('sales/order'), 'base_trugiftcard_discount', 'decimal(12,4) NULL');
		$installer->getConnection()->addColumn($setup->getTable('sales/order'), 'base_trugiftcard_discount_for_shipping', 'decimal(12,4) NULL');
		$installer->getConnection()->addColumn($setup->getTable('sales/order'), 'trugiftcard_discount_for_shipping', 'decimal(12,4) NULL');
		$installer->getConnection()->addColumn($setup->getTable('sales/order'), 'base_trugiftcard_hidden_tax', 'decimal(12,4) NULL');
		$installer->getConnection()->addColumn($setup->getTable('sales/order'), 'trugiftcard_hidden_tax', 'decimal(12,4) NULL');
		$installer->getConnection()->addColumn($setup->getTable('sales/order'), 'base_trugiftcard_shipping_hidden_tax', 'decimal(12,4) NULL');
		$installer->getConnection()->addColumn($setup->getTable('sales/order'), 'trugiftcard_shipping_hidden_tax', 'decimal(12,4) NULL');

		$installer->getConnection()->addColumn($setup->getTable('sales/invoice'), 'trugiftcard_discount', 'decimal(12,4) NULL');
		$installer->getConnection()->addColumn($setup->getTable('sales/invoice'), 'base_trugiftcard_discount', 'decimal(12,4) NULL');
		$installer->getConnection()->addColumn($setup->getTable('sales/invoice'), 'base_trugiftcard_hidden_tax', 'decimal(12,4) NULL');
		$installer->getConnection()->addColumn($setup->getTable('sales/invoice'), 'trugiftcard_hidden_tax', 'decimal(12,4) NULL');

		$installer->getConnection()->addColumn($setup->getTable('sales/creditmemo'), 'trugiftcard_discount', 'decimal(12,4) NULL');
		$installer->getConnection()->addColumn($setup->getTable('sales/creditmemo'), 'base_trugiftcard_discount', 'decimal(12,4) NULL');
		$installer->getConnection()->addColumn($setup->getTable('sales/creditmemo'), 'base_trugiftcard_hidden_tax', 'decimal(12,4) NULL');
		$installer->getConnection()->addColumn($setup->getTable('sales/creditmemo'), 'trugiftcard_hidden_tax', 'decimal(12,4) NULL');

		$installer->getConnection()->addColumn($setup->getTable('sales/order_item'), 'trugiftcard_discount', 'decimal(12,4) NULL');
		$installer->getConnection()->addColumn($setup->getTable('sales/order_item'), 'base_trugiftcard_discount', 'decimal(12,4) NULL');
		$installer->getConnection()->addColumn($setup->getTable('sales/order_item'), 'base_trugiftcard_hidden_tax', 'decimal(12,4) NULL');
		$installer->getConnection()->addColumn($setup->getTable('sales/order_item'), 'trugiftcard_hidden_tax', 'decimal(12,4) NULL');
		
		$installer->endSetup();
		echo "success";
	}

	public function updateDb3Action(){
		$setup = new Mage_Core_Model_Resource_Setup();
		$installer = $setup;
		$installer->startSetup();
		$installer->run("
              ALTER TABLE {$setup->getTable('trugiftcard/transaction')} ADD recipient_transaction_id int(10) unsigned;
              ALTER TABLE {$setup->getTable('trugiftcard/transaction')} ADD point_back FLOAT;
              ALTER TABLE {$setup->getTable('trugiftcard/transaction')} ADD order_filter_ids text;
        ");
		$installer->endSetup();
		echo "success";
	}


	public function transactionsAction(){
		$this->loadLayout();
		$this->_title(Mage::helper('trugiftcard')->__('Trunited Gift Card Transactions'));
		$this->renderLayout();
	}

	public function shareTruGiftCardAction(){
		if(!Mage::helper('trugiftcard')->getEnableSharing())
		{
			Mage::getSingleton('core/session')->addError(
				Mage::helper('trugiftcard')->__('The sharing Trunited Gift Card feature has been disabled.')
			);
			$this->_redirectUrl(Mage::getUrl('*/*/'));
			return;
		}
		$this->loadLayout();
		$this->_title(Mage::helper('trugiftcard')->__('Share Trunited Gift Card Money'));
		$this->renderLayout();
	}

	public function checkAction()
	{
		$helper = Mage::helper('trugiftcard/transaction');
		$collection = Mage::getModel('trugiftcard/transaction')->getCollection()
			->addFieldToFilter('action_type', Magestore_TruGiftCard_Model_Type::TYPE_TRANSACTION_SHARING)
			->addFieldToFilter('status', Magestore_TruGiftCard_Model_Status::STATUS_TRANSACTION_PENDING)
            ->addFieldToFilter('expiration_date', array('notnull' => true))
			->addFieldToFilter('expiration_date', array('lteq' => date('Y-m-d 23:59:59')))
			->setOrder('transaction_id', 'desc')
		;

		echo $collection->getSelect();
		zend_debug::dump(sizeof($collection));
		zend_debug::dump(($collection->getData()));
		exit;
		$count = 0;
		if (sizeof($collection) > 0) {

			foreach ($collection as $transaction) {
				$expiration_date = strtotime($transaction->getExpirationDate());
//				$compare_time = $helper->compareExpireDate($expiration_date, strtotime('04-08-2017 4:59:00'));

				if (true) {
					/* user still dont register an new account */
					if ($transaction->getReceiverCustomerId() == 0) {
						$helper->updateTransaction(
							$transaction,
							Magestore_TruGiftCard_Model_Status::STATUS_TRANSACTION_CANCELLED,
							abs($transaction->getChangedCredit())
						);

						$rewardAccount = Mage::helper('trugiftcard/account')->loadByCustomerId($transaction->getCustomerId());
						$rewardAccount->setTrugiftcardCredit($rewardAccount->getTrugiftcardCredit() + abs($transaction->getChangedCredit()));
						$rewardAccount->save();
					} /* User created an new account and check the amount of truGiftCard in order to return back */
					else {
						$orders = $helper->getCollectionOrderByCustomer(
							$transaction->getReceiverCustomerId(),
							strtotime($transaction->getCreatedTime()),
							$transaction
						);

						$truGiftCard_used = 0;
						$order_filter_ids = array();
						if ($orders != null && sizeof($orders) > 0) {
							foreach ($orders as $order) {
								$truGiftCard_used += $order->getTrugiftcardDiscount();
								$order_filter_ids[] = $order->getEntityId();
							}
						}

						if ($truGiftCard_used >= abs($transaction->getChangedCredit())) {
							$helper->updateTransaction(
								$transaction,
								Magestore_TruGiftCard_Model_Status::STATUS_TRANSACTION_COMPLETED,
								$order_filter_ids
							);
						} else {
							$return_points = abs($transaction->getChangedCredit()) - $truGiftCard_used;
							$rewardAccount = Mage::helper('trugiftcard/account')->loadByCustomerId($transaction->getCustomerId());
							$rewardAccount->setTrugiftcardCredit($rewardAccount->getTrugiftcardCredit() + abs($return_points));
							$rewardAccount->save();

							$receiveAccount = Mage::helper('trugiftcard/account')->loadByCustomerId($transaction->getReceiverCustomerId());
							$receiveAccount->setTrugiftcardCredit($receiveAccount->getTrugiftcardCredit() - abs($return_points));
							$receiveAccount->save();

							$helper->updateTransaction(
								$transaction,
								Magestore_TruGiftCard_Model_Status::STATUS_TRANSACTION_COMPLETED,
								$order_filter_ids,
								$return_points
							);
							$count++;
						}
					}
				}
			}
		}

		zend_debug::dump('----------RESULT----------------');
		zend_debug::dump($count);
		echo 'Success';
	}

	public function orderAction()
	{
		$orders = Mage::getModel('sales/order')->getCollection()
			->addAttributeToSelect('entity_id')
			->addAttributeToSelect('increment_id')
			->addAttributeToSelect('truwallet_discount')
			->addAttributeToSelect('trugiftcard_discount')
			->addAttributeToFilter('created_at', array(
				'from'	=> '2017-08-01',
				'to'	=> date('Y-m-d 23:59:59', time())
			))
			->addAttributeToFilter('truwallet_discount', array('gt' => 0))
			->addAttributeToFilter('trugiftcard_discount', array('gt' => 0))
//			->addAttributeToFilter('entity_id', array('from' => 24836, 'to' => 26000))
//			->addAttributeToFilter('entity_id', array('from' => 26001, 'to' => 27000))
//			->addAttributeToFilter('entity_id', array('from' => 27001, 'to' => 30000))
//			->addAttributeToFilter('entity_id', array('from' => 30001, 'to' => 32000))
//			->addAttributeToFilter('entity_id', array('from' => 32001, 'to' => 35000))
//			->addAttributeToFilter('entity_id', array('from' => 35001, 'to' => 37000))
//			->addAttributeToFilter('entity_id', array('from' => 37001, 'to' => 39000))
			->addAttributeToFilter('entity_id', array('from' => 39001, 'to' => 42000))
			->setOrder('entity_id', 'desc')
			;

		echo $orders->getSelect();
		zend_debug::dump(sizeof($orders));
//		exit;
		$rs = 0;
		if(sizeof($orders) > 0)
		{
			$transactionSave = Mage::getModel('core/resource_transaction');
			try{
				foreach ($orders as $order) {
					$items = $order->getAllItems();
					if(sizeof($items) > 0)
					{
						$baseDiscountTotal = 0;

						foreach ($items as $item) {
							if ($item->getParentItemId()) {
								continue;
							}

							if ($item->getHasChildren() && $item->isChildrenCalculated()) {
								foreach ($item->getChildren() as $child) {
									if (!$child->isDeleted()) {
										$itemDiscount = $child->getBaseRowTotal() + $child->getBaseTaxAmount() - $child->getBaseDiscountAmount() - $child->getMagestoreBaseDiscount();
										$baseDiscountTotal += $itemDiscount;
									}
								}
							} else if ($item->getProduct()) {
								if (!$item->isDeleted()) {
									$itemDiscount = $item->getBaseRowTotal() + $item->getBaseTaxAmount() - $item->getBaseDiscountAmount() - $item->getMagestoreBaseDiscount();
									$baseDiscountTotal += $itemDiscount;
								}
							}
						}

						$baseDiscount = $order->getTruwalletDiscount() + $order->getTrugiftcardDiscount();

						if ($baseDiscount < $baseDiscountTotal)
							$rate = $baseDiscount / $baseDiscountTotal;
						else {
							$rate = 1;
						}

						$rate_truwallet = $order->getTruwalletDiscount() > 0 ? $order->getTruwalletDiscount() / $baseDiscountTotal : 0;
						$rate_trugiftcard = $order->getTrugiftcardDiscount() > 0 ? $order->getTrugiftcardDiscount() / $baseDiscountTotal : 0;

						foreach ($items as $item) {
							if ($item->getParentItemId())
								continue;
							if ($item->getHasChildren() && $item->isChildrenCalculated()) {
								foreach ($item->getChildren() as $child) {
									if (!$child->isDeleted()) {
										$baseItemPrice = $child->getBaseRowTotal() + $child->getBaseTaxAmount() - $child->getBaseDiscountAmount() - $child->getMagestoreBaseDiscount();
										$itemBaseDiscount = $baseItemPrice * $rate;
										$itemDiscount = Mage::app()->getStore()->convertPrice($itemBaseDiscount);

										if ($order->getTruwalletDiscount() > 0 && $order->getTrugiftcardDiscount() == null) {

											$child->setBaseTruwalletDiscount($itemBaseDiscount)
												->setTruwalletDiscount($itemDiscount);

										} else if ($order->getTrugiftcardDiscount() > 0 && $order->getTruwalletDiscount() == null) {

											$child->setBaseTrugiftcardDiscount($itemBaseDiscount)
												->setTrugiftcardDiscount($itemDiscount);

										} else if ($order->getTrugiftcardDiscount() > 0 && $order->getTruwalletDiscount() > 0) {

											$item_discount_truwallet = $baseItemPrice * $rate_truwallet;
											$child->setBaseTruwalletDiscount($item_discount_truwallet)
												->setTruwalletDiscount(Mage::app()->getStore()->convertPrice($item_discount_truwallet));

											$item_discount_trugiftcard = $baseItemPrice * $rate_trugiftcard;
											$child->setBaseTrugiftcardDiscount($item_discount_trugiftcard)
												->setTrugiftcardDiscount(Mage::app()->getStore()->convertPrice($item_discount_trugiftcard));
										}

										$child->save();
										$rs++;
									}
								}
							} else if ($item->getProduct()) {
								if (!$item->isDeleted()) {
									$baseItemPrice = $item->getBaseRowTotal() + $item->getBaseTaxAmount() - $item->getBaseDiscountAmount() - $item->getMagestoreBaseDiscount();

									$itemBaseDiscount = $baseItemPrice * $rate;
									$itemDiscount = Mage::app()->getStore()->convertPrice($itemBaseDiscount);
									$item->setMagestoreBaseDiscount($item->getMagestoreBaseDiscount() + $itemBaseDiscount);

									if ($order->getTruwalletDiscount() > 0 && $order->getTrugiftcardDiscount() == null) {

										$item->setBaseTruwalletDiscount($itemBaseDiscount)
											->setTruwalletDiscount($itemDiscount);

									} else if ($order->getTrugiftcardDiscount() > 0 && $order->getTruwalletDiscount() == null) {

										$item->setBaseTrugiftcardDiscount($itemBaseDiscount)
											->setTrugiftcardDiscount($itemDiscount);

									} else if ($order->getTrugiftcardDiscount() > 0 && $order->getTruwalletDiscount() > 0) {

										$item_discount_truwallet = $baseItemPrice * $rate_truwallet;
										$item->setBaseTruwalletDiscount($item_discount_truwallet)
											->setTruwalletDiscount(Mage::app()->getStore()->convertPrice($item_discount_truwallet));

										$item_discount_trugiftcard = $baseItemPrice * $rate_trugiftcard;
										$item->setBaseTrugiftcardDiscount($item_discount_trugiftcard)
											->setTrugiftcardDiscount(Mage::app()->getStore()->convertPrice($item_discount_trugiftcard));
									}

									$item->save();
									$rs++;
								}
							}


						}
					}
				}
				$transactionSave->save();
				zend_debug::dump($rs);
			} catch (Exception $ex) {
				zend_debug::dump($ex->getMessage());
				exit;
			}
		}
	}
}
