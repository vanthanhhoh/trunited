<?php

class Magestore_TruGiftCard_GiftcodeController extends Mage_Core_Controller_Front_Action
{
    /**
     * check customer is logged in
     */
    public function preDispatch()
    {
        parent::preDispatch();
        if (!$this->getRequest()->isDispatched()) {
            return;
        }
        $action = $this->getRequest()->getActionName();
        if ($action != 'policy' && $action != 'redirectLogin') {
            // Check customer authentication
            if (!Mage::getSingleton('customer/session')->isLoggedIn()) {
                Mage::getSingleton('customer/session')->setAfterAuthUrl(
                    Mage::getUrl($this->getFullActionName('/'))
                );
                $this->_redirect('customer/account/login');
                $this->setFlag('', Mage_Core_Controller_Varien_Action::FLAG_NO_DISPATCH, true);
            }
        }
    }

	public function indexAction(){
		$this->loadLayout();
		$this->_title(Mage::helper('trugiftcard')->__('My Gift Card Code'));
		$this->renderLayout();
	}

	public function historyAction()
    {
        $id = $this->getRequest()->getParam('id');

        $history = Mage::getModel('trugiftcard/history')
            ->getCollection()
            ->addFieldToFilter('giftcode_id', $id)
            ->setOrder('history_id', 'desc')
            ;

        $giftCode = Mage::getModel('trugiftcard/giftcode')->load($id);

        $html = Mage::app()->getLayout()->createBlock('core/template')->setData('history', $history)->setData('code', $giftCode->getCouponCode())->setTemplate('trugiftcard/giftcode/history.phtml')->toHtml();

        echo $html;
    }

    public function completeAction()
    {
        $transaction_sender_ids = Mage::getModel('trugiftcard/giftcode')
            ->getCollection();

        $sender_ids = $transaction_sender_ids->getColumnValues('transaction_sender_id');

        $collection = Mage::getModel('trugiftcard/transaction')->getCollection()
            ->addFieldToFilter('action_type', Magestore_TruGiftCard_Model_Type::TYPE_TRANSACTION_SHARING)
            ->addFieldToFilter('status', Magestore_TruGiftCard_Model_Status::STATUS_TRANSACTION_PENDING)
            ->addFieldToFilter('transaction_id', array('nin' => $sender_ids))
//            ->addFieldToFilter('expiration_date', array('notnull' => true))
            ->setOrder('transaction_id', 'desc')
        ;

        echo $collection->getSelect();
        zend_debug::dump(sizeof($collection));

        $t = 0;
        foreach ($collection as $transaction) {
            $transaction->setUpdatedTime(now());
            $transaction->setStatus(Magestore_TruGiftCard_Model_Status::STATUS_TRANSACTION_COMPLETED);
            $transaction->setPointBack(0);
            $transaction->setOrderFilterIds(json_encode(array()));
            $transaction->save();

            $receive_transaction = Mage::getModel('trugiftcard/transaction')->load($transaction->getRecipientTransactionId());
            if ($receive_transaction != null && $receive_transaction->getId()) {
                $receive_transaction->setUpdatedTime(now());
                $receive_transaction->setStatus(Magestore_TruGiftCard_Model_Status::STATUS_TRANSACTION_COMPLETED);
                $receive_transaction->setPointBack(0);
                $receive_transaction->setOrderFilterIds(json_encode(array()));
                $receive_transaction->save();
            }

            $t++;
        }

        zend_debug::dump($t);
    }
}
