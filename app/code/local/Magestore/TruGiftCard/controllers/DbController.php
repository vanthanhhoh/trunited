<?php

class Magestore_TruGiftCard_DbController extends Mage_Core_Controller_Front_Action
{
	public function indexAction()
	{
		$setup = new Mage_Core_Model_Resource_Setup(null);
		$installer = $setup;
		$installer->startSetup();
		$installer->run("
            DROP TABLE IF EXISTS {$setup->getTable('trugiftcard/history')};
            CREATE TABLE {$setup->getTable('trugiftcard/history')} (
                `history_id` int(11) unsigned NOT NULL auto_increment,
                `giftcode_id` int(10) unsigned NULL,
                `applied_amount` DECIMAL(10,2) NULL default 0,
                `order_id` int(10) NULL,
                `type` smallint(5) NOT NULL,
                `status` smallint(5) NOT NULL,
                `created_at` datetime NULL,
                `updated_at` datetime NULL,
            PRIMARY KEY (`history_id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
            
            DROP TABLE IF EXISTS {$setup->getTable('trugiftcard/giftcode')};
            CREATE TABLE {$setup->getTable('trugiftcard/giftcode')} (
                `giftcode_id` int(11) unsigned NOT NULL auto_increment,
                `rule_id` int(11) unsigned NOT NULL,
                `transaction_sender_id` int(10) unsigned NULL,
                `transaction_recipient_id` int(10) unsigned NULL,
                `customer_id` int(10) unsigned NULL,
                `email` varchar(255) NULL,
                `name` varchar(255) NULL,
                `coupon_code` varchar(255) NULL,
                `coupon_value` DECIMAL(10,2) NULL default 0,
                `remaining_value` DECIMAL(10,2) NULL default 0,
                `customer_is_guest` smallint(5) NOT NULL,
                `description` varchar(255) NULL,
                `expiration_from` datetime NULL,
                `to_date` datetime NULL,
                `status` smallint(5) NOT NULL,
                `type` smallint(5) NOT NULL,
                `can_use` smallint(5) NOT NULL,
                `created_at` datetime NULL,
                `updated_at` datetime NULL,
            PRIMARY KEY (`giftcode_id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
        ");

		$installer->endSetup();
		echo "success";
	}

    public function index2Action()
    {
        $setup = new Mage_Core_Model_Resource_Setup(null);
        $installer = $setup;
        $installer->startSetup();
        $installer->run("
            
            ALTER  TABLE {$setup->getTable('trugiftcard/history')} ADD COLUMN `customer_id` int(11) unsigned NULL;
            ALTER  TABLE {$setup->getTable('trugiftcard/history')} ADD COLUMN `email` varchar(255) NULL;
            ALTER  TABLE {$setup->getTable('trugiftcard/history')} ADD COLUMN `name` varchar(255) NULL;
            
        ");

        $installer->endSetup();
        echo "success";
    }

    public function index3Action()
    {
        $setup = new Mage_Core_Model_Resource_Setup(null);
        $installer = $setup;
        $installer->startSetup();
        $installer->run("
            
            ALTER  TABLE {$setup->getTable('trugiftcard/giftcode')} ADD COLUMN `order_id` int(11) unsigned NULL;
            
        ");

        $installer->endSetup();
        echo "success";
    }
}
