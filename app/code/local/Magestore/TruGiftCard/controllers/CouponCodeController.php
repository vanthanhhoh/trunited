<?php

/**
 * Magestore
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magestore.com license that is
 * available through the world-wide-web at this URL:
 * http://www.magestore.com/license-agreement.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Magestore
 * @package     Magestore_Storecredit
 * @module      Storecredit
 * @author      Magestore Developer
 *
 * @copyright   Copyright (c) 2016 Magestore (http://www.magestore.com/)
 * @license     http://www.magestore.com/license-agreement.html
 *
 */

/**
 * TruWallet Controller
 * 
 * @category    Magestore
 * @package     Magestore_TruGiftCard
 * @author      Magestore Developer
 */
class Magestore_TruGiftCard_CouponCodeController extends Mage_Core_Controller_Front_Action
{
    public function indexAction()
    {
        try {
            /** @var $model Mage_SalesRule_Model_Rule */
            $model = Mage::getModel('salesrule/rule');
            $data = array(
                'product_ids'   => '',
                'name'  => 'Test1',
                'description'   => 'Test description',
                'is_active' => 1,
                'website_ids'   => array(Mage::app()->getWebsite()->getId()),
                'customer_group_ids' => array(0,1,2,3,5,6,7,8),
                'coupon_type'   => 2,
                'coupon_code'   => 'ZZZ1',
                'uses_per_coupon'   => '',
                'uses_per_customer' => '',
                'from_date' => '2017-12-01',
                'to_date'   => '2017-12-30',
                'sort_order'    => '',
                'is_rss'    => 0,
                'rule'  => array(
                    'conditions'    => array(
                        array(
                            'type'  => 'salesrule/rule_condition_combine',
                            'aggregator'    => 'all',
                            'value' => 1,
                            'new_child' => '',
                        )
                    ),

                    'actions' => array(
                        array(
                            'type'  => 'salesrule/rule_condition_product_combine',
                            'aggregator'    => 'all',
                            'value' => 1,
                            'new_child' => '',
                        )
                    )
                ),
                'simple_action' => 'by_fixed',
                'discount_amount'   => 34.6,
                'discount_qty'  => 0,
                'discount_step' => '',
                'apply_to_shipping' => 0,
                'simple_free_shipping'  => 0,
                'stop_rules_processing' => 0,
                'store_labels'  => array('', '')
            );

            $validateResult = $model->validateData(new Varien_Object($data));
            if ($validateResult !== true) {
                foreach($validateResult as $errorMessage) {
                    zend_debug::dump($errorMessage);
                }
                exit;
            }

            if (isset($data['simple_action']) && $data['simple_action'] == 'by_percent'
            && isset($data['discount_amount'])) {
                $data['discount_amount'] = min(100,$data['discount_amount']);
            }
            if (isset($data['rule']['conditions'])) {
                $data['conditions'] = $data['rule']['conditions'];
            }
            if (isset($data['rule']['actions'])) {
                $data['actions'] = $data['rule']['actions'];
            }
            unset($data['rule']);
            $model->loadPost($data);

            $useAutoGeneration = (int)!empty($data['use_auto_generation']);
            $model->setUseAutoGeneration($useAutoGeneration);

            $model->save();
            zend_debug::dump('SUCCESS');
            zend_debug::dump($model->debug());
            return;
        } catch (Mage_Core_Exception $e) {
            zend_debug::dump($e->getMessage());
        } catch (Exception $e) {
            zend_debug::dump($e->getMessage());
        }
    }
}
