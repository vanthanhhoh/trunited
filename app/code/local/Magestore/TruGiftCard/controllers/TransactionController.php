<?php

/**
 * Magestore
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magestore.com license that is
 * available through the world-wide-web at this URL:
 * http://www.magestore.com/license-agreement.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Magestore
 * @package     Magestore_RewardPoints
 * @copyright   Copyright (c) 2012 Magestore (http://www.magestore.com/)
 * @license     http://www.magestore.com/license-agreement.html
 */

/**
 * RewardPoints Index Controller
 *
 * @category    Magestore
 * @package     Magestore_RewardPoints
 * @author      Magestore Developer
 */
class Magestore_TruGiftCard_TransactionController extends Mage_Core_Controller_Front_Action
{
    /*public function cancelTransactionAction()
    {
        $transaction_id = $this->getRequest()->getParam('id');
        $transaction = Mage::getModel('trugiftcard/transaction')->load($transaction_id);
        $connection = Mage::getSingleton('core/resource')->getConnection('core_write');
        $helper = Mage::helper('trugiftcard/transaction');
        try {
            $connection->beginTransaction();
            if (!$transaction->getId()) {
                throw new Exception(Mage::helper('trugiftcard')->__('Transaction doesn\'t exist'));
            }

            if(!Mage::helper('trugiftcard/transaction')->isShowCancel($transaction)){
                throw new Exception(
                    Mage::helper('trugiftcard')->__('You don\'t have the permission to take this action. ')
                );
            }

            if ($transaction->getReceiverCustomerId() == 0) {
                $helper->updateTransaction(
                    $transaction,
                    Magestore_TruGiftCard_Model_Status::STATUS_TRANSACTION_CANCELLED,
                    array(),
                    abs($transaction->getChangedCredit())
                );

                $rewardAccount = Mage::helper('trugiftcard/account')->loadByCustomerId($transaction->getCustomerId());
                $rewardAccount->setTrugiftcardCredit(
                    $rewardAccount->getTrugiftcardCredit() + abs($transaction->getChangedCredit())
                );
                $rewardAccount->save();
            } else {
                $orders = $helper->getCollectionOrderByCustomer(
                    $transaction->getReceiverCustomerId(),
                    strtotime($transaction->getCreatedTime()),
                    $transaction
                );

                $truGiftCard_used = 0;
                $order_filter_ids = array();
                if ($orders != null && sizeof($orders) > 0) {
                    foreach ($orders as $order) {
                        $truGiftCard_used += $order->getTrugiftcardDiscount();
                        $order_filter_ids[] = $order->getEntityId();
                    }
                }

                if ($truGiftCard_used >= abs($transaction->getChangedCredit())) {
                    $helper->updateTransaction(
                        $transaction,
                        Magestore_TruGiftCard_Model_Status::STATUS_TRANSACTION_CANCELLED,
                        $order_filter_ids
                    );
                } else {
                    $return_points = abs($transaction->getChangedCredit()) - $truGiftCard_used;
                    $rewardAccount = Mage::helper('trugiftcard/account')->loadByCustomerId($transaction->getCustomerId());
                    $rewardAccount->setTrugiftcardCredit($rewardAccount->getTrugiftcardCredit() + abs($return_points));
                    $rewardAccount->save();

                    $receiveAccount = Mage::helper('trugiftcard/account')
                        ->loadByCustomerId($transaction->getReceiverCustomerId());
                    $receiveAccount->setTrugiftcardCredit($receiveAccount->getTrugiftcardCredit() - abs($return_points));
                    $receiveAccount->save();

                    $helper->updateTransaction(
                        $transaction,
                        Magestore_TruGiftCard_Model_Status::STATUS_TRANSACTION_CANCELLED,
                        $order_filter_ids,
                        $return_points
                    );
                }
            }

            $connection->commit();
            Mage::getSingleton('core/session')->addSuccess(
                Mage::helper('trugiftcard')->__('Transaction has been cancelled successfully')
            );
        } catch (Exception $ex) {
            $connection->rollback();
            Mage::getSingleton('core/session')->addError(
                $ex->getMessage()
            );
        }

        $this->_redirectUrl(Mage::getUrl('trugiftcard'));
    }*/

    public function cancelTransactionAction()
    {
        $transaction_id = $this->getRequest()->getParam('id');
        $transaction = Mage::getModel('trugiftcard/transaction')->load($transaction_id);
        $connection = Mage::getSingleton('core/resource')->getConnection('core_write');
        $helper = Mage::helper('trugiftcard/transaction');

        try {
            $connection->beginTransaction();

            if (!$transaction->getId()) {
                throw new Exception(Mage::helper('trugiftcard')->__('Transaction doesn\'t exist'));
            }

            if(!Mage::helper('trugiftcard/giftcode')->isCanCancel($transaction))
            {
                throw new Exception(
                    Mage::helper('trugiftcard')->__('You can\'t cancel this transaction because the Gift Card Code has been used.')
                );
            }

            $giftCodes = Mage::getModel('trugiftcard/giftcode')
                ->getCollection()
                ->addFieldToFilter('transaction_sender_id', $transaction->getId())
                ->addFieldToFilter('can_use', Magestore_TruGiftCard_Model_Status::GIFT_CODE_CAN_USE_YES)
                ;

            $total_back = 0;
            if(sizeof($giftCodes) > 0)
            {
                foreach ($giftCodes as $giftCode) {
                    $total_back += $giftCode->getData('remaining_value');
                    $giftCode->setCanUse(Magestore_TruGiftCard_Model_Status::GIFT_CODE_CAN_USE_NO);
                    $giftCode->save();

                    $oRule = Mage::getModel('salesrule/rule')->load($giftCode->getRuleId());
                    if($oRule != null && $oRule->getId())
                    {
                        $oRule->setData('is_active', 0);
                        $oRule->save();
                    }
                }
            }

            if($total_back > 0)
            {
                $helper->updateTransaction(
                    $transaction,
                    Magestore_TruGiftCard_Model_Status::STATUS_TRANSACTION_CANCELLED,
                    array(),
                    $total_back
                );

                $rewardAccount = Mage::helper('trugiftcard/account')->loadByCustomerId($transaction->getCustomerId());
                $rewardAccount->setTrugiftcardCredit(
                    $rewardAccount->getTrugiftcardCredit() + $total_back
                );
                $rewardAccount->save();
            }

            $connection->commit();
            Mage::getSingleton('core/session')->addSuccess(
                Mage::helper('trugiftcard')->__('Transaction has been cancelled successfully')
            );
        } catch (Exception $ex) {
            $connection->rollback();
            Mage::getSingleton('core/session')->addError(
                $ex->getMessage()
            );
        }

        $this->_redirectUrl(Mage::getUrl('trugiftcard'));
    }

    public function registerAction()
    {
        $email = $this->getRequest()->getParam('email');
        Mage::getSingleton('core/session')->setEmailRefer($email);
        $this->_redirectUrl(Mage::getUrl('customer/account/create/'));
        return;
    }


}
