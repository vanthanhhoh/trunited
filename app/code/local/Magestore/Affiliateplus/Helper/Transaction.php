<?php

class Magestore_Affiliateplus_Helper_Transaction extends Mage_Core_Helper_Abstract
{
    public function import()
    {
        $fileName = $_FILES['csv_store']['tmp_name'];
        $csvObject = new Varien_File_Csv();
        $csvData = $csvObject->getData($fileName);
        $import_count = 0;

        $transactionSave = Mage::getModel('core/resource_transaction');
        $connection = Mage::getSingleton('core/resource')->getConnection('core_write');

        try {
            $connection->beginTransaction();
            if (sizeof($csvData) > 0) {

                // $current_symbol = Mage::app()->getLocale()->currency(Mage::app()->getStore()->getCurrentCurrencyCode())->getSymbol();

                foreach ($csvData as $csv) {
                    // $amount = preg_replace("/[^0-9,-.]/", "", $csv[1]);
                    $amount = preg_replace(array('/,+$/s', '/[^0-9.]/s'), "", $csv[1]);
                    
                    // $amount = str_replace($current_symbol, '', $csv[1]);
                    if (isset($csv[0]) && !filter_var($csv[0], FILTER_VALIDATE_INT) === false) {
                        $customer = Mage::getModel('customer/customer')->load($csv[0]);

                        if ($customer != null && $customer->getId() > 0) {
                            $account = Mage::getModel('affiliateplus/account')->load($customer->getId(), 'customer_id');
                            if ($account != null && $account->getId()) {

                                $balance = $account->getBalance() + $amount;
                                
                                $account->setBalance($balance)->save();

                                $obj = Mage::getModel('affiliateplus/transaction');

                                $_data = array(
                                    'account_id' => $account->getId(),
                                    'account_name' => $account->getName(),
                                    'account_email' => $account->getEmail(),
                                    'commission' => $amount,
                                    'store_id' => Mage::app()->getStore()->getStoreId(),
                                    'created_time' => now(),
                                    'status' => 1,
                                    'type' => Magestore_Affiliateplus_Model_Type::TRANSACTION_TYPE_AFFILIATE_CASH_PAYOUT,
                                    'description' => $csv[2],
                                );

                                $obj->setData($_data);

                                $transactionSave->addObject($obj);
                                $import_count++;
                            }
                        }
                    }
                }
            }

            $transactionSave->save();
            $connection->commit();
        } catch (Exception $e) {
            $connection->rollback();
            zend_debug::dump('a');
            zend_debug::dump($e->getMessage());
            exit;
        }
        return $import_count;
    }
}
