<?php

class Magestore_Affiliateplus_Model_Admin_Observer
{
    /**
     * @param $observer
     * @return $this
     * @throws Exception
     */
    public function customerSaveAfter($observer)
    {
        $customer = $observer['customer'];

        $tracking = Mage::getModel('affiliateplus/tracking')->getCollection()
            ->addFieldToFilter('customer_id', $customer->getId())
            ->getFirstItem();

        if($tracking != null && $tracking->getId()){
            $tracking->setCustomerEmail($customer->getEmail())->save();
        }
    }
}
