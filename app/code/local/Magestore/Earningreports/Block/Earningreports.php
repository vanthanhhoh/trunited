<?php

class Magestore_Earningreports_Block_Earningreports extends Mage_Core_Block_Template
{
	public function _prepareLayout(){
		return parent::_prepareLayout();
	}

	public function getCustomerStats()
	{
		if($this->getM() != null)
		{
			return Mage::helper('badges')->getCustomerStats(
				Mage::getSingleton('customer/session')->getCustomer()->getId(),
				false,
				Mage::helper('badges')->getPeriodIdByTime($this->getM())
			);
		} else
			return Mage::helper('badges')->getCustomerStats(
				Mage::getSingleton('customer/session')->getCustomer()->getId(),
				false,
				false
			);
	}

	public function getM()
	{
		return base64_decode($this->getRequest()->getParam('m'));
	}

	public function getCurrentMonth()
	{
		if($this->getM() != null)
			return date('F Y', strtotime($this->getM()));
		else
			return date('F Y', time());
	}

	public function getBadgeFromStats($badge_name)
	{
		return Mage::helper('badges')->getBadgeFromStats(
			$badge_name
		);
	}


	public function getImagePath($image_name)
	{
		return Mage::helper('badges')->getImagePath(
			$image_name
		);
	}

	public function getTruWalletPoints()
	{
		if($this->getM() != null)
			return Mage::helper('earningreports')->getTruWalletPoints(
				Mage::getSingleton('customer/session')->getCustomer()->getId(),
				date('Y-m-d', strtotime('+1 month', strtotime($this->getM())))
			);
		else
			return Mage::helper('earningreports')->getTruWalletPoints(
				Mage::getSingleton('customer/session')->getCustomer()->getId(),
				date('Y-m-d', strtotime('+1 month'))
			);
	}

	public function getAffiliateIncome()
	{
		if($this->getM() != null)
			return Mage::helper('earningreports')->getAffiliateIncome(
				Mage::getSingleton('customer/session')->getCustomer()->getId(),
				date('Y-m-d', strtotime('+1 month', strtotime($this->getM())))
			);
		else
			return Mage::helper('earningreports')->getAffiliateIncome(
				Mage::getSingleton('customer/session')->getCustomer()->getId(),
				date('Y-m-d', strtotime('+1 month'))
			);
	}

	public function getListYears()
	{
		$rs = array();
		for	($start = 2017; $start <= date('Y', time()); $start++)
		{
			$rs[] = $start;
		}

		return $rs;
	}

	public function getListMonthsByYear($year)
	{
		$rs = array();

		if($year == 2017)
		{
			for ($i = 9; $i <= 12; $i++)
			{
				$rs[$i] = date('F', strtotime("$year-$i-01"));
			}
		} else {
			for ($i = 1; $i <= date('m', time()); $i++)
			{
				$rs[$i] = date('F', strtotime("$year-$i-01"));
			}
		}

		return $rs;
	}

	public function getByMonth($month, $year)
	{
		$m = "$year-$month-01";
		return $this->getUrl('*/*/', array('m' => base64_encode($m)));
	}

	public function getActiveYear()
	{
		if($this->getM() != null)
		{
			return date('Y', strtotime($this->getM()));
		} else {
			return date('Y', time());
		}
	}

}