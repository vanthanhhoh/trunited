<?php

class Magestore_Earningreports_Helper_Data extends Mage_Core_Helper_Abstract
{
    public function getTruWalletPoints($customer_id, $month)
    {
        $readConnection = Mage::getSingleton('core/resource')->getConnection('core_read');
        $truWallet_transaction_name = Mage::getSingleton('core/resource')->getTableName('truwallet/transaction');

        $start_month = date('Y-m-01', strtotime($month));
        $end_month = date('Y-m-t', strtotime($month));

        $query = "SELECT SUM(changed_credit)";
        $query .= " FROM $truWallet_transaction_name";
        $query .= " WHERE action_type = ".Magestore_TruWallet_Model_Type::TYPE_TRANSACTION_PAYOUT_EARNING;
        $query .= "  and created_time >= '$start_month' and created_time <= '$end_month'";
        $query .= "  and customer_id = $customer_id";

        $result = $readConnection->fetchOne($query);

        return $result > 0 ? $result : 0;
    }

    public function getAffiliateIncome($customer_id, $month)
    {
        $affiliate = Mage::getModel('affiliateplus/account')->loadByCustomerId($customer_id);
        if($affiliate != null && $affiliate->getId())
        {
            $readConnection = Mage::getSingleton('core/resource')->getConnection('core_read');
            $affiliate_transaction_name = Mage::getSingleton('core/resource')->getTableName('affiliateplus/transaction');

            $start_month = date('Y-m-01', strtotime($month));
            $end_month = date('Y-m-t', strtotime($month));
            $affiliate_id = $affiliate->getId();

            $query = "SELECT SUM(commission)";
            $query .= " FROM $affiliate_transaction_name";
            $query .= " WHERE type = 11 AND commission > 0";
            $query .= "  and created_time >= '$start_month' and created_time <= '$end_month'";
            $query .= "  and account_id = $affiliate_id";

            $result = $readConnection->fetchOne($query);

            return $result > 0 ? $result : 0;
        } else {
            return 0;
        }
    }
}