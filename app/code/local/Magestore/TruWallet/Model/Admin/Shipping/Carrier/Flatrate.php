<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Shipping
 * @copyright  Copyright (c) 2006-2016 X.commerce, Inc. and affiliates (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */


/**
 * Flat rate shipping model
 *
 * @category   Mage
 * @package    Mage_Shipping
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Magestore_TruWallet_Model_Admin_Shipping_Carrier_Flatrate extends Mage_Shipping_Model_Carrier_Flatrate
{
    public function checkOrderFromTruBox()
    {
        $admin_session = Mage::getSingleton('adminhtml/session');
        $customer_id = $admin_session->getOrderCustomerId();
        if(isset($customer_id) && $customer_id > 0 && Mage::app()->getStore()->isAdmin())
            return true;
        else
            return false;
    }

    public function collectRates(Mage_Shipping_Model_Rate_Request $request)
    {
        if (!$this->getConfigFlag('active')) {
            return false;
        }

        $items = $request->getAllItems();
        $roses_rule = Mage::helper('other')->isSITA($items);
        $is_roses = $roses_rule < 3 || ($roses_rule == 3 && strcasecmp('flatrate', Mage::helper('other')->getConfigData('roses_rule', 'shipping_method')) != 0);
        $delivery_type = Mage::getSingleton('checkout/session')->getData('delivery_type');

        $totals = Mage::getSingleton('checkout/session')->getQuote()->getTotals();
        $subtotal = $totals["subtotal"]->getValue();

        $free_minimum_amount = Mage::getStoreConfig('carriers/freeshipping/free_shipping_subtotal');

        if(($subtotal >= $free_minimum_amount && $free_minimum_amount > 0)
            || ($delivery_type == 1 && Mage::getSingleton('customer/session')->isLoggedIn())
        ){
            if($is_roses)
                return false;
        }
        
        $freeBoxes = 0;

        if (sizeof($items) > 0) {
            foreach ($items as $item) {

                if ($item->getProduct()->isVirtual() || $item->getParentItem()) {
                    continue;
                }

                if ($item->getHasChildren() && $item->isShipSeparately()) {
                    foreach ($item->getChildren() as $child) {
                        if ($child->getFreeShipping() && !$child->getProduct()->isVirtual()) {
                            $freeBoxes += $item->getQty() * $child->getQty();
                        }
                    }
                } elseif ($item->getFreeShipping()) {
                    $freeBoxes += $item->getQty();
                }
            }
        }
        $this->setFreeBoxes($freeBoxes);

        $result = Mage::getModel('shipping/rate_result');
        if ($this->getConfigData('type') == 'O') { // per order
            $shippingPrice = $this->getConfigData('price');
        } elseif ($this->getConfigData('type') == 'I') { // per item
            $shippingPrice = ($request->getPackageQty() * $this->getConfigData('price')) - ($this->getFreeBoxes() * $this->getConfigData('price'));
        } else {
            $shippingPrice = false;
        }

        
        $shippingPrice = $this->getFinalPriceWithHandlingFee($shippingPrice);

        if ($shippingPrice !== false) {
            $method = Mage::getModel('shipping/rate_result_method');

            $method->setCarrier('flatrate');
            $method->setCarrierTitle($this->getConfigData('title'));

            $method->setMethod('flatrate');
            $method->setMethodTitle($this->getConfigData('name'));

            if (($request->getFreeShipping() === true || $request->getPackageQty() == $this->getFreeBoxes()) && $is_roses) {
                $shippingPrice = '0.00';
            }

            if($this->checkOrderFromTruBox())
            {
                $shipping_amount = Mage::helper('trubox')->getShippingAmount();
                if($shipping_amount != null)
                    $shippingPrice = $shipping_amount;
            }

            $method->setPrice($shippingPrice);
            $method->setCost($shippingPrice);

            $result->append($method);
        }

        return $result;
    }
}
