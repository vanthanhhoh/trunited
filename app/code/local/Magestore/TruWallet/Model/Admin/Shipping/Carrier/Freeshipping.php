<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Shipping
 * @copyright  Copyright (c) 2006-2017 X.commerce, Inc. and affiliates (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */


/**
 * Free shipping model
 *
 * @category   Mage
 * @package    Mage_Shipping
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Magestore_TruWallet_Model_Admin_Shipping_Carrier_Freeshipping
    extends Mage_Shipping_Model_Carrier_Freeshipping
{
    public function checkIsAdmin()
    {
        return Mage::app()->getStore()->isAdmin();
    }
    
    /**
     * @param Mage_Shipping_Model_Rate_Request $request
     * @return bool|false|Mage_Core_Model_Abstract
     */
    public function collectRates(Mage_Shipping_Model_Rate_Request $request)
    {
        if (!$this->getConfigFlag('active')) {
            return false;
        }

        $items = $request->getAllItems();
        $roses_rule = Mage::helper('other')->isSITA($items);

        $is_roses = $roses_rule == 3 && strcasecmp('freeshipping', Mage::helper('other')->getConfigData('roses_rule', 'shipping_method')) == 0;

        if($roses_rule == 3 && !$this->checkIsAdmin() &&
            strcasecmp('freeshipping', Mage::helper('other')->getConfigData('roses_rule', 'shipping_method')) != 0)
            return false;

        $result = Mage::getModel('shipping/rate_result');
        $this->_updateFreeMethodQuote($request);

        $delivery_type = Mage::getSingleton('checkout/session')->getData('delivery_type');

        $totals = Mage::getSingleton('checkout/session')->getQuote()->getTotals();
        $subtotal = $totals["subtotal"]->getValue();

        if ($request->getFreeShipping() || ($subtotal >= $this->getConfigData('free_shipping_subtotal'))
        || ($delivery_type == 1 && Mage::getSingleton('customer/session')->isLoggedIn()) ||
            $this->checkIsAdmin() || $is_roses)
        {
            $method = Mage::getModel('shipping/rate_result_method');

            $method->setCarrier('freeshipping');
            $method->setCarrierTitle($this->getConfigData('title'));

            $method->setMethod('freeshipping');
            $method->setMethodTitle($this->getConfigData('name'));

            $method->setPrice('0.00');
            $method->setCost('0.00');

            $result->append($method);
        }

        return $result;
    }
}
