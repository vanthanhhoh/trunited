<?php
/**
 * Magestore
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the Magestore.com license that is
 * available through the world-wide-web at this URL:
 * http://www.magestore.com/license-agreement.html
 * 
 * DISCLAIMER
 * 
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 * 
 * @category    Magestore
 * @package     Magestore_RewardPoints
 * @copyright   Copyright (c) 2012 Magestore (http://www.magestore.com/)
 * @license     http://www.magestore.com/license-agreement.html
 */

/**
 * Rewardpoints Account Dashboard Recent Transactions
 * 
 * @category    Magestore
 * @package     Magestore_RewardPoints
 * @author      Magestore Developer
 */
class Magestore_TruWallet_Block_Account_Dashboard_Transactions extends Mage_Core_Block_Template
{
    protected function _construct()
    {
        $status = (int)$_GET['status'];

        switch( $status ){
            case 0:
                $searchStatus = 0;
                break;
            case 1:
                // 2 for completed
                $searchStatus = '2';
                break;
            case 2:
                // 1 for pending
                $searchStatus = '1';
                break;
            /*case 3:
                $searchStatus = '3';
                break;
            case 4:
                $searchStatus = '4';
                break;
            case 5:
                $searchStatus = '5';
                break;*/
            default:
                $searchStatus = 0;
                break;
        }
        
        parent::_construct();
        $customerId = Mage::getSingleton('customer/session')->getCustomerId();
       if ($searchStatus){
            $collection = Mage::getResourceModel('truwallet/transaction_collection')
            ->addFieldToFilter('status',$searchStatus)->addFieldToFilter('customer_id', $customerId);
        }else{
            $collection = Mage::getResourceModel('truwallet/transaction_collection')
            ->addFieldToFilter('customer_id', $customerId);
        }
        /*$collection->getSelect()->limit(7)
            ->order('created_time DESC');*/
        $collection->setOrder('created_time','DESC');
        $collection->setOrder('transaction_id','DESC');
        $this->setCollection($collection);
    }
    public function _prepareLayout() {
        parent::_prepareLayout();
        $pager = $this->getLayout()->createBlock('page/html_pager', 'transactions_pager')
                ->setCollection($this->getCollection());
        $this->setChild('transactions_pager', $pager);
        return $this;
    }

    public function getPagerHtml() {
        return $this->getChildHtml('transactions_pager');
    }
}
