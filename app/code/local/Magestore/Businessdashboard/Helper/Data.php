<?php

class Magestore_Businessdashboard_Helper_Data extends Mage_Core_Helper_Abstract
{
    const TBL_PAYOUT_SUMMARY = 'tr_PayoutSummary';

    public function getCoreResource()
    {
        return Mage::getSingleton('core/resource');
    }

    public function getReadConnection()
    {
        return $this->getCoreResource()->getConnection('core_read');
    }

    public function getYtdSaving()
    {
        $readConnection = $this->getReadConnection();

        $query = "SELECT SUM(personalpts) / SUM(connectionpts + personalpts) as point_value";
        $query .= " FROM ".self::TBL_PAYOUT_SUMMARY."";

        $result = $readConnection->fetchOne($query);

        return $result;
    }
}