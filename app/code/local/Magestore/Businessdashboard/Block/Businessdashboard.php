<?php

class Magestore_Businessdashboard_Block_Businessdashboard extends Mage_Core_Block_Template
{
	public function _prepareLayout(){
		return parent::_prepareLayout();
	}

	public function getCustomerStats()
    {
        return Mage::helper('badges')->getCustomerStats(
            Mage::getSingleton('customer/session')->getCustomer()->getId()
        );
    }

    public function getYtdSaving()
    {
        return Mage::helper('businessdashboard')->getYtdSaving();
    }

    public function getCustomerId()
    {
        return Mage::getSingleton('customer/session')->getCustomer()->getId();
    }

    public function getCurrentCustomer()
    {
        return  Mage::getModel('customer/customer')->load(
            $this->getCustomerId()
        );
    }

    public function getCustomerLeaderStats($customer_id)
    {
        return Mage::helper('badges')->getCustomerStats(
            $customer_id
        );
    }

    public function getBadgeFromStats($badge_name)
    {
        return Mage::helper('badges')->getBadgeFromStats(
            $badge_name
        );
    }

    public function getCustomerBadgesHistory($customer_id)
    {
        return Mage::helper('badges')->getCustomerBadgesHistory(
            $customer_id
        );
    }

    public function getImagePath($image_name)
    {
        return Mage::helper('badges')->getImagePath(
            $image_name
        );
    }

    public function isEnableLeaderSection()
    {
        return Mage::helper('nationpassport')->getConfigData('general', 'leader_board_on_business');
    }

    public function getRankOption()
    {
        return  Magestore_Nationpassport_Model_Status::getBusinessRankOption();
    }

    public function getFilterLearBoardUrl()
    {
        return $this->getUrl('*/*/leaderboard');
    }


    public function getMemberInfoURL($id)
    {
        return Mage::getUrl('passport/member/info', array('id'  => base64_encode($id)));
    }

    public function getCurrentPointValue()
    {
        $current_point_value = Mage::helper('other')->getConfigData('general', 'current_point_value');
        return Mage::helper('core')->currency($current_point_value,true,false);
    }
}
