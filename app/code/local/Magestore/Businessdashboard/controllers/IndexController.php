<?php

class Magestore_Businessdashboard_IndexController extends Mage_Core_Controller_Front_Action
{
	/**
     * check customer is logged in
     */
    public function preDispatch()
    {
        parent::preDispatch();
        if (!$this->getRequest()->isDispatched()) {
            return;
        }
        $action = $this->getRequest()->getActionName();
        if ($action != 'policy' && $action != 'redirectLogin') {
            // Check customer authentication
            if (!Mage::getSingleton('customer/session')->isLoggedIn()) {
                Mage::getSingleton('customer/session')->setAfterAuthUrl(
                    Mage::getUrl($this->getFullActionName('/'))
                );
                $this->_redirect('customer/account/login');
                $this->setFlag('', Mage_Core_Controller_Varien_Action::FLAG_NO_DISPATCH, true);
            }
        }
    }
    
	public function indexAction(){
		$this->loadLayout();

		$this->_title(Mage::helper('businessdashboard')->__('Business Dashboard'));

        $breadcrumbs = $this->getLayout()->getBlock("breadcrumbs");
        $breadcrumbs->addCrumb("home", array(
            "label" => $this->__("Home"),
            "title" => $this->__("Home"),
            "link"  => Mage::getBaseUrl()
        ));

        $breadcrumbs->addCrumb("my_account", array(
            "label" => $this->__("My Account"),
            "title" => $this->__("My Account"),
            "link"  => Mage::getUrl('customer/account/')
        ));

        $breadcrumbs->addCrumb("business_dashboard", array(
            "label" => $this->__("Business Dashboard"),
            "title" => $this->__("Business Dashboard"),
        ));


		$this->renderLayout();
	}

    public function leaderboardAction()
    {
        $data = $this->getRequest()->getParams();

        if(!isset($data)){
            echo $this->__('<b>Error!</b> Something was wrong with this. Please check them again!');
            return;
        }

        $rank_array = Magestore_Nationpassport_Model_Status::getBusinessRankArray();
        if($data['rank'] == null || !in_array($data['rank'], $rank_array))
        {
            echo $this->__('<b>Error!</b> The param was wrong.');
            return;
        }

        $list = Mage::helper('nationpassport')->findBusinessLeaderBoars(
            $data['rank'],
            isset($data['keyword']) && $data['keyword'] != null ? $data['keyword'] : null
        );

        echo $this->getLayout()->createBlock('businessdashboard/businessdashboard')
            ->setData('list', $list)
            ->setTemplate('businessdashboard/leader_board.phtml')
            ->toHtml();
    }
}
