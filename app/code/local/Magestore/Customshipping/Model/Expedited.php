<?php
class Magestore_Customshipping_Model_Expedited extends Mage_Shipping_Model_Carrier_Abstract 
implements Mage_Shipping_Model_Carrier_Interface
{
	protected $_code = 'expedited_shippingmethod';

	public function collectRates(Mage_Shipping_Model_Rate_Request $request)
	{
		$items = $request->getAllItems();
		$roses_rule = Mage::helper('other')->isSITA($items);

		if($roses_rule == 3 && strcasecmp('expedited_shippingmethod', Mage::helper('other')->getConfigData('roses_rule', 'shipping_method')) != 0)
			return false;

		$result = Mage::getModel('shipping/rate_result');
		$result->append($this->_getDefaultRate($request));

		return $result;
	}

	public function getAllowedMethods()
	{
		return array(
			'expedited_shippingmethod' => $this->getConfigData('name'),
		);
	}

	protected function _getDefaultRate($request)
	{
		$rate = Mage::getModel('shipping/rate_result_method');

		$flat_rate_price = $this->calculatePrice($request);
		$final_price = $this->getConfigData('price') + $flat_rate_price;

		$rate->setCarrier($this->_code);
		$rate->setCarrierTitle($this->getConfigData('title'));
		$rate->setMethod($this->_code);
		$rate->setMethodTitle($this->getConfigData('title'));
		$rate->setPrice($final_price);
		$rate->setCost($final_price);

		return $rate;
	}

	public function calculatePrice($request)
	{
		$delivery_type = Mage::getSingleton('checkout/session')->getData('delivery_type');

        $totals = Mage::getSingleton('checkout/session')->getQuote()->getTotals();
        $subtotal = $totals["subtotal"]->getValue();

        $free_minimum_amount = Mage::getStoreConfig('carriers/freeshipping/free_shipping_subtotal');

        if(($subtotal >= $free_minimum_amount && $free_minimum_amount > 0)
            || ($delivery_type == 1 && Mage::getSingleton('customer/session')->isLoggedIn())
        ){
            return 0;
        }

        $freeBoxes = 0;
        $items = $request->getAllItems();
        if (sizeof($items) > 0) {
            foreach ($items as $item) {

                if ($item->getProduct()->isVirtual() || $item->getParentItem()) {
                    continue;
                }

                if ($item->getHasChildren() && $item->isShipSeparately()) {
                    foreach ($item->getChildren() as $child) {
                        if ($child->getFreeShipping() && !$child->getProduct()->isVirtual()) {
                            $freeBoxes += $item->getQty() * $child->getQty();
                        }
                    }
                } elseif ($item->getFreeShipping()) {
                    $freeBoxes += $item->getQty();
                }
            }
        }
		
		$shippingPrice = ($request->getPackageQty() * Mage::getStoreConfig('carriers/flatrate/price')) - ($freeBoxes * Mage::getStoreConfig('carriers/flatrate/price'));

        
        $shippingPrice = $this->getFinalPriceWithHandlingFee($shippingPrice);

        return $shippingPrice;
	}
}
