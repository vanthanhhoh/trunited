<?php
class Magestore_Customshipping_Model_Mytrubox extends Mage_Shipping_Model_Carrier_Abstract 
implements Mage_Shipping_Model_Carrier_Interface
{
	protected $_code = 'mytrubox_shippingmethod';

	public function collectRates(Mage_Shipping_Model_Rate_Request $request)
	{
		if(!Mage::getSingleton('customer/session')->isLoggedIn())
			return false;

		$items = $request->getAllItems();
		$roses_rule = Mage::helper('other')->isSITA($items);
		if($roses_rule == 3 && strcasecmp('mytrubox_shippingmethod', Mage::helper('other')->getConfigData('roses_rule', 'shipping_method')) != 0)
			return false;

		$result = Mage::getModel('shipping/rate_result');
		$result->append($this->_getDefaultRate());

		return $result;
	}

	public function getAllowedMethods()
	{
		return array(
			'mytrubox_shippingmethod' => $this->getConfigData('name'),
		);
	}

	protected function _getDefaultRate()
	{
		$rate = Mage::getModel('shipping/rate_result_method');

		$rate->setCarrier($this->_code);
		$rate->setCarrierTitle($this->getConfigData('title'));
		$rate->setMethod($this->_code);
		$rate->setMethodTitle($this->getConfigData('title'));
		$rate->setPrice(0);
		$rate->setCost(0);

		return $rate;
	}
}
