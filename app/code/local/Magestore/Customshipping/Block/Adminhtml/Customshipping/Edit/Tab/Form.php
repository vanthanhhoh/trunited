<?php

class Magestore_Customshipping_Block_Adminhtml_Customshipping_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
	protected function _prepareForm(){
		$form = new Varien_Data_Form();
		$this->setForm($form);
		
		if (Mage::getSingleton('adminhtml/session')->getCustomshippingData()){
			$data = Mage::getSingleton('adminhtml/session')->getCustomshippingData();
			Mage::getSingleton('adminhtml/session')->setCustomshippingData(null);
		}elseif(Mage::registry('customshipping_data'))
			$data = Mage::registry('customshipping_data')->getData();
		
		$fieldset = $form->addFieldset('customshipping_form', array('legend'=>Mage::helper('customshipping')->__('Item information')));

		$fieldset->addField('title', 'text', array(
			'label'		=> Mage::helper('customshipping')->__('Title'),
			'class'		=> 'required-entry',
			'required'	=> true,
			'name'		=> 'title',
		));

		$fieldset->addField('filename', 'file', array(
			'label'		=> Mage::helper('customshipping')->__('File'),
			'required'	=> false,
			'name'		=> 'filename',
		));

		$fieldset->addField('status', 'select', array(
			'label'		=> Mage::helper('customshipping')->__('Status'),
			'name'		=> 'status',
			'values'	=> Mage::getSingleton('customshipping/status')->getOptionHash(),
		));

		$fieldset->addField('content', 'editor', array(
			'name'		=> 'content',
			'label'		=> Mage::helper('customshipping')->__('Content'),
			'title'		=> Mage::helper('customshipping')->__('Content'),
			'style'		=> 'width:700px; height:500px;',
			'wysiwyg'	=> false,
			'required'	=> true,
		));

		$form->setValues($data);
		return parent::_prepareForm();
	}
}