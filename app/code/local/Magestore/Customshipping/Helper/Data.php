<?php

class Magestore_Customshipping_Helper_Data extends Mage_Core_Helper_Abstract
{
	public function getConfigData($group, $field)
    {
        return Mage::getStoreConfig('customshipping/'.$group.'/'.$field, Mage::app()->getStore()->getId());
    }

    public function getShippingConfigData($group, $field)
    {
        return Mage::getStoreConfig('carriers/'.$group.'/'.$field, Mage::app()->getStore()->getId());
    }

    public function getShippingCutOffText()
    {
        return $this->getConfigData('general', 'shipping_cut_off_time');
    }

    public function getShippingTextAtTop()
    {
        return $this->getConfigData('general', 'shipping_text_at_top');
    }

    public function getBonusPoint()
    {
        $quote = Mage::getSingleton('checkout/cart')->getQuote();
        $items = $quote->getAllItems();
		
		$total_rewardpoints_earn = 0;

        foreach ($items as $item) {
            /**
             * Skip if this item is virtual
             */
            if ($item->getProduct()->isVirtual()) {
                continue;
            }

            /**
             * Children weight we calculate for parent
             */
            if ($item->getParentItem()) {
                continue;
            }

            if ($item->getHasChildren() && $item->isShipSeparately()) {
                foreach ($item->getChildren() as $child) {
                    if ($child->getProduct()->isVirtual()) {
                        continue;
                    }
                    if (!$item->getProduct()->getWeightType()) {
						$product = Mage::getModel('catalog/product')->load($item->getProduct()->getId());
	                    if($product->getRewardpointsEarn() > 0){
	                        $total_rewardpoints_earn += $product->getRewardpointsEarn() * $item->getQty();
	                    }
                    }
                }

                if ($item->getProduct()->getWeightType()) {
                	$product = Mage::getModel('catalog/product')->load($item->getProduct()->getId());
	                if($product->getRewardpointsEarn() > 0){
	                    $total_rewardpoints_earn += $product->getRewardpointsEarn() * $item->getQty();
	                }
                }
            } else {
                $product = Mage::getModel('catalog/product')->load($item->getProduct()->getId());
                if($product->getRewardpointsEarn() > 0){
                    $total_rewardpoints_earn += $product->getRewardpointsEarn() * $item->getQty();
                }
            }
        }

        $bonusPoints = 0.1 * $total_rewardpoints_earn;

        return $bonusPoints;
    }

    public function getTextTruBoxMethod()
    {
        if(Mage::helper('trubox')->isShowCurrentMonth()){
            $text = $this->__('Next ship date %s', date('m/d/Y', time()));
        } else {
            $next_month = strtotime("first day of +1 month");
            $text = $this->__('Next ship date %s', date('m/10/Y', $next_month));
        }

        return $text;
    }
}
