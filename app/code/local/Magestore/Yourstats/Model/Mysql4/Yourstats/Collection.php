<?php

class Magestore_Yourstats_Model_Mysql4_Yourstats_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
	public function _construct(){
		parent::_construct();
		$this->_init('yourstats/yourstats');
	}
}