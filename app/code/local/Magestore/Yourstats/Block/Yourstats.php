<?php

class Magestore_Yourstats_Block_Yourstats extends Mage_Core_Block_Template
{
	public function _prepareLayout(){
		return parent::_prepareLayout();
	}

	public function getCustomerStats()
	{
		return Mage::helper('badges')->getCustomerStats(
			Mage::getSingleton('customer/session')->getCustomer()->getId()
		);
	}
}