<?php

class Magestore_Manageapi_Block_Adminhtml_Walmart_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
	public function __construct(){
		parent::__construct();
		$this->setId('walmart_api_tabs');
		$this->setDestElementId('edit_form');
		$this->setTitle(Mage::helper('manageapi')->__('Walmart API Information'));
	}

	protected function _beforeToHtml(){
		$this->addTab('form_section', array(
			'label'	 => Mage::helper('manageapi')->__('Walmart API Information'),
			'title'	 => Mage::helper('manageapi')->__('Walmart API Information'),
			'content'	 => $this->getLayout()->createBlock('manageapi/adminhtml_walmart_edit_tab_form')->toHtml(),
		));
		return parent::_beforeToHtml();
	}
}
