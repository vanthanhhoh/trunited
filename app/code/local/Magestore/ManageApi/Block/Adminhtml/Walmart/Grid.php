<?php

class Magestore_Manageapi_Block_Adminhtml_Walmart_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('walmartGrid');
        $this->setDefaultSort('walmart_id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
    }

    protected function _prepareCollection()
    {
        $collection = Mage::getModel('manageapi/walmart')->getCollection()
            ->setOrder('walmart_id', 'desc');
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $this->addColumn('walmart_id', array(
            'header' => Mage::helper('manageapi')->__('ID'),
            'align' => 'right',
            'width' => '50px',
            'index' => 'walmart_id',
        ));

        $this->addColumn('itemId', array(
            'header' => Mage::helper('manageapi')->__('Item ID'),
            'align' => 'right',
            'width' => '50px',
            'index' => 'itemId',
        ));

        $this->addColumn('largeImage', array(
            'header' => Mage::helper('manageapi')->__('Image'),
            'index' => 'largeImage',
            'renderer' => 'Magestore_ManageApi_Block_Adminhtml_Renderer_Walmart_Image',
        ));

        $this->addColumn('name', array(
            'header' => Mage::helper('manageapi')->__('Name'),
            'align' => 'left',
            'index' => 'name',
        ));

        $this->addColumn('msrp', array(
            'header' => Mage::helper('manageapi')->__('Msrp'),
            'index' => 'msrp',
        ));

        $this->addColumn('salePrice', array(
            'header' => Mage::helper('manageapi')->__('Sale Price'),
            'index' => 'salePrice',
        ));

        $this->addColumn('brandName', array(
            'header' => Mage::helper('manageapi')->__('Brand Name'),
            'index' => 'brandName',
        ));

        $this->addColumn('productTrackingUrl', array(
            'header' => Mage::helper('manageapi')->__('Tracking URL'),
            'index' => 'productTrackingUrl',
            'width' => '50px',
            'renderer' => 'Magestore_ManageApi_Block_Adminhtml_Renderer_Walmart_Url',
        ));

        $this->addColumn('productUrl', array(
            'header' => Mage::helper('manageapi')->__('Product URL'),
            'index' => 'productUrl',
            'width' => '50px',
            'renderer' => 'Magestore_ManageApi_Block_Adminhtml_Renderer_Walmart_Url',
        ));

        $this->addColumn('is_bestsellers', array(
            'header' => Mage::helper('affiliatepoints')->__('Is Bestsellers'),
            'align' => 'left',
            'index' => 'is_bestsellers',
            'type' => 'options',
            'options' => array(
                '1' => 'Yes',
                '0' =>  'No',
            ),
        ));

        $this->addColumn('product_created_at', array(
            'header' => Mage::helper('manageapi')->__('Product Created At'),
            'align' => 'left',
            'index' => 'product_created_at',
            'type' => 'date'
        ));

        $this->addColumn('product_id', array(
            'header' => Mage::helper('manageapi')->__('Product ID'),
            'align' => 'left',
            'width' => '50px',
            'index' => 'product_id',
            'renderer' => 'Magestore_ManageApi_Block_Adminhtml_Renderer_Coupon_Product',
        ));

        $this->addColumn('created_at', array(
            'header' => Mage::helper('manageapi')->__('Created At'),
            'align' => 'left',
            'index' => 'created_at',
            'type' => 'date'
        ));

        $this->addColumn('updated_at', array(
            'header' => Mage::helper('manageapi')->__('Updated At'),
            'align' => 'left',
            'index' => 'updated_at',
            'type' => 'date'
        ));
        $this->addColumn('categoryPath', array(
            'header' => Mage::helper('manageapi')->__('Category Path'),
            'index' => 'categoryPath',
        ));
        $this->addColumn('rollBack', array(
            'header' => Mage::helper('manageapi')->__('rollBack'),
            'index' => 'rollBack',
        ));
        $this->addColumn('clearance', array(
            'header' => Mage::helper('manageapi')->__('Clearance'),
            'index' => 'clearance',
        ));
        $this->addColumn('specialBuy', array(
            'header' => Mage::helper('manageapi')->__('Special Buy'),
            'index' => 'specialBuy',
        ));
        $this->addColumn('bundle', array(
            'header' => Mage::helper('manageapi')->__('Bundle'),
            'index' => 'bundle',
        ));
        $this->addColumn('preOrder', array(
            'header' => Mage::helper('manageapi')->__('PreOrder'),
            'index' => 'preOrder',
        ));

        $this->addColumn('addToCartUrl', array(
            'header' => Mage::helper('manageapi')->__('Add To Card URL'),
            'index' => 'addToCartUrl',
            'width' => '50px',
            'renderer' => 'Magestore_ManageApi_Block_Adminhtml_Renderer_Walmart_Url',
        ));
        $this->addColumn('affiliateAddToCartUrl', array(
            'header' => Mage::helper('manageapi')->__('Affiliate Add to Card URL'),
            'index' => 'affiliateAddToCartUrl',
            'width' => '50px',
            'renderer' => 'Magestore_ManageApi_Block_Adminhtml_Renderer_Walmart_Url',
        ));


        $this->addColumn('parentItemId', array(
            'header' => Mage::helper('manageapi')->__('Parent Item Id'),
            'index' => 'parentItemId',
        ));
        $this->addColumn('upc', array(
            'header' => Mage::helper('manageapi')->__('UPC'),
            'index' => 'upc',
        ));

        $this->addColumn('ninetySevenCentShipping', array(
            'header' => Mage::helper('manageapi')->__('NinetySevenCentShipping'),
            'index' => 'ninetySevenCentShipping',
        ));
        $this->addColumn('standardShipRate', array(
            'header' => Mage::helper('manageapi')->__('StandardShipRate'),
            'index' => 'standardShipRate',
        ));
        $this->addColumn('twoThreeDayShippingRate', array(
            'header' => Mage::helper('manageapi')->__('TwoThreeDayShippingRate'),
            'index' => 'twoThreeDayShippingRate',
        ));
        $this->addColumn('size', array(
            'header' => Mage::helper('manageapi')->__('Size'),
            'index' => 'size',
        ));
        $this->addColumn('marketplace', array(
            'header' => Mage::helper('manageapi')->__('Marketplace'),
            'index' => 'marketplace',
        ));
        $this->addColumn('shipToStore', array(
            'header' => Mage::helper('manageapi')->__('ShipToStore'),
            'index' => 'shipToStore',
        ));
        $this->addColumn('freeShipToStore', array(
            'header' => Mage::helper('manageapi')->__('FreeShipToStore'),
            'index' => 'freeShipToStore',
        ));
        $this->addColumn('modelNumber', array(
            'header' => Mage::helper('manageapi')->__('ModelNumber'),
            'index' => 'modelNumber',
        ));
        $this->addColumn('customerRating', array(
            'header' => Mage::helper('manageapi')->__('CustomerRating'),
            'index' => 'customerRating',
        ));
        $this->addColumn('numReviews', array(
            'header' => Mage::helper('manageapi')->__('NumReviews'),
            'index' => 'numReviews',
        ));
        $this->addColumn('customerRatingImage', array(
            'header' => Mage::helper('manageapi')->__('CustomerRatingImage'),
            'index' => 'customerRatingImage',
            'renderer' => 'Magestore_ManageApi_Block_Adminhtml_Renderer_Walmart_Image',
        ));
        $this->addColumn('categoryNode', array(
            'header' => Mage::helper('manageapi')->__('CategoryNode'),
            'index' => 'categoryNode',
        ));

        $this->addColumn('stock', array(
            'header' => Mage::helper('manageapi')->__('Stock'),
            'index' => 'stock',
        ));
        $this->addColumn('gender', array(
            'header' => Mage::helper('manageapi')->__('Gender'),
            'index' => 'gender',
        ));
        $this->addColumn('freeShippingOver50Dollars', array(
            'header' => Mage::helper('manageapi')->__('FreeShippingOver50Dollars'),
            'index' => 'freeShippingOver50Dollars',
        ));
        $this->addColumn('maxItemsInOrder', array(
            'header' => Mage::helper('manageapi')->__('MaxItemsInOrder'),
            'index' => 'maxItemsInOrder',
        ));
        $this->addColumn('availableOnline', array(
            'header' => Mage::helper('manageapi')->__('AvailableOnline'),
            'index' => 'availableOnline',
        ));
//        $this->addColumn('shortDescription', array(
//            'header' => Mage::helper('manageapi')->__('ShortDescription'),
//            'index' => 'shortDescription',
//        ));
//        $this->addColumn('longDescription', array(
//            'header' => Mage::helper('manageapi')->__('LongDescription'),
//            'index' => 'longDescription',
//        ));
        $this->addColumn('thumbnailImage', array(
            'header' => Mage::helper('manageapi')->__('ThumbnailImage'),
            'index' => 'thumbnailImage',
            'renderer' => 'Magestore_ManageApi_Block_Adminhtml_Renderer_Walmart_Image',
        ));
        $this->addColumn('mediumImage', array(
            'header' => Mage::helper('manageapi')->__('MediumImage'),
            'index' => 'mediumImage',
            'renderer' => 'Magestore_ManageApi_Block_Adminhtml_Renderer_Walmart_Image',
        ));

        $this->addExportType('*/*/exportCsv', Mage::helper('manageapi')->__('CSV'));
        $this->addExportType('*/*/exportXml', Mage::helper('manageapi')->__('XML'));

        return parent::_prepareColumns();
    }

    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('walmart_id');
        $this->getMassactionBlock()->setFormFieldName('walmart');

        $this->getMassactionBlock()->addItem('delete', array(
            'label' => Mage::helper('manageapi')->__('Delete'),
            'url' => $this->getUrl('*/*/massDelete'),
            'confirm' => Mage::helper('manageapi')->__('Are you sure?')
        ));

        $this->getMassactionBlock()->addItem('update', array(
            'label' => Mage::helper('manageapi')->__('Synchronize Product(s)'),
            'url' => $this->getUrl('*/*/massProduct'),
        ));

        return $this;
    }

    public function getRowUrl($row)
    {
        return '';
    }
}
