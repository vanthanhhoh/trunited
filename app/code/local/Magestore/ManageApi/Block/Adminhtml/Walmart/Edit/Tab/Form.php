<?php

class Magestore_Manageapi_Block_Adminhtml_Walmart_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form();
        $this->setForm($form);

        if (Mage::getSingleton('adminhtml/session')->getWalmartData()) {
            $data = Mage::getSingleton('adminhtml/session')->getWalmartData();
            Mage::getSingleton('adminhtml/session')->setWalmartData(null);
        } elseif (Mage::registry('walmart_data'))
            $data = Mage::registry('walmart_data')->getData();

        $fieldset = $form->addFieldset('manageapi_form', array('legend' => Mage::helper('manageapi')->__('Walmart API
		information')));

        $data['format'] = 'json';
        $data['specialOffer'] = 'rollback';
        $data['is_continue'] = '1';

        $fieldset->addField('format', 'select', array(
            'label' => Mage::helper('manageapi')->__('Deal Type'),
            'title' => Mage::helper('manageapi')->__('Deal Type'),
            'name' => 'format',
            'required' => false,
            'value' => 'json',
            'values' => array(
                'json' => Mage::helper('manageapi')->__('json'),
                'xml' => Mage::helper('manageapi')->__('xml'),
            ),
            'after_element_html' => "<br /><small>Format of the output in either JSON or XML</small>",
        ));

        $fieldset->addField('category', 'text', array(
            'label' => Mage::helper('manageapi')->__('Category'),
            'title' => Mage::helper('manageapi')->__('Category'),
            'name' => 'category',
            'required' => false,
            'after_element_html' => "<br /><small>Category ID</small>",
        ));

        $fieldset->addField('brand', 'text', array(
            'label' => Mage::helper('manageapi')->__('Brand'),
            'title' => Mage::helper('manageapi')->__('Brand'),
            'name' => 'brand',
            'required' => false,
            'after_element_html' => "<br /><small>Brand</small>",
        ));

        $fieldset->addField('specialOffer', 'select', array(
            'label' => Mage::helper('manageapi')->__('specialOffer'),
            'title' => Mage::helper('manageapi')->__('specialOffer'),
            'name' => 'specialOffer',
            'required' => false,
            'value' => 'all',
            'values' => array(
                '' => Mage::helper('manageapi')->__('All'),
                'rollback' => Mage::helper('manageapi')->__('rollback'),
                'clearance' => Mage::helper('manageapi')->__('clearance'),
                'specialbuy' => Mage::helper('manageapi')->__('specialbuy'),
            ),
            'after_element_html' => "<br /><small>Special offer like rollback, clearance etc</small>",
        ));

        $fieldset->addField('is_continue', 'select', array(
            'label' => Mage::helper('manageapi')->__('Continue From Last Page'),
            'title' => Mage::helper('manageapi')->__('Continue From Last Page'),
            'name' => 'is_continue',
            'required' => false,
            'value' => '2',
            'values' => array(
                '1' => Mage::helper('manageapi')->__('Yes'),
                '2' => Mage::helper('manageapi')->__('No'),
            ),
            'after_element_html' => "<br /><small>Continue running from the last page or running the API from beginning</small>",
        ));

        $form->setValues($data);
        return parent::_prepareForm();
    }
}
