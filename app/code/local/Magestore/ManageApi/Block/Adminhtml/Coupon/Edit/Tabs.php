<?php

class Magestore_Manageapi_Block_Adminhtml_Coupon_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
	public function __construct(){
		parent::__construct();
		$this->setId('coupon_api_tabs');
		$this->setDestElementId('edit_form');
		$this->setTitle(Mage::helper('manageapi')->__('Coupon API Information'));
	}

	protected function _beforeToHtml(){
		$this->addTab('form_section', array(
			'label'	 => Mage::helper('manageapi')->__('Coupon API Information'),
			'title'	 => Mage::helper('manageapi')->__('Coupon API Information'),
			'content'	 => $this->getLayout()->createBlock('manageapi/adminhtml_coupon_edit_tab_form')->toHtml(),
		));
		return parent::_beforeToHtml();
	}
}