<?php

class Magestore_Manageapi_Block_Adminhtml_Coupon_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
	protected function _prepareForm(){
		$form = new Varien_Data_Form();
		$this->setForm($form);
		
		if (Mage::getSingleton('adminhtml/session')->getDealsData()){
			$data = Mage::getSingleton('adminhtml/session')->getDealsData();
			Mage::getSingleton('adminhtml/session')->setDealsData(null);
		}elseif(Mage::registry('deals_data'))
			$data = Mage::registry('deals_data')->getData();
		
		$fieldset = $form->addFieldset('manageapi_form', array('legend'=>Mage::helper('manageapi')->__('Coupon API
		information')));

        $data['network'] = 1;
        $data['resultsperpage'] = 500;

        $fieldset->addField('category', 'text', array(
            'label'    => Mage::helper('manageapi')->__('Category'),
            'title'    => Mage::helper('manageapi')->__('Category'),
            'name'     => 'category',
            'required' => false,
            'after_element_html' => "<small>Filter by one or more Category IDs (Separate multiple by using the pipe character ['|']). You can find
more detailed informatin in this <a href='".$this->getSkinUrl('images/Appendix_1.png')."' target='_blank'>link</a>.</small>",

        ));

        $fieldset->addField('promotiontype', 'text', array(
            'label'    => Mage::helper('manageapi')->__('Promotion Type'),
            'title'    => Mage::helper('manageapi')->__('Promotion Type'),
            'name'     => 'promotiontype',
            'required' => false,
            'after_element_html' => "<small>Filter by one of more Promotion Type IDs (Separate multiple by using the pipe character ['|']). You can find
more detailed informatin in this <a href='".$this->getSkinUrl('images/Appendix_2.png')."' target='_blank'>link</a>.</small>",

        ));

        $fieldset->addField('network', 'text', array(
            'label'    => Mage::helper('manageapi')->__('Network'),
            'title'    => Mage::helper('manageapi')->__('Network'),
            'name'     => 'network',
            'required' => false,
            'after_element_html' => "<small>Filter by one or more networks (Separate multiple by using the pipe character ['|'])</small>",

        ));

        $fieldset->addField('mid', 'text', array(
            'label'    => Mage::helper('manageapi')->__('MID'),
            'title'    => Mage::helper('manageapi')->__('MID'),
            'name'     => 'mid',
            'required' => false,
            'after_element_html' => "<small>Filter by one or more Advertiser IDs (Separate multiple by using the pipe character ['|'])</small>",

        ));

        $fieldset->addField('resultsperpage', 'text', array(
            'label'    => Mage::helper('manageapi')->__('Results Per Page'),
            'title'    => Mage::helper('manageapi')->__('Results Per Page'),
            'name'     => 'resultsperpage',
            'required' => false,
            'after_element_html' => "<small>Specify a number between 1 and 500 to indicate desired result count per page. Specify 0 to acquire total result count</small>",

        ));

        $fieldset->addField('pagenumber', 'text', array(
            'label'    => Mage::helper('manageapi')->__('Page Number'),
            'title'    => Mage::helper('manageapi')->__('Page Number'),
            'name'     => 'pagenumber',
            'required' => false,
            'after_element_html' => "<small>Page number of the results (Default: 1)</small>",

        ));

        $fieldset->addField('promocat', 'text', array(
            'label'    => Mage::helper('manageapi')->__('Promotion Category'),
            'title'    => Mage::helper('manageapi')->__('Promotion Category'),
            'name'     => 'promocat',
            'required' => false,
            'after_element_html' => "<small>Specify '1' with no other options set to acquire all valid Category and Promotion Type IDs</small>",

        ));

		$form->setValues($data);
		return parent::_prepareForm();
	}
}