<?php

class Magestore_Manageapi_Block_Adminhtml_Coupon_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
	public function __construct(){
		parent::__construct();
		$this->setId('couponGrid');
		$this->setDefaultSort('deals_id');
		$this->setDefaultDir('DESC');
		$this->setSaveParametersInSession(true);
	}

	protected function _prepareCollection(){
		$collection = Mage::getModel('manageapi/deals')->getCollection()
			->setOrder('deals_id', 'desc')
		;
		$this->setCollection($collection);
		return parent::_prepareCollection();
	}

	protected function _prepareColumns(){
		$this->addColumn('deals_id', array(
			'header'	=> Mage::helper('manageapi')->__('ID'),
			'align'	 =>'right',
			'width'	 => '50px',
			'index'	 => 'deals_id',
		));

		$this->addColumn('categories', array(
			'header'	=> Mage::helper('manageapi')->__('Categories'),
			'align'	 =>'left',
			'index'	 => 'categories',
			'renderer'  => 'Magestore_ManageApi_Block_Adminhtml_Renderer_Coupon_Category',
		));

		$this->addColumn('promotiontypes', array(
			'header'	=> Mage::helper('manageapi')->__('Promotion Types'),
			'index'	 => 'promotiontypes',
			'renderer'  => 'Magestore_ManageApi_Block_Adminhtml_Renderer_Coupon_Promotion',
		)); 

		$this->addColumn('advertisername', array(
			'header'	=> Mage::helper('manageapi')->__('Advertiser Name'),
			'index'	 => 'advertisername',
		));

		$this->addColumn('offerdescription', array(
			'header'	=> Mage::helper('manageapi')->__('Offer Description'),
			'index'	 => 'offerdescription',
		));

		$this->addColumn('couponcode', array(
			'header'	=> Mage::helper('manageapi')->__('Coupon Code'),
			'index'	 => 'couponcode',
		));

		$this->addColumn('offerstartdate', array(
			'header' => Mage::helper('manageapi')->__('Start Date'),
			'align' => 'left',
			'index' => 'offerstartdate',
			'type' => 'date'
		));

		$this->addColumn('offerenddate', array(
			'header' => Mage::helper('manageapi')->__('End Date'),
			'align' => 'left',
			'index' => 'offerenddate',
			'type' => 'date'
		));

		$this->addColumn('clickurl', array(
			'header'	=> Mage::helper('manageapi')->__('Click URL'),
			'index'	 => 'clickurl',
		));

		$this->addColumn('product_created_at', array(
			'header' => Mage::helper('manageapi')->__('Product Created At'),
			'align' => 'left',
			'index' => 'product_created_at',
			'type' => 'date'
		));

		$this->addColumn('product_id', array(
			'header' => Mage::helper('manageapi')->__('Product ID'),
			'align' => 'left',
			'width'	 => '50px',
			'index' => 'product_id',
			'renderer'  => 'Magestore_ManageApi_Block_Adminhtml_Renderer_Coupon_Product',
		));

		$this->addColumn('network', array(
			'header'	=> Mage::helper('manageapi')->__('Network'),
			'index'	 => 'network',
		));

		$this->addColumn('created_at', array(
			'header' => Mage::helper('manageapi')->__('Created At'),
			'align' => 'left',
			'index' => 'created_at',
			'type' => 'date'
		));

		$this->addColumn('couponrestriction', array(
			'header'	=> Mage::helper('manageapi')->__('Coupon Restriction'),
			'index'	 => 'couponrestriction',
		));

		$this->addColumn('impressionpixel', array(
			'header'	=> Mage::helper('manageapi')->__('Impression Pixel'),
			'index'	 => 'impressionpixel',
		));

		$this->addColumn('advertiserid', array(
			'header'	=> Mage::helper('manageapi')->__('Advertiser ID'),
			'index'	 => 'advertiserid',
		));

		$this->addColumn('updated_at', array(
			'header' => Mage::helper('manageapi')->__('Updated At'),
			'align' => 'left',
			'index' => 'updated_at',
			'type' => 'date'
		));


		$this->addExportType('*/*/exportCsv', Mage::helper('manageapi')->__('CSV'));
		$this->addExportType('*/*/exportXml', Mage::helper('manageapi')->__('XML'));

		return parent::_prepareColumns();
	}

	protected function _prepareMassaction(){
		$this->setMassactionIdField('manageapi_id');
		$this->getMassactionBlock()->setFormFieldName('manageapi');

		$this->getMassactionBlock()->addItem('delete', array(
			'label'		=> Mage::helper('manageapi')->__('Delete'),
			'url'		=> $this->getUrl('*/*/massDelete'),
			'confirm'	=> Mage::helper('manageapi')->__('Are you sure?')
		));

		$this->getMassactionBlock()->addItem('synchronize', array(
			'label'		=> Mage::helper('manageapi')->__('Synchronize Product(s)'),
			'url'		=> $this->getUrl('*/*/massSynchronize')
		));

		return $this;
	}

	public function getRowUrl($row){
		return $this->getUrl('*/*/edit', array('id' => $row->getId()));
	}
}
