<?php

class Magestore_Manageapi_Block_Adminhtml_Merchant_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
	protected function _prepareForm(){
		$form = new Varien_Data_Form();
		$this->setForm($form);
		
		if (Mage::getSingleton('adminhtml/session')->getDealsData()){
			$data = Mage::getSingleton('adminhtml/session')->getDealsData();
			Mage::getSingleton('adminhtml/session')->setDealsData(null);
		}elseif(Mage::registry('deals_data'))
			$data = Mage::registry('deals_data')->getData();
		
		$fieldset = $form->addFieldset('manageapi_form', array('legend'=>Mage::helper('manageapi')->__('Coupon API
		information')));

        $data['page'] = 1;
        $data['results_per_page'] = 20;

        $fieldset->addField('category', 'select', array(
            'label'    => Mage::helper('manageapi')->__('Category'),
            'title'    => Mage::helper('manageapi')->__('Category'),
            'name'     => 'category[]',
            'required' => false,
            'values' => Mage::helper('manageapi/merchant')->getListCategories(),
            'after_element_html' => "<br /><small>A category ID to filter the merchants.</small>",

        ));

        $fieldset->addField('network', 'select', array(
            'label'    => Mage::helper('manageapi')->__('Network'),
            'title'    => Mage::helper('manageapi')->__('Network'),
            'name'     => 'network',
            'required' => false,
            'values' => Mage::helper('manageapi/merchant')->getListNetworks(),
            'after_element_html' => "<br /><small>The affiliate network ID to filter the merchants on.</small>",

        ));

        $fieldset->addField('page', 'text', array(
            'label'    => Mage::helper('manageapi')->__('Page'),
            'title'    => Mage::helper('manageapi')->__('Page'),
            'name'     => 'page',
            'required' => false,
            'after_element_html' => "<br /><small>The page number of results to return. Allowed values 1-15. Default 1</small>",

        ));

        $fieldset->addField('results_per_page', 'text', array(
            'label'    => Mage::helper('manageapi')->__('Results Per Page'),
            'title'    => Mage::helper('manageapi')->__('Results Per Page'),
            'name'     => 'results_per_page',
            'required' => false,
            'after_element_html' => "<br /><small>The number of merchants to return in every response. Allowed values 1-100. Default 20</small>",

        ));

		$form->setValues($data);
		return parent::_prepareForm();
	}
}
