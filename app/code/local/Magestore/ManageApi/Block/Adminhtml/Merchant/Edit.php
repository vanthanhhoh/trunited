<?php

class Magestore_Manageapi_Block_Adminhtml_Merchant_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
	public function __construct(){
		parent::__construct();
		
		$this->_objectId = 'id';
		$this->_blockGroup = 'manageapi';
		$this->_controller = 'adminhtml_merchant';
		
		$this->_updateButton('save', 'label', Mage::helper('manageapi')->__('Run'));
	}

	public function getHeaderText(){
		if(Mage::registry('merchant_data') && Mage::registry('merchant_data')->getId())
			return Mage::helper('manageapi')->__("Edit Item '%s'", $this->htmlEscape(Mage::registry('merchant_data')->getTitle()));
		return Mage::helper('manageapi')->__('Run Popshops Merchant API');
	}
}
