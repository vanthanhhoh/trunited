<?php

class Magestore_Manageapi_Block_Adminhtml_Merchant_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
	public function __construct(){
		parent::__construct();
		$this->setId('merchantGrid');
		$this->setDefaultSort('popshops_merchant_id');
		$this->setDefaultDir('DESC');
		$this->setSaveParametersInSession(true);
	}

	protected function _prepareCollection(){
		$collection = Mage::getModel('manageapi/popshopsmerchant')->getCollection()
			->setOrder('popshops_merchant_id', 'desc')
		;
		$this->setCollection($collection);
		return parent::_prepareCollection();
	}

	protected function _prepareColumns(){
		$this->addColumn('popshops_merchant_id', array(
			'header'	=> Mage::helper('manageapi')->__('ID'),
			'align'	 =>'right',
			'width'	 => '50px',
			'index'	 => 'popshops_merchant_id',
		));

		$this->addColumn('merchant_id', array(
			'header'	=> Mage::helper('manageapi')->__('Merchant ID'),
			'align'	 =>'right',
			'width'	 => '50px',
			'index'	 => 'merchant_id',
		));

		$this->addColumn('logo_url', array(
			'header' => Mage::helper('manageapi')->__('Logo'),
			'index' => 'logo_url',
			'width'	 => '150px',
			'renderer' => 'Magestore_ManageApi_Block_Adminhtml_Renderer_Popshops_Merchant_Logo',
		));

		$this->addColumn('name', array(
			'header'	=> Mage::helper('manageapi')->__('Name'),
			'align'	 =>'left',
			'index'	 => 'name',
		));

		$this->addColumn('network', array(
			'header'	=> Mage::helper('manageapi')->__('Network'),
			'width'	 => '50px',
			'index'	 => 'network',
		));

		$this->addColumn('url', array(
			'header' => Mage::helper('manageapi')->__('URL'),
			'index' => 'url',
			'width'	 => '60px',
			'renderer' => 'Magestore_ManageApi_Block_Adminhtml_Renderer_Popshops_Merchant_Link',
		));

		$this->addColumn('site_url', array(
			'header' => Mage::helper('manageapi')->__('Site URL'),
			'index' => 'site_url',
			'width'	 => '70px',
			'renderer' => 'Magestore_ManageApi_Block_Adminhtml_Renderer_Popshops_Merchant_Link',
		));

		$this->addColumn('created_at', array(
			'header' => Mage::helper('manageapi')->__('Created At'),
			'align' => 'left',
			'index' => 'created_at',
			'type' => 'date'
		));

		$this->addColumn('updated_at', array(
			'header' => Mage::helper('manageapi')->__('Updated At'),
			'align' => 'left',
			'index' => 'updated_at',
			'type' => 'date'
		));

		$this->addColumn('product_count', array(
			'header'	=> Mage::helper('manageapi')->__('Product Count'),
			'width'	 => '50px',
			'index'	 => 'product_count',
		));

		$this->addColumn('deal_count', array(
			'header'	=> Mage::helper('manageapi')->__('Deal Count'),
			'width'	 => '50px',
			'index'	 => 'deal_count',
		));

		$this->addColumn('network_merchant_id', array(
			'header'	=> Mage::helper('manageapi')->__('Network Merchant ID'),
			'width'	 => '50px',
			'index'	 => 'network_merchant_id',
		));

		$this->addColumn('country', array(
			'header'	=> Mage::helper('manageapi')->__('Country'),
			'width'	 => '50px',
			'index'	 => 'country',
		));

		$this->addColumn('category', array(
			'header'	=> Mage::helper('manageapi')->__('Category'),
			'width'	 => '50px',
			'index'	 => 'category',
		));

		$this->addColumn('merchant_type', array(
			'header'	=> Mage::helper('manageapi')->__('Merchant Type'),
			'width'	 => '50px',
			'index'	 => 'merchant_type',
		));


		$this->addExportType('*/*/exportCsv', Mage::helper('manageapi')->__('CSV'));
		$this->addExportType('*/*/exportXml', Mage::helper('manageapi')->__('XML'));

		return parent::_prepareColumns();
	}

	protected function _prepareMassaction(){
		$this->setMassactionIdField('popshops_merchant_id');
		$this->getMassactionBlock()->setFormFieldName('popshops_merchant');

		$this->getMassactionBlock()->addItem('delete', array(
			'label'		=> Mage::helper('manageapi')->__('Delete'),
			'url'		=> $this->getUrl('*/*/massDelete'),
			'confirm'	=> Mage::helper('manageapi')->__('Are you sure?')
		));

		return $this;
	}

	public function getRowUrl($row){
		return '';
	}
}
