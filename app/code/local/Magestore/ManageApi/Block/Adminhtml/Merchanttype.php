<?php

class Magestore_ManageApi_Block_Adminhtml_Merchanttype extends Mage_Adminhtml_Block_Widget_Grid_Container
{
	public function __construct(){
		$this->_controller = 'adminhtml_merchanttype';
		$this->_blockGroup = 'manageapi';
		$this->_headerText = Mage::helper('manageapi')->__('Merchant Types API');
		$this->_addButtonLabel = Mage::helper('manageapi')->__('Refresh');
		parent::__construct();
	}
}
