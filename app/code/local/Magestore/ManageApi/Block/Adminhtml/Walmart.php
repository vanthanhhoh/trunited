<?php

class Magestore_ManageApi_Block_Adminhtml_Walmart extends Mage_Adminhtml_Block_Widget_Grid_Container
{
	public function __construct(){
		$this->_controller = 'adminhtml_walmart';
		$this->_blockGroup = 'manageapi';
		$this->_headerText = Mage::helper('manageapi')->__('Manage Walmart API');
		$this->_addButtonLabel = Mage::helper('manageapi')->__('Try It');
		parent::__construct();

//		$url = Mage::helper('adminhtml')->getUrl('manageapiadmin/adminhtml_walmartbestsellers/new');
//        $this->_addButton('run_best_sellers', array(
//            'label'   => Mage::helper('manageapi')->__('Run Bestsellers API'),
//            'onclick' => "setLocation('$url')",
//            'class'   => 'add'
//        ));
	}
}
