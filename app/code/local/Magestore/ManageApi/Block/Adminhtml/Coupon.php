<?php

class Magestore_ManageApi_Block_Adminhtml_Coupon extends Mage_Adminhtml_Block_Widget_Grid_Container
{
	public function __construct(){
		$this->_controller = 'adminhtml_coupon';
		$this->_blockGroup = 'manageapi';
		$this->_headerText = Mage::helper('manageapi')->__('Manage Coupon API');
		$this->_addButtonLabel = Mage::helper('manageapi')->__('Try It');
		parent::__construct();
	}
}