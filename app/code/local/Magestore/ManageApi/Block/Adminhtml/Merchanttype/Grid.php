<?php

class Magestore_Manageapi_Block_Adminhtml_Merchanttype_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
	public function __construct(){
		parent::__construct();
		$this->setId('merchanttypeGrid');
		$this->setDefaultSort('merchant_type_id');
		$this->setDefaultDir('DESC');
		$this->setSaveParametersInSession(true);
	}

	protected function _prepareCollection(){
		$collection = Mage::getModel('manageapi/merchanttype')->getCollection()
			->setOrder('merchant_type_id', 'desc')
		;
		$this->setCollection($collection);
		return parent::_prepareCollection();
	}

	protected function _prepareColumns(){
		$this->addColumn('merchant_type_id', array(
			'header'	=> Mage::helper('manageapi')->__('ID'),
			'align'	 =>'right',
			'width'	 => '50px',
			'index'	 => 'merchant_type_id',
		));

		$this->addColumn('type_id', array(
			'header'	=> Mage::helper('manageapi')->__('Merchant Type ID'),
			'align'	 =>'right',
			'width'	 => '50px',
			'index'	 => 'type_id',
		));

		$this->addColumn('name', array(
			'header'	=> Mage::helper('manageapi')->__('Name'),
			'align'	 =>'left',
			'index'	 => 'name',
		));

		$this->addColumn('merchant_count', array(
			'header'	=> Mage::helper('manageapi')->__('Merchant Count'),
			'width'	 => '50px',
			'index'	 => 'merchant_count',
		));

		$this->addColumn('created_at', array(
			'header' => Mage::helper('manageapi')->__('Created At'),
			'align' => 'left',
			'index' => 'created_at',
			'type' => 'date'
		));

		$this->addColumn('updated_at', array(
			'header' => Mage::helper('manageapi')->__('Updated At'),
			'align' => 'left',
			'index' => 'updated_at',
			'type' => 'date'
		));

		$this->addExportType('*/*/exportCsv', Mage::helper('manageapi')->__('CSV'));
		$this->addExportType('*/*/exportXml', Mage::helper('manageapi')->__('XML'));

		return parent::_prepareColumns();
	}

	protected function _prepareMassaction(){
		$this->setMassactionIdField('merchant_type_id');
		$this->getMassactionBlock()->setFormFieldName('merchanttype');

		$this->getMassactionBlock()->addItem('delete', array(
			'label'		=> Mage::helper('manageapi')->__('Delete'),
			'url'		=> $this->getUrl('*/*/massDelete'),
			'confirm'	=> Mage::helper('manageapi')->__('Are you sure?')
		));

		return $this;
	}

	public function getRowUrl($row){
		return '';
	}
}
