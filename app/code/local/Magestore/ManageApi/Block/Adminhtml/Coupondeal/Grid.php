<?php

class Magestore_Manageapi_Block_Adminhtml_Coupondeal_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
	public function __construct(){
		parent::__construct();
		$this->setId('coupon_dealGrid');
		$this->setDefaultSort('coupon_deal_id');
		$this->setDefaultDir('DESC');
		$this->setSaveParametersInSession(true);
	}

	protected function _prepareCollection(){
		$collection = Mage::getModel('manageapi/coupondeal')->getCollection()
			->setOrder('coupon_deal_id', 'desc')
		;
		$this->setCollection($collection);
		return parent::_prepareCollection();
	}

	protected function _prepareColumns(){
		$this->addColumn('coupon_deal_id', array(
			'header'	=> Mage::helper('manageapi')->__('ID'),
			'align'	 =>'right',
			'width'	 => '50px',
			'index'	 => 'coupon_deal_id',
		));

		$this->addColumn('deal_id', array(
			'header'	=> Mage::helper('manageapi')->__('Primary ID'),
			'align'	 =>'right',
			'width'	 => '50px',
			'index'	 => 'deal_id',
		));

		$this->addColumn('image_url', array(
			'header' => Mage::helper('manageapi')->__('Image'),
			'index' => 'image_url',
			'renderer' => 'Magestore_ManageApi_Block_Adminhtml_Renderer_Popshops_Coupondeal_Image',
		));

		$this->addColumn('name', array(
			'header'	=> Mage::helper('manageapi')->__('Name'),
			'align'	 =>'left',
			'index'	 => 'name',
		));

		$this->addColumn('url', array(
			'header' => Mage::helper('manageapi')->__('URL'),
			'index' => 'url',
			'width'	 => '50px',
			'renderer' => 'Magestore_ManageApi_Block_Adminhtml_Renderer_Popshops_Coupondeal_Url',
		));

		$this->addColumn('merchant', array(
			'header'	=> Mage::helper('manageapi')->__('Merchant'),
			'index'	 => 'merchant',
			'type'		=> 'options',
			'options'	 => Magestore_ManageApi_Model_Options::getMerchantsOption(),
		));

		$this->addColumn('deal_type', array(
			'header'	=> Mage::helper('manageapi')->__('Deal Type'),
			'index'	 => 'deal_type',
			'type'		=> 'options',
			'options'	 => Magestore_ManageApi_Model_Options::getDealTypesOption(),
		));

		$this->addColumn('sku', array(
			'header'	=> Mage::helper('manageapi')->__('Sku'),
			'index'	 => 'sku',
		));

		$this->addColumn('site_wide', array(
			'header' => Mage::helper('manageapi')->__('Site Wide'),
			'index' => 'site_wide',
			'width'	 => '50px',
		));

		$this->addColumn('code', array(
			'header' => Mage::helper('manageapi')->__('Code'),
			'index' => 'code',
		));

		$this->addColumn('start_on', array(
			'header' => Mage::helper('manageapi')->__('Start On'),
			'align' => 'left',
			'index' => 'start_on',
			'type' => 'date',
			'width'	 => '50px',
		));

		$this->addColumn('end_on', array(
			'header' => Mage::helper('manageapi')->__('End On'),
			'align' => 'left',
			'index' => 'end_on',
			'type' => 'date',
			'width'	 => '50px',
		));

		$this->addColumn('product_created_at', array(
			'header' => Mage::helper('manageapi')->__('Product Created At'),
			'align' => 'left',
			'index' => 'product_created_at',
			'type' => 'date'
		));

		$this->addColumn('product_id', array(
			'header' => Mage::helper('manageapi')->__('Product ID'),
			'align' => 'left',
			'width'	 => '50px',
			'index' => 'product_id',
			'renderer'  => 'Magestore_ManageApi_Block_Adminhtml_Renderer_Coupon_Product',
		));

		$this->addColumn('restrictions', array(
			'header' => Mage::helper('manageapi')->__('Restriction'),
			'index' => 'restrictions',
		));

		$this->addColumn('created_at', array(
			'header' => Mage::helper('manageapi')->__('Created At'),
			'align' => 'left',
			'index' => 'created_at',
			'type' => 'date'
		));

		$this->addColumn('updated_at', array(
			'header' => Mage::helper('manageapi')->__('Updated At'),
			'align' => 'left',
			'index' => 'updated_at',
			'type' => 'date'
		));

		$this->addColumn('description', array(
			'header' => Mage::helper('manageapi')->__('Description'),
			'index' => 'description',
		));

		$this->addExportType('*/*/exportCsv', Mage::helper('manageapi')->__('CSV'));
		$this->addExportType('*/*/exportXml', Mage::helper('manageapi')->__('XML'));

		return parent::_prepareColumns();
	}

	protected function _prepareMassaction(){
		$this->setMassactionIdField('coupon_deal_id');
		$this->getMassactionBlock()->setFormFieldName('coupondeal');

		$this->getMassactionBlock()->addItem('delete', array(
			'label'		=> Mage::helper('manageapi')->__('Delete'),
			'url'		=> $this->getUrl('*/*/massDelete'),
			'confirm'	=> Mage::helper('manageapi')->__('Are you sure?')
		));

		$this->getMassactionBlock()->addItem('synchronize', array(
			'label'		=> Mage::helper('manageapi')->__('Synchronize Product(s)'),
			'url'		=> $this->getUrl('*/*/massSynchronize')
		));

		return $this;
	}

	public function getRowUrl($row){
		return '';
	}
}
