<?php

class Magestore_Manageapi_Block_Adminhtml_Coupondeal_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form();
        $this->setForm($form);

        if (Mage::getSingleton('adminhtml/session')->getCoupondealData()) {
            $data = Mage::getSingleton('adminhtml/session')->getCoupondealData();
            Mage::getSingleton('adminhtml/session')->setCoupondealData(null);
        } elseif (Mage::registry('coupondeal_data'))
            $data = Mage::registry('coupondeal_data')->getData();

        $dateTimeFormatIso = Mage::app()->getLocale()->getDateTimeFormat(Mage_Core_Model_Locale::FORMAT_TYPE_SHORT);

        $fieldset = $form->addFieldset('manageapi_form', array('legend' => Mage::helper('manageapi')->__('Coupons and Deals API
		information')));

        $data['page'] = 1;
        $data['results_per_page'] = 20;
        $data['site_wide'] = 'all';
        $data['sort_deal'] = '';

        $fieldset->addField('start_on', 'datetime', array(
            'label' => Mage::helper('manageapi')->__('Start On'),
            'title' => Mage::helper('manageapi')->__('Start On'),
            'time' => true,
            'name' => 'start_on',
            'image' => $this->getSkinUrl('images/grid-cal.gif'),
            'format' => $dateTimeFormatIso,
            'required' => false,
        ));

        $fieldset->addField('end_on', 'datetime', array(
            'label' => Mage::helper('manageapi')->__('End On'),
            'title' => Mage::helper('manageapi')->__('End On'),
            'time' => true,
            'name' => 'end_on',
            'image' => $this->getSkinUrl('images/grid-cal.gif'),
            'format' => $dateTimeFormatIso,
            'required' => false,
        ));

        $fieldset->addField('deal_type', 'text', array(
            'label' => Mage::helper('manageapi')->__('Deal Type'),
            'title' => Mage::helper('manageapi')->__('Deal Type'),
            'name' => 'deal_type',
            'required' => false,
            'after_element_html' => "<br /><small>The deal type ID a deal belongs to.</small>",
        ));

        $fieldset->addField('merchant', 'text', array(
            'label' => Mage::helper('manageapi')->__('Merchant'),
            'title' => Mage::helper('manageapi')->__('Merchant'),
            'name' => 'merchant',
            'required' => false,
            'after_element_html' => "<br /><small>The merchant ID of the merchant a deal belongs to.</small>",
        ));

        $fieldset->addField('merchant_type', 'text', array(
            'label' => Mage::helper('manageapi')->__('Merchant Type'),
            'title' => Mage::helper('manageapi')->__('Merchant Type'),
            'name' => 'merchant_type',
            'required' => false,
            'after_element_html' => "<br /><small>The merchant type ID that a deal's merchant belongs to.</small>",
        ));

        $fieldset->addField('page', 'text', array(
            'label' => Mage::helper('manageapi')->__('Page'),
            'title' => Mage::helper('manageapi')->__('Page'),
            'name' => 'page',
            'required' => false,
            'after_element_html' => "<br /><small>The page number of results to return. Allowed values 1-10. Default 1</small>",
        ));

        $fieldset->addField('results_per_page', 'text', array(
            'label' => Mage::helper('manageapi')->__('Results Per Page'),
            'title' => Mage::helper('manageapi')->__('Results Per Page'),
            'name' => 'results_per_page',
            'required' => false,
            'after_element_html' => "<br /><small>The number of merchants to return in every response. Allowed values 1-100. Default 20</small>",
        ));

        $fieldset->addField('site_wide', 'select', array(
            'label' => Mage::helper('manageapi')->__('Site Wide'),
            'title' => Mage::helper('manageapi')->__('Site Wide'),
            'name' => 'site_wide',
            'required' => false,
            'value' => 'all',
            'values' => array(
                'all' => Mage::helper('manageapi')->__('All'),
                'yes' => Mage::helper('manageapi')->__('Yes'),
                'no' => Mage::helper('manageapi')->__('No'),
            ),
            'after_element_html' => "<br /><small>Indicates whether or not a deal applies site wide on a merchant's site, as opposed to being for a specific product or specific category within the merchant's site. Default: all</small>",
        ));

        $fieldset->addField('sort_deal', 'select', array(
            'label' => Mage::helper('manageapi')->__('Sort Deal'),
            'title' => Mage::helper('manageapi')->__('Sort Deal'),
            'name' => 'sort_deal',
            'required' => false,
            'value' => '',
            'values' => array(
                '' => Mage::helper('manageapi')->__('Select..'),
                'relevance' => Mage::helper('manageapi')->__('Relevance'),
                'name_asc' => Mage::helper('manageapi')->__('Name ASC'),
                'name_desc' => Mage::helper('manageapi')->__('Name DESC'),
                'start_on_asc' => Mage::helper('manageapi')->__('Start On ASC'),
                'start_on_desc' => Mage::helper('manageapi')->__('Start On DESC'),
                'end_on_asc' => Mage::helper('manageapi')->__('End On ASC'),
                'end_on_desc' => Mage::helper('manageapi')->__('End On DESC'),
                'merchant_name_asc' => Mage::helper('manageapi')->__('Merchant Name ASC'),
                'merchant_name_desc' => Mage::helper('manageapi')->__('Merchant Name DESC'),
            ),
        ));

        $fieldset->addField('tracking_id', 'text', array(
            'label' => Mage::helper('manageapi')->__('Tracking ID'),
            'title' => Mage::helper('manageapi')->__('Tracking ID'),
            'name' => 'tracking_id',
            'required' => false,
            'after_element_html' => "<br /><small>Optional custom tracking id to be passed along in affiliate links to the affiliate network.</small>",
        ));

        $form->setValues($data);
        return parent::_prepareForm();
    }
}
