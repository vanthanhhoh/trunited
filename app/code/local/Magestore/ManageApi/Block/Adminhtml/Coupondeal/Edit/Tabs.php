<?php

class Magestore_Manageapi_Block_Adminhtml_Coupondeal_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
	public function __construct(){
		parent::__construct();
		$this->setId('marchant_api_tabs');
		$this->setDestElementId('edit_form');
		$this->setTitle(Mage::helper('manageapi')->__('Coupons and Deals API Information'));
	}

	protected function _beforeToHtml(){
		$this->addTab('form_section', array(
			'label'	 => Mage::helper('manageapi')->__('Coupons and Deals API Information'),
			'title'	 => Mage::helper('manageapi')->__('Coupons and Deals API Information'),
			'content'	 => $this->getLayout()->createBlock('manageapi/adminhtml_coupondeal_edit_tab_form')->toHtml(),
		));
		return parent::_beforeToHtml();
	}
}
