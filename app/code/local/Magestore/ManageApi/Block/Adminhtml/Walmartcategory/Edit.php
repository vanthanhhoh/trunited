<?php

class Magestore_Manageapi_Block_Adminhtml_Walmartcategory_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
	public function __construct(){
		parent::__construct();
		
		$this->_objectId = 'id';
		$this->_blockGroup = 'manageapi';
		$this->_controller = 'adminhtml_walmartcategory';
		
		$this->_updateButton('save', 'label', Mage::helper('manageapi')->__('Run'));
	}

	public function getHeaderText(){
		if(Mage::registry('category_data') && Mage::registry('category_data')->getId())
			return Mage::helper('manageapi')->__("Run API");
		return Mage::helper('manageapi')->__('Run Category Walmart API');
	}
}
