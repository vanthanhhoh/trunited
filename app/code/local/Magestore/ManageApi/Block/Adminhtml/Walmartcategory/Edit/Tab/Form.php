<?php

class Magestore_Manageapi_Block_Adminhtml_Walmartcategory_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form();
        $this->setForm($form);

        if (Mage::getSingleton('adminhtml/session')->getCategoryData()) {
            $data = Mage::getSingleton('adminhtml/session')->getCategoryData();
            Mage::getSingleton('adminhtml/session')->setCategoryData(null);
        } elseif (Mage::registry('category_data'))
            $data = Mage::registry('category_data')->getData();

        $fieldset = $form->addFieldset('manageapi_form', array(
            'legend' => Mage::helper('manageapi')->__('Category Walmart API information'))
        );

        $data['format'] = 'json';

        $fieldset->addField('format', 'select', array(
            'label' => Mage::helper('manageapi')->__('Format'),
            'title' => Mage::helper('manageapi')->__('Format'),
            'name' => 'format',
            'required' => false,
            'value' => 'json',
            'values' => array(
                'json' => Mage::helper('manageapi')->__('json'),
                'xml' => Mage::helper('manageapi')->__('xml'),
            ),
            'after_element_html' => "<br /><small>Format of the output in either JSON or XML</small>",
        ));

        $form->setValues($data);
        return parent::_prepareForm();
    }
}
