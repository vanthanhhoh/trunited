<?php

class Magestore_Manageapi_Block_Adminhtml_Walmartcategory_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
	public function __construct(){
		parent::__construct();
		$this->setId('walmart_category_tabs');
		$this->setDestElementId('edit_form');
		$this->setTitle(Mage::helper('manageapi')->__('Category Walmart API Information'));
	}

	protected function _beforeToHtml(){
		$this->addTab('form_section', array(
			'label'	 => Mage::helper('manageapi')->__('Category Walmart API Information'),
			'title'	 => Mage::helper('manageapi')->__('Category Walmart API Information'),
			'content'	 => $this->getLayout()->createBlock('manageapi/adminhtml_walmartcategory_edit_tab_form')->toHtml(),
		));
		return parent::_beforeToHtml();
	}
}
