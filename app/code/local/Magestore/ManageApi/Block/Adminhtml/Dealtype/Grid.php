<?php

class Magestore_Manageapi_Block_Adminhtml_Dealtype_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
	public function __construct(){
		parent::__construct();
		$this->setId('dealtypeGrid');
		$this->setDefaultSort('deal_type_id');
		$this->setDefaultDir('DESC');
		$this->setSaveParametersInSession(true);
	}

	protected function _prepareCollection(){
		$collection = Mage::getModel('manageapi/dealtype')->getCollection()
			->setOrder('deal_type_id', 'desc')
		;
		$this->setCollection($collection);
		return parent::_prepareCollection();
	}

	protected function _prepareColumns(){
		$this->addColumn('deal_type_id', array(
			'header'	=> Mage::helper('manageapi')->__('ID'),
			'align'	 =>'right',
			'width'	 => '50px',
			'index'	 => 'deal_type_id',
		));

		$this->addColumn('type_id', array(
			'header'	=> Mage::helper('manageapi')->__('Deal Type ID'),
			'align'	 =>'right',
			'width'	 => '50px',
			'index'	 => 'type_id',
		));

		$this->addColumn('name', array(
			'header'	=> Mage::helper('manageapi')->__('Name'),
			'align'	 =>'left',
			'index'	 => 'name',
		));

		$this->addColumn('created_at', array(
			'header' => Mage::helper('manageapi')->__('Created At'),
			'align' => 'left',
			'index' => 'created_at',
			'type' => 'date'
		));

		$this->addColumn('updated_at', array(
			'header' => Mage::helper('manageapi')->__('Updated At'),
			'align' => 'left',
			'index' => 'updated_at',
			'type' => 'date'
		));

		$this->addExportType('*/*/exportCsv', Mage::helper('manageapi')->__('CSV'));
		$this->addExportType('*/*/exportXml', Mage::helper('manageapi')->__('XML'));

		return parent::_prepareColumns();
	}

	protected function _prepareMassaction(){
		$this->setMassactionIdField('deal_type_id');
		$this->getMassactionBlock()->setFormFieldName('dealtype');

		$this->getMassactionBlock()->addItem('delete', array(
			'label'		=> Mage::helper('manageapi')->__('Delete'),
			'url'		=> $this->getUrl('*/*/massDelete'),
			'confirm'	=> Mage::helper('manageapi')->__('Are you sure?')
		));

		return $this;
	}

	public function getRowUrl($row){
		return '';
	}
}
