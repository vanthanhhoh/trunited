<?php

class Magestore_ManageApi_Block_Adminhtml_Walmartbestsellers extends Mage_Adminhtml_Block_Widget_Grid_Container
{
	public function __construct(){
		$this->_controller = 'adminhtml_walmartbestsellers';
		$this->_blockGroup = 'manageapi';
		$this->_headerText = Mage::helper('manageapi')->__('Manage Bestsellers Walmart API');
		$this->_addButtonLabel = Mage::helper('manageapi')->__('Try It');
		parent::__construct();
	}
}
