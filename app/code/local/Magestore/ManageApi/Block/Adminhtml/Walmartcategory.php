<?php

class Magestore_ManageApi_Block_Adminhtml_Walmartcategory extends Mage_Adminhtml_Block_Widget_Grid_Container
{
	public function __construct(){
		$this->_controller = 'adminhtml_walmartcategory';
		$this->_blockGroup = 'manageapi';
		$this->_headerText = Mage::helper('manageapi')->__('Manage Category Walmart API');
		$this->_addButtonLabel = Mage::helper('manageapi')->__('Refresh');
		parent::__construct();
	}
}
