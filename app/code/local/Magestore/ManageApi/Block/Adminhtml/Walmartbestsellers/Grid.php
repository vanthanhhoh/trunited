<?php

class Magestore_Manageapi_Block_Adminhtml_Walmartbestsellers_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('walmartBestsellersGrid');
        $this->setDefaultSort('level');
        $this->setDefaultDir('ASC');
        $this->setSaveParametersInSession(true);
    }

    protected function _prepareCollection()
    {
        $collection = Mage::getModel('manageapi/walmart')->getCollection()
            ->setOrder('level', 'asc')
        ;
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $this->addColumn('category_id', array(
            'header' => Mage::helper('manageapi')->__('ID'),
            'align' => 'right',
            'width' => '50px',
            'index' => 'category_id',
        ));

        $this->addColumn('id', array(
            'header' => Mage::helper('manageapi')->__('Category ID'),
            'align' => 'right',
            'width' => '50px',
            'index' => 'id',
        ));

        $this->addColumn('name', array(
            'header' => Mage::helper('manageapi')->__('Name'),
            'index' => 'name',
        ));

        $this->addColumn('path', array(
            'header' => Mage::helper('manageapi')->__('Path'),
            'align' => 'left',
            'index' => 'path',
        ));

        $this->addColumn('level', array(
            'header' => Mage::helper('manageapi')->__('Level'),
            'index' => 'level',
            'width' => '50px',
            'type' => 'options',
            'options' => Mage::getSingleton('manageapi/options')->getLevelOption(),
        ));

        $this->addColumn('parent_id', array(
            'header' => Mage::helper('manageapi')->__('Parent Category ID'),
            'index' => 'parent_id',
        ));

        $this->addColumn('created_at', array(
            'header' => Mage::helper('manageapi')->__('Created At'),
            'align' => 'left',
            'index' => 'created_at',
            'type' => 'date'
        ));

        $this->addColumn('updated_at', array(
            'header' => Mage::helper('manageapi')->__('Updated At'),
            'align' => 'left',
            'index' => 'updated_at',
            'type' => 'date'
        ));

        $this->addExportType('*/*/exportCsv', Mage::helper('manageapi')->__('CSV'));
        $this->addExportType('*/*/exportXml', Mage::helper('manageapi')->__('XML'));

        return parent::_prepareColumns();
    }

    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('category_id');
        $this->getMassactionBlock()->setFormFieldName('category');

        $this->getMassactionBlock()->addItem('delete', array(
            'label' => Mage::helper('manageapi')->__('Delete'),
            'url' => $this->getUrl('*/*/massDelete'),
            'confirm' => Mage::helper('manageapi')->__('Are you sure?')
        ));

        return $this;
    }

    public function getRowUrl($row)
    {
        return '';
    }
}
