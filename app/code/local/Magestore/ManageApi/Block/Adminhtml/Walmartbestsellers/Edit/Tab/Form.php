<?php

class Magestore_Manageapi_Block_Adminhtml_Walmartbestsellers_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form();
        $this->setForm($form);

        if (Mage::getSingleton('adminhtml/session')->getCategoryData()) {
            $data = Mage::getSingleton('adminhtml/session')->getCategoryData();
            Mage::getSingleton('adminhtml/session')->setCategoryData(null);
        } elseif (Mage::registry('category_data'))
            $data = Mage::registry('category_data')->getData();

        $fieldset = $form->addFieldset('manageapi_form', array(
            'legend' => Mage::helper('manageapi')->__('Category Walmart API information'))
        );

        $data['format'] = 'json';

        $fieldset->addField('category', 'select', array(
            'label' => Mage::helper('manageapi')->__('Category'),
            'title' => Mage::helper('manageapi')->__('Category'),
            'name' => 'category',
            'required' => true,
            'values' => Mage::getSingleton('manageapi/options')->getCategoryOptions(),
            'after_element_html' => "<br /><small>The API will get the data from this category. Currently, the categories have level 1.</small>",
        ));

        $form->setValues($data);
        return parent::_prepareForm();
    }
}
