<?php

class Magestore_Manageapi_Block_Adminhtml_Walmartbestsellers_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
	public function __construct(){
		parent::__construct();
		
		$this->_objectId = 'id';
		$this->_blockGroup = 'manageapi';
		$this->_controller = 'adminhtml_walmartbestsellers';
		
		$this->_updateButton('save', 'label', Mage::helper('manageapi')->__('Run'));
	}

	public function getHeaderText(){
		return Mage::helper('manageapi')->__('Run Bestsellers Walmart API');
	}
}
