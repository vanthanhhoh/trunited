<?php
/**
 * Created by PhpStorm.
 * User: longvuxuan
 * Date: 8/31/17
 * Time: 5:39 PM
 */

class Magestore_ManageApi_Block_Adminhtml_Renderer_Coupon_Product extends
    Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {
        $value = $row->getData($this->getColumn()->getIndex());
        if($value != null)
            $html = '<a href="'.Mage::helper("adminhtml")->getUrl("adminhtml/catalog_product/edit", array
                ('id'=>$value)).'" target="_blank">'.$value.'</a>';
        else
            $html = '';
        return '<span>' . $html . '</span>';

    }

}
