<?php
/**
 * Created by PhpStorm.
 * User: longvuxuan
 * Date: 8/31/17
 * Time: 5:39 PM
 */

class Magestore_ManageApi_Block_Adminhtml_Renderer_Coupon_Category extends
    Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {
        $value = $row->getData($this->getColumn()->getIndex());
        $data = json_decode($value, true);
        $html = '';
        if(is_array($data['category'])){
            $i = 0;
            foreach ($data['category'] as $cat) {
                if($i == 0){
                    $html .= '<b>'.$cat. ' (primary)</b> <hr />';
                } else {
                    $html .= $cat.'<br />';
                }
                $i++;
            }
        } else {
            $html .= '<b>'.$data['category']. ' (primary)</b>';
        }

        return $html;
    }

}
