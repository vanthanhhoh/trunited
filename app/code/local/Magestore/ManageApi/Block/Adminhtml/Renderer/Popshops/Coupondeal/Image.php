<?php
/**
 * Created by PhpStorm.
 * User: longvuxuan
 * Date: 8/31/17
 * Time: 5:39 PM
 */

class Magestore_ManageApi_Block_Adminhtml_Renderer_Popshops_Coupondeal_Image extends
    Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {
        $value = $row->getData($this->getColumn()->getIndex());

        $html = '';
        if($value != null){
            $html .= "<img src='$value' width='150px' height='50px' />";
        }

        return $html;
    }

}
