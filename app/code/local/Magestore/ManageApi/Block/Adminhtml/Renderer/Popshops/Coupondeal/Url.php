<?php
/**
 * Created by PhpStorm.
 * User: longvuxuan
 * Date: 8/31/17
 * Time: 5:39 PM
 */

class Magestore_ManageApi_Block_Adminhtml_Renderer_Popshops_Coupondeal_Url extends
    Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {
        $value = $row->getData($this->getColumn()->getIndex());

        $html = '';
        if($value != null){
            $html .= "<a href='$value' target='_blank' >".Mage::helper('manageapi')->__('Click')."</a>";
        }

        return $html;
    }

}
