<?php

class Magestore_ManageApi_Adminhtml_WalmartcategoryController extends Mage_Adminhtml_Controller_Action
{
    protected function _initAction()
    {
        $this->loadLayout()
            ->_setActiveMenu('manageapi/manageapi')
            ->_addBreadcrumb(Mage::helper('adminhtml')->__('Category Walmart API Manager'),
                Mage::helper('adminhtml')->__('Category Walmart API Manager'));
        return $this;
    }

    public function indexAction()
    {
        $this->_initAction()
            ->renderLayout();
    }

    public function editAction()
    {
        $id = $this->getRequest()->getParam('id');
        $model = null;
        if ($model != null && $model->getId() || $id == 0) {
            $data = Mage::getSingleton('adminhtml/session')->getFormData(true);
            if (!empty($data))
                $model->setData($data);

            Mage::register('category_data', $model);

            $this->loadLayout();
            $this->_setActiveMenu('manageapi/manageapi');

            $this->_addBreadcrumb(Mage::helper('adminhtml')->__('Category Walmart API Manager'), Mage::helper('adminhtml')->__('Walmart API Manager'));
            $this->_addBreadcrumb(Mage::helper('adminhtml')->__('Category Walmart API Manager'), Mage::helper('adminhtml')->__('Walmart API Manager'));

            $this->getLayout()->getBlock('head')->setCanLoadExtJs(true);
            $this->_addContent($this->getLayout()->createBlock('manageapi/adminhtml_walmartcategory_edit'))
                ->_addLeft($this->getLayout()->createBlock('manageapi/adminhtml_walmartcategory_edit_tabs'));

            $this->renderLayout();
        } else {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('manageapi')->__('Category Walmart API does not exist'));
            $this->_redirect('*/*/');
        }
    }

    public function newAction()
    {
        $apiKey = Mage::helper('manageapi')->getDataConfig('api_key', 'walmart_api');
        $origin_url = Mage::helper('manageapi')->getDataConfig('category_api', 'walmart_api');

        $url = str_replace('{{apiKey}}', $apiKey, $origin_url);

        $rs = Mage::helper('manageapi/walmartcategory')->processAPI($url);

        Mage::getSingleton('adminhtml/session')->addSuccess(
            Mage::helper('manageapi')->__('%s records were saved successfully', $rs['new'])
        );

        $this->_redirect('*/*/');
        return;
    }

    public function getHelperData()
    {
        return Mage::helper('manageapi');
    }

    public function saveAction()
    {
        if ($data = $this->getRequest()->getPost()) {

            if ($data != null) {

                $origin_url = Mage::helper('manageapi')->getDataConfig('category_api', 'walmart_api');

                if ($data['format'] != null)
                    $origin_url .= '&format=' . $data['format'];

                if ($data['category'] != null)
                    $origin_url .= '&category=' . $data['category'];

                if ($data['brand'] != null)
                    $origin_url .= '&brand=' . $data['brand'];

                if ($data['specialOffer'] != null)
                    $origin_url .= '&specialOffer=' . $data['specialOffer'];

                $origin_url .= '&apiKey=' . Mage::helper('manageapi')->getDataConfig('api_key', 'walmart_api');

                $url = str_replace('?&', '?', $origin_url);

                $rs = Mage::helper('manageapi/walmart')->processAPI($url, $data['format']);

                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('manageapi')->__('%s new records were saved successfully <br /> %s records were updated
                    successfully <br /> %s products were created successfully <br /> %s products were updated
                    successfully', $rs['new'], $rs['update'], $rs['product']['new'], $rs['product']['update'])
                );
            } else {
                Mage::getSingleton('adminhtml/session')->addError('Unable to find API to run');
            }

            $this->_redirect('*/*/');
            return;
        } else {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('manageapi')->__('Data was wrong!'));
        }

        $this->_redirect('*/*/');
    }

    public function deleteAction()
    {
        if ($this->getRequest()->getParam('id') > 0) {
            try {
                $model = Mage::getModel('manageapi/walmartcategory');
                $model->setId($this->getRequest()->getParam('id'))
                    ->delete();
                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Category Walmart API was successfully deleted'));
                $this->_redirect('*/*/');
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
            }
        }
        $this->_redirect('*/*/');
    }

    public function massDeleteAction()
    {
        $manageapiIds = $this->getRequest()->getParam('category');

        if (!is_array($manageapiIds)) {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('adminhtml')->__('Please select item(s)'));
        } else {
            try {
                foreach ($manageapiIds as $manageapiId) {
                    $manageapi = Mage::getModel('manageapi/walmartcategory')->load($manageapiId);
                    $manageapi->delete();
                }
                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Total of %d record(s) were successfully deleted', count($manageapiIds)));
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
        }
        $this->_redirect('*/*/index');
    }

    public function exportCsvAction()
    {
        $fileName = 'walmart.csv';
        $content = $this->getLayout()->createBlock('manageapi/adminhtml_walmartcategory_grid')->getCsv();
        $this->_prepareDownloadResponse($fileName, $content);
    }

    public function exportXmlAction()
    {
        $fileName = 'walmart.xml';
        $content = $this->getLayout()->createBlock('manageapi/adminhtml_walmartcategory_grid')->getXml();
        $this->_prepareDownloadResponse($fileName, $content);
    }
}
