<?php

class Magestore_ManageApi_Adminhtml_DealtypeController extends Mage_Adminhtml_Controller_Action
{
    protected function _initAction()
    {
        $this->loadLayout()
            ->_setActiveMenu('manageapi/manageapi')
            ->_addBreadcrumb(Mage::helper('adminhtml')->__('Deal Types API Manager'),
                Mage::helper('adminhtml')->__('Deal Types API Manager'));
        return $this;
    }

    public function indexAction()
    {
        $this->_initAction()
            ->renderLayout();
    }

    public function getHelperData()
    {
        return Mage::helper('manageapi');
    }

    public function newAction()
    {
        $origin_url = Mage::helper('manageapi')->getDataConfig('deal_type_api', 'popshops_api');

        $origin_url .= '&account='.Mage::helper('manageapi')->getDataConfig('account_public_api_key', 'popshops_api');

        $url = str_replace('?&', '?', $origin_url);

        $rs = Mage::helper('manageapi/dealtype')->processAPI($url);

        Mage::getSingleton('adminhtml/session')->addSuccess(
            Mage::helper('manageapi')->__('%s new records were saved successfully <br /> %s records were updated
                    successfully', $rs['new'], $rs['update'])
        );

        $this->_redirect('*/*/');
        return;
    }

    public function deleteAction()
    {
        if ($this->getRequest()->getParam('id') > 0) {
            try {
                $model = Mage::getModel('manageapi/dealtype');
                $model->setId($this->getRequest()->getParam('id'))
                    ->delete();
                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Coupon API was successfully deleted'));
                $this->_redirect('*/*/');
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
            }
        }
        $this->_redirect('*/*/');
    }

    public function massDeleteAction()
    {
        $manageapiIds = $this->getRequest()->getParam('dealtype');
        if (!is_array($manageapiIds)) {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('adminhtml')->__('Please select item(s)'));
        } else {
            try {
                foreach ($manageapiIds as $manageapiId) {
                    $manageapi = Mage::getModel('manageapi/dealtype')->load($manageapiId);
                    $manageapi->delete();
                }
                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Total of %d record(s) were successfully deleted', count($manageapiIds)));
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
        }
        $this->_redirect('*/*/index');
    }

    public function exportCsvAction()
    {
        $fileName = 'popshops_deal_types.csv';
        $content = $this->getLayout()->createBlock('manageapi/adminhtml_dealtype_grid')->getCsv();
        $this->_prepareDownloadResponse($fileName, $content);
    }

    public function exportXmlAction()
    {
        $fileName = 'popshops_deal_types.xml';
        $content = $this->getLayout()->createBlock('manageapi/adminhtml_dealtype_grid')->getXml();
        $this->_prepareDownloadResponse($fileName, $content);
    }
}
