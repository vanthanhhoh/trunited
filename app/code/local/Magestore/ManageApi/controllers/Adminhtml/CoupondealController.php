<?php

class Magestore_ManageApi_Adminhtml_CoupondealController extends Mage_Adminhtml_Controller_Action
{
    protected function _initAction()
    {
        $this->loadLayout()
            ->_setActiveMenu('manageapi/manageapi')
            ->_addBreadcrumb(Mage::helper('adminhtml')->__('Coupons and Deals API Manager'),
                Mage::helper('adminhtml')->__('Coupons and Deals API Manager'));
        return $this;
    }

    public function indexAction()
    {
        $this->_initAction()
            ->renderLayout();
    }

    public function editAction()
    {
        $id = $this->getRequest()->getParam('id');
        $model = null;
        if ($model != null && $model->getId() || $id == 0) {
            $data = Mage::getSingleton('adminhtml/session')->getFormData(true);
            if (!empty($data))
                $model->setData($data);

            Mage::register('coupondeal_data', $model);

            $this->loadLayout();
            $this->_setActiveMenu('manageapi/manageapi');

            $this->_addBreadcrumb(Mage::helper('adminhtml')->__('Coupons and Deals API Manager'), Mage::helper('adminhtml')->__('Popshops Merchants API Manager'));
            $this->_addBreadcrumb(Mage::helper('adminhtml')->__('Coupons and Deals API Manager'), Mage::helper('adminhtml')->__('Popshops Merchants API Manager'));

            $this->getLayout()->getBlock('head')->setCanLoadExtJs(true);
            $this->_addContent($this->getLayout()->createBlock('manageapi/adminhtml_coupondeal_edit'))
                ->_addLeft($this->getLayout()->createBlock('manageapi/adminhtml_coupondeal_edit_tabs'));

            $this->renderLayout();
        } else {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('manageapi')->__('Coupons and Deals API does not exist'));
            $this->_redirect('*/*/');
        }
    }

    public function newAction()
    {
        $this->_forward('edit');
    }

    public function getHelperData()
    {
        return Mage::helper('manageapi');
    }

    public function saveAction()
    {
        if ($data = $this->getRequest()->getPost()) {

            if ($data != null) {

                $origin_url = Mage::helper('manageapi')->getDataConfig('coupon_deal_api', 'popshops_api');

                if ($data['end_on'] != null)
                    $origin_url .= '&end_on=' . $data['end_on'];

                if ($data['deal_type'] != null)
                    $origin_url .= '&deal_type=' . $data['deal_type'];

                if ($data['merchant'] != null)
                    $origin_url .= '&merchant=' . $data['merchant'];

                if ($data['merchant_type'] != null)
                    $origin_url .= '&merchant_type=' . $data['merchant_type'];

                if ($data['page'] != null)
                    $origin_url .= '&page=' . $data['page'];

                if ($data['site_wide'] != null)
                    $origin_url .= '&site_wide=' . $data['site_wide'];

                if ($data['sort_deal'] != null)
                    $origin_url .= '&sort_deal=' . $data['sort_deal'];

                if ($data['start_on'] != null)
                    $origin_url .= '&start_on=' . $data['start_on'];

                if ($data['tracking_id'] != null)
                    $origin_url .= '&tracking_id=' . $data['tracking_id'];

                if ($data['results_per_page'] != null)
                    $origin_url .= '&results_per_page=' . $data['results_per_page'];

                $origin_url .= '&account=' . Mage::helper('manageapi')->getDataConfig('account_public_api_key', 'popshops_api');
                $origin_url .= '&catalog=' . Mage::helper('manageapi')->getDataConfig('catalog_key', 'popshops_api');

                $url = str_replace('?&', '?', $origin_url);

                $rs = Mage::helper('manageapi/coupondeal')->processAPI($url, true);

                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('manageapi')->__('%s new records were saved successfully <br /> %s records were updated
                    successfully <br /> %s products were created successfully <br /> %s products were updated
                    successfully', $rs['new'], $rs['update'], $rs['product']['new'], $rs['product']['update'])
                );
            } else {
                Mage::getSingleton('adminhtml/session')->addError('Unable to find API to run');
            }

            $this->_redirect('*/*/');
            return;
        } else {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('manageapi')->__('Data was wrong!'));
        }

        $this->_redirect('*/*/');
    }

    public function deleteAction()
    {
        if ($this->getRequest()->getParam('id') > 0) {
            try {
                $model = Mage::getModel('manageapi/coupondeal');
                $model->setId($this->getRequest()->getParam('id'))
                    ->delete();
                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Coupon API was successfully deleted'));
                $this->_redirect('*/*/');
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
            }
        }
        $this->_redirect('*/*/');
    }

    public function massDeleteAction()
    {
        $manageapiIds = $this->getRequest()->getParam('coupondeal');

        if (!is_array($manageapiIds)) {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('adminhtml')->__('Please select item(s)'));
        } else {
            try {
                foreach ($manageapiIds as $manageapiId) {
                    $manageapi = Mage::getModel('manageapi/coupondeal')->load($manageapiId);
                    $manageapi->delete();
                }
                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Total of %d record(s) were successfully deleted', count($manageapiIds)));
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
        }
        $this->_redirect('*/*/index');
    }

    public function massSynchronizeAction()
    {
        $manageapiIds = $this->getRequest()->getParam('coupondeal');
        if (!is_array($manageapiIds)) {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('adminhtml')->__('Please select item(s)'));
        } else {
            try {
                $rs = Mage::helper('manageapi/coupondeal')->synchronizeProduct($manageapiIds);
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('manageapi')->__('%s new products were created successfully <br /> %s products were updated
                    successfully', $rs['new'], $rs['update'])
                );
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
        }
        $this->_redirect('*/*/index');
    }

    public function exportCsvAction()
    {
        $fileName = 'popshops_coupon_deal.csv';
        $content = $this->getLayout()->createBlock('manageapi/adminhtml_coupondeal_grid')->getCsv();
        $this->_prepareDownloadResponse($fileName, $content);
    }

    public function exportXmlAction()
    {
        $fileName = 'popshops_coupon_deal.xml';
        $content = $this->getLayout()->createBlock('manageapi/adminhtml_coupondeal_grid')->getXml();
        $this->_prepareDownloadResponse($fileName, $content);
    }
}
