<?php

class Magestore_ManageApi_Adminhtml_CouponController extends Mage_Adminhtml_Controller_Action
{
    protected function _initAction()
    {
        $this->loadLayout()
            ->_setActiveMenu('manageapi/manageapi')
            ->_addBreadcrumb(Mage::helper('adminhtml')->__('Coupon API Manager'), Mage::helper('adminhtml')->__('Coupon API
            Manager'));
        return $this;
    }

    public function indexAction()
    {
        $this->_initAction()
            ->renderLayout();
    }

    public function editAction()
    {
        $id = $this->getRequest()->getParam('id');
        $model = null;
        if ($model != null && $model->getId() || $id == 0) {
            $data = Mage::getSingleton('adminhtml/session')->getFormData(true);
            if (!empty($data))
                $model->setData($data);

            Mage::register('deals_data', $model);

            $this->loadLayout();
            $this->_setActiveMenu('manageapi/manageapi');

            $this->_addBreadcrumb(Mage::helper('adminhtml')->__('Coupon API Manager'), Mage::helper('adminhtml')->__('Coupon API Manager'));
            $this->_addBreadcrumb(Mage::helper('adminhtml')->__('Coupon API News'), Mage::helper('adminhtml')->__('Coupon API News'));

            $this->getLayout()->getBlock('head')->setCanLoadExtJs(true);
            $this->_addContent($this->getLayout()->createBlock('manageapi/adminhtml_coupon_edit'))
                ->_addLeft($this->getLayout()->createBlock('manageapi/adminhtml_coupon_edit_tabs'));

            $this->renderLayout();
        } else {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('manageapi')->__('Coupon API does not exist'));
            $this->_redirect('*/*/');
        }
    }

    public function newAction()
    {
        $this->_forward('edit');
    }

    public function getHelperData()
    {
        return Mage::helper('manageapi');
    }

    public function saveAction()
    {
        if ($data = $this->getRequest()->getPost()) {

            if ($data != null) {

                $origin_url = Mage::helper('manageapi')->getDataConfig('link_api', 'coupon_api');
                if($data['category'] != null)
                    $origin_url .= '&category='.urlencode($data['category']);

                if($data['promotiontype'] != null)
                    $origin_url .= '&promotiontype='.urlencode($data['promotiontype']);

                if($data['network'] != null)
                    $origin_url .= '&network='.urlencode($data['network']);

                if($data['mid'] != null)
                    $origin_url .= '&mid='.urlencode($data['mid']);

                if($data['resultsperpage'] != null)
                    $origin_url .= '&resultsperpage='.$data['resultsperpage'];

                if($data['pagenumber'] != null)
                    $origin_url .= '&pagenumber='.$data['pagenumber'];

                if($data['promocat'] != null)
                    $origin_url .= '&promocat='.$data['promocat'];

                $url = str_replace('?&', '?', $origin_url);

                $rs = Mage::helper('manageapi/coupon')->processAPI($url, true);
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('manageapi')->__('%s new records were saved successfully <br /> %s records were updated
                    successfully <br /> %s products were created successfully <br /> %s products were updated
                    successfully', $rs['new'], $rs['update'], $rs['product']['new'], $rs['product']['update'])
                );
            } else {
                Mage::getSingleton('adminhtml/session')->addError('Unable to find API to run');
            }

            $this->_redirect('*/*/');
            return;
        } else {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('manageapi')->__('Data was wrong!'));
        }

        $this->_redirect('*/*/');
    }

    public function deleteAction()
    {
        if ($this->getRequest()->getParam('id') > 0) {
            try {
                $model = Mage::getModel('manageapi/deals');
                $model->setId($this->getRequest()->getParam('id'))
                    ->delete();
                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Coupon API was successfully deleted'));
                $this->_redirect('*/*/');
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
            }
        }
        $this->_redirect('*/*/');
    }

    public function massDeleteAction()
    {
        $manageapiIds = $this->getRequest()->getParam('manageapi');
        if (!is_array($manageapiIds)) {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('adminhtml')->__('Please select item(s)'));
        } else {
            try {
                foreach ($manageapiIds as $manageapiId) {
                    $manageapi = Mage::getModel('manageapi/deals')->load($manageapiId);
                    $manageapi->delete();
                }
                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Total of %d record(s) were successfully deleted', count($manageapiIds)));
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
        }
        $this->_redirect('*/*/index');
    }

    public function massSynchronizeAction()
    {
        $manageapiIds = $this->getRequest()->getParam('manageapi');
        if (!is_array($manageapiIds)) {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('adminhtml')->__('Please select item(s)'));
        } else {
            try {
                $count = Mage::helper('manageapi/coupon')->synchronizeProduct($manageapiIds);
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('adminhtml')->__('Total of %d product(s) were successfully updated', $count)
                );
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
        }
        $this->_redirect('*/*/index');
    }

    public function massStatusAction()
    {
        $manageapiIds = $this->getRequest()->getParam('manageapi');
        if (!is_array($manageapiIds)) {
            Mage::getSingleton('adminhtml/session')->addError($this->__('Please select item(s)'));
        } else {
            try {
                foreach ($manageapiIds as $manageapiId) {
                    $manageapi = Mage::getSingleton('manageapi/manageapi')
                        ->load($manageapiId)
                        ->setStatus($this->getRequest()->getParam('status'))
                        ->setIsMassupdate(true)
                        ->save();
                }
                $this->_getSession()->addSuccess(
                    $this->__('Total of %d record(s) were successfully updated', count($manageapiIds))
                );
            } catch (Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            }
        }
        $this->_redirect('*/*/index');
    }

    public function exportCsvAction()
    {
        $fileName = 'coupon_api.csv';
        $content = $this->getLayout()->createBlock('manageapi/adminhtml_coupon_grid')->getCsv();
        $this->_prepareDownloadResponse($fileName, $content);
    }

    public function exportXmlAction()
    {
        $fileName = 'coupon_api.xml';
        $content = $this->getLayout()->createBlock('manageapi/adminhtml_coupon_grid')->getXml();
        $this->_prepareDownloadResponse($fileName, $content);
    }
}
