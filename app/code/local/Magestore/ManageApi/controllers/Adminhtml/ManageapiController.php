<?php

class Magestore_ManageApi_Adminhtml_ManageapiController extends Mage_Adminhtml_Controller_Action
{
    protected function _initAction()
    {
        $this->loadLayout()
            ->_setActiveMenu('manageapi/manageapi')
            ->_addBreadcrumb(Mage::helper('adminhtml')->__('Items Manager'), Mage::helper('adminhtml')->__('Item Manager'));
        return $this;
    }

    public function indexAction()
    {
        $this->_initAction()
            ->renderLayout();
    }

    public function editAction()
    {
        $id = $this->getRequest()->getParam('id');
        $model = null;
        if ($model != null && $model->getId() || $id == 0) {
            $data = Mage::getSingleton('adminhtml/session')->getFormData(true);
            if (!empty($data))
                $model->setData($data);

            Mage::register('manageapi_data', $model);

            $this->loadLayout();
            $this->_setActiveMenu('manageapi/manageapi');

            $this->_addBreadcrumb(Mage::helper('adminhtml')->__('Item Manager'), Mage::helper('adminhtml')->__('Item Manager'));
            $this->_addBreadcrumb(Mage::helper('adminhtml')->__('Item News'), Mage::helper('adminhtml')->__('Item News'));

            $this->getLayout()->getBlock('head')->setCanLoadExtJs(true);
            $this->_addContent($this->getLayout()->createBlock('manageapi/adminhtml_manageapi_edit'))
                ->_addLeft($this->getLayout()->createBlock('manageapi/adminhtml_manageapi_edit_tabs'));

            $this->renderLayout();
        } else {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('manageapi')->__('Item does not exist'));
            $this->_redirect('*/*/');
        }
    }

    public function newAction()
    {
        $this->_forward('edit');
    }

    public function getHelperData()
    {
        return Mage::helper('manageapi');
    }

    public function saveAction()
    {
        if ($data = $this->getRequest()->getPost()) {
            $start_date = date('Y-m-d H:i:s', strtotime($data['start_date']));
            $end_date = date('Y-m-d H:i:s', strtotime($data['end_date']));
            $select_apis = $data['select_api'];
            $day_configured = Mage::helper('manageapi')->getDataConfig('hotel_days', 'price_line');
            $_days = $day_configured != null ? $day_configured : 1;

            if (is_array($select_apis) && sizeof($select_apis) > 0) {
                $api_called = array();

                foreach ($select_apis as $api_name) {
                    if ($api_name == 1) {
                        // link share api
                        $enable = $this->getHelperData()->getDataConfig('enable', 'link_share');
                        if($enable)
                        {
                            $url = $this->getHelperData()->getDataConfig('link_share_api', 'link_share');
                            if($url != null)
                            {
                                $link_share_start_date = date('Y-m-d', strtotime($start_date));
                                $link_share_end_date = date('Y-m-d', strtotime($end_date));
                                $_url = str_replace(array('{{start_date}}','{{end_date}}'),array($link_share_start_date, $link_share_end_date), $url);
                                $api_called[$_url] = '<a href="'.$_url.'" target="_blank">LINK SHARE API</a> ';
                                $file = Mage::getBaseDir('media') . DS . Magestore_ManageApi_Helper_Data::LINK_SHARE_FILE;
                                Mage::helper('manageapi/linkshare')->processAPI($_url, $file, $link_share_start_date);
                            }
                        }
                    } else if ($api_name == 2) {
                        // price line hotel api
                        $enable = $this->getHelperData()->getDataConfig('enable_hotel', 'price_line');
                        if ($enable) {
                            $url = $this->getHelperData()->getDataConfig('hotel_api', 'price_line');
                            if ($url != null) {
                                $format = $this->getHelperData()->getDataConfig('hotel_format', 'price_line');

                                $end_30 = strtotime($start_date);
                                $start_30 = strtotime('28 days ago', $end_30);
                                $start_date_30 = date('Y-m-d_00:00:00', $start_30);
                                $end_date_30 = date('Y-m-d_23:59:00', $end_30);
                                $_url_30 = str_replace(
                                    array('{{start_date}}', '{{end_date}}', '{{format}}'),
                                    array($start_date_30, $end_date_30, $format),
                                    $url
                                );
                                $api_called[$_url_30] = '<a href="'.$_url_30.'" target="_blank">PRICE LINE HOTEL API
                                30 days ago </a> ';
                                Mage::helper('manageapi/hotel')->processAPI($_url_30, $start_date_30);

                                $end_60 = strtotime('1 days ago', $start_30);
                                $start_60 = strtotime('29 days ago', $end_60);
                                $start_date_60 = date('Y-m-d_00:00:00', $start_60);
                                $end_date_60 = date('Y-m-d_23:59:00', $end_60);
                                $_url_60 = str_replace(
                                    array('{{start_date}}', '{{end_date}}', '{{format}}'),
                                    array($start_date_60, $end_date_60, $format),
                                    $url
                                );
                                $api_called[$_url_60] = '<a href="'.$_url_60.'" target="_blank">PRICE LINE HOTEL API
                                60 days ago </a> ';
                                Mage::helper('manageapi/hotel')->processAPI($_url_60, $start_date_60);


                                $end_90 = strtotime('1 days ago', $start_60);
                                $start_90 = strtotime('29 days ago', $end_90);
                                $start_date_90 = date('Y-m-d_00:00:00', $start_90);
                                $end_date_90 = date('Y-m-d_23:59:00', $end_90);
                                $_url_90 = str_replace(
                                    array('{{start_date}}', '{{end_date}}', '{{format}}'),
                                    array($start_date_90, $end_date_90, $format),
                                    $url
                                );
                                $api_called[$_url_90] = '<a href="'.$_url_90.'" target="_blank">PRICE LINE HOTEL API
                                90 days ago </a> ';
                                Mage::helper('manageapi/hotel')->processAPI($_url_90, $start_date_90, true);
                            }
                        }
                    } else if ($api_name == 3) {
                        // price line flight api
                        $enable = $this->getHelperData()->getDataConfig('enable_flight', 'price_line');
                        if ($enable) {
                            $url = $this->getHelperData()->getDataConfig('flight_api', 'price_line');
                            if ($url != null) {
                                $format = $this->getHelperData()->getDataConfig('flight_format', 'price_line');

                                $end_30 = strtotime($start_date);
                                $start_30 = strtotime('28 days ago', $end_30);
                                $start_date_30 = date('Y-m-d_00:00:00', $start_30);
                                $end_date_30 = date('Y-m-d_23:59:00', $end_30);
                                $_url_30 = str_replace(
                                    array('{{start_date}}', '{{end_date}}', '{{format}}'),
                                    array($start_date_30, $end_date_30, $format),
                                    $url
                                );
                                $api_called[$_url_30] = '<a href="'.$_url_30.'" target="_blank">PRICE LINE FLIGHT API
                                30 days ago </a> ';
                                Mage::helper('manageapi/flight')->processAPI($_url_30, $start_date_30);

                                $end_60 = strtotime('1 days ago', $start_30);
                                $start_60 = strtotime('29 days ago', $end_60);
                                $start_date_60 = date('Y-m-d_00:00:00', $start_60);
                                $end_date_60 = date('Y-m-d_23:59:00', $end_60);
                                $_url_60 = str_replace(
                                    array('{{start_date}}', '{{end_date}}', '{{format}}'),
                                    array($start_date_60, $end_date_60, $format),
                                    $url
                                );
                                $api_called[$_url_60] = '<a href="'.$_url_60.'" target="_blank">PRICE LINE FLIGHT API
                                60 days ago </a> ';
                                Mage::helper('manageapi/flight')->processAPI($_url_60, $start_date_60);


                                $end_90 = strtotime('1 days ago', $start_60);
                                $start_90 = strtotime('29 days ago', $end_90);
                                $start_date_90 = date('Y-m-d_00:00:00', $start_90);
                                $end_date_90 = date('Y-m-d_23:59:00', $end_90);
                                $_url_90 = str_replace(
                                    array('{{start_date}}', '{{end_date}}', '{{format}}'),
                                    array($start_date_90, $end_date_90, $format),
                                    $url
                                );
                                $api_called[$_url_90] = '<a href="'.$_url_90.'" target="_blank">PRICE LINE FLIGHT API
                                90 days ago </a> ';
                                Mage::helper('manageapi/flight')->processAPI($_url_90, $start_date_90, true);
                            }
                        }
                    } else if ($api_name == 4) {
                        // price line car api
                        $enable = $this->getHelperData()->getDataConfig('enable_car', 'price_line');
                        if ($enable) {
                            $url = $this->getHelperData()->getDataConfig('car_api', 'price_line');
                            if ($url != null) {
                                $format = $this->getHelperData()->getDataConfig('car_format', 'price_line');

                                $end_30 = strtotime($start_date);
                                $start_30 = strtotime('28 days ago', $end_30);
                                $start_date_30 = date('Y-m-d_00:00:00', $start_30);
                                $end_date_30 = date('Y-m-d_23:59:00', $end_30);
                                $_url_30 = str_replace(
                                    array('{{start_date}}', '{{end_date}}', '{{format}}'),
                                    array($start_date_30, $end_date_30, $format),
                                    $url
                                );
                                $api_called[$_url_30] = '<a href="'.$_url_30.'" target="_blank">PRICE LINE CAR API
                                30 days ago </a> ';
                                Mage::helper('manageapi/car')->processAPI($_url_30, $start_date_30);

                                $end_60 = strtotime('1 days ago', $start_30);
                                $start_60 = strtotime('28 days ago', $end_60);
                                $start_date_60 = date('Y-m-d_00:00:00', $start_60);
                                $end_date_60 = date('Y-m-d_23:59:00', $end_60);
                                $_url_60 = str_replace(
                                    array('{{start_date}}', '{{end_date}}', '{{format}}'),
                                    array($start_date_60, $end_date_60, $format),
                                    $url
                                );
                                $api_called[$_url_60] = '<a href="'.$_url_60.'" target="_blank">PRICE LINE CAR API
                                60 days ago </a> ';
                                Mage::helper('manageapi/car')->processAPI($_url_60, $start_date_60);

                                $end_90 = strtotime('1 days ago', $start_60);
                                $start_90 = strtotime('29 days ago', $end_90);
                                $start_date_90 = date('Y-m-d_00:00:00', $start_90);
                                $end_date_90 = date('Y-m-d_23:59:00', $end_90);
                                $_url_90 = str_replace(
                                    array('{{start_date}}', '{{end_date}}', '{{format}}'),
                                    array($start_date_90, $end_date_90, $format),
                                    $url
                                );
                                $api_called[$_url_90] = '<a href="'.$_url_90.'" target="_blank">PRICE LINE CAR API
                                90 days ago </a> ';
                                Mage::helper('manageapi/car')->processAPI($_url_90, $start_date_90, true);
                            }
                        }
                    } else if ($api_name == 5) {
                        // price line vacation api
                        $enable = $this->getHelperData()->getDataConfig('enable_vacation', 'price_line');
                        if ($enable) {
                            $url = $this->getHelperData()->getDataConfig('vacation_api', 'price_line');
                            if ($url != null) {
                                $format = $this->getHelperData()->getDataConfig('vacation_format', 'price_line');
                                $start_date_vacation = date('Y-m-d_00:00:00', strtotime($start_date));
                                $end_date_vacation = date('Y-m-d_23:59:59', strtotime($end_date));
                                $_url = str_replace(array('{{start_date}}', '{{end_date}}', '{{format}}'), array($start_date_vacation, $end_date_vacation, $format), $url);
                                $api_called[$_url] = '<a href="'.$_url.'" target="_blank">PRICE LINE VACATION API</a> ';
                                Mage::helper('manageapi/vacation')->processAPI($_url, $start_date);
                            }
                        }
                    } else if ($api_name == 6) {
                        // cj api
                        $enable = $this->getHelperData()->getDataConfig('enable_cj', 'cj');
                        if ($enable) {
                            $start_date_cj = date('Y-m-d', strtotime($start_date));
                            $end_date_cj = date('Y-m-d', strtotime($end_date));
                            $data_type = $this->getHelperData()->getDataConfig('cj_data_type', 'cj');

                            $url = $this->getHelperData()->getDataConfig('cj_api', 'cj');
                            
                            $_url = str_replace(
                                array('{{start_date}}', '{{end_date}}', '{{data_type}}'), 
                                array($start_date_cj, $end_date_cj, $data_type), 
                                $url
                            );

                            $api_called[$_url] = '<a href="'.$_url.'" target="_blank">CJ API</a> ';

                            Mage::helper('manageapi/cj')->processCron2($start_date_cj, $end_date_cj);
                        }
                    } else if ($api_name == 7) {
                        // target api
                        $enable = $this->getHelperData()->getDataConfig('enable_target', 'target');
                        if ($enable) {
                            $url = $this->getHelperData()->getDataConfig('target_api', 'target');
                            if ($url != null) {
                                $_url = str_replace(array('{{start_date}}', '{{end_date}}'), array($start_date, $end_date), $url);
                                $api_called[$_url] = '<a href="'.$_url.'" target="_blank">TARGET API</a> ';
                                Mage::helper('manageapi/target')->processAPI($_url, $start_date);
                            }
                        }
                    } else if($api_name == 8) {
                        $enable = $this->getHelperData()->getDataConfig('enable', 'share_a_sale');
                        if($enable)
                        {
                            $url = $this->getHelperData()->getDataConfig('shareasale_api', 'share_a_sale');
                            if($url != null)
                            {
                                $affiliateId = $this->getHelperData()->getDataConfig('affiliate_id', 'share_a_sale');
                                $token = $this->getHelperData()->getDataConfig('token', 'share_a_sale');
                                $start_date_a_sale = date('m-d-Y', strtotime($start_date));
                                $end_date_a_sale = date('m-d-Y', strtotime($end_date));
                                $_url = str_replace(array('{{affiliate_id}}', '{{token}}', '{{start_date}}', '{{end_date}}'), array($affiliateId, $token, $start_date_a_sale, $end_date_a_sale), $url);
                                $api_called[$_url] = '<a href="'.$_url.'" target="_blank">Shareasale API</a> ';
                                Mage::helper('manageapi/shareasale')->processAPI($_url, $start_date);
                            }
                        }
                    } else if($api_name == 9) {
                        $enable = $this->getHelperData()->getDataConfig('enable', 'link_share_advertisers');
                        if($enable)
                        {
                            $url = $this->getHelperData()->getDataConfig('link_share_advertiser_api', 'link_share_advertisers');
                            if($url != null)
                            {
                                $start_date_advertisers = urlencode(date('Y-m-d H:i:s', strtotime($start_date)));
                                $end_date_advertisers = urlencode(date('Y-m-d H:i:s', strtotime($end_date)));
                                $_url = str_replace(array('{{start_date}}', '{{end_date}}'), array($start_date_advertisers, $end_date_advertisers), $url);
                                $api_called[$_url] = '<a href="'.$_url.'" target="_blank">LinkShare Advertisers API</a> ';
                                Mage::helper('manageapi/linkshareadvertisers')->processAPI($_url, $start_date);
                            }
                        }
                    }
                }

                if($api_called != '')
                {
                    Mage::getSingleton('adminhtml/session')->setData('url_called', $api_called);
                    Mage::getSingleton('adminhtml/session')->setData('api_called', $select_apis);
                    Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('manageapi')->__('APIs were successfully run'));
                } else {
                    Mage::getSingleton('adminhtml/session')->addError('Error');
                }
            } else {
                Mage::getSingleton('adminhtml/session')->addError('Unable to find API to run');
            }

            $this->_redirect('*/*/new',array('run' => true));
            return;
        }
        Mage::getSingleton('adminhtml/session')->addError(Mage::helper('manageapi')->__('Unable to find item to save'));
        $this->_redirect('*/*/new');
    }

    public function testAction()
    {
       /* $url = Mage::getBaseDir('media').'/manage_api/cj/cj.xml';
       $url = Mage::getBaseDir('media').'/manage_api/cj/cj9.xml';
       $_data = simplexml_load_file($url);
       $data = json_decode(json_encode((array)$_data), 1);

       $transactionSave = Mage::getModel('core/resource_transaction');
       $connection = Mage::getSingleton('core/resource')->getConnection('core_write');

       $start_date = '2017-08-02';

       try {
           $connection->beginTransaction();
           foreach ($data['commissions']['commission'] as $v) {

               $model = Mage::getModel('manageapi/cjactions');
               $_dt = array();
               foreach ($v as $k => $_v) {
                   $_dt[str_replace('-','_',$k)] = is_array($_v) ? json_encode($_v) : $_v;
               }

               $model->setData($_dt);
               $model->setData('created_time', now());

               $customer = Mage::getModel('customer/customer')->load($_dt['sid']);
               if($customer != null && $customer->getId() && floor($_dt['commission_amount']) > 0){
                   Mage::helper('rewardpoints/action')->addTransaction('global_brand', $customer, new Varien_Object(array(
                           'product_credit_title' => 0,
                           'product_credit' => 0,
                           'point_amount' => floor($_dt['commission_amount']),
                           'title' => Mage::helper('manageapi')->__('Points awarded for global brand %s order %s on %s', $_dt['advertiser_name'], $_dt['order_id'], $start_date),
                           'expiration_day' => 0,
                           'expiration_day_credit' => 0,
                           'is_on_hold' => 1,
                           'created_time' => date('Y-m-d H:i:s', strtotime($_dt['posting_date'])),
                           'order_increment_id' => $_dt['order_id']
                       ))
                   );
               }
               $transactionSave->addObject($model);
           }
           Mage::log('CJ API at '.date('Y-m-d H:i:s', time()).' - Result:'.sizeof($data['commissions']['commission']).' - URL: '.$url, null, 'check_manage_api.log');

           $transactionSave->save();
           $connection->commit();
       } catch (Exception $e) {
           $connection->rollback();
       } */

        /* $url = Mage::getBaseDir('media').'/manage_api/target/target.xml';
        $_data = simplexml_load_file($url);
        $data = json_decode(json_encode((array)$_data), 1);
        $transactionSave = Mage::getModel('core/resource_transaction');
        $connection = Mage::getSingleton('core/resource')->getConnection('core_write');

        try {
            $connection->beginTransaction();
           
            foreach ($data['Records']['Record'] as $v) {
                $model = Mage::getModel('manageapi/targetactions');
                $_dt = array();
                foreach ($v as $k => $_v) {
                    $_dt[strtolower(trim(str_replace('-','_',$k)))] = is_array($_v) ? json_encode($_v) : $_v;
                }

                $model->setData($_dt);
                $model->setData('created_time', now());

                $customer = Mage::getModel('customer/customer')->load($_dt['subid1']);
                if($customer != null && $customer->getId() && floor($_dt['payout']) > 0){
                    Mage::helper('rewardpoints/action')->addTransaction('global_brand', $customer, new Varien_Object(array(
                            'product_credit_title' => 0,
                            'product_credit' => 0,
                            'point_amount' => floor($_dt['payout']),
                            'title' => Mage::helper('manageapi')->__('Points awarded for global brand Target.com order on %s. Action ID [%s]', $start_date, $_dt['action_id']),
                            'expiration_day' => 0,
                            'expiration_day_credit' => 0,
                            'is_on_hold' => 1,
                            'created_time' => date('Y-m-d H:i:s', strtotime($_dt['action_date'])),
                            'order_increment_id' => $_dt['action_id']
                        ))
                    );
                }

                $transactionSave->addObject($model);
            }
            Mage::log('TARGET API at '.date('Y-m-d H:i:s', time()).' - Result:'.sizeof($data['Records']['Record']).' - URL: '.$url, null, 'check_manage_api.log');
            $transactionSave->save();
            $connection->commit();
        } catch (Exception $e) {
            $connection->rollback();
        } */

        echo 'success';

    }

    public function deleteAction()
    {
        if ($this->getRequest()->getParam('id') > 0) {
            try {
                $model = Mage::getModel('manageapi/manageapi');
                $model->setId($this->getRequest()->getParam('id'))
                    ->delete();
                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Item was successfully deleted'));
                $this->_redirect('*/*/');
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
            }
        }
        $this->_redirect('*/*/');
    }

    public function massDeleteAction()
    {
        $manageapiIds = $this->getRequest()->getParam('manageapi');
        if (!is_array($manageapiIds)) {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('adminhtml')->__('Please select item(s)'));
        } else {
            try {
                foreach ($manageapiIds as $manageapiId) {
                    $manageapi = Mage::getModel('manageapi/manageapi')->load($manageapiId);
                    $manageapi->delete();
                }
                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Total of %d record(s) were successfully deleted', count($manageapiIds)));
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
        }
        $this->_redirect('*/*/index');
    }

    public function massStatusAction()
    {
        $manageapiIds = $this->getRequest()->getParam('manageapi');
        if (!is_array($manageapiIds)) {
            Mage::getSingleton('adminhtml/session')->addError($this->__('Please select item(s)'));
        } else {
            try {
                foreach ($manageapiIds as $manageapiId) {
                    $manageapi = Mage::getSingleton('manageapi/manageapi')
                        ->load($manageapiId)
                        ->setStatus($this->getRequest()->getParam('status'))
                        ->setIsMassupdate(true)
                        ->save();
                }
                $this->_getSession()->addSuccess(
                    $this->__('Total of %d record(s) were successfully updated', count($manageapiIds))
                );
            } catch (Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            }
        }
        $this->_redirect('*/*/index');
    }

    public function exportCsvAction()
    {
        $fileName = 'manageapi.csv';
        $content = $this->getLayout()->createBlock('manageapi/adminhtml_manageapi_grid')->getCsv();
        $this->_prepareDownloadResponse($fileName, $content);
    }

    public function exportXmlAction()
    {
        $fileName = 'manageapi.xml';
        $content = $this->getLayout()->createBlock('manageapi/adminhtml_manageapi_grid')->getXml();
        $this->_prepareDownloadResponse($fileName, $content);
    }
}