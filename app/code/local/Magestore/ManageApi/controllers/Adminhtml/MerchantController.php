<?php

class Magestore_ManageApi_Adminhtml_MerchantController extends Mage_Adminhtml_Controller_Action
{
    protected function _initAction()
    {
        $this->loadLayout()
            ->_setActiveMenu('manageapi/manageapi')
            ->_addBreadcrumb(Mage::helper('adminhtml')->__('Popshops Merchants API Manager'),
                Mage::helper('adminhtml')->__('Popshops Merchants API Manager'));
        return $this;
    }

    public function indexAction()
    {
        $this->_initAction()
            ->renderLayout();
    }

    public function editAction()
    {
        $id = $this->getRequest()->getParam('id');
        $model = null;
        if ($model != null && $model->getId() || $id == 0) {
            $data = Mage::getSingleton('adminhtml/session')->getFormData(true);
            if (!empty($data))
                $model->setData($data);

            Mage::register('merchant_data', $model);

            $this->loadLayout();
            $this->_setActiveMenu('manageapi/manageapi');

            $this->_addBreadcrumb(Mage::helper('adminhtml')->__('Popshops Merchants API Manager'), Mage::helper('adminhtml')->__('Popshops Merchants API Manager'));
            $this->_addBreadcrumb(Mage::helper('adminhtml')->__('Popshops Merchants API Manager'), Mage::helper('adminhtml')->__('Popshops Merchants API Manager'));

            $this->getLayout()->getBlock('head')->setCanLoadExtJs(true);
            $this->_addContent($this->getLayout()->createBlock('manageapi/adminhtml_merchant_edit'))
                ->_addLeft($this->getLayout()->createBlock('manageapi/adminhtml_merchant_edit_tabs'));

            $this->renderLayout();
        } else {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('manageapi')->__('Popshops Merchants API does not exist'));
            $this->_redirect('*/*/');
        }
    }

    public function newAction()
    {
        $this->_forward('edit');
    }

    public function getHelperData()
    {
        return Mage::helper('manageapi');
    }

    public function saveAction()
    {
        if ($data = $this->getRequest()->getPost()) {

            if ($data != null) {

                $origin_url = Mage::helper('manageapi')->getDataConfig('merchant_api', 'popshops_api');

                if($data['category'] != null)
                    $origin_url .= '&category='.urlencode($data['category']);

                if($data['network'] != null)
                    $origin_url .= '&network='.urlencode($data['network']);

                if($data['page'] != null)
                    $origin_url .= '&page='.urlencode($data['page']);

                if($data['results_per_page'] != null)
                    $origin_url .= '&results_per_page='.$data['results_per_page'];

                $origin_url .= '&account='.Mage::helper('manageapi')->getDataConfig('account_public_api_key', 'popshops_api');
                $origin_url .= '&catalog='.Mage::helper('manageapi')->getDataConfig('catalog_key', 'popshops_api');

                $url = str_replace('?&', '?', $origin_url);

                $rs = Mage::helper('manageapi/merchant')->processAPI($url, true);

                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('manageapi')->__('%s new records were saved successfully <br /> %s records were updated
                    successfully', $rs['new'], $rs['update'])
                );
            } else {
                Mage::getSingleton('adminhtml/session')->addError('Unable to find API to run');
            }

            $this->_redirect('*/*/');
            return;
        } else {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('manageapi')->__('Data was wrong!'));
        }

        $this->_redirect('*/*/');
    }

    public function deleteAction()
    {
        if ($this->getRequest()->getParam('id') > 0) {
            try {
                $model = Mage::getModel('manageapi/popshopsmerchant');
                $model->setId($this->getRequest()->getParam('id'))
                    ->delete();
                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Coupon API was successfully deleted'));
                $this->_redirect('*/*/');
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
            }
        }
        $this->_redirect('*/*/');
    }

    public function massDeleteAction()
    {
        $manageapiIds = $this->getRequest()->getParam('popshops_merchant');

        if (!is_array($manageapiIds)) {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('adminhtml')->__('Please select item(s)'));
        } else {
            try {
                foreach ($manageapiIds as $manageapiId) {
                    $manageapi = Mage::getModel('manageapi/popshopsmerchant')->load($manageapiId);
                    $manageapi->delete();
                }
                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Total of %d record(s) were successfully deleted', count($manageapiIds)));
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
        }
        $this->_redirect('*/*/index');
    }

    public function exportCsvAction()
    {
        $fileName = 'popshops_merchants.csv';
        $content = $this->getLayout()->createBlock('manageapi/adminhtml_merchant_grid')->getCsv();
        $this->_prepareDownloadResponse($fileName, $content);
    }

    public function exportXmlAction()
    {
        $fileName = 'popshops_merchants.xml';
        $content = $this->getLayout()->createBlock('manageapi/adminhtml_merchant_grid')->getXml();
        $this->_prepareDownloadResponse($fileName, $content);
    }
}
