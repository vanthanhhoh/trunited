<?php

/**
 * Magestore
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magestore.com license that is
 * available through the world-wide-web at this URL:
 * http://www.magestore.com/license-agreement.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Magestore
 * @package     Magestore_TruBox
 * @module     TruBox
 * @author      Magestore Developer
 *
 * @copyright   Copyright (c) 2016 Magestore (http://www.magestore.com/)
 * @license     http://www.magestore.com/license-agreement.html
 *
 */

/**
 * TruBox Index Controller
 *
 * @category    Magestore
 * @package     Magestore_TruBox
 * @author      Magestore Developer
 */
class Magestore_ManageApi_IndexController extends Mage_Core_Controller_Front_Action
{

    /**
     * index action
     */
    public function indexAction()
    {
//        Mage::helper('manageapi/linkshare')->processCron();
//        Mage::helper('manageapi/hotel')->processCron();
//        Mage::helper('manageapi/flight')->processCron();
//        Mage::helper('manageapi/car')->processCron();
//        Mage::helper('manageapi/vacation')->processCron();
//        Mage::helper('manageapi/cj')->processCron();
//        Mage::helper('manageapi/target')->processCron();

        $this->loadLayout();
        $this->_title(Mage::helper('manageapi')->__('Manage API'));
        $this->renderLayout();
    }

    public function renameDbAction()
    {
        $setup = new Mage_Core_Model_Resource_Setup();
        $installer = $setup;
        $installer->startSetup();
        $installer->run("
            RENAME TABLE trpriceline_cj_actions to trcj_actions;
        ");
        $installer->endSetup();
        echo "success";
    }

    public function updateDb2Action()
    {
        $setup = new Mage_Core_Model_Resource_Setup();
        $installer = $setup;
        $installer->startSetup();
        $installer->run("
        ALTER TABLE {$setup->getTable('manageapi/linkshare')} MODIFY `order_id` VARCHAR(255) ;
      ");
        $installer->endSetup();
        echo "success";
    }


    //ALTER TABLE {$setup->getTable('trubox/address')} ADD `address_type` int(10) DEFAULT 2;
    public function updateDbAction()
    {
        $setup = new Mage_Core_Model_Resource_Setup();
        $installer = $setup;
        $installer->startSetup();
        $installer->run("
            DROP TABLE IF EXISTS {$setup->getTable('manageapi/linkshare')};
            CREATE TABLE {$setup->getTable('manageapi/linkshare')} (
              `linkshare_id` int(11) unsigned NOT NULL auto_increment,
              `member_id` VARCHAR(255) NOT NULL,
              `mid` VARCHAR(255) NOT NULL,
              `advertiser_name` varchar(255) NOT NULL,
              `order_id` VARCHAR(255)  NOT NULL,
              `transaction_date` datetime NULL,
              `sku` VARCHAR(255) NULL,
              `sales` FLOAT unsigned,
              `items` VARCHAR(255) NULL,
              `total_commission` FLOAT unsigned,
              `process_date` datetime NULL,
              `created_at` datetime NULL,
              PRIMARY KEY (`linkshare_id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
            
            DROP TABLE IF EXISTS {$setup->getTable('manageapi/hotelactions')};
            CREATE TABLE {$setup->getTable('manageapi/hotelactions')} (
              `hotel_actions_id` int(10) unsigned NOT NULL auto_increment,
              `confirmed_subtotal` text NULL DEFAULT '',
              `confirmed_commission` text NULL DEFAULT '',
              `confirmed_fee` text NULL DEFAULT '',
              `confirmed_total_earnings` text NULL DEFAULT '',
              `reconciled_status` text NULL DEFAULT '',
              `invoice_number` text NULL DEFAULT '',
              `confirmed_insurance_commission` text NULL DEFAULT '',
              `insurance_reconciled_status` text NULL DEFAULT '',
              `insurance_invoice_number` text NULL DEFAULT '',
              `reservation_date_time` datetime NULL,
              `refid` VARCHAR(255) NULL,
              `site_name` VARCHAR(255) NULL,
              `member_id` text NULL,
              `accountid` VARCHAR(255) NULL,
              `refclickid` VARCHAR(255) NULL,
              `hotelid` VARCHAR(255) NULL,
              `cityid` VARCHAR(255) NULL,
              `tripid` VARCHAR(255) NULL,
              `ratecat` VARCHAR(255) NULL,
              `portal` VARCHAR(255) NULL,
              `total` FLOAT NULL,
              `sub_total` FLOAT NULL,
              `commission` FLOAT NULL,
              `fee` FLOAT NULL,
              `revenue` FLOAT NULL,
              `booked_currency` VARCHAR(255) NULL,
              `rooms` VARCHAR(255) NULL,
              `city` VARCHAR(255) NULL,
              `hotel_name` VARCHAR(255) NULL,
              `state` VARCHAR(255) NULL,
              `country` VARCHAR(255) NULL,
              `number_of_days` INT(10) NULL,
              `promo` VARCHAR(255) NULL,
              `check_in_date_time` datetime NULL,
              `check_out_date_time` datetime NULL,
              `user_name` VARCHAR(255) NULL,
              `user_middlename` text NULL,
              `user_lastname` VARCHAR(255) NULL,
              `user_email` VARCHAR(255) NULL,
              `user_phone` VARCHAR(255) NULL,
              `user_phone_extension` text NULL,
              `user_address` text NULL,
              `user_location_city` VARCHAR(255) NULL,
              `user_location_state` VARCHAR(255) NULL,
              `user_country` VARCHAR(255) NULL,
              `user_zip` VARCHAR(255) NULL,
              `phn_sale` VARCHAR(255) NULL,
              `mobile_sale` VARCHAR(255) NULL,
              `device` VARCHAR(255) NULL,
              `rate_type` text NULL,
              `chain_code` VARCHAR(255) NULL,
              `insurance_flag` VARCHAR(255) NULL,
              `insured_days` VARCHAR(255) NULL,
              `insurance_commission` VARCHAR(255) NULL,
              `est_insurance_subtotal` VARCHAR(255) NULL,
              `invoice_date` text NULL,
              `pending_commission` text NULL,
              `status` VARCHAR(255) NULL,
              `payment_commission` text NULL,
              `other` text NULL,
              `on_hold_at` datetime NULL,
              `created_time` datetime NULL,
              PRIMARY KEY (`hotel_actions_id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
            
            DROP TABLE IF EXISTS {$setup->getTable('manageapi/flightactions')};
            CREATE TABLE {$setup->getTable('manageapi/flightactions')} (
              `flight_actions_id` int(10) unsigned NOT NULL auto_increment,
              `air_offer_id` VARCHAR(255) NULL ,
              `reservation_date_time` datetime NULL ,
              `session_id` VARCHAR(255) NULL ,
              `accountid` VARCHAR(255) NULL ,
              `refid` VARCHAR(255) NULL,
              `site_name` VARCHAR(255) NULL,
              `refclickid` VARCHAR(255) NULL,
              `insurance_flag` VARCHAR(255) NULL,
              `total` FLOAT NULL,
              `sub_total` FLOAT NULL,
              `fee` FLOAT NULL,
              `commission` FLOAT NULL,
              `insurance_commission` VARCHAR(255) NULL,
              `ratecat` VARCHAR(255) NULL,
              `passengers` VARCHAR(255) NULL,
              `insured_passengers` VARCHAR(255) NULL,
              `air_search_type` VARCHAR (255) NULL,
              `start_date_time` datetime NULL ,
              `end_date_time` datetime NULL ,
              `user_name` VARCHAR(255) NULL,
              `user_middlename` text NULL,
              `user_lastname` VARCHAR(255) NULL,
              `user_email` VARCHAR(255) NULL,
              `user_phone` VARCHAR(255) NULL,
              `user_phone_extension` text NULL,
              `user_address` text NULL,
              `user_location_city` VARCHAR(255) NULL,
              `user_location_state` VARCHAR(255) NULL,
              `user_country` VARCHAR(255) NULL,
              `user_zip` VARCHAR(255) NULL,
              `status` VARCHAR(255) NULL,
              `revenue` FLOAT NULL,
              `flights` text NULL,
              `total_insurance` FLOAT NULL,
              `ins_subtotal` FLOAT NULL,
              `affiliate_cut` FLOAT NULL,
              `accounting_sub_total` FLOAT NULL,
              `origin_Airport_name` text NULL,
              `dest_Airport_name` text NULL,
              `origin_City` text NULL,
              `dest_City` text NULL,
              `device` VARCHAR(255) NULL,
              `confirmed_subtotal` text NULL DEFAULT '',
              `confirmed_commission` text NULL DEFAULT '',
              `confirmed_fee` text NULL DEFAULT '',
              `confirmed_total_earnings` text NULL DEFAULT '',
              `reconciled_status` text NULL DEFAULT '',   
              `invoice_number` text NULL DEFAULT '',
              `confirmed_insurance_commission` text NULL DEFAULT '',
              `insurance_reconciled_status` text NULL DEFAULT '',
              `insurance_invoice_number` text NULL DEFAULT '',
              `other` text NULL,
              `on_hold_at` datetime NULL,
              `created_time` datetime NULL,
              PRIMARY KEY (`flight_actions_id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
            
            DROP TABLE IF EXISTS {$setup->getTable('manageapi/caractions')};
            CREATE TABLE {$setup->getTable('manageapi/caractions')} (
              `car_actions_id` int(10) unsigned NOT NULL auto_increment,
              `driver_fname` VARCHAR (255) NULL ,
              `driver_lname` VARCHAR (255) NULL ,
              `pickup_locationid` VARCHAR (255) NULL ,
              `dropoff_locationid` VARCHAR (255) NULL ,
              `pickup_time` datetime NULL ,
              `dropoff_time` datetime NULL ,
              `pickup_location_city` text NULL ,
              `pickup_location_state` text NULL ,
              `pickup_location_country` text NULL,
              `dropoff_location_city` text NULL,
              `dropoff_location_state` text NULL,
              `dropoff_location_country` text NULL,
              `affiliate_cut` VARCHAR(255) NULL,
              `date` datetime NULL,
              `revenue` FLOAT NULL,
              `promo_coupon_code` text NULL,
              `confirmed_subtotal` text NULL,
              `confirmed_commission` text NULL,
              `confirmed_fee` text NULL,
              `confirmed_total_earnings` text NULL,
              `reconciled_status` text NULL,
              `invoice_number` text NULL,
              `confirmed_insurance_commission` text NULL,
              `insurance_reconciled_status` text NULL,
              `insurance_invoice_number` text NULL,
              `accountid` VARCHAR(255) NULL,
              `refid` VARCHAR(255) NULL,
              `ratecat` VARCHAR (255) NULL,
              `site_name` VARCHAR (255) NULL,
              `portal` VARCHAR(255) NULL,
              `refclickid` text NULL,
              `requestid` VARCHAR (255) NULL,
              `insurance_flag` VARCHAR (255) NULL,
              `total` FLOAT NULL,
              `sub_total` FLOAT NULL,
              `tax` FLOAT NULL,
              `insured_days` VARCHAR(255) NULL,
              `currency` VARCHAR (255) NULL,
              `user_name` VARCHAR(255) NULL,
              `user_middlename` text NULL,
              `user_lastname` VARCHAR(255) NULL,
              `user_email` VARCHAR(255) NULL,
              `user_phone` VARCHAR(255) NULL,
              `user_phone_extension` text NULL,
              `user_address` text NULL,
              `user_location_city` VARCHAR(255) NULL,
              `user_location_state` VARCHAR(255) NULL,
              `user_country` VARCHAR(255) NULL,
              `user_zip` VARCHAR(255) NULL,
              `car_type` VARCHAR(255) NULL,
              `company_name` VARCHAR(255) NULL,
              `company_code` VARCHAR(255) NULL,
              `num_days` VARCHAR(255) NULL,
              `pickup_location` VARCHAR(255) NULL,
              `dropoff_location` VARCHAR(255) NULL,
              `bookingid` VARCHAR(255) NULL,
              `tripid` VARCHAR(255) NULL,
              `newsletter_optin` VARCHAR(255) NULL,
              `device` VARCHAR (255) NULL,
              `ins_subtotal` text NULL,
              `insurance_commission` text NULL,
              `accounting_aux_value` text NULL,
              `fee` FLOAT NULL,
              `commission` FLOAT NULL,
              `rate_type` VARCHAR (255) NULL,
              `phn_sale` VARCHAR(255) NULL,
              `member_id` text NULL,
              `invoice_date` text NULL,
              `pending_commission` text NULL,
              `status` VARCHAR (255) NULL,
              `payment_commission` text NULL,
              `other` text NULL,
              `on_hold_at` datetime NULL,
              `created_time` datetime NULL,
              PRIMARY KEY (`car_actions_id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
            
            DROP TABLE IF EXISTS {$setup->getTable('manageapi/vacationactions')};
            CREATE TABLE {$setup->getTable('manageapi/vacationactions')} (
              `vacation_actions_id` int(10) unsigned NOT NULL auto_increment,
              `date` datetime NULL ,
              `car_driver_fname` text NULL ,
              `car_driver_lname` text NULL ,
              `car_pickup_locationid` text NULL ,
              `car_dropoff_locationid` text NULL ,
              `car_pickup_datetime` text NULL ,
              `car_dropoff_datetime` text NULL ,
              `car_pickup_location_city` text NULL ,
              `car_pickup_location_state` text NULL ,
              `car_pickup_location_country` text NULL ,
              `car_dropoff_location_city` text NULL ,
              `car_dropoff_location_state` text NULL ,
              `car_dropoff_location_country` text NULL ,
              `flights` text NULL ,
              `insurance_fee` VARCHAR(255) NULL ,
              `orig_airport_code` VARCHAR (255) NULL ,
              `dest_airport_code` VARCHAR (255) NULL ,
              `hotel_city` text NULL ,
              `hotel_name` VARCHAR (255) NULL ,
              `hotel_state` text NULL ,
              `hotel_country` text NULL ,
              `confirmed_subtotal` text NULL,
              `confirmed_commission` text NULL,
              `confirmed_fee` text NULL,
              `confirmed_total_earnings` text NULL,
              `reconciled_status` text NULL,
              `invoice_number` text NULL,
              `confirmed_insurance_commission` text NULL,
              `insurance_reconciled_status` text NULL,
              `insurance_invoice_number` text NULL,
              `accountid` VARCHAR(255) NULL,
              `refid` VARCHAR(255) NULL,
              `site_name` VARCHAR (255) NULL,
              `portal` VARCHAR(255) NULL,
              `refclickid` text NULL,
              `total` FLOAT NULL,
              `sub_total` FLOAT NULL,
              `process_fee` FLOAT NULL,
              `commission` FLOAT NULL,
              `commission_fee` FLOAT NULL,
              `currency` VARCHAR (255) NULL,
              `accounting_currency` VARCHAR (255) NULL,
              `user_name` VARCHAR(255) NULL,
              `user_middlename` text NULL,
              `user_lastname` VARCHAR(255) NULL,
              `user_email` VARCHAR(255) NULL,
              `user_phone` VARCHAR(255) NULL,
              `user_phone_extension` text NULL,
              `user_address` text NULL,
              `user_location_city` VARCHAR(255) NULL,
              `user_location_state` VARCHAR(255) NULL,
              `user_country` VARCHAR(255) NULL,
              `user_zip` VARCHAR(255) NULL,
              `tripid` VARCHAR(255) NULL,
              `depart_date_time` datetime NULL,
              `return_date_time` datetime NULL,
              `check_in_date_time` datetime NULL,
              `check_out_date_time` datetime NULL,
              `air_city_id` VARCHAR(255) NULL,
              `car` VARCHAR(255) NULL,
              `device` VARCHAR(255) NULL,
              `rate_type` text NULL,
              `member_id` text NULL,
              `rooms` VARCHAR(255) NULL,
              `hotelid` VARCHAR(255) NULL,
              `cityid` VARCHAR(255) NULL,
              `invoice_date` text NULL,
              `pending_commission` text NULL,
              `status` VARCHAR(255) NULL,
              `payment_commission` text NULL,
              `other` text NULL,
              `on_hold_at` datetime NULL,
              `created_time` datetime NULL,
              PRIMARY KEY (`vacation_actions_id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
            
            DROP TABLE IF EXISTS {$setup->getTable('manageapi/cjactions')};
            CREATE TABLE {$setup->getTable('manageapi/cjactions')} (
              `cj_actions_id` int(10) unsigned NOT NULL auto_increment,
              `action_status` VARCHAR(255) NULL,
              `action_type` VARCHAR(255) NULL,
              `aid` VARCHAR(255) NULL,
              `commission_id` VARCHAR(255) NULL,
              `country` text NULL,
              `event_date` datetime NULL,
              `locking_date` datetime NULL,
              `order_id` VARCHAR(255) NULL,
              `original` VARCHAR(255) NULL,
              `original_action_id` VARCHAR(255) NULL,
              `posting_date` datetime NULL,
              `website_id` VARCHAR(255) NULL,
              `action_tracker_id` VARCHAR(255) NULL,
              `action_tracker_name` VARCHAR(255) NULL,
              `cid` VARCHAR(255) NULL,
              `advertiser_name` VARCHAR(255) NULL,
              `commission_amount` FLOAT NULL,
              `order_discount` FLOAT NULL,
              `sid` VARCHAR(255) NULL,
              `sale_amount` FLOAT NULL,
              `created_time` datetime NULL,
              PRIMARY KEY (`cj_actions_id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
            
            DROP TABLE IF EXISTS {$setup->getTable('manageapi/targetactions')};
            CREATE TABLE {$setup->getTable('manageapi/targetactions')} (
              `target_actions_id` int(10) unsigned NOT NULL auto_increment,
              `referral_date` text NULL,
              `action_date` datetime NULL,
              `locking_date` text NULL,
              `adj_date` text NULL,
              `scheduled_clearing_date` text NULL,
              `action_id` VARCHAR(255) NULL,
              `campaign` VARCHAR(255) NULL,
              `action_tracker` VARCHAR(255) NULL,
              `status` VARCHAR(255) NULL,
              `status_detail` text NULL,
              `category_list` text NULL,
              `sku` text NULL,
              `item_name` text NULL,
              `category` text NULL,
              `quantity` text NULL,
              `sale_amount` FLOAT NULL,
              `original_sale_amount` text NULL,
              `payout` FLOAT NULL,
              `original_payout` FLOAT NULL,
              `vat` FLOAT NULL,
              `promo_code` text NULL,
              `ad` text NULL,
              `referring_url` text NULL,
              `referring_type` text NULL,
              `ip_address` text NULL,
              `geo_location` text NULL,
              `subid1` VARCHAR(255) NULL,
              `subid2` text NULL,
              `subid3` text NULL,
              `sharedid` text NULL,
              `date1` text NULL,
              `date2` text NULL,
              `paystub` text NULL,
              `device` text NULL,
              `os` text NULL,
              `user_agent` text NULL,
              `created_time` datetime NULL,
              PRIMARY KEY (`target_actions_id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
            
            
		");
        $installer->endSetup();
        echo "success";
    }

    public function updateDb3Action()
    {
        $setup = new Mage_Core_Model_Resource_Setup();
        $installer = $setup;
        $installer->startSetup();
        $installer->run("
              DROP TABLE IF EXISTS {$setup->getTable('manageapi/shareasale')};
              CREATE TABLE {$setup->getTable('manageapi/shareasale')} (
                `shareasale_id` int(11) unsigned NOT NULL auto_increment,
                `transid` VARCHAR(255) NOT NULL,
                `userid` VARCHAR(255) NOT NULL,
                `merchantid` varchar(255) NOT NULL,
                `transdate` datetime NULL,
                `transamount` VARCHAR(255) NULL,
                `commission` VARCHAR(255) NULL,
                `comment` VARCHAR(255) NULL,
                `voided` VARCHAR(255) NULL,
                `pendingdate` VARCHAR(255) NULL,
                `locked` VARCHAR(255) NULL,
                `affcomment` VARCHAR(255) NULL,
                `bannerpage` VARCHAR(255) NULL,
                `reversaldate` VARCHAR(255) NULL,
                `clickdate` VARCHAR(255) NULL,
                `clicktime` VARCHAR(255) NULL,
                `bannerid` VARCHAR(255) NULL,
                `skulist` VARCHAR(255) NULL,
                `quantitylist` VARCHAR(255) NULL,
                `lockdate` datetime NULL,
                `paiddate` datetime NULL,
                `merchantorganization` VARCHAR(255) NULL,
                `merchantwebsite` VARCHAR(255) NULL,
                `transtype` VARCHAR(255) NULL,
                `merchantdefinedtype` VARCHAR(255) NULL,
                `created_time` datetime NULL,
                PRIMARY KEY (`shareasale_id`)
              ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

          ");
        $installer->endSetup();
        echo "success";
    }

    public function checkAction()
    {
        $rwd_transactions = Mage::getModel('rewardpoints/transaction')->getCollection()
            ->addFieldToFilter('action', 'global_brand')
            ->addFieldToFilter('is_on_hold', 1);

        if (sizeof($rwd_transactions) > 0) {
            $data = array();
            $duplicates = array();
            $i = 0;
            foreach ($rwd_transactions as $rwd) {
                if ($i == 0) {
                    $data[$rwd->getCustomerId()] = $rwd->getOrderIncrementId();
                } else {
                    if (isset($data[$rwd->getCustomerId()]) && $data[$rwd->getCustomerId()] != null && $data[$rwd->getCustomerId()] == $rwd->getOrderIncrementId()) {
                        $duplicates[] = array(
                            'transaction_id' => $rwd->getId(),
                            'customer_id' => $rwd->getCustomerId(),
                            'order_increment_id' => $rwd->getOrderIncrementId(),
                        );
                    } else {
                        $data[$rwd->getCustomerId()] = $rwd->getOrderIncrementId();
                    }
                }
                $i++;
            }

            zend_debug::dump($duplicates);
        }

        echo 'success';
    }

    public function updateDb4Action()
    {
        $setup = new Mage_Core_Model_Resource_Setup();
        $installer = $setup;
        $installer->startSetup();
        $installer->run("
              DROP TABLE IF EXISTS {$setup->getTable('manageapi/linkshareadvertisers')};
              CREATE TABLE {$setup->getTable('manageapi/linkshareadvertisers')} (
                `linkshare_advertisers_id` int(11) unsigned NOT NULL auto_increment,
                `etransaction_id` VARCHAR(255) NOT NULL,
                `advertiser_id` VARCHAR(255) NOT NULL,
                `sid` varchar(255) NOT NULL,
                `order_id` varchar(255) NULL,
                `offer_id` VARCHAR(255) NULL,
                `sku_number` VARCHAR(255) NULL,
                `sale_amount` VARCHAR(255) NULL,
                `quantity` VARCHAR(255) NULL,
                `commissions` VARCHAR(255) NULL,
                `process_date` datetime NULL,
                `transaction_date` datetime NULL,
                `transaction_type` VARCHAR(255) NULL,
                `product_name` VARCHAR(255) NULL,
                `u1` VARCHAR(255) NULL,
                `currency` VARCHAR(255) NULL,
                `is_event` VARCHAR(255) NULL,
                `created_time` datetime NULL,
                PRIMARY KEY (`linkshare_advertisers_id`)
              ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

          ");
        $installer->endSetup();
        echo "success";
    }

    public function updateDb5Action()
    {
        $setup = new Mage_Core_Model_Resource_Setup();
        $installer = $setup;
        $installer->startSetup();
        $installer->run("
            DROP TABLE IF EXISTS {$setup->getTable('manageapi/linkshare')};
            CREATE TABLE {$setup->getTable('manageapi/linkshare')} (
              `linkshare_id` int(11) unsigned NOT NULL auto_increment,
              `member_id` VARCHAR(255) NOT NULL,
              `mid` VARCHAR(255) NOT NULL,
              `advertiser_name` varchar(255) NOT NULL,
              `order_id` VARCHAR(255)  NOT NULL,
              `transaction_date` datetime NULL,
              `sku` VARCHAR(255) NULL,
              `sales` FLOAT unsigned,
              `items` VARCHAR(255) NULL,
              `total_commission` FLOAT unsigned,
              `process_date` datetime NULL,
              `created_at` datetime NULL,
              PRIMARY KEY (`linkshare_id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
          ");
        $installer->endSetup();
        echo "success";
    }

    public function updateDb6Action()
    {
        $setup = new Mage_Core_Model_Resource_Setup();
        $installer = $setup;
        $installer->startSetup();
        $installer->run("
              ALTER TABLE {$setup->getTable('manageapi/vacationactions')} ADD `on_hold_at` datetime NULL;
              ALTER TABLE {$setup->getTable('manageapi/caractions')} ADD `on_hold_at` datetime NULL;
              ALTER TABLE {$setup->getTable('manageapi/hotelactions')} ADD `on_hold_at` datetime NULL;
              ALTER TABLE {$setup->getTable('manageapi/flightactions')} ADD `on_hold_at` datetime NULL;

            ");
        $installer->endSetup();
        echo "success";
    }

    public function updateDb7Action()
    {
        $setup = new Mage_Core_Model_Resource_Setup();
        $installer = $setup;
        $installer->startSetup();
        $installer->run("

            DROP TABLE IF EXISTS {$setup->getTable('manageapi/hotelactions')};
            CREATE TABLE {$setup->getTable('manageapi/hotelactions')} (
              `hotel_actions_id` int(10) unsigned NOT NULL auto_increment,
              `confirmed_subtotal` text NULL DEFAULT '',
              `confirmed_commission` text NULL DEFAULT '',
              `confirmed_fee` text NULL DEFAULT '',
              `confirmed_total_earnings` text NULL DEFAULT '',
              `reconciled_status` text NULL DEFAULT '',
              `invoice_number` text NULL DEFAULT '',
              `confirmed_insurance_commission` text NULL DEFAULT '',
              `insurance_reconciled_status` text NULL DEFAULT '',
              `insurance_invoice_number` text NULL DEFAULT '',
              `reservation_date_time` datetime NULL,
              `refid` VARCHAR(255) NULL,
              `site_name` VARCHAR(255) NULL,
              `member_id` text NULL,
              `accountid` VARCHAR(255) NULL,
              `refclickid` VARCHAR(255) NULL,
              `hotelid` VARCHAR(255) NULL,
              `cityid` VARCHAR(255) NULL,
              `tripid` VARCHAR(255) NULL,
              `ratecat` VARCHAR(255) NULL,
              `portal` VARCHAR(255) NULL,
              `total` FLOAT NULL,
              `sub_total` FLOAT NULL,
              `commission` FLOAT NULL,
              `fee` FLOAT NULL,
              `revenue` FLOAT NULL,
              `booked_currency` VARCHAR(255) NULL,
              `rooms` VARCHAR(255) NULL,
              `city` VARCHAR(255) NULL,
              `hotel_name` VARCHAR(255) NULL,
              `state` VARCHAR(255) NULL,
              `country` VARCHAR(255) NULL,
              `number_of_days` INT(10) NULL,
              `promo` VARCHAR(255) NULL,
              `check_in_date_time` datetime NULL,
              `check_out_date_time` datetime NULL,
              `user_name` VARCHAR(255) NULL,
              `user_middlename` text NULL,
              `user_lastname` VARCHAR(255) NULL,
              `user_email` VARCHAR(255) NULL,
              `user_phone` VARCHAR(255) NULL,
              `user_phone_extension` text NULL,
              `user_address` text NULL,
              `user_location_city` VARCHAR(255) NULL,
              `user_location_state` VARCHAR(255) NULL,
              `user_country` VARCHAR(255) NULL,
              `user_zip` VARCHAR(255) NULL,
              `phn_sale` VARCHAR(255) NULL,
              `mobile_sale` VARCHAR(255) NULL,
              `device` VARCHAR(255) NULL,
              `rate_type` text NULL,
              `chain_code` VARCHAR(255) NULL,
              `insurance_flag` VARCHAR(255) NULL,
              `insured_days` VARCHAR(255) NULL,
              `insurance_commission` VARCHAR(255) NULL,
              `est_insurance_subtotal` VARCHAR(255) NULL,
              `invoice_date` text NULL,
              `pending_commission` text NULL,
              `status` VARCHAR(255) NULL,
              `payment_commission` text NULL,
              `other` text NULL,
              `on_hold_at` datetime NULL,
              `created_time` datetime NULL,
              PRIMARY KEY (`hotel_actions_id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

            DROP TABLE IF EXISTS {$setup->getTable('manageapi/flightactions')};
            CREATE TABLE {$setup->getTable('manageapi/flightactions')} (
              `flight_actions_id` int(10) unsigned NOT NULL auto_increment,
              `air_offer_id` VARCHAR(255) NULL ,
              `reservation_date_time` datetime NULL ,
              `session_id` VARCHAR(255) NULL ,
              `accountid` VARCHAR(255) NULL ,
              `refid` VARCHAR(255) NULL,
              `site_name` VARCHAR(255) NULL,
              `refclickid` VARCHAR(255) NULL,
              `insurance_flag` VARCHAR(255) NULL,
              `total` FLOAT NULL,
              `sub_total` FLOAT NULL,
              `fee` FLOAT NULL,
              `commission` FLOAT NULL,
              `insurance_commission` VARCHAR(255) NULL,
              `ratecat` VARCHAR(255) NULL,
              `passengers` VARCHAR(255) NULL,
              `insured_passengers` VARCHAR(255) NULL,
              `air_search_type` VARCHAR (255) NULL,
              `start_date_time` datetime NULL ,
              `end_date_time` datetime NULL ,
              `user_name` VARCHAR(255) NULL,
              `user_middlename` text NULL,
              `user_lastname` VARCHAR(255) NULL,
              `user_email` VARCHAR(255) NULL,
              `user_phone` VARCHAR(255) NULL,
              `user_phone_extension` text NULL,
              `user_address` text NULL,
              `user_location_city` VARCHAR(255) NULL,
              `user_location_state` VARCHAR(255) NULL,
              `user_country` VARCHAR(255) NULL,
              `user_zip` VARCHAR(255) NULL,
              `status` VARCHAR(255) NULL,
              `revenue` FLOAT NULL,
              `flights` text NULL,
              `total_insurance` FLOAT NULL,
              `ins_subtotal` FLOAT NULL,
              `affiliate_cut` FLOAT NULL,
              `accounting_sub_total` FLOAT NULL,
              `origin_Airport_name` text NULL,
              `dest_Airport_name` text NULL,
              `origin_City` text NULL,
              `dest_City` text NULL,
              `device` VARCHAR(255) NULL,
              `confirmed_subtotal` text NULL DEFAULT '',
              `confirmed_commission` text NULL DEFAULT '',
              `confirmed_fee` text NULL DEFAULT '',
              `confirmed_total_earnings` text NULL DEFAULT '',
              `reconciled_status` text NULL DEFAULT '',
              `invoice_number` text NULL DEFAULT '',
              `confirmed_insurance_commission` text NULL DEFAULT '',
              `insurance_reconciled_status` text NULL DEFAULT '',
              `insurance_invoice_number` text NULL DEFAULT '',
              `other` text NULL,
              `on_hold_at` datetime NULL,
              `created_time` datetime NULL,
              PRIMARY KEY (`flight_actions_id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

            DROP TABLE IF EXISTS {$setup->getTable('manageapi/caractions')};
            CREATE TABLE {$setup->getTable('manageapi/caractions')} (
              `car_actions_id` int(10) unsigned NOT NULL auto_increment,
              `driver_fname` VARCHAR (255) NULL ,
              `driver_lname` VARCHAR (255) NULL ,
              `pickup_locationid` VARCHAR (255) NULL ,
              `dropoff_locationid` VARCHAR (255) NULL ,
              `pickup_time` datetime NULL ,
              `dropoff_time` datetime NULL ,
              `pickup_location_city` text NULL ,
              `pickup_location_state` text NULL ,
              `pickup_location_country` text NULL,
              `dropoff_location_city` text NULL,
              `dropoff_location_state` text NULL,
              `dropoff_location_country` text NULL,
              `affiliate_cut` VARCHAR(255) NULL,
              `date` datetime NULL,
              `revenue` FLOAT NULL,
              `promo_coupon_code` text NULL,
              `confirmed_subtotal` text NULL,
              `confirmed_commission` text NULL,
              `confirmed_fee` text NULL,
              `confirmed_total_earnings` text NULL,
              `reconciled_status` text NULL,
              `invoice_number` text NULL,
              `confirmed_insurance_commission` text NULL,
              `insurance_reconciled_status` text NULL,
              `insurance_invoice_number` text NULL,
              `accountid` VARCHAR(255) NULL,
              `refid` VARCHAR(255) NULL,
              `ratecat` VARCHAR (255) NULL,
              `site_name` VARCHAR (255) NULL,
              `portal` VARCHAR(255) NULL,
              `refclickid` text NULL,
              `requestid` VARCHAR (255) NULL,
              `insurance_flag` VARCHAR (255) NULL,
              `total` FLOAT NULL,
              `sub_total` FLOAT NULL,
              `tax` FLOAT NULL,
              `insured_days` VARCHAR(255) NULL,
              `currency` VARCHAR (255) NULL,
              `user_name` VARCHAR(255) NULL,
              `user_middlename` text NULL,
              `user_lastname` VARCHAR(255) NULL,
              `user_email` VARCHAR(255) NULL,
              `user_phone` VARCHAR(255) NULL,
              `user_phone_extension` text NULL,
              `user_address` text NULL,
              `user_location_city` VARCHAR(255) NULL,
              `user_location_state` VARCHAR(255) NULL,
              `user_country` VARCHAR(255) NULL,
              `user_zip` VARCHAR(255) NULL,
              `car_type` VARCHAR(255) NULL,
              `company_name` VARCHAR(255) NULL,
              `company_code` VARCHAR(255) NULL,
              `num_days` VARCHAR(255) NULL,
              `pickup_location` VARCHAR(255) NULL,
              `dropoff_location` VARCHAR(255) NULL,
              `bookingid` VARCHAR(255) NULL,
              `tripid` VARCHAR(255) NULL,
              `newsletter_optin` VARCHAR(255) NULL,
              `device` VARCHAR (255) NULL,
              `ins_subtotal` text NULL,
              `insurance_commission` text NULL,
              `accounting_aux_value` text NULL,
              `fee` FLOAT NULL,
              `commission` FLOAT NULL,
              `rate_type` VARCHAR (255) NULL,
              `phn_sale` VARCHAR(255) NULL,
              `member_id` text NULL,
              `invoice_date` text NULL,
              `pending_commission` text NULL,
              `status` VARCHAR (255) NULL,
              `payment_commission` text NULL,
              `other` text NULL,
              `on_hold_at` datetime NULL,
              `created_time` datetime NULL,
              PRIMARY KEY (`car_actions_id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

            DROP TABLE IF EXISTS {$setup->getTable('manageapi/vacationactions')};
            CREATE TABLE {$setup->getTable('manageapi/vacationactions')} (
              `vacation_actions_id` int(10) unsigned NOT NULL auto_increment,
              `date` datetime NULL ,
              `car_driver_fname` text NULL ,
              `car_driver_lname` text NULL ,
              `car_pickup_locationid` text NULL ,
              `car_dropoff_locationid` text NULL ,
              `car_pickup_datetime` text NULL ,
              `car_dropoff_datetime` text NULL ,
              `car_pickup_location_city` text NULL ,
              `car_pickup_location_state` text NULL ,
              `car_pickup_location_country` text NULL ,
              `car_dropoff_location_city` text NULL ,
              `car_dropoff_location_state` text NULL ,
              `car_dropoff_location_country` text NULL ,
              `flights` text NULL ,
              `insurance_fee` VARCHAR(255) NULL ,
              `orig_airport_code` VARCHAR (255) NULL ,
              `dest_airport_code` VARCHAR (255) NULL ,
              `hotel_city` text NULL ,
              `hotel_name` VARCHAR (255) NULL ,
              `hotel_state` text NULL ,
              `hotel_country` text NULL ,
              `confirmed_subtotal` text NULL,
              `confirmed_commission` text NULL,
              `confirmed_fee` text NULL,
              `confirmed_total_earnings` text NULL,
              `reconciled_status` text NULL,
              `invoice_number` text NULL,
              `confirmed_insurance_commission` text NULL,
              `insurance_reconciled_status` text NULL,
              `insurance_invoice_number` text NULL,
              `accountid` VARCHAR(255) NULL,
              `refid` VARCHAR(255) NULL,
              `site_name` VARCHAR (255) NULL,
              `portal` VARCHAR(255) NULL,
              `refclickid` text NULL,
              `total` FLOAT NULL,
              `sub_total` FLOAT NULL,
              `process_fee` FLOAT NULL,
              `commission` FLOAT NULL,
              `commission_fee` FLOAT NULL,
              `currency` VARCHAR (255) NULL,
              `accounting_currency` VARCHAR (255) NULL,
              `user_name` VARCHAR(255) NULL,
              `user_middlename` text NULL,
              `user_lastname` VARCHAR(255) NULL,
              `user_email` VARCHAR(255) NULL,
              `user_phone` VARCHAR(255) NULL,
              `user_phone_extension` text NULL,
              `user_address` text NULL,
              `user_location_city` VARCHAR(255) NULL,
              `user_location_state` VARCHAR(255) NULL,
              `user_country` VARCHAR(255) NULL,
              `user_zip` VARCHAR(255) NULL,
              `tripid` VARCHAR(255) NULL,
              `depart_date_time` datetime NULL,
              `return_date_time` datetime NULL,
              `check_in_date_time` datetime NULL,
              `check_out_date_time` datetime NULL,
              `air_city_id` VARCHAR(255) NULL,
              `car` VARCHAR(255) NULL,
              `device` VARCHAR(255) NULL,
              `rate_type` text NULL,
              `member_id` text NULL,
              `rooms` VARCHAR(255) NULL,
              `hotelid` VARCHAR(255) NULL,
              `cityid` VARCHAR(255) NULL,
              `invoice_date` text NULL,
              `pending_commission` text NULL,
              `status` VARCHAR(255) NULL,
              `payment_commission` text NULL,
              `other` text NULL,
              `on_hold_at` datetime NULL,
              `created_time` datetime NULL,
              PRIMARY KEY (`vacation_actions_id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;


		");
        $installer->endSetup();
        echo "success";
    }

    public function updateDb8Action()
    {
        $setup = new Mage_Core_Model_Resource_Setup();
        $installer = $setup;
        $installer->startSetup();
        $installer->run("

            DROP TABLE IF EXISTS {$setup->getTable('manageapi/deals')};
            CREATE TABLE {$setup->getTable('manageapi/deals')} (
              `deals_id` int(10) unsigned NOT NULL auto_increment,
              `product_id` int(10) unsigned NULL,
              `categories` text NULL DEFAULT '',
              `promotiontypes` text NULL DEFAULT '',
              `offerdescription` VARCHAR(255) NULL DEFAULT '',
              `couponrestriction` VARCHAR(255) NULL DEFAULT '',
              `clickurl` VARCHAR(255) NULL DEFAULT '',
              `impressionpixel` VARCHAR(255) NULL DEFAULT '',
              `advertiserid` VARCHAR(255) NULL DEFAULT '',
              `advertisername` VARCHAR(255) NULL DEFAULT '',
              `couponcode` VARCHAR(255) NULL DEFAULT '',
              `network` VARCHAR(255) NULL DEFAULT '',
              `offerstartdate` datetime NULL,
              `offerenddate` datetime NULL,
              `product_created_at` datetime NULL,
              `created_at` datetime NULL,
              `updated_at` datetime NULL,
              PRIMARY KEY (`deals_id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
		");
        $installer->endSetup();
        echo "success";
    }

    public function updateDb9Action()
    {
        $setup = new Mage_Catalog_Model_Resource_Eav_Mysql4_Setup();
        $installer = $setup;
        $installer->startSetup();
        $installer->run("");

        $setup->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'primary_category', array(
            'group' => 'General',
            'input' => 'text',
            'type' => 'text',
            'sort_order' => 4,
            'label' => 'Primary Category',
            'backend' => '',
            'visible' => true,
            'required' => false,
            'wysiwyg_enabled' => false,
            'visible_on_front' => true,
            'used_in_product_listing' => true,
            'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
            'note' => '',
        ));

        $setup->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'primary_promotion_type', array(
            'group' => 'General',
            'input' => 'text',
            'type' => 'text',
            'sort_order' => 4,
            'label' => 'Primary Promotion Type',
            'backend' => '',
            'visible' => true,
            'required' => false,
            'wysiwyg_enabled' => false,
            'visible_on_front' => true,
            'used_in_product_listing' => true,
            'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
            'note' => '',
        ));

        $setup->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'created_from', array(
            'group' => 'General',
            'input' => 'boolean',
            'type' => 'int',
            'sort_order' => 4,
            'source' => 'eav/entity_attribute_source_boolean',
            'label' => 'Created From API',
            'backend' => '',
            'visible' => true,
            'required' => false,
            'wysiwyg_enabled' => false,
            'visible_on_front' => true,
            'used_in_product_listing' => true,
            'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
            'note' => '',
            'unique' => false,
            'user_defined' => false,
        ));

        $installer->updateAttribute(Mage_Catalog_Model_Product::ENTITY, 'created_from', 'apply_to',
            Magestore_CustomProduct_Model_Type::PRODUCT_TYPE_DAILY_DEALS);

        $installer->updateAttribute(Mage_Catalog_Model_Product::ENTITY, 'primary_category', 'apply_to',
            Magestore_CustomProduct_Model_Type::PRODUCT_TYPE_DAILY_DEALS);

        $installer->updateAttribute(Mage_Catalog_Model_Product::ENTITY, 'primary_promotion_type', 'apply_to',
            Magestore_CustomProduct_Model_Type::PRODUCT_TYPE_DAILY_DEALS);

        $installer->endSetup();
        echo "success";
    }

    public function productAction()
    {

//    Mage::helper('manageapi')->addNewOptionToAttribute('Kaka');
//    $arg_attribute = 'manufacturer';
//    $arg_value = 'Haha';
//
//    $attr_model = Mage::getModel('catalog/resource_eav_attribute');
//    $attr = $attr_model->loadByCode('catalog_product', $arg_attribute);
//    $attr_id = $attr->getAttributeId();
//
//    $option['attribute_id'] = $attr_id;
//    $option['value']['any_option_name'][0] = $arg_value;
//
//    $setup = new Mage_Eav_Model_Entity_Setup('core_setup');
//    $setup->addAttributeOption($option);

        $attribute_code = 'manufacturer';
        $attribute = Mage::getModel('eav/config')->getAttribute('catalog_product', $attribute_code);
        $options = $attribute->getSource()->getAllOptions();
        $optionsDelete = array();
        foreach ($options as $option) {
            if ($option['value'] != "") {
                $optionsDelete['delete'][$option['value']] = true;
                $optionsDelete['value'][$option['value']] = true;
            }
        }
        $installer = new Mage_Eav_Model_Entity_Setup('core_setup');
        $installer->addAttributeOption($optionsDelete);

//    zend_debug::dump(Mage::helper('attributeoptionimage')->getAttributeOptionImage(248));
//    zend_debug::dump(Mage::helper('attributeoptionimage')->getAttributeOptionImage(123));
//    $attribute = Mage::getSingleton('eav/config')
//        ->getAttribute(Mage_Catalog_Model_Product::ENTITY, 'manufacturer');
//
//    if ($attribute->usesSource()) {
//      $options = $attribute->getSource()->getAllOptions(false);
//    }
//
//    zend_debug::dump($options);
//
//
//
//    $product = Mage::getModel('catalog/product')->load(15757);
//    zend_debug::dump($product->debug());


        echo 'success';
    }

    public function check2Action()
    {
        Mage::helper('manageapi/cj')->checkOnHold2('2017-09-01');
        Mage::helper('manageapi/target')->checkOnHold2('2017-09-01');
        Mage::helper('manageapi/linkshare')->checkOnHold2('2017-09-01');
    }

    public function updateDb10Action()
    {
        $setup = new Mage_Core_Model_Resource_Setup();
        $installer = $setup;
        $installer->startSetup();
        $installer->run("

            DROP TABLE IF EXISTS {$setup->getTable('manageapi/popshopsmerchant')};
            CREATE TABLE {$setup->getTable('manageapi/popshopsmerchant')} (
              `popshops_merchant_id` int(10) unsigned NOT NULL auto_increment,
              `merchant_id` int(10) unsigned NULL,
              `name` VARCHAR(255) NULL DEFAULT '',
              `network` int(10) unsigned NULL,
              `product_count` int(10) unsigned NULL,
              `deal_count` int(10) unsigned NULL,
              `network_merchant_id` VARCHAR(255) NULL,
              `country` int(10) unsigned NULL,
              `category` int(10) unsigned NULL,
              `merchant_type` int(10) unsigned NULL,
              `logo_url` VARCHAR(255) NULL DEFAULT '',
              `url` text NULL DEFAULT '',
              `site_url` VARCHAR(255) NULL DEFAULT '',
              `created_at` datetime NULL,
              `updated_at` datetime NULL,
              PRIMARY KEY (`popshops_merchant_id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

            DROP TABLE IF EXISTS {$setup->getTable('manageapi/merchanttype')};
            CREATE TABLE {$setup->getTable('manageapi/merchanttype')} (
              `merchant_type_id` int(10) unsigned NOT NULL auto_increment,
              `type_id` int(10) unsigned NULL,
              `name` VARCHAR(255) NULL DEFAULT '',
              `merchant_count` int(10) unsigned NULL,
              `created_at` datetime NULL,
              `updated_at` datetime NULL,
              PRIMARY KEY (`merchant_type_id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

            DROP TABLE IF EXISTS {$setup->getTable('manageapi/dealtype')};
            CREATE TABLE {$setup->getTable('manageapi/dealtype')} (
              `deal_type_id` int(10) unsigned NOT NULL auto_increment,
              `type_id` int(10) unsigned NULL,
              `name` VARCHAR(255) NULL DEFAULT '',
              `created_at` datetime NULL,
              `updated_at` datetime NULL,
              PRIMARY KEY (`deal_type_id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

            DROP TABLE IF EXISTS {$setup->getTable('manageapi/coupondeal')};
            CREATE TABLE {$setup->getTable('manageapi/coupondeal')} (
              `coupon_deal_id` int(10) unsigned NOT NULL auto_increment,
              `deal_id` int(10) unsigned NULL,
              `name` VARCHAR(255) NULL DEFAULT '',
              `url` text NULL DEFAULT '',
              `merchant` int(10) unsigned NULL,
              `deal_type` int(10) unsigned NULL,
              `sku` VARCHAR(255) NULL DEFAULT '',
              `site_wide` VARCHAR(255) NULL DEFAULT '',
              `code` VARCHAR(255) NULL DEFAULT '',
              `description` VARCHAR(255) NULL DEFAULT '',
              `image_url` VARCHAR(255) NULL DEFAULT '',
              `restrictions` VARCHAR(255) NULL DEFAULT '',
              `start_on` datetime NULL,
              `end_on` datetime NULL,
              `product_created_at` datetime NULL,
              `product_id` int(10) unsigned NULL,
              `created_at` datetime NULL,
              `updated_at` datetime NULL,
              PRIMARY KEY (`coupon_deal_id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
		");
        $installer->endSetup();
        echo "success";
    }

    public function check3Action()
    {
        $attribute_code = 'manufacturer';
        $new_value = 'Twst 1';

        $attr_model = Mage::getModel('catalog/resource_eav_attribute');
        $attr = $attr_model->loadByCode('catalog_product', $attribute_code);

        $option = array(
            'value' => array(
                'option_0' => array(
                    $new_value,
                    $new_value,
                ),
            ),
            'order' => array(
                'option_0' => '',
            ),
            'image' => array(
                'option_0' => '',
            ),
            'delete' => array(
                'option_0' => '',
            ),
        );

        $attr->setOption($option);
        $attr->save();

        $attribute = Mage::getSingleton('eav/config')
            ->getAttribute(Mage_Catalog_Model_Product::ENTITY, $attribute_code);

        $options = null;
        if ($attribute->usesSource()) {
            $options = $attribute->getSource()->getAllOptions(false);
        }

        if (sizeof($options) > 0) {
            foreach ($options as $option) {
                if (strcasecmp($option['label'], $new_value) == 0) {
                    zend_debug::dump($option);
                    echo '<hr />';
                }
            }
        }

        echo 'success';
    }

    public function updateDb11Action()
    {
        $setup = new Mage_Core_Model_Resource_Setup(null);
        $installer = $setup;
        $installer->startSetup();
        $installer->run("

            DROP TABLE IF EXISTS {$setup->getTable('manageapi/walmart')};
            CREATE TABLE {$setup->getTable('manageapi/walmart')} (
              `walmart_id` int(10) unsigned NOT NULL auto_increment,
              `itemId` VARCHAR(255)  NULL,
              `specialOffer` VARCHAR(255) NULL DEFAULT '',
              `parentItemId` VARCHAR(255)  NULL,
              `name` VARCHAR(255) NULL DEFAULT '',
              `msrp` VARCHAR(255) NULL DEFAULT '',
              `salePrice` VARCHAR(255) NULL DEFAULT '',
              `upc` VARCHAR(255) NULL DEFAULT '',
              `categoryPath` VARCHAR(255) NULL DEFAULT '',
              `shortDescription` text NULL DEFAULT '',
              `longDescription` text NULL DEFAULT '',
              `brandName` VARCHAR(255) NULL DEFAULT '',
              `thumbnailImage` VARCHAR(255) NULL DEFAULT '',
              `mediumImage` VARCHAR(255) NULL DEFAULT '',
              `largeImage` VARCHAR(255) NULL DEFAULT '',
              `productTrackingUrl` text NULL DEFAULT '',
              `ninetySevenCentShipping` VARCHAR(255) NULL DEFAULT '',
              `standardShipRate` VARCHAR(255) NULL DEFAULT '',
              `twoThreeDayShippingRate` VARCHAR(255) NULL DEFAULT '',
              `size` VARCHAR(255) NULL DEFAULT '',
              `color` VARCHAR(255) NULL DEFAULT '',
              `marketplace` VARCHAR(255) NULL DEFAULT '',
              `shipToStore` VARCHAR(255) NULL DEFAULT '',
              `specialBuy` VARCHAR(255) NULL DEFAULT '',
              `freeShipToStore` VARCHAR(255) NULL DEFAULT '',
              `modelNumber` VARCHAR(255) NULL DEFAULT '',
              `productUrl` text NULL DEFAULT '',
              `customerRating` VARCHAR(255) NULL DEFAULT '',
              `numReviews` VARCHAR(255) NULL DEFAULT '',
              `customerRatingImage` VARCHAR(255) NULL DEFAULT '',
              `categoryNode` VARCHAR(255) NULL DEFAULT '',
              `rollBack` VARCHAR(255) NULL DEFAULT '',
              `bundle` VARCHAR(255) NULL DEFAULT '',
              `clearance` VARCHAR(255) NULL DEFAULT '',
              `preOrder` VARCHAR(255) NULL DEFAULT '',
              `stock` VARCHAR(255) NULL DEFAULT '',
              `gender` VARCHAR(255) NULL DEFAULT '',
              `age` VARCHAR(255) NULL DEFAULT '',
              `addToCartUrl` text NULL DEFAULT '',
              `affiliateAddToCartUrl` text NULL DEFAULT '',
              `freeShippingOver50Dollars` VARCHAR(255) NULL DEFAULT '',
              `maxItemsInOrder` VARCHAR(255) NULL DEFAULT '',
              `availableOnline` VARCHAR(255) NULL DEFAULT '',
              `is_bestsellers` SMALLINT NULL DEFAULT 0,
              `product_created_at` datetime NULL,
              `product_id` int(10) unsigned NULL,
              `created_at` datetime NULL,
              `updated_at` datetime NULL,
              PRIMARY KEY (`walmart_id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
		");
        $installer->endSetup();
        echo "success";
    }

    public function check4Action()
    {
      Mage::helper('manageapi/linkshare')->checkOnHold2();
    }

    public function updateDb12Action()
    {
        $setup = new Mage_Catalog_Model_Resource_Eav_Mysql4_Setup(null);
        $installer = $setup;
        $installer->startSetup();
        $installer->run("");

        $setup->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'daily_deals_msrp', array(
            'group' => 'Global Brand Product',
            'input' => 'text',
            'type' => 'text',
            'sort_order' => 4,
            'label' => 'Daily Deals Msrp',
            'backend' => '',
            'visible' => true,
            'required' => false,
            'wysiwyg_enabled' => false,
            'visible_on_front' => true,
            'used_in_product_listing' => true,
            'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
            'note' => 'The msrp from Walmart API',
        ));

        $setup->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'daily_deals_sale_price', array(
            'group' => 'Global Brand Product',
            'input' => 'text',
            'type' => 'text',
            'sort_order' => 4,
            'label' => 'Daily Deals Sale Price',
            'backend' => '',
            'visible' => true,
            'required' => false,
            'wysiwyg_enabled' => false,
            'visible_on_front' => true,
            'used_in_product_listing' => true,
            'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
            'note' => 'The sale price from Walmart API',
        ));

        $installer->updateAttribute(Mage_Catalog_Model_Product::ENTITY, 'daily_deals_msrp', 'apply_to',
            Magestore_CustomProduct_Model_Type::PRODUCT_TYPE_DAILY_DEALS);

        $installer->updateAttribute(Mage_Catalog_Model_Product::ENTITY, 'daily_deals_sale_price', 'apply_to',
            Magestore_CustomProduct_Model_Type::PRODUCT_TYPE_DAILY_DEALS);

        $installer->endSetup();
        echo "success";
    }

    public function updateDb13Action()
    {
        $setup = new Mage_Catalog_Model_Resource_Eav_Mysql4_Setup(null);
        $installer = $setup;
        $installer->startSetup();
        $installer->run("");

        $setup->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'specialOffer', array(
            'group' => 'Global Brand Product',
            'input' => 'text',
            'type' => 'text',
            'sort_order' => 10,
            'label' => 'Special Offer',
            'backend' => '',
            'visible' => true,
            'required' => false,
            'wysiwyg_enabled' => false,
            'visible_on_front' => true,
            'used_in_product_listing' => true,
            'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
            'note' => 'The special offer from Walmart API',
        ));

        $installer->updateAttribute(Mage_Catalog_Model_Product::ENTITY, 'specialOffer', 'apply_to',
            Magestore_CustomProduct_Model_Type::PRODUCT_TYPE_DAILY_DEALS);

        $installer->endSetup();
        echo "success";
    }

    public function updateDb14Action()
    {
        $setup = new Mage_Core_Model_Resource_Setup(null);
        $installer = $setup;
        $installer->startSetup();
        $installer->run("
              ALTER TABLE {$setup->getTable('manageapi/walmart')} ADD `is_bestsellers` SMALLINT NULL DEFAULT 0;
            ");
        $installer->endSetup();
        echo "success";
    }

    public function updateDb15Action()
    {
        $setup = new Mage_Core_Model_Resource_Setup(null);
        $installer = $setup;
        $installer->startSetup();
        $installer->run("
            DROP TABLE IF EXISTS {$setup->getTable('manageapi/walmartcategory')};
            CREATE TABLE {$setup->getTable('manageapi/walmartcategory')} (
              `category_id` int(10) unsigned NOT NULL auto_increment,
              `id` VARCHAR(255) NOT NULL,
              `parent_id` VARCHAR(255)  NULL,
              `name` VARCHAR(255) NULL DEFAULT '',
              `path` VARCHAR(255) NULL DEFAULT '',
              `level` SMALLINT unsigned,
              `created_at` datetime NULL,
              `updated_at` datetime NULL,
              PRIMARY KEY (`category_id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
		");
        $installer->endSetup();
        echo "success";
    }

    public function exportCSVAction()
    {
        $url = "http://api.walmartlabs.com/v1/taxonomy?apiKey=693rnk25yabscpjjm7waajmp&format=json";
        $file_path = Mage::getBaseDir() . '/category.csv';
        $mage_csv = new Varien_File_Csv();
        $_data = $this->getHelperData()->getContentByCurl($url);
        $data = json_decode($_data, true);
        zend_debug::dump($data['categories'][0]);
        $rs = array();

        $rs[] = array(
            'id' => 'ID',
            'name' => 'Name',
            'path' => 'Path',
        );
        foreach ($data['categories'] as $cat) {
            $rs[] = array(
                'id' => $cat['id'],
                'name' => $cat['name'],
                'path' => $cat['path'],
            );
        }

        $mage_csv->saveData($file_path, $rs);
    }

    public function updateDb16Action()
    {
        $setup = new Mage_Catalog_Model_Resource_Eav_Mysql4_Setup(null);
        $installer = $setup;
        $installer->startSetup();
        $installer->run("");

        $setup->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'is_bestsellers', array(
            'group' => 'Global Brand Product',
            'input' => 'boolean',
            'type' => 'int',
            'sort_order' => 20,
            'source' => 'eav/entity_attribute_source_boolean',
            'label' => 'Is Bestsellers',
            'backend' => '',
            'visible' => true,
            'required' => false,
            'wysiwyg_enabled' => false,
            'visible_on_front' => true,
            'used_in_product_listing' => true,
            'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
            'note' => '',
            'unique' => false,
            'user_defined' => false,
        ));

        $installer->updateAttribute(
            Mage_Catalog_Model_Product::ENTITY,
            'is_bestsellers',
            'apply_to',
            Magestore_CustomProduct_Model_Type::PRODUCT_TYPE_DAILY_DEALS
        );

        $installer->endSetup();
        echo "success";
    }
}
