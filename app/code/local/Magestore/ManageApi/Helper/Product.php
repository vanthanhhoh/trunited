<?php

class Magestore_ManageApi_Helper_Product extends Mage_Core_Helper_Abstract
{
    public function getHelperData()
    {
        return Mage::helper('manageapi');
    }

    public function getSearchApiUrl($params, $page)
    {
        $helper = $this->getHelperData();
        $url = '';

        if($helper->getDataConfig('enable', 'popshops_api')){
            $url .= $helper->getDataConfig('product_api', 'popshops_api');
            $url .= '&account=' . $helper->getDataConfig('account_public_api_key', 'popshops_api');
            $url .= '&catalog=' . $helper->getDataConfig('catalog_key', 'popshops_api');
            $url .= '&keyword=' . $params['q'];

            if($page == null || !isset($params['current_page_dd']))
                $page = $helper->getDataConfig('page', 'product_popshops_api');
            else {
                $page = $params['current_page_dd'];
            }

            $url .= '&page='.$page;
            $url .= '&results_per_page='.$helper->getDataConfig('results_per_page', 'product_popshops_api');

            if($helper->getDataConfig('product_spec', 'product_popshops_api') == 1)
                $url .= '&product_spec=true';
            else
                $url .= '&product_spec=false';

            if($helper->getDataConfig('include_identifiers', 'product_popshops_api') == 1)
                $url .= '&include_identifiers=true';
            else
                $url .= '&include_identifiers=false';
            
            if($helper->getDataConfig('include_discounts', 'product_popshops_api') == 1)
                $url .= '&include_discounts=true';
            else
                $url .= '&include_discounts=false';

            if(isset($params['is_advanced_search']) && $params['is_advanced_search'] == 1){
                if(isset($params['merchant']) && sizeof($params['merchant']) > 0)
                {
                    $_merchants = implode(',', $params['merchant']);
                    $url .= '&merchant='.$_merchants;
                }

                if(isset($params['merchant_type']) && sizeof($params['merchant_type']) > 0)
                {
                    $_merchant_types = implode(',', $params['merchant_type']);
                    $url .= '&merchant_type='.$_merchant_types;
                }

                if(isset($params['sort_product']) && $params['sort_product'] != '')
                {
                    $url .= '&sort_product='.$params['sort_product'];
                }

                if(isset($params['percent_off']) && $params['percent_off'] != '')
                {
                    $percent = explode('-', str_replace('%', '', $params['percent_off']));
                    $_percent = trim($percent[0]).'-'.trim($percent[1]);
                    $url .= '&percent_off='.$_percent;
                }

                if(isset($params['price']) && $params['price'] != '')
                {
                    $price = explode('-', str_replace('$', '', $params['price']));
                    $_price = trim($price[0]).'-'.trim($price[1]);
                    $url .= '&price='.$_price;
                }
            }
            
            $url = str_replace('?&', '?', $url);

        }

        return $url;
    }

    public function processAPI($params, $page = 1)
    {
        $data = array();
        $url = $this->getSearchApiUrl($params, $page);
        
        if (strpos($url, '.xml') > 0) {
            $_data = $this->getHelperData()->getDataXML($url);
            $data = json_decode(json_encode((array)$_data), 1);
        } else if (strpos($url, '.json') > 0) {
            $_data = $this->getHelperData()->getContentByCurl($url);
            $data = json_decode($_data, true);
        } else {
            return $data;
        }

        if($data == null || sizeof($data['results']['products']['product']) == 0){
            return array();
        }

        return $data;
    }

    public function getInfoProduct($product_id)
    {
        $helper = $this->getHelperData();
        $url = '';

        if($helper->getDataConfig('enable', 'popshops_api')){
            $url .= $helper->getDataConfig('product_api', 'popshops_api');
            $url .= '&account=' . $helper->getDataConfig('account_public_api_key', 'popshops_api');
            $url .= '&catalog=' . $helper->getDataConfig('catalog_key', 'popshops_api');
            $url .= '&product=' . $product_id;

            $url = str_replace('?&', '?', $url);

        }

        $data = array();
        
        if($url != ''){
            if (strpos($url, '.xml') > 0) {
                $_data = $this->getHelperData()->getDataXML($url);
                $data = json_decode(json_encode((array)$_data), 1);
            } else if (strpos($url, '.json') > 0) {
                $_data = $this->getHelperData()->getContentByCurl($url);
                $data = json_decode($_data, true);
            } else {
                return $data;
            }

            if($data == null || sizeof($data['results']['products']['product']) == 0){
                return array();
            }
        }
        
        return $data;
    }
}
