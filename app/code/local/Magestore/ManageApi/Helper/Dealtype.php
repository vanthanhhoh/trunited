<?php

class Magestore_ManageApi_Helper_Dealtype extends Mage_Core_Helper_Abstract
{
    public function getHelperData()
    {
        return Mage::helper('manageapi');
    }

    public function processAPI($url)
    {
        $data = null;
        $_data = $this->getHelperData()->getDataXML($url);
        $data = json_decode(json_encode((array)$_data), 1);

        $count = 0;
        $update = 0;

        if (intval($data['results']['deal_types']['@attributes']['count']) <= 0) {
            Mage::getSingleton('adminhtml/session')->addNotice(
                Mage::helper('manageapi')->__('No records from the API')
            );
        } else {
            $results = $data['results']['deal_types']['deal_type'];
            if ($results != null && is_array($results) && sizeof($results) > 0) {
                $transactionSave = Mage::getModel('core/resource_transaction');
                try {

                    $_count = sizeof($results);
                    foreach ($results as $_sale) {

                        if($_count == 1)
                            $sale = $_sale;
                        else
                            $sale = $_sale['@attributes'];

                        $id = $this->checkModelId($sale['id']);

                        $model = Mage::getModel('manageapi/dealtype');
                        $sale['type_id'] = $sale['id'];

                        $model->setData($sale);
                        $model->setId($id);

                        $transactionSave->addObject($model);

                        if ($id != null && $id > 0)
                            $update++;
                        else
                            $count++;
                    }

                    $transactionSave->save();

                } catch (Exception $e) {
                    Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                }
            } else {
                Mage::getSingleton('adminhtml/session')->addError('Please wait few seconds before running API again.');
            }
        }

        return array(
            'new' => $count,
            'update' => $update,
        );
    }

    public function checkModelId($type_id)
    {
        $model = Mage::getModel('manageapi/dealtype')->getCollection()
            ->addFieldToFilter('type_id', array('eq' => $type_id))
            ->getFirstItem();

        if ($model != null && $model->getId())
            return $model->getId();
        else
            return null;
    }
}
