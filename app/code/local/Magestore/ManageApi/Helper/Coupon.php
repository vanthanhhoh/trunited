<?php

class Magestore_ManageApi_Helper_Coupon extends Mage_Core_Helper_Abstract
{
    public function getHelperData()
    {
        return Mage::helper('manageapi');
    }

    function simpleXmlToArray($xmlObject)
    {
        $array = [];
        $data = (array)$xmlObject;

        foreach ($data as $k => $v) {
            if ($v instanceof SimpleXMLElement) {
                $link_dt = array();
                foreach ((array)$v as $_k => $_v) {
                    $link_dt[$_k] = $_v instanceof SimpleXMLElement ? json_encode((array)$_v) : $_v;
                }
                $array[$k][] = $link_dt;
            } else if (is_array($v) && sizeof($v) > 0) {
                foreach ($v as $_v) {
                    $link_dt = array();
                    foreach ((array)$_v as $_d => $_dt) {
                        $link_dt[$_d] = $_dt instanceof SimpleXMLElement ? json_encode((array)$_dt) : $_dt;
                    }
                    $array[$k][] = $link_dt;
                }
            } else {
                $array[$k] = $v;
            }
        }

        return $array['link'];
    }

    public function processAPI($url, $is_create_product = false)
    {
        $data = null;
        $_data = $this->getHelperData()->getDataXML($url, $this->getParamsHeader());
        // $_data = $this->getHelperData()->getDataXMLCouponAPI($url, $this->getParamsHeader());
        $refresh_token = null;
        $new_token_request = null;
        if (strpos($_data, 'Token Expired') > 0 || strpos($_data, 'Token Inactive') > 0
            || strpos($_data, 'Access Token Inactive') > 0
        ) {
            $refresh_token_data = $this->getRefreshToken();
            $refresh_data = json_decode($refresh_token_data, true);

            if (is_array($refresh_data) && isset($refresh_data['refresh_token'])) {
                $refresh_token = $refresh_data['refresh_token'];
            }

            if ($refresh_token != null) {
                $new_token_request_data = $this->getNewTokenRequest($refresh_token);
                $token_request_data = json_decode($new_token_request_data, true);

                if (is_array($token_request_data) && isset($token_request_data['refresh_token'])) {
                    $new_token_request = $token_request_data['access_token'];
                }

                if ($new_token_request != null) {
                    $_data = $this->getHelperData()->getDataXML($url, $this->getParamsHeader('Bearer ' . $new_token_request));
                }
            }
        }

        $data = json_decode(json_encode((array)$_data), 1);

        $count = 0;
        $update = 0;
        $product_count = 0;
        $data_products = array();
        $manufacturers = array();

        if (intval($data['TotalMatches']) <= 0) {
            Mage::getSingleton('adminhtml/session')->addNotice(
                Mage::helper('manageapi')->__('No records from the API')
            );
        } else {
            if ($data['link'] != null && is_array($data['link']) && sizeof($data['link']) > 0) {
                $transactionSave = Mage::getModel('core/resource_transaction');
                try {
                    $_result = $data['link'];

                    if (!is_array($_result[0])) {
                        $id = $this->checkModelId($_result['offerdescription'], $_result['network']);
                        $_result['categories'] = is_array($_result['categories']) ? json_encode($_result['categories']) :
                            $_result['categories'];
                        $_result['promotiontypes'] = is_array($_result['promotiontypes']) ? json_encode($_result['promotiontypes']) : $_result['promotiontypes'];
                        $model = Mage::getModel('manageapi/deals');
                        $model->setData($_result);
                        $model->setId($id);
                        $transactionSave->addObject($model);
                        if ($id != null && $id > 0)
                            $update++;
                        else
                            $count++;

                        $manufacturers[] = $_result['advertisername'];

                        $data_products[] = $_result;

                    } else {

                        foreach ($data['link'] as $sale) {
                            $id = $this->checkModelId($sale['offerdescription'], $sale['network']);
                            $sale['categories'] = is_array($sale['categories']) ? json_encode($sale['categories']) :
                                $sale['categories'];
                            $sale['promotiontypes'] = is_array($sale['promotiontypes']) ? json_encode
                            ($sale['promotiontypes']) : $sale['promotiontypes'];
                            $model = Mage::getModel('manageapi/deals');
                            $model->setData($sale);
                            $model->setId($id);
                            $transactionSave->addObject($model);
                            if ($id != null && $id > 0)
                                $update++;
                            else
                                $count++;

                            if (!in_array($sale['advertisername'], $manufacturers))
                                $manufacturers[] = $sale['advertisername'];

                            $data_products[] = $sale;
                        }
                    }

                    $transactionSave->save();

//                $product_count = $this->updateAttributeValue($manufacturers, false, $data_products);
                    $product_count = $this->updateAttributeValue($manufacturers, false, $data_products);

                } catch (Exception $e) {
                    Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                }
            } else {
                Mage::getSingleton('adminhtml/session')->addError('Please wait few seconds before running API again.');
            }
        }

        return array(
            'new' => $count,
            'update' => $update,
            'product' => $product_count,
        );
    }

    public function updateAttributeValue($data, $is_create_product, $data_products = array())
    {
        $rs = array(
            'new' => 0,
            'update' => 0,
        );
        if (sizeof($data) > 0) {
            foreach ($data as $dt) {
                if ($dt != '')
                    Mage::helper('manageapi')->addNewOptionToAttribute($dt);
            }
        }

        if ($is_create_product && is_array($data_products) && sizeof($data_products) > 0) {
            $rs = $this->createMultipleProducts($data_products);
        }

        return $rs;
    }

    public function createMultipleProducts($data)
    {
        $new = 0;
        $update = 0;
        if (sizeof($data) > 0) {
            $transactionSave = Mage::getModel('core/resource_transaction');
            try {
                foreach ($data as $dt) {
                    if ($dt['offerdescription'] != '') {
                        $product_id = $this->checkProductId($dt['offerdescription']);
                        $product = Mage::getModel('catalog/product');
                        $categories = json_decode($dt['categories'], true);
                        $promotion_types = json_decode($dt['promotiontypes'], true);

                        $_dt = array(
                            'website_ids' => array(1),
                            'attribute_set_id' => 4,
                            'type_id' => 'dailyproduct',
                            'name' => $dt['offerdescription'],
                            'short_description' => $dt['offerdescription'],
                            'description' => $dt['offerdescription'],
                            'shop_now' => $dt['clickurl'] . '&u1={{customer_id}}',
                            'sku' => 'daily-deals-' . $this->generateRandomString() . '-' . date('Ymd', time()),
                            'url_key' => strtolower('daily-deals-' . $this->generateRandomString() . '-' . date('Ymd', time())),
                            'news_from_date' => date('m/d/Y', strtotime($dt['offerstartdate'])),
                            'news_to_date' => date('m/d/Y', strtotime($dt['offerenddate'])),
                            'status' => 1,
                            'visibility' => Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH,
                            'manufacturer' => $this->getAttributeId($dt['advertisername']),
                            'primary_category' => is_array($categories['category']) ? $categories['category'][0] :
                                $categories['category'],
                            'primary_promotion_type' => is_array($promotion_types['promotiontype']) ? $promotion_types['promotiontype'][0] : $promotion_types['promotiontype'],
                            'options_container' => 'container1',
                            'page_layout' => 'one_column',
                            /*'stock_data' => array(
                                'use_config_manage_stock' => 0,
                                'manage_stock' => 0,
                                'min_sale_qty' => 0,
                                'max_sale_qty' => 0,
                                'is_in_stock' => 1,
                                'qty' => 1000
                            ),*/
                            'category_ids' => array(71, 72),
                            'created_from' => 1,
                        );

                        $product->setData($_dt);
                        $product->setId($product_id);
                        $product->save();

                        $this->updateRecord($dt['offerdescription'], $dt['network'], $product);

                        if ($product_id > 0)
                            $update++;
                        else
                            $new++;
                    }

//                    $transactionSave->addObject($product);
                }

                $transactionSave->save();
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
        }

        return array(
            'new' => $new,
            'update' => $update
        );
    }

    public function updateRecord($offerdescription, $network, $product)
    {
        $model = Mage::getModel('manageapi/deals')->getCollection()
            ->addFieldToFilter('offerdescription', $offerdescription)
            ->addFieldToFilter('network', $network)
            ->setOrder('deals_id', 'desc')
            ->getFirstItem();

        if ($model != null && $model->getId()) {
            $model->setData('product_created_at', now());
            $model->setData('product_id', $product->getId());
            $model->setData('updated_at', now());
            $model->save();
        }
    }

    function generateRandomString($length = 5)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public function getAttributeId($attribute_name)
    {
        $options = Mage::helper('manageapi')->getOptionAttribute();
        $id = null;

        if ($options == null) {
            Mage::helper('manageapi')->addNewOptionToAttribute($attribute_name);
        }

        if (sizeof($options) > 0) {
            foreach ($options as $option) {
                if (strcasecmp($option['label'], $attribute_name) == 0) {
                    $id = $option['value'];
                    break;
                }
            }

            if ($id == null) {
                Mage::helper('manageapi')->addNewOptionToAttribute($attribute_name);
            }
        }

        if ($id == null) {
            $options = Mage::helper('manageapi')->getOptionAttribute();
            foreach ($options as $option) {
                if (strcasecmp($option['label'], $attribute_name) == 0) {
                    $id = $option['value'];
                    break;
                }
            }
        }

        return $id;
    }

    public function checkProductId($name)
    {
        $model = Mage::getModel('catalog/product')->getCollection()
            ->addAttributeToFilter('name', array('eq' => $name))
            ->getFirstItem();

        if ($model != null && $model->getId())
            return $model->getId();
        else
            return null;
    }

    public function checkModelId($name, $network_id)
    {
        $model = Mage::getModel('manageapi/deals')->getCollection()
            ->addFieldToFilter('offerdescription', array('eq' => $name))
            ->addFieldToFilter('network', array('eq' => $network_id))
            ->getFirstItem();

        if ($model != null && $model->getId())
            return $model->getId();
        else
            return null;
    }

    public function processCron()
    {
        $enable = $this->getHelperData()->getDataConfig('enable', 'coupon_api');
        if ($enable) {
            $url = $this->getHelperData()->getDataConfig('link_api', 'coupon_api');
            if ($url != null) {
                $_url = Mage::helper('manageapi')->processCouponApiURL();
                $this->processAPI($_url);
            }
        }
    }

    public function getParamsHeader($authorize_token = null)
    {
        if ($authorize_token == null)
            $authorize_token = $this->getHelperData()->getDataConfig('token_request_authorization', 'coupon_api');

        return array(
            "authorization: $authorize_token",
            "content-type: application/xml"
        );
    }

    public function getRefreshToken()
    {
        $token_request = $this->getHelperData()->getDataConfig('token_request', 'coupon_api');
        $url = $this->getHelperData()->getDataConfig('url_request_token', 'coupon_api');
        $data = $this->getHelperData()->getContentByCurl(
            $url,
            array(
                "Authorization: $token_request",
                "Accept: text/json"
            ),
            true,
            array(
                'grant_type' => 'password',
                'username' => $this->getHelperData()->getDataConfig('username', 'coupon_api'),
                'password' => $this->getHelperData()->getDataConfig('password', 'coupon_api'),
                'scope' => $this->getHelperData()->getDataConfig('site_id', 'coupon_api'),
            )
        );

        return $data;
    }

    public function getNewTokenRequest($refresh_token)
    {
        $token_request = $this->getHelperData()->getDataConfig('token_request', 'coupon_api');
        $url = $this->getHelperData()->getDataConfig('url_request_token', 'coupon_api');
        $data = $this->getHelperData()->getContentByCurl(
            $url,
            array(
                "Authorization: $token_request",
                "Accept: text/json"
            ),
            true,
            array(
                'grant_type' => 'refresh_token',
                'refresh_token' => $refresh_token,
                'scope' => $this->getHelperData()->getDataConfig('site_id', 'coupon_api'),
            )
        );

        return $data;
    }

    public function synchronizeProduct($deals_ids)
    {
        $count = 0;
        if (sizeof($deals_ids) > 0) {
            $deals = Mage::getModel('manageapi/deals')->getCollection()
                ->addFieldToFilter('deals_id', array('in' => $deals_ids))
                ->setOrder('deals_id', 'desc');

            if (sizeof($deals) > 0) {
                $transactionSave = Mage::getModel('core/resource_transaction');
                foreach ($deals as $deal) {
                    if ($deal->getProductId() > 0) {
                        $product = Mage::getModel('catalog/product')->load($deal->getProductId());

                        $categories = json_decode($deal->getData('categories'), true);
                        $promotion_types = json_decode($deal->getData('promotiontypes'), true);

                        $_dt = array(
                            'name' => $deal->getData('offerdescription'),
                            'short_description' => $deal->getData('offerdescription'),
                            'description' => $deal->getData('offerdescription'),
                            'shop_now' => $deal->getData('clickurl') . '&u1={{customer_id}}',
                            'news_from_date' => date('m/d/Y', strtotime($deal->getData('offerstartdate'))),
                            'news_to_date' => date('m/d/Y', strtotime($deal->getData('offerenddate'))),
                            'manufacturer' => $this->getAttributeId($deal->getData('advertisername')),
                            'primary_category' => is_array($categories['category']) ? $categories['category'][0] : $categories['category'],
                            'primary_promotion_type' => is_array($promotion_types['promotiontype']) ? $promotion_types['promotiontype'][0] : $promotion_types['promotiontype'],
                            'options_container' => 'container1',
                            'page_layout' => 'one_column',
                        );

                        $product->setData($_dt);
                        $product->setId($deal->getProductId());
                        $product->save();
//                        $transactionSave->addObject($product);
                    } else {
                        $product_id = $this->checkProductId($deal->getData('offerdescription'));
                        $product = Mage::getModel('catalog/product');
                        $categories = json_decode($deal->getData('categories'), true);
                        $promotion_types = json_decode($deal->getData('promotiontypes'), true);

                        $_dt = array(
                            'website_ids' => array(1),
                            'attribute_set_id' => 4,
                            'type_id' => 'dailyproduct',
                            'name' => $deal->getData('offerdescription'),
                            'short_description' => $deal->getData('offerdescription'),
                            'description' => $deal->getData('offerdescription'),
                            'shop_now' => $deal->getData('clickurl') . '&u1={{customer_id}}',
                            'sku' => 'daily-deals-' . $this->generateRandomString() . '-' . date('Ymd', time()),
                            'url_key' => strtolower('daily-deals-' . $this->generateRandomString() . '-' . date('Ymd', time())),
                            'news_from_date' => date('m/d/Y', strtotime($deal->getData('offerstartdate'))),
                            'news_to_date' => date('m/d/Y', strtotime($deal->getData('offerenddate'))),
                            'status' => 1,
                            'visibility' => Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH,
                            'manufacturer' => $this->getAttributeId($deal->getData('advertisername')),
                            'country_of_manufacture' => 'US',
                            'primary_category' => is_array($categories['category']) ? $categories['category'][0] : $categories['category'],
                            'primary_promotion_type' => is_array($promotion_types['promotiontype']) ? $promotion_types['promotiontype'][0] : $promotion_types['promotiontype'],
                            'category_ids' => array(71, 72),
                            'options_container' => 'container1',
                            'page_layout' => 'one_column',
                            'created_from' => 1,
                        );

                        $product->setData($_dt);
                        $product->setId($product_id);
                        $product->save();

                        $this->updateRecord($deal->getData('offerdescription'), $deal->getData('network'), $product);

                    }

                    $count++;
                }
                $transactionSave->save();
            }
        }

        return $count;
    }
}
