<?php

class Magestore_ManageApi_Helper_Linkshare extends Mage_Core_Helper_Abstract
{
    public function getHelperData()
    {
        return Mage::helper('manageapi');
    }

    public function processAPI($url, $file, $start_date)
    {
        $data = $this->getHelperData()->getDataCSV($url, $file);
        if($data != null && is_array($data))
        {
            $transactionSave = Mage::getModel('core/resource_transaction');

            try {

                if(sizeof($data) > 1)
                {
                    $flag = 0;
                    foreach ($data as $ls) {
                        if(sizeof($ls) == 2)
                        {
                            Mage::getSingleton('adminhtml/session')->addError('LINKSHARE API: '.$ls[0].$ls[1]);
                            break;
                        }
                        if($flag > 0)
                        {
                            $model = Mage::getModel('manageapi/linkshare');
                            $_dt = array(
                                'member_id' => $ls[0],
                                'mid' => $ls[1],
                                'advertiser_name' => $ls[2],
                                'order_id' => $ls[3],
                                'transaction_date' => date('Y-m-d',strtotime($ls[4].' '.$ls[5])),
                                'sku' => $ls[6],
                                'sales' => $ls[7],
                                'items' => $ls[8],
                                'total_commission' => $ls[9],
                                'process_date' => date('Y-m-d',strtotime($ls[10].' '.$ls[11])),
                                'created_at' => now(),
                            );

                            $model->setData($_dt);
                            $transactionSave->addObject($model);
                        }
                        $flag++;
                    }
                    Mage::log('LINKSHARE API at '.date('Y-m-d H:i:s', time()).' - Result:'.sizeof($data).' - URL: '.$url, null, 'check_manage_api.log');
                } else {
                    Mage::getSingleton('adminhtml/session')->addError('LINK SHARE API: Something was wrong with this response. Please click <a href="'.$url.'" target="_blank">here</a> view more detailed information.');
                }

                $transactionSave->save();

            } catch (Exception $e) {

            }
        }

        $this->checkOnHold($start_date);
    }

    public function processCron()
    {
        $enable = $this->getHelperData()->getDataConfig('enable', 'link_share');
        if($enable)
        {
            $url = $this->getHelperData()->getDataConfig('link_share_api', 'link_share');
            if($url != null)
            {
                $start_date = $end_data = date('Y-m-d',strtotime('-1 day', time()));
                $_url = str_replace(array('{{start_date}}','{{end_date}}'),array($start_date, $end_data), $url);
                $file = Mage::getBaseDir('media') . DS . Magestore_ManageApi_Helper_Data::LINK_SHARE_FILE;
                $this->processAPI($_url, $file, $start_date);
            }
        }
    }

    public function checkOnHold($start_date)
    {
//        $rewardpoints_transactions = Mage::getModel('rewardpoints/transaction')
//            ->getCollection()
//            ->addFieldToSelect('order_increment_id')
//            ->addFieldToFilter('order_increment_id', array('notnull' => true))
//            ->setOrder('transaction_id', 'desc')
//        ;
//        $order_ids = $rewardpoints_transactions->getColumnValues('order_increment_id');

        $helper = Mage::helper('manageapi');

        $readConnection = Mage::getSingleton('core/resource')->getConnection('core_read');

        $table_name = Mage::getSingleton('core/resource')->getTableName('manageapi/linkshare');
        $sql = "SELECT member_id as customer_id, advertiser_name, process_date, order_id, SUM(total_commission) as on_hold_points ";
        $sql .= " FROM $table_name";
        $sql .= " WHERE process_date >= '$start_date' and total_commission > 0";
        $sql .= " GROUP BY order_id";

        $result = $readConnection->fetchAll($sql);
        if(sizeof($result) > 0)
        {
            try{
                $enable_fundraiser = $helper->getDataConfig('enable_fundraiser', 'affiliate_points');
                $enable_daily_deals = $helper->getDataConfig('enable_daily_deals', 'affiliate_points');
                $suffix_fundraiser = $helper->getDataConfig('suffix_fundraiser', 'affiliate_points');
                $suffix_daily_deals = $helper->getDataConfig('suffix_daily_deals', 'affiliate_points');
                $day_hold_points = $helper->getDataConfig('day_hold_points', 'affiliate_points');
                $day_hold_credits = Mage::helper('affiliateearnings')->getConfigData('general', 'day_hold_credit');
                $percent_on_hold_credit = Mage::helper('affiliateearnings')->getConfigData('general', 'percent_on_hold_credit') != '' ? Mage::helper('affiliateearnings')->getConfigData('general', 'percent_on_hold_credit') : 1;
                $enable_gcm = Mage::getStoreConfig('rewardpoints/general/enable_giftcardmall');
                $date_gcm = Mage::getStoreConfig('rewardpoints/general/day_giftcardmall');

                foreach ($result as $linkshare) {

                    $refclickid = $linkshare['customer_id'];

                    if ($helper->checkID($refclickid, $suffix_fundraiser) > 0 && $enable_fundraiser) {
                        $customer_id = $helper->getOriginId($refclickid, $suffix_fundraiser);
                        $customer = Mage::getModel('customer/customer')->load($customer_id);
                        if ($customer != null && $customer->getId()) {
                            $affiliatepointsAccount = Mage::helper('affiliatepoints/account')
                                ->loadByCustomerId($customer->getId());

                            if ($affiliatepointsAccount != null && $affiliatepointsAccount->getId()) {
                                $expiration_on = Mage::helper('affiliatepoints')->addDaysToDate(
                                    $linkshare['process_date'],
                                    $day_hold_points
                                );
                                $params = array(
                                    'title' => Mage::helper('manageapi')->__('Points awarded for global brand %s order %s on %s', $linkshare['advertiser_name'], $linkshare['order_id'], $linkshare['process_date']),
                                    'award_from' => Magestore_Affiliatepoints_Model_Status::AWARD_FROM_FUNDRAISER,
                                    'current_point' => $affiliatepointsAccount->getAffiliatePoints(),
                                    'is_on_hold' => 1,
                                    'hold_point' => $linkshare['on_hold_points'],
                                    'expiration_at' => $expiration_on,
                                    'created_at' => $linkshare['process_date'],
                                    'order_id' => $linkshare['order_id']
                                );
                                Mage::helper('affiliatepoints/transaction')->createTransaction(
                                    $affiliatepointsAccount,
                                    $params,
                                    Magestore_Affiliatepoints_Model_Type::TYPE_TRANSACTION_RECEIVE_FROM_GLOBAL_BRAND,
                                    Magestore_Affiliatepoints_Model_Status::STATUS_TRANSACTION_ON_HOLD
                                );
                            }
                        }
                    } else if ($helper->checkID($refclickid, $suffix_daily_deals) > 0 && $enable_daily_deals) {
                        $customer_id = $helper->getOriginId($refclickid, $suffix_daily_deals);
                        $customer = Mage::getModel('customer/customer')->load($customer_id);
                        if ($customer != null && $customer->getId()) {
                            $affiliateearningsAccount = Mage::helper('affiliateearnings/account')
                                ->loadByCustomerId($customer->getId());

                            if ($affiliateearningsAccount != null && $affiliateearningsAccount->getId()) {
                                $expiration_on = Mage::helper('affiliateearnings')->addDaysToDate(
                                    $linkshare['process_date'],
                                    $day_hold_credits
                                );
                                $params = array(
                                    'title' => Mage::helper('manageapi')->__('Guest Commission awarded for global brand %s order %s on %s', $linkshare['advertiser_name'], $linkshare['order_id'], $linkshare['process_date']),
                                    'award_from' => Magestore_Affiliateearnings_Model_Status::AWARD_FROM_DAILY_DEALS,
                                    'current_credit' => $affiliateearningsAccount->getGuestCommission(),
                                    'is_on_hold' => 1,
                                    'hold_credit' => $linkshare['on_hold_points'] * $percent_on_hold_credit * 0.01,
                                    'expiration_at' => $expiration_on,
                                    'created_at' => $linkshare['process_date'],
                                    'order_id' => $linkshare['order_id']
                                );
                                Mage::helper('affiliateearnings/transaction')->createTransaction(
                                    $affiliateearningsAccount,
                                    $params,
                                    Magestore_Affiliateearnings_Model_Type::TYPE_TRANSACTION_RECEIVE_FROM_GLOBAL_BRAND,
                                    Magestore_Affiliateearnings_Model_Status::STATUS_TRANSACTION_ON_HOLD
                                );
                            }
                        }
                    } else {
                        $customer = Mage::getModel('customer/customer')->load($linkshare['customer_id']);
                        if($customer != null && $customer->getId()){
                            $exist_transaction = Mage::getModel('rewardpoints/transaction')
                                ->getCollection()
                                ->addFieldToFilter('is_on_hold', 1)
                                ->addFieldToFilter('order_increment_id', array('eq' => $linkshare['order_id']))
                                ->setOrder('transaction_id', 'DESC')
                                ->getFirstItem()
                            ;

                            if($exist_transaction != null && $exist_transaction->getId()){
                                $exist_transaction->setData('updated_time', now());
                                $exist_transaction->setData('on_hold_at', now());
                                $exist_transaction->setData('hold_point', $linkshare['on_hold_points']);
                                $exist_transaction->save();

                                if(strcasecmp($linkshare['advertiser_name'], 'GiftCardMall.com') == 0  &&
                                    $enable_gcm &&
                                    strtotime($linkshare['process_date']) >= strtotime($date_gcm)
                                ){

                                    if($linkshare['on_hold_points'] > $exist_transaction->getData('hold_point')){
                                        $update_points = $linkshare['on_hold_points'] - $exist_transaction->getData('hold_point');
                                        $rewardAccount = Mage::getModel('rewardpoints/customer')->load(
                                            $exist_transaction->getCustomerId(),
                                            'customer_id'
                                        );

                                        if($rewardAccount != null && $rewardAccount->getId()){
                                            $rewardAccount->setPointBalance(
                                                $rewardAccount->getPointBalance() + $update_points
                                            );

                                            $rewardAccount->save();
                                        }

                                    } else if($linkshare['on_hold_points'] < $exist_transaction->getData('hold_point')) {
                                        $update_points = $exist_transaction->getData('hold_point') - $linkshare['on_hold_points'];
                                        $rewardAccount = Mage::getModel('rewardpoints/customer')->load(
                                            $exist_transaction->getCustomerId(),
                                            'customer_id'
                                        );

                                        if($rewardAccount != null && $rewardAccount->getId()){
                                            $new_points = $rewardAccount->getPointBalance() - $update_points;
                                            $rewardAccount->setPointBalance(
                                                $new_points
                                            );

                                            $rewardAccount->save();
                                        }
                                    }
                                }
                            } else {
                                $transaction = Mage::helper('rewardpoints/action')->addTransaction('global_brand', $customer, new Varien_Object(array(
                                        'product_credit_title' => 0,
                                        'product_credit' => 0,
                                        'point_amount' => $linkshare['on_hold_points'],
                                        'title' => Mage::helper('manageapi')->__('Points awarded for global brand %s order %s on %s', $linkshare['advertiser_name'], $linkshare['order_id'], $linkshare['process_date']),
                                        'expiration_day' => 0,
                                        'expiration_day_credit' => 0,
                                        'is_on_hold' => 1,
                                        'created_time' => $linkshare['process_date'],
                                        'order_increment_id' => $linkshare['order_id']
                                    ))
                                );

                                if($transaction !== false && $transaction->getId())
                                {
                                    if(strcasecmp($linkshare['advertiser_name'], 'GiftCardMall.com') == 0  &&
                                        $enable_gcm &&
                                        strtotime($linkshare['process_date']) >= strtotime($date_gcm)
                                    ){
                                        $transaction->completeTransaction();
                                    }
                                }
                            }

                        }
                    }
                }
            } catch (Exception $ex) {

            }
        }
    }

    public function checkOnHold2($start_date = '2017-10-01')
    {
        $readConnection = Mage::getSingleton('core/resource')->getConnection('core_read');
        $writeConnection = Mage::getSingleton('core/resource')->getConnection('core_write');

        $table_name = Mage::getSingleton('core/resource')->getTableName('manageapi/linkshare');

//        $sql = "SELECT * FROM trlinkshare_actions WHERE member_id LIKE '=%' AND process_date >= '2017-10-01'";
//        $result = $readConnection->fetchAll($sql);
//        foreach ($result as $linkshare) {
//            $new_member_id = str_replace('=', '', $linkshare['member_id']);
//            $id = $linkshare['linkshare_id'];
//            $query = "UPDATE $table_name SET member_id = '$new_member_id' WHERE linkshare_id = $id";
//            zend_debug::dump($query);
//            $writeConnection->query($query);
//        }
//        exit;

        $sql = "SELECT * FROM trlinkshare_actions WHERE member_id LIKE '=%' AND process_date >= '2017-10-01'";
        // $sql = "SELECT linkshare_id, member_id as customer_id, advertiser_name, process_date, order_id, SUM(total_commission) as on_hold_points ";
        // $sql .= " FROM $table_name";
        // $sql .= " WHERE member_id LIKE '=%' AND process_date >= '$start_date'";
        // $sql .= " GROUP BY order_id";

        zend_debug::dump($sql);
        $count = 0;
        $result = $readConnection->fetchAll($sql);
        zend_debug::dump(sizeof($result));
        if(sizeof($result) > 0)
        {
            try{
                foreach ($result as $linkshare) {
                    $new_member_id = str_replace('=', '', $linkshare['customer_id']);
                    $id = $linkshare['linkshare_id'];
                    $query = "UPDATE $table_name SET member_id = '$new_member_id' WHERE linkshare_id = $id";
                    $writeConnection->query($query);
                    $count++;
                    // $customer = Mage::getModel('customer/customer')->load($new_member_id);
//                    zend_debug::dump($new_member_id);
//                    $count++;
                    // if($customer != null && $customer->getId()){
                    //     Mage::helper('rewardpoints/action')->addTransaction('global_brand', $customer, new Varien_Object(array(
                    //             'product_credit_title' => 0,
                    //             'product_credit' => 0,
                    //             'point_amount' => $linkshare['on_hold_points'],
                    //             'title' => Mage::helper('manageapi')->__('Points awarded for global brand %s order %s on %s', $linkshare['advertiser_name'], $linkshare['order_id'], $linkshare['process_date']),
                    //             'expiration_day' => 0,
                    //             'expiration_day_credit' => 0,
                    //             'is_on_hold' => 1,
                    //             'created_time' => $linkshare['process_date'],
                    //             'order_increment_id' => $linkshare['order_id']
                    //         ))
                    //     );
                    //     $count++;
                    // }
                }
            } catch (Exception $ex) {

            }
        }
        zend_debug::dump('LINKSHARE: '.$count);
    }
}
