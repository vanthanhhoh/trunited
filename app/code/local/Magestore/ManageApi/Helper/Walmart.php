<?php

class Magestore_ManageApi_Helper_Walmart extends Mage_Core_Helper_Abstract
{
    public function getHelperData()
    {
        return Mage::helper('manageapi');
    }

    public function processAPI($url, $format, $is_bestsellers = false, &$number)
    {

        if($number == Mage::helper('manageapi')->getDataConfig('number_pages_run', 'walmart_api')){
            return array();
        }

        $data = null;
        $is_xml = false;
        if (strcasecmp($format, 'xml') == 0) {
            $_data = $this->getHelperData()->getDataXML($url);
            $data = json_decode(json_encode((array)$_data), 1);
            $is_xml = true;
        } else if (strcasecmp($format, 'json') == 0) {
            $_data = $this->getHelperData()->getContentByCurl($url);
            $data = json_decode($_data, true);
        }

        // $count = 0;
        // $update = 0;
        $product_count = 0;
        $data_products = array();
        $manufacturers = array();

        if (sizeof($data) == 0) {
            // Mage::getSingleton('adminhtml/session')->addNotice(
            //     Mage::helper('manageapi')->__('No results from the API')
            // );
        } else {
            if (!$is_xml) {
                // json
                $results = $data['items'];
            } else {
                $results = $data['items']['item'];
            }

            $new = Mage::getSingleton('admin/session')->getNewRecords();
            $update = Mage::getSingleton('admin/session')->getUpdateRecords();

            $minimum_discount = Mage::helper('manageapi')->getDataConfig('minimum_discount', 'walmart_api');

            if ($results != null && is_array($results) && sizeof($results) > 0) {
                $transactionSave = Mage::getModel('core/resource_transaction');

                try {
                    foreach ($results as $_sale) {
                        if ($_sale['availableOnline']) {

                            if($minimum_discount == 0 || 
                                ($minimum_discount > 0 && isset($_sale['salePrice']) && isset($_sale['msrp']) && 
                                ((floatval($_sale['msrp'] - $_sale['salePrice']) / floatval($_sale['msrp'])) * 100) >= $minimum_discount)
                            ){
                                $sale = array();
                                foreach ($_sale as $k => $v) {
                                    $sale[$k] = is_array($v) ? json_encode($v) : $v;
                                }

                                $sale['specialOffer'] = isset($data['specialOffer']) ? $data['specialOffer'] : '';
                                $id = $this->checkModelId($sale['itemId']);

                                $sale['rollBack'] = $sale['rollBack'] === true ? 1 : 0;
                                $sale['bundle'] = $sale['bundle'] === true ? 1 : 0;
                                $sale['clearance'] = $sale['clearance'] === true ? 1 : 0;
                                $sale['preOrder'] = $sale['preOrder'] === true ? 1 : 0;
                                $sale['is_bestsellers'] = $is_bestsellers;

                                $model = Mage::getModel('manageapi/walmart');
                                $model->setData($sale);
                                $model->setId($id);

                                $transactionSave->addObject($model);

                                if ($id != null && $id > 0){
                                    $update++;
                                    Mage::getSingleton('admin/session')->setUpdateRecords($update);
                                }
                                else{
                                    $new++;
                                    Mage::getSingleton('admin/session')->setNewRecords($new);
                                }

                                if (!in_array($sale['brandName'], $manufacturers))
                                    $manufacturers[] = $sale['brandName'];

                                $data_products[] = $sale;
                            }
                        }
                    }

                    $transactionSave->save();

                    $product_count = $this->updateAttributeValue($manufacturers, false, $data_products);

                } catch (Exception $e) {
                    Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                }
            } else {
                Mage::getSingleton('adminhtml/session')->addError('Please wait few seconds before running API again.');
            }
        }

        if(isset($data['nextPage']) && $data['nextPage'] != null){
            $number++;
            $next_url = 'http://api.walmartlabs.com'.$data['nextPage'];
        
            if(!isset($data['specialOffer'])){
                Mage::getConfig()->saveConfig('manageapi/walmart_api/maxId', isset($data['maxId']) ? $data['maxId'] : '', 'default', 0);
                Mage::getConfig()->saveConfig('manageapi/walmart_api/nextPage', $next_url, 'default', 0);
            } else if(strcasecmp($data['specialOffer'], 'rollback') == 0){
                Mage::getConfig()->saveConfig('manageapi/walmart_api/maxId_rollback', isset($data['maxId']) ? $data['maxId'] : '', 'default', 0);
                Mage::getConfig()->saveConfig('manageapi/walmart_api/nextPage_rollback', $next_url, 'default', 0);
            } else if(strcasecmp($data['specialOffer'], 'clearance') == 0){
                Mage::getConfig()->saveConfig('manageapi/walmart_api/maxId_clearance', isset($data['maxId']) ? $data['maxId'] : '', 'default', 0);
                Mage::getConfig()->saveConfig('manageapi/walmart_api/nextPage_clearance', $next_url, 'default', 0);
            } else if(strcasecmp($data['specialOffer'], 'specialbuy') == 0){
                Mage::getConfig()->saveConfig('manageapi/walmart_api/maxId_specialbuy', isset($data['maxId']) ? $data['maxId'] : '', 'default', 0);
                Mage::getConfig()->saveConfig('manageapi/walmart_api/nextPage_specialbuy', $next_url, 'default', 0);
            }

            

            $this->processAPI($next_url, $data['format'], false, $number);
        }

        return array(
            'new' => $count,
            'update' => $update,
            // 'product' => $product_count,
        );
    }

    public function checkModelId($item_id)
    {
        $model = Mage::getModel('manageapi/walmart')->getCollection()
            ->addFieldToFilter('itemId', array('eq' => $item_id))
            ->getFirstItem();

        if ($model != null && $model->getId())
            return $model->getId();
        else
            return null;
    }

    public function updateAttributeValue($data, $is_create_product, $data_products = array())
    {
        $rs = array(
            'new' => 0,
            'update' => 0,
        );
        if (sizeof($data) > 0) {
            foreach ($data as $dt) {
                if ($dt != '') {
                    Mage::helper('manageapi')->addNewOptionToAttribute($dt['brandName']);
                }

            }
        }

        if ($is_create_product && is_array($data_products) && sizeof($data_products) > 0) {
            $rs = $this->createMultipleProducts($data_products);
        }

        return $rs;
    }

    public function massProduct($walmart_ids)
    {
        $collection = Mage::getModel('manageapi/walmart')->getCollection()
            ->addFieldToFilter('walmart_id', array('in' => $walmart_ids))
            ->setOrder('walmart_id', 'desc');

        $rs = array();

        if (sizeof($collection) > 0) {
            $data = array();
            foreach ($collection as $walmart) {
                $data[] = $walmart->getData();
            }

            if (sizeof($data) > 0) {
                $rs = $this->createMultipleProducts($data);
            }

        }

        return $rs;
    }

    public function createMultipleProducts($data)
    {
        $origin_param = Mage::helper('manageapi')->getDataConfig('origin_param','walmart_api');
        $replace_param = Mage::helper('manageapi')->getDataConfig('new_param','walmart_api');
        $new = 0;
        $update = 0;
        if (sizeof($data) > 0) {
            $transactionSave = Mage::getModel('core/resource_transaction');
            try {
                foreach ($data as $dt) {
                    if ($dt['itemId'] != '') {
                        $product_id = $this->checkProductId($dt['name']);
                        $product = Mage::getModel('catalog/product');

                        $random_str = $this->generateRandomString();

                        $url = $dt['productTrackingUrl'] . '&u1={{customer_id}}';

                        $url = str_replace($origin_param, $replace_param, $url);

                        $_dt = array(
                            'website_ids' => array(1),
                            'attribute_set_id' => 4,
                            'type_id' => 'dailyproduct',
                            'name' => $dt['name'],
                            'short_description' => $dt['shortDescription'],
                            'description' => $dt['longDescription'],
                            'shop_now' => $url,
                            'external_image_url' => $dt['largeImage'],
                            'sku' => 'daily-deals-' . $random_str . '-' . date('Ymd', time()),
                            'url_key' => strtolower('daily-deals-' . $random_str . '-' . date('Ymd', time())),
                            'status' => 1,
                            'visibility' => Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH,
                            'manufacturer' => $this->getAttributeId($dt['brandName']),
                            'primary_category' => $this->processCategoryFromPath($dt['categoryPath']),
                            'is_bestsellers' => $dt['is_bestsellers'] ? $dt['is_bestsellers'] : 0,
                            'primary_promotion_type' => '',
                            'page_layout' => 'one_column',
                            'category_ids' => array(71, 72),
                            'created_from' => 1,
                            'daily_deals_msrp' => $dt['msrp'],
                            'daily_deals_sale_price' => $dt['salePrice'],
                            'specialOffer' => $this->getSpecialOffer($dt),
                        );

                        $product->setData($_dt);
                        $product->setId($product_id);
                        $product->save();

                        $this->updateRecord($dt['itemId'], $product);

                        if ($product_id > 0)
                            $update++;
                        else
                            $new++;
                    }
//                    $transactionSave->addObject($product);
                }

                $transactionSave->save();
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
        }

        return array(
            'new' => $new,
            'update' => $update
        );
    }

    public function getSpecialOffer($data)
    {
        if(isset($data['rollBack']) && $data['rollBack'] == 1)
            return Mage::helper('manageapi')->__('rollBack');
        else if(isset($data['clearance']) && $data['clearance'] == 1)
            return Mage::helper('manageapi')->__('clearance');
        else if(isset($data['specialBuy']) && $data['specialBuy'] == 1)
            return Mage::helper('manageapi')->__('specialBuy');
        else
            return '';
    }


    public function processCategoryFromPath($path)
    {
        $result = str_replace('Home/', '', $path);
        if(strpos($result, '/') > 0){
            $_result = explode('/', $result);
            $result = $_result[0];
        }
        return $result;
    }

    public function updateRecord($id, $product)
    {
        $model = Mage::getModel('manageapi/walmart')->load($id, 'itemId');

        if ($model != null && $model->getId()) {
            $model->setData('product_created_at', now());
            $model->setData('product_id', $product->getId());
            $model->setData('updated_at', now());
            $model->save();
        }
    }

    function generateRandomString($length = 5)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public function getAttributeId($attribute_name)
    {
        $options = Mage::helper('manageapi')->getOptionAttribute();
        $id = null;

        if ($options == null) {
            Mage::helper('manageapi')->addNewOptionToAttribute($attribute_name);
        }

        if (sizeof($options) > 0) {
            foreach ($options as $option) {
                if (strcasecmp($option['label'], $attribute_name) == 0) {
                    $id = $option['value'];
                    break;
                }
            }

            if ($id == null) {
                Mage::helper('manageapi')->addNewOptionToAttribute($attribute_name);
            }
        }

        if ($id == null) {
            $options = Mage::helper('manageapi')->getOptionAttribute();
            foreach ($options as $option) {
                if (strcasecmp($option['label'], $attribute_name) == 0) {
                    $id = $option['value'];
                    break;
                }
            }
        }

        return $id;
    }

    public function checkProductId($name)
    {
        $model = Mage::getModel('catalog/product')->getCollection()
            ->addAttributeToFilter('name', array('eq' => $name))
            ->getFirstItem();

        if ($model != null && $model->getId())
            return $model->getId();
        else
            return null;
    }
}
