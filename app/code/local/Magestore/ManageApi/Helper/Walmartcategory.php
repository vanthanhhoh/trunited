<?php

class Magestore_ManageApi_Helper_Walmartcategory extends Mage_Core_Helper_Abstract
{
    public function getHelperData()
    {
        return Mage::helper('manageapi');
    }

    public function processAPI($url, $format = 'json')
    {
        $data = null;
        $is_xml = false;
        if (strcasecmp($format, 'xml') == 0) {
            $_data = $this->getHelperData()->getDataXML($url);
            $data = json_decode(json_encode((array)$_data), 1);
            $is_xml = true;
        } else if (strcasecmp($format, 'json') == 0) {
            $_data = $this->getHelperData()->getContentByCurl($url);
            $data = json_decode($_data, true);
        }

        $count = 0;

        if (sizeof($data) == 0) {
            Mage::getSingleton('adminhtml/session')->addNotice(
                Mage::helper('manageapi')->__('No results from the API')
            );
        } else {
            $results = $data['categories'];

            if ($results != null && is_array($results) && sizeof($results) > 0) {
                $transactionSave = Mage::getModel('core/resource_transaction');

                try {
                    foreach ($results as $sale) {
                        $this->recursive($sale, $transactionSave, $count);
                    }

                    $transactionSave->save();

                } catch (Exception $e) {
                    Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                }
            } else {
                Mage::getSingleton('adminhtml/session')->addError('Please wait few seconds before running API again.');
            }
        }

        return array(
            'new' => $count,
        );
    }

    public function recursive($item, &$transactionSave, &$count)
    {
        $id = $this->checkModelId($item['id']);
        $model = Mage::getModel('manageapi/walmartcategory');

        $ids = explode('_', $item['id']);
        $ids_count = sizeof($ids);
        $item['level'] = $ids_count;

        if ($ids_count <= 1) {
            $item['parent_id'] = '';
        } else if ($ids_count > 1) {
            $item['parent_id'] = $ids[0];
            if(isset($ids[1]) && $ids[1] != null && sizeof($ids) >= 3)
                $item['parent_id'] .= '_'.$ids[1];
        }

        $model->setData($item);
        $model->setId($id);

        $transactionSave->addObject($model);

        $count++;

        if (isset($item['children']) && sizeof($item['children']) > 0) {
            foreach ($item['children'] as $child) {
                $this->recursive($child, $transactionSave, $count);
            }
        }
    }

    public function checkModelId($item_id)
    {
        $model = Mage::getModel('manageapi/walmartcategory')->getCollection()
            ->addFieldToFilter('id', array('eq' => $item_id))
            ->getFirstItem();

        if ($model != null && $model->getId())
            return $model->getId();
        else
            return null;
    }
}
