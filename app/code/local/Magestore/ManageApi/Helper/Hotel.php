<?php

class Magestore_ManageApi_Helper_Hotel extends Mage_Core_Helper_Abstract
{
    public function getHelperData()
    {
        return Mage::helper('manageapi');
    }

    public function processAPI($url, $start_date, $is_check_on_hold = false)
    {
        $data = null;
        $is_xml = false;
        if (strpos($url, 'format=xml') > 0) {
            $_data = $this->getHelperData()->getDataXML($url);
            $data = json_decode(json_encode((array)$_data), 1);
            $is_xml = true;
        } else if (strpos($url, 'format=json') > 0) {
            $_data = $this->getHelperData()->getContentByCurl($url);
            $dt = json_decode($_data, true);
            $data = $dt['getSharedTRK.Sales.Select.Hotel'];
        } else
            return;

        if ($data != null && is_array($data) && sizeof($data) > 0 && isset($data['results']) && sizeof($data['results']) > 0) {
            $transactionSave = Mage::getModel('core/resource_transaction');
            try {
                $other_data = array();
                foreach ($data['results'] as $k => $v) {
                    if ($k != 'sales_data') {
                        if (is_array($v))
                            $other_data[$k] = sizeof($v) > 0 ? json_encode($v) : '';
                        else
                            $other_data[$k] = $v;
                    }
                }

                if (isset($data['results']['sales_data']) && sizeof($data['results']['sales_data']) > 0) {
                    if ($is_xml) {
                        $_result = $data['results']['sales_data']['sale'];

                        if (is_array($_result[0])) {
                            $rs = $_result;
                        } else {
                            $rs = $data['results']['sales_data'];
                        }

                        foreach ($rs as $sale) {
                            $model = Mage::getModel('manageapi/hotelactions');
                            foreach ($sale as $k => $v) {
                                if (is_array($v))
                                    $_dt[$k] = sizeof($v) > 0 ? json_encode($v) : '';
                                else
                                    $_dt[$k] = $v;
                            }
                            $_dt['other'] = json_encode($other_data);
                            $_dt['created_time'] = now();

                            $model->setData($_dt);
                            $model->setId($this->getModelId($_dt['tripid']));

                            $transactionSave->addObject($model);
                        }
                        Mage::log('HOTEL API at ' . date('Y-m-d H:i:s', time()) . ' - Result:' . sizeof($data['results']['sales_data']['sale']) . ' - URL: ' . $url, null, 'check_manage_api.log');
                    } else if (!$is_xml) {
                        foreach ($data['results']['sales_data'] as $sale) {
                            $model = Mage::getModel('manageapi/hotelactions');
                            foreach ($sale as $k => $v) {
                                if (is_array($v))
                                    $_dt[$k] = sizeof($v) > 0 ? json_encode($v) : '';
                                else
                                    $_dt[$k] = $v;
                            }
                            $_dt['other'] = json_encode($other_data);
                            $_dt['created_time'] = now();

                            $model->setData($_dt);
                            $model->setId($this->getModelId($_dt['tripid']));

                            $transactionSave->addObject($model);
                        }
                        Mage::log('HOTEL API at ' . date('Y-m-d H:i:s', time()) . ' - Result:' . sizeof($data['results']['sales_data']) . ' - URL: ' . $url, null, 'check_manage_api.log');
                    }
                } else {
                    $model = Mage::getModel('manageapi/hotelactions');
                    $_dt = array(
                        'other' => json_encode($other_data),
                        'created_time' => now(),
                    );
                    $model->setData($_dt);
                    $transactionSave->addObject($model);
                    Mage::log('HOTEL API at ' . date('Y-m-d H:i:s', time()) . ' - Result: 0 - URL: ' . $url, null, 'check_manage_api.log');
                }

                $transactionSave->save();
            } catch (Exception $e) {
                zend_debug::dump($e);
                exit;
            }
        } else {
            $error_message = '';
            if ($is_xml) {
                $errors = json_decode(json_encode((array)$_data), 1);
                $error_message .= $errors['error']['status'];
                $flag_error = true;
            } else {
                $errors = json_decode($_data, true);
                $error_message .= $errors['getSharedTRK.Sales.Select.Hotel']['error']['status'];
                $flag_error = true;
            }
            if ($flag_error)
                Mage::getSingleton('adminhtml/session')->addError('PRICE LINE HOTEL API: ' . $error_message);
        }

        if($is_check_on_hold)
            $this->checkOnHold();
    }

    public function processCron()
    {
        $enable = $this->getHelperData()->getDataConfig('enable_hotel', 'price_line');
        if ($enable) {
            $url = $this->getHelperData()->getDataConfig('hotel_api', 'price_line');
            if ($url != null) {
                $format = $this->getHelperData()->getDataConfig('hotel_format', 'price_line');
                $days = $this->getHelperData()->getDataConfig('hotel_days', 'price_line');
                $_days = $days != null ? $days : 1;

                $end_30 = strtotime('' . $_days . ' days ago', time());
                $start_30 = strtotime('29 days ago', $end_30);
                $start_date_30 = date('Y-m-d_00:00:00', $start_30);
                $end_date_30 = date('Y-m-d_23:59:00', $end_30);
                $_url_30 = str_replace(
                    array('{{start_date}}', '{{end_date}}', '{{format}}'),
                    array($start_date_30, $end_date_30, $format),
                    $url
                );
                $this->processAPI($_url_30, $start_date_30);

                $end_60 = strtotime('1 days ago', $start_30);
                $start_60 = strtotime('29 days ago', $end_60);
                $start_date_60 = date('Y-m-d_00:00:00', $start_60);
                $end_date_60 = date('Y-m-d_23:59:00', $end_60);
                $_url_60 = str_replace(
                    array('{{start_date}}', '{{end_date}}', '{{format}}'),
                    array($start_date_60, $end_date_60, $format),
                    $url
                );
                $this->processAPI($_url_60, $start_date_60);


                $end_90 = strtotime('1 days ago', $start_60);
                $start_90 = strtotime('29 days ago', $end_90);
                $start_date_90 = date('Y-m-d_00:00:00', $start_90);
                $end_date_90 = date('Y-m-d_23:59:00', $end_90);
                $_url_90 = str_replace(
                    array('{{start_date}}', '{{end_date}}', '{{format}}'),
                    array($start_date_90, $end_date_90, $format),
                    $url
                );
                $this->processAPI($_url_90, $start_date_90, true);
            }
        }
    }

    public function getModelId($trip_id)
    {
        $model = Mage::getModel('manageapi/hotelactions')
            ->getCollection()
            ->addFieldToFilter('tripid', array('eq' => $trip_id))
            ->getFirstItem();

        if ($model != null && $model->getId())
            return $model->getId();
        else
            return null;
    }

    public function checkOnHold()
    {
        $helper = Mage::helper('manageapi');
        $collection = Mage::getModel('manageapi/hotelactions')
            ->getCollection()
            ->addFieldToFilter('status', array('eq' => 'Active'))
            ->addFieldToFilter('check_out_date_time', array('lt' => date('Y-m-d H:i:s', time())))
            ->addFieldToFilter('revenue', array('gt' => 0))
            ->addFieldToFilter('on_hold_at', array('null' => true))
            ->setOrder('hotel_actions_id', 'desc');

        if (sizeof($collection) > 0) {
            try {
                $transactionSave = Mage::getModel('core/resource_transaction');

                $enable_fundraiser = $helper->getDataConfig('enable_fundraiser', 'affiliate_points');
                $enable_daily_deals = $helper->getDataConfig('enable_daily_deals', 'affiliate_points');
                $suffix_fundraiser = $helper->getDataConfig('suffix_fundraiser', 'affiliate_points');
                $suffix_daily_deals = $helper->getDataConfig('suffix_daily_deals', 'affiliate_points');
                $day_hold_points = $helper->getDataConfig('day_hold_points', 'affiliate_points');
                $day_hold_credits = Mage::helper('affiliateearnings')->getConfigData('general', 'day_hold_credit');
                $percent_on_hold_credit = Mage::helper('affiliateearnings')->getConfigData('general', 'percent_on_hold_credit') != '' ? Mage::helper('affiliateearnings')->getConfigData('general', 'percent_on_hold_credit') : 1;

                $enable_complete = Mage::helper('manageapi')->getDataConfig('enable', 'price_line');
                
                foreach ($collection as $car) {

                    $refclickid = $car->getData('refclickid');

                    if ($helper->checkID($refclickid, $suffix_fundraiser) > 0 && $enable_fundraiser) {
                        $customer_id = $helper->getOriginId($refclickid, $suffix_fundraiser);
                        $customer = Mage::getModel('customer/customer')->load($customer_id);
                        if ($customer != null && $customer->getId()) {
                            $affiliatepointsAccount = Mage::helper('affiliatepoints/account')
                                ->loadByCustomerId($customer->getId());

                            if ($affiliatepointsAccount != null && $affiliatepointsAccount->getId()) {
                                $expiration_on = Mage::helper('affiliatepoints')->addDaysToDate(
                                    date('Y-m-d H:i:s', strtotime($car->getData('check_out_date_time'))),
                                    $day_hold_points
                                );
                                $params = array(
                                    'title' => Mage::helper('manageapi')->__('Points awarded for Priceline Booking: %s on %s', $car->getData('hotel_name'), date('Y-m-d H:i:s', strtotime($car->getData('check_out_date_time')))),
                                    'award_from' => Magestore_Affiliatepoints_Model_Status::AWARD_FROM_FUNDRAISER,
                                    'current_point' => $affiliatepointsAccount->getAffiliatePoints(),
                                    'is_on_hold' => 1,
                                    'hold_point' => $car->getData('revenue'),
                                    'expiration_at' => $expiration_on,
                                    'created_at' => date('Y-m-d H:i:s', strtotime($car->getData('check_out_date_time'))),
                                    'order_id' => $car->getData('tripid')
                                );
                                Mage::helper('affiliatepoints/transaction')->createTransaction(
                                    $affiliatepointsAccount,
                                    $params,
                                    Magestore_Affiliatepoints_Model_Type::TYPE_TRANSACTION_RECEIVE_FROM_GLOBAL_BRAND,
                                    Magestore_Affiliatepoints_Model_Status::STATUS_TRANSACTION_ON_HOLD
                                );
                                $car->setData('on_hold_at', now());
                                $transactionSave->addObject($car);
                            }
                        }
                    } else if ($helper->checkID($refclickid, $suffix_daily_deals) > 0 && $enable_daily_deals) {
                        $customer_id = $helper->getOriginId($refclickid, $suffix_daily_deals);
                        $customer = Mage::getModel('customer/customer')->load($customer_id);
                        if ($customer != null && $customer->getId()) {
                            $affiliateearningsAccount = Mage::helper('affiliateearnings/account')
                                ->loadByCustomerId($customer->getId());

                            if ($affiliateearningsAccount != null && $affiliateearningsAccount->getId()) {
                                $expiration_on = Mage::helper('affiliatepoints')->addDaysToDate(
                                    date('Y-m-d H:i:s', strtotime($car->getData('check_out_date_time'))),
                                    $day_hold_credits
                                );
                                
                                $params = array(
                                    'title' => Mage::helper('manageapi')->__('Guest Commission awarded for Priceline Booking: %s on %s', $car->getData('hotel_name'), date('Y-m-d H:i:s', strtotime($car->getData('check_out_date_time')))),
                                    'award_from' => Magestore_Affiliateearnings_Model_Status::AWARD_FROM_DAILY_DEALS,
                                    'current_credit' => $affiliateearningsAccount->getGuestCommission(),
                                    'is_on_hold' => 1,
                                    'hold_credit' => $car->getData('revenue') * $percent_on_hold_credit * 0.01,
                                    'expiration_at' => $expiration_on,
                                    'created_at' => date('Y-m-d H:i:s', strtotime($car->getData('check_out_date_time'))),
                                    'order_id' => $car->getData('tripid')
                                );
                                Mage::helper('affiliatepoints/transaction')->createTransaction(
                                    $affiliateearningsAccount,
                                    $params,
                                    Magestore_Affiliateearnings_Model_Type::TYPE_TRANSACTION_RECEIVE_FROM_GLOBAL_BRAND,
                                    Magestore_Affiliateearnings_Model_Status::STATUS_TRANSACTION_ON_HOLD
                                );
                                $car->setData('on_hold_at', now());
                                $transactionSave->addObject($car);
                            }
                        }
                    } else {
                        $customer = Mage::getModel('customer/customer')->load($car->getData('refclickid'));
                        if ($customer != null && $customer->getId()) {

                            $exist_transaction = Mage::getModel('rewardpoints/transaction')
                                ->getCollection()
                                ->addFieldToFilter('is_on_hold', 1)
                                ->addFieldToFilter('order_increment_id', array('eq' => $car->getData('tripid')))
                                ->setOrder('transaction_id', 'DESC')
                                ->getFirstItem()
                            ;

                            if($exist_transaction != null && $exist_transaction->getId()){
                                $exist_transaction->setData('updated_time', now());
                                $exist_transaction->setData('on_hold_at', now());
                                $exist_transaction->setData('hold_point', $car->getData('revenue'));
                                $exist_transaction->save();

                                if($enable_complete){

                                    if($car->getData('revenue') > $exist_transaction->getData('hold_point')){
                                        $update_points = $car->getData('revenue') - $exist_transaction->getData('hold_point');
                                        $rewardAccount = Mage::getModel('rewardpoints/customer')->load(
                                            $exist_transaction->getCustomerId(),
                                            'customer_id'
                                        );

                                        if($rewardAccount != null && $rewardAccount->getId()){
                                            $rewardAccount->setPointBalance(
                                                $rewardAccount->getPointBalance() + $update_points
                                            );

                                            $rewardAccount->save();
                                        }

                                    } else if($car->getData('revenue') < $exist_transaction->getData('hold_point')) {
                                        $update_points = $exist_transaction->getData('hold_point') - $car->getData('revenue');
                                        $rewardAccount = Mage::getModel('rewardpoints/customer')->load(
                                            $exist_transaction->getCustomerId(),
                                            'customer_id'
                                        );

                                        if($rewardAccount != null && $rewardAccount->getId()){
                                            $new_points = $rewardAccount->getPointBalance() - $update_points;
                                            $rewardAccount->setPointBalance(
                                                $new_points
                                            );

                                            $rewardAccount->save();
                                        }
                                    }
                                }
                            } else {
                                $transaction = Mage::helper('rewardpoints/action')->addTransaction('global_brand', $customer, new Varien_Object(array(
                                        'product_credit_title' => 0,
                                        'product_credit' => 0,
                                        'point_amount' => $car->getData('revenue'),
                                        'title' => Mage::helper('manageapi')->__('Points awarded for Priceline Booking: %s on %s', $car->getData('hotel_name'), date('Y-m-d H:i:s', strtotime($car->getData('check_out_date_time')))),
                                        'expiration_day' => 0,
                                        'expiration_day_credit' => 0,
                                        'is_on_hold' => 1,
                                        'created_time' => date('Y-m-d H:i:s', strtotime($car->getData('check_out_date_time'))),
                                        'order_increment_id' => $car->getData('tripid')
                                    ))
                                );

                                if($transaction !== false && $transaction->getId())
                                {
                                    if($enable_complete){
                                        $transaction->completeTransaction();
                                    }
                                }

                                $car->setData('on_hold_at', now());
                                $transactionSave->addObject($car);
                            }
                        }
                    }
                }

                $transactionSave->save();
            } catch (Exception $ex) {

            }
        }
    }
}
