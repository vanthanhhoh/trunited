<?php

class Magestore_ManageApi_Helper_Cj extends Mage_Core_Helper_Abstract
{
    public function getHelperData()
    {
        return Mage::helper('manageapi');
    }

    public function processAPI($url, $start_date)
    {
        $data = null;
        $cj_developer_key = $this->getHelperData()->getDataConfig('cj_developer_key', 'cj');
        if ($cj_developer_key == null)
            return;

        $params_header = array(
            'authorization: ' . $cj_developer_key,
        );

        $_data = $this->getHelperData()->getDataXML($url, $params_header);
        $data = json_decode(json_encode((array)$_data), 1);

        if ($data != null && is_array($data) && sizeof($data) > 0 && isset($data['commissions']['commission'])
            && is_array($data['commissions']['commission']) && sizeof($data['commissions']['commission']) > 0) {

            $transactionSave = Mage::getModel('core/resource_transaction');
            $connection = Mage::getSingleton('core/resource')->getConnection('core_write');

            try {
                $connection->beginTransaction();
                foreach ($data['commissions']['commission'] as $v) {
                    $model = Mage::getModel('manageapi/cjactions');
                    $_dt = array();
                    foreach ($v as $k => $_v) {
                        $_dt[str_replace('-', '_', $k)] = is_array($_v) ? json_encode($_v) : $_v;
                    }

                    $model->setData($_dt);
                    $model->setData('created_time', now());
                    $transactionSave->addObject($model);
                }
                Mage::log('CJ API at ' . date('Y-m-d H:i:s', time()) . ' - Result:' . sizeof($data['commissions']['commission']) . ' - URL: ' . $url, null, 'check_manage_api.log');

                $transactionSave->save();
                $connection->commit();
            } catch (Exception $e) {
                $connection->rollback();
            }
        } else {
            if (isset($data['error-message']))
                Mage::getSingleton('adminhtml/session')->addError('CJ API: ' . $data['error-message']);
        }

        $this->checkOnHold($start_date);
    }

    public function processCron()
    {
        $enable = $this->getHelperData()->getDataConfig('enable_cj', 'cj');
        if ($enable) {
            $url = $this->getHelperData()->getDataConfig('cj_api', 'cj');
            if ($url != null) {
                $data_type = $this->getHelperData()->getDataConfig('cj_data_type', 'cj');
                $start_date = date('Y-m-d', strtotime('-1 day', time()));
                $end_date = date('Y-m-d', time());
                $_url = str_replace(array('{{start_date}}', '{{end_date}}', '{{data_type}}'), array($start_date, $end_date, $data_type), $url);
                $this->processAPI($_url, $start_date);
            }
        }
    }

    public function processCron2($start_date, $end_date)
    {
        $enable = $this->getHelperData()->getDataConfig('enable_cj', 'cj');
        if ($enable) {
            $url = $this->getHelperData()->getDataConfig('cj_api', 'cj');
            if ($url != null) {
                $data_type = $this->getHelperData()->getDataConfig('cj_data_type', 'cj');
                
                $_url = str_replace(
                    array('{{start_date}}', '{{end_date}}', '{{data_type}}'), 
                    array($start_date, $end_date, $data_type), 
                    $url
                );
                
                $this->processAPI($_url, $start_date);
            }
        }
    }

    public function checkOnHold($start_date)
    {
//        $rewardpoints_transactions = Mage::getModel('rewardpoints/transaction')
//            ->getCollection()
//            ->addFieldToSelect('order_increment_id')
//            ->addFieldToFilter('order_increment_id', array('notnull' => true))
//            ->setOrder('transaction_id', 'desc')
//        ;
//        $order_ids = $rewardpoints_transactions->getColumnValues('order_increment_id');
        $helper = Mage::helper('manageapi');
        $readConnection = Mage::getSingleton('core/resource')->getConnection('core_read');

        $table_name = Mage::getSingleton('core/resource')->getTableName('manageapi/cjactions');
        $sql = "SELECT order_id, sid as customer_id, SUM(commission_amount) as on_hold_points, posting_date, advertiser_name as global_brand";
        $sql .= " FROM $table_name";
        $sql .= " WHERE posting_date >= '$start_date' and commission_amount > 0";
        $sql .= " GROUP BY order_id";
        $sql .= " ORDER BY order_id";

        $result = $readConnection->fetchAll($sql);
        if (sizeof($result) > 0) {
            try {
                $enable_fundraiser = $helper->getDataConfig('enable_fundraiser', 'affiliate_points');
                $enable_daily_deals = $helper->getDataConfig('enable_daily_deals', 'affiliate_points');
                $suffix_fundraiser = $helper->getDataConfig('suffix_fundraiser', 'affiliate_points');
                $suffix_daily_deals = $helper->getDataConfig('suffix_daily_deals', 'affiliate_points');
                $day_hold_points = $helper->getDataConfig('day_hold_points', 'affiliate_points');
                $day_hold_credits = Mage::helper('affiliateearnings')->getConfigData('general', 'day_hold_credit');
                $percent_on_hold_credit = Mage::helper('affiliateearnings')->getConfigData('general', 'percent_on_hold_credit') != '' ? Mage::helper('affiliateearnings')->getConfigData('general', 'percent_on_hold_credit') : 1;

                $enable_gcm = Mage::getStoreConfig('rewardpoints/general/enable_giftcardmall');
                $date_gcm = Mage::getStoreConfig('rewardpoints/general/day_giftcardmall');

                foreach ($result as $cj) {
                    $refclickid = $cj['customer_id'];

                    if ($helper->checkID($refclickid, $suffix_fundraiser) > 0 && $enable_fundraiser) {
                        $customer_id = $helper->getOriginId($refclickid, $suffix_fundraiser);
                        $customer = Mage::getModel('customer/customer')->load($customer_id);
                        if ($customer != null && $customer->getId()) {
                            $affiliatepointsAccount = Mage::helper('affiliatepoints/account')
                                ->loadByCustomerId($customer->getId());

                            if ($affiliatepointsAccount != null && $affiliatepointsAccount->getId()) {
                                $expiration_on = Mage::helper('affiliatepoints')->addDaysToDate(
                                    $cj['posting_date'],
                                    $day_hold_points
                                );
                                $params = array(
                                    'title' => Mage::helper('manageapi')->__('Points awarded for global brand %s order %s on %s', $cj['global_brand'], $cj['order_id'], $cj['posting_date']),
                                    'award_from' => Magestore_Affiliatepoints_Model_Status::AWARD_FROM_FUNDRAISER,
                                    'current_point' => $affiliatepointsAccount->getAffiliatePoints(),
                                    'is_on_hold' => 1,
                                    'hold_point' => $cj['on_hold_points'],
                                    'expiration_at' => $expiration_on,
                                    'created_at' => $cj['posting_date'],
                                    'order_id' => $cj['order_id']
                                );
                                Mage::helper('affiliatepoints/transaction')->createTransaction(
                                    $affiliatepointsAccount,
                                    $params,
                                    Magestore_Affiliatepoints_Model_Type::TYPE_TRANSACTION_RECEIVE_FROM_GLOBAL_BRAND,
                                    Magestore_Affiliatepoints_Model_Status::STATUS_TRANSACTION_ON_HOLD
                                );
                            }
                        }
                    } else if ($helper->checkID($refclickid, $suffix_daily_deals) > 0 && $enable_daily_deals) {
                        $customer_id = $helper->getOriginId($refclickid, $suffix_daily_deals);
                        $customer = Mage::getModel('customer/customer')->load($customer_id);
                        if ($customer != null && $customer->getId()) {
                            $affiliateearningsAccount = Mage::helper('affiliateearnings/account')
                                ->loadByCustomerId($customer->getId());

                            if ($affiliateearningsAccount != null && $affiliateearningsAccount->getId()) {
                                $expiration_on = Mage::helper('affiliateearnings')->addDaysToDate(
                                    $cj['posting_date'],
                                    $day_hold_credits
                                );
                                $params = array(
                                    'title' => Mage::helper('manageapi')->__('Guest Commission awarded for global brand %s order %s on %s', $cj['global_brand'], $cj['order_id'], $cj['posting_date']),
                                    'award_from' => Magestore_Affiliateearnings_Model_Status::AWARD_FROM_DAILY_DEALS,
                                    'current_credit' => $affiliateearningsAccount->getGuestCommission(),
                                    'is_on_hold' => 1,
                                    'hold_credit' => $cj['on_hold_points'] * $percent_on_hold_credit * 0.01,
                                    'expiration_at' => $expiration_on,
                                    'created_at' => $cj['posting_date'],
                                    'order_id' => $cj['order_id']
                                );
                                Mage::helper('affiliateearnings/transaction')->createTransaction(
                                    $affiliateearningsAccount,
                                    $params,
                                    Magestore_Affiliateearnings_Model_Type::TYPE_TRANSACTION_RECEIVE_FROM_GLOBAL_BRAND,
                                    Magestore_Affiliateearnings_Model_Status::STATUS_TRANSACTION_ON_HOLD
                                );
                            }
                        }
                    } else {
                        $customer = Mage::getModel('customer/customer')->load($cj['customer_id']);
                        if ($customer != null && $customer->getId()) {
                            $exist_transaction = Mage::getModel('rewardpoints/transaction')
                                ->getCollection()
                                ->addFieldToFilter('is_on_hold', 1)
                                ->addFieldToFilter('order_increment_id', array('eq' => $cj['order_id']))
                                ->setOrder('transaction_id', 'DESC')
                                ->getFirstItem()
                            ;

                            if($exist_transaction != null && $exist_transaction->getId()){
                                $exist_transaction->setData('updated_time', now());
                                $exist_transaction->setData('on_hold_at', now());
                                $exist_transaction->setData('hold_point', $cj['on_hold_points']);
                                $exist_transaction->save();

                                if(strcasecmp($cj['global_brand'], 'GiftCardMall.com') == 0 &&
                                    $enable_gcm &&
                                    strtotime($cj['posting_date']) >= strtotime($date_gcm)
                                ){

                                    if($cj['on_hold_points'] > $exist_transaction->getData('hold_point')){
                                        $update_points = $cj['on_hold_points'] - $exist_transaction->getData('hold_point');
                                        $rewardAccount = Mage::getModel('rewardpoints/customer')->load(
                                            $exist_transaction->getCustomerId(),
                                            'customer_id'
                                        );

                                        if($rewardAccount != null && $rewardAccount->getId()){
                                            $rewardAccount->setPointBalance(
                                                $rewardAccount->getPointBalance() + $update_points
                                            );

                                            $rewardAccount->save();
                                        }

                                    } else if($cj['on_hold_points'] < $exist_transaction->getData('hold_point')) {
                                        $update_points = $exist_transaction->getData('hold_point') - $cj['on_hold_points'];
                                        $rewardAccount = Mage::getModel('rewardpoints/customer')->load(
                                            $exist_transaction->getCustomerId(),
                                            'customer_id'
                                        );

                                        if($rewardAccount != null && $rewardAccount->getId()){
                                            $new_points = $rewardAccount->getPointBalance() - $update_points;
                                            $rewardAccount->setPointBalance(
                                                $new_points
                                            );

                                            $rewardAccount->save();
                                        }
                                    }
                                }
                            } else {
                                $transaction = Mage::helper('rewardpoints/action')->addTransaction('global_brand', $customer, new Varien_Object(array(
                                        'product_credit_title' => 0,
                                        'product_credit' => 0,
                                        'point_amount' => $cj['on_hold_points'],
                                        'title' => Mage::helper('manageapi')->__('Points awarded for global brand %s order %s on %s', $cj['global_brand'], $cj['order_id'], $cj['posting_date']),
                                        'expiration_day' => 0,
                                        'expiration_day_credit' => 0,
                                        'is_on_hold' => 1,
                                        'created_time' => $cj['posting_date'],
                                        'order_increment_id' => $cj['order_id']
                                    ))
                                );

                                if($transaction !== false && $transaction->getId())
                                {

                                    if(strcasecmp($cj['global_brand'], 'GiftCardMall.com') == 0 &&
                                        $enable_gcm &&
                                        strtotime($cj['posting_date']) >= strtotime($date_gcm)
                                    ){
                                        $transaction->completeTransaction();
                                    }
                                }
                            }

                        }
                    }
                }
            } catch (Exception $ex) {

            }
        }
    }

    public function checkOnHold2($start_date)
    {
        $readConnection = Mage::getSingleton('core/resource')->getConnection('core_read');

        $table_name = Mage::getSingleton('core/resource')->getTableName('manageapi/cjactions');
        $sql = "SELECT order_id, sid as customer_id, SUM(commission_amount) as on_hold_points, posting_date, advertiser_name as global_brand";
        $sql .= " FROM $table_name";
        $sql .= " WHERE posting_date >= '$start_date' and commission_amount BETWEEN 0.1 and 0.999999999";
        $sql .= " GROUP BY order_id";
        $sql .= " ORDER BY order_id";
        zend_debug::dump($sql);
        $result = $readConnection->fetchAll($sql);
        $count = 0;
        zend_debug::dump(sizeof($result));
        if (sizeof($result) > 0) {
            try {
                foreach ($result as $cj) {
                    $customer = Mage::getModel('customer/customer')->load($cj['customer_id']);
                    if ($customer != null && $customer->getId()) {
                        Mage::helper('rewardpoints/action')->addTransaction('global_brand', $customer, new Varien_Object(array(
                                'product_credit_title' => 0,
                                'product_credit' => 0,
                                'point_amount' => $cj['on_hold_points'],
                                'title' => Mage::helper('manageapi')->__('Points awarded for global brand %s order %s on %s', $cj['global_brand'], $cj['order_id'], $cj['posting_date']),
                                'expiration_day' => 0,
                                'expiration_day_credit' => 0,
                                'is_on_hold' => 1,
                                'created_time' => $cj['posting_date'],
                                'order_increment_id' => $cj['order_id']
                            ))
                        );
                        $count++;
                    }
                }
            } catch (Exception $ex) {

            }
        }
        zend_debug::dump('CJ: ' . $count);
    }
}
