<?php

class Magestore_ManageApi_Helper_Data extends Mage_Core_Helper_Abstract
{
    const LINK_SHARE_FILE = 'manage_api/linkshare/linkshare.csv';
    const PRICE_LINE_HOTEL = 'manage_api/priceline/hotel.xml';
    const PRICE_LINE_FIGHT = 'manage_api/priceline/fight.xml';
    const PRICE_LINE_CAR = 'manage_api/priceline/car.xml';
    const PRICE_LINE_VACATION = 'manage_api/priceline/vacation.xml';
    const CJ_VACATION = 'manage_api/cj/cj.xml';

    public function getDataConfig($file_name, $group = 'general')
    {
        return Mage::getStoreConfig(
            'manageapi/' . $group . '/' . $file_name,
            Mage::app()->getStore()
        );
    }

    /**
     * @param $url
     * @param array $param_header
     * @param bool $is_post
     * @param array $param_fields
     * @param null $encoding
     * @return mixed|null
     */
    public function getContentByCurl(
        $url,
        $param_header = array(),
        $is_post = false,
        $param_fields = array(),
        $encoding = null
    )
    {
        if(sizeof($param_header) > 0)
        {
            foreach ($param_header as $_header) {
                header($_header);
            }
        }

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);

        if(sizeof($param_header) > 0)
            curl_setopt($ch, CURLOPT_HTTPHEADER, $param_header);

        if($is_post && sizeof($param_fields) > 0){
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS , http_build_query($param_fields));
        } else {
            curl_setopt($ch, CURLOPT_POST, 0);
        }

        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLINFO_HEADER_OUT, true);

        if($encoding != null)
            curl_setopt($ch,CURLOPT_ENCODING , $encoding);

        try {
            $data = curl_exec($ch);
        } catch (Exception $ex) {
            return null;
        }
        curl_close($ch);

        return $data;
    }

    public function downloadFile($data, $file_name)
    {
        if (file_exists($file_name)) {
            unlink($file_name);
        }

        try {
            $file = fopen($file_name, "w+");
            fputs($file, $data);
        } catch (Exception $ex) {
            return null;
        }

        return $file_name;
    }

    public function getCsvData($file)
    {
        $csvObject = new Varien_File_Csv();
        try {
            return $csvObject->getData($file);
        } catch (Exception $e) {
            Mage::log('Csv: ' . $file . ' - getCsvData() error - ' . $e->getMessage(), Zend_Log::ERR, 'exception.log', true);
            return false;
        }

    }

    public function getDataCSV($url, $file)
    {
        $_data = null;
        $data = $this->getContentByCurl($url);

        if ($data != null) {
            $_file = $this->downloadFile($data, $file);
            if ($_file != null) {
                $_data = $this->getCsvData($_file);
            }
        }

        return $_data;
    }

    public function getXMLData($str)
    {
        return simplexml_load_string($str);
    }

    public function getDataXML($url, $param_header = array())
    {
        $_data = null;
        $data = $this->getContentByCurl($url, $param_header);
        if ($data != null) {

            if(strpos($data, 'Access Token Inactive') > 0)
                $_data = $data;
            else 
                $_data = $this->getXMLData($data);
        }
        return $_data;
    }

    public function getDataXMLCouponAPI($url, $param_header = array())
    {
        $_data = null;
        $data = $this->getContentByCurl($url, $param_header);
        return $data;
    }

    public function processCouponApiURL()
    {
        $origin_url = $this->getDataConfig('link_api', 'coupon_api');
        $category = $this->getDataConfig('category', 'coupon_api');
        $promotiontype = $this->getDataConfig('promotiontype', 'coupon_api');
        $network = $this->getDataConfig('network', 'coupon_api');
        $mid = $this->getDataConfig('mid', 'coupon_api');
        $resultsperpage = $this->getDataConfig('resultsperpage', 'coupon_api');
        $pagenumber = $this->getDataConfig('pagenumber', 'coupon_api');
        $promocat = $this->getDataConfig('promocat', 'coupon_api');

        if($category != null)
            $origin_url .= '&category='.$category;

        if($promotiontype != null)
            $origin_url .= '&promotiontype='.$promotiontype;

        if($network != null)
            $origin_url .= '&network='.$network;

        if($mid != null)
            $origin_url .= '&mid='.$mid;

        if($resultsperpage != null)
            $origin_url .= '&resultsperpage='.$resultsperpage;

        if($pagenumber != null)
            $origin_url .= '&pagenumber='.$pagenumber;

        if($promocat != null)
            $origin_url .= '&promocat='.$promocat;

        $url = str_replace('?&', '?', $origin_url);

        return $url;
    }

    public function getOptionAttribute($attribute_code = 'manufacturer')
    {
        $attribute = Mage::getSingleton('eav/config')
            ->getAttribute(Mage_Catalog_Model_Product::ENTITY, $attribute_code);

        $options = null;
        if ($attribute->usesSource()) {
            $options = $attribute->getSource()->getAllOptions(false);
        }

        return $options;
    }

    public function addNewOptionToAttribute($new_value, $attribute_code = 'manufacturer')
    {
        $options = $this->getOptionAttribute($attribute_code);

        $is_create = true;
        if(sizeof($options) > 0){
            foreach ($options as $option) {
                if(strcasecmp($option['label'], $new_value) == 0){
                    $is_create = false;
                    break;
                }
            }
        }

        if($is_create){
            $attr_model = Mage::getModel('catalog/resource_eav_attribute');
            $attr = $attr_model->loadByCode('catalog_product', $attribute_code);
            $option = array(
                'value' => array(
                    'option_0' => array(
                        $new_value,
                        $new_value,
                    ),
                ),
                'order' => array(
                    'option_0' => '',
                ),
                'image' => array(
                    'option_0' => '',
                ),
                'delete' => array(
                    'option_0' => '',
                ),
            );

            $attr->setOption($option);
            $attr->save();
        }
    }

    public function getAllCategoryPaths()
    {
        $collection = Mage::getModel('catalog/product')->getCollection()
            ->addAttributeToSelect('entity_id')
            ->addAttributeToSelect('primary_category')
            ->addAttributeToFilter('primary_category', array('notnull'  => true))
            ->addAttributeToFilter('status', 1)
            ->addAttributeToFilter('visibility', 4)
            ->setOrder('entity_id', 'desc')
            ;

        $result = array();

        if(sizeof($collection) > 0)
        {
            $rs = array();
            foreach ($collection as $item) {
                if(!in_array($item->getData('primary_category'), $rs))
                    $rs[] = $item->getData('primary_category');
            }

            if(sizeof($rs) > 0){
                foreach ($rs as $cat) {
                    $_count = $this->countCategoryPath($cat);
                    if($_count > 0)
                        $result[$cat] = $this->countCategoryPath($cat);
                }
            }
        }

        return $result;
    }

    public function countCategoryPath($cat)
    {
        $collection = Mage::getModel('catalog/product')->getCollection()
            ->addAttributeToSelect('entity_id')
            ->addAttributeToSelect('primary_category')
            ->addAttributeToFilter('primary_category', array('like'  => '%'.$cat.'%'))
            ->addAttributeToFilter('status', 1)
            ->addAttributeToFilter('visibility', 4)
            ->setOrder('entity_id', 'desc')
        ;

        return sizeof($collection);
    }

    public function checkID($customer_id, $need)
    {
        return stripos($customer_id, $need);
    }

    public function getOriginId($id, $need)
    {
        return str_replace($need, '', $id);
    }

}
