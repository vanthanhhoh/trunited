<?php

class Magestore_ManageApi_Helper_Coupondeal extends Mage_Core_Helper_Abstract
{
    public function getHelperData()
    {
        return Mage::helper('manageapi');
    }

    public function processAPI($url)
    {
        $data = null;
        $_data = $this->getHelperData()->getDataXML($url);
        $data = json_decode(json_encode((array)$_data), 1);

        $count = 0;
        $update = 0;
        $product_count = 0;
        $data_products = array();
        $manufacturers = array();

        if (intval($data['results']['deals']['@attributes']['count']) <= 0) {
            Mage::getSingleton('adminhtml/session')->addNotice(
                Mage::helper('manageapi')->__('No records from the API')
            );
        } else {
            $results = $data['results']['deals']['deal'];
            if ($results != null && is_array($results) && sizeof($results) > 0) {
                $transactionSave = Mage::getModel('core/resource_transaction');

                $deal_types = $data['resources']['deal_types'];
                if($deal_types['@attributes']['count'] > 0){
                    $data_deal_types = $data['resources']['deal_types']['deal_type'];
                    $this->updatedDealTypes($data_deal_types);
                }

                $merchants = $data['resources']['merchants'];
                if($merchants['@attributes']['count'] > 0){
                    $data_merchants = $data['resources']['merchants']['merchant'];
                    $this->updatedMerchants($data_merchants);
                }

                $merchant_types = $data['resources']['merchant_types'];
                if($merchant_types['@attributes']['count'] > 0){
                    $data_merchant_types = $data['resources']['merchant_types']['merchant_type'];
                    $this->updatedMerchantTypes($data_merchant_types);
                }

                try {
                    $_count = sizeof($results);
                    foreach ($results as $_sale) {

                        if($_count == 1)
                            $sale = $_sale;
                        else
                            $sale = $_sale['@attributes'];

                        if(strtotime($sale['end_on']) >= strtotime(date('Y-m-d 00:00:00', time()))){
                            $id = $this->checkModelId($sale['id']);

                            $model = Mage::getModel('manageapi/coupondeal');
                            $sale['deal_id'] = $sale['id'];

                            $model->setData($sale);
                            $model->setId($id);

                            $transactionSave->addObject($model);

                            if ($id != null && $id > 0)
                                $update++;
                            else
                                $count++;

                            if (!in_array($sale['merchant'], $manufacturers))
                                $manufacturers[] = $sale['merchant'];

                            $data_products[] = $sale;
                        }
                    }

                    $transactionSave->save();

                    $product_count = $this->updateAttributeValue($manufacturers, false, $data_products);

                } catch (Exception $e) {
                    Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                }
            } else {
                Mage::getSingleton('adminhtml/session')->addError('Please wait few seconds before running API again.');
            }
        }

        return array(
            'new' => $count,
            'update' => $update,
            'product' => $product_count,
        );
    }

    public function updatedDealTypes($results)
    {
        $transactionSave = Mage::getModel('core/resource_transaction');
        try {

            $_count = sizeof($results);
            foreach ($results as $_sale) {

                if($_count == 1)
                    $sale = $_sale;
                else
                    $sale = $_sale['@attributes'];

                $id = $this->checkModelTypeId($sale['id']);

                $model = Mage::getModel('manageapi/dealtype');
                $sale['type_id'] = $sale['id'];

                $model->setData($sale);
                $model->setId($id);

                $transactionSave->addObject($model);
            }

            $transactionSave->save();

        } catch (Exception $e) {
            Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
        }
    }

    public function checkModelTypeId($type_id)
    {
        $model = Mage::getModel('manageapi/dealtype')->getCollection()
            ->addFieldToFilter('type_id', array('eq' => $type_id))
            ->getFirstItem();

        if ($model != null && $model->getId())
            return $model->getId();
        else
            return null;
    }

    public function updatedMerchants($results)
    {
        $transactionSave = Mage::getModel('core/resource_transaction');
        try {
            $_count = sizeof($results);
            $update = 0;
            $count = 0;
            foreach ($results as $_sale) {

                if($_count == 1)
                    $sale = $_sale;
                else
                    $sale = $_sale['@attributes'];

                $id = $this->checkModelMerchantId($sale['id'], $sale['network']);

                $model = Mage::getModel('manageapi/popshopsmerchant');
                $sale['merchant_id'] = $sale['id'];

                $model->setData($sale);
                $model->setId($id);

                $transactionSave->addObject($model);

                if ($id != null && $id > 0)
                    $update++;
                else
                    $count++;
            }

            $transactionSave->save();
        } catch (Exception $e) {
            Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
        }
    }

    public function checkModelMerchantId($merchant_id, $network_id)
    {
        $model = Mage::getModel('manageapi/popshopsmerchant')->getCollection()
            ->addFieldToFilter('merchant_id', array('eq' => $merchant_id))
            ->addFieldToFilter('network', array('eq' => $network_id))
            ->getFirstItem();

        if ($model != null && $model->getId())
            return $model->getId();
        else
            return null;
    }

    public function updatedMerchantTypes($results)
    {
        $transactionSave = Mage::getModel('core/resource_transaction');
        try {

            $_count = sizeof($results);
            foreach ($results as $_sale) {

                if($_count == 1)
                    $sale = $_sale;
                else
                    $sale = $_sale['@attributes'];

                $id = $this->checkModelMerchantTypesId($sale['id']);

                $model = Mage::getModel('manageapi/merchanttype');
                $sale['type_id'] = $sale['id'];

                $model->setData($sale);
                $model->setId($id);

                $transactionSave->addObject($model);
            }

            $transactionSave->save();

        } catch (Exception $e) {
            Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
        }
    }

    public function checkModelMerchantTypesId($type_id)
    {
        $model = Mage::getModel('manageapi/merchanttype')->getCollection()
            ->addFieldToFilter('type_id', array('eq' => $type_id))
            ->getFirstItem();

        if ($model != null && $model->getId())
            return $model->getId();
        else
            return null;
    }

    public function checkModelId($deal_id)
    {
        $model = Mage::getModel('manageapi/coupondeal')->getCollection()
            ->addFieldToFilter('deal_id', array('eq' => $deal_id))
            ->getFirstItem();

        if ($model != null && $model->getId())
            return $model->getId();
        else
            return null;
    }

    public function processCron()
    {
        $enable = $this->getHelperData()->getDataConfig('enable', 'popshops_api');
        if ($enable) {
            $url = $this->getHelperData()->getDataConfig('merchant_api', 'popshops_api');
            if ($url != null) {
                $_url = Mage::helper('manageapi')->processCouponApiURL();
                $this->processAPI($_url);
            }
        }
    }

    public function updateAttributeValue($data, $is_create_product, $data_products = array())
    {
        $rs = array(
            'new' => 0,
            'update' => 0,
        );
        if (sizeof($data) > 0) {
            foreach ($data as $dt) {
                if ($dt != '') {
                    $merchant = Mage::getModel('manageapi/popshopsmerchant')->load($dt, 'merchant_id');
                    if($merchant != null && $merchant->getId())
                        Mage::helper('manageapi')->addNewOptionToAttribute($merchant->getName());
                }

            }
        }

        if ($is_create_product && is_array($data_products) && sizeof($data_products) > 0) {
            $rs = $this->createMultipleProducts($data_products);
        }

        return $rs;
    }

    public function synchronizeProduct($ids)
    {
        $collection = Mage::getModel('manageapi/coupondeal')->getCollection()
            ->addFieldToFilter('coupon_deal_id', array('in' => $ids))
            ->setOrder('coupon_deal_id', 'desc')
        ;

        $rs = array();

        if(sizeof($collection) > 0)
        {
            $data = array();
            foreach ($collection as $coupondeal) {
                $_data = $coupondeal->getData();
                $_data['id'] = $_data['deal_id'];
                $data[] = $_data;
            }

            if(sizeof($data) > 0){
                $rs = $this->createMultipleProducts($data);
            }

        }

        return $rs;
    }

    public function createMultipleProducts($data)
    {
        $new = 0;
        $update = 0;
        if (sizeof($data) > 0) {
            $transactionSave = Mage::getModel('core/resource_transaction');
            try {

                foreach ($data as $dt) {

                    if ($dt['id'] != '') {
                        $product_id = $this->checkProductId($dt['id']);
                        $product = Mage::getModel('catalog/product');
                        $merchant = Mage::getModel('manageapi/popshopsmerchant')->load($dt['merchant'], 'merchant_id');
                        $random_str = $this->generateRandomString();
                        if($merchant != null && $merchant->getId()){
                            $primary_deal = explode(',', $dt['deal_type']);
                            $dealtype = Mage::getModel('manageapi/dealtype')->load($primary_deal[0], 'type_id');
                            $merchant_type = Mage::getModel('manageapi/merchanttype')->load(
                                $merchant->getData('merchant_type')
                            );


                            $_dt = array(
                                'website_ids' => array(1),
                                'attribute_set_id' => 4,
                                'type_id' => 'dailyproduct',
                                'name' => $dt['name'],
                                'short_description' => $dt['name'],
                                'description' => $dt['name'],
                                'shop_now' => $dt['url'] . '&u1={{customer_id}}',
                                'sku' => 'daily-deals-' . $random_str . '-' . date('Ymd', time()),
                                'url_key' => strtolower('daily-deals-' . $random_str . '-' . date('Ymd', time())),
                                'news_from_date' => date('m/d/Y', strtotime($dt['start_on'])),
                                'news_to_date' => date('m/d/Y', strtotime($dt['end_on'])),
                                'status' => 1,
                                'visibility' => Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH,
                                'manufacturer' => $this->getAttributeId($merchant->getName()),
                                'primary_category' => $merchant_type != null && $merchant_type->getId() ? $merchant_type->getName() : '',
                                'primary_promotion_type' => $dealtype != null && $dealtype->getId() ? $dealtype->getName() : '',

                                'page_layout' => 'one_column',
                                /*'stock_data' => array(
                                    'use_config_manage_stock' => 0,
                                    'manage_stock' => 0,
                                    'min_sale_qty' => 0,
                                    'max_sale_qty' => 0,
                                    'is_in_stock' => 1,
                                    'qty' => 1000
                                ),*/
                                'category_ids' => array(71, 72),
                                'created_from' => 1,
                            );

                            $product->setData($_dt);
                            $product->setId($product_id);
                            $product->save();

                            $this->updateRecord($dt['id'], $product);

                            if ($product_id > 0)
                                $update++;
                            else
                                $new++;
                        }
                    }
//                    $transactionSave->addObject($product);
                }

                $transactionSave->save();
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
        }

        return array(
            'new' => $new,
            'update' => $update
        );
    }


    public function updateRecord($id, $product)
    {
        $model = Mage::getModel('manageapi/coupondeal')->load($id, 'deal_id');

        if ($model != null && $model->getId()) {
            $model->setData('product_created_at', now());
            $model->setData('product_id', $product->getId());
            $model->setData('updated_at', now());
            $model->save();
        }
    }

    function generateRandomString($length = 5)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public function getAttributeId($attribute_name)
    {
        $options = Mage::helper('manageapi')->getOptionAttribute();
        $id = null;

        if ($options == null) {
            Mage::helper('manageapi')->addNewOptionToAttribute($attribute_name);
        }

        if (sizeof($options) > 0) {
            foreach ($options as $option) {
                if (strcasecmp($option['label'], $attribute_name) == 0) {
                    $id = $option['value'];
                    break;
                }
            }

            if ($id == null) {
                Mage::helper('manageapi')->addNewOptionToAttribute($attribute_name);
            }
        }

        if ($id == null) {
            $options = Mage::helper('manageapi')->getOptionAttribute();
            foreach ($options as $option) {
                if (strcasecmp($option['label'], $attribute_name) == 0) {
                    $id = $option['value'];
                    break;
                }
            }
        }

        return $id;
    }

    public function checkProductId($name)
    {
        $model = Mage::getModel('catalog/product')->getCollection()
            ->addAttributeToFilter('name', array('eq' => $name))
            ->getFirstItem();

        if ($model != null && $model->getId())
            return $model->getId();
        else
            return null;
    }
}
