<?php
ini_set('memory_limit', '-1');
class Magestore_ManageApi_Helper_Walmart extends Mage_Core_Helper_Abstract
{
    public function getHelperData()
    {
        return Mage::helper('manageapi');
    }

    public function processAPI($url, $format, $is_bestsellers = false, $sql = '')
    {
        $data = null;
        $is_xml = false;
        if (strcasecmp($format, 'xml') == 0) {
            $_data = $this->getHelperData()->getDataXML($url);
            $data = json_decode(json_encode((array)$_data), 1);
            $is_xml = true;
        } else if (strcasecmp($format, 'json') == 0) {
            $_data = $this->getHelperData()->getContentByCurl($url);
            $data = json_decode($_data, true);
        }

        if($sql == '') {
            $sql = "INSERT INTO `trwalmart_affiliate`(`itemId`, `specialOffer`, `parentItemId`, `name`, `msrp`, `salePrice`, `upc`, `categoryPath`, `shortDescription`, `longDescription`, `brandName`, `thumbnailImage`, `mediumImage`, `largeImage`, `productTrackingUrl`, `ninetySevenCentShipping`, `standardShipRate`, `twoThreeDayShippingRate`, `size`, `color`, `marketplace`, `shipToStore`, `specialBuy`, `freeShipToStore`, `modelNumber`, `productUrl`, `customerRating`, `numReviews`, `customerRatingImage`, `categoryNode`, `rollBack`, `bundle`, `clearance`, `preOrder`, `stock`, `gender`, `age`, `addToCartUrl`, `affiliateAddToCartUrl`, `freeShippingOver50Dollars`, `maxItemsInOrder`, `availableOnline`, `product_created_at`, `product_id`, `created_at`, `updated_at`, `is_bestsellers`) VALUES";
        }

        $count = 0;
        $update = 0;
        $product_count = 0;
        $data_products = array();
        $manufacturers = array();

        if (sizeof($data) == 0) {
            Mage::getSingleton('adminhtml/session')->addNotice(
                Mage::helper('manageapi')->__('No results from the API')
            );
        } else {
            if (!$is_xml) {
                // json
                $results = $data['items'];
            } else {
                $results = $data['items']['item'];
            }

            if ($results != null && is_array($results) && sizeof($results) > 0) {
                $transactionSave = Mage::getModel('core/resource_transaction');

                try {
                    $resource = Mage::getSingleton('core/resource');
                    $writeConnection = $resource->getConnection('core_write');

                    $insert_query = "INSERT INTO `trwalmart_affiliate`(
                                                        `itemId`,
                                                        `specialOffer`,
                                                        `parentItemId`,
                                                        `name`,
                                                        `msrp`,
                                                        `salePrice`,
                                                        `upc`,
                                                        `categoryPath`,
                                                        `shortDescription`,
                                                        `longDescription`,
                                                        `brandName`,
                                                        `thumbnailImage`,
                                                        `mediumImage`,
                                                        `largeImage`,
                                                        `productTrackingUrl`,
                                                        `ninetySevenCentShipping`,
                                                        `standardShipRate`,
                                                        `twoThreeDayShippingRate`,
                                                        `size`,
                                                        `color`,
                                                        `marketplace`,
                                                        `shipToStore`,
                                                        `specialBuy`,
                                                        `freeShipToStore`,
                                                        `modelNumber`,
                                                        `productUrl`,
                                                        `customerRating`,
                                                        `numReviews`,
                                                        `customerRatingImage`,
                                                        `categoryNode`,
                                                        `rollBack`,
                                                        `bundle`,
                                                        `clearance`,
                                                        `preOrder`,
                                                        `stock`,
                                                        `gender`,
                                                        `age`,
                                                        `addToCartUrl`,
                                                        `affiliateAddToCartUrl`,
                                                        `freeShippingOver50Dollars`,
                                                        `maxItemsInOrder`,
                                                        `availableOnline`,
                                                        `is_bestsellers`,
                                                        `product_created_at`,
                                                        `product_id`,
                                                        `created_at`,
                                                        `updated_at`
                                                    ) VALUES ";

                    $_val = '';
                    foreach ($results as $_sale) {
                        if ($_sale['availableOnline']) {
                            $sale = array();
                            foreach ($_sale as $k => $v) {
                                $sale[$k] = is_array($v) ? json_encode($v) : $v;
                            }

                            $sale['specialOffer'] = isset($data['specialOffer']) ? $data['specialOffer'] : '';
                            $id = $this->checkModelId($sale['itemId']);

                            $sale['rollBack'] = $sale['rollBack'] === true ? 1 : 0;
                            $sale['bundle'] = $sale['bundle'] === true ? 1 : 0;
                            $sale['clearance'] = $sale['clearance'] === true ? 1 : 0;
                            $sale['preOrder'] = $sale['preOrder'] === true ? 1 : 0;
                            $sale['is_bestsellers'] = $is_bestsellers ? 1 : 0;

                            $time = date('Y-m-d H:i:s', time());

                            $_val .= "('".str_replace("'", "\'", $sale['itemId'])."',";
                            $_val .= "'".str_replace("'", "\'", $sale['specialOffer'])."',";
                            $_val .= "'".str_replace("'", "\'", $sale['parentItemId'])."',";
                            $_val .= "'".str_replace("'", "\'", $sale['name'])."',";
                            $_val .= "'".str_replace("'", "\'", $sale['msrp'])."',";
                            $_val .= "'".str_replace("'", "\'", $sale['salePrice'])."',";
                            $_val .= "'".str_replace("'", "\'", $sale['upc'])."',";
                            $_val .= "'".str_replace("'", "\'", $sale['categoryPath'])."',";
                            $_val .= "'".str_replace("'", "\'", $sale['shortDescription'])."',";
                            $_val .= "'".str_replace("'", "\'", $sale['longDescription'])."',";
                            $_val .= "'".str_replace("'", "\'", $sale['brandName'])."',";
                            $_val .= "'".str_replace("'", "\'", $sale['thumbnailImage'])."',";
                            $_val .= "'".str_replace("'", "\'", $sale['mediumImage'])."',";
                            $_val .= "'".str_replace("'", "\'", $sale['largeImage'])."',";
                            $_val .= "'".str_replace("'", "\'", $sale['productTrackingUrl'])."',";
                            $_val .= "'".str_replace("'", "\'", $sale['ninetySevenCentShipping'])."',";
                            $_val .= "'".str_replace("'", "\'", $sale['standardShipRate'])."',";
                            $_val .= "'".str_replace("'", "\'", $sale['twoThreeDayShippingRate'])."',";
                            $_val .= "'".str_replace("'", "\'", $sale['size'])."',";
                            $_val .= "'".str_replace("'", "\'", $sale['color'])."',";
                            $_val .= "'".str_replace("'", "\'", $sale['marketplace'])."',";
                            $_val .= "'".str_replace("'", "\'", $sale['shipToStore'])."',";
                            $_val .= "'".str_replace("'", "\'", $sale['specialBuy'])."',";
                            $_val .= "'".str_replace("'", "\'", $sale['freeShipToStore'])."',";
                            $_val .= "'".str_replace("'", "\'", $sale['modelNumber'])."',";
                            $_val .= "'".str_replace("'", "\'", $sale['productUrl'])."',";
                            $_val .= "'".str_replace("'", "\'", $sale['customerRating'])."',";
                            $_val .= "'".str_replace("'", "\'", $sale['numReviews'])."',";
                            $_val .= "'".str_replace("'", "\'", $sale['customerRatingImage'])."',";
                            $_val .= "'".str_replace("'", "\'", $sale['categoryNode'])."',";
                            $_val .= "'".str_replace("'", "\'", $sale['rollBack'])."',";
                            $_val .= "'".str_replace("'", "\'", $sale['bundle'])."',";
                            $_val .= "'".str_replace("'", "\'", $sale['clearance'])."',";
                            $_val .= "'".str_replace("'", "\'", $sale['preOrder'])."',";
                            $_val .= "'".str_replace("'", "\'", $sale['stock'])."',";
                            $_val .= "'".str_replace("'", "\'", $sale['gender'])."',";
                            $_val .= "'".str_replace("'", "\'", $sale['age'])."',";
                            $_val .= "'".str_replace("'", "\'", $sale['addToCartUrl'])."',";
                            $_val .= "'".str_replace("'", "\'", $sale['affiliateAddToCartUrl'])."',";
                            $_val .= "'".str_replace("'", "\'", $sale['freeShippingOver50Dollars'])."',";
                            $_val .= "'".str_replace("'", "\'", $sale['maxItemsInOrder'])."',";
                            $_val .= "'".str_replace("'", "\'", $sale['availableOnline'])."',";
                            $_val .= "'".$sale['is_bestsellers']."',";
                            $_val .= "null,";
                            $_val .= "null,";
                            $_val .= "'$time',";
                            $_val .= "'$time'),";

                            // $model = Mage::getModel('manageapi/walmart');
                            // $model->setData($sale);
                            // $model->setId($id);

                            // $transactionSave->addObject($model);

                            // if ($id != null && $id > 0)
                            //     $update++;
                            // else
                            //     $count++;

                            // if (!in_array($sale['brandName'], $manufacturers))
                            //     $manufacturers[] = $sale['brandName'];

                            // $data_products[] = $sale;
                        }
                    }

                    if(isset($data['nextPage']) && $data['nextPage'] != null){
                        $next_url = 'http://api.walmartlabs.com'.$data['nextPage'];
                        $_val .= $this->continueAPI($next_url, $data['format'], $_val);
                    }

                    $_val .= ";";

                    $values = str_replace(',;', ';', $_val);

                    $query = $insert_query.$values;

                    // $writeConnection->query($query);

                    $transactionSave->save();

                    // $product_count = $this->updateAttributeValue($manufacturers, false, $data_products);

                } catch (Exception $e) {
                    Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                }
            } else {
                Mage::getSingleton('adminhtml/session')->addError('Please wait few seconds before running API again.');
            }
        }

        return array(
            'new' => $count,
            'update' => $update,
            'product' => $product_count,
        );
    }

    public function continueAPI($url, $format, &$_val)
    {
        $data = null;
        $is_xml = false;
        if (strcasecmp($format, 'xml') == 0) {
            $_data = $this->getHelperData()->getDataXML($url);
            $data = json_decode(json_encode((array)$_data), 1);
            $is_xml = true;
        } else if (strcasecmp($format, 'json') == 0) {
            $_data = $this->getHelperData()->getContentByCurl($url);
            $data = json_decode($_data, true);
        }

        if (sizeof($data) == 0) {
            Mage::getSingleton('adminhtml/session')->addNotice(
                Mage::helper('manageapi')->__('No results from the API')
            );
        } else {
            if (!$is_xml) {
                // json
                $results = $data['items'];
            } else {
                $results = $data['items']['item'];
            }

            if ($results != null && is_array($results) && sizeof($results) > 0) {
                $transactionSave = Mage::getModel('core/resource_transaction');

                try {

                    foreach ($results as $_sale) {
                        if ($_sale['availableOnline']) {
                            $sale = array();
                            foreach ($_sale as $k => $v) {
                                $sale[$k] = is_array($v) ? json_encode($v) : $v;
                            }

                            $sale['specialOffer'] = isset($data['specialOffer']) ? $data['specialOffer'] : '';
                            // $id = $this->checkModelId($sale['itemId']);

                            $sale['rollBack'] = $sale['rollBack'] === true ? 1 : 0;
                            $sale['bundle'] = $sale['bundle'] === true ? 1 : 0;
                            $sale['clearance'] = $sale['clearance'] === true ? 1 : 0;
                            $sale['preOrder'] = $sale['preOrder'] === true ? 1 : 0;
                            $sale['is_bestsellers'] = 0;

                            $time = date('Y-m-d H:i:s', time());

                            $_val .= "('".str_replace("'", "\'", $sale['itemId'])."',";
                            $_val .= "'".str_replace("'", "\'", $sale['specialOffer'])."',";
                            $_val .= "'".str_replace("'", "\'", $sale['parentItemId'])."',";
                            $_val .= "'".str_replace("'", "\'", $sale['name'])."',";
                            $_val .= "'".str_replace("'", "\'", $sale['msrp'])."',";
                            $_val .= "'".str_replace("'", "\'", $sale['salePrice'])."',";
                            $_val .= "'".str_replace("'", "\'", $sale['upc'])."',";
                            $_val .= "'".str_replace("'", "\'", $sale['categoryPath'])."',";
                            $_val .= "'".str_replace("'", "\'", $sale['shortDescription'])."',";
                            $_val .= "'".str_replace("'", "\'", $sale['longDescription'])."',";
                            $_val .= "'".str_replace("'", "\'", $sale['brandName'])."',";
                            $_val .= "'".str_replace("'", "\'", $sale['thumbnailImage'])."',";
                            $_val .= "'".str_replace("'", "\'", $sale['mediumImage'])."',";
                            $_val .= "'".str_replace("'", "\'", $sale['largeImage'])."',";
                            $_val .= "'".str_replace("'", "\'", $sale['productTrackingUrl'])."',";
                            $_val .= "'".str_replace("'", "\'", $sale['ninetySevenCentShipping'])."',";
                            $_val .= "'".str_replace("'", "\'", $sale['standardShipRate'])."',";
                            $_val .= "'".str_replace("'", "\'", $sale['twoThreeDayShippingRate'])."',";
                            $_val .= "'".str_replace("'", "\'", $sale['size'])."',";
                            $_val .= "'".str_replace("'", "\'", $sale['color'])."',";
                            $_val .= "'".str_replace("'", "\'", $sale['marketplace'])."',";
                            $_val .= "'".str_replace("'", "\'", $sale['shipToStore'])."',";
                            $_val .= "'".str_replace("'", "\'", $sale['specialBuy'])."',";
                            $_val .= "'".str_replace("'", "\'", $sale['freeShipToStore'])."',";
                            $_val .= "'".str_replace("'", "\'", $sale['modelNumber'])."',";
                            $_val .= "'".str_replace("'", "\'", $sale['productUrl'])."',";
                            $_val .= "'".str_replace("'", "\'", $sale['customerRating'])."',";
                            $_val .= "'".str_replace("'", "\'", $sale['numReviews'])."',";
                            $_val .= "'".str_replace("'", "\'", $sale['customerRatingImage'])."',";
                            $_val .= "'".str_replace("'", "\'", $sale['categoryNode'])."',";
                            $_val .= "'".str_replace("'", "\'", $sale['rollBack'])."',";
                            $_val .= "'".str_replace("'", "\'", $sale['bundle'])."',";
                            $_val .= "'".str_replace("'", "\'", $sale['clearance'])."',";
                            $_val .= "'".str_replace("'", "\'", $sale['preOrder'])."',";
                            $_val .= "'".str_replace("'", "\'", $sale['stock'])."',";
                            $_val .= "'".str_replace("'", "\'", $sale['gender'])."',";
                            $_val .= "'".str_replace("'", "\'", $sale['age'])."',";
                            $_val .= "'".str_replace("'", "\'", $sale['addToCartUrl'])."',";
                            $_val .= "'".str_replace("'", "\'", $sale['affiliateAddToCartUrl'])."',";
                            $_val .= "'".str_replace("'", "\'", $sale['freeShippingOver50Dollars'])."',";
                            $_val .= "'".str_replace("'", "\'", $sale['maxItemsInOrder'])."',";
                            $_val .= "'".str_replace("'", "\'", $sale['availableOnline'])."',";
                            $_val .= "'".$sale['is_bestsellers']."',";
                            $_val .= "null,";
                            $_val .= "null,";
                            $_val .= "'$time',";
                            $_val .= "'$time'),";
                        }
                    }

                    if(isset($data['nextPage']) && $data['nextPage'] != null){
                        $next_url = 'http://api.walmartlabs.com'.$data['nextPage'];
                        $_val .= $this->continueAPI($next_url, $data['format'], $_val);
                    }

                    return $_val;
                    // $product_count = $this->updateAttributeValue($manufacturers, false, $data_products);

                } catch (Exception $ex){

                }
            }
        }

        return '';
    }

    public function checkModelId($item_id)
    {
        $model = Mage::getModel('manageapi/walmart')->getCollection()
            ->addFieldToFilter('itemId', array('eq' => $item_id))
            ->getFirstItem();

        if ($model != null && $model->getId())
            return $model->getId();
        else
            return null;
    }

    public function updateAttributeValue($data, $is_create_product, $data_products = array())
    {
        $rs = array(
            'new' => 0,
            'update' => 0,
        );
        if (sizeof($data) > 0) {
            foreach ($data as $dt) {
                if ($dt != '') {
                    Mage::helper('manageapi')->addNewOptionToAttribute($dt['brandName']);
                }

            }
        }

        if ($is_create_product && is_array($data_products) && sizeof($data_products) > 0) {
            $rs = $this->createMultipleProducts($data_products);
        }

        return $rs;
    }

    public function massProduct($walmart_ids)
    {
        $collection = Mage::getModel('manageapi/walmart')->getCollection()
            ->addFieldToFilter('walmart_id', array('in' => $walmart_ids))
            ->setOrder('walmart_id', 'desc');

        $rs = array();

        if (sizeof($collection) > 0) {
            $data = array();
            foreach ($collection as $walmart) {
                $data[] = $walmart->getData();
            }

            if (sizeof($data) > 0) {
                $rs = $this->createMultipleProducts($data);
            }

        }

        return $rs;
    }

    public function createMultipleProducts($data)
    {
        $origin_param = Mage::helper('manageapi')->getDataConfig('origin_param','walmart_api');
        $replace_param = Mage::helper('manageapi')->getDataConfig('new_param','walmart_api');
        $new = 0;
        $update = 0;
        if (sizeof($data) > 0) {
            $transactionSave = Mage::getModel('core/resource_transaction');
            try {
                foreach ($data as $dt) {
                    if ($dt['itemId'] != '') {
                        $product_id = $this->checkProductId($dt['name']);
                        $product = Mage::getModel('catalog/product');

                        $random_str = $this->generateRandomString();

                        $url = $dt['productTrackingUrl'] . '&u1={{customer_id}}';

                        $url = str_replace($origin_param, $replace_param, $url);

                        $_dt = array(
                            'website_ids' => array(1),
                            'attribute_set_id' => 4,
                            'type_id' => 'dailyproduct',
                            'name' => $dt['name'],
                            'short_description' => $dt['shortDescription'],
                            'description' => $dt['longDescription'],
                            'shop_now' => $url,
                            'external_image_url' => $dt['largeImage'],
                            'sku' => 'daily-deals-' . $random_str . '-' . date('Ymd', time()),
                            'url_key' => strtolower('daily-deals-' . $random_str . '-' . date('Ymd', time())),
                            'status' => 1,
                            'visibility' => Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH,
                            'manufacturer' => $this->getAttributeId($dt['brandName']),
                            'primary_category' => $this->processCategoryFromPath($dt['categoryPath']),
                            'is_bestsellers' => $dt['is_bestsellers'] ? $dt['is_bestsellers'] : 0,
                            'primary_promotion_type' => '',
                            'page_layout' => 'one_column',
                            'category_ids' => array(71, 72),
                            'created_from' => 1,
                            'daily_deals_msrp' => $dt['msrp'],
                            'daily_deals_sale_price' => $dt['salePrice'],
                            'specialOffer' => $this->getSpecialOffer($dt),
                        );

                        $product->setData($_dt);
                        $product->setId($product_id);
                        $product->save();

                        $this->updateRecord($dt['itemId'], $product);

                        if ($product_id > 0)
                            $update++;
                        else
                            $new++;
                    }
//                    $transactionSave->addObject($product);
                }

                $transactionSave->save();
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
        }

        return array(
            'new' => $new,
            'update' => $update
        );
    }

    public function getSpecialOffer($data)
    {
        if(isset($data['rollBack']) && $data['rollBack'] == 1)
            return Mage::helper('manageapi')->__('rollBack');
        else if(isset($data['clearance']) && $data['clearance'] == 1)
            return Mage::helper('manageapi')->__('clearance');
        else if(isset($data['specialBuy']) && $data['specialBuy'] == 1)
            return Mage::helper('manageapi')->__('specialBuy');
        else
            return '';
    }


    public function processCategoryFromPath($path)
    {
        $result = str_replace('Home/', '', $path);
        if(strpos($result, '/') > 0){
            $_result = explode('/', $result);
            $result = $_result[0];
        }
        return $result;
    }

    public function updateRecord($id, $product)
    {
        $model = Mage::getModel('manageapi/walmart')->load($id, 'itemId');

        if ($model != null && $model->getId()) {
            $model->setData('product_created_at', now());
            $model->setData('product_id', $product->getId());
            $model->setData('updated_at', now());
            $model->save();
        }
    }

    function generateRandomString($length = 5)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public function getAttributeId($attribute_name)
    {
        $options = Mage::helper('manageapi')->getOptionAttribute();
        $id = null;

        if ($options == null) {
            Mage::helper('manageapi')->addNewOptionToAttribute($attribute_name);
        }

        if (sizeof($options) > 0) {
            foreach ($options as $option) {
                if (strcasecmp($option['label'], $attribute_name) == 0) {
                    $id = $option['value'];
                    break;
                }
            }

            if ($id == null) {
                Mage::helper('manageapi')->addNewOptionToAttribute($attribute_name);
            }
        }

        if ($id == null) {
            $options = Mage::helper('manageapi')->getOptionAttribute();
            foreach ($options as $option) {
                if (strcasecmp($option['label'], $attribute_name) == 0) {
                    $id = $option['value'];
                    break;
                }
            }
        }

        return $id;
    }

    public function checkProductId($name)
    {
        $model = Mage::getModel('catalog/product')->getCollection()
            ->addAttributeToFilter('name', array('eq' => $name))
            ->getFirstItem();

        if ($model != null && $model->getId())
            return $model->getId();
        else
            return null;
    }
}
