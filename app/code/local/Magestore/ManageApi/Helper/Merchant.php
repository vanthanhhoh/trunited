<?php

class Magestore_ManageApi_Helper_Merchant extends Mage_Core_Helper_Abstract
{
    public function getHelperData()
    {
        return Mage::helper('manageapi');
    }

    public function processAPI($url)
    {
        $data = null;
        $_data = $this->getHelperData()->getDataXML($url);
        $data = json_decode(json_encode((array)$_data), 1);

        $count = 0;
        $update = 0;

        if (intval($data['results']['merchants']['@attributes']['count']) <= 0) {
            Mage::getSingleton('adminhtml/session')->addNotice(
                Mage::helper('manageapi')->__('No records from the API')
            );
        } else {
            $results = $data['results']['merchants']['merchant'];
            if ($results != null && is_array($results) && sizeof($results) > 0) {
                $transactionSave = Mage::getModel('core/resource_transaction');
                try {

                    $_count = sizeof($results);
                    foreach ($results as $_sale) {

                        if($_count == 1)
                            $sale = $_sale;
                        else
                            $sale = $_sale['@attributes'];

                        $id = $this->checkModelId($sale['id'], $sale['network']);

                        $model = Mage::getModel('manageapi/popshopsmerchant');
                        $sale['merchant_id'] = $sale['id'];

                        $model->setData($sale);
                        $model->setId($id);

                        $transactionSave->addObject($model);

                        if ($id != null && $id > 0)
                            $update++;
                        else
                            $count++;
                    }

                    $transactionSave->save();

                } catch (Exception $e) {
                    Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                }
            } else {
                Mage::getSingleton('adminhtml/session')->addError('Please wait few seconds before running API again.');
            }
        }

        return array(
            'new' => $count,
            'update' => $update,
        );
    }

    public function checkModelId($merchant_id, $network_id)
    {
        $model = Mage::getModel('manageapi/popshopsmerchant')->getCollection()
            ->addFieldToFilter('merchant_id', array('eq' => $merchant_id))
            ->addFieldToFilter('network', array('eq' => $network_id))
            ->getFirstItem();

        if ($model != null && $model->getId())
            return $model->getId();
        else
            return null;
    }

    public function processCron()
    {
        $enable = $this->getHelperData()->getDataConfig('enable', 'popshops_api');
        if ($enable) {
            $url = $this->getHelperData()->getDataConfig('merchant_api', 'popshops_api');
            if ($url != null) {
                $_url = Mage::helper('manageapi')->processCouponApiURL();
                $this->processAPI($_url);
            }
        }
    }

    public function getListCategories()
    {
        $data = null;

        $url = 'http://api.popshops.com/v3/categories.xml?';
        $url .= '&account='.Mage::helper('manageapi')->getDataConfig('account_public_api_key', 'popshops_api');

        $url = str_replace('?&', '?', $url);

        $_data = $this->getHelperData()->getDataXML($url);
        $data = json_decode(json_encode((array)$_data), 1);

        $rs = array();
        if(isset($data['results']) && sizeof($data['results']) > 0){
            $rs[] = array(
                'value'    => $data['results']['categories']['category']['@attributes']['id'],
                'label'    => $data['results']['categories']['category']['@attributes']['name'],
            );

            $this->recursiveCategory($data['results']['categories']['category']['categories'], $rs);
        }

        return $rs;
    }

    public function recursiveCategory($cat, &$result)
    {
        if(isset($cat['category']) && sizeof($cat['category']) > 0)
        {
            foreach ($cat['category'] as $sub_cat) {
                if(isset($sub_cat['@attributes']['leaf']) && $sub_cat['@attributes']['leaf'] == true)
                {
                    $result[] = array(
                        'value' => $sub_cat['@attributes']['id'],
                        'label' => $sub_cat['@attributes']['name'],
                    );
                } else {
                    $result[] = array(
                        'value' => $sub_cat['@attributes']['id'],
                        'label' => $sub_cat['@attributes']['name'],
                    );

                    $this->recursiveCategory($sub_cat['categories'], $result);
                }
            }
        }
    }

    public function getListNetworks()
    {
        $data = null;

        $url = 'http://popshops.com/v3/networks.xml?';
        $url .= '&account='.Mage::helper('manageapi')->getDataConfig('account_public_api_key', 'popshops_api');

        $url = str_replace('?&', '?', $url);

        $_data = $this->getHelperData()->getDataXML($url);
        $data = json_decode(json_encode((array)$_data), 1);

        $rs = array();
        $rs[] = array(
            'value' => '',
            'label' => 'All',
        );
        if(isset($data['results']['networks']) && sizeof($data['results']['networks']) > 0){
            foreach ($data['results']['networks']['network'] as $dt) {
                $rs[] = array(
                    'value' => $dt['@attributes']['id'],
                    'label' => $dt['@attributes']['name'],
                );
            }
        }
        
        return $rs;
    }
}
