<?php

class Magestore_ManageApi_Helper_Target extends Mage_Core_Helper_Abstract
{
    public function getHelperData()
    {
        return Mage::helper('manageapi');
    }

    public function processAPI($url, $start_date)
    {
        $data = null;
        $_data = $this->getHelperData()->getDataXML($url);
        $data = json_decode(json_encode((array)$_data), 1);

        if ($data != null && is_array($data) && sizeof($data) > 0 && isset($data['Records']['Record'])
            && is_array($data['Records']['Record']) && sizeof($data['Records']['Record']) > 0) {

            $transactionSave = Mage::getModel('core/resource_transaction');
            $connection = Mage::getSingleton('core/resource')->getConnection('core_write');

            try {
                $connection->beginTransaction();
                foreach ($data['Records']['Record'] as $v) {
                    $model = Mage::getModel('manageapi/targetactions');
                    $_dt = array();
                    foreach ($v as $k => $_v) {
                        $_dt[strtolower(trim(str_replace('-','_',$k)))] = is_array($_v) ? json_encode($_v) : $_v;
                    }

                    $model->setData($_dt);
                    $model->setData('created_time', now());
                    $transactionSave->addObject($model);
                }
                Mage::log('TARGET API at '.date('Y-m-d H:i:s', time()).' - Result:'.sizeof($data['Records']['Record']).' - URL: '.$url, null, 'check_manage_api.log');
                $transactionSave->save();
                $connection->commit();
            } catch (Exception $e) {
                $connection->rollback();
            }
        } else {
            if(isset($data['Status']) && strcasecmp($data['Status'], 'ERROR') == 0)
                Mage::getSingleton('adminhtml/session')->addError('TARGET API: '.$data['Message']);
        }

         $this->checkOnHold($start_date);
    }

    public function processCron()
    {
        $enable = $this->getHelperData()->getDataConfig('enable_target', 'target');
        if ($enable) {
            $url = $this->getHelperData()->getDataConfig('target_api', 'target');
            if ($url != null) {
                $days = $this->getHelperData()->getDataConfig('target_days', 'target');
                $_days = $days != null ? $days : 1;
                $start_date = date('Y-m-d', strtotime('-'.$_days.' day', time()));
                $end_data = date('Y-m-d', strtotime('-'.$_days.' day', time()));
                $_url = str_replace(array('{{start_date}}', '{{end_date}}'), array($start_date, $end_data), $url);
                $this->processAPI($_url, $start_date);
            }
        }
    }

    public function checkOnHold($start_date)
    {
//        $rewardpoints_transactions = Mage::getModel('rewardpoints/transaction')
//            ->getCollection()
//            ->addFieldToSelect('order_increment_id')
//            ->addFieldToFilter('order_increment_id', array('notnull' => true))
//            ->setOrder('transaction_id', 'desc')
//        ;
//        $order_ids = $rewardpoints_transactions->getColumnValues('order_increment_id');

        $helper = Mage::helper('manageapi');

        $readConnection = Mage::getSingleton('core/resource')->getConnection('core_read');

        $table_name = Mage::getSingleton('core/resource')->getTableName('manageapi/targetactions');
        $sql = "SELECT subid1 as customer_id, SUM(payout) as on_hold_points, action_date, action_id as order_id";
        $sql .= " FROM $table_name";
        $sql .= " WHERE action_date >= '$start_date' and payout > 0";
        $sql .= " GROUP BY action_id";

        $result = $readConnection->fetchAll($sql);
        if(sizeof($result) > 0)
        {
            try{
                $enable_fundraiser = $helper->getDataConfig('enable_fundraiser', 'affiliate_points');
                $enable_daily_deals = $helper->getDataConfig('enable_daily_deals', 'affiliate_points');
                $suffix_fundraiser = $helper->getDataConfig('suffix_fundraiser', 'affiliate_points');
                $suffix_daily_deals = $helper->getDataConfig('suffix_daily_deals', 'affiliate_points');
                $day_hold_points = $helper->getDataConfig('day_hold_points', 'affiliate_points');
                $day_hold_credits = Mage::helper('affiliateearnings')->getConfigData('general', 'day_hold_credit');
                $percent_on_hold_credit = Mage::helper('affiliateearnings')->getConfigData('general', 'percent_on_hold_credit') != '' ? Mage::helper('affiliateearnings')->getConfigData('general', 'percent_on_hold_credit') : 1;

                foreach ($result as $target) {

                    $refclickid = $target['customer_id'];

                    if ($helper->checkID($refclickid, $suffix_fundraiser) > 0 && $enable_fundraiser) {
                        $customer_id = $helper->getOriginId($refclickid, $suffix_fundraiser);
                        $customer = Mage::getModel('customer/customer')->load($customer_id);
                        if ($customer != null && $customer->getId()) {
                            $affiliatepointsAccount = Mage::helper('affiliatepoints/account')
                                ->loadByCustomerId($customer->getId());

                            if ($affiliatepointsAccount != null && $affiliatepointsAccount->getId()) {
                                $expiration_on = Mage::helper('affiliatepoints')->addDaysToDate(
                                    $target['action_date'],
                                    $day_hold_points
                                );
                                $params = array(
                                    'title' => Mage::helper('manageapi')->__('Points awarded for global brand Target.com order on %s. Action ID [%s]', $target['action_date'], $target['order_id']),
                                    'award_from' => Magestore_Affiliatepoints_Model_Status::AWARD_FROM_FUNDRAISER,
                                    'current_point' => $affiliatepointsAccount->getAffiliatePoints(),
                                    'is_on_hold' => 1,
                                    'hold_point' => $target['payout'],
                                    'expiration_at' => $expiration_on,
                                    'created_at' => $target['action_date'],
                                    'order_id' => $target['order_id']
                                );
                                Mage::helper('affiliatepoints/transaction')->createTransaction(
                                    $affiliatepointsAccount,
                                    $params,
                                    Magestore_Affiliatepoints_Model_Type::TYPE_TRANSACTION_RECEIVE_FROM_GLOBAL_BRAND,
                                    Magestore_Affiliatepoints_Model_Status::STATUS_TRANSACTION_ON_HOLD
                                );
                            }
                        }
                    } else if ($helper->checkID($refclickid, $suffix_daily_deals) > 0 && $enable_daily_deals) {
                        $customer_id = $helper->getOriginId($refclickid, $suffix_daily_deals);
                        $customer = Mage::getModel('customer/customer')->load($customer_id);
                        if ($customer != null && $customer->getId()) {
                            $affiliateearningsAccount = Mage::helper('affiliateearnings/account')
                                ->loadByCustomerId($customer->getId());

                            if ($affiliateearningsAccount != null && $affiliateearningsAccount->getId()) {
                                $expiration_on = Mage::helper('affiliateearnings')->addDaysToDate(
                                    $target['action_date'],
                                    $day_hold_credits
                                );
                                $params = array(
                                    'title' => Mage::helper('manageapi')->__('Guest Commission awarded for global brand Target.com order on %s. Action ID [%s]', $target['action_date'], $target['order_id']),
                                    'award_from' => Magestore_Affiliateearnings_Model_Status::AWARD_FROM_DAILY_DEALS,
                                    'current_credit' => $affiliateearningsAccount->getGuestCommission(),
                                    'is_on_hold' => 1,
                                    'hold_credit' => $target['payout'] * $percent_on_hold_credit * 0.01,
                                    'expiration_at' => $expiration_on,
                                    'created_at' => $target['action_date'],
                                    'order_id' => $target['order_id']
                                );
                                Mage::helper('affiliateearnings/transaction')->createTransaction(
                                    $affiliateearningsAccount,
                                    $params,
                                    Magestore_Affiliateearnings_Model_Type::TYPE_TRANSACTION_RECEIVE_FROM_GLOBAL_BRAND,
                                    Magestore_Affiliateearnings_Model_Status::STATUS_TRANSACTION_ON_HOLD
                                );
                            }
                        }
                    } else {
                        $customer = Mage::getModel('customer/customer')->load($target['customer_id']);
                        if($customer != null && $customer->getId()){
                            $exist_transaction = Mage::getModel('rewardpoints/transaction')
                                ->getCollection()
                                ->addFieldToFilter('is_on_hold', 1)
                                ->addFieldToFilter('order_increment_id', array('eq' => $target['order_id']))
                                ->setOrder('transaction_id', 'DESC')
                                ->getFirstItem()
                            ;

                            if($exist_transaction != null && $exist_transaction->getId()){
                                $exist_transaction->setData('updated_time', now());
                                $exist_transaction->setData('on_hold_at', now());
                                $exist_transaction->setData('hold_point', $target['payout']);
                                $exist_transaction->save();
                            } else {
                                Mage::helper('rewardpoints/action')->addTransaction('global_brand', $customer, new Varien_Object(array(
                                        'product_credit_title' => 0,
                                        'product_credit' => 0,
                                        'point_amount' => $target['payout'],
                                        'title' => Mage::helper('manageapi')->__('Points awarded for global brand Target.com order on %s. Action ID [%s]', $target['action_date'], $target['order_id']),
                                        'expiration_day' => 0,
                                        'expiration_day_credit' => 0,
                                        'is_on_hold' => 1,
                                        'created_time' => $target['action_date'],
                                        'order_increment_id' => $target['order_id']
                                    ))
                                );
                            }
                        }
                    }
                }
            } catch (Exception $ex) {

            }
        }
    }

    public function checkOnHold2($start_date)
    {
        $readConnection = Mage::getSingleton('core/resource')->getConnection('core_read');

        $table_name = Mage::getSingleton('core/resource')->getTableName('manageapi/targetactions');
        $sql = "SELECT subid1 as customer_id, SUM(payout) as on_hold_points, action_date, action_id as order_id";
        $sql .= " FROM $table_name";
        $sql .= " WHERE action_date >= '$start_date' and payout BETWEEN 0.1 and 0.999999999";
        $sql .= " GROUP BY action_id";

        zend_debug::dump($sql);
        $count = 0;
        $result = $readConnection->fetchAll($sql);
        zend_debug::dump(sizeof($result));
        if(sizeof($result) > 0)
        {
            try{
                foreach ($result as $target) {
                    $customer = Mage::getModel('customer/customer')->load($target['customer_id']);
                    if($customer != null && $customer->getId()){
                        Mage::helper('rewardpoints/action')->addTransaction('global_brand', $customer, new Varien_Object(array(
                                'product_credit_title' => 0,
                                'product_credit' => 0,
                                'point_amount' => $target['payout'],
                                'title' => Mage::helper('manageapi')->__('Points awarded for global brand Target.com order on %s. Action ID [%s]', $target['action_date'], $target['order_id']),
                                'expiration_day' => 0,
                                'expiration_day_credit' => 0,
                                'is_on_hold' => 1,
                                'created_time' => $target['action_date'],
                                'order_increment_id' => $target['order_id']
                            ))
                        );
                        $count++;
                    }
                }
            } catch (Exception $ex) {

            }
        }
        zend_debug::dump('TARGET: '.$count);
    }
}
