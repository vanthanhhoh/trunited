<?php

class Magestore_ManageApi_Model_Options
{
	static public function getDealTypesOption()
	{
		$opts = array();
		$collection = Mage::getModel('manageapi/dealtype')->getCollection()
				->addFieldToSelect('type_id')
				->addFieldToSelect('name')
				->addFieldToSelect('deal_type_id')
				->setOrder('deal_type_id', 'desc')
			;

		if(sizeof($collection) > 0)
		{
			foreach ($collection as $deal_type) {
				$opts[$deal_type->getTypeId()] = $deal_type->getName();
			}

		}
		return $opts;
	}

	static public function getMerchantsOption()
	{
		$opts = array();
		$collection = Mage::getModel('manageapi/popshopsmerchant')->getCollection()
				->addFieldToSelect('merchant_id')
				->addFieldToSelect('name')
				->addFieldToSelect('popshops_merchant_id')
				->setOrder('popshops_merchant_id', 'desc')
			;

		if(sizeof($collection) > 0)
		{
			foreach ($collection as $deal_type) {
				$opts[$deal_type->getMerchantId()] = $deal_type->getName();
			}

		}
		return $opts;
	}

	public function getLevelOption()
    {
        $collection = Mage::getModel('manageapi/walmartcategory')
            ->getCollection()
            ->addFieldToSelect('category_id')
            ->addFieldToSelect('level')
            ->setOrder('level' , 'asc')
            ;

        $rs = array();

        if(sizeof($collection) > 0)
        {
            $rs[''] = Mage::helper('manageapi')->__('All');
            foreach ($collection as $item) {
                if(sizeof($rs) == 0 || !in_array($item->getLevel(), $rs)){
                    $rs[$item->getLevel()] = $item->getLevel();
                }
            }
        } else {
            $rs[''] = Mage::helper('manageapi')->__('All');
        }

        return $rs;
    }

    public function getCategoryOptions()
    {
        $collection = Mage::getModel('manageapi/walmartcategory')
            ->getCollection()
            ->addFieldToSelect('category_id')
            ->addFieldToSelect('id')
            ->addFieldToSelect('name')
            ->addFieldToSelect('level')
            ->addFieldToFilter('level', array('eq'  => 1))
            ->setOrder('level' , 'asc')
        ;

        $rs = array();

        if(sizeof($collection) > 0)
        {
            $rs[''] = Mage::helper('manageapi')->__('Select category..');
            foreach ($collection as $item) {
                if(sizeof($rs) == 0 || !in_array($item->getLevel(), $rs)){
                    $rs[$item->getData('id')] = $item->getname();
                }
            }
        } else {
            $rs[''] = Mage::helper('manageapi')->__('Select category..');
        }

        return $rs;
    }
}
