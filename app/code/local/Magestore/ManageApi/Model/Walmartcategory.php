<?php

class Magestore_ManageApi_Model_Walmartcategory extends Mage_Core_Model_Abstract
{
	public function _construct(){
		parent::_construct();
		$this->_init('manageapi/walmartcategory');
	}

	/**
	 * Before object save manipulations
	 *
	 * @return Magestore_ManageApi_Model_Walmartcategory
	 */
	protected function _beforeSave()
	{
		parent::_beforeSave();

		if($this->isObjectNew())
			$this->setData('created_at', now());

		$this->setData('updated_at', now());

		return $this;
	}
}
