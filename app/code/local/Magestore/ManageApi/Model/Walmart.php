<?php

class Magestore_ManageApi_Model_Walmart extends Mage_Core_Model_Abstract
{
	public function _construct(){
		parent::_construct();
		$this->_init('manageapi/walmart');
	}

	/**
	 * Before object save manipulations
	 *
	 * @return Magestore_Fundraiser_Model_Customer
	 */
	protected function _beforeSave()
	{
		parent::_beforeSave();

		if($this->isObjectNew())
			$this->setData('created_at', now());

		$this->setData('updated_at', now());

		return $this;
	}

	/**
	 * Processing object after save data
	 *
	 * @return Magestore_Fundraiser_Model_Customer
	 */
	protected function _afterSave()
	{
		return parent::_afterSave();
	}
}
