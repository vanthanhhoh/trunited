<?php

class Magestore_ManageApi_Model_Mysql4_Popshopsmerchant extends Mage_Core_Model_Mysql4_Abstract
{
	public function _construct(){
		$this->_init('manageapi/popshopsmerchant', 'popshops_merchant_id');
	}
}
