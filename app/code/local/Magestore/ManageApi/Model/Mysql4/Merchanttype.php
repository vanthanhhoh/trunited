<?php

class Magestore_ManageApi_Model_Mysql4_Merchanttype extends Mage_Core_Model_Mysql4_Abstract
{
	public function _construct(){
		$this->_init('manageapi/merchanttype', 'merchant_type_id');
	}
}
