<?php

class Magestore_Affiliateearnings_Helper_Account extends Mage_Core_Helper_Abstract
{
    public function updateCredit($customer_id, $credit)
    {
        if(!Mage::helper('affiliateearnings')->isEnable())
            return null;

        $account = $this->loadByCustomerId($customer_id);
        if($account->getId())
        {
            $_current_credit = $account->getGuestCommission();
            $new_credit = $_current_credit + $credit;
            $account->setGuestCommission($new_credit > 0 ? $new_credit : 0);
            $account->save();

            return $account;
        } else
            return null;

    }

    public function loadByCustomerId($customer_id)
    {
        $account = Mage::getModel('affiliateearnings/customer')->load($customer_id, 'customer_id');
        if($account->getId())
            return $account;
        else{
            $new_account = $this->createAffiliateearningsCustomer($customer_id);
            if($new_account != null)
                return $new_account;
            else
                return null;
        }
    }

    public function createAffiliateearningsCustomer($customer_id)
    {
        if(!Mage::helper('affiliateearnings')->isEnable())
            return null;
            
        $model = Mage::getModel('affiliateearnings/customer');

        $model->setData('customer_id', $customer_id);
        $model->setData('guest_commission', 0);

        try{
            if($customer_id != 0)
                $model->save();
            else
                return null;
        } catch (Exception $ex){
            return null;
        }

        return $model;
    }

    public function getCustomerId()
    {
        if(Mage::getSingleton('customer/session')->isLoggedIn()) {
            return Mage::getSingleton('customer/session')->getCustomer()->getId();
        } else {
            return null;
        }
    }

    public function getCurrentAccount()
    {
        $customer_id = $this->getCustomerId();
        if ($customer_id != null) {
            $account = $this->loadByCustomerId($customer_id);

            if ($account != null) {
                return $account;
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    public function getGuestCommission($is_format = true)
    {
        $account = $this->getCurrentAccount();
        if($account != null)
        {
            if ($is_format)
                return Mage::helper('core')->currency(
                    $account->getGuestCommission(),
                    true,
                    false
                );
            else
                return $account->getGuestCommission();
        } else {
            return null;
        }
    }
	
}
