<?php

$installer = $this;
$installer->startSetup();

$installer->run("

	DROP TABLE IF EXISTS {$this->getTable('affiliateearnings/customer')};
	DROP TABLE IF EXISTS {$this->getTable('affiliateearnings/transaction')};	
	DROP TABLE IF EXISTS {$this->getTable('affiliateearnings/temporary')};

	CREATE TABLE {$this->getTable('affiliateearnings/customer')} (
		`affiliateearnings_id` int(11) unsigned NOT NULL auto_increment,
		`customer_id` int(10) unsigned NOT NULL,
		`affiliate_earnings` DECIMAL(10,2) unsigned NOT NULL default 0,
		`created_at` datetime NULL,
		`updated_at` datetime NULL,
		PRIMARY KEY (`affiliateearnings_id`)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8;

	CREATE TABLE {$this->getTable('affiliateearnings/transaction')} (
		`transaction_id` int(10) unsigned NOT NULL auto_increment,
		`affiliateearnings_id` int(10) unsigned NULL,
		`customer_id` int(10) unsigned NULL,
		`customer_email` varchar(255) NOT NULL,
		`title` varchar(255) NOT NULL,
		`description` varchar(255) NOT NULL,
		`action_type` smallint(5) NOT NULL,
		`status` smallint(5) NOT NULL,
		`current_credit` DECIMAL(10,2) unsigned NOT NULL default 0,
		`changed_credit` DECIMAL(10,2) NOT NULL default 0,
		`is_on_hold` TINYINT(4) NULL default 0,
		`hold_credit` DECIMAL(10,2) NULL default 0,
		`expiration_at` datetime NULL,	
		`created_at` datetime NULL,
		`updated_at` datetime NULL,
		`order_id` varchar(255) NULL,
		`receiver_email` varchar(255) NULL,
		`receiver_customer_id` INT unsigned NULL,
		`recipient_transaction_id` int(10) unsigned,
		`credit_back` FLOAT,
		`order_filter_ids` text,
		PRIMARY KEY (`transaction_id`)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8;	

	CREATE TABLE {$this->getTable('affiliateearnings/temporary')} (
		`temporary_id` int(10) unsigned NOT NULL auto_increment,
		`customer_id` int(10) unsigned NULL,
		`created_at` datetime NULL,
		`updated_at` datetime NULL,
		`order_id` int(10) unsigned NULL,
		`status` smallint(5) NOT NULL,
	PRIMARY KEY (`temporary_id`)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8;

	ALTER TABLE {$this->getTable('affiliateearnings/customer')} ADD UNIQUE `unique_affiliateearnings_index`(`customer_id`);

");

// $installer->getConnection()->addColumn($this->getTable('sales/order'), 'affiliateearnings_earn', 'decimal(12,4) NOT NULL default 0');
// $installer->getConnection()->addColumn($this->getTable('sales/order'), 'affiliateearnings_spent', 'decimal(12,4) NOT NULL default 0');
// $installer->getConnection()->addColumn($this->getTable('sales/order'), 'affiliateearnings_discount', 'decimal(12,4) NULL');
// $installer->getConnection()->addColumn($this->getTable('sales/order'), 'base_affiliateearnings_discount', 'decimal(12,4) NULL');

// $installer->getConnection()->addColumn($this->getTable('sales/order_item'), 'affiliateearnings_earn', 'decimal(12,4) NOT NULL default 0');
// $installer->getConnection()->addColumn($this->getTable('sales/order_item'), 'affiliateearnings_spent', 'decimal(12,4) NOT NULL default 0');
// $installer->getConnection()->addColumn($this->getTable('sales/order_item'), 'affiliateearnings_discount', 'decimal(12,4) NULL');
// $installer->getConnection()->addColumn($this->getTable('sales/order_item'), 'base_affiliateearnings_discount', 'decimal(12,4) NULL');

// $installer->getConnection()->addColumn($this->getTable('sales/invoice'), 'affiliateearnings_earn', 'decimal(12,4) NOT NULL default 0');
// $installer->getConnection()->addColumn($this->getTable('sales/invoice'), 'affiliateearnings_discount', 'decimal(12,4) NULL');
// $installer->getConnection()->addColumn($this->getTable('sales/invoice'), 'base_affiliateearnings_discount', 'decimal(12,4) NULL');

// $installer->getConnection()->addColumn($this->getTable('sales/creditmemo'), 'affiliateearnings_earn', 'decimal(12,4) NOT NULL default 0');
// $installer->getConnection()->addColumn($this->getTable('sales/creditmemo'), 'affiliateearnings_discount', 'decimal(12,4) NULL');
// $installer->getConnection()->addColumn($this->getTable('sales/creditmemo'), 'base_affiliateearnings_discount', 'decimal(12,4) NULL');

$installer->endSetup(); 