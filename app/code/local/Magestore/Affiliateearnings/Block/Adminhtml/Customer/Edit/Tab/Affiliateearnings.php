<?php
/**
 * Magestore
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magestore.com license that is
 * available through the world-wide-web at this URL:
 * http://www.magestore.com/license-agreement.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Magestore
 * @package     Magestore_affiliateearnings
 * @copyright   Copyright (c) 2012 Magestore (http://www.magestore.com/)
 * @license     http://www.magestore.com/license-agreement.html
 */

/**
 * affiliateearnings Tab on Customer Edit Form Block
 *
 * @category    Magestore
 * @package     Magestore_affiliateearnings
 * @author      Magestore Developer
 */
class Magestore_Affiliateearnings_Block_Adminhtml_Customer_Edit_Tab_Affiliateearnings
    extends Mage_Adminhtml_Block_Widget_Form
    implements Mage_Adminhtml_Block_Widget_Tab_Interface
{
    protected $_affiliateearningsAccount = null;

    /**
     * @return Mage_Core_Model_Abstract|null
     * @throws Exception
     */
    public function getAffiliateearningsAccount()
    {
        if (is_null($this->_affiliateearningsAccount)) {
            $customerId = $this->getRequest()->getParam('id');
            $this->_affiliateearningsAccount = Mage::getModel('affiliateearnings/customer')
                ->load($customerId, 'customer_id');
        }
        return $this->_affiliateearningsAccount;
    }

    public function getGuestCommission()
    {
        return $this->getAffiliateearningsAccount()->getGuestCommission();
    }

    public function getOnHoldGuestCommission()
    {
        $guest_commission_id = $this->getAffiliateearningsAccount()->getId();

        $collection = Mage::getModel('affiliateearnings/transaction')->getCollection()
            ->addFieldToSelect('transaction_id')
            ->addFieldToSelect('hold_credit')
            ->addFieldToFilter('guest_commission_id', $guest_commission_id)
            ->addFieldToFilter('customer_id', $this->getRequest()->getParam('id'))
            ->addFieldToFilter('status', Magestore_Affiliateearnings_Model_Status::STATUS_TRANSACTION_ON_HOLD)
            ->addFieldToFilter('action_type', array(
                'in'    => Magestore_Affiliateearnings_Model_Type::getDataType()
            ))
            ;

        $total = 0;

        if(sizeof($collection) > 0)
        {
            foreach ($collection as $item) {
                $total += $item->getData('hold_credit');
            }
        }

        return number_format($total, 2);
    }

    /**
     * @return Mage_Adminhtml_Block_Widget_Form
     */
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form();
        $form->setHtmlIdPrefix('affiliateearnings_');
        $this->setForm($form);

        $fieldset = $form->addFieldset('affiliateearnings_form', array(
            'legend' => Mage::helper('affiliateearnings')->__('Guest Commission Information')
        ));

        $fieldset->addField('affiliateearnings_balance', 'note', array(
            'label' => Mage::helper('affiliateearnings')->__('Guest Commission'),
            'title' => Mage::helper('affiliateearnings')->__('Guest Commission'),
            'text' => '<strong>' . Mage::helper('core')->currency($this->getGuestCommission(),true,false) . '</strong>',
        ));

        $fieldset->addField('holding_point', 'note', array(
            'label' => Mage::helper('rewardpoints')->__('On Hold Guest Commission Balance'),
            'title' => Mage::helper('rewardpoints')->__('On Hold Guest Commission Balance'),
            'text' => '<strong>' . Mage::helper('core')->currency($this->getOnHoldGuestCommission(),true,false) . '</strong>',
        ));

        $fieldset->addField('affiliateearnings_point', 'text', array(
            'label' => Mage::helper('affiliateearnings')->__('Change Guest Commission'),
            'title' => Mage::helper('affiliateearnings')->__('Change Guest Commission'),
            'name' => 'Affiliateearnings[affiliate_earnings]',
            'class' => 'validate-number',
            'note' => Mage::helper('affiliateearnings')->__('Add or subtract customer\'s guest commission. For ex: 99 or -99 affiliate points.'),
        ));

        $fieldset->addField('title_point', 'textarea', array(
            'label' => Mage::helper('affiliateearnings')->__('Change Transaction Title'),
            'title' => Mage::helper('affiliateearnings')->__('Change Transaction Title'),
            'name' => 'Affiliateearnings[title]',
            'style' => 'height: 5em;'
        ));

        $fieldset->addField('description_point', 'textarea', array(
            'label' => Mage::helper('affiliateearnings')->__('Change Transaction Description'),
            'title' => Mage::helper('affiliateearnings')->__('Change Transaction Description'),
            'name' => 'Affiliateearnings[description]',
            'style' => 'height: 5em;'
        ));

        $fieldset->addField('expiration_at', 'text', array(
            'label' => Mage::helper('affiliateearnings')->__('Guest Commission Expire On'),
            'title' => Mage::helper('affiliateearnings')->__('Guest Commission Expire On'),
            'name'  => 'Affiliateearnings[expiration_at]',
            'note'  => Mage::helper('affiliateearnings')->__('day(s) since the transaction date. If empty or zero, there is no limitation.')
        ));

        $fieldset->addField('is_on_hold', 'checkbox', array(
            'label' => Mage::helper('affiliateearnings')->__('Guest Commission On Hold'),
            'title' => Mage::helper('affiliateearnings')->__('Guest Commission On Hold'),
            'name'  => 'Affiliateearnings[is_on_hold]',
            'value' => 1,
        ));

        $fieldset = $form->addFieldset('affiliateearnings_history_fieldset', array(
            'legend' => Mage::helper('affiliateearnings')->__('Transaction History')
        ))->setRenderer($this->getLayout()->createBlock('adminhtml/widget_form_renderer_fieldset')->setTemplate(
            'affiliateearnings/history.phtml'
        ));

        return parent::_prepareForm();
    }

    public function getTabLabel()
    {
        return Mage::helper('affiliateearnings')->__('Guest Commission');
    }

    public function getTabTitle()
    {
        return Mage::helper('affiliateearnings')->__('Guest Commission');
    }

    public function canShowTab()
    {
        return true;
    }

    public function isHidden()
    {
        return false;
    }
}
