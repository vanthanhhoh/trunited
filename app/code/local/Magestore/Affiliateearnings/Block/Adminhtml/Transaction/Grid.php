<?php

class Magestore_Affiliateearnings_Block_Adminhtml_Transaction_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
	public function __construct(){
		parent::__construct();
		$this->setId('affiliateearnings_transactionGrid');
		$this->setDefaultSort('transaction_id');
		$this->setDefaultDir('DESC');
		$this->setSaveParametersInSession(true);
	}

	protected function _prepareCollection(){
        $collection = Mage::getModel('affiliateearnings/transaction')->getCollection()
            ->setOrder('transaction_id', 'desc');
        $this->setCollection($collection);
		return parent::_prepareCollection();
	}

	protected function _prepareColumns(){
        $this->addColumn('transaction_id', array(
            'header' => Mage::helper('affiliateearnings')->__('ID'),
            'align' => 'right',
            'width' => '50px',
            'index' => 'transaction_id',
//            'type' => 'number',
        ));

        $this->addColumn('title', array(
            'header' => Mage::helper('affiliateearnings')->__('Title'),
            'align' => 'left',
            'index' => 'title',
            'renderer'  => 'Magestore_Affiliateearnings_Block_Adminhtml_Renderer_Transaction_Title',
        ));

        $this->addColumn('action_type', array(
            'header' => Mage::helper('affiliateearnings')->__('Action'),
            'align' => 'left',
            'index' => 'action_type',
            'type' => 'options',
            'options' => Mage::getSingleton('affiliateearnings/type')->getOptionArray(),
        ));

        $this->addColumn('current_credit', array(
            'header' => Mage::helper('affiliateearnings')->__('Current Credit'),
            'align' => 'right',
            'index' => 'current_credit',
            'type' => 'number',
        ));

        $this->addColumn('changed_credit', array(
            'header' => Mage::helper('affiliateearnings')->__('Updated Credit'),
            'align' => 'right',
            'index' => 'changed_credit',
            'type' => 'number',
        ));

        $this->addColumn('created_at', array(
            'header' => Mage::helper('affiliateearnings')->__('Created On'),
            'index' => 'created_at',
            'type' => 'datetime',
        ));

        $this->addColumn('updated_at', array(
            'header' => Mage::helper('affiliateearnings')->__('Updated On'),
            'index' => 'updated_at',
            'type' => 'datetime',
        ));

        $this->addColumn('expiration_at', array(
            'header' => Mage::helper('affiliateearnings')->__('Expires On'),
            'index' => 'expiration_at',
            'type' => 'datetime',
        ));

        $this->addColumn('status', array(
            'header' => Mage::helper('affiliateearnings')->__('Status'),
            'align' => 'left',
            'index' => 'status',
            'type' => 'options',
            'options' => Mage::getSingleton('affiliateearnings/status')->getTransactionOptionArray(),
        ));

        $this->addColumn('is_on_hold', array(
            'header' => Mage::helper('affiliateearnings')->__('Is On Hold'),
            'align' => 'left',
            'index' => 'is_on_hold',
            'type' => 'options',
            'options' => array(
                '1' => 'Yes',
                '0' =>  'No',
            ),
        ));

        $this->addColumn('hold_credit', array(
            'header' => Mage::helper('affiliateearnings')->__('Hold Credit'),
            'align' => 'left',
            'index' => 'hold_credit',
        ));

        // $this->addColumn('award_from', array(
        //     'header' => Mage::helper('affiliateearnings')->__('Award From'),
        //     'align' => 'left',
        //     'index' => 'award_from',
        //     'type' => 'options',
        //     'options' => Mage::getSingleton('affiliateearnings/status')->getOptionAwardFromArray(),
        // ));

		$this->addExportType('*/*/exportCsv', Mage::helper('affiliateearnings')->__('CSV'));
		$this->addExportType('*/*/exportXml', Mage::helper('affiliateearnings')->__('XML'));

		return parent::_prepareColumns();
	}

	protected function _prepareMassaction(){
		$this->setMassactionIdField('transaction_id');
		$this->getMassactionBlock()->setFormFieldName('transaction');

		$this->getMassactionBlock()->addItem('delete', array(
			'label'		=> Mage::helper('affiliateearnings')->__('Delete'),
			'url'		=> $this->getUrl('*/*/massDelete'),
			'confirm'	=> Mage::helper('affiliateearnings')->__('Are you sure?')
		));

		return $this;
	}

	public function getRowUrl($row){
//		return $this->getUrl('*/*/edit', array('id' => $row->getId()));
	}
}
