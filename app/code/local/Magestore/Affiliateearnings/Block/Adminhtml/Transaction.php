
<?php

class Magestore_Affiliateearnings_Block_Adminhtml_Transaction extends Mage_Adminhtml_Block_Widget_Grid_Container
{
	public function __construct(){
		$this->_controller = 'adminhtml_transaction';
		$this->_blockGroup = 'affiliateearnings';
		$this->_headerText = Mage::helper('affiliateearnings')->__('Transaction Manager');
		$this->_addButtonLabel = Mage::helper('affiliateearnings')->__('Add Transaction');
		parent::__construct();
		$this->_removeButton('add');
	}
}
