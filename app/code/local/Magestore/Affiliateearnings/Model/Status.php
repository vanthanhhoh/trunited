<?php

class Magestore_Affiliateearnings_Model_Status extends Varien_Object
{
	const STATUS_ENABLED	= 1;
	const STATUS_DISABLED	= 2;

    const STATUS_TRANSACTION_PENDING = 1;
    const STATUS_TRANSACTION_COMPLETED = 2;
    const STATUS_TRANSACTION_CANCELED = 3;
    const STATUS_TRANSACTION_EXPIRED = 4;
    const STATUS_TRANSACTION_ON_HOLD = 5;

    const AWARD_FROM_OTHER	= 0;
    const AWARD_FROM_FUNDRAISER	= 1;
    const AWARD_FROM_DAILY_DEALS = 2;
    const AWARD_FROM_GUEST_CHECKOUT = 3;

    const TEMPORARY_PENDING = 1;
    const TEMPORARY_COMPLETE = 2;
    const TEMPORARY_CANCELED = 3;

	static public function getOptionArray(){
		return array(
			self::STATUS_ENABLED	=> Mage::helper('affiliateearnings')->__('Enabled'),
			self::STATUS_DISABLED   => Mage::helper('affiliateearnings')->__('Disabled')
		);
	}
	
	static public function getOptionHash(){
		$options = array();
		foreach (self::getOptionArray() as $value => $label)
			$options[] = array(
				'value'	=> $value,
				'label'	=> $label
			);
		return $options;
	}

    static public function getTransactionOptionArray(){
        return array(
            self::STATUS_TRANSACTION_PENDING	=> Mage::helper('affiliateearnings')->__('Pending'),
            self::STATUS_TRANSACTION_COMPLETED   => Mage::helper('affiliateearnings')->__('Completed'),
            self::STATUS_TRANSACTION_CANCELED   => Mage::helper('affiliateearnings')->__('Canceled'),
            self::STATUS_TRANSACTION_EXPIRED   => Mage::helper('affiliateearnings')->__('Expired'),
            self::STATUS_TRANSACTION_ON_HOLD   => Mage::helper('affiliateearnings')->__('On Hold'),
        );
    }

    static public function getTransactionOptionHash(){
        $options = array();
        foreach (self::getTransactionOptionArray() as $value => $label)
            $options[] = array(
                'value'	=> $value,
                'label'	=> $label
            );
        return $options;
    }

    static public function getOptionAwardFromArray(){
        return array(
            self::AWARD_FROM_OTHER	=> Mage::helper('affiliateearnings')->__('Other'),
            self::AWARD_FROM_FUNDRAISER	=> Mage::helper('affiliateearnings')->__('Fundraiser'),
            self::AWARD_FROM_DAILY_DEALS   => Mage::helper('affiliateearnings')->__('Daily Deals'),
            self::AWARD_FROM_GUEST_CHECKOUT   => Mage::helper('affiliateearnings')->__('Guest Checkout')
        );
    }

    static public function getOptionAwardFromHas(){
        $options = array();
        foreach (self::getOptionArray() as $value => $label)
            $options[] = array(
                'value'	=> $value,
                'label'	=> $label
            );
        return $options;
    }

    static public function getOptionTemporaryArray(){
        return array(
            self::TEMPORARY_PENDING    => Mage::helper('affiliateearnings')->__('Pending'),
            self::TEMPORARY_COMPLETE   => Mage::helper('affiliateearnings')->__('Completed'),
            self::TEMPORARY_CANCELED   => Mage::helper('affiliateearnings')->__('Canceled')
        );
    }
    
    static public function getOptionTemporaryHash(){
        $options = array();
        foreach (self::getOptionTemporaryArray() as $value => $label)
            $options[] = array(
                'value' => $value,
                'label' => $label
            );
        return $options;
    }
}