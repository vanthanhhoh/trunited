<?php

class Magestore_Affiliateearnings_Model_Observer
{
    public function affiliateearningsCreated($observer)
    {
        $customer = $observer->getCustomer();

        if($customer != null && $customer->getId()){
            Mage::helper('affiliateearnings')->createNewObj($customer->getId());
        }

        return $this;
    }

    public function orderPlaceAfter($observer)
    {
        if(!Mage::helper('affiliateearnings')->getConfigData('general', 'enable_guest')){
            return $this;
        }

        $percent = Mage::helper('affiliateearnings')->getConfigData('general', 'percent_earnings_from_guest');
        $_percent = $percent != '' ? $percent : 1;

        $order = $observer['order'];

        if($order->getData('customer_is_guest')){
            $affiliate_account = Mage::helper('affiliateplus/account')->getAffiliateInfoFromCookie();
            if($affiliate_account != null && $affiliate_account->getId())
            {
                Mage::helper('affiliateearnings/transaction')->saveTemporary($order, $affiliate_account->getCustomerId());
            }
        }
    }

    public function orderSaveAfter($observer)
    {
    	if(!Mage::helper('affiliateearnings')->getConfigData('general', 'enable_guest')){
    		return $this;
    	}

        $order = $observer['order'];

        if (in_array($order->getStatus(), array(
            Mage_Sales_Model_Order::STATE_COMPLETE,
            Mage_Sales_Model_Order::STATE_PROCESSING,
        )))
        {
            Mage::helper('affiliateearnings/transaction')->createTransactionFromGuest($order);
        }
    }

    public function orderCancelAfter($observer)
    {
        $order = $observer->getOrder();
        $order_id = $order->getIncrementId();
        
        $transactions = Mage::getModel('affiliateearnings/transaction')->getCollection()
            ->addFieldToFilter('order_id', $order_id)
            ->addFieldToFilter('status', Magestore_Affiliateearnings_Model_Status::STATUS_TRANSACTION_COMPLETED)
            ->addFieldToFilter('action_type', array('in' => array(
                Magestore_Affiliateearnings_Model_Type::TYPE_TRANSACTION_RECEIVE_FROM_GUEST_CHECKOUT,
            )))
            ->setOrder('transaction_id', 'desc')
        ;

        if(sizeof($transactions) > 0)
        {
            $connection = Mage::getSingleton('core/resource')->getConnection('core_write');
            try{
                $connection->beginTransaction();

                foreach ($transactions as $transaction) {
                    $amount_credit = $transaction->getData('changed_credit');
                    $transaction->setData('status', Magestore_Affiliateearnings_Model_Status::STATUS_TRANSACTION_CANCELED);
                    $transaction->setData('updated_at', now());
                    $transaction->save();

                    $customer_id = $transaction->getCustomerId();

                    $affiliateearnings_account = Mage::helper('affiliateearnings/account')->updateCredit(
                        $customer_id, 
                        -$amount_credit
                    );

                    if($affiliateearnings_account != null && $affiliateearnings_account->getId())
                    {
                        $params = array(
                            'title' => Mage::helper('manageapi')->__('Canceled order #<a href="%s">%s</a>',
                                Mage::getUrl('sales/order/view', array('order_id' => $order->getId())),
                                $order->getIncrementId()
                            ),
                            'award_from' => Magestore_Affiliateearnings_Model_Status::AWARD_FROM_OTHER,
                            'current_credit' => $affiliateearnings_account->getGuestCommission(),
                            'is_on_hold' => 0,
                            'hold_credit' => 0,
                            'changed_credit' =>  -$amount_credit,
                            'expiration_at' => '',
                            'created_at' => now(),
                            'order_id' => $order->getIncrementId()
                        );

                        Mage::helper('affiliateearnings/transaction')->createTransaction(
                            $affiliateearnings_account,
                            $params,
                            Magestore_Affiliateearnings_Model_Type::TYPE_TRANSACTION_CANCEL_ORDER,
                            Magestore_Affiliateearnings_Model_Status::STATUS_TRANSACTION_COMPLETED
                        );
                    }
                }

                $connection->commit();
            } catch (Exception $e) {
                $connection->rollback();
            }

        }
    }

    public function creditmemoSaveAfter(Varien_Event_Observer $observer)
    {
        $creditmemo = $observer->getEvent()->getCreditmemo();
        $order_id = $creditmemo->getOrderId();

        $order = Mage::getSingleton('sales/order')->load($order_id);
        
        $transactions = Mage::getModel('affiliateearnings/transaction')->getCollection()
            ->addFieldToFilter('order_id', $order->getIncrementId())
            ->addFieldToFilter('status', Magestore_Affiliateearnings_Model_Status::STATUS_TRANSACTION_COMPLETED)
            ->addFieldToFilter('action_type', array('in' => array(
                Magestore_Affiliateearnings_Model_Type::TYPE_TRANSACTION_RECEIVE_FROM_GUEST_CHECKOUT,
            )))
            ->setOrder('transaction_id', 'desc')
        ;

        if(sizeof($transactions) > 0)
        {
            $connection = Mage::getSingleton('core/resource')->getConnection('core_write');
            try{
                $connection->beginTransaction();

                foreach ($transactions as $transaction) {
                    $amount_credit = $transaction->getData('changed_credit');
                    $transaction->setData('status', Magestore_Affiliateearnings_Model_Status::STATUS_TRANSACTION_CANCELED);
                    $transaction->setData('updated_at', now());
                    $transaction->save();

                    $customer_id = $transaction->getCustomerId();

                    $affiliateearnings_account = Mage::helper('affiliateearnings/account')->updateCredit(
                        $customer_id, 
                        -$amount_credit
                    );

                    if($affiliateearnings_account != null && $affiliateearnings_account->getId())
                    {
                        $params = array(
                            'title' => Mage::helper('manageapi')->__('Refunded order #<a href="%s">%s</a>',
                                Mage::getUrl('sales/order/view', array('order_id' => $order->getId())),
                                $order->getIncrementId()
                            ),
                            'award_from' => Magestore_Affiliateearnings_Model_Status::AWARD_FROM_OTHER,
                            'current_credit' => $affiliateearnings_account->getGuestCommission(),
                            'is_on_hold' => 0,
                            'hold_credit' => 0,
                            'changed_credit' =>  -$amount_credit,
                            'expiration_at' => '',
                            'created_at' => now(),
                            'order_id' => $order->getIncrementId()
                        );

                        Mage::helper('affiliateearnings/transaction')->createTransaction(
                            $affiliateearnings_account,
                            $params,
                            Magestore_Affiliateearnings_Model_Type::TYPE_TRANSACTION_REFUNDED_ORDER,
                            Magestore_Affiliateearnings_Model_Status::STATUS_TRANSACTION_COMPLETED
                        );
                    }
                }

                $connection->commit();
            } catch (Exception $e) {
                $connection->rollback();
            }

        }
    }
}
