<?php

class Magestore_Affiliateearnings_Model_Temporary extends Mage_Core_Model_Abstract
{
	public function _construct(){
		parent::_construct();
		$this->_init('affiliateearnings/temporary');
	}

	/**
     * Before object save manipulations
     *
     * @return Magestore_Affiliateearnings_Model_Customer
     */
    protected function _beforeSave()
    {
        parent::_beforeSave();

        if($this->isObjectNew())
            $this->setData('created_at', now());

        $this->setData('updated_at', now());

        return $this;
    }
}
