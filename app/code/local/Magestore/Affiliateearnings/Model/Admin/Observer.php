<?php

class Magestore_Affiliateearnings_Model_Admin_Observer
{

    /**
     * @param $observer
     * @return $this
     * @throws Exception
     */
    public function customerSaveAfter($observer)
    {
        $customer = $observer['customer'];
        $params = Mage::app()->getRequest()->getParam('Affiliateearnings');
        $helper = Mage::helper('affiliateearnings');

        if(!isset($params) || $params['affiliate_earnings'] == '')
            return $this;

        if(filter_var($params['affiliate_earnings'], FILTER_VALIDATE_FLOAT) === false)
            throw new Exception(
                Mage::helper('affiliateearnings')->__('Data invalid')
            );

        $params['is_on_hold'] = empty($params['is_on_hold']) ? 0 : 1;

        $connection = Mage::getSingleton('core/resource')->getConnection('core_write');

        try {
            $connection->beginTransaction();

            if($params['is_on_hold'] == 1){
                $affiliateearningsAccount = Mage::helper('affiliateearnings/account')->updateCredit(
                    $customer->getId(),
                    0
                );

                if($params['expiration_at'] != null)
                    $params['expiration_at'] = $helper->addDaysToDate(
                        now(),
                        $params['expiration_at']
                    );

                $params['hold_credit'] = $params['affiliate_earnings'];
            } else {
                $affiliateearningsAccount = Mage::helper('affiliateearnings/account')->updateCredit(
                    $customer->getId(),
                    $params['affiliate_earnings']
                );
            }

            if($affiliateearningsAccount != null)
            {
                if($params['is_on_hold'] == 1){
                    $status = Magestore_Affiliateearnings_Model_Status::STATUS_TRANSACTION_ON_HOLD;
                } else {
                    $params['changed_credit'] = $params['affiliate_earnings'];
                    $status = Magestore_Affiliateearnings_Model_Status::STATUS_TRANSACTION_COMPLETED;
                }
                Mage::helper('affiliateearnings/transaction')->createTransaction(
                    $affiliateearningsAccount,
                    $params,
                    Magestore_Affiliateearnings_Model_Type::TYPE_TRANSACTION_BY_ADMIN,
                    $status
                );
            }

            $connection->commit();

        } catch (Exception $e) {
            $connection->rollback();
            Mage::getSingleton('adminhtml/session')->addError(
                Mage::helper('affiliateearnings')->__($e->getMessage())
            );
        }

    }
}
