<?php

class Magestore_Affiliateearnings_Model_Transaction extends Mage_Core_Model_Abstract
{
	public function _construct(){
		parent::_construct();
		$this->_init('affiliateearnings/transaction');
	}

	public function completeTransaction()
	{
		try {
			Mage::helper('affiliateearnings/account')->updateCredit(
				$this->getCustomerId(),
				$this->getHoldCredit()
			);

			$this->setData('changed_credit', $this->getHoldCredit());
			$this->setData('updated_at', now());
			$this->setData('status', Magestore_Affiliateearnings_Model_Status::STATUS_TRANSACTION_COMPLETED);
			$this->save();

		} catch (Exception $e) {
			Mage::logException($e);
		}
	}
}
