<?php

class Magestore_Affiliateearnings_IndexController extends Mage_Core_Controller_Front_Action
{
	public function indexAction(){
		$this->loadLayout();
		$this->renderLayout();
	}

	public function accountAction(){
	    Mage::helper('affiliateearnings')->synchronizeAccount();
    }

    public function updateDbAction()
    {
        $setup = new Mage_Core_Model_Resource_Setup();
        $installer = $setup;
        $installer->startSetup();
        $installer->run("
        		DROP TABLE IF EXISTS {$setup->getTable('affiliateearnings/transaction')};
              CREATE TABLE {$setup->getTable('affiliateearnings/transaction')} (
				`transaction_id` int(10) unsigned NOT NULL auto_increment,
				`affiliateearnings_id` int(10) unsigned NULL,
				`customer_id` int(10) unsigned NULL,
				`customer_email` varchar(255) NOT NULL,
				`title` varchar(255) NOT NULL,
				`description` varchar(255) NOT NULL,
				`action_type` smallint(5) NOT NULL,
				`status` smallint(5) NOT NULL,
				`current_credit` DECIMAL(10,2) unsigned NOT NULL default 0,
				`changed_credit` DECIMAL(10,2) NOT NULL default 0,
				`is_on_hold` TINYINT(4) NULL default 0,
				`hold_credit` DECIMAL(10,2) NULL default 0,
				`expiration_at` datetime NULL,	
				`created_at` datetime NULL,
				`updated_at` datetime NULL,
				`order_id` varchar(255) NULL,
				`receiver_email` varchar(255) NULL,
				`receiver_customer_id` INT unsigned NULL,
				`recipient_transaction_id` int(10) unsigned,
				`credit_back` FLOAT,
				`order_filter_ids` text,
				PRIMARY KEY (`transaction_id`)
			) ENGINE=InnoDB DEFAULT CHARSET=utf8;	
          ");
        $installer->endSetup();
        echo "success";
    }

    public function updateDb2Action()
    {
        $setup = new Mage_Core_Model_Resource_Setup(null);
        $installer = $setup;
        $installer->startSetup();
        $installer->run("
        		DROP TABLE IF EXISTS {$setup->getTable('affiliateearnings/temporary')};
				CREATE TABLE {$setup->getTable('affiliateearnings/temporary')} (
					`temporary_id` int(10) unsigned NOT NULL auto_increment,
					`customer_id` int(10) unsigned NULL,
					`created_at` datetime NULL,
					`updated_at` datetime NULL,
					`order_id` int(10) unsigned NULL,
					`status` smallint(5) NOT NULL,
				PRIMARY KEY (`temporary_id`)
				) ENGINE=InnoDB DEFAULT CHARSET=utf8;	
          ");
        $installer->endSetup();
        echo "success";
    }

    public function updateDb3Action()
    {
        $setup = new Mage_Core_Model_Resource_Setup(null);
        $installer = $setup;
        $installer->startSetup();
        $installer->run("
        	RENAME TABLE `traffiliateearnings_customer` TO `trguest_commission_customer`;
        	RENAME TABLE `traffiliateearnings_transaction` TO `trguest_commission_transaction`;
        	RENAME TABLE `traffiliateearnings_temporary` TO `trguest_commission_temporary`;
          ");
        $installer->endSetup();
        echo "success";
    }

    public function updateDb4Action()
    {
        Mage::helper('affiliateearnings')->synchronizeAccount();
        echo "success";
    }

    public function updateDb5Action()
    {
        $setup = new Mage_Core_Model_Resource_Setup(null);
        $installer = $setup;
        $installer->startSetup();
        
        $installer->run("
            SET FOREIGN_KEY_CHECKS=0;
            
            ALTER TABLE {$setup->getTable('affiliateearnings/customer')} CHANGE affiliate_earnings guest_commission decimal(10,2);
        
            ALTER TABLE {$setup->getTable('affiliateearnings/transaction')} CHANGE affiliateearnings_id guest_commission_id     int(10) UNSIGNED NOT NULL;

            ALTER TABLE {$setup->getTable('affiliateearnings/customer')} CHANGE affiliateearnings_id guest_commission_id int(10) unsigned NOT NULL AUTO_INCREMENT;

            ALTER TABLE `tr_payoutperiod_stats` CHANGE affiliate_points guest_commission decimal(12,2) NULL;
            
            SET FOREIGN_KEY_CHECKS=1;
          ");

        $installer->endSetup();
        echo "success";
    }
}
