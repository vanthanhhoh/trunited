<?php

class Magestore_Affiliateearnings_Adminhtml_TransactionController extends Mage_Adminhtml_Controller_Action
{
	protected function _initAction(){
		$this->loadLayout()
			->_setActiveMenu('affiliateearnings/affiliateearnings')
			->_addBreadcrumb(Mage::helper('adminhtml')->__('Transaction Manager'), Mage::helper('adminhtml')->__('Transaction Manager'));
		return $this;
	}
 
	public function indexAction(){
		$this->_initAction()
			->renderLayout();
	}

	public function deleteAction() {
		if( $this->getRequest()->getParam('id') > 0 ) {
			try {
				$model = Mage::getModel('affiliateearnings/transaction');
				$model->setId($this->getRequest()->getParam('id'))
					->delete();
				Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Transaction was successfully deleted'));
				$this->_redirect('*/*/');
			} catch (Exception $e) {
				Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
				$this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
			}
		}
		$this->_redirect('*/*/');
	}

	public function massDeleteAction() {
		$affiliateearningsIds = $this->getRequest()->getParam('transaction');
		if(!is_array($affiliateearningsIds)){
			Mage::getSingleton('adminhtml/session')->addError(Mage::helper('adminhtml')->__('Please select transaction(s)'));
		}else{
			try {
				foreach ($affiliateearningsIds as $affiliateearningsId) {
					$affiliateearnings = Mage::getModel('affiliateearnings/transaction')->load($affiliateearningsId);
					$affiliateearnings->delete();
				}
				Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Total of %d record(s) were successfully deleted', count($affiliateearningsIds)));
			} catch (Exception $e) {
				Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
			}
		}
		$this->_redirect('*/*/index');
	}
  
	public function exportCsvAction(){
		$fileName   = 'affiliateearnings_transaction.csv';
		$content	= $this->getLayout()->createBlock('affiliateearnings/adminhtml_transaction_grid')->getCsv();
		$this->_prepareDownloadResponse($fileName,$content);
	}

	public function exportXmlAction(){
		$fileName   = 'affiliateearnings_transaction.xml';
		$content	= $this->getLayout()->createBlock('affiliateearnings/adminhtml_transaction_grid')->getXml();
		$this->_prepareDownloadResponse($fileName,$content);
	}

	public function gridAction()
	{
		$this->loadLayout();
		$this->renderLayout();
	}
}
