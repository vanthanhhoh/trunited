<?php

class Magestore_Applyprovider_Model_Mysql4_Applyprovider_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
	public function _construct(){
		parent::_construct();
		$this->_init('applyprovider/applyprovider');
	}
}