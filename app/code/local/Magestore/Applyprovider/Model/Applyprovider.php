<?php

class Magestore_Applyprovider_Model_Applyprovider extends Mage_Core_Model_Abstract
{
	public function _construct(){
		parent::_construct();
		$this->_init('applyprovider/applyprovider');
	}

	/**
	 * Before object save manipulations
	 *
	 * @return Magestore_Fundraiser_Model_Visit
	 */
	protected function _beforeSave()
	{
		parent::_beforeSave();

		if($this->isObjectNew())
			$this->setData('created_at', now());

		$this->setData('updated_at', now());

		return $this;
	}
}