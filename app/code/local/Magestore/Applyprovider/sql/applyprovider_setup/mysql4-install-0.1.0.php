<?php

$installer = $this;
$installer->startSetup();

$installer->run("

DROP TABLE IF EXISTS {$this->getTable('applyprovider/applyprovider')};
CREATE TABLE {$this->getTable('applyprovider/applyprovider')} (
  `applyprovider_id` int(11) unsigned NOT NULL auto_increment,
  `service_type` varchar(255) NOT NULL default '',
  `company_name` varchar(255) NOT NULL default '',
  `company_description` varchar(255) NOT NULL default '',
  `website_url` varchar(255) NOT NULL default '',
  `phone` varchar(255) NOT NULL default '',
  `contact_name` varchar(255) NOT NULL default '',
  `status` smallint(6) NOT NULL default '0',
  `created_at` datetime NULL,
  `updated_at` datetime NULL,
  PRIMARY KEY (`applyprovider_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

    ");

$installer->endSetup(); 