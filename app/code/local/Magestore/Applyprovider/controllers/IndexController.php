<?php

class Magestore_Applyprovider_IndexController extends Mage_Core_Controller_Front_Action
{
	public function indexAction(){
		$this->loadLayout();
		$this->renderLayout();
	}

	public function applyPostAction()
	{
		$data = $this->getRequest()->getParams();
		if($data != null)
		{
			$provider = Mage::getModel('applyprovider/applyprovider');
			$provider->setData('service_type', $data['service_type']);
			$provider->setData('company_description', $data['company_description']);
			$provider->setData('website_url', $data['website_url']);
			$provider->setData('contact_name', $data['contact_name']);

			try{
				$provider->save();
				Mage::getSingleton('core/session')->addSuccess(
					Mage::helper('applyprovider')->__('Thank you for applying as a service provider on Trunited.com! We believe this to be the next great pillar of the Trunited platform. We will read your application and reach out to you when the time is right.
')
				);
			} catch (Exception $ex) {
				Mage::getSingleton('core/session')->addError(
					$ex->getMessage()
				);
			}
		} else {
			Mage::getSingleton('core/session')->addError(
				Mage::helper('applyprovider')->__('No data')
			);
		}

		$this->_redirectUrl(Mage::getUrl('apply-provider'));
	}
}