<?php

class Magestore_Applyprovider_Helper_Data extends Mage_Core_Helper_Abstract
{
    public function getConfigData($section, $field, $store = null)
    {
        return Mage::getStoreConfig('applyprovider/'.$section.'/'.$field, $store);
    }

    public function isEnable()
    {
        return $this->getConfigData('general', 'enable');
    }

    public function getServices()
    {
        $services = $this->getConfigData('general', 'services');
        if ($services) {
            $service_data = unserialize($services);
            if (is_array($service_data)) {
                $rs = array();
                foreach($service_data as $service) {
                    $rs[] = $service['service_name'];
                }
                return $rs;
            } else {
                return null;
            }
        }

        return null;
    }
	
}