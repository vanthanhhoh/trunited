<?php

class Magestore_Applyprovider_Block_Applyprovider extends Mage_Core_Block_Template
{
	public function _prepareLayout(){
		return parent::_prepareLayout();
	}

	public function getActionUrl()
	{
		return $this->getUrl('applyprovider/index/applyPost');
	}

	public function getServices()
	{
		return Mage::helper('applyprovider')->getServices();
	}
}