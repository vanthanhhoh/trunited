<?php

class Magestore_Applyprovider_Block_Adminhtml_Applyprovider_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
	public function __construct(){
		parent::__construct();
		
		$this->_objectId = 'id';
		$this->_blockGroup = 'applyprovider';
		$this->_controller = 'adminhtml_applyprovider';
		
		$this->_updateButton('save', 'label', Mage::helper('applyprovider')->__('Save Item'));
		$this->_updateButton('delete', 'label', Mage::helper('applyprovider')->__('Delete Item'));
		
		$this->_addButton('saveandcontinue', array(
			'label'		=> Mage::helper('adminhtml')->__('Save And Continue Edit'),
			'onclick'	=> 'saveAndContinueEdit()',
			'class'		=> 'save',
		), -100);

		$this->_formScripts[] = "
			function toggleEditor() {
				if (tinyMCE.getInstanceById('applyprovider_content') == null)
					tinyMCE.execCommand('mceAddControl', false, 'applyprovider_content');
				else
					tinyMCE.execCommand('mceRemoveControl', false, 'applyprovider_content');
			}

			function saveAndContinueEdit(){
				editForm.submit($('edit_form').action+'back/edit/');
			}
		";
	}

	public function getHeaderText(){
		if(Mage::registry('applyprovider_data') && Mage::registry('applyprovider_data')->getId())
			return Mage::helper('applyprovider')->__("Edit Item '%s'", $this->htmlEscape(Mage::registry('applyprovider_data')->getTitle()));
		return Mage::helper('applyprovider')->__('Add Item');
	}
}