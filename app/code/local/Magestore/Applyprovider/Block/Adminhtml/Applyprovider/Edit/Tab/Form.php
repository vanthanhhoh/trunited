<?php

class Magestore_Applyprovider_Block_Adminhtml_Applyprovider_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
	protected function _prepareForm(){
		$form = new Varien_Data_Form();
		$this->setForm($form);
		
		if (Mage::getSingleton('adminhtml/session')->getApplyproviderData()){
			$data = Mage::getSingleton('adminhtml/session')->getApplyproviderData();
			Mage::getSingleton('adminhtml/session')->setApplyproviderData(null);
		}elseif(Mage::registry('applyprovider_data'))
			$data = Mage::registry('applyprovider_data')->getData();
		
		$fieldset = $form->addFieldset('applyprovider_form', array('legend'=>Mage::helper('applyprovider')->__('Item information')));

		$fieldset->addField('title', 'text', array(
			'label'		=> Mage::helper('applyprovider')->__('Title'),
			'class'		=> 'required-entry',
			'required'	=> true,
			'name'		=> 'title',
		));

		$fieldset->addField('filename', 'file', array(
			'label'		=> Mage::helper('applyprovider')->__('File'),
			'required'	=> false,
			'name'		=> 'filename',
		));

		$fieldset->addField('status', 'select', array(
			'label'		=> Mage::helper('applyprovider')->__('Status'),
			'name'		=> 'status',
			'values'	=> Mage::getSingleton('applyprovider/status')->getOptionHash(),
		));

		$fieldset->addField('content', 'editor', array(
			'name'		=> 'content',
			'label'		=> Mage::helper('applyprovider')->__('Content'),
			'title'		=> Mage::helper('applyprovider')->__('Content'),
			'style'		=> 'width:700px; height:500px;',
			'wysiwyg'	=> false,
			'required'	=> true,
		));

		$form->setValues($data);
		return parent::_prepareForm();
	}
}