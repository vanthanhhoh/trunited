<?php

class Magestore_Applyprovider_Block_Adminhtml_Applyprovider_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('applyproviderGrid');
        $this->setDefaultSort('applyprovider_id');
        $this->setDefaultDir('ASC');
        $this->setSaveParametersInSession(true);
    }

    protected function _prepareCollection()
    {
        $collection = Mage::getModel('applyprovider/applyprovider')->getCollection();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $this->addColumn('applyprovider_id', array(
            'header' => Mage::helper('applyprovider')->__('ID'),
            'align' => 'right',
            'width' => '50px',
            'index' => 'applyprovider_id',
        ));

        $this->addColumn('service_type', array(
            'header' => Mage::helper('applyprovider')->__('Service Type'),
            'align' => 'left',
            'index' => 'service_type',
        ));

//        $this->addColumn('company_name', array(
//            'header' => Mage::helper('applyprovider')->__('Company Name'),
//            'index' => 'company_name',
//        ));
        $this->addColumn('company_description', array(
            'header' => Mage::helper('applyprovider')->__('Company Description'),
            'index' => 'company_description',
        ));
        $this->addColumn('website_url', array(
            'header' => Mage::helper('applyprovider')->__('Website'),
            'index' => 'website_url',
        ));
//        $this->addColumn('phone', array(
//            'header' => Mage::helper('applyprovider')->__('Phone'),
//            'index' => 'phone',
//        ));
        $this->addColumn('contact_name', array(
            'header' => Mage::helper('applyprovider')->__('Contact Name'),
            'index' => 'contact_name',
        ));

        $this->addColumn('created_at', array(
            'header' => Mage::helper('applyprovider')->__('Created At'),
            'align' => 'right',
            'index' => 'created_at',
            'type' => 'date'
        ));

        $this->addExportType('*/*/exportCsv', Mage::helper('applyprovider')->__('CSV'));
        $this->addExportType('*/*/exportXml', Mage::helper('applyprovider')->__('XML'));

        return parent::_prepareColumns();
    }

    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('applyprovider_id');
        $this->getMassactionBlock()->setFormFieldName('applyprovider');

        $this->getMassactionBlock()->addItem('delete', array(
            'label' => Mage::helper('applyprovider')->__('Delete'),
            'url' => $this->getUrl('*/*/massDelete'),
            'confirm' => Mage::helper('applyprovider')->__('Are you sure?')
        ));

        return $this;
    }

    public function getRowUrl($row)
    {
//		return $this->getUrl('*/*/edit', array('id' => $row->getId()));
    }
}