<?php

class Magestore_Applyprovider_Block_Adminhtml_Applyprovider extends Mage_Adminhtml_Block_Widget_Grid_Container
{
	public function __construct(){
		$this->_controller = 'adminhtml_applyprovider';
		$this->_blockGroup = 'applyprovider';
		$this->_headerText = Mage::helper('applyprovider')->__('Request Provider Manager');
		$this->_addButtonLabel = Mage::helper('applyprovider')->__('Add Item');
		parent::__construct();
		$this->_removeButton('add');
	}
}