<?php
/**
 * Created by PhpStorm.
 * User: longvuxuan
 * Date: 8/30/17
 * Time: 11:21 AM
 */

class Magestore_Applyprovider_Block_Adminhtml_System_Config_Form_Fieldset_Service extends
    Mage_Adminhtml_Block_System_Config_Form_Field_Array_Abstract
{
    public function _prepareToRender()
    {
        $this->addColumn('service_name', array(
            'label' => Mage::helper('applyprovider')->__('Service Name'),
            'style' => 'width:300px',
        ));
        $this->_addAfter = false;
        $this->_addButtonLabel = Mage::helper('applyprovider')->__('Add');
    }

}
