<?php

$installer = $this;
$installer->startSetup();

$installer->run("

DROP TABLE IF EXISTS {$this->getTable('nationpassport')};
CREATE TABLE {$this->getTable('nationpassport')} (
  `nationpassport_id` int(11) unsigned NOT NULL auto_increment,
  `title` varchar(255) NOT NULL default '',
  `filename` varchar(255) NOT NULL default '',
  `content` text NOT NULL default '',
  `status` smallint(6) NOT NULL default '0',
  `created_time` datetime NULL,
  `update_time` datetime NULL,
  PRIMARY KEY (`nationpassport_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS {$this->getTable('nationpassport/message')};
            CREATE TABLE {$this->getTable('nationpassport/message')} (
                `message_id` int(11) unsigned NOT NULL auto_increment,
                `customer_id` int(10) unsigned NOT NULL,
                `recipient_id` int(10) unsigned NOT NULL,
                `subject` VARCHAR(255) NULL DEFAULT '',
                `body` text NULL DEFAULT '',
                `status` tinyint NULL,
                `created_at` datetime NULL,
                `updated_at` datetime NULL,
                PRIMARY KEY (`message_id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

    ");

$installer->endSetup(); 
