<?php

class Magestore_Nationpassport_Model_Message extends Mage_Core_Model_Abstract
{
	public function _construct(){
		parent::_construct();
		$this->_init('nationpassport/message');
	}

    /**
     * Before object save manipulations
     *
     * @return Magestore_Nationpassport_Model_Message
     */
    protected function _beforeSave()
    {
        parent::_beforeSave();

        if($this->isObjectNew()){
            $this->setData('created_at', now());
            $this->setData('status', Magestore_Nationpassport_Model_Status::STATUS_ENABLED);
        }


        $this->setData('updated_at', now());

        return $this;
    }
}