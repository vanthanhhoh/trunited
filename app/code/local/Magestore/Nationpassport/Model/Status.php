<?php

class Magestore_Nationpassport_Model_Status extends Varien_Object
{
	const STATUS_ENABLED	= 1;
	const STATUS_DISABLED	= 2;

	const ACCOUNT_TYPE_PERSONAL_CHECKING = 'personal_checking';
	const ACCOUNT_TYPE_PERSONAL_SAVINGS = 'personal_saving';
	const ACCOUNT_TYPE_BUSINESS_CHECKING = 'business_checking';
	const ACCOUNT_TYPE_BUSINESS_SAVINGS = 'business_saving';

	const RANK_PERSONAL_POINTS = 1;
	const RANK_DEPT_CREDIT = 2;
	const RANK_AVG_TEAM_POINTS = 3;
	const RANK_TRUBOX_POINTS = 4;
	const RANK_ON_HOLD_POINTS = 5;

	static public function getOptionArray(){
		return array(
			self::STATUS_ENABLED	=> Mage::helper('nationpassport')->__('Enabled'),
			self::STATUS_DISABLED   => Mage::helper('nationpassport')->__('Disabled')
		);
	}
	
	static public function getOptionHash(){
		$options = array();
		foreach (self::getOptionArray() as $value => $label)
			$options[] = array(
				'value'	=> $value,
				'label'	=> $label
			);
		return $options;
	}

    static public function getOptionAccountTypeArray(){
        return array(
            self::ACCOUNT_TYPE_PERSONAL_CHECKING	=> Mage::helper('nationpassport')->__('Personal Checking'),
            self::ACCOUNT_TYPE_PERSONAL_SAVINGS   => Mage::helper('nationpassport')->__('Personal Saving'),
            self::ACCOUNT_TYPE_BUSINESS_CHECKING   => Mage::helper('nationpassport')->__('Business Checking'),
            self::ACCOUNT_TYPE_BUSINESS_SAVINGS   => Mage::helper('nationpassport')->__('Business Saving'),
        );
    }

    static public function getOptionAccountTypeHash(){
        $options = array();
        foreach (self::getOptionAccountTypeArray() as $value => $label)
            $options[] = array(
                'value'	=> $value,
                'label'	=> $label
            );
        return $options;
    }

    static public function getRankOption()
    {
        return array(
          self::RANK_PERSONAL_POINTS => Mage::helper('nationpassport')->__('Personal Points'),
//          self::RANK_DEPT_CREDIT => Mage::helper('nationpassport')->__('Depth Credit'),
          self::RANK_TRUBOX_POINTS => Mage::helper('nationpassport')->__('TruBox Points'),
          self::RANK_ON_HOLD_POINTS => Mage::helper('nationpassport')->__('On Hold Points'),
        );
    }

    static public function getBusinessRankOption()
    {
        return array(
            self::RANK_PERSONAL_POINTS => Mage::helper('nationpassport')->__('Personal Points'),
            self::RANK_DEPT_CREDIT => Mage::helper('nationpassport')->__('Depth Credit'),
            self::RANK_TRUBOX_POINTS => Mage::helper('nationpassport')->__('TruBox Points'),
            self::RANK_ON_HOLD_POINTS => Mage::helper('nationpassport')->__('On Hold Points'),
        );
    }

    static  public function getRankArray()
    {
        return array(
            self::RANK_PERSONAL_POINTS,
//            self::RANK_DEPT_CREDIT,
            self::RANK_TRUBOX_POINTS,
            self::RANK_ON_HOLD_POINTS,
        );
    }

    static  public function getBusinessRankArray()
    {
        return array(
            self::RANK_PERSONAL_POINTS,
            self::RANK_DEPT_CREDIT,
            self::RANK_TRUBOX_POINTS,
            self::RANK_ON_HOLD_POINTS,
        );
    }
}
