<?php

class Magestore_Nationpassport_Model_Observer
{
    public function redirectAfterLogin($observer)
    {
        $enable = Mage::helper('nationpassport')->getConfigData('general', 'enable');
        $enable_redirect = Mage::helper('nationpassport')->getConfigData('general', 'redirect_nation_page');
        if($enable_redirect && $enable) {
            Mage::getSingleton('customer/session')->setBeforeAuthUrl(Mage::getUrl('passport'));
			Mage::getSingleton('customer/session')->setAfterAuthUrl(Mage::getUrl('passport'));
        }
    }
}
