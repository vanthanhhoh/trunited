<?php

class Magestore_Nationpassport_IndexController extends Mage_Core_Controller_Front_Action
{
    /**
     * check customer is logged in
     */
    public function preDispatch()
    {
        parent::preDispatch();
        if (!$this->getRequest()->isDispatched()) {
            return;
        }
        $action = $this->getRequest()->getActionName();
        if ($action != 'policy' && $action != 'redirectLogin') {
            // Check customer authentication
            if (!Mage::getSingleton('customer/session')->isLoggedIn()) {
                Mage::getSingleton('customer/session')->setAfterAuthUrl(
                    Mage::getUrl($this->getFullActionName('/'))
                );
                $this->_redirectUrl(Mage::getUrl('customer/account/login'));
                $this->setFlag('', Mage_Core_Controller_Varien_Action::FLAG_NO_DISPATCH, true);
            }
        }
    }

	public function indexAction(){
		$this->loadLayout();

        $this->_title(Mage::helper('nationpassport')->__('Trunited Nation Passport'));

        $breadcrumbs = $this->getLayout()->getBlock("breadcrumbs");
        $breadcrumbs->addCrumb("home", array(
            "label" => $this->__("Home"),
            "title" => $this->__("Home"),
            "link"  => Mage::getBaseUrl()
        ));

        $breadcrumbs->addCrumb("passport", array(
            "label" => $this->__("Trunited Nation Passport"),
            "title" => $this->__("Trunited Nation Passport"),
        ));

		$this->renderLayout();
	}

    public function updateDbAction(){
        $setup = new Mage_Core_Model_Resource_Setup();
        $installer = $setup;
        $installer->startSetup();
        $installer->run("
              ALTER TABLE {$setup->getTable('rewardpoints/transaction')} MODIFY COLUMN point_amount FLOAT ;
              ALTER TABLE {$setup->getTable('rewardpoints/transaction')} MODIFY COLUMN point_used  FLOAT ;
              ALTER TABLE {$setup->getTable('rewardpoints/transaction')} MODIFY COLUMN real_point  FLOAT ;
              
              ALTER TABLE {$setup->getTable('rewardpoints/customer')} MODIFY COLUMN point_balance FLOAT ;
              ALTER TABLE {$setup->getTable('rewardpoints/customer')} MODIFY COLUMN holding_balance   FLOAT ;
              ALTER TABLE {$setup->getTable('rewardpoints/customer')} MODIFY COLUMN spent_balance   FLOAT ;
        ");
        $installer->endSetup();
        echo "success";
    }

    public function updateDb2Action(){
        $setup = new Mage_Core_Model_Resource_Setup();
        $installer = $setup;
        $installer->startSetup();
        $installer->run("
              ALTER TABLE {$setup->getTable('affiliatepluspayment/bankaccount')} ADD `account_type` VARCHAR(255) NULL DEFAULT '';
        ");
        $installer->endSetup();
        echo "success";
    }

    public function updateDb3Action(){
        $setup = new Mage_Core_Model_Resource_Setup();
        $installer = $setup;
        $installer->startSetup();
        $installer->run("
            DROP TABLE IF EXISTS {$setup->getTable('nationpassport/message')};
            CREATE TABLE {$setup->getTable('nationpassport/message')} (
                `message_id` int(11) unsigned NOT NULL auto_increment,
                `customer_id` int(10) unsigned NOT NULL,
                `recipient_id` int(10) unsigned NOT NULL,
                `subject` VARCHAR(255) NULL DEFAULT '',
                `body` text NULL DEFAULT '',
                `status` tinyint NULL,
                `created_at` datetime NULL,
                `updated_at` datetime NULL,
                PRIMARY KEY (`message_id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
        ");
        $installer->endSetup();
        echo "success";
    }

    public function getContentPopupAction()
    {
        $result = array();

        $popup = Mage::getModel('badges/popup');
        $popup->setData('customer_id', Mage::getSingleton('customer/session')->getCustomer()->getId());
        $popup->save();

        $result['is_show'] = true;

        $result['html'] = $this->getLayout()->createBlock('nationpassport/popup')
            ->setTemplate('nationpassport/popup.phtml')->toHtml();

        echo json_encode($result);
    }

    public function testAction()
    {
    	# code...
    	$resource = Mage::getSingleton('core/resource');
    	$readConnection = $resource->getConnection('core_read');
		$tableName = $resource->getTableName('affiliateplus/transaction');

		$end_date = date('Y-m-d', time());
		$start_date = date("Y-m-d", strtotime("first day of this month"));
 
		$query = "SELECT SUM(commission) AS `affiliate_income` FROM $tableName WHERE (`account_id` = '136') AND (`type` = '10') AND (`created_time` <= '$end_date') AND (`created_time` >= '$start_date') GROUP BY `account_id`";

		$affiliate_income = $readConnection->fetchCol($query);

        if(sizeof($affiliate_income) > 0)
            zend_debug::dump($affiliate_income[0]);
        else
            zend_debug::dump('b');
		zend_debug::dump($query); 
		zend_debug::dump($affiliate_income);
    }
}
