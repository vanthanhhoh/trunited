<?php

class Magestore_Nationpassport_MessageController extends Mage_Core_Controller_Front_Action
{
    /**
     * check customer is logged in
     */
    public function preDispatch()
    {
        parent::preDispatch();
        if (!$this->getRequest()->isDispatched()) {
            return;
        }
        $action = $this->getRequest()->getActionName();
        if ($action != 'policy' && $action != 'redirectLogin') {
            // Check customer authentication
            if (!Mage::getSingleton('customer/session')->isLoggedIn()) {
                Mage::getSingleton('customer/session')->setAfterAuthUrl(
                    Mage::getUrl($this->getFullActionName('/'))
                );
                $this->_redirect('customer/account/login');
                $this->setFlag('', Mage_Core_Controller_Varien_Action::FLAG_NO_DISPATCH, true);
            }
        }
    }

	public function indexAction()
    {
        echo $this->getLayout()->createBlock('nationpassport/message')->setTemplate('nationpassport/message.phtml')->toHtml();
    }

    public function validateNameAction()
    {
        $customer_id = $this->getRequest()->getParam('id');
        $customer_name = $this->getRequest()->getParam('name');

        $rs = array();
        if(!isset($customer_id) || !isset($customer_name))
        {
            $rs['result'] = false;
            return json_encode($rs);
        }

        $customer = Mage::getModel('customer/customer')->load($customer_id);

        if($customer != null && $customer->getId()){
            if(strcasecmp($customer->getName(), $customer_name) == 0){
                $rs['result'] = true;
            } else {
                $rs['result'] = false;
            }
            return json_encode($rs);
        } else {
            $rs['result'] = false;
            return json_encode($rs);
        }
    }

    public function sendAction()
    {
        $data = $this->getRequest()->getParams();

        $rs = array();
        if(!isset($data)){
            $rs['is_done'] = false;
            $rs['message'] = $this->__('<b>Error!</b> Something was wrong with this. Please check them again!');
            echo json_encode($rs);
            return;
        }

        if($data['sms_subject'] == null || $data['sms_content'] == null)
        {
            $rs['is_done'] = false;
            $rs['message'] = $this->__('<b>Error!</b> You have to fulfil the required fields.');
            echo json_encode($rs);
            return;
        }

        $list_customers = array();

        if(isset($data['recipient_id']) && $data['recipient_id'] != null){
            $list_customers = array_merge($list_customers, explode(',', $data['recipient_id']));
        }

        if(isset($data['without_points']) && $data['without_points'] != null){
            $list_customers = array_merge($list_customers, explode(',', $data['without_points']));
        }

        if(isset($data['with_points']) && $data['with_points'] != null){
            $list_customers = array_merge($list_customers, explode(',', $data['with_points']));
        }

        if(isset($data['without_trubox']) && $data['without_trubox'] != null){
            $list_customers = array_merge($list_customers, explode(',', $data['without_trubox']));
        }

        if(isset($data['new_people']) && $data['new_people'] != null){
            $list_customers = array_merge($list_customers, explode(',', $data['new_people']));
        }

        if(sizeof($list_customers) <= 0){
            $rs['is_done'] = false;
            $rs['message'] = $this->__('<b>Error!</b> You don\'t select any recipients. Please try again!');
            echo json_encode($rs);
            return;
        }

        $list_customers = array_unique($list_customers);

        $transactionSave = Mage::getModel('core/resource_transaction');
        $_rs = 0;
        $send_email = '';
        foreach ($list_customers as $id) {
            $customer = Mage::getModel('customer/customer')->load($id);
            if($customer != null && $customer->getId()){
                
                $message = Mage::getModel('nationpassport/message');
                $_data = array(
                    'customer_id'   => Mage::getSingleton('customer/session')->getCustomer()->getId(),
                    'recipient_id'  => $customer->getId(),
                    'subject'   => $data['sms_subject'],
                    'body'   => $data['sms_content'],
                );
                
                $message->setData($_data);
                $transactionSave->addObject($message);
                
                

                if($send_email == '')
                    $send_email .= $customer->getId();
                else
                    $send_email .= ','.$customer->getId();

                $_rs++;
            }
        }
        $transactionSave->save();

        if($_rs > 0){
            $count = Mage::helper('nationpassport')->getUnreadMessage();
            $rs['is_done'] = true;
            $rs['message'] = $this->__('<b>Success!</b> You have just sent %s messages', $_rs);
            $rs['html'] = $this->getLayout()->createBlock('nationpassport/message')->setData('total', $_rs)->setTemplate('nationpassport/message/ajax.phtml')->toHtml();
            $rs['count'] = $count;
            $rs['ids'] = $send_email;
            echo json_encode($rs);
            return;
        } else {
            $rs['is_done'] = false;
            $rs['message'] = $this->__('<b>Error!</b> The recipient doesn\'t exist. Please try again!');
            echo json_encode($rs);
            return;
        }
    }

    public function readMessageAction()
    {
        $message_id = $this->getRequest()->getParam('id');
        $message = Mage::getModel('nationpassport/message')->load($message_id);

        if($message != null && $message->getId())
        {
            $message->setData('status', Magestore_Nationpassport_Model_Status::STATUS_DISABLED);
            $message->save();
        }
    }

    public function sendEmailAction()
    {
        $customer_ids = $this->getRequest()->getParam('id');
        if($customer_ids != null){
            $ids = explode(',', $customer_ids);
            foreach ($ids as $id) {
                $customer = Mage::getModel('customer/customer')->load($id);
                if($customer != null && $customer->getId()){
                    /* Send email to recipient */
                    Mage::helper('nationpassport/message')->sendEmailNotificationToRecipient(
                        Mage::getSingleton('customer/session')->getCustomer()->getName(),
                        $customer
                    );
                    /* END Send email to recipient */
                }
            }
        }
    }

    public function searchSentMessagesAction()
    {
        $name = $this->getRequest()->getParam('name');

        if($name == ''){
            $messages = Mage::getModel('nationpassport/message')
                ->getCollection()
                ->addFieldToFilter('customer_id', Mage::getSingleton('customer/session')->getCustomer()->getId())
                ->setOrder('message_id', 'desc')
            ;
        } else {
            $collection = Mage::getModel('customer/customer')->getCollection()
                ->addAttributeToSelect('entity_id')
                ->addAttributeToSelect('firstname')
                ->addAttributeToSelect('lastname')
                ->setOrder('entity_id', 'desc');

            $collection->addAttributeToFilter(
                array(
                    array('attribute' => 'firstname', 'like' => '%' . $name . '%'),
                    array('attribute' => 'lastname', 'like' => '%' . $name . '%'),
                )
            );

            $customer_ids = $collection->getColumnValues('entity_id');

            $messages = Mage::getModel('nationpassport/message')
                ->getCollection()
                ->addFieldToFilter('customer_id', Mage::getSingleton('customer/session')->getCustomer()->getId())
                ->addFieldToFilter('recipient_id', array('in'    => $customer_ids))
                ->setOrder('message_id', 'desc')
            ;
        }

        echo $this->getLayout()->createBlock('nationpassport/message')->setData('messages', $messages)->setTemplate('nationpassport/message/search_sent.phtml')->toHtml();
    }

    public function searchInboxMessagesAction()
    {
        $name = $this->getRequest()->getParam('name');

        if($name == ''){
            $messages = Mage::getModel('nationpassport/message')
                ->getCollection()
                ->addFieldToFilter('recipient_id', Mage::getSingleton('customer/session')->getCustomer()->getId())
                ->setOrder('message_id', 'desc')
            ;
        } else {
            $collection = Mage::getModel('customer/customer')->getCollection()
                ->addAttributeToSelect('entity_id')
                ->addAttributeToSelect('firstname')
                ->addAttributeToSelect('lastname')
                ->setOrder('entity_id', 'desc');

            $collection->addAttributeToFilter(
                array(
                    array('attribute' => 'firstname', 'like' => '%' . $name . '%'),
                    array('attribute' => 'lastname', 'like' => '%' . $name . '%'),
                )
            );

            $customer_ids = $collection->getColumnValues('entity_id');

            $messages = Mage::getModel('nationpassport/message')
                ->getCollection()
                ->addFieldToFilter('recipient_id', Mage::getSingleton('customer/session')->getCustomer()->getId())
                ->addFieldToFilter('customer_id', array('in'    => $customer_ids))
                ->setOrder('message_id', 'desc')
            ;
        }

        echo $this->getLayout()->createBlock('nationpassport/message')->setData('messages', $messages)->setTemplate('nationpassport/message/search_inbox.phtml')->toHtml();
    }
}
