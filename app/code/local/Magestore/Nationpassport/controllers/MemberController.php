<?php

class Magestore_Nationpassport_MemberController extends Mage_Core_Controller_Front_Action
{
    /**
     * check customer is logged in
     */
    public function preDispatch()
    {
        parent::preDispatch();
        if (!$this->getRequest()->isDispatched()) {
            return;
        }
        $action = $this->getRequest()->getActionName();
        if ($action != 'policy' && $action != 'redirectLogin') {
            // Check customer authentication
            if (!Mage::getSingleton('customer/session')->isLoggedIn()) {
                Mage::getSingleton('customer/session')->setAfterAuthUrl(
                    Mage::getUrl($this->getFullActionName('/'))
                );
                $this->_redirect('customer/account/login');
                $this->setFlag('', Mage_Core_Controller_Varien_Action::FLAG_NO_DISPATCH, true);
            }
        }
    }

	public function indexAction()
    {
        echo $this->getLayout()->createBlock('nationpassport/message')->setTemplate('nationpassport/message.phtml')->toHtml();
    }

    public function findMemberAction()
    {
        $data = $this->getRequest()->getParams();
        $rs = array();
        if(!isset($data)){
            $rs['is_done'] = false;
            $rs['message'] = $this->__('<b>Error!</b> Something was wrong with this. Please check them again!');
            echo json_encode($rs);
            return;
        }

        if($data['member_name'] == null)
        {
            $rs['is_done'] = false;
            $rs['message'] = $this->__('<b>Error!</b> You have to enter the name to find the members.');
            echo json_encode($rs);
            return;
        }

        $collection = Mage::helper('nationpassport')->searchCustomer($data['member_name']);

        $rs['is_done'] = true;
        $rs['html'] = $this->getLayout()->createBlock('nationpassport/member')->setData('customers', $collection)->setTemplate('nationpassport/member/ajax.phtml')->toHtml();
        $rs['count'] = sizeof($collection);

        echo json_encode($rs);
        return;

    }

    public function readMessageAction()
    {
        $message_id = $this->getRequest()->getParam('id');
        $message = Mage::getModel('nationpassport/message')->load($message_id);

        if($message != null && $message->getId())
        {
            $message->setData('status', Magestore_Nationpassport_Model_Status::STATUS_DISABLED);
            $message->save();
        }
    }

    public function infoAction()
    {
        $this->loadLayout();

        $this->_title(Mage::helper('nationpassport')->__('Member Information'));

        $breadcrumbs = $this->getLayout()->getBlock("breadcrumbs");
        $breadcrumbs->addCrumb("home", array(
            "label" => $this->__("Home"),
            "title" => $this->__("Home"),
            "link"  => Mage::getBaseUrl()
        ));

        $breadcrumbs->addCrumb("member_info", array(
            "label" => $this->__("Member Information"),
            "title" => $this->__("TMember Information"),
        ));

        $this->renderLayout();
    }

    public function leaderboardAction()
    {
        $data = $this->getRequest()->getParams();

        if(!isset($data)){
            echo $this->__('<b>Error!</b> Something was wrong with this. Please check them again!');
            return;
        }

        $rank_array = Magestore_Nationpassport_Model_Status::getRankArray();
        if($data['rank'] == null || !in_array($data['rank'], $rank_array))
        {
            echo $this->__('<b>Error!</b> The param was wrong.');
            return;
        }

        $list = Mage::helper('nationpassport')->findLeaderBoars(
            $data['rank'],
            isset($data['new_member']) && $data['new_member'] != null ? $data['new_member'] : null
        );

        echo $this->getLayout()->createBlock('nationpassport/leaderboard')
            ->setData('list', $list)
            ->setTemplate('nationpassport/member/leader_board.phtml')
            ->toHtml();
    }
}
