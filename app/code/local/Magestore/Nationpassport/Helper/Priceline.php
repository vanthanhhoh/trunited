<?php

class Magestore_Nationpassport_Helper_Priceline extends Mage_Core_Helper_Abstract
{
    protected $_helper;

    public function getHelper()
    {
        if($this->_helper == null)
            $this->_helper = Mage::helper('nationpassport');

        return $this->_helper;
    }

    public function isEnable()
    {
        return $this->getHelper()->getConfigData('priceline_badge', 'enable');
    }

    public function getImage()
    {
        return Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA) . DS . 'nationpassport'. DS .$this->getHelper()->getConfigData('priceline_badge', 'image');
    }

    public function getPricelineUrl($customer_id = null)
    {
        $url = $this->getHelper()->getConfigData('priceline_badge', 'url');
        if($customer_id == null)
            $url = str_replace(
                '{{customer_id}}',
                Mage::getSingleton('customer/session')->getCustomer()->getId(),
                $url
            );
        else
            $url = str_replace(
                '{{customer_id}}',
                $customer_id,
                $url
            );

        return $url;
    }

    public function isShowImage()
    {
        if(!$this->isEnable())
            return null;

        $customer_id = Mage::getSingleton('customer/session')->getCustomer()->getId();

        $current_payout_period_id = Mage::helper('badges')->getCurrentPayoutPeriod();

        if($current_payout_period_id != null && $current_payout_period_id > 0)
        {
            $current_time = date('Y-m-d 00:00:00', time());
            $readConnection = Mage::getSingleton('core/resource')->getConnection('core_read');

            $query = "SELECT payout_period_id";
            $query .= " FROM ".Magestore_Badges_Helper_Data::TBL_PAYOUT_PERIOD_STATS."";
            $query .= " WHERE affiliate_renewal >= '$current_time' and customer_id = $customer_id and payout_period_id = $current_payout_period_id";
            $query .= " ORDER BY payout_period_id ASC";
            $query .= " LIMIT 1";

            $result = $readConnection->fetchOne($query);

            return $result;
        }

        return null;
    }

}
