<?php

class Magestore_Nationpassport_Helper_Message extends Mage_Core_Helper_Abstract
{
    const XML_PATH_EMAIL_ENABLE = 'nationpassport/email/enable';
    const XML_PATH_EMAIL_SENDER = 'nationpassport/email/sender';
    const XML_PATH_EMAIL_NOTIFICATION_TO_RECIPIENT = 'nationpassport/email/notify_message_recipient';


    public function sendEmailNotificationToRecipient($sender_name, $recipient)
    {
        $store = Mage::app()->getStore();
        if (!Mage::getStoreConfigFlag(self::XML_PATH_EMAIL_ENABLE, $store->getId())) {
            return $this;
        }

        $translate = Mage::getSingleton('core/translate');
        $translate->setTranslateInline(false);

        $name = $recipient->getName();

        $data = array(
            'store' => $store,
            'customer_name' => $recipient->getName(),
            'sender_name' => $sender_name,
            'login_url' => Mage::getUrl('customer/account/login'),
        );

        $email_path = Mage::getStoreConfig(self::XML_PATH_EMAIL_NOTIFICATION_TO_RECIPIENT, $store);

        Mage::getModel('core/email_template')
            ->setDesignConfig(array(
                'area' => 'frontend',
                'store' => Mage::app()->getStore()->getId()
            ))->sendTransactional(
                $email_path,
                Mage::getStoreConfig(self::XML_PATH_EMAIL_SENDER, $store->getId()),
                $recipient->getEmail(),
                $name,
                $data
            );

        $translate->setTranslateInline(true);

        return $this;
    }

}
