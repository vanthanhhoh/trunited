<?php

class Magestore_Nationpassport_Helper_Data extends Mage_Core_Helper_Abstract
{
    const TBL_PAYOUT_PERIOD_STATS = 'tr_payoutperiod_stats';
    const TBL_PAYOUT_PERIOD = 'tr_PayoutPeriod';
    const TBL_GENERALOGY = 'tr_Genealogy';

    const CACHE_TAG = 'block_html';
    const CACHE_BUSINESS_PERSONAL_POINT = 'bpp';
    const CACHE_BUSINESS_PERSONAL_POINT_NEW = 'bppn';
    const CACHE_BUSINESS_DEPTH_CREDIT = 'bdc';
    const CACHE_BUSINESS_DEPTH_CREDIT_NEW = 'bdcn';
    const CACHE_BUSINESS_TRUBOX_POINT = 'btp';
    const CACHE_BUSINESS_TRUBOX_POINT_NEW = 'btpn';
    const CACHE_BUSINESS_ON_HOLD_POINT = 'bohp';
    const CACHE_BUSINESS_ON_HOLD_POINT_NEW = 'bohpn';

    const CACHE_PERSONAL_POINT = 'pp';
    const CACHE_PERSONAL_POINT_NEW = 'ppn';
    const CACHE_DEPTH_CREDIT = 'dc';
    const CACHE_DEPTH_CREDIT_NEW = 'dcn';
    const CACHE_TRUBOX_POINT = 'tp';
    const CACHE_TRUBOX_POINT_NEW = 'tpn';
    const CACHE_ON_HOLD_POINT = 'ohp';
    const CACHE_ON_HOLD_POINT_NEW = 'ohpn';

    public function getConfigData($group, $field, $store = null)
    {
        return Mage::getStoreConfig('nationpassport/' . $group . '/' . $field, $store);
    }

    public function isEnableContest()
    {
        return $this->getConfigData('contest', 'enable');
    }

    public function getContentOfContest()
    {
        return $this->getConfigData('contest', 'content');
    }

    public function getTeamConfiguration()
    {
        $teams = $this->getConfigData('contest', 'team');
        if ($teams) {
            $team_data = unserialize($teams);
            if (is_array($team_data)) {
                $rs = array();
                foreach ($team_data as $team) {
                    $rs[] = array(
                        'team_id' => explode(',', $team['team_id']),
                        'team_name' => $team['team_name'],
                    );
                }
                return $rs;
            } else {
                return null;
            }
        }

        return null;
    }

    public function getNationPassportLabel()
    {
        return $this->__('Trunited Nation Passport');
    }

    public function getAffiliateRefer($customer_id)
    {
        $tracking = Mage::getModel('affiliateplus/tracking')->getCollection()
            ->addFieldToFilter('customer_id', $customer_id)
            ->setOrder('tracking_id', 'desc')
            ->getFirstItem();

        if ($tracking->getId()) {
            $affiliate = Mage::getModel('affiliateplus/account')->load($tracking->getAccountId());
            if ($affiliate->getId())
                return $affiliate;
            else
                return null;
        } else {
            return null;
        }
    }

    public function getTruGiftCardProduct()
    {
        $sku = Mage::helper('trugiftcard')->getTruGiftCardSku();
        $product = Mage::getModel('catalog/product')->loadByAttribute('sku', $sku);

        if ($product != null && $product->getId())
            return $product;
        else
            return null;
    }

    public function getAffiliatesFromCustomer($customer_id)
    {
        $ids = $this->getChildCustomer($customer_id);
        if ($ids == null || sizeof($ids) == 0)
            return array();

        $customers = Mage::getModel('customer/customer')
            ->getCollection()
            ->addAttributeToSelect('entity_id')
            ->addAttributeToFilter('entity_id', array('in' => $ids))
            ->setOrder('entity_id', 'desc');

        $customer_ids = $customers->getColumnValues('entity_id');

        $collection = Mage::getModel('affiliateplus/account')->getCollection()
            ->addFieldToFilter('customer_id', array('in' => $customer_ids))
            ->setOrder('name', 'asc');

        if (sizeof($collection) > 0)
            return $collection;
        else
            return null;
    }

    public function searchAffiliate($keyword, $level, $customer_id = null)
    {
        if ($customer_id == null)
            $customer_id = Mage::getSingleton('customer/session')->getCustomer()->getId();

        $ids = $this->getChildCustomer($customer_id, $level);
        if ($ids == null || sizeof($ids) == 0)
            return null;

        $collection = Mage::getModel('customer/customer')->getCollection()
            ->addAttributeToSelect('entity_id')
            ->addAttributeToSelect('firstname')
            ->addAttributeToSelect('lastname')
            ->addAttributeToFilter('entity_id', array('in' => $ids))
            ->setOrder('entity_id', 'desc');

        if ($keyword != null)
            $collection->addAttributeToFilter(
                array(
                    array('attribute' => 'firstname', 'like' => '%' . $keyword . '%'),
                    array('attribute' => 'lastname', 'like' => '%' . $keyword . '%'),
                )
            );

        $customer_ids = $collection->getColumnValues('entity_id');

        $collection = Mage::getModel('affiliateplus/account')->getCollection()
            ->addFieldToFilter('customer_id', array('in' => $customer_ids));

        if (sizeof($collection) > 0)
            return $collection;
        else
            return null;
    }

    public function getUnreadMessage()
    {
        $collection = Mage::getModel('nationpassport/message')
            ->getCollection()
            ->addFieldToFilter('recipient_id', Mage::getSingleton('customer/session')->getCustomer()->getId())
            ->addFieldToFilter('status', Magestore_Nationpassport_Model_Status::STATUS_ENABLED)
            ->setOrder('message_id', 'desc');

        return sizeof($collection);
    }

    public function searchCustomer($keyword, $customer_id = null)
    {
        if ($customer_id == null)
            $customer_id = Mage::getSingleton('customer/session')->getCustomer()->getId();

        $ids = $this->getRelatedCustomer($customer_id);
        if ($ids == null || sizeof($ids) == 0)
            return null;

        $collection = Mage::getModel('customer/customer')->getCollection()
            ->addAttributeToSelect('entity_id')
            ->addAttributeToSelect('firstname')
            ->addAttributeToSelect('lastname')
            ->addAttributeToSelect('name')
            ->addAttributeToSelect('email')
            ->addAttributeToSelect('is_active');

        $collection->addAttributeToFilter(
            array(
                array('attribute' => 'firstname', 'like' => '%' . $keyword . '%'),
                array('attribute' => 'lastname', 'like' => '%' . $keyword . '%'),
            )
        );

        $collection->addAttributeToFilter('firstname', array('nlike'    => '%SoComm%'));

        $collection->addAttributeToFilter('entity_id', array('in', $ids));

        return $collection;
    }

    public function getChildCustomer($customer_id, $level = null)
    {
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');

        if ($level != null)
            $query = 'SELECT PersonID FROM tr_Genealogy WHERE TreeRootID = ' . $customer_id . ' AND Level = ' . $level . ' ORDER BY
            Level asc';
        else
            $query = 'SELECT PersonID FROM tr_Genealogy WHERE TreeRootID = ' . $customer_id . ' AND Level > 0 ORDER BY Level asc';

        $ids = $readConnection->fetchCol($query);
        return $ids;
    }

    public function getRelatedCustomer($customer_id)
    {
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');

        $query = "SELECT TreeRootID, CASE WHEN Level = 0 THEN ParentId ELSE PersonID END as PersonID, ParentId, Level";
        $query .= " FROM tr_Genealogy WHERE TreeRootID = $customer_id";

        $rs = $readConnection->fetchAll($query);

        $result = array();
        if(sizeof($rs) > 0){
            foreach ($rs as $q) {
                $result[] = $q['PersonID'];
            }
        }

        return $result;
    }

    public function isShowPopupBadges()
    {
        if (!$this->getConfigData('general', 'enable_popup'))
            return false;

        $time_stamp = strtotime('first day of this month', time());
        $first_day_current_month = date('d', $time_stamp);
//        $current_date = date('d', time());
//        if($current_date != $first_day_current_month)
//            return false;

        $customer = Mage::getModel('customer/customer')->load(Mage::getSingleton('customer/session')->getCustomer()->getId());
        if ($customer == null || !$customer->getId())
            return false;

        $created_at = strtotime($customer->getCreatedAt());
        if (date('Y', $created_at) == date('Y', time()) && date('m', $created_at) == date('m', time()))
            return false;

        $dateStart = date('Y-m-d 00:00:00', $time_stamp);
        $dateEnd = date('Y-m-t 23:59:59', time());
        $is_opened = Mage::getModel('badges/popup')->getCollection()
            ->addFieldToFilter('customer_id', $customer->getId())
            ->addFieldToFilter('opened_time', array('from' => $dateStart, 'to' => $dateEnd))
            ->getFirstItem();

        if ($is_opened != null && $is_opened->getId())
            return false;

        return true;
    }

    public function getCoreResource()
    {
        return Mage::getSingleton('core/resource');
    }

    public function getReadConnection()
    {
        return $this->getCoreResource()->getConnection('core_read');
    }

    public function getWriteConnection()
    {
        return $this->getCoreResource()->getConnection('core_write');
    }

    public function findLeaderBoars($rank, $new_member = null)
    {
        $default_days = $this->getConfigData('general', 'leader_board_days');
        $current_payout_period_id = Mage::helper('badges')->getCurrentPayoutPeriod();
        $readConnection = $this->getReadConnection();

//        $current_payout_period_id = 11;

        $customer_table = $this->getCoreResource()->getTableName('customer_entity');
        $order_table = $this->getCoreResource()->getTableName('sales/order');
        $rewardpoints_transaction_table = $this->getCoreResource()->getTableName('rewardpoints/transaction');
        $customer_varchar_table = $this->getCoreResource()->getTableName('customer_entity_varchar');
        $badges_table = $this->getCoreResource()->getTableName('badges/badges');

        $firstnameAttr = Mage::getModel('eav/entity_attribute')->loadByCode('1', 'firstname');
        $lastnameAttr = Mage::getModel('eav/entity_attribute')->loadByCode('1', 'lastname');

        if ($rank == Magestore_Nationpassport_Model_Status::RANK_PERSONAL_POINTS) {
            if ($new_member === 'true') {
                $query = "SELECT pps.name, pps.badge, pps.personal_points, pps.customer_id,";
                $query .= " CONCAT(`ce1`.`value`, ' ', `ce2`.`value`) AS fullname,";
                $query .= " (SELECT hex_color FROM $badges_table WHERE name = pps.badge and is_title = 'Y' and status = 'active' ORDER BY badges_id DESC LIMIT 1 ) as hex_color,";
                $query .= " (SELECT image_name FROM $badges_table WHERE name = pps.badge and is_title = 'Y' and status = 'active' ORDER BY badges_id DESC LIMIT 1 ) as image_name";
                $query .= " FROM " . self::TBL_PAYOUT_PERIOD_STATS . " pps";
                $query .= " JOIN $customer_table c ON pps.customer_id = c.entity_id";
                $query .= " AND c.created_at >= DATE_SUB(NOW(), INTERVAL $default_days DAY)";
                $query .= " INNER JOIN $customer_varchar_table AS ce1 ON ce1.entity_id = pps.customer_id";
                $query .= " INNER JOIN $customer_varchar_table AS ce2 ON ce2.entity_id = pps.customer_id";
                $query .= " WHERE pps.payout_period_id = $current_payout_period_id AND pps.customer_id NOT IN (2401)";
                $query .= " AND pps.customer_id NOT BETWEEN 2963 AND 2974";
                $query .= " AND (ce1.entity_type_id = '1' AND ce2.entity_type_id = '1') AND (ce1.attribute_id = " . $firstnameAttr->getAttributeId() . ") AND(ce2.attribute_id = " . $lastnameAttr->getAttributeId() . ")";
                $query .= " ORDER BY pps.personal_points DESC";
                $query .= " LIMIT 12";

            } else {
                $query = "SELECT name, badge, personal_points, customer_id,";
                $query .= " CONCAT(`ce1`.`value`, ' ', `ce2`.`value`) AS fullname,";
                $query .= " (SELECT hex_color FROM $badges_table WHERE name = pps.badge and is_title = 'Y' and status = 'active' ORDER BY badges_id DESC LIMIT 1 ) as hex_color,";
                $query .= " (SELECT image_name FROM $badges_table WHERE name = pps.badge and is_title = 'Y' and status = 'active' ORDER BY badges_id DESC LIMIT 1 ) as image_name";
                $query .= " FROM " . self::TBL_PAYOUT_PERIOD_STATS . " pps";
                $query .= " INNER JOIN $customer_varchar_table AS ce1 ON ce1.entity_id = pps.customer_id";
                $query .= " INNER JOIN $customer_varchar_table AS ce2 ON ce2.entity_id = pps.customer_id";
                $query .= " WHERE payout_period_id = $current_payout_period_id AND customer_id NOT IN (2401)";
                $query .= " AND customer_id NOT BETWEEN 2963 AND 2974";
                $query .= " AND (ce1.entity_type_id = '1' AND ce2.entity_type_id = '1') AND (ce1.attribute_id = " . $firstnameAttr->getAttributeId() . ") AND(ce2.attribute_id = " . $lastnameAttr->getAttributeId() . ")";
                $query .= " ORDER BY personal_points DESC";
                $query .= " LIMIT 12";
            }
            $result = $readConnection->fetchAll($query);

            return sizeof($result) > 0 ? $result : null;

        } else if ($rank == Magestore_Nationpassport_Model_Status::RANK_DEPT_CREDIT) {

            if ($new_member === 'true') {
                $query = "SELECT pps.name, pps.badge, pps.depth_credit, pps.customer_id,";
                $query .= " CONCAT(`ce1`.`value`, ' ', `ce2`.`value`) AS fullname,";
                $query .= " (SELECT hex_color FROM $badges_table WHERE name = pps.badge and is_title = 'Y' and status = 'active' ORDER BY badges_id DESC LIMIT 1 ) as hex_color,";
                $query .= " (SELECT image_name FROM $badges_table WHERE name = pps.badge and is_title = 'Y' and status = 'active' ORDER BY badges_id DESC LIMIT 1 ) as image_name";
                $query .= " FROM " . self::TBL_PAYOUT_PERIOD_STATS . " pps";
                $query .= " JOIN $customer_table c ON pps.customer_id = c.entity_id";
                $query .= " AND c.created_at >= DATE_SUB(NOW(), INTERVAL $default_days DAY)";
                $query .= " INNER JOIN $customer_varchar_table AS ce1 ON ce1.entity_id = pps.customer_id";
                $query .= " INNER JOIN $customer_varchar_table AS ce2 ON ce2.entity_id = pps.customer_id";
                $query .= " WHERE pps.payout_period_id = $current_payout_period_id AND pps.customer_id NOT IN (2401)";
                $query .= " AND pps.customer_id NOT BETWEEN 2963 AND 2974";
                $query .= " AND (ce1.entity_type_id = '1' AND ce2.entity_type_id = '1') AND (ce1.attribute_id = " . $firstnameAttr->getAttributeId() . ") AND(ce2.attribute_id = " . $lastnameAttr->getAttributeId() . ")";
                $query .= " ORDER BY pps.depth_credit DESC, pps.personal_points DESC";
                $query .= " LIMIT 12";
            } else {
                $query = "SELECT name, badge, depth_credit, customer_id,";
                $query .= " CONCAT(`ce1`.`value`, ' ', `ce2`.`value`) AS fullname,";
                $query .= " (SELECT hex_color FROM $badges_table WHERE name = pps.badge and is_title = 'Y' and status = 'active' ORDER BY badges_id DESC LIMIT 1 ) as hex_color,";
                $query .= " (SELECT image_name FROM $badges_table WHERE name = pps.badge and is_title = 'Y' and status = 'active' ORDER BY badges_id DESC LIMIT 1 ) as image_name";
                $query .= " FROM " . self::TBL_PAYOUT_PERIOD_STATS . " pps";
                $query .= " INNER JOIN $customer_varchar_table AS ce1 ON ce1.entity_id = pps.customer_id";
                $query .= " INNER JOIN $customer_varchar_table AS ce2 ON ce2.entity_id = pps.customer_id";
                $query .= " WHERE payout_period_id = $current_payout_period_id AND customer_id NOT IN (2401)";
                $query .= " AND customer_id NOT BETWEEN 2963 AND 2974";
                $query .= " AND (ce1.entity_type_id = '1' AND ce2.entity_type_id = '1') AND (ce1.attribute_id = " . $firstnameAttr->getAttributeId() . ") AND(ce2.attribute_id = " . $lastnameAttr->getAttributeId() . ")";
                $query .= " ORDER BY depth_credit DESC, personal_points DESC";
                $query .= " LIMIT 12";
            }

            $result = $readConnection->fetchAll($query);

            return sizeof($result) > 0 ? $result : null;

        } else if ($rank == Magestore_Nationpassport_Model_Status::RANK_AVG_TEAM_POINTS) {

            $query = "SELECT g.treerootid, pps.name, pps.badge, pps.depth_credit, pps.customer_id,";
            $query .= " ROUND(AVG(g.points), 1) as avg_team_points,";
            $query .= " COUNT(*) as num_people";
            $query .= " FROM " . self::TBL_GENERALOGY . " g";
            $query .= " JOIN " . self::TBL_PAYOUT_PERIOD_STATS . " pps ON g.treerootid = pps.customer_id";
            $query .= " WHERE pps.payout_period_id = $current_payout_period_id AND g.level <= pps.depth_credit";
            $query .= " AND g.Points > 0 AND g.treerootid NOT IN (2401)";
            $query .= " AND g.treerootid NOT BETWEEN 2963 AND 2974 AND pps.depth_credit >= 2";
            $query .= " GROUP BY g.treerootid, pps.name, pps.badge, pps.depth_credit";
            $query .= " ORDER BY avg_team_points DESC, num_people DESC";
            $query .= " LIMIT 12";

            $result = $readConnection->fetchAll($query);

            if (sizeof($result) == 0)
                return null;

            return $result;
        } else if ($rank == Magestore_Nationpassport_Model_Status::RANK_TRUBOX_POINTS) {

            if ($new_member === 'true') {
                $query = "SELECT pps.name, pps.badge, o.rewardpoints_earn, pps.customer_id,";
                $query .= " CONCAT(`ce1`.`value`, ' ', `ce2`.`value`) AS fullname,";
                $query .= " (SELECT hex_color FROM $badges_table WHERE name = pps.badge and is_title = 'Y' and status = 'active' ORDER BY badges_id DESC LIMIT 1 ) as hex_color,";
                $query .= " (SELECT image_name FROM $badges_table WHERE name = pps.badge and is_title = 'Y' and status = 'active' ORDER BY badges_id DESC LIMIT 1 ) as image_name";
                $query .= " FROM " . self::TBL_PAYOUT_PERIOD_STATS . " pps";
                $query .= " JOIN " . self::TBL_PAYOUT_PERIOD . " pp ON pps.payout_period_id = pp.payout_period_id";
                $query .= " JOIN $order_table o ON pps.customer_id = o.customer_id";
                $query .= " AND o.created_by = 1 AND o.created_at BETWEEN pp.start_date AND pp.end_date";
                $query .= " JOIN $customer_table c ON pps.customer_id = c.entity_id";
                $query .= " AND c.created_at >= DATE_SUB(NOW(), INTERVAL $default_days DAY)";
                $query .= " INNER JOIN $customer_varchar_table AS ce1 ON ce1.entity_id = pps.customer_id";
                $query .= " INNER JOIN $customer_varchar_table AS ce2 ON ce2.entity_id = pps.customer_id";
                $query .= " WHERE pps.payout_period_id = $current_payout_period_id";
                $query .= " AND pps.customer_id NOT IN (2401) AND pps.customer_id NOT BETWEEN 2963 AND 2974";
                $query .= " AND o.status NOT IN ('Canceled', 'Closed')";
                $query .= " AND (ce1.entity_type_id = '1' AND ce2.entity_type_id = '1') AND (ce1.attribute_id = " . $firstnameAttr->getAttributeId() . ") AND(ce2.attribute_id = " . $lastnameAttr->getAttributeId() . ")";
                $query .= " ORDER BY o.rewardpoints_earn DESC";
                $query .= " LIMIT 12";
            } else {
                $query = "SELECT pps.name, pps.badge, o.rewardpoints_earn, pps.customer_id,";
                $query .= " CONCAT(`ce1`.`value`, ' ', `ce2`.`value`) AS fullname,";
                $query .= " (SELECT hex_color FROM $badges_table WHERE name = pps.badge and is_title = 'Y' and status = 'active' ORDER BY badges_id DESC LIMIT 1 ) as hex_color,";
                $query .= " (SELECT image_name FROM $badges_table WHERE name = pps.badge and is_title = 'Y' and status = 'active' ORDER BY badges_id DESC LIMIT 1 ) as image_name";
                $query .= " FROM " . self::TBL_PAYOUT_PERIOD_STATS . " pps";
                $query .= " JOIN " . self::TBL_PAYOUT_PERIOD . " pp ON pps.payout_period_id = pp.payout_period_id";
                $query .= " JOIN $order_table o ON pps.customer_id = o.customer_id";
                $query .= " AND o.created_by = 1 AND o.created_at BETWEEN pp.start_date AND pp.end_date";
                $query .= " INNER JOIN $customer_varchar_table AS ce1 ON ce1.entity_id = pps.customer_id";
                $query .= " INNER JOIN $customer_varchar_table AS ce2 ON ce2.entity_id = pps.customer_id";
                $query .= " WHERE pps.payout_period_id = $current_payout_period_id";
                $query .= " AND pps.customer_id NOT IN (2401) AND pps.customer_id NOT BETWEEN 2963 AND 2974";
                $query .= " AND o.status NOT IN ('Canceled', 'Closed')";
                $query .= " AND (ce1.entity_type_id = '1' AND ce2.entity_type_id = '1') AND (ce1.attribute_id = " . $firstnameAttr->getAttributeId() . ") AND(ce2.attribute_id = " . $lastnameAttr->getAttributeId() . ")";
                $query .= " ORDER BY o.rewardpoints_earn DESC";
                $query .= " LIMIT 12";
            }

            $result = $readConnection->fetchAll($query);

            return sizeof($result) > 0 ? $result : null;

        } else if ($rank == Magestore_Nationpassport_Model_Status::RANK_ON_HOLD_POINTS) {

            if ($new_member === 'true') {
                $query = "SELECT  pps.name, pps.badge, SUM(rpt.hold_point) as on_hold_points, pps.customer_id,";
                $query .= " CONCAT(`ce1`.`value`, ' ', `ce2`.`value`) AS fullname,";
                $query .= " (SELECT hex_color FROM $badges_table WHERE name = pps.badge and is_title = 'Y' and status = 'active' ORDER BY badges_id DESC LIMIT 1 ) as hex_color,";
                $query .= " (SELECT image_name FROM $badges_table WHERE name = pps.badge and is_title = 'Y' and status = 'active' ORDER BY badges_id DESC LIMIT 1 ) as image_name";
                $query .= " FROM " . self::TBL_PAYOUT_PERIOD_STATS . " pps";
                $query .= " JOIN $customer_table c ON pps.customer_id = c.entity_id ";
                $query .= " AND c.created_at >= DATE_SUB(NOW(), INTERVAL $default_days DAY)";
                $query .= " JOIN $rewardpoints_transaction_table rpt ON pps.customer_id = rpt.customer_id ";
                $query .= " AND rpt.is_on_hold = 1 AND rpt.status = 2 AND rpt.action_type = 11";
                $query .= " INNER JOIN $customer_varchar_table AS ce1 ON ce1.entity_id = pps.customer_id";
                $query .= " INNER JOIN $customer_varchar_table AS ce2 ON ce2.entity_id = pps.customer_id";
                $query .= " WHERE pps.payout_period_id = $current_payout_period_id";
                $query .= " AND pps.customer_id NOT IN (2401) AND pps.customer_id NOT BETWEEN 2963 AND 2974";
                $query .= " AND (ce1.entity_type_id = '1' AND ce2.entity_type_id = '1') AND (ce1.attribute_id = " . $firstnameAttr->getAttributeId() . ") AND(ce2.attribute_id = " . $lastnameAttr->getAttributeId() . ")";
                $query .= " GROUP BY pps.name, pps.badge";
                $query .= " ORDER BY on_hold_points DESC";
                $query .= " LIMIT 12";
            } else {
                $query = "SELECT  pps.name, pps.badge, SUM(rpt.hold_point) as on_hold_points, pps.customer_id,";
                $query .= " CONCAT(`ce1`.`value`, ' ', `ce2`.`value`) AS fullname,";
                $query .= " (SELECT hex_color FROM $badges_table WHERE name = pps.badge and is_title = 'Y' and status = 'active' ORDER BY badges_id DESC LIMIT 1 ) as hex_color,";
                $query .= " (SELECT image_name FROM $badges_table WHERE name = pps.badge and is_title = 'Y' and status = 'active' ORDER BY badges_id DESC LIMIT 1 ) as image_name";
                $query .= " FROM " . self::TBL_PAYOUT_PERIOD_STATS . " pps";
                $query .= " JOIN trrewardpoints_transaction rpt ON pps.customer_id = rpt.customer_id ";
                $query .= " AND rpt.is_on_hold = 1 AND rpt.status = 2 AND rpt.action_type = 11";
                $query .= " INNER JOIN $customer_varchar_table AS ce1 ON ce1.entity_id = pps.customer_id";
                $query .= " INNER JOIN $customer_varchar_table AS ce2 ON ce2.entity_id = pps.customer_id";
                $query .= " WHERE pps.payout_period_id = $current_payout_period_id";
                $query .= " AND pps.customer_id NOT IN (2401) AND pps.customer_id NOT BETWEEN 2963 AND 2974";
                $query .= " AND (ce1.entity_type_id = '1' AND ce2.entity_type_id = '1') AND (ce1.attribute_id = " . $firstnameAttr->getAttributeId() . ") AND(ce2.attribute_id = " . $lastnameAttr->getAttributeId() . ")";
                $query .= " GROUP BY pps.name, pps.badge";
                $query .= " ORDER BY on_hold_points DESC";
                $query .= " LIMIT 12";

            }

            $result = $readConnection->fetchAll($query);

            return sizeof($result) > 0 ? $result : null;
        }

        return null;
    }

    public function findBusinessLeaderBoars($rank, $keyword = null)
    {
        $current_payout_period_id = Mage::helper('badges')->getCurrentPayoutPeriod();
        $customer_id = Mage::getSingleton('customer/session')->getCustomer()->getId();
        $readConnection = $this->getReadConnection();

//        $current_payout_period_id = 11;

        $order_table = $this->getCoreResource()->getTableName('sales/order');
        $badges_table = $this->getCoreResource()->getTableName('badges/badges');
        $rewardpoints_transaction_table = $this->getCoreResource()->getTableName('rewardpoints/transaction');
        $customer_varchar_table = $this->getCoreResource()->getTableName('customer_entity_varchar');

        $customer_ids = null;

        $firstnameAttr = Mage::getModel('eav/entity_attribute')->loadByCode('1', 'firstname');
        $lastnameAttr = Mage::getModel('eav/entity_attribute')->loadByCode('1', 'lastname');

        if ($rank == Magestore_Nationpassport_Model_Status::RANK_PERSONAL_POINTS) {
            if ($keyword != null) {

                $collection = Mage::getModel('customer/customer')->getCollection()
                    ->addAttributeToSelect('entity_id')
                    ->addAttributeToSelect('firstname')
                    ->addAttributeToSelect('lastname')
                    ->addAttributeToFilter(
                        array(
                            array('attribute' => 'firstname', 'like' => '%' . $keyword . '%'),
                            array('attribute' => 'lastname', 'like' => '%' . $keyword . '%'),
                        )
                    )
                    ->setOrder('entity_id', 'desc');

                $ids = $collection->getColumnValues('entity_id');
                if (sizeof($ids) == 0)
                    return null;

                $customer_ids = implode(',', $ids);
            }

            $query = "SELECT pps.name, pps.badge, pps.personal_points, pps.customer_id,";
            $query .= " CONCAT(`ce1`.`value`, ' ', `ce2`.`value`) AS fullname,";
            $query .= " (SELECT hex_color FROM $badges_table WHERE name = pps.badge and is_title = 'Y' and status = 'active' ORDER BY badges_id DESC LIMIT 1 ) as hex_color,";
            $query .= " (SELECT image_name FROM $badges_table WHERE name = pps.badge and is_title = 'Y' and status = 'active' ORDER BY badges_id DESC LIMIT 1 ) as image_name";
            $query .= " FROM " . self::TBL_PAYOUT_PERIOD_STATS . " pps";
            $query .= " INNER JOIN $customer_varchar_table AS ce1 ON ce1.entity_id = pps.customer_id";
            $query .= " INNER JOIN $customer_varchar_table AS ce2 ON ce2.entity_id = pps.customer_id";
            $query .= " WHERE pps.payout_period_id = $current_payout_period_id AND pps.customer_id NOT IN (2401)";
            $query .= " AND pps.customer_id NOT BETWEEN 2963 AND 2974";
            $query .= " AND (ce1.entity_type_id = '1' AND ce2.entity_type_id = '1') AND (ce1.attribute_id = " . $firstnameAttr->getAttributeId() . ") AND(ce2.attribute_id = " . $lastnameAttr->getAttributeId() . ")";

            if ($customer_ids != null) {
                $query .= " AND pps.customer_id IN ($customer_ids)";
            }

            $query .= " AND pps.customer_id IN (SELECT PersonID FROM " . self::TBL_GENERALOGY . " WHERE TreeRootID = $customer_id AND Level > 0 ORDER BY Level asc )";
            $query .= " ORDER BY personal_points DESC";

            $result = $readConnection->fetchAll($query);
            return sizeof($result) > 0 ? $result : null;

        } else if ($rank == Magestore_Nationpassport_Model_Status::RANK_DEPT_CREDIT) {
            if ($keyword != null) {

                $collection = Mage::getModel('customer/customer')->getCollection()
                    ->addAttributeToSelect('entity_id')
                    ->addAttributeToSelect('firstname')
                    ->addAttributeToSelect('lastname')
                    ->addAttributeToFilter(
                        array(
                            array('attribute' => 'firstname', 'like' => '%' . $keyword . '%'),
                            array('attribute' => 'lastname', 'like' => '%' . $keyword . '%'),
                        )
                    )
                    ->setOrder('entity_id', 'desc');

                $ids = $collection->getColumnValues('entity_id');
                if (sizeof($ids) == 0)
                    return null;

                $customer_ids = implode(',', $ids);
            }

            $query = "SELECT name, badge, depth_credit, customer_id,";
            $query .= " CONCAT(`ce1`.`value`, ' ', `ce2`.`value`) AS fullname,";
            $query .= "(SELECT hex_color FROM $badges_table WHERE name = badge and is_title = 'Y' and status = 'active' ORDER BY badges_id DESC LIMIT 1 ) as hex_color,";
            $query .= "(SELECT image_name FROM $badges_table WHERE name = badge and is_title = 'Y' and status = 'active' ORDER BY badges_id DESC LIMIT 1 ) as image_name";
            $query .= " FROM " . self::TBL_PAYOUT_PERIOD_STATS . " pps";
            $query .= " INNER JOIN $customer_varchar_table AS ce1 ON ce1.entity_id = pps.customer_id";
            $query .= " INNER JOIN $customer_varchar_table AS ce2 ON ce2.entity_id = pps.customer_id";
            $query .= " WHERE payout_period_id = $current_payout_period_id AND customer_id NOT IN (2401)";
            $query .= " AND customer_id NOT BETWEEN 2963 AND 2974";
            $query .= " AND (ce1.entity_type_id = '1' AND ce2.entity_type_id = '1') AND (ce1.attribute_id = " . $firstnameAttr->getAttributeId() . ") AND(ce2.attribute_id = " . $lastnameAttr->getAttributeId() . ")";
            if ($customer_ids != null) {
                $query .= " AND pps.customer_id IN ($customer_ids)";
            }
            $query .= " AND customer_id IN (SELECT PersonID FROM " . self::TBL_GENERALOGY . " WHERE TreeRootID = $customer_id AND Level > 0 ORDER BY Level asc )";
            $query .= " ORDER BY depth_credit DESC, personal_points DESC";

            $result = $readConnection->fetchAll($query);
            return sizeof($result) > 0 ? $result : null;

        } else if ($rank == Magestore_Nationpassport_Model_Status::RANK_AVG_TEAM_POINTS) {

            $query = "SELECT g.treerootid, pps.name, pps.badge, pps.depth_credit, pps.customer_id,";
            $query .= " ROUND(AVG(g.points), 1) as avg_team_points,";
            $query .= " COUNT(*) as num_people";
            $query .= " FROM " . self::TBL_GENERALOGY . " g";
            $query .= " JOIN " . self::TBL_PAYOUT_PERIOD_STATS . " pps ON g.treerootid = pps.customer_id";
            $query .= " WHERE pps.payout_period_id = $current_payout_period_id AND g.level <= pps.depth_credit";
            $query .= " AND g.Points > 0 AND g.treerootid NOT IN (2401)";
            $query .= " AND g.treerootid NOT BETWEEN 2963 AND 2974 AND pps.depth_credit >= 2";
            $query .= " GROUP BY g.treerootid, pps.name, pps.badge, pps.depth_credit";
            $query .= " ORDER BY avg_team_points DESC, num_people DESC";

            $result = $readConnection->fetchAll($query);

            if (sizeof($result) == 0)
                return null;

            return $result;
        } else if ($rank == Magestore_Nationpassport_Model_Status::RANK_TRUBOX_POINTS) {

            if ($keyword != null) {
                $collection = Mage::getModel('customer/customer')->getCollection()
                    ->addAttributeToSelect('entity_id')
                    ->addAttributeToSelect('firstname')
                    ->addAttributeToSelect('lastname')
                    ->addAttributeToFilter(
                        array(
                            array('attribute' => 'firstname', 'like' => '%' . $keyword . '%'),
                            array('attribute' => 'lastname', 'like' => '%' . $keyword . '%'),
                        )
                    )
                    ->setOrder('entity_id', 'desc');

                $ids = $collection->getColumnValues('entity_id');
                if (sizeof($ids) == 0)
                    return null;

                $customer_ids = implode(',', $ids);
            }

            $query = "SELECT pps.name, pps.badge, o.rewardpoints_earn, pps.customer_id,";
            $query .= " CONCAT(`ce1`.`value`, ' ', `ce2`.`value`) AS fullname,";
            $query .= "(SELECT hex_color FROM $badges_table WHERE name = pps.badge and is_title = 'Y' and status = 'active' ORDER BY badges_id DESC LIMIT 1 ) as hex_color,";
            $query .= "(SELECT image_name FROM $badges_table WHERE name = pps.badge and is_title = 'Y' and status = 'active' ORDER BY badges_id DESC LIMIT 1 ) as image_name";
            $query .= " FROM " . self::TBL_PAYOUT_PERIOD_STATS . " pps";
            $query .= " JOIN " . self::TBL_PAYOUT_PERIOD . " pp ON pps.payout_period_id = pp.payout_period_id";
            $query .= " JOIN $order_table o ON pps.customer_id = o.customer_id";
            $query .= " AND o.created_by = 1 AND o.created_at BETWEEN pp.start_date AND pp.end_date";
            $query .= " INNER JOIN $customer_varchar_table AS ce1 ON ce1.entity_id = pps.customer_id";
            $query .= " INNER JOIN $customer_varchar_table AS ce2 ON ce2.entity_id = pps.customer_id";
            $query .= " WHERE pps.payout_period_id = $current_payout_period_id";
            $query .= " AND (ce1.entity_type_id = '1' AND ce2.entity_type_id = '1') AND (ce1.attribute_id = " . $firstnameAttr->getAttributeId() . ") AND(ce2.attribute_id = " . $lastnameAttr->getAttributeId() . ")";
            if ($customer_ids != null) {
                $query .= " AND pps.customer_id IN ($customer_ids)";
            }
            $query .= " AND pps.customer_id NOT IN (2401) AND pps.customer_id NOT BETWEEN 2963 AND 2974";
            $query .= " AND pps.customer_id IN (SELECT PersonID FROM " . self::TBL_GENERALOGY . " WHERE TreeRootID = $customer_id AND Level > 0 ORDER BY Level asc )";
            $query .= " AND o.status NOT IN ('Canceled', 'Closed')";
            $query .= " ORDER BY o.rewardpoints_earn DESC";

            $result = $readConnection->fetchAll($query);
            return sizeof($result) > 0 ? $result : null;

        } else if ($rank == Magestore_Nationpassport_Model_Status::RANK_ON_HOLD_POINTS) {

            if ($keyword != null) {
                $collection = Mage::getModel('customer/customer')->getCollection()
                    ->addAttributeToSelect('entity_id')
                    ->addAttributeToSelect('firstname')
                    ->addAttributeToSelect('lastname')
                    ->addAttributeToFilter(
                        array(
                            array('attribute' => 'firstname', 'like' => '%' . $keyword . '%'),
                            array('attribute' => 'lastname', 'like' => '%' . $keyword . '%'),
                        )
                    )
                    ->setOrder('entity_id', 'desc');

                $ids = $collection->getColumnValues('entity_id');
                if (sizeof($ids) == 0)
                    return null;

                $customer_ids = implode(',', $ids);
            }

            $query = "SELECT  pps.name, pps.badge, SUM(rpt.hold_point) as on_hold_points, pps.customer_id,";
            $query .= " CONCAT(`ce1`.`value`, ' ', `ce2`.`value`) AS fullname,";
            $query .= "(SELECT hex_color FROM $badges_table WHERE name = pps.badge and is_title = 'Y' and status = 'active' ORDER BY badges_id DESC LIMIT 1 ) as hex_color,";
            $query .= "(SELECT image_name FROM $badges_table WHERE name = pps.badge and is_title = 'Y' and status = 'active' ORDER BY badges_id DESC LIMIT 1 ) as image_name";
            $query .= " FROM " . self::TBL_PAYOUT_PERIOD_STATS . " pps";
            $query .= " JOIN $rewardpoints_transaction_table rpt ON pps.customer_id = rpt.customer_id ";
            $query .= " AND rpt.is_on_hold = 1 AND rpt.status = 2 AND rpt.action_type = 11";
            $query .= " INNER JOIN $customer_varchar_table AS ce1 ON ce1.entity_id = pps.customer_id";
            $query .= " INNER JOIN $customer_varchar_table AS ce2 ON ce2.entity_id = pps.customer_id";
            $query .= " WHERE pps.payout_period_id = $current_payout_period_id";
            $query .= " AND (ce1.entity_type_id = '1' AND ce2.entity_type_id = '1') AND (ce1.attribute_id = " . $firstnameAttr->getAttributeId() . ") AND(ce2.attribute_id = " . $lastnameAttr->getAttributeId() . ")";
            if ($customer_ids != null) {
                $query .= " AND pps.customer_id IN ($customer_ids)";
            }
            $query .= " AND pps.customer_id NOT IN (2401) AND pps.customer_id NOT BETWEEN 2963 AND 2974";
            $query .= " AND pps.customer_id IN (SELECT PersonID FROM " . self::TBL_GENERALOGY . " WHERE TreeRootID = $customer_id AND Level > 0 ORDER BY Level asc )";
            $query .= " GROUP BY pps.name, pps.badge";
            $query .= " ORDER BY on_hold_points DESC";

            $result = $readConnection->fetchAll($query);
            return sizeof($result) > 0 ? $result : null;

        }

        return null;
    }

}
