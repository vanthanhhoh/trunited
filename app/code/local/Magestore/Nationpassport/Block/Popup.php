<?php

class Magestore_Nationpassport_Block_Popup extends Mage_Core_Block_Template
{
	public function _prepareLayout(){
		return parent::_prepareLayout();
	}

    public function getCustomerId()
    {
        return Mage::getSingleton('customer/session')->getCustomer()->getId();
    }

    public function getCurrentCustomer()
    {
        return  Mage::getModel('customer/customer')->load(
            $this->getCustomerId()
        );
    }

    public function getBalance()
    {
        $account = Mage::getModel('affiliateplus/account')->load($this->getCustomerId(), 'customer_id');
        if($account != null && $account->getId()){
            $account_id = $account->getId();
            $resource = Mage::getSingleton('core/resource');
            $readConnection = $resource->getConnection('core_read');
            $tableName = $resource->getTableName('affiliateplus/transaction');

            $end_date = date('Y-m-d 23:59:59', time());
            $start_date = date("Y-m-d 00:00:00", strtotime("first day of this month"));

            $query = "SELECT SUM(commission) AS `affiliate_income`";
            $query .= " FROM $tableName";

            $type = Magestore_Affiliateplus_Model_Type::TRANSACTION_TYPE_AFFILIATE_CASH_PAYOUT;

            if(date('Y', time()) < 2018){
                $query .= " WHERE (`account_id` = $account_id) AND (`type` = '10')";
            } else {
                $query .= " WHERE (`account_id` = $account_id) AND (`type` = '$type')";
            }

            $query .= " AND (`created_time` <= '$end_date') AND (`created_time` >= '$start_date')";
            $query .= " GROUP BY `account_id`";
            
            $affiliate_income = $readConnection->fetchCol($query);

            if(sizeof($affiliate_income) > 0)
                return Mage::helper('core')->currency($affiliate_income[0], true, false);
            else
                return Mage::helper('core')->currency(0, true, false);
        } else
            return Mage::helper('core')->currency(0, true, false);
    }

    public function getTruWalletCredit()
    {
        $account = Mage::getModel('truwallet/customer')->load($this->getCustomerId(), 'customer_id');
        return Mage::helper('core')->currency($account->getTruwalletCredit(), true, false);

    }

    public function getTruGiftCardCredit()
    {
        $account = Mage::getModel('trugiftcard/customer')->load($this->getCustomerId(), 'customer_id');
        return Mage::helper('core')->currency($account->getTrugiftcardCredit(), true, false);
    }

    public function getCustomerStats()
    {
        return Mage::helper('badges')->getCustomerStats(
            $this->getCustomerId()
            ,true
        );
    }

    public function getCustomerBadgesHistory()
    {
        return Mage::helper('badges')->getCustomerBadgesHistory(
            $this->getCustomerId()
            ,true
        );
    }

    public function getImagePath($image_name)
    {
        return Mage::helper('badges')->getImagePath(
            $image_name
        );
    }


}
