<?php
/**
 * Created by PhpStorm.
 * User: longvuxuan
 * Date: 8/30/17
 * Time: 11:21 AM
 */

class Magestore_Nationpassport_Block_Adminhtml_System_Config_Form_Fieldset_Team extends
    Mage_Adminhtml_Block_System_Config_Form_Field_Array_Abstract
{
    public function _prepareToRender()
    {
        $this->addColumn('team_id', array(
            'label' => Mage::helper('nationpassport')->__('District'),
            'style' => 'width:300px',
        ));
        $this->addColumn('team_name', array(
            'label' => Mage::helper('nationpassport')->__('Team Name'),
            'style' => 'width:300px',
        ));
        $this->_addAfter = false;
        $this->_addButtonLabel = Mage::helper('nationpassport')->__('Add');
    }

}
