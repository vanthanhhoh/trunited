<?php

class Magestore_Nationpassport_Block_Member extends Mage_Core_Block_Template
{
	public function _prepareLayout(){
		return parent::_prepareLayout();
	}

    public function getFindMemberAction()
    {
        return $this->getUrl('*/member/findMember');
    }

    public function getMemberInfo($customer_id)
    {
        return $this->getUrl('*/member/info', array('id' => base64_encode($customer_id)));
    }

    public function getCustomerId()
    {
        return base64_decode($this->getRequest()->getParam('id'));
    }

    public function getCurrentCustomer()
    {
        return  Mage::getModel('customer/customer')->load(
            $this->getCustomerId()
        );
    }

    public function getAffiliateRefer()
    {
        return Mage::helper('nationpassport')->getAffiliateRefer(
            $this->getCustomerId()
        );
    }

    public function getBalance()
    {
        $account = Mage::getModel('affiliateplus/account')->load($this->getCustomerId(), 'customer_id');
        if($account != null && $account->getId())
            return Mage::helper('core')->currency($account->getBalance(), true, false);
        else
            return Mage::helper('core')->currency(0, true, false);
    }

    public function getTruWalletCredit()
    {
        $account = Mage::getModel('truwallet/customer')->load($this->getCustomerId(), 'customer_id');
        return Mage::helper('core')->currency($account->getTruwalletCredit(), true, false);

    }

    public function getTruGiftCardCredit()
    {
        $account = Mage::getModel('trugiftcard/customer')->load($this->getCustomerId(), 'customer_id');
        return Mage::helper('core')->currency($account->getTrugiftcardCredit(), true, false);
    }

    public function getCustomerStats()
    {
        return Mage::helper('badges')->getCustomerStats(
            $this->getCustomerId()
        );
    }

    public function getBadgeFromStats($badge_name)
    {
        return Mage::helper('badges')->getBadgeFromStats(
            $badge_name
        );
    }

    public function getCustomerBadgesHistory()
    {
        return Mage::helper('badges')->getCustomerBadgesHistory(
            $this->getCustomerId()
        );
    }

    public function getImagePath($image_name)
    {
        return Mage::helper('badges')->getImagePath(
            $image_name
        );
    }


}
