<?php

class Magestore_Nationpassport_Block_Message extends Mage_Core_Block_Template
{
    protected $_customers;
    protected $_withoutPoints;
    protected $_withPoints;
    protected $_withoutTrubox;
    protected $_newPeople;

	public function _prepareLayout(){
        if($this->_customers == null){
            $this->_customers = $this->getCollectCustomers();

            if(sizeof($this->_customers) > 0)
            {
                $g1 = $g2 = $g3 = $g4 =array();

                foreach ($this->_customers as $c) {
                    $rewardAccount = Mage::getModel('rewardpoints/customer')->load($c->getId(), 'customer_id');

                    if($rewardAccount->getPointBalance() <= 0){
                        if(!in_array($c->getId(), $g1))
                            $g1[] = $c->getId();
                    } else {
                        if(!in_array($c->getId(), $g2))
                            $g2[] = $c->getId();
                    }

                    if($this->compareExpireDate(strtotime($c->getCreatedAt()), time()) <= 30){
                        if(!in_array($c->getId(), $g4))
                            $g4[] = $c->getId();
                    }

                    $truBox = Mage::getModel('trubox/trubox')->getCollection()
                        ->addFieldToFilter('status', 'open')
                        ->addFieldToFilter('customer_id', $c->getId())
                        ->getFirstItem();

                    if($truBox != null && $truBox->getId())
                    {
                        $items = Mage::getModel('trubox/item')
                            ->getCollection()
                            ->addFieldToFilter('trubox_id', $truBox->getId())
                            ->getFirstItem();

                        if($items == null){
                            if(!in_array($c->getId(), $g3))
                                $g3[] = $c->getId();
                        }

                    } else {
                        if(!in_array($c->getId(), $g3))
                            $g3[] = $c->getId();
                    }
                }

                $this->_withoutPoints = $g1;
                $this->_withPoints = $g2;
                $this->_withoutTrubox = $g3;
                $this->_newPeople = $g4;
            }
        }

		return parent::_prepareLayout();
	}

    public function getCollectCustomers()
    {
        $ids = Mage::helper('nationpassport')->getRelatedCustomer(Mage::getSingleton('customer/session')->getCustomer()->getId());
        if($ids == null || sizeof($ids) == 0)
            return array();

        $customers = Mage::getModel('customer/customer')
            ->getCollection()
            ->addAttributeToSelect('entity_id')
            ->addAttributeToSelect('firstname')
            ->addAttributeToSelect('lastname')
            ->addAttributeToSelect('created_at')
            ->addAttributeToFilter('entity_id', array('in'  => $ids))
            ->setOrder('entity_id', 'desc')
            ;

        return $customers;
    }

	public function getListCustomer()
    {
        return $this->_customers;
    }

    public function getCustomerWithoutPoints()
    {
        return $this->_withoutPoints;
    }

    public function getCustomerWithPoints()
    {
        return $this->_withPoints;
    }

    public function getCustomerWithoutTrubox()
    {
        return $this->_withoutTrubox;
    }

    public function getCustomerNewPeople()
    {
        return $this->_newPeople;
    }

    public function getValidateNameUrl()
    {
        return $this->getUrl('*/message/validateName');
    }

    public function getSendAction()
    {
        return $this->getUrl('*/message/send');
    }

    public function getSearchSentUrl()
    {
        return $this->getUrl('*/message/searchSentMessages');
    }

    public function getSearchInboxUrl()
    {
        return $this->getUrl('*/message/searchInboxMessages');
    }

    public function getSentMessages()
    {
        $collection = Mage::getModel('nationpassport/message')
            ->getCollection()
            ->addFieldToFilter('customer_id', Mage::getSingleton('customer/session')->getCustomer()->getId())
            ->setOrder('message_id', 'desc')
            ;

        return $collection;
    }

    public function getInboxMessages()
    {
        $collection = Mage::getModel('nationpassport/message')
            ->getCollection()
            ->addFieldToFilter('recipient_id', Mage::getSingleton('customer/session')->getCustomer()->getId())
            ->setOrder('message_id', 'desc')
        ;

        return $collection;
    }

    public function getReadMessageUrl()
    {
        return $this->getUrl('*/message/readMessage');
    }

    public function getSendEmailUrl()
    {
        return Mage::getUrl('*/message/sendEmail');
    }

    public function compareExpireDate($start_time, $end_time)
    {
        $sub = $end_time - $start_time;
        if ($sub < 0)
            return false;

        $diff = abs($sub);

        $years = floor($diff / (365 * 60 * 60 * 24));
        $months = floor(($diff - $years * 365 * 60 * 60 * 24) / (30 * 60 * 60 * 24));
        $days = floor(($diff - $years * 365 * 60 * 60 * 24 - $months * 30 * 60 * 60 * 24) / (60 * 60 * 24));
        $hours = floor(($diff - $years * 365 * 60 * 60 * 24 - $months * 30 * 60 * 60 * 24 - $days * 60 * 60 * 24) / (60 * 60));
        $minutes = floor(($diff - $years * 365 * 60 * 60 * 24 - $months * 30 * 60 * 60 * 24 - $days * 60 * 60 * 24 - $hours * 60 * 60) / 60);
        $seconds = floor(($diff - $years * 365 * 60 * 60 * 24 - $months * 30 * 60 * 60 * 24 - $days * 60 * 60 * 24 - $hours * 60 * 60 - $minutes * 60));

        return $years * 365 + $months * 30 + $days;
    }

}
