<?php

class Magestore_Nationpassport_Block_Leaderboard extends Mage_Core_Block_Template
{
	public function _prepareLayout(){
		return parent::_prepareLayout();
	}

    public function getCustomerId()
    {
        return Mage::getSingleton('customer/session')->getCustomer()->getId();
    }

    public function getCurrentCustomer()
    {
        return  Mage::getModel('customer/customer')->load(
            $this->getCustomerId()
        );
    }

    public function getCustomerStats($customer_id)
    {
        return Mage::helper('badges')->getCustomerStats(
            $customer_id
        );
    }

    public function getBadgeFromStats($badge_name)
    {
        return Mage::helper('badges')->getBadgeFromStats(
            $badge_name
        );
    }

    public function getCustomerBadgesHistory($customer_id)
    {
        return Mage::helper('badges')->getCustomerBadgesHistory(
            $customer_id
        );
    }

    public function getImagePath($image_name)
    {
        return Mage::helper('badges')->getImagePath(
            $image_name
        );
    }

    public function getMemberInfoURL($id)
    {
        return Mage::getUrl('passport/member/info', array('id'  => base64_encode($id)));
    }


}
