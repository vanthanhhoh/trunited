<?php

class Magestore_Nationpassport_Block_Nationpassport extends Mage_Core_Block_Template
{
	public function _prepareLayout(){
		return parent::_prepareLayout();
	}

	public function getAccountDashboardUrl()
    {
        return $this->getUrl('customer/account');
    }

    public function getCurrentCustomer()
    {
        return  Mage::getModel('customer/customer')->load(
            Mage::getSingleton('customer/session')->getCustomer()->getId()
        );
    }

    public function getAffiliateRefer()
    {
        return Mage::helper('nationpassport')->getAffiliateRefer(
            $this->getCurrentCustomer()->getId()
        );
    }

    public function getTruGiftCardProduct()
    {
        return Mage::helper('nationpassport')->getTruGiftCardProduct();
    }

    public function getTruWalletCredit()
    {
        return Mage::helper('truwallet/account')->getTruWalletCredit();
    }

    public function getTruGiftCardCredit()
    {
        return Mage::helper('trugiftcard/account')->getTruGiftCardCredit();
    }

    public function getBalance()
    {
        $account = Mage::getSingleton('affiliateplus/session')->getAccount();
        if($account != null && $account->getId())
            return Mage::helper('core')->currency($account->getBalance(), true, false);
        else
            return Mage::helper('core')->currency(0, true, false);
    }

    public function getShareTruGiftCardUrl()
    {
        return $this->getUrl('*/balance/share');
    }

    public function getBalanceDashboardUrl()
    {
        return $this->getUrl('*/balance/');
    }

    public function getTransferUrl()
    {
        return $this->getUrl('*/balance/transfer');
    }

    public function getWithdrawalUrl()
    {
        return $this->getUrl('*/balance/withdrawal');
    }

    public function getSendMessageUrl()
    {
        return $this->getUrl('*/message/index');
    }

    public function getCustomerStats()
    {
        return Mage::helper('badges')->getCustomerStats(
            Mage::getSingleton('customer/session')->getCustomer()->getId()
        );
    }

    public function getBadgeFromStats($badge_name)
    {
        return Mage::helper('badges')->getBadgeFromStats(
            $badge_name
        );
    }

    public function getCustomerBadgesHistory()
    {
        return Mage::helper('badges')->getCustomerBadgesHistory(
            Mage::getSingleton('customer/session')->getCustomer()->getId()
        );
    }

    public function getImagePath($image_name)
    {
        return Mage::helper('badges')->getImagePath(
            $image_name
        );
    }

    public function getFilterLearBoardUrl()
    {
        return $this->getUrl('*/member/leaderboard');
    }

    public function getContest($district)
    {
        if($district == null)
            return false;

        $helper = Mage::helper('nationpassport');
        if($helper->isEnableContest()){
            $teams = $helper->getTeamConfiguration();
            if($teams != null && sizeof($teams) > 0)
            {
                $content = $helper->getContentOfContest();
                if($content != null)
                {
                    $result = '';
                    foreach ($teams as $team) {
                        if(in_array($district, $team['team_id'])){
                            $result .= str_replace('{{team_name}}', $team['team_name'], $content);
                            break;
                        }
                    }

                    return $result;
                }
            }
        }

        return false;
    }

    public function isShowPricelineDiscount()
    {
        return Mage::helper('nationpassport/priceline')->isShowImage();
    }

    public function getPricelineUrl()
    {
        return Mage::helper('nationpassport/priceline')->getPricelineUrl();
    }

    public function getPricelineImage()
    {
        return Mage::helper('nationpassport/priceline')->getImage();
    }
}
