<?php

class Magestore_Connection_Block_Adminhtml_Connection_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
	public function __construct(){
		parent::__construct();
		
		$this->_objectId = 'id';
		$this->_blockGroup = 'connection';
		$this->_controller = 'adminhtml_connection';
		
		$this->_updateButton('save', 'label', Mage::helper('connection')->__('Save Item'));
		$this->_updateButton('delete', 'label', Mage::helper('connection')->__('Delete Item'));
		
		$this->_addButton('saveandcontinue', array(
			'label'		=> Mage::helper('adminhtml')->__('Save And Continue Edit'),
			'onclick'	=> 'saveAndContinueEdit()',
			'class'		=> 'save',
		), -100);

		$this->_formScripts[] = "
			function toggleEditor() {
				if (tinyMCE.getInstanceById('connection_content') == null)
					tinyMCE.execCommand('mceAddControl', false, 'connection_content');
				else
					tinyMCE.execCommand('mceRemoveControl', false, 'connection_content');
			}

			function saveAndContinueEdit(){
				editForm.submit($('edit_form').action+'back/edit/');
			}
		";
	}

	public function getHeaderText(){
		if(Mage::registry('connection_data') && Mage::registry('connection_data')->getId())
			return Mage::helper('connection')->__("Edit Item '%s'", $this->htmlEscape(Mage::registry('connection_data')->getTitle()));
		return Mage::helper('connection')->__('Add Item');
	}
}