<?php

class Magestore_Connection_Block_Adminhtml_Connection_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
	protected function _prepareForm(){
		$form = new Varien_Data_Form();
		$this->setForm($form);
		
		if (Mage::getSingleton('adminhtml/session')->getConnectionData()){
			$data = Mage::getSingleton('adminhtml/session')->getConnectionData();
			Mage::getSingleton('adminhtml/session')->setConnectionData(null);
		}elseif(Mage::registry('connection_data'))
			$data = Mage::registry('connection_data')->getData();
		
		$fieldset = $form->addFieldset('connection_form', array('legend'=>Mage::helper('connection')->__('Item information')));

		$fieldset->addField('title', 'text', array(
			'label'		=> Mage::helper('connection')->__('Title'),
			'class'		=> 'required-entry',
			'required'	=> true,
			'name'		=> 'title',
		));

		$fieldset->addField('filename', 'file', array(
			'label'		=> Mage::helper('connection')->__('File'),
			'required'	=> false,
			'name'		=> 'filename',
		));

		$fieldset->addField('status', 'select', array(
			'label'		=> Mage::helper('connection')->__('Status'),
			'name'		=> 'status',
			'values'	=> Mage::getSingleton('connection/status')->getOptionHash(),
		));

		$fieldset->addField('content', 'editor', array(
			'name'		=> 'content',
			'label'		=> Mage::helper('connection')->__('Content'),
			'title'		=> Mage::helper('connection')->__('Content'),
			'style'		=> 'width:700px; height:500px;',
			'wysiwyg'	=> false,
			'required'	=> true,
		));

		$form->setValues($data);
		return parent::_prepareForm();
	}
}