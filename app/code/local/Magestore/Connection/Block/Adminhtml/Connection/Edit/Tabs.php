<?php

class Magestore_Connection_Block_Adminhtml_Connection_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
	public function __construct(){
		parent::__construct();
		$this->setId('connection_tabs');
		$this->setDestElementId('edit_form');
		$this->setTitle(Mage::helper('connection')->__('Item Information'));
	}

	protected function _beforeToHtml(){
		$this->addTab('form_section', array(
			'label'	 => Mage::helper('connection')->__('Item Information'),
			'title'	 => Mage::helper('connection')->__('Item Information'),
			'content'	 => $this->getLayout()->createBlock('connection/adminhtml_connection_edit_tab_form')->toHtml(),
		));
		return parent::_beforeToHtml();
	}
}