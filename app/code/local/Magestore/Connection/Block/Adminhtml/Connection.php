<?php

class Magestore_Connection_Block_Adminhtml_Connection extends Mage_Adminhtml_Block_Widget_Grid_Container
{
	public function __construct(){
		$this->_controller = 'adminhtml_connection';
		$this->_blockGroup = 'connection';
		$this->_headerText = Mage::helper('connection')->__('Item Manager');
		$this->_addButtonLabel = Mage::helper('connection')->__('Add Item');
		parent::__construct();
	}
}