<?php

class Magestore_Connection_Block_Map extends Mage_Core_Block_Template
{
    protected $_currentPeriod;

	public function _prepareLayout(){
	    if($this->_currentPeriod == null){
	        $this->_currentPeriod = Mage::helper('badges')->getCurrentPayoutPeriod();
        }
		return parent::_prepareLayout();
	}

	public function getCurrentCustomerId()
    {
        return Mage::getSingleton('customer/session')->getCustomer()->getId();
    }
    
    public function getViewCustomerId()
    {
        if($this->getRequest()->getParam('person_id') !== null && $this->getRequest()->getParam('person_name') !== ''){
            return $this->getRequest()->getParam('person_id');
        } else {
            return $this->getCurrentCustomerId();
        }
    }

    public function getDefaultLevel()
    {
        $default_level = Mage::helper('connection')->getConfigData('general', 'default_level');
        return $default_level != null ? $default_level : 5;
    }

    public function getMaxLevel()
    {
        $max_level = Mage::helper('connection')->getConfigData('general', 'max_level');
        return $max_level != null ? $max_level : 15;
    }

    public function getDefaultZoom()
    {
        $default_zoom = Mage::helper('connection')->getConfigData('general', 'default_zoom');
        return $default_zoom != null ? $default_zoom : 100;
    }

    public function getMinimumZoom()
    {
        $minimum_zoom = Mage::helper('connection')->getConfigData('general', 'minimum_zoom');
        return $minimum_zoom != null ? $minimum_zoom : 25;
    }

    public function getMaximumZoom()
    {
        $maximum_zoom = Mage::helper('connection')->getConfigData('general', 'maximum_zoom');
        return $maximum_zoom != null ? $maximum_zoom : 200;
    }

    public function getStepZoom()
    {
        $step_zoom = Mage::helper('connection')->getConfigData('general', 'step_zoom');
        return $step_zoom != null ? $step_zoom : 25;
    }

    public function getMaxLevelCustomer($items)
    {
        $max_level_customer = $items[sizeof($items) - 1]['level'] + 1;
        $max_level_configured = $this->getMaxLevel();

        if($max_level_customer >= $max_level_configured)
            return $max_level_configured;
        else
            return $max_level_customer;
    }

    public function getSelectedDefaultLevel($max)
    {
        $request_level = $this->getRequest()->getParam('tree_lvl');
        if($request_level != null)
            return $request_level;
        else
            return ($this->getDefaultLevel() >= $max && $max != null) ? $max : $this->getDefaultLevel();
    }

	public function getSqlQuery()
    {
        $sql = "SELECT g.treerootid, g.personid, g.ParentId, pps.name, g.level, pps.badge, pps.personal_points, b.image_name, b.hex_color,  pps.personal_points + pps.guest_commission as '30_points'";
        $sql .= " FROM tr_Genealogy g";
        $sql .= " JOIN tr_payoutperiod_stats pps ON g.personid = pps.customer_id AND pps.payout_period_id = ".$this->_currentPeriod;
        $sql .= " JOIN trcustomer_badges b ON pps.badge = b.name AND b.is_title = 'Y'";
        $sql .= " WHERE g.treerootid = ".$this->getViewCustomerId();
        $sql .= " ORDER BY g.level, g.leg";

        return $sql;
    }

    public function getItems()
    {
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');

        $query = $this->getSqlQuery();

        $results = $readConnection->fetchAll($query);

        return $results;
    }

    public function getImage($image_url)
    {
        return Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA).'badges/icon/'.$image_url;
    }

    public function getCustomers()
    {
        $ids = Mage::helper('nationpassport')->getRelatedCustomer(Mage::getSingleton('customer/session')->getCustomer()->getId());
        if($ids == null || sizeof($ids) == 0)
            return array();

        $customers = Mage::getModel('customer/customer')
            ->getCollection()
            ->addAttributeToSelect('entity_id')
            ->addAttributeToSelect('firstname')
            ->addAttributeToSelect('lastname')
            ->addAttributeToSelect('created_at')
            ->addAttributeToFilter('entity_id', array('in'  => $ids))
            ->setOrder('entity_id', 'desc')
        ;

        return $customers;
    }

    public function isShowActive()
    {
        return $this->getRequest()->getParam('active_users') !== null ? true : false;
    }

    public function isShow30()
    {
        return $this->getRequest()->getParam('users_30') !== null ? true : false;
    }

    public function getDetailsUrl()
    {
        return Mage::getUrl('*/*/details');
    }

    public function isVisible($item)
    {
        if($item['level'] == 0)
            return true;

        if($this->isShowActive() && $this->isShow30()){
            if($item['30_points'] >= 30)
                return true;
            else
                return false;
        } else if ($this->isShow30() && !$this->isShowActive()){
            if($item['30_points'] >= 30)
                return true;
            else
                return false;
        } else if ($this->isShowActive() && !$this->isShow30()){

            if($item['personal_points'] > 0)
                return true;
            else
                return false;
        }

        return true;
    }
}
