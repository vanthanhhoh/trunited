<?php

class Magestore_Connection_Helper_Data extends Mage_Core_Helper_Abstract
{
    public function getConfigData($group, $field, $store = null)
    {
        return Mage::getStoreConfig('connection/' . $group . '/' . $field, $store);
    }

    public function isEnable()
    {
        return $this->getConfigData('general', 'enable');
    }
}
