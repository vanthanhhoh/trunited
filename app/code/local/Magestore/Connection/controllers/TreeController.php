<?php

class Magestore_Connection_TreeController extends Mage_Core_Controller_Front_Action
{
    /**
     * check customer is logged in
     */
    public function preDispatch()
    {
        parent::preDispatch();
        if (!$this->getRequest()->isDispatched()) {
            return;
        }
        $action = $this->getRequest()->getActionName();
        if ($action != 'policy' && $action != 'redirectLogin') {
            // Check customer authentication
            if (!Mage::getSingleton('customer/session')->isLoggedIn()) {
                Mage::getSingleton('customer/session')->setAfterAuthUrl(
                    Mage::getUrl($this->getFullActionName('/'))
                );
                $this->_redirectUrl(Mage::getUrl('customer/account/login'));
                $this->setFlag('', Mage_Core_Controller_Varien_Action::FLAG_NO_DISPATCH, true);
            }
        }
    }


    public function indexAction(){
		$this->loadLayout();

        $this->_title(Mage::helper('connection')->__('Connection Tree'));

        $breadcrumbs = $this->getLayout()->getBlock("breadcrumbs");
        $breadcrumbs->addCrumb("home", array(
            "label" => $this->__("Home"),
            "title" => $this->__("Home"),
            "link"  => Mage::getBaseUrl()
        ));

        $breadcrumbs->addCrumb("connection", array(
            "label" => $this->__("My Tree"),
            "title" => $this->__("My Tree"),
        ));

        $this->renderLayout();
	}

	public function detailsAction()
    {
        echo $this->getLayout()->createBlock('nationpassport/member')
            ->setTemplate('connection/details.phtml')->toHtml();
    }
}
