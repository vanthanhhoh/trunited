<?php
/**
 * Magestore
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magestore.com license that is
 * available through the world-wide-web at this URL:
 * http://www.magestore.com/license-agreement.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Magestore
 * @package     Magestore_TruBox
 * @module      TruBox
 * @author      Magestore Developer
 *
 * @copyright   Copyright (c) 2016 Magestore (http://www.magestore.com/)
 * @license     http://www.magestore.com/license-agreement.html
 *
 */

/**
 * TruBox Helper
 *
 * @category    Magestore
 * @package     Magestore_TruBox
 * @module      TruBox
 * @author      Magestore Developer
 */
class Magestore_TruBox_Helper_Order extends Mage_Core_Helper_Abstract
{
    const XML_PATH_EMAIL_OUT_OF_STOCK = 'trubox/email/out_of_stock';
    const XML_PATH_EMAIL_SENDER = 'trubox/email/sender';

    protected $_shippingMethod = 'freeshipping_freeshipping';
    protected $_paymentMethod = 'authnetcim';
    protected $_freePaymentMethod = 'free';

    protected $_customer;

    protected $_subTotal = 0;
    protected $_order;
    protected $_storeId;

    public function setShippingMethod($methodName)
    {
        $this->_shippingMethod = $methodName;
    }

    public function setPaymentMethod($methodName)
    {
        $this->_paymentMethod = $methodName;
    }

    public function setCustomer($customer)
    {
        if ($customer instanceof Mage_Customer_Model_Customer) {
            $this->_customer = $customer;
        }
        if (is_numeric($customer)) {
            $this->_customer = Mage::getModel('customer/customer')->load($customer);
        }
    }

    public function prepareOrder($data)
    {
        $flag = array();
        if (sizeof($data) > 0) {
            foreach ($data as $trubox_id => $itms) {
                $trubox = Mage::getModel('trubox/trubox')->load($trubox_id);
                if ($trubox->getId()) {
                    if ($rs = $this->createOrder($trubox->getCustomerId(), $itms)) {
                        if (sizeof($rs) > 0)
                            $flag[] = $rs;

                        sleep(Mage::helper('trubox')->getDelaySecond());
                    }
                }

            }
        }

        return $flag;
    }

    public function getAddressByTruBoxId($customer_id, $type = Magestore_TruBox_Model_Address::ADDRESS_TYPE_BILLING)
    {
        $truBox_id = Mage::helper('trubox')->getCurrentTruBoxId($customer_id);
        if ($truBox_id == null)
            return null;

        $address = Mage::getModel('trubox/address')->getCollection()
            ->addFieldToFilter('trubox_id', $truBox_id)
            ->addFieldToFilter('address_type', $type)
            ->getFirstItem();

        if ($address->getId())
            return $address;
        else
            return null;
    }

    public function checkConditionCustomer($customer_id)
    {
        $truBox_id = Mage::helper('trubox')->getCurrentTruBoxId($customer_id);
        if ($truBox_id == null)
            throw new Exception(
                Mage::helper('trubox')->__('TruBox does not exits !')
            );

        $address = Mage::getModel('trubox/address')->getCollection()
            ->addFieldToSelect('address_id')
            ->addFieldToSelect('address_type')
            ->addFieldToFilter('trubox_id', $truBox_id);

        $customer = Mage::getModel('customer/customer')->load($customer_id);
        if (sizeof($address) > 0) {
            $flag = 0;
            foreach ($address as $addr) {
                if ($addr->getAddressType() == Magestore_TruBox_Model_Address::ADDRESS_TYPE_BILLING)
                    $flag++;
                else if ($addr->getAddressType() == Magestore_TruBox_Model_Address::ADDRESS_TYPE_SHIPPING)
                    $flag++;
            }


            if ($flag != 2)
                throw new Exception(
                    Mage::helper('trubox')->__('%s don\'t have address information !', $customer->getName())
                );

        } else {
            throw new Exception(
                Mage::helper('trubox')->__('%s don\'t have address information !', $customer->getName())
            );
        }

//        $payment = Mage::getModel('trubox/payment')->getCollection()
//                ->addFieldToFilter('trubox_id', $truBox_id)
//                ->getFirstItem()
//            ;
//
//        if(!$payment->getId())
//            throw new Exception(
//                Mage::helper('trubox')->__('%s don\'t have payment information !', $customer->getName())
//            );
    }

    public function getProductParams($customer_id, $data_items)
    {
        $truBox_id = Mage::helper('trubox')->getCurrentTruBoxId($customer_id);
        if ($truBox_id == null)
            throw new Exception(
                Mage::helper('trubox')->__('TruBox does not exits !')
            );

        $data = array();
        $items = Mage::getModel('trubox/item')->getCollection()
            ->addFieldToFilter('trubox_id', $truBox_id)
            ->addFieldToFilter('item_id', array('in' => $data_items));


        if (sizeof($items) > 0) {
            foreach ($items as $item) {
                $product = Mage::getModel('catalog/product')->load($item->getProductId());

                if ($product->getIsInStock() && $product->isSaleable() === true) {
                    if ($item->getOptionParams() != null) {
                        $option_params = json_decode($item->getOptionParams(), true);
                        if ($product->getTypeId() == 'configurable') {
                            $main_child_product = Mage::getModel('catalog/product_type_configurable')->getProductByAttributes($option_params, $product)->getId();
                            $childProducts = Mage::getModel('catalog/product_type_configurable')->getUsedProducts(null, $product);
                            $flag = false;
                            foreach ($childProducts as $childProduct) {
                                $qty = Mage::getModel('cataloginventory/stock_item')->loadByProduct($childProduct)->getQty();
                                if ($childProduct->getId() == $main_child_product) {

                                    if ($qty <= 0) {
                                        $name = $product->getName() . '<br />';
                                        $flag = true;
                                        $_options = Mage::helper('trubox')->getConfigurableOptionProduct($product);
                                        if ($_options && sizeof($option_params) > 0) {
                                            foreach ($_options as $_option) {
                                                $_attribute_value = 0;
                                                foreach ($option_params as $k => $v) {
                                                    if ($k == $_option['attribute_id']) {
                                                        $_attribute_value = $v;
                                                        break;
                                                    }
                                                }
                                                if ($_attribute_value > 0) {
                                                    $name .= '<small><b>' . $_option['label'] . '</b></small><br />';
                                                    foreach ($_option['values'] as $val) {
                                                        if ($val['value_index'] == $_attribute_value) {
                                                            $name .= '<small><i>' . $val['default_label'] . '</i></small>';
                                                            break;
                                                        }
                                                    }
                                                }
                                            }
                                        }

                                        $data['email'][] = array(
                                            'product_name' => $name,
                                            'qty' => $item->getQty(),
                                            'price' => $item->getPrice(),
                                        );
                                    }
                                    break;
                                }
                            }

                            if (!$flag) {
                                $data['product'][$item->getId()] = array(
                                    $item->getProductId() => array(
                                        'qty' => $item->getQty(),
                                        'super_attribute' => $option_params,
                                        '_processing_params' => array(),
                                    )
                                );
                            }
                        } else {
                            $data['product'][$item->getId()] = array(
                                $item->getProductId() => array(
                                    'qty' => $item->getQty(),
                                    'options' => $option_params,
                                    '_processing_params' => array(),
                                )
                            );
                        }
                    } else {
                        $data['product'][$item->getId()] = array(
                            $item->getProductId() => array(
                                'qty' => $item->getQty(),
                                '_processing_params' => array(),
                            )
                        );
                    }
                } else {
                    if ($item->getOptionParams() != null) {
                        $name = $product->getName() . '<br />';
                        $option_params = json_decode($item->getOptionParams(), true);
                        if ($product->getTypeId() == 'configurable') {
                            $_options = Mage::helper('trubox')->getConfigurableOptionProduct($product);
                            if ($_options && sizeof($option_params) > 0) {
                                foreach ($_options as $_option) {
                                    $_attribute_value = 0;
                                    foreach ($option_params as $k => $v) {
                                        if ($k == $_option['attribute_id']) {
                                            $_attribute_value = $v;
                                            break;
                                        }
                                    }
                                    if ($_attribute_value > 0) {
                                        $name .= '<small><b>' . $_option['label'] . '</b></small><br />';
                                        foreach ($_option['values'] as $val) {
                                            if ($val['value_index'] == $_attribute_value) {
                                                $name .= '<small><i>' . $val['default_label'] . '</i></small>';
                                                break;
                                            }
                                        }
                                    }
                                }
                            }
                        } else {
                            foreach ($product->getOptions() as $o) {
                                $values = $o->getValues();
                                $_attribute_value = 0;

                                foreach ($option_params as $k => $v) {
                                    if ($k == $o->getOptionId()) {
                                        $_attribute_value = $v;
                                        break;
                                    }
                                }
                                if ($_attribute_value > 0) {
                                    $name .= '<small><b>' . $o->getTitle() . '</b></small><br />';
                                    foreach ($values as $val) {
                                        if (is_array($_attribute_value)) {
                                            if (in_array($val->getOptionTypeId(), $_attribute_value)) {
                                                $name .= '<small><i>' . $val->getTitle() . '</i></small>';
                                            }
                                        } else if ($val->getOptionTypeId() == $_attribute_value) {
                                            $name .= '<small><i>' . $val->getTitle() . '</i></small>';
                                        }
                                    }
                                }
                            }
                        }
                        $data['email'][] = array(
                            'product_name' => $name,
                            'qty' => $item->getQty(),
                            'price' => $item->getPrice(),
                        );
                    } else {
                        $data['email'][] = array(
                            'product_name' => $product->getName(),
                            'qty' => $item->getQty(),
                            'price' => $item->getPrice(),
                        );
                    }
                }
            }
        }

        return $data;
    }

    public function getPaymentInformation($customer_id, $is_no_need_payment)
    {
        $customer = Mage::getModel('customer/customer')->load($customer_id);
        $truBox_id = Mage::helper('trubox')->getCurrentTruBoxId($customer_id);
        if ($truBox_id == null)
            throw new Exception(
                Mage::helper('trubox')->__('TruBox does not exits !')
            );

        $card = Mage::getModel('tokenbase/card')->getCollection()
            ->addFieldToFilter('active', 1)
            ->addFieldToFilter('customer_id', $customer_id)
            ->addFieldToFilter('method', 'authnetcim')
            ->addFieldToFilter('use_in_trubox', 1)
            ->setOrder('id', 'desc')
            ->getFirstItem();

        if ($card == null || !$card->getId()) {
            $card = Mage::getModel('tokenbase/card')->getCollection()
                ->addFieldToFilter('active', 1)
                ->addFieldToFilter('customer_id', $customer_id)
                ->addFieldToFilter('method', 'authnetcim')
                ->setOrder('id', 'desc')
                ->getFirstItem();
        }

        if (($card == null || !$card->getId()) && !$is_no_need_payment)
            throw new Exception(
                Mage::helper('trubox')->__('%s don\'t have payment information !', $customer->getEmail())
            );

        if ($card != null && $card->getId() && $card->getData('use_in_trubox') == 0) {
            $card->setData('use_in_trubox', 1);
            $card->setData('updated_at', now());
            $card->save();
        }
        return $card;
    }

    public function createOrder($customer_id, $data_items, $is_collect_totals = false, $is_wrong_address = false)
    {
        Mage::helper('catalog/product')->setSkipSaleableCheck(true);
        $customer = Mage::getModel('customer/customer')->load($customer_id);
        $admin_session = Mage::getSingleton('adminhtml/session');
        $result = array();
        try {

            $admin_session->setIsOrderBackend(true);
            $admin_session->setOrderCustomerId($customer->getId());

            /* Check customer */
            $truBox_id = Mage::helper('trubox')->getCurrentTruBoxId($customer->getId());
            if ($truBox_id == null)
                throw new Exception(
                    Mage::helper('trubox')->__('TruBox does not exits !')
                );
            /* End check customer */


            /* Check conditions include: billing, shipping and payment information before creating order */
            $this->checkConditionCustomer($customer_id);
            /* END Check conditions include: billing, shipping and payment information before creating order */

            $payment_information = $this->getPaymentInformation($customer_id, true);

            $billing_trubox = $this->getAddressByTruBoxId($customer_id);

            $shipping_trubox = $this->getAddressByTruBoxId($customer_id, Magestore_TruBox_Model_Address::ADDRESS_TYPE_SHIPPING);

            $prepare_data = $this->getProductParams($customer_id, $data_items);
            $products = $prepare_data['product'];
            if (sizeof($products) == 0)
                throw new Exception(
                    Mage::helper('trubox')->__('%s - No Items found!', $customer->getName())
                );


            if($is_wrong_address){
                $_addr = $shipping_specialoccasion = $payment_information->getAddress();

                $billingAddress = $shippingAddress = array(
                    'prefix' => '',
                    'firstname' => $_addr['firstname'],
                    'middlename' => '',
                    'lastname' => $_addr['lastname'],
                    'suffix' => '',
                    'company' => '',
                    'street' => $_addr['street'],
                    'city' => $_addr['city'],
                    'country_id' => $_addr['country_id'],
                    'region' => $_addr['region'],
                    'region_id' => $_addr['region_id'],
                    'postcode' => $_addr['postcode'],
                    'telephone' => $_addr['telephone'],
                    'fax' => '',
                    'vat_id' => '',
                    'save_in_address_book' => '1',
                    'use_for_shipping' => '1',
                );



            } else {
                $billingAddress = array(
                    'prefix' => '',
                    'firstname' => $billing_trubox->getFirstname(),
                    'middlename' => '',
                    'lastname' => $billing_trubox->getLastname(),
                    'suffix' => '',
                    'company' => '',
                    'street' => $shipping_trubox->getStreet(),
                    'city' => $billing_trubox->getCity(),
                    'country_id' => $billing_trubox->getCountry(),
                    'region' => $billing_trubox->getRegion(),
                    'region_id' => $billing_trubox->getRegionId(),
                    'postcode' => $billing_trubox->getZipcode(),
                    'telephone' => $billing_trubox->getTelephone(),
                    'fax' => '',
                    'vat_id' => '',
                    'save_in_address_book' => '0',
                    'use_for_shipping' => '1',
                );

                $shippingAddress = array(
                    'prefix' => '',
                    'firstname' => $shipping_trubox->getFirstname(),
                    'middlename' => '',
                    'lastname' => $shipping_trubox->getLastname(),
                    'suffix' => '',
                    'company' => '',
                    'street' => $shipping_trubox->getStreet(),
                    'city' => $shipping_trubox->getCity(),
                    'country_id' => $shipping_trubox->getCountry(),
                    'region' => $shipping_trubox->getRegion(),
                    'region_id' => $shipping_trubox->getRegionId(),
                    'postcode' => $shipping_trubox->getZipcode(),
                    'telephone' => $shipping_trubox->getTelephone(),
                    'fax' => '',
                    'vat_id' => '',
                );
            }

            $quote = Mage::getModel('adminhtml/session_quote')->getQuote()->setStoreId(1);

            /*Load Product and add to cart*/
            $before_grandTotal = 0;
            foreach ($products as $itemid => $pro) {
                $item_price = Mage::helper('trubox/item')->getItemPrice(Mage::getModel('trubox/item')->load($itemid));
                $before_grandTotal += $item_price;
                foreach ($pro as $k => $v) {
                    $product = Mage::getModel('catalog/product')->load($k);
                    $quote->addProduct($product, new Varien_Object($v));
                }
            }

            $this->checkConditionSubtotal($before_grandTotal);

            $admin_session->setIsFree(1);

            $admin_session->setGrandTotalOrder($before_grandTotal);

            /*Add Billing Address*/
            $quote->getBillingAddress()
                ->addData($billingAddress);

            $_shipping = Mage::helper('trubox')->getShippingMethod();
            if ($_shipping != null)
                $this->_shippingMethod = $_shipping;

            /*Add Shipping Address and set shipping method*/
            $quote->getShippingAddress()
                ->addData($shippingAddress)
                ->setCollectShippingRates(true)
                ->setShippingMethod($this->_shippingMethod)
                // ->setPaymentMethod($this->_paymentMethod)
                // ->collectTotals();
            ;

            if ($is_collect_totals) {
                $quote->collectTotals();
            }

            /*Set Customer group As Guest*/
            $quote->setCustomer($customer);

            if ($quote->isVirtual()) {
                $quote->getBillingAddress()->setPaymentMethod($this->_paymentMethod);
            }

            $tax_amount = $quote->getShippingAddress()->getData('tax_amount');

            $is_no_need_payment = $this->checkApplyBalanceToPayment($customer, $before_grandTotal + $tax_amount);
            $payment_information = $this->getPaymentInformation($customer_id, $is_no_need_payment);

            /*$paymentData = array(
                'method' => $this->_paymentMethod,
                'cc_type' => $payment_information->getCardType(),
                'cc_number' => $payment_information->getCardNumber(),
                'cc_exp_month' => $payment_information->getMonthExpire(),
                'cc_exp_year' => $payment_information->getYearExpire(),
                'cc_cid' => $payment_information->getCvv(),
            );*/

            $paymentData = array(
                'method' => $this->_paymentMethod,
                'card_id' => $payment_information->getHash(),
                'cc_type' => '',
                'cc_number' => '',
                'cc_exp_month' => '',
                'cc_exp_year' => '',
                'cc_cid' => '',
                'save' => 0,
            );


            if ($is_no_need_payment) {
                $paymentData = array(
                    'method' => $this->_freePaymentMethod,
                );

                // $this->_paymentMethod = $this->_freePaymentMethod;
            }

            $quote->getPayment()->importData($paymentData);
            $quote->collectTotals()->save();

            $service = Mage::getModel('sales/service_quote', $quote);
            $service->submitAll();
            /* Fix bug remove items in carts after creating orders */
            $quote->setIsActive(false)->save();
            Mage::getSingleton('adminhtml/session_quote')->clear();
            /* END Fix bug remove items in carts after creating orders */

            $increment_id = $service->getOrder()->getIncrementId();

            Mage::app()->getStore()->setConfig(Mage_Sales_Model_Order::XML_PATH_EMAIL_ENABLED, "1");


            $order_mail = new Mage_Sales_Model_Order();
            $order_mail->loadByIncrementId($increment_id);
            $order_mail->setCreatedBy(Magestore_TruBox_Model_Status::ORDER_CREATED_BY_ADMIN_YES)->save();
            $order_mail->sendNewOrderEmail();

            /** update table trubox order & history **/
            $product_save = array();
            $type_data = Magestore_TruBox_Model_Type::getOptionArray();
            foreach ($products as $item_id => $p) {
                $item = Mage::getModel('trubox/item')->load($item_id);
                if ($item != null && $item->getId()) {
                    foreach ($p as $product_id => $_dt) {
                        $product = Mage::getModel('catalog/product')->load($product_id);
                        if ($product != null && $product->getId()) {
                            $product_save[] = array(
                                'product_id' => $product->getId(),
                                'product_name' => $product->getName(),
                                'qty' => $_dt['qty'],
                                'price' => Mage::helper('core')->currency($product->getPrice(), true, false),
                                'type' => $type_data[$item->getTypeItem()],
                                'onetime_month' => $item->getOnetimeMonth(),
                                'onetime_month_text' => $item->getOnetimeMonthText(),
                            );
                        }
                    }
                }
            }

            $order_details = array(
                'status' => $order_mail->getStatus(),
                'shipping' => $order_mail->getData('shipping_description'),
                'grand_total' => Mage::helper('core')->currency($order_mail->getData('grand_total'), true, false),
                'subtotal' => Mage::helper('core')->currency($order_mail->getData('subtotal'), true, false),
                'shipping_fee' => Mage::helper('core')->currency($order_mail->getData('shipping_incl_tax'), true,
                    false),
                'tax_amount' => Mage::helper('core')->currency($order_mail->getData('tax_amount'), true, false),
            );
            $truBox_history = Mage::getModel('trubox/history');
            $history_data = array(
                'customer_id' => $customer_id,
                'customer_name' => $customer->getName(),
                'customer_email' => $customer->getEmail(),
                'order_id' => $order_mail->getEntityId(),
                'order_increment_id' => $increment_id,
                'updated_at' => now(),
                'created_at' => now(),
                'points' => $order_mail->getRewardpointsEarn(),
                'order_details' => json_encode($order_details),
                'item_details' => json_encode($product_save),
                'truwallet_balance' => $order_mail->getData('truwallet_discount'),
                'trugiftcard_balance' => $order_mail->getData('trugiftcard_discount'),
            );
            $truBox_history->setData($history_data);
            $truBox_history->save();
            /** END update table trubox order **/

            /** REMOVE ITEMS WITH ONE TIME TYPE **/
            $item_onetime = Mage::getModel('trubox/item')->getCollection()
                ->addFieldToFilter('trubox_id', $truBox_id)
                ->addFieldToFilter('type_item', Magestore_TruBox_Model_Type::TYPE_ONE_TIME)
                ->addFieldToFilter('onetime_month', array('lteq' => date('Y-m-d 23:59:59', time())));
            if (sizeof($item_onetime) > 0) {
                foreach ($item_onetime as $onetime) {
                    $onetime->delete();
                }
            }
            /** END REMOVE ITEMS WITH ONE TIME TYPE **/

            $admin_session->unsIsOrderBackend();
            $admin_session->unsOrderCustomerId();
            $admin_session->unsGrandTotalOrder();
            $admin_session->unsIsFree();

            $order_url = Mage::helper('adminhtml')->getUrl('adminhtml/sales_order/view', array('order_id' => $service->getOrder()->getId()));

            $customer_url = Mage::helper('adminhtml')->getUrl('adminhtml/customer/edit', array('id' => $customer_id));

            $result[] = array(
                'ID' => "<a href='$customer_url' target='_blank'>$customer_id</a>",
                'Email' => $customer->getEmail(),
                'Order_id' => "<a href='$order_url' target='_blank'>$increment_id</a>"
            );

            /*$success = "ID: <a href='$customer_url' target='_blank'>$customer_id</a>";
            $success .= " - EMAIL: ".$customer->getEmail();
            $success .= " - ORDER: <a href='$order_url' target='_blank'>$increment_id</a>";
            $admin_session->addSuccess($success);*/

//            if (sizeof($prepare_data['email']) > 0 && $order_mail->getId()) {
//                $this->sendEmailOutOfStock(Mage::getModel('customer/customer')->load($customer_id), $prepare_data['email']);
//            }
        } catch (Exception $ex) {
            Mage::getSingleton('adminhtml/session_quote')->clear();

            if (strpos($ex->getMessage(), 'requested Payment Method is not available') > 0) {
                $this->createOrder($customer_id, $data_items, true);
                return;
            }

            if(strpos($ex->getMessage(), 'CIM Gateway: Could not load payment record') > 0){
                $this->createOrder($customer_id, $data_items, true, true);
                return;
            }

            Mage::getSingleton('adminhtml/session')->addError(
                'Customer: ' . $customer_id . ' - ' . $customer->getEmail() . ' - ' . $ex->getMessage()
            );

            /*$result[] = array(
                'customer' => $customer_id,
                'order_increment_id' => $increment_id,
                'error' => 'Email: '.$customer->getEmail().' - '.Mage::helper('trubox')->__($ex->getMessage())
            );*/
        }

        return $result;
    }

    public function checkApplyBalanceToPayment($customer, $grandTotal)
    {

        $account = Mage::helper('truwallet/account')->loadByCustomerId($customer->getId());

        if ($account->getId()) {
            $total_discount = floatval($account->getTruwalletCredit());
            if (Mage::helper('trugiftcard')->isAppliedTGCToOrder($customer->getId())) {
                $account = Mage::helper('trugiftcard/account')->loadByCustomerId($customer->getId());
                $total_discount += $account->getTrugiftcardCredit();
            }

            if (strcasecmp($this->_shippingMethod, 'flatrate_flatrate') == 0) {
                $_grandTotal = floatval($grandTotal) + floatval(Mage::helper('trubox')->getShippingAmount());
            } else {
                $_grandTotal = floatval($grandTotal);
            }

            if ($total_discount >= $_grandTotal)
                return true;
            else
                return false;
        } else {
            return false;
        }
    }

    public function getNextIncrementId()
    {
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');

        $entityStoreTable = $resource->getTableName('eav_entity_store');
        $entityTypeTable = $resource->getTableName('eav_entity_type');

        $selectEntity = $readConnection->select()->from($entityTypeTable, "*")
            ->where("entity_type_code = 'order'");

        $entityTypeRow = $readConnection->fetchRow($selectEntity);

        if (isset($entityTypeRow['entity_type_id']) && $entityTypeRow['entity_type_id'] > 0) {
            $orderEntityTypeId = $entityTypeRow['entity_type_id'];
            $entityStoreSelect = $readConnection->select()->from($entityStoreTable, "*")
                ->where("store_id = ? AND entity_type_id = $orderEntityTypeId", 1);

            $row = $readConnection->fetchRow($entityStoreSelect);

            $lastIncrementId = 0;
            if (isset($row['increment_last_id'])) {
                $lastIncrementId = $row['increment_last_id'] + 1;
            }
            return $lastIncrementId;
        }

        return 0;
    }

    public function checkRegionId($country, $region_name, $region_id = 0)
    {
        if ($region_id > 0 && !filter_var($region_id, FILTER_VALIDATE_INT) === false) {
            return $region_id;
        } else {
            $region = Mage::getModel('directory/region')->getCollection()
                ->addFieldToSelect('region_id')
                ->addFieldToFilter('country_id', $country)
                ->addFieldToFilter('default_name', $region_name)
                ->getFirstItem();

            if ($region->getId())
                return $region->getId();
            else
                return null;
        }
    }

    /**
     * @param $customer
     * @param $products
     * @return $this
     */
    public function sendEmailOutOfStock($customer, $products)
    {
        $store = Mage::app()->getStore();
        $translate = Mage::getSingleton('core/translate');
        $translate->setTranslateInline(false);

        if (!$customer->getId())
            return $this;

        $email_path = Mage::getStoreConfig(self::XML_PATH_EMAIL_OUT_OF_STOCK, $store);

        $data = array(
            'store' => $store,
            'customer_name' => $customer->getName(),
            'items' => $products
        );

        Mage::getModel('core/email_template')
            ->setDesignConfig(array(
                'area' => 'frontend',
                'store' => Mage::app()->getStore()->getId()
            ))->sendTransactional(
                $email_path,
                Mage::getStoreConfig(self::XML_PATH_EMAIL_SENDER, $store->getId()),
                $customer->getEmail(),
                $customer->getName(),
                $data
            );

        $translate->setTranslateInline(true);
        return $this;
    }

    public function checkConditionSubtotal( $subtotal)
    {
        $minimum_subtotal_order = Mage::helper('trubox')->getMinimumSubtotalOrder();
        if($subtotal < $minimum_subtotal_order)
        {
            throw new Exception(
                Mage::helper('trubox')->__(
                    'has subtotal (%s) is less than %s!',
                    Mage::helper('core')->currency($subtotal, true, false),
                    Mage::helper('core')->currency($minimum_subtotal_order, true, false)
                )
            );
        }
    }

}
