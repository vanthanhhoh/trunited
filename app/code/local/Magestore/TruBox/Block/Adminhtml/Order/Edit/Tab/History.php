<?php

class Magestore_TruBox_Block_Adminhtml_Order_Edit_Tab_History extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('historyGrid');
        $this->setUseAjax(true);
        $this->setDefaultSort('created_at');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
    }

    protected function _getStore()
    {
        $storeId = (int)$this->getRequest()->getParam('store', 0);

        return Mage::app()->getStore($storeId);
    }

    protected function _prepareCollection()
    {
        $collection = Mage::getModel('trubox/history')
            ->getCollection()
            ->addFieldToFilter('created_at', array('gt' => date('Y-m-d 00:00:00', time())))
            ->setOrder('history_id', 'desc');
        $this->setCollection($collection);

        parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {

        $this->addColumn('history_id', array(
            'header' => Mage::helper('trubox')->__('ID'),
            'align' => 'right',
            'width' => '50px',
            'index' => 'history_id',
        ));

        $this->addColumn('customer_id', array(
            'header' => Mage::helper('trubox')->__('Customer ID'),
            'index' => 'customer_id',
            'filter_name' => 'customer_id',
            'width' => '20px',
        ));

        $this->addColumn('customer_name', array(
            'header' => Mage::helper('trubox')->__('Customer Name'),
            'index' => 'customer_name',
        ));

        $this->addColumn('customer_email', array(
            'header' => Mage::helper('Sales')->__('Customer Email'),
            'index' => 'customer_email',
        ));

        $this->addColumn('order_id', array(
            'header' => Mage::helper('trubox')->__('Order ID'),
            'index' => 'order_id',
        ));

        $this->addColumn('order_increment_id', array(
            'header' => Mage::helper('trubox')->__('Order Increment ID'),
            'index' => 'order_increment_id',
            'width' => '20px',
        ));

        $this->addColumn('points', array(
            'header' => Mage::helper('trubox')->__('Earned Points'),
            'index' => 'points',
            'type' => 'number',
            'width' => '50px',
        ));

        $this->addColumn('truwallet_balance', array(
            'header' => Mage::helper('trubox')->__('TruWallet.bk Balance'),
            'width' => '20px',
            'type' => 'price',
            'currency_code' => Mage::app()->getStore()->getBaseCurrency()->getCode(),
            'index' => 'truwallet_balance',
        ));

        $this->addColumn('trugiftcard_balance', array(
            'header' => Mage::helper('trubox')->__('TGC Balance'),
            'width' => '20px',
            'type' => 'price',
            'currency_code' => Mage::app()->getStore()->getBaseCurrency()->getCode(),
            'index' => 'trugiftcard_balance',
        ));

        $this->addColumn('order_details', array(
            'header' => Mage::helper('trubox')->__('Order Details'),
            'index' => 'order_details',
            'width' => '500px',
            'renderer' => 'Magestore_TruBox_Block_Adminhtml_Renderer_History_Order_Detail',
        ));

        // $this->addColumn('item_details', array(
        //     'header' => Mage::helper('trubox')->__('Items Details'),
        //     'index' => 'item_details',
        //     'width' => '500px',
        //     'renderer' => 'Magestore_TruBox_Block_Adminhtml_Renderer_History_Item_Detail',
        // ));

        $this->addColumn('created_at', array(
            'header' => Mage::helper('trubox')->__('Created At'),
            'align' => 'right',
            'index' => 'created_at',
            'type' => 'date'
        ));

        $this->addColumn('updated_at', array(
            'header' => Mage::helper('trubox')->__('Updated At'),
            'align' => 'right',
            'index' => 'updated_at',
            'type' => 'date'
        ));

        return parent::_prepareColumns();
    }

    /**
     * @return mixed|string
     */
    public function getGridUrl()
    {
        return $this->_getData('grid_url')
            ? $this->_getData('grid_url')
            : $this->getUrl('*/*/history', array('_current' => true));
    }

    /**
     * @param $item
     *
     * @return bool|string
     */
    public function getRowUrl($item)
    {

    }

}
