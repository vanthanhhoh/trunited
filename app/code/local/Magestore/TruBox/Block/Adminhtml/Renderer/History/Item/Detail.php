<?php

/**
 * Created by PhpStorm.
 * User: anthony
 * Date: 1/17/17
 * Time: 2:16 PM
 */
class Magestore_TruBox_Block_Adminhtml_Renderer_History_Item_Detail extends
    Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {
        $value = $row->getData();
        $data = json_decode($value['item_details']);
        $display = '';
        if(sizeof($data) > 0)
        {
            $count = sizeof($data);
            foreach ($data as $dt) {
                foreach ($dt as $k=>$v) {
                    if($v != null)
                        $display .= "<b>$k: </b> $v<br />";
                }

                if($count > 1)
                    $display .= '<hr /><br />';
            }

        }
        return $display;
    }

}