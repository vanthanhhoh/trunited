<?php

/**
 * Created by PhpStorm.
 * User: anthony
 * Date: 1/17/17
 * Time: 2:16 PM
 */
class Magestore_TruBox_Block_Adminhtml_Renderer_History_Order_Detail extends
    Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {
        $value = $row->getData();
        $data = json_decode($value['order_details']);

        $display = '';
        if(sizeof($data) > 0)
        {
            foreach ($data as $k=>$v) {
                $display .= "<b>$k: </b> $v<br />";
            }

        }
        return $display;
    }

}