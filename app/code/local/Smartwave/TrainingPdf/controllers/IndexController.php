<?php
class Smartwave_TrainingPdf_IndexController extends Mage_Core_Controller_Front_Action
{
    
    public function indexAction(){
        $params = $this->getRequest()->getParams();
        $lang = ( isset( $params['lang'] ) && $params['lang'] ) ? $params['lang'] : 0;
        $searchtext = (isset($params['searchtext1']) && $params['searchtext1']) ? $params['searchtext1'] : 0;
        switch ($lang) {
            case 0:
                $language = 0;
                break;
            case 1:
                $language = 'english';
                break;
            case 2:
                $language = 'spanish';
                break;
            case 3:
                $language = 'french';
                break;
            default:
                $language = 0;
                break;
        }
        if( !Mage::getSingleton('customer/session')->isLoggedIn()){
            Mage::app()->getFrontController()->getResponse()->setRedirect(Mage::getUrl('customer/account/login'));
        }
        $this->loadLayout();     
		$this->getLayout()->getBlock('head')->setTitle('Training Pdf');
        $categoryid = 97;

        // $category = new Mage_Catalog_Model_Category();
        // $category->load($categoryid);
        // $collection = $category->getProductCollection();
        // if ($language){
        //     $collection->addAttributeToFilter('language',$language);
        // }
        // $searchtext = ($searchtext == 'Search Product') ? '' : $searchtext;
        // if ($searchtext){
        //     $collection->addAttributeToFilter('name', array('like' => '%'.$searchtext.'%'));
        // }


        $collection = Mage::getModel('catalog/product')
             ->getCollection()
             ->joinField('category_id', 'catalog/category_product', 'category_id', 'product_id = entity_id', null, 'left')
             ->addAttributeToSelect('*')
             ->addAttributeToFilter('status', array('eq' => Mage_Catalog_Model_Product_Status::STATUS_ENABLED))
             ->addAttributeToFilter('visibility', array('eq' => 4))
             ->addAttributeToFilter('category_id', array('in' => array(97)));

        if ($language){
            $collection->addAttributeToFilter('language',$language);
        }

        $searchtext = ($searchtext == 'Search Product') ? '' : $searchtext;
        if ($searchtext){
            $collection->addAttributeToFilter('name', array('like' => '%'.$searchtext.'%'));
        }

        // $collection->addAttributeToSelect('*');
        $products = $collection->load();

        Mage::register('current_category',$category);
        Mage::register('product_collection',$products);
        Mage::register('searchtext',$searchtext);
        Mage::register('lang',$lang);
        //$this->getLayout()->getBlock('root')->setTemplate('page/1column.phtml');
        //$this->getLayout()->getBlock('content')->append($block);
		$this->_initLayoutMessages('core/session');
       // Zend_Debug::dump($this->getLayout()->getUpdate()->getHandles());
        $this->renderLayout();
        	
    }
    
}