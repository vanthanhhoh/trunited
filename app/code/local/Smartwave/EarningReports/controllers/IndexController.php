<?php
class Smartwave_EarningReports_IndexController extends Mage_Core_Controller_Front_Action
{
    
    public function indexAction(){
        //Get current layout state
        $this->loadLayout();     
		$block = $this->getLayout()->createBlock(
            'Mage_Core_Block_Template',
            'earning_reports',
            array('template' => 'page/earning_reports.phtml')
        );
		$this->getLayout()->getBlock('head')->setTitle('Earning Reports');
        $this->getLayout()->getBlock('root')->setTemplate('page/1column.phtml');
        $this->getLayout()->getBlock('content')->append($block);
		$this->_initLayoutMessages('core/session'); 
        $this->renderLayout();		
    }
    /*public function globalAction(){
        //Get current layout state
        $params = $this->getRequest()->getParams();
        $url = $params['url'];
        $url = str_replace('.html', '', $url);
        $products = Mage::getModel('catalog/product')
                    ->getCollection()
                    ->addAttributeToSelect('*')
                    ->addAttributeToFilter(
                        array(array('attribute' => 'url_key', 'like' => $url))
                    )
                    ->load();
        if (count($products)<=0){
            $products = Mage::getModel('catalog/product')
                    ->getCollection()
                    ->addAttributeToSelect('*')
                    ->addAttributeToFilter(
                        array(array('attribute' => 'sku', 'like' => $url))
                    )
                    ->load();
        }
        if (count($products)>0){
            foreach($products as $product){
                $product = $product;
                break;
            }
        }*/

        //$this->loadLayout();   
        //$block = $this->getLayout()->createBlock(
        //   'Mage_Core_Block_Template',
        //    'customer_dashboard',
        //    array('template' => 'page/customer_dashboard.phtml')
       // );
        //Mage::register('current_product',$product);
        //$name = ($product) ? $product->getName() : '404 page not found';
        //$this->getLayout()->getBlock('head')->setTitle($name);
        //$this->getLayout()->getBlock('root')->setTemplate('page/2columns-left.phtml');
        //$this->getLayout()->getBlock('content')->append($block);
        //$this->_initLayoutMessages('core/session'); 
        //$this->renderLayout();
    //}
}